﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TAM.Scheduler
{
    public class PVOutput
    {
        public int PV_NO { get; set; }
        public int PV_YEAR { get; set; }
        public string STATUS_PV { get; set; }
        public string ERR_MESSAGE { get; set; }
    }
}
