﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TAM.Scheduler
{
    public partial class PVHeader
    {
       
        public int? PROCESS_ID { get; set; }

        public string UPLOAD_FILENAME { get; set; }
        public int? REFF_NO { get; set; }
        public int? ROWS_COUNT { get; set; }
        public byte? OK { get; set; }

        public string PV_NO { get; set; }//
        public string PV_YEAR { get; set; }//
        public int? STATUS_CD { get; set; }//
        public string PAY_METHOD_CD { get; set; }//
        public int? VENDOR_GROUP_CD { get; set; }//
        public string VENDOR_CD { get; set; }//
        public int? PV_TYPE_CD { get; set; }//
        public int? TRANSACTION_CD { get; set; }//
        public int? SUSPENSE_NO { get; set; }//
        public DateTime? ACTIVITY_DATE_FROM { get; set; }//
        public DateTime? ACTIVITY_DATE_TO { get; set; }//
        public string DIVISION_ID { get; set; }//
        public DateTime? PV_DATE { get; set; }//
        public int? REFFERENCE_NO { get; set; }//
        public int? BANK_TYPE { get; set; }//
        public DateTime? POSTING_DATE { get; set; }//
        public decimal AMOUNT { get; set; }//

        public string PV_TYPE { get; set; }//
        public string PAYMENT_METHOD { get; set; }//
        public string BUDGET { get; set; }//
        public string TRANSACTION_DESC { get; set; }//
        public string VENDOR_NAME { get; set; }//
        public string VENDOR_GRP { get; set; }

        // 2020-03-02
        public string STATUS_FLAG { get; set; }
        public string STATUS_DESC { get; set; }
        public bool IS_BUDGET { get; set; }
        public string STATUS_BUDGET { get; set; }
        public string INVOICE_NO { get; set; }
        public string PV_DATE_DT { get; set; }
        public string REJECTED_REASON { get; set; }//
        public string IS_USERTAM { get; set; }
        public decimal TOTAL_TURNOVER { get; set; }// 
        public string IS_EBILLING { get; set; }

        public string CREATED_BY { get; set; }
        public string CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public string CHANGED_DT { get; set; }

        public int approveLevel { get; set; }
        public bool financeHeader { get; set; }

        public List<PVMonitoring> pVMonitorings { get; set; }
        public List<PVAmount> pvAmounts { get; set; }
        public List<PVMonitoringAttachment> pvAttachments { get; set; }

        // 2020-03-30
        public string ACCOUNT_INFO { get; set; }

        //2020-03-31
        public string STATUS_COMPARE { get; set; }
        public DateTime? EXPIRED_DATE { get; set; }
        public string URL_WHT { get; set; }

        public string REJECTED_BY { get; set; }
        public string REJECTED_DT { get; set; }

        //Email
        public int TOTAL_NEW { get; set; }
        public int TOTAL_ALL { get; set; }
        public int REMAINING_DAY { get; set; }
        public string GROUP { get; set; }
        public string DAY_REMAINING { get; set; }
        public string DOMAIN { get; set; }

        public string BUDGET_NO_FORMAT { get; set; }
        public string BUDGET_DESCRIPTION { get; set; }
        public string PV_TYPE_NAME { get; set; }
        public string DESCRIPTION { get; set; }
        public string DIVISION_NAME { get; set; }

        public DateTime? PLANNING_PAYMENT_DT { get; set; }
        public DateTime? ACTUAL_PAYMENT_DT { get; set; }

        #region Enhancement juli 2021
        public string NOMINATIVE_TYPE { get; set; }
        public string CATEGORY_CODE { get; set; }
        public string ENOM_NO { get; set; }
        //public string NOMINATIVE_STATUS { get; set; } 
        #endregion

        //2021-28-06 wot ardi
        public DateTime? ACTUAL_PAYMENT_DATE { get; set; }
        public DateTime? VERIFIED_DATE { get; set; }
    }
}
