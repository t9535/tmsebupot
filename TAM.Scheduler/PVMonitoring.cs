﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TAM.Scheduler
{
    public partial class PVMonitoring
    {
   
        public int? PROCESS_ID { get; set; }
        public int SEQ_NO { get; set; } //
        public int SEQ_XCL { get; set; }
        public string INVOICE_NO { get; set; }//
        public string INVOICE_DATE { get; set; }//
        public string COST_CENTER_CD { get; set; }//
        public string CURRENCY_CD { get; set; }//
        public Double? AMOUNT { get; set; }//
        public string DESCRIPTION { get; set; }//
        public byte OK { get; set; }
        public int? GL_ACCOUNT { get; set; }//
        public Double? DPP_AMOUNT { get; set; }//
        public string ACC_INF_ASSIGNMENT { get; set; }//
        public string TAX_NO { get; set; }//
        public string TAX_DT { get; set; }//
        public Double? AMOUNT_TURN_OVER { get; set; }
        //public string AMOUNT_TURN_OVER { get; set; }
        public Double? AMOUNT_PPN { get; set; }
        public Double? AMOUNT_SEAL { get; set; }
        public Double? AMOUNT_PPh21_INTERN { get; set; }
        public Double? AMOUNT_PPh21_EXTERN { get; set; }
        public Double? AMOUNT_PPh26 { get; set; }
        public Double? AMOUNT_PPh_FINAL { get; set; }
        public Double? AMOUNT_BM { get; set; }
        public Double? AMOUNT_PPN_IMPORT { get; set; }
        public Double? AMOUNT_PPnBM { get; set; }
        public Double? AMOUNT_PPh22 { get; set; }
        public Double? AMOUNT_PnPB { get; set; }
        public Double? AMOUNT_BANK_CHARGE { get; set; }
        public string ERRMSG { get; set; }

        public Double? AMOUNT_SALES_OTHER { get; set; }
        public Double? AMOUNT_RENTAL_EXPENSE { get; set; }
        public Double? AMOUNT_TRANSPORT_OPERATIONAL { get; set; }
        public Double? AMOUNT_TRANSPORT_TOLL { get; set; }
        public Double? AMOUNT_CONSUMPTION { get; set; }
        public Double? AMOUNT_MEALS_PAPER { get; set; }
        public Double? AMOUNT_OFFICE_SUPPLIES { get; set; }
        public Double? AMOUNT_COMMUNICATION { get; set; }
        public Double? AMOUNT_NEWSPAPER { get; set; }
        public Double? AMOUNT_REPAIR { get; set; }
        public Double? AMOUNT_OTHER { get; set; }
        public Double? DPP_PPh_AMOUNT { get; set; }
        public string TAX_CODE_PPh23 { get; set; }
        public Double? TAX_TARIFF_PPh23 { get; set; }
        public string TAX_CODE_PPh21_EXTERN { get; set; }
        public Double? TAX_DPP_ACCUMULATION_PPH21_EXTERN { get; set; }
        public Double? TAX_TARIFF_PPh21_EXTERN { get; set; }
        public string NIK_PPh21_EXTERN { get; set; }
        public string TAX_CODE_PPh26 { get; set; }
        public Double? TAX_TARIFF_PPh26 { get; set; }
        public string TAX_CODE_PPh_FINAL { get; set; }
        public Double? TAX_TARIFF_PPh_FINAL { get; set; }
        public string INFO_PPh_FINAL { get; set; }
        public string NPWP_Available { get; set; }

        public DateTime CREATED_DATE { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? MODIFIED_DATE { get; set; }
        public string MODIFIED_BY { get; set; }

        //Gak ada ditable
        public Double? MATERAI { get; set; }
        public Double? AMOUNT_PPh23 { get; set; }

        //Helper
        public DateTime INVOICE_DATE_DT { get; set; }
        public string STATUS_FLAG { get; set; }
        public string STATUS_DESC { get; set; }
        public string ATTACHMENT_UPLOADED { get; set; }
        public string PV_NO { get; set; } //
        public string PV_YEAR { get; set; } //
        public string WBS_NO { get; set; } //
        public int? ITEM_TRANSACTION_CD { get; set; }//
        public string TAX_CD { get; set; }//
        public string FROM_CURR { get; set; }//
        public string FROM_SEQ { get; set; }//
        public int? ITEM_NO { get; set; }//
        public string TAX_ASSIGNMENT { get; set; }//
        public string WHT_TAX_CODE { get; set; }//
        public string WHT_TAX_TARIFF { get; set; }//
        public string WHT_TAX_ADDITIONAL_INFO { get; set; }//
        public string WHT_DPP_PPh_AMOUNT { get; set; }//

        public string VENDOR_DESC { get; set; }
        public string STATUS_BUDGET { get; set; }
        public string IS_USERTAM { get; set; }

        public string ACCOUNT_INFO { get; set; }
        public string STATUS_FAKTUR_PAJAK { get; set; }
        public string BUDGET_NO { get; set; }
    }
}
