﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TAM.Scheduler
{
    public class WHTManualInputViewEprocModel
    {
        public Guid Id { get; set; }
        public Guid TransactionId { get; set; }
        public String InvoiceNumber { get; set; }
        public string TaxType { get; set; }
        public String InvoiceNumberFull { get; set; }
        public String KDJenisTransaksi { get; set; }
        public String FGPengganti { get; set; }
        public String InvoiceDate { get; set; }
        public String SupplierNPWP { get; set; }
        public String SupplierName { get; set; }
        public String SupplierAddress { get; set; }
        public String NPWPLawanTransaksi { get; set; }
        public String NamaLawanTransaksi { get; set; }
        public String AlamatLawanTransaksi { get; set; }
        public String StatusApprovalXML { get; set; }
        public String StatusFakturXML { get; set; }
        public string FileName { get; set; }
        public string URL { get; set; }
        public Nullable<Decimal> VATBaseAmount { get; set; }
        public Nullable<Decimal> VATAmount { get; set; }
        public Nullable<Decimal> JumlahPPnBM { get; set; }

        IList<VATInDetailManualInputViewEprocModel> _VATInEprocDetails = new List<VATInDetailManualInputViewEprocModel>();
        public IList<VATInDetailManualInputViewEprocModel> VATInEprocDetails { get { return _VATInEprocDetails; } set { _VATInEprocDetails = value; } }

        public String ExpireDate { get; set; }
        public String NomorInvoice { get; set; }
        public String StatusInvoice { get; set; }
        public String CreatedByPV { get; set; }
        public String StatusEPROC { get; set; }
        public String PVNumber { get; set; }
        public Nullable<DateTime> PostingDate { get; set; }

        public String SAPDocNumber { get; set; }
        public String SAPPostingDate { get; set; }
        public String SupplierNPWPNumber { get; set; }
        public String VendorEFakturFlag { get; set; }
        public String Description { get; set; }

        public String TurnOverAmount { get; set; }

        public String InvoiceSubmittedBy { get; set; }

        public String RemainingTaxInvoice { get; set; }

        public String Item { get; set; }

        public String SendDateTime { get; set; }
        public String TaxInvoiceDate { get; set; }
    }
}
