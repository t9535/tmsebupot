﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAM.Scheduler.OCR;

namespace TAM.Scheduler.MatchingData
{
    class MatchingDataWHT
    {

        public static int SaveMatchingDataWHT(string eBupotNumber, string SourcePath, string FileName, string DestinationPath, string NewFileName, string failPath)
        {

         //  
           

            try
            {
                int isPDFData = 0;
                Connection cn = new Connection();
                cn.CreateConnection();
                cn.OpenConnection();

                cn._sqlCommand.CommandText = "sp_eBupot_compare";
                cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                cn._sqlCommand.Parameters.AddWithValue("@eBupotNumber", eBupotNumber);
                cn._sqlCommand.Parameters.AddWithValue("@SourcePath", SourcePath);
                cn._sqlCommand.Parameters.AddWithValue("@FileName", FileName);
                cn._sqlCommand.Parameters.AddWithValue("@DestinationPath", DestinationPath);
                cn._sqlCommand.Parameters.AddWithValue("@NewFileName", NewFileName);

                SqlDataReader rd = cn._sqlCommand.ExecuteReader();
                if (rd.Read())
                {
                    isPDFData = Convert.ToInt32(rd["isPDFData"]);
                }
                
                cn.CloseConnection();
                cn.DisposeConnection();
                return isPDFData;
            }
            catch (Exception ex)
            {
                
                SyncLog.InsertLog(failPath, eBupotNumber, "EBUPOT_WHT_ERR", ex.Message);
                return 0;

            }
            
        }
    }
}
