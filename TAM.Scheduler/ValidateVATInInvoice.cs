﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TAM.Scheduler
{
    class ValidateVATInInvoice
    {
        public static string Validate(string nomor)
        {
            string Result = "";
            Connection cn = new Connection();

            cn.CreateConnection();
            cn.OpenConnection();

            cn._sqlCommand.CommandText = "usp_ValidateVATInInvoiceNumberEproc";
            cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            cn._sqlCommand.Parameters.AddWithValue("@Invoice", Convert.ToString(nomor));
            cn._sqlCommand.ExecuteNonQuery();

            SqlDataReader rd = cn._sqlCommand.ExecuteReader();
            rd.Read();

            Result = rd[0].ToString();

            cn.CloseConnection();
            cn.DisposeConnection();

            return Result;
        }
    }
}
