﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TAM.Scheduler
{
    public class VATInDetailManualInputViewEbilingModel
    {
        public Guid tpid { get; set; }
        public int RowNum { get; set; }
        public String UnitName { get; set; }
        public Decimal UnitPrice { get; set; }
        public Decimal Quantity { get; set; }
        public Decimal TotalPrice { get; set; }
        public Decimal Discount { get; set; }
        public Decimal DPP { get; set; }
        public Decimal PPN { get; set; }
        public Decimal TarifPPNBM { get; set; }
        public Decimal PPNBM { get; set; }
    }
}
