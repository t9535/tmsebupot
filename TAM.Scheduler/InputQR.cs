﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAM.EFaktur.Web.Models.VATIn;

namespace TAM.Scheduler
{
    class InputQR
    {
        public static void save(VATInManualInputViewModel model, Guid SyncID)
        {
            Connection cn = new Connection();
            cn.CreateConnection();
            cn.OpenConnection();

            cn._sqlCommand.CommandText = "usp_InsertCustom_TB_R_QRURL";
            cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            cn._sqlCommand.Parameters.AddWithValue("@Id", model.Id);
            cn._sqlCommand.Parameters.AddWithValue("@SyncId", SyncID);
            cn._sqlCommand.Parameters.AddWithValue("@KDJenisTransaksi", model.KDJenisTransaksi);
            cn._sqlCommand.Parameters.AddWithValue("@FGPengganti", model.FGPengganti);
            cn._sqlCommand.Parameters.AddWithValue("@NomorFaktur", model.InvoiceNumber);
            cn._sqlCommand.Parameters.AddWithValue("@NomorFakturGabungan", model.InvoiceNumberFull);
            cn._sqlCommand.Parameters.AddWithValue("@TanggalFaktur", model.InvoiceDate);
            cn._sqlCommand.Parameters.AddWithValue("@ExpireDate", model.ExpireDate);
            cn._sqlCommand.Parameters.AddWithValue("@NPWPPenjual", model.SupplierNPWP);
            cn._sqlCommand.Parameters.AddWithValue("@NamaPenjual", model.SupplierName);
            cn._sqlCommand.Parameters.AddWithValue("@AlamatPenjual", model.SupplierAddress);
            cn._sqlCommand.Parameters.AddWithValue("@JumlahDPP", model.VATBaseAmount);
            cn._sqlCommand.Parameters.AddWithValue("@JumlahPPN", model.VATAmount);
            cn._sqlCommand.Parameters.AddWithValue("@JumlahPPNBM", model.JumlahPPnBM);
            cn._sqlCommand.Parameters.AddWithValue("@StatusApprovalXML", model.StatusApprovalXML);
            cn._sqlCommand.Parameters.AddWithValue("@StatusFakturXML", model.StatusFakturXML);
            cn._sqlCommand.Parameters.AddWithValue("@FakturType", "eFaktur");
            cn._sqlCommand.Parameters.AddWithValue("@CreatedOn", DateTime.Now);
            cn._sqlCommand.Parameters.AddWithValue("@CreatedBy", "System");
            cn._sqlCommand.Parameters.AddWithValue("@FileName", model.FileName);
            cn._sqlCommand.Parameters.AddWithValue("@URL", model.URL);
            cn._sqlCommand.Parameters.AddWithValue("@NPWPLawanTransaksi", model.NPWPLawanTransaksi);
            cn._sqlCommand.ExecuteReader();
            cn.CloseConnection();
            cn.DisposeConnection();
        }
    }
}
