﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TAM.Scheduler
{
    class Connection
    {
        public static string connetionString = ConfigurationManager.ConnectionStrings["TAM_EFAKTURConnectionString"].ConnectionString;// "Data Source=BII-NB;Initial Catalog=TAM_EFAKTUR;User ID=sa;Password=sa";
        public static string connetionString2 = ConfigurationManager.ConnectionStrings["ELVIS_DBConnectionString"].ConnectionString;//"Data Source=.\\MSSQLSERVER2014;Initial Catalog=ELVIS_DB;User ID=sa;Password=sa";
        public SqlCommand _sqlCommand;


      

        protected static IDbConnection cnnOpen()
        {
            var conn = connetionString;
            IDbConnection connection = new SqlConnection(conn);
            connection.Open();
            return connection;

        }

        public  IDbConnection cnnopp()
        {

            
            var conn = connetionString;
            IDbConnection connection = new SqlConnection(conn);
            connection.Open();
            return connection;
        }

        public  IDbConnection cnnDispose()
        {

            var conn = connetionString;
            IDbConnection connection = new SqlConnection(conn);
            connection.Dispose();
            return connection;
        }
        
        public IEnumerable<TRes> executeProcedure<TRes>(string spName, object param, CommandType? commandType = null)
        {
            using (var connection = cnnOpen())
            {
                return connection.Query<TRes>(spName, param, commandType:
                    commandType ?? CommandType.StoredProcedure);
            }
        }

        protected IDbTransaction DbTx = null;
        protected int? DbTimeOut = null;
        public void ExecData(
                string spName
              , object param
              , IDbTransaction transaction = null
              , int? commandTimeout = null
              , CommandType? commandType = CommandType.StoredProcedure)
        {
            var connection = cnnOpen();
             connection.Execute(spName, param
                , transaction ?? DbTx, commandTimeout ?? DbTimeOut, commandType);
        }
        public void CreateConnection()
        {
            SqlConnection _sqlConnection = new SqlConnection(connetionString);
            _sqlCommand = new SqlCommand();
            _sqlCommand.Connection = _sqlConnection;
        }
        public void CreateConnection2()
        {
            SqlConnection _sqlConnection = new SqlConnection(connetionString2);
            _sqlCommand = new SqlCommand();
            _sqlCommand.Connection = _sqlConnection;
        }
        public void OpenConnection()
        {
            _sqlCommand.Connection.Open();
        }
        public void CloseConnection()
        {
            _sqlCommand.Connection.Close();
        }
        public void DisposeConnection()
        {
            _sqlCommand.Connection.Dispose();
        }
        public void RefreshParam()
        {
            _sqlCommand.Parameters.Clear();
        }
    }
}
