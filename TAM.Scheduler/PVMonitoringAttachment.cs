﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TAM.Scheduler
{
    public partial class PVMonitoringAttachment
    {
       
        public string REFERENCE_NO { get; set; }
        public int REF_SEQ_NO { get; set; }
        public int ID { get; set; }
        public string ATTACH_CD { get; set; }
        public Int64 AttachmentId { get; set; }
        public string DESCRIPTION { get; set; }
        public string DIRECTORY { get; set; }
        public string FILE_NAME { get; set; }

        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }
        public string FILE_TYPE { get; set; }

        public string ValidateFileType { get; set; }
        public bool ValidateVendorGroup { get; set; }

        public bool IS_ENOMINATIVE { get; set; }

        //helper
        public string ATTACH_DESC { get; set; }
        public string STATUS { get; set; }
        public string PV_NO { get; set; }
        public string PV_YEAR { get; set; }
        public string IS_USERTAM { get; set; }
        public string BY_INTERNAL { get; set; }
    }
}
