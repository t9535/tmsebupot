﻿using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using TAM.Scheduler;
using System.Data.SqlClient;
//using System.Web.Mvc;
//using System.Windows.Documents;
using TAM.EFaktur.Web.Models;
//using TAM.EFaktur.Web.Models.Master_Config;
//using TAM.EFaktur.Web.Models.VATOut;

namespace TAM.Scheduler
{
    public class PdfHelper
    {

        #region PDF Nota Retur Original VAT Out
        public static string CreatePDFNotaRetur(List<string[]> ListArr, string pdffullpath)
        {
            var document = CreateDocument(ListArr);

            var ddl = MigraDoc.DocumentObjectModel.IO.DdlWriter.WriteToString(document);
            //MigraDoc.DocumentObjectModel.IO.DdlWriter.WriteToFile(document, "MigraDoc.mdddl");

            var renderer = new PdfDocumentRenderer(true);
            renderer.Document = document;

            renderer.RenderDocument();

            // Save the document...
            string SourcePathGL = GetConfigValueByConfigKey("UrlTemplateGenerateNotaVATOutSalesRetur");

            string path = Path.Combine(SourcePathGL, pdffullpath);
            //string path = "D:/Interface/Temp/Upload/";

            var filename = path + ".pdf";

            renderer.PdfDocument.Save(filename);
            return filename;

        }
        public static Document CreateDocument(List<string[]> data)
        {
            // Create a new MigraDoc document.
            var document = new Document();

            DefineStyles(document);

            DefineContentSection(document);

            CreatePDFDocument(document, data);

            return document;

        }

        static void DefineContentSection(Document document)
        {
            var section = document.AddSection();
            section.PageSetup.OddAndEvenPagesHeaderFooter = true;
            section.PageSetup.StartingNumber = 1;
           
            // Create a paragraph with centered page number. See definition of style "Footer".
            var paragraph = new MigraDoc.DocumentObjectModel.Paragraph();
            paragraph.AddTab();
            paragraph.AddPageField();

            // Add paragraph to footer for odd pages.
            section.Footers.Primary.Add(paragraph);
            // Add clone of paragraph to footer for odd pages. Cloning is necessary because an object must
            // not belong to more than one other object. If you forget cloning an exception is thrown.
            section.Footers.EvenPage.Add(paragraph.Clone());
        }

        public static void DefineStyles(Document document)
        {
            // Get the predefined style Normal.
            var style = document.Styles["Normal"];
            // Because all styles are derived from Normal, the next line changes the 
            // font of the whole document. Or, more exactly, it changes the font of
            // all styles and paragraphs that do not redefine the font.
            style.Font.Name = "Segoe UI";

            // Heading1 to Heading9 are predefined styles with an outline level. An outline level
            // other than OutlineLevel.BodyText automatically creates the outline (or bookmarks) 
            // in PDF.

            style = document.Styles["Heading1"];
            style.Font.Name = "Segoe UI Light";
            style.Font.Size = 16;
            //style.Font.Bold = true;
            style.Font.Color = Colors.DarkBlue;
            style.ParagraphFormat.PageBreakBefore = true;
            style.ParagraphFormat.SpaceAfter = 6;
            // Set KeepWithNext for all headings to prevent headings from appearing all alone
            // at the bottom of a page. The other headings inherit this from Heading1.
            style.ParagraphFormat.KeepWithNext = true;

            style = document.Styles["Heading2"];
            style.Font.Size = 14;
            //style.Font.Bold = true;
            style.ParagraphFormat.PageBreakBefore = false;
            style.ParagraphFormat.SpaceBefore = 6;
            style.ParagraphFormat.SpaceAfter = 6;

            style = document.Styles["Heading3"];
            style.Font.Size = 12;
            //style.Font.Bold = true;
            style.Font.Italic = true;
            style.ParagraphFormat.SpaceBefore = 6;
            style.ParagraphFormat.SpaceAfter = 3;

            style = document.Styles[StyleNames.Header];
            style.ParagraphFormat.AddTabStop("16cm", TabAlignment.Right);

            style = document.Styles[StyleNames.Footer];
            style.ParagraphFormat.AddTabStop("8cm", TabAlignment.Center);

            // Create a new style called TextBox based on style Normal.
            style = document.Styles.AddStyle("TextBox", "Normal");
            style.ParagraphFormat.Alignment = ParagraphAlignment.Justify;
            style.ParagraphFormat.Borders.Width = 2.5;
            style.ParagraphFormat.Borders.Distance = "3pt";
            //TODO: Colors
            style.ParagraphFormat.Shading.Color = Colors.SkyBlue;

            // Create a new style called TOC based on style Normal.
            style = document.Styles.AddStyle("TOC", "Normal");
            style.ParagraphFormat.AddTabStop("16cm", TabAlignment.Right, TabLeader.Dots);
            style.ParagraphFormat.Font.Color = Colors.Blue;
        }

        public static void CreatePDFDocument(Document document, List<string[]> data)
        {

            var table = document.LastSection.AddTable();
            table.Format.Font.Size = 9;
            table.Borders.Visible = true;
            table.TopPadding = 5;
            table.BottomPadding = 5;
            table.Rows.Height = 35;

            #region Add Column 
            var column = table.AddColumn(Unit.FromCentimeter(1)); //No
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn(Unit.FromCentimeter(3)); //Macam dan Jenis Barang Kena Pajak
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn(Unit.FromCentimeter(3)); //Macam dan Jenis Barang Kena Pajak
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn(Unit.FromCentimeter(2)); //Kuantum
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn(Unit.FromCentimeter(4)); //Harga Satuan Menurut Faktur Pajak (Rp.)
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn(Unit.FromCentimeter(4)); //Harga BKP yang dikembalikan (Rp.)
            column.Format.Alignment = ParagraphAlignment.Center;
            #endregion

            #region Header 
            var row = table.AddRow();

            string[] danumber = data[0];
            string[] nofakturgabungan = data[1];
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[0].MergeRight = 5;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[0].AddParagraph("NOTA RETUR").Format.Font.Bold = true;
            row.Cells[0].AddParagraph(danumber[0]);
            row.Cells[0].AddParagraph(nofakturgabungan[0]);

            row = table.AddRow();
            row.Height = 20;
            row.Borders.Top.Color = Colors.Black;
            row.Borders.Color = Colors.Transparent;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.Format.Font.Bold = true;
            row.Cells[0].MergeRight = 4;
            row.Cells[0].AddParagraph("PEMBELI");

            string[] Namacustomer = data[2];
            row = table.AddRow();
            row.Height = 20;
            row.Borders.Color = Colors.Transparent;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.Cells[0].MergeRight = 1;
            row.Cells[0].AddParagraph("Nama");
            row.Cells[2].MergeRight = 3;
            row.Cells[2].AddParagraph(Namacustomer[0]);

            string[] Alamatcustomer = data[3];
            row = table.AddRow();
            row.Height = 20;
            row.Borders.Color = Colors.Transparent;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.Cells[0].MergeRight = 1;
            row.Cells[0].AddParagraph("Alamat");
            row.Cells[2].MergeRight = 3;
            row.Cells[2].AddParagraph(Alamatcustomer[0]);

            string[] NPWPCustomer = data[4];
            row = table.AddRow();
            row.Height = 20;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.Borders.Color = Colors.Transparent;
            row.Cells[0].MergeRight = 1;
            row.Cells[0].AddParagraph("NPWP");
            row.Cells[2].MergeRight = 3;
            row.Cells[2].AddParagraph(NPWPCustomer[0]);

            row = table.AddRow();
            row.Height = 20;
            row.Borders.Color = Colors.Transparent;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.Format.Font.Bold = true;
            row.Cells[0].MergeRight = 4;
            row.Cells[0].AddParagraph("KEPADA PENJUAL");

            row = table.AddRow();
            row.Height = 20;
            row.Borders.Color = Colors.Transparent;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.Cells[0].MergeRight = 1;
            row.Cells[0].AddParagraph("Nama");
            row.Cells[2].MergeRight = 3;
            row.Cells[2].AddParagraph(": PT TOYOTA-ASTRA MOTOR");

            row = table.AddRow();
            row.Height = 20;
            row.Borders.Color = Colors.Transparent;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.Cells[0].MergeRight = 1;
            row.Cells[0].AddParagraph("Alamat");
            row.Cells[2].MergeRight = 3;
            row.Cells[2].AddParagraph(": JALAN GAYA MOTOR SELATAN NO 5, SUNGAI BAMBU, TANJUNG PRIOK JAKARTA UTARA - DKI JAKARTA 14330");

            row = table.AddRow();
            row.Height = 20;
            row.Borders.Color = Colors.Transparent;
            row.Borders.Bottom.Color = Colors.Black;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.Cells[0].MergeRight = 1;
            row.Cells[0].AddParagraph("Nomor Pengukuhan PKP");
            row.Cells[2].MergeRight = 3;
            row.Cells[2].AddParagraph(": 02.116.115.3-092.000");

            #endregion


            #region Grid Table

            row = table.AddRow();
            row.VerticalAlignment = VerticalAlignment.Center;
            for (int i = 8; i < 9; i++)
            {
                string[] columnHeader1 = data[i];
                for (int j = 0; j < columnHeader1.Count(); j++)
                {
                    if (j == 1)
                    {
                        row.Cells[j].MergeRight = 1;
                        
                    }
                    row.Cells[j].AddParagraph(columnHeader1[j]);

                }
            }

            int number = 0;
            for (int i = 9; i < data.Count(); i++)
            {
                row = table.AddRow();
                string[] datacolumn = data[i];
                number++;
                for (int a = 0; a < datacolumn.Count(); a++)
                {
                    //a0-1 = center
                    //a2 = left
                    //a3 = center
                    //a4-5 = right
                    if (a == 0)
                    {
                        row.Cells[a].AddParagraph("");
                        row.Cells[a].AddParagraph(number.ToString());

                    }
                    else if (a == 2)
                    {
                        row.Cells[a].AddParagraph("0022058379454").Format.Alignment = ParagraphAlignment.Left;
                        row.Cells[a].AddParagraph(datacolumn[a]).Format.Alignment = ParagraphAlignment.Left;
                    }
                    else if (a == 4 || a == 5)
                    {
                        row.Cells[a].AddParagraph("");
                        row.Cells[a].AddParagraph(datacolumn[a]).Format.Alignment = ParagraphAlignment.Right;
                    }
                    else
                    {
                        row.Cells[a].AddParagraph("");
                        row.Cells[a].AddParagraph(datacolumn[a]);
                    }
                }
            }

            #endregion


            #region Table Total

            string[] JumlahBKP = data[5];
            row = table.AddRow();
            row.Height = 20;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.Cells[0].MergeRight = 4;
            row.Cells[0].AddParagraph("Jumlah harga BKP yang dikembalikan:");
            row.Cells[5].Format.Alignment = ParagraphAlignment.Right;
            row.Cells[5].AddParagraph(JumlahBKP[0]);

            string[] JumlahPPN = data[6];
            row = table.AddRow();
            row.Height = 20;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.Cells[0].MergeRight = 4;
            row.Cells[0].AddParagraph("Pajak Pertambahan Nilai yang diminta kembali:");
            row.Cells[5].Format.Alignment = ParagraphAlignment.Right;
            row.Cells[5].AddParagraph(JumlahPPN[0]);

            row = table.AddRow();
            row.Height = 20;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.Cells[0].MergeRight = 4;
            row.Cells[0].AddParagraph("Pajak Penjualan Atas Barang Mewah diminta kembali");
            row.Cells[5].Format.Alignment = ParagraphAlignment.Right;
            row.Cells[5].AddParagraph("");

            #endregion

            #region TTD

            row = table.AddRow();
            row.Height = 20;
            row.Borders.Bottom.Color = Colors.Transparent;
            row.Borders.Right.Color = Colors.Transparent;
            row.Cells[0].MergeRight = 4;
            row.Cells[5].AddParagraph("__________________");

            row = table.AddRow();
            row.Height = 60;
            row.Borders.Color = Colors.Transparent;
            row.Cells[0].MergeRight = 4;
            row.Cells[5].VerticalAlignment = VerticalAlignment.Bottom;
            row.Cells[5].AddParagraph("(__________________)");

            row = table.AddRow();
            row.Height = 20;
            row.Borders.Top.Color = Colors.Transparent;
            row.Borders.Right.Color = Colors.Transparent;
            row.Cells[0].MergeRight = 4;

            #endregion 

            #region Footer

            row = table.AddRow();
            row.Height = 20;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.Borders.Right.Color = Colors.Transparent;
            row.Cells[0].MergeRight = 4;
            row.Cells[0].AddParagraph("Lembar ke-1 : Untuk Pengusaha Kena Pajak Yang Menerbitkan Faktur Pajak.");
            row.Cells[0].AddParagraph("Lembar ke-2 : Untuk Pembeli.");

            #endregion

            //set border box table
            table.SetEdge(0, 0, 6, number + 17, Edge.Box, BorderStyle.Single, 0.5, Colors.Black);
        }
        #endregion

        #region PDF Original DRKB 
        public static string PutExcelDRKBOriginal(List<string[]> ListArr, object o, string TempDownloadFolder, string XlsName = null, bool ForceName = false)
        {
            var document = CreateDocumentDRKBOriginal(ListArr);

            var ddl = MigraDoc.DocumentObjectModel.IO.DdlWriter.WriteToString(document);
          
            var renderer = new PdfDocumentRenderer(true);
            renderer.Document = document;

            renderer.RenderDocument();

            // Save the document...      
            string fileName = XlsName + " Original";
            string fullPath = Path.Combine(@TempDownloadFolder, fileName + ".pdf");

            renderer.PdfDocument.Save(fullPath);
            return fullPath;
        }

        public static void DefineStylesDRKB(Document document)
        {
            // Get the predefined style Normal.
            var style = document.Styles["Normal"];          
            style.Font.Name = "Segoe UI";

            style = document.Styles["Heading1"];
            style.Font.Name = "Segoe UI Light";
            style.Font.Size = 16;
            //style.Font.Bold = true;
            style.Font.Color = Colors.DarkBlue;
            style.ParagraphFormat.PageBreakBefore = true;
            style.ParagraphFormat.SpaceAfter = 6;
            // Set KeepWithNext for all headings to prevent headings from appearing all alone
            // at the bottom of a page. The other headings inherit this from Heading1.
            style.ParagraphFormat.KeepWithNext = true;

            style = document.Styles["Heading2"];
            style.Font.Size = 14;
            //style.Font.Bold = true;
            style.ParagraphFormat.PageBreakBefore = false;
            style.ParagraphFormat.SpaceBefore = 6;
            style.ParagraphFormat.SpaceAfter = 6;

            style = document.Styles["Heading3"];
            style.Font.Size = 12;
            //style.Font.Bold = true;
            style.Font.Italic = true;
            style.ParagraphFormat.SpaceBefore = 6;
            style.ParagraphFormat.SpaceAfter = 3;

            style = document.Styles[StyleNames.Header];
            style.ParagraphFormat.AddTabStop("16cm", TabAlignment.Right);
           
            // Create a new style called TextBox based on style Normal.
            style = document.Styles.AddStyle("TextBox", "Normal");
            style.ParagraphFormat.Alignment = ParagraphAlignment.Justify;
            style.ParagraphFormat.Borders.Width = 2.5;
            style.ParagraphFormat.Borders.Distance = "3pt";
            //TODO: Colors
            style.ParagraphFormat.Shading.Color = Colors.SkyBlue;

            // Create a new style called TOC based on style Normal.
            style = document.Styles.AddStyle("TOC", "Normal");
            style.ParagraphFormat.AddTabStop("16cm", TabAlignment.Right, TabLeader.Dots);
            style.ParagraphFormat.Font.Color = Colors.Blue;
        }
        public static Document CreateDocumentDRKBOriginal(List<string[]> datadrkb)
        {
            // Create a new MigraDoc document.
            var document = new Document();

            //set document orientation
            PageSetup pageSetup = document.DefaultPageSetup.Clone();

            DefineStylesDRKB(document);

            DefineContentSectionDRKB(document);

            CreatePDFDocumentDRKBOriginal(document, datadrkb);

            return document;

        }
        static void DefineContentSectionDRKB(Document document)
        {
            var section = document.AddSection();
            section.PageSetup.OddAndEvenPagesHeaderFooter = false;                   
            section.PageSetup.PageFormat = PageFormat.A2;         
            section.PageSetup.StartingNumber = 1;         
        

            // Create a paragraph with centered page number. See definition of style "Footer".
            var paragraph = new MigraDoc.DocumentObjectModel.Paragraph();
            paragraph.AddTab();
            paragraph.AddPageField();

            // Add paragraph to footer for odd pages.
            section.Footers.Primary.Add(paragraph);
            section.Footers.Primary.Format.Alignment = ParagraphAlignment.Center;
            section.Footers.Primary.Format.Font.Bold = true;
            // Add clone of paragraph to footer for odd pages. Cloning is necessary because an object must
            // not belong to more than one other object. If you forget cloning an exception is thrown.
            section.Footers.EvenPage.Add(paragraph.Clone());
        }
        public static void CreatePDFDocumentDRKBOriginal(Document document, List<string[]> datadrkb)
        {
            document.LastSection.PageSetup.Orientation = Orientation.Landscape;

            var table = document.LastSection.AddTable();
            table.Borders.Visible = false;
            table.TopPadding = 1;
            table.BottomPadding = 1;
            table.Format.Font.Size = 10;          
            table.Format.Font.Bold = true;  
           
            MigraDoc.DocumentObjectModel.Tables.Column column;


            #region Add Column 
            column = table.AddColumn(Unit.FromCentimeter(1)); //No
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn(Unit.FromCentimeter(4)); //Business Unit
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn(Unit.FromCentimeter(4)); //Tax Invoice or Tax Credit Note 2
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn(Unit.FromCentimeter(4)); //Tax Invoice or Tax Credit Note
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn(Unit.FromCentimeter(4)); //Customer, 4
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn(Unit.FromCentimeter(6)); //Customer
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn(Unit.FromCentimeter(3)); //Frame No
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn(Unit.FromCentimeter(3)); //Engine No
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn(Unit.FromCentimeter(4)); //Merk/Type
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn(Unit.FromCentimeter(2)); //Model
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn(Unit.FromCentimeter(2)); //Year
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn(Unit.FromCentimeter(3.5)); //Vehicle Selling Price, 11
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn(Unit.FromCentimeter(3.5)); //Vehicle Selling Price
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn(Unit.FromCentimeter(3.5)); //Vehicle Selling Price
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn(Unit.FromCentimeter(3.5)); //Previous Luxury Tax Amount
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn(Unit.FromCentimeter(4)); //Remarks
            column.Format.Alignment = ParagraphAlignment.Center;

            #endregion

            #region Header 
            var row = table.AddRow();
            row.Borders.Color = Colors.Transparent;
            row.Height = 20;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].MergeRight = 15;
            row.Cells[0].VerticalAlignment = MigraDoc.DocumentObjectModel.Tables.VerticalAlignment.Center;
            row.Cells[0].AddParagraph("DETAILED LIST OF MOTOR VEHICLES");

            row = table.AddRow();
            string[] FilterDateHeader = datadrkb[0];
            row.Borders.Color = Colors.Transparent;           
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = true;
            row.Cells[0].MergeRight = 15;
            row.Cells[0].AddParagraph(FilterDateHeader[0]);

            row = table.AddRow();
            row.Borders.Color = Colors.Transparent;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = true;
            row.Cells[0].AddParagraph();

            row = table.AddRow();
            string[] Title2 = datadrkb[1];          
            row.Borders.Color = Colors.Transparent;
            row.Height = 20;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.Format.Font.Bold = true;
            row.Cells[0].MergeRight = 10;
            row.Cells[0].AddParagraph(Title2[0]);

            string[] Nama = datadrkb[2];
            row = table.AddRow();           
            row.Borders.Color = Colors.Transparent;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.Cells[0].MergeRight = 1;
            row.Cells[0].AddParagraph(Nama[0]);
            row.Cells[2].MergeRight = 10;
            row.Cells[2].AddParagraph(Nama[1]);

            string[] Address = datadrkb[3];
            row = table.AddRow();          
            row.Borders.Color = Colors.Transparent;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.Cells[0].MergeRight = 1;
            row.Cells[0].AddParagraph(Address[0]);
            row.Cells[2].MergeRight = 10;
            row.Cells[2].AddParagraph(Address[1]);

            string[] NPWP = datadrkb[4];
            row = table.AddRow();         
            row.Format.Alignment = ParagraphAlignment.Left;
            row.Borders.Color = Colors.Transparent;
            row.Cells[0].MergeRight = 1;
            row.Cells[0].AddParagraph(NPWP[0]);
            row.Cells[2].MergeRight = 10;
            row.Cells[2].AddParagraph(NPWP[1]);

            string[] NoPKP = datadrkb[5];
            row = table.AddRow();
            row.Height = 20;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.Borders.Color = Colors.Transparent;
            row.Borders.Bottom.Color = Colors.Black;
            row.Cells[0].MergeRight = 1;
            row.Cells[0].AddParagraph(NoPKP[0]);
            row.Cells[2].MergeRight = 10;
            row.Cells[2].AddParagraph(NoPKP[1]);


            #endregion


            #region Grid Table

            row = table.AddRow();
            row.Borders.Visible = true;
            row.Shading.Color = Color.FromArgb(255, 28, 73, 125);
            row.VerticalAlignment = MigraDoc.DocumentObjectModel.Tables.VerticalAlignment.Center;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Color = Colors.White;           
            for (int i = 9; i < 10; i++)
            {
                string[] columnheader1 = datadrkb[i];
                for (int j = 0; j < columnheader1.Count(); j++)
                {
                    if (j == 2 || j == 4)
                    {
                        row.Cells[j].MergeRight = 1;
                    }
                    else if (j == 11)
                    {
                        row.Cells[j].MergeRight = 2;
                    }
                    else
                    {
                        row.Cells[j].MergeDown = 1;
                    }

                    row.Cells[j].AddParagraph(columnheader1[j]);
                }

            }
            row = table.AddRow();
            row.Borders.Visible = true;
            row.Shading.Color = Color.FromArgb(255, 28, 73, 125);
            row.VerticalAlignment = MigraDoc.DocumentObjectModel.Tables.VerticalAlignment.Center;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Color = Colors.White;        
            for (int i = 10; i < 11; i++)
            {
                string[] columnheader1 = datadrkb[i];
                for (int j = 0; j < columnheader1.Count(); j++)
                {                   
                    row.Cells[j].AddParagraph(columnheader1[j]);
                }

            }
            row = table.AddRow();
            row.Borders.Visible = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Shading.Color = Color.FromArgb(255, 253, 233, 217);
            for (int i = 9; i < 10; i++)
            {
                string[] columnheader2 = datadrkb[i];
                for (int j = 0; j < columnheader2.Count(); j++)
                {
                    row.Cells[j].AddParagraph("(" + (j + 1).ToString() + ")");
                }
            }


            int number = 0;
            for (int i = 11; i < datadrkb.Count(); i++)
            {
                row = table.AddRow();
                row.Borders.Visible = true;
                string[] datacolumn = datadrkb[i];
                number++;
                for (int a = 0; a < datacolumn.Count(); a++)
                {

                    if (a == 0)
                    {
                        row.Cells[a].AddParagraph(number.ToString());
                        row.Cells[a].AddParagraph(datacolumn[a]).Format.Alignment = ParagraphAlignment.Center;
                    }
                    else if (a == 11 || a == 12 || a == 13 || a == 14)
                    {
                        string d = "0.0";
                        if (datacolumn[a] == null)
                        {
                            d = "0.0";

                        }
                        else
                        {
                            d = datacolumn[a];
                        }

                        row.Cells[a].AddParagraph(d.ToString()).Format.Alignment = ParagraphAlignment.Right;

                    }
                    else if (a == 1 || a == 2 || a == 3 || a == 4 || a == 5 || a == 6 || a == 7 || a == 8 || a==9|| a==10 || a==15)
                    {
                        var d = "";
                        if (datacolumn[a] == null)
                        {
                            d = "";
                            row.Cells[a].AddParagraph(d.ToString()).Format.Alignment = ParagraphAlignment.Left;
                        }
                        else
                        {
                            row.Cells[a].AddParagraph(datacolumn[a]).Format.Alignment = ParagraphAlignment.Left;
                        }
                       

                    }
                    else
                    {
                        row.Cells[a].AddParagraph(datacolumn[a]).Format.Alignment = ParagraphAlignment.Left;             
                    }
                }
            }

            #endregion


            #region TTD

            string[] Lokasi = datadrkb[6];
            row = table.AddRow();
            row.Height = 40;
            row.Borders.Bottom.Color = Colors.Transparent;
            row.Borders.Right.Color = Colors.Transparent;
            row.Borders.Left.Color = Colors.Transparent;
            row.Cells[0].MergeRight = 10;
            row.Cells[11].VerticalAlignment = MigraDoc.DocumentObjectModel.Tables.VerticalAlignment.Bottom;
            row.Cells[11].MergeRight = 1;
            row.Cells[11].AddParagraph(Lokasi[0] + ", ___________________________").Format.Alignment = ParagraphAlignment.Left;

            string[] NamaTTD = datadrkb[7];
            string[] JbtTTD = datadrkb[8];
            row = table.AddRow();
            row.Height = 60;
            row.Borders.Color = Colors.Transparent;
            row.Cells[0].MergeRight = 10;
            row.Cells[11].MergeRight = 1;
            row.Cells[11].VerticalAlignment = MigraDoc.DocumentObjectModel.Tables.VerticalAlignment.Bottom;
            row.Cells[11].AddParagraph(NamaTTD[0]).Format.Alignment = ParagraphAlignment.Center;
            row.Cells[11].AddParagraph("_____________________________________________").Format.Alignment = ParagraphAlignment.Left;
            row.Cells[11].AddParagraph(JbtTTD[0]).Format.Alignment = ParagraphAlignment.Center;


            #endregion 

        }

        #endregion

        #region PDF Report DRKB 
        public static string PutExcelDRKBReport(List<string[]> ListArr, object o, string TempDownloadFolder, string XlsName = null, bool ForceName = false, bool isPDF = false)
        {
            var document = CreateDocumentDRKBReport(ListArr);

            var ddl = MigraDoc.DocumentObjectModel.IO.DdlWriter.WriteToString(document);

            var renderer = new PdfDocumentRenderer(true);
            renderer.Document = document;

            renderer.RenderDocument();

            // Save the document...      
            string fileName = XlsName + " Report";
            string fullPath = Path.Combine(@TempDownloadFolder, fileName + ".pdf");

            renderer.PdfDocument.Save(fullPath);
            return fullPath;
        }

        public static Document CreateDocumentDRKBReport(List<string[]> datadrkb)
        {
            // Create a new MigraDoc document.
            var document = new Document();

            //set document orientation
            PageSetup pageSetup = document.DefaultPageSetup.Clone();

            DefineStylesDRKB(document);

            DefineContentSectionDRKB(document);

            CreatePDFDocumentDRKBReport(document, datadrkb);

            return document;

        }
      
        public static void CreatePDFDocumentDRKBReport(Document document, List<string[]> datadrkb)
        {
            document.LastSection.PageSetup.Orientation = Orientation.Landscape;

            var table = document.LastSection.AddTable();
            table.Borders.Visible = false;
            table.TopPadding = 1;
            table.BottomPadding = 1;
            table.Format.Font.Size = 10;
            table.Format.Font.Bold = true;

            MigraDoc.DocumentObjectModel.Tables.Column column;


            #region Add Column            

            column = table.AddColumn(Unit.FromCentimeter(4)); //Business Unit
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn(Unit.FromCentimeter(4)); //Tax Invoice or Tax Credit Note 2
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn(Unit.FromCentimeter(4)); //Tax Invoice or Tax Credit Note
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn(Unit.FromCentimeter(4)); //Customer, 4
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn(Unit.FromCentimeter(6)); //Customer
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn(Unit.FromCentimeter(3)); //Frame No
            column.Format.Alignment = ParagraphAlignment.Center;
          
            column = table.AddColumn(Unit.FromCentimeter(3)); //Merk/Type
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn(Unit.FromCentimeter(4)); //Model
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn(Unit.FromCentimeter(3)); //Year
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn(Unit.FromCentimeter(4)); //Vehicle Selling Price, 11
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn(Unit.FromCentimeter(3.5)); //Vehicle Selling Price
            column.Format.Alignment = ParagraphAlignment.Center;
           

            column = table.AddColumn(Unit.FromCentimeter(3.5)); //Previous Luxury Tax Amount
            column.Format.Alignment = ParagraphAlignment.Center;

            column = table.AddColumn(Unit.FromCentimeter(4)); //Remarks
            column.Format.Alignment = ParagraphAlignment.Center;

            #endregion

            #region Header 
            var row = table.AddRow();
            row.Borders.Color = Colors.Transparent;
            row.Height = 20;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].MergeRight = 12;
            row.Cells[0].VerticalAlignment = MigraDoc.DocumentObjectModel.Tables.VerticalAlignment.Center;
            row.Cells[0].AddParagraph("DETAILED LIST OF MOTOR VEHICLES");

            row = table.AddRow();
            string[] FilterDateHeader = datadrkb[0];
            row.Borders.Color = Colors.Transparent;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = true;
            row.Cells[0].MergeRight = 12;
            row.Cells[0].AddParagraph(FilterDateHeader[0]);


            row = table.AddRow();
            row.Borders.Color = Colors.Transparent;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = true;
            row.Cells[0].AddParagraph();


            row = table.AddRow();
            string[] Title2 = datadrkb[1];
            row.Borders.Color = Colors.Transparent;
            row.Height = 20;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.Format.Font.Bold = true;
            row.Cells[0].MergeRight = 10;
            row.Cells[0].AddParagraph(Title2[0]);

            string[] Nama = datadrkb[2];
            row = table.AddRow();
            row.Borders.Color = Colors.Transparent;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.Cells[0].MergeRight = 1;
            row.Cells[0].AddParagraph(Nama[0]);
            row.Cells[2].MergeRight = 10;
            row.Cells[2].AddParagraph(Nama[1]);

            string[] Address = datadrkb[3];
            row = table.AddRow();
            row.Borders.Color = Colors.Transparent;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.Cells[0].MergeRight = 1;
            row.Cells[0].AddParagraph(Address[0]);
            row.Cells[2].MergeRight = 10;
            row.Cells[2].AddParagraph(Address[1]);

            string[] NPWP = datadrkb[4];
            row = table.AddRow();
            row.Format.Alignment = ParagraphAlignment.Left;
            row.Borders.Color = Colors.Transparent;
            row.Cells[0].MergeRight = 1;
            row.Cells[0].AddParagraph(NPWP[0]);
            row.Cells[2].MergeRight = 10;
            row.Cells[2].AddParagraph(NPWP[1]);

            string[] NoPKP = datadrkb[5];
            row = table.AddRow();
            row.Height = 20;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.Borders.Color = Colors.Transparent;
            row.Borders.Bottom.Color = Colors.Black;
            row.Cells[0].MergeRight = 1;
            row.Cells[0].AddParagraph(NoPKP[0]);
            row.Cells[2].MergeRight = 10;
            row.Cells[2].AddParagraph(NoPKP[1]);


            #endregion


            #region Grid Table

            row = table.AddRow();
            row.Borders.Visible = true;
            row.Shading.Color = Color.FromArgb(255, 28, 73, 125);
            row.VerticalAlignment = MigraDoc.DocumentObjectModel.Tables.VerticalAlignment.Center;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Color = Colors.White;
            for (int i = 9; i < 10; i++)
            {
                string[] columnheader1 = datadrkb[i];
                for (int j = 0; j < columnheader1.Count(); j++)
                {
                    if (j == 1 || j == 3)
                    {
                        row.Cells[j].MergeRight = 1;
                    }
                    else if (j == 9)
                    {
                        row.Cells[j].MergeRight = 1;
                    }
                    else
                    {
                        row.Cells[j].MergeDown = 1;
                    }

                    row.Cells[j].AddParagraph(columnheader1[j]);
                }

            }
            row = table.AddRow();
            row.Borders.Visible = true;
            row.Shading.Color = Color.FromArgb(255, 28, 73, 125);
            row.VerticalAlignment = MigraDoc.DocumentObjectModel.Tables.VerticalAlignment.Center;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Color = Colors.White;
            for (int i = 10; i < 11; i++)
            {
                string[] columnheader1 = datadrkb[i];
                for (int j = 0; j < columnheader1.Count(); j++)
                {
                    row.Cells[j].AddParagraph(columnheader1[j]);
                }

            }

            row = table.AddRow();
            row.Borders.Visible = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Shading.Color = Color.FromArgb(255, 253, 233, 217);           
            for (int i = 9; i < 10; i++)
            {
                string[] columnheader2 = datadrkb[i];
                for (int j = 0; j < columnheader2.Count(); j++)
                {
                    row.Cells[j].AddParagraph("(" + (j + 1).ToString() + ")");
                }
            }
                


            for (int i = 11; i < datadrkb.Count(); i++)
            {
                row = table.AddRow();
                row.Borders.Visible = true;
                string[] datacolumn = datadrkb[i];
                for (int a = 0; a < datacolumn.Count(); a++)
                {

                    if (a == 9 || a == 10 || a == 11)
                    {
                        string d = "0.0";
                        if (datacolumn[a] == null)
                        {
                            d = "0.0";

                        }
                        else
                        {
                            d = datacolumn[a];
                        }

                        row.Cells[a].AddParagraph(d.ToString()).Format.Alignment = ParagraphAlignment.Right;

                    }
                    else if (a == 1 || a == 2 || a == 3 || a == 4 || a == 5 || a == 6 || a == 7 || a == 8 )
                    {
                        var d = "";
                        if (datacolumn[a] == null)
                        {
                            d = "";
                            row.Cells[a].AddParagraph(d.ToString()).Format.Alignment = ParagraphAlignment.Left;
                        }
                        else
                        {
                            row.Cells[a].AddParagraph(datacolumn[a]).Format.Alignment = ParagraphAlignment.Left;
                        }
                      

                    }
                    else
                    {
                        row.Cells[a].AddParagraph(datacolumn[a]).Format.Alignment = ParagraphAlignment.Left;
                    }
                }
            }

            #endregion


            #region TTD

            string[] Lokasi = datadrkb[6];
            row = table.AddRow();
            row.Height = 40;
            row.Borders.Bottom.Color = Colors.Transparent;
            row.Borders.Right.Color = Colors.Transparent;
            row.Borders.Left.Color = Colors.Transparent;
            row.Cells[0].MergeRight = 8;
            row.Cells[9].VerticalAlignment = MigraDoc.DocumentObjectModel.Tables.VerticalAlignment.Bottom;
            row.Cells[9].MergeRight = 1;
            row.Cells[9].AddParagraph(Lokasi[0] + ", ___________________________").Format.Alignment = ParagraphAlignment.Left;

            string[] NamaTTD = datadrkb[7];
            string[] JbtTTD = datadrkb[8];
            row = table.AddRow();
            row.Height = 60;
            row.Borders.Color = Colors.Transparent;
            row.Cells[0].MergeRight = 8;
            row.Cells[9].MergeRight = 1;
            row.Cells[9].VerticalAlignment = MigraDoc.DocumentObjectModel.Tables.VerticalAlignment.Bottom;
            row.Cells[9].AddParagraph(NamaTTD[0]).Format.Alignment = ParagraphAlignment.Center;
            row.Cells[9].AddParagraph("_____________________________________________").Format.Alignment = ParagraphAlignment.Left;
            row.Cells[9].AddParagraph(JbtTTD[0]).Format.Alignment = ParagraphAlignment.Center;


            #endregion 

        }

        #endregion

        public static string GetConfigValueByConfigKey(string ConfigKey)
        {
            string Value = "";

            Connection cn = new Connection();
            cn.CreateConnection();
            cn.OpenConnection();

            cn._sqlCommand.CommandText = "efb_sp_GetConfigValueByConfigKey";
            cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            cn._sqlCommand.Parameters.AddWithValue("@ConfigKey", ConfigKey);
            cn._sqlCommand.CommandTimeout = 0;

            SqlDataReader rd = cn._sqlCommand.ExecuteReader();
            if (rd.Read())
            {
                Value = rd[0].ToString(); ;
            }

            cn.CloseConnection();
            cn.DisposeConnection();

            return Value;
        }

    }
}
