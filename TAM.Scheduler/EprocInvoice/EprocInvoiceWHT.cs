﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TAM.Scheduler.OCR;

namespace TAM.Scheduler.EprocInvoice
{
    class EprocInvoiceWHT
    {
        public static Guid SyncID { get; set; }

        public static void saveDataEprocWht(WHTManualInputViewEprocModel dataModal, Guid SyncID)
        {
            Connection cn = new Connection();
            cn.CreateConnection();
            cn.OpenConnection();

            cn._sqlCommand.CommandText = "usp_InsertCustom_TB_R_WHT_Eproc";
            cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            cn._sqlCommand.Parameters.AddWithValue("@Id", dataModal.Id);
            cn._sqlCommand.Parameters.AddWithValue("@TransactionId", dataModal.TransactionId);
           
            cn._sqlCommand.ExecuteReader();
            cn.CloseConnection();
            cn.DisposeConnection();
        }

        public static int EprocInvoiceDataWHTPaging(int pageNumber, int pageSize)
        {
            int cdata = 0;

            try
            {
                Connection cn = new Connection();
                cn.CreateConnection();
                cn.OpenConnection();

                cn._sqlCommand.CommandText = "usp_GetEprocInvoiceWHT";
                cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                cn._sqlCommand.Parameters.AddWithValue("@PageNumber", pageNumber);
                cn._sqlCommand.Parameters.AddWithValue("@PageSize", pageSize);
                SqlDataReader rd = cn._sqlCommand.ExecuteReader();
                while (rd.Read())
                {

                    try
                    {
                        cdata = Convert.ToInt32(rd["c"].ToString());
                    }
                    catch (Exception ex)
                    {
                        SyncLog.InsertLog("", "CountData1", "EPROC_WHT_ERR", ex.Message);
                    }
                }
                cn.CloseConnection();
                cn.DisposeConnection();

                return cdata;
            }
            catch (Exception ex)
            {
                SyncLog.InsertLog("", "CountData2", "EPROC_VATIN_ERR", ex.Message);
                return 0;
            }
        }

        public static void EprocInvoiceDataWHT()
        {

           

                 int whttotal = 0;
            whttotal = EprocInvoiceDataWHTPaging(1, 1);
            int _pageNumber = 1;
            int _pageSize = 10;
            int _page = 0;
            int _pagemid = 0;
            _page = whttotal / _pageSize;
            _pagemid = (whttotal % _pageSize) > 0 ? 1 : 0;
            _pageNumber = _page + _pagemid;

            for (int i = 1; i <= _pageNumber; i++)
            {
                string URL = "";
                string TransactionId = "";

                try
                {
                    Connection cn = new Connection();
                    cn.CreateConnection();
                    cn.OpenConnection();

                    cn._sqlCommand.CommandText = "usp_GetEprocInvoiceWHT";
                    cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                   
                    cn._sqlCommand.Parameters.AddWithValue("@PageNumber", 1);
                    cn._sqlCommand.Parameters.AddWithValue("@PageSize", _pageSize);

                    SqlDataReader rd = cn._sqlCommand.ExecuteReader();
                    while (rd.Read())
                    {
                        WHTManualInputViewEprocModel model = new WHTManualInputViewEprocModel();
                        TransactionId = rd["TransactionId"].ToString();
                        SyncID = Guid.NewGuid();

                        model.Id = Guid.NewGuid();
                        model.TransactionId = new Guid(TransactionId);
                        try
                        {
                            InsertEprocWHT(model, TransactionId, SyncID);
                        }
                        catch (Exception ex)
                        {
                            SyncLog.InsertLog(URL, TransactionId, "EPROC_WHT_ERR", ex.Message);
                            SyncLog.InsertLogEprocInvoice(TransactionId, ex.Message);

                        }
                    }
                    cn.CloseConnection();
                    cn.DisposeConnection();
                }
                catch (Exception ex)
                {
                    SyncLog.InsertLog(URL, TransactionId, "EPROC_WHT_ERR", ex.Message);
                    SyncLog.InsertLogEprocInvoice(TransactionId, ex.Message);

                }
            }
        }

        public static void InsertEprocWHT(WHTManualInputViewEprocModel dataModal, string id, Guid SyncID)
        {

            string transid =Convert.ToString(dataModal.TransactionId);

            try
            {
                //saveDataEprocWht(dataModal, SyncID);

                Connection cn = new Connection();
                cn.CreateConnection();
                cn.OpenConnection();

                cn._sqlCommand.CommandText = "usp_InsertCustom_TB_R_WHT_Eproc";
                cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                cn._sqlCommand.Parameters.AddWithValue("@Id", dataModal.Id);
                cn._sqlCommand.Parameters.AddWithValue("@TransactionId", dataModal.TransactionId);

                cn._sqlCommand.ExecuteReader();
                cn.CloseConnection();
                cn.DisposeConnection();

                SyncLog.InsertLog("", transid, "EPROC_WHT_INF", "Insert EPROC Success");

                //try
                //{
                //   // Syncdata.InsertSync("", SyncID);
                //    SyncLog.InsertLog("", id, "EPROC_WHT_INF", "Insert SyncData Success");
                //}
                //catch (Exception ex)
                //{
                //    SyncLog.InsertLog("", id, "EPROC_WHT_ERR", ex.Message);
                //    SyncLog.InsertLogEprocInvoice(id, ex.Message);
                //}
            }
            catch (Exception ex)
            {
                SyncLog.InsertLog("", transid, "EPROC_WHT_ERR", ex.Message);
                SyncLog.InsertLogEprocInvoice(transid, ex.Message);
            }
        }
    }
}

