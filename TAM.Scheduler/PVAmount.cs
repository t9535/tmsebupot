﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TAM.Scheduler
{
    public partial class PVAmount
    {
        public int? DOC_NO { get; set; }
        public int? DOC_YEAR { get; set; }
        public string CURRENCY_CD { get; set; }
        public decimal TOTAL_AMOUNT { get; set; }
        public decimal EXCHANGE_RATE { get; set; }
    }
}
