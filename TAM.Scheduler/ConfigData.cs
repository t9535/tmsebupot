﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TAM.Scheduler
{
    public class ConfigData
    {
        public string ConfigKey { get; set; }
        public string ConfigValue { get; set; }
    }

    public class Result
    {
        public bool ResultCode { get; set; }
        public string ResultDesc { get; set; }
        public string ResultDescs { get; set; }
    }
}
