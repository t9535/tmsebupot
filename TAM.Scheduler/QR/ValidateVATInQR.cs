﻿using System;
using System.Data.SqlClient;
using TAM.EFaktur.Web.Models.VATIn;

namespace TAM.Scheduler.QR
{
    class ValidateVATInQR
    {
        public static string ValidateExpireDate(string invoicedate)
        {
            string InvoiceDate = invoicedate;
            string errmsg = "";

            DateTime expdate = Convert.ToDateTime(InvoiceDate).AddDays(1).AddMonths(3).AddDays(-1);//
            DateTime endOfMonth = new DateTime(expdate.Year, expdate.Month, 1).AddMonths(1).AddDays(-1);
            string tanggalexpired = endOfMonth.ToShortDateString();
            int SelisihHari = (Convert.ToDateTime(tanggalexpired) - DateTime.Now.Date.AddDays(-1)).Days;
            int NilaiSelisihHari = CheckExpiredDate("VATInDaysExpired");

            if (SelisihHari < NilaiSelisihHari)
            {
                if (SelisihHari <= 0)
                {
                    SelisihHari = 0;
                }
                errmsg = "Tax Invoice can’t be saved. Tax Invoice EXPIRED is Already " + SelisihHari + " Days Before Expired.";
            }
            else
            {
                errmsg = "";
            }
            return errmsg;
        }

        private static int CheckExpiredDate(string configValue)
        {
            Connection cn = new Connection();
            cn.CreateConnection();
            cn.OpenConnection();

            cn._sqlCommand.CommandText = "usp_GetConfigurationDataByConfigKey";
            cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            cn._sqlCommand.Parameters.AddWithValue("@ConfigKey", configValue);

            int result = 0;

            SqlDataReader rd = cn._sqlCommand.ExecuteReader();
            if (rd.HasRows)
            {
                while (rd.Read())
                {
                    result = Convert.ToInt32(rd[2].ToString());
                }
            }
            rd.Close();

            cn.CloseConnection();
            cn.DisposeConnection();

            return result;
        }

        public static string ValidateVATIn(string nomorfakturgab,string nomorfaktur,string fgpengganti, string typeFaktur, string NPWPPenjual, string invoiceDate)
        {
            string errmsg = "", errmsgcode = "";

            Connection cn = new Connection();
            cn.CreateConnection();
            cn.OpenConnection();

            cn._sqlCommand.CommandText = "usp_ValidateVATInInvoiceNumber";
            cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            cn._sqlCommand.Parameters.AddWithValue("@NomorFakturGabungan",nomorfakturgab);
            cn._sqlCommand.Parameters.AddWithValue("@NomorFaktur", nomorfaktur);
            cn._sqlCommand.Parameters.AddWithValue("@FGPengganti", fgpengganti);
            cn._sqlCommand.Parameters.AddWithValue("@FakturType", typeFaktur);
            cn._sqlCommand.Parameters.AddWithValue("@NPWPPenjual", NPWPPenjual);
            cn._sqlCommand.Parameters.AddWithValue("@InvoiceDate", invoiceDate);

            SqlDataReader rd = cn._sqlCommand.ExecuteReader();
            if (rd.HasRows)
            {
                while (rd.Read())
                {
                    errmsgcode = rd[0].ToString();
                    errmsg = rd[1].ToString();
                }
            }
            rd.Close();

            cn.CloseConnection();
            cn.DisposeConnection();

            if (errmsgcode == "True")
                return errmsg = "";
            else
                return errmsg;
        }
        public static string ValidateNPWP(string npwp)
        {
            string npwpJakarta = CheckNPWP("CompanyNPWP");
            string npwpBatam = CheckNPWP("CompanyNPWPBatam");
            string errmsg = "";

            if (npwp == npwpJakarta || npwp == npwpBatam)
                return errmsg;
            else
               return errmsg = "Buyer NPWP '" + npwp + "' does not match with CompanyNPWP '" + npwpJakarta + "'";
        }
        private static string CheckNPWP(string configValue)
        {
            Connection cn = new Connection();
            cn.CreateConnection();
            cn.OpenConnection();

            cn._sqlCommand.CommandText = "usp_GetConfigurationDataByConfigKey";
            cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            cn._sqlCommand.Parameters.AddWithValue("@ConfigKey", configValue);

            string result = "";

            SqlDataReader rd = cn._sqlCommand.ExecuteReader();
            if (rd.HasRows)
            {
                while (rd.Read())
                {
                    result = rd[2].ToString();
                }
            }
            rd.Close();

            cn.CloseConnection();
            cn.DisposeConnection();

            return result;
        }
    }
}
