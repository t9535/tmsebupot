﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TAM.Scheduler
{
    class URLlog
    {
        public static void InsertLog(string message)
        {
            Connection cn = new Connection();
            cn.CreateConnection();
            cn.OpenConnection();

            cn._sqlCommand.CommandText = "usp_insertURLlog";
            cn._sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            cn._sqlCommand.Parameters.AddWithValue("@Message", message);

            cn._sqlCommand.ExecuteReader();
            cn.CloseConnection();
            cn.DisposeConnection();
        }
    }
}
