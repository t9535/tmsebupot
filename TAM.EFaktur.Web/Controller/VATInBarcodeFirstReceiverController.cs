﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using TAM.EFaktur.Web.Models;
using TAM.EFaktur.Web.Models.Master_Config;
using TAM.EFaktur.Web.Models.VATIn;
using Toyota.Common.Web.Platform;

namespace TAM.EFaktur.Web.Controllers
{
    public class VATInBarcodeFirstReceiverController : BaseController
    {
        //
        // GET: /VATInBarcodeFirstReceiver/
        public VATInBarcodeFirstReceiverController()
        {
            
            Settings.Title = "Input VAT-In By Scan";
            
        }

        [HttpPost]
        public JsonResult ValidateInvoiceDate(FormCollection collection, string serviceURL)
        {
            var data = collection["data"];
            Result result = new Result();
            #region Load XML Model from JSON
            var VATInXML = new JavaScriptSerializer().Deserialize<resValidateFakturPm>(data);
            #endregion
            VATInManualInputViewModel model = new VATInManualInputViewModel();
            #region Map XML Model to ViewModel
            //Split to get variable date now
            String sDate = DateTime.Now.ToString();
            DateTime datevalue = (Convert.ToDateTime(sDate.ToString()));
            String mn = datevalue.Month.ToString();
            String yy = datevalue.Year.ToString();
            int MonthNow = Convert.ToInt32(mn);
            int YearNow = Convert.ToInt32(yy);

            string InvoiceDate = VATInXML.tanggalFakturDateFormat.FormatSQLDate();
            string[] TanggalFaktur = InvoiceDate.Split('-');
            string MonthFaktur = TanggalFaktur[1].ToString();
            string YearFaktur = TanggalFaktur[0].ToString();
            int BulanFaktur = Convert.ToInt32(MonthFaktur);
            int TahunFaktur = Convert.ToInt32(YearFaktur);


            int SelisihTahun = YearNow - TahunFaktur;
            int SelisihBulan = ((SelisihTahun * 12) + MonthNow) - BulanFaktur;

            if (SelisihBulan > 3)
            {
                result.ResultCode = true;
                result.ResultDesc = "Tax Invoice can’t be saved. Tax Invoice has been EXPIRED: Invoice Date is already " + SelisihBulan + " months ago.";
            }
            #endregion
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Save(FormCollection collection, string serviceURL)
        {
            var data = collection["data"];

            Result result = new Result();
            if (!string.IsNullOrEmpty(data))
            {
                try
                {
                    #region Load XML Model from JSON
                    var VATInXML = new JavaScriptSerializer().Deserialize<resValidateFakturPm>(data);
                    #endregion

                    #region Map XML Model to ViewModel
                    VATInManualInputViewModel model = new VATInManualInputViewModel();

                    model.InvoiceNumber = VATInXML.nomorFaktur;
                    model.InvoiceNumberFull = VATInXML.nomorFaktur.FormatNomorFakturGabungan(VATInXML.kdJenisTransaksi, VATInXML.fgPengganti);
                    model.KDJenisTransaksi = VATInXML.kdJenisTransaksi;
                    model.FGPengganti = VATInXML.fgPengganti;
                    model.InvoiceDate = VATInXML.tanggalFakturDateFormat.FormatSQLDate();
                    model.ExpireDate = Convert.ToDateTime(model.InvoiceDate).AddMonths(3).FormatSQLDate();
                    model.SupplierNPWP = VATInXML.npwpPenjual.FormatNPWP();
                    model.SupplierName = VATInXML.namaPenjual;
                    model.SupplierAddress = VATInXML.alamatPenjual;
                    model.NPWPLawanTransaksi = VATInXML.npwpLawanTransaksi.FormatNPWP();
                    model.NamaLawanTransaksi = VATInXML.namaLawanTransaksi;
                    model.AlamatLawanTransaksi = VATInXML.alamatLawanTransaksi;
                    model.StatusApprovalXML = VATInXML.statusApproval;
                    model.StatusFakturXML = VATInXML.statusFaktur;
                    model.VATBaseAmount = VATInXML.jumlahDpp;
                    model.VATAmount = VATInXML.jumlahPpn;
                    model.JumlahPPnBM = VATInXML.jumlahPpnBm;
                    model.VATInDetails = new List<VATInDetailManualInputViewModel>();

                    foreach (resValidateFakturPmDetailTransaksi detail in VATInXML.detailTransaksi)
                    {
                        VATInDetailManualInputViewModel detailModel = new VATInDetailManualInputViewModel();

                        detailModel.UnitName = detail.nama;
                        detailModel.UnitPrice = detail.hargaSatuan;
                        detailModel.Quantity = detail.jumlahBarang;
                        detailModel.TotalPrice = detail.hargaTotal;
                        detailModel.Discount = detail.diskon;
                        detailModel.DPP = detail.dpp;
                        detailModel.PPN = detail.ppn;
                        detailModel.PPNBM = detail.ppnbm;
                        detailModel.TarifPPNBM = detail.tarifPpnbm;

                        model.VATInDetails.Add(detailModel);
                    }
                    #endregion

                    result = VATInRepository.Instance.Insert(model, serviceURL, CurrentLogin.Username, DateTime.Now);
                    
                }
                catch (Exception e)
                {
                    e.LogToApp("VATIn Barcode First Receiver", MessageType.ERR, CurrentLogin.Username);
                    result.ResultCode = false;
                    result.ResultDesc = e.Message;
                }
            }
            else
            {
                result.ResultCode = false;
                result.ResultDesc = "Data not found or not valid";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //[HttpGet]
        //public JsonResult CekNPWPVal(FormCollection collection, string serviceURL)
        //{
        //    var data = collection["data"];
        //    Result result = new Result();
        //    if (!string.IsNullOrEmpty(data))
        //    {
               
        //            #region Load XML Model from JSON
        //            var VATInXML = new JavaScriptSerializer().Deserialize<resValidateFakturPm>(data);
        //            #endregion

        //            #region Map XML Model to ViewModel
        //            VATInManualInputViewModel model = new VATInManualInputViewModel();

        //            model.InvoiceNumber = VATInXML.nomorFaktur;
        //            model.InvoiceNumberFull = VATInXML.nomorFaktur.FormatNomorFakturGabungan(VATInXML.kdJenisTransaksi, VATInXML.fgPengganti);
        //            model.KDJenisTransaksi = VATInXML.kdJenisTransaksi;
        //            model.FGPengganti = VATInXML.fgPengganti;
        //            model.InvoiceDate = VATInXML.tanggalFakturDateFormat.FormatSQLDate();
        //            model.SupplierNPWP = VATInXML.npwpPenjual.FormatNPWP();
        //            model.SupplierName = VATInXML.namaPenjual;
        //            model.SupplierAddress = VATInXML.alamatPenjual;
        //            model.NPWPLawanTransaksi = VATInXML.npwpLawanTransaksi.FormatNPWP();
        //            model.NamaLawanTransaksi = VATInXML.namaLawanTransaksi;
        //            model.AlamatLawanTransaksi = VATInXML.alamatLawanTransaksi;
        //            model.StatusApprovalXML = VATInXML.statusApproval;
        //            model.StatusFakturXML = VATInXML.statusFaktur;
        //            model.VATBaseAmount = VATInXML.jumlahDpp;
        //            model.VATAmount = VATInXML.jumlahPpn;
        //            model.JumlahPPnBM = VATInXML.jumlahPpnBm;
        //            model.VATInDetails = new List<VATInDetailManualInputViewModel>();

        //            foreach (resValidateFakturPmDetailTransaksi detail in VATInXML.detailTransaksi)
        //            {
        //                VATInDetailManualInputViewModel detailModel = new VATInDetailManualInputViewModel();

        //                detailModel.UnitName = detail.nama;
        //                detailModel.UnitPrice = detail.hargaSatuan;
        //                detailModel.Quantity = detail.jumlahBarang;
        //                detailModel.TotalPrice = detail.hargaTotal;
        //                detailModel.Discount = detail.diskon;
        //                detailModel.DPP = detail.dpp;
        //                detailModel.PPN = detail.ppn;
        //                detailModel.PPNBM = detail.ppnbm;
        //                detailModel.TarifPPNBM = detail.tarifPpnbm;

        //                model.VATInDetails.Add(detailModel);
        //            }
        //            #endregion

        //            result = VATInRepository.Instance.CekNPWP(model);
        //    }
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}

        [HttpGet]
        public JsonResult GetXML(string serviceUrl = null)
        {
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = "Success",
                ResultDescs ="Success"
            };
            var response = new HttpResponseMessage();
            resValidateFakturPm VATInXML = new resValidateFakturPm();
            try
            {
                if (!string.IsNullOrEmpty(serviceUrl))
                {
                    using (HttpClient client = new HttpClient())
                    {
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml", 0.9));
                        Uri serviceUri = new Uri(serviceUrl, UriKind.Absolute);

                        XmlDocument doc = new XmlDocument();
                        response = client.GetAsync(serviceUri).Result;

                        var stream = new System.IO.MemoryStream(response.Content.ReadAsByteArrayAsync().Result);

                        var serializer = new XmlSerializer(typeof(resValidateFakturPm));
                        using (var reader = XmlReader.Create(stream))
                        {
                            VATInXML = (resValidateFakturPm)serializer.Deserialize(reader);
                            if (string.IsNullOrEmpty(VATInXML.nomorFaktur))
                            {
                                throw new Exception("Invalid URL, Invoice not found");
                            }
                        }
                        #region cekValidasi Tanggal dan NPWP
                        DateTime tglexpired = Convert.ToDateTime(VATInXML.tanggalFakturDateFormat.AddMonths(3).FormatSQLDate());
                        string InvoiceDate = VATInXML.tanggalFakturDateFormat.FormatSQLDate();
                        var config = MasterConfigRepository.Instance.GetByConfigKey("CompanyNPWP");
                        int Expdate = (Convert.ToDateTime(tglexpired).Date - DateTime.Today).Days;
                        
                        if (Expdate <= 0)
                        {
                            Expdate = 0;
                        }
                     

                        try
                        {
                          result.ResultCode = true;
                          result.ResultDesc = "Remaining Days Before Expired" + " " + Expdate + config.ConfigValue;
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                        #endregion
                    }
                }
            }
            catch (Exception e)
            {
                result.ResultCode = false;
                result.ResultDesc = e.LogToApp("VATIn Barcode First Receiver", MessageType.ERR, CurrentLogin.Username, serviceUrl).Message;
            }
            
            return Json(result.AsDynamic(VATInXML), JsonRequestBehavior.AllowGet);
        }
    }
}

