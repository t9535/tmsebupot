﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TAM.EFaktur.Web.Models.Category_Nominative
{
    public class CategoryNominativeRepository : BaseRepository
    {
        #region Singletonh
        private CategoryNominativeRepository() { }
        private static CategoryNominativeRepository instance = null;
        public static CategoryNominativeRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CategoryNominativeRepository();
                }
                return instance;
            }
        }
        #endregion

        #region Counter Data
        public int Count(CategoryNominativeDashboardSearchParamViewModel model)
        {
            dynamic args = new
            {
                CategoryCode = model.CategoryCode,
                NominativeType = model.NominativeType,
                JointGroup = model.JointGroup,
                GroupSeq = model.GroupSeq
            };

            return db.SingleOrDefault<int>("Category_Nominative/usp_CountCategoryNominativeListDashboard", args);
        }
        #endregion
        #region Processing Data

        public List<Guid> GetIdBySearchParam(CategoryNominativeSearchCategoryViewModel model)
        {
            IEnumerable<Guid> result = db.Query<Guid>("Category_Nominative/usp_GetCategoryNominativeIdBySearchParam", model.MapFromModel());
            return result.ToList();
        }

        public List<Category> GetXlsReportById(Category model)
        {
            dynamic args = new
            {
                //Id = model.Id,
                CategoryCode = model.CategoryCode,
                JointGroup = model.JointGroup,
                GroupSeq = model.GroupSeq,
                NominativeType = model.NominativeType,
                SortBy = 1,
                //SortBy = sortBy,
                SortDirection = "DESC"

            };
            IEnumerable<Category> result = db.Query<Category>("Category_Nominative/usp_GetCategoryReportById", args);
            return result.ToList();
        }

        public List<CategoryNominativeDashboardViewModel> GetList(CategoryNominativeDashboardSearchParamViewModel model, int SortBy, string SortDirection, int FromNumber, int ToNumber)
        {
            dynamic args = new
            {
                CategoryCode = model.CategoryCode,
                NominativeType = model.NominativeType,
                JointGroup = model.JointGroup,
                GroupSeq = model.GroupSeq,
                SortBy = SortBy,
                SortDirection = SortDirection,
                FromNumber = FromNumber,
                ToNumber = ToNumber
            };
            IEnumerable<CategoryNominativeDashboardViewModel> result = db.Query<CategoryNominativeDashboardViewModel>("Category_Nominative/usp_GetCategoryListDashboard", args);
            return result.ToList();
        }
        #endregion

        public Category GetById(string CategoryCode)
        {
            return db.SingleOrDefault<Category>("Category_Nominative/usp_GetCategoryNominativeByID", new { CategoryCode = CategoryCode });
        }
        public Category GetByIdDup(string CategoryCode)
        {
            return db.SingleOrDefault<Category>("Category_Nominative/usp_GetCategoryNominativeByID", new { CategoryCode = CategoryCode });
        }

        public Category GetByConfigKey(String ConfigKey)
        {
            return db.SingleOrDefault<Category>("Master_Config/usp_GetConfigurationDataByConfigKey", new { ConfigKey = ConfigKey });
        }

        //insert Data
        public Result Insert(CategoryNominativeCreateUpdate model, string EventActor, DateTime EventDate)
        {
            try
            {
                dynamic args = new
                {
                    //Id = Guid.NewGuid(),
                    CategoryCode = model.CategoryCode,
                    NominativeType = model.NominativeType,
                    JointGroup = model.JointGroup,
                    GroupSeq = model.GroupSeq,
                    PositionSignature = model.PositionSignature,
                    EventDate = EventDate.FormatSQLDateTime(),
                    EventActor = EventActor
                };
                db.Execute("Category_Nominative/usp_Insert_TB_M_CategoryNominative", args);
                return MsgResultSuccessInsert();
            }
            catch (Exception e)
            {
                return MsgResultError(e.Message.ToString());
            }
        }

        //Update Data
        public Result Update(CategoryNominativeCreateUpdate model, string EventActor, DateTime EventDate)
        {
            try
            {
                dynamic args = new
                {
                    //Id = model.Id,
                    CategoryCode = model.CategoryCode,
                    //NominativeType = model.NominativeType,
                    JointGroup = model.JointGroup,
                    GroupSeq = model.GroupSeq,
                    PositionSignature = model.PositionSignature,
                    EventDate = EventDate.FormatSQLDateTime(),
                    EventActor = EventActor,

                };
                db.Execute("Category_Nominative/usp_Update_TB_M_CategoryNominative_ENUM", args);
                return MsgResultSuccessUpdate();
            }
            catch (Exception e)
            {
                return MsgResultError(e.Message.ToString());
            }
        }

        //Delete Data
        public Result Delete(CategoryNominativeCreateUpdate model)
        {
            try
            {
                dynamic args = new
                {
                    CategoryCode = model.CategoryCode

                };
                db.Execute("Category_Nominative/usp_Delete_TB_M_CategoryNominative_ENUM", args);
                return MsgResultSuccessDelete();
            }
            catch (Exception e)
            {
                return MsgResultError(e.Message.ToString());
            }
        }
        //
        public Result Save(Category objConfig, DateTime EventDate, string EventActor)
        {
            try
            {
                dynamic args = new
                {
                    //Id = objConfig.Id,
                    CategoryCode = objConfig.CategoryCode,
                    NominativeType = objConfig.NominativeType,
                    JointGroup = objConfig.JointGroup,
                    GroupSeq = objConfig.GroupSeq,
                    RowStatus = objConfig.RowStatus,
                    EventDate = EventDate,
                    EventActor = EventActor
                };
                db.Execute("Config/usp_SaveConfig", args);
                return MsgResultSuccessInsert();
            }
            catch (Exception e)
            {
                return MsgResultError(e.Message.ToString());
            }
        }

        public Result Delete(Guid Id)
        {
            try
            {
                db.Execute("Config/usp_DeleteConfigByID", new { Id = Id });
                return MsgResultSuccessDelete();
            }
            catch (Exception e)
            {
                return MsgResultError(e.Message.ToString());
            }
        }


        public List<DropdownModel> GetCategoryNominativeDropdownList()
        {

            IEnumerable<DropdownModel> result = db.Query<DropdownModel>("Category_Nominative/usp_DropDownCategoryNominative");
            return result.ToList();
        }
        
        public List<DropdownModel> GetCategoryNominativeDropdownListGrouping()
        {

            IEnumerable<DropdownModel> result = db.Query<DropdownModel>("Category_Nominative/usp_DropDownCategorygrouping");
            return result.ToList();
        }
        public List<DropdownModel> GetCategoryCodeNominativeDDL()
        {

            IEnumerable<DropdownModel> result = db.Query<DropdownModel>("Category_Nominative/usp_DDLCategoryCode");
            return result.ToList();
        }
        //#endregion
        public List<CategoryNominativeCreateUpdate> CekDataDuplicatUpdate(string category_code_old)
        {
            dynamic args = new
            {
                category_code_old = category_code_old,
            };
            IEnumerable<CategoryNominativeCreateUpdate> result = db.Query<CategoryNominativeCreateUpdate>("Category_Nominative/Get_Usp_Duplicat_category_Enum", args);

            return result.ToList();
        }

    }
}
