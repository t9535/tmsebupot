﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.Category_Nominative
{
    public class CategoryNominativeCreateUpdate
    {
        public string CategoryCode { get; set; }
        public string CategoryCodeOld { get; set; }
        public string NominativeType { get; set; }
        public string JointGroup { get; set; }
        public int GroupSeq { get; set; }
        public Boolean PositionSignature { get; set; }
    }
}