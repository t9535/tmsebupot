﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TAM.EFaktur.Web.Models.Category_Nominative
{
    public class CategoryRepository : BaseRepository
    {
       #region Singleton
        private CategoryRepository() { }
        private static CategoryRepository instance = null;
        public static CategoryRepository Instance
        {
            get 
            {
                if (instance == null)
                {
                    instance = new CategoryRepository();
                }
                return instance;
            }
        }
        #endregion

        #region Counter Data
        public int Count(string ConfigName, string configValue)
        {
            dynamic args = new
            {
                ConfigName = ConfigName,
                configValue = configValue,
                
            };

            return db.SingleOrDefault<int>("Config/countApplications", args); 
        }
        #endregion

        #region Processing Data

        public List<Category> GetList(string ConfigKey,string ConfigValue)
        {
            dynamic args = new
            {
                ConfigKey = ConfigKey,
                ConfigValue = ConfigValue
               
            };
            IEnumerable<Category> result = db.Query<Category>("Config/nosqlyet", args);
            return result.ToList();
        }

        public Category GetById(Guid Id)
        {
            return db.SingleOrDefault<Category>("Config/usp_GetConfigByID", new { Id = Id });
        }

        public Result Save(Category objConfig, DateTime EventDate, string EventActor)
        {
            try
            {
                dynamic args = new
                {
                    //Id= objConfig.Id,
                    CategoryCode = objConfig.CategoryCode,
                    NominativeType = objConfig.NominativeType,
                    JointGroup = objConfig.JointGroup,
                    GroupSeq = objConfig.GroupSeq,
                    RowStatus           = objConfig.RowStatus,
                    EventDate           = EventDate,
                    EventActor          = EventActor                
                };
                db.Execute("Config/usp_SaveConfig", args);
                return MsgResultSuccessInsert();
            }
            catch (Exception e)
            {
                return MsgResultError(e.Message.ToString());
            }
        }

        public Result Delete(Guid Id)
        {
            try
            {
                db.Execute("Config/usp_DeleteConfigByID", new { Id = Id });
                return MsgResultSuccessDelete();
            }
            catch (Exception e)
            {
                return MsgResultError(e.Message.ToString());
            }
        }
        #endregion

        #region Get Dropdown

        public List<DropdownViewModel> GetGeneralParamDropdown(string ParamType)
        {
            dynamic args = new
            {
                ParamType = ParamType
            };
            IEnumerable<DropdownViewModel> result = db.Query<DropdownViewModel>("Master_GeneralParam/usp_GetGeneralParamDropdownListByType", args);
            return result.ToList();
        }
        
        #endregion
    }
}
