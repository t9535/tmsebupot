﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.Category_Nominative
{
    public class CategoryNominativeSearchCategoryViewModel
    {
        public string CategoryCode { get; set; }
		public string NominativeType { get; set; }
		public string JointGroup { get; set; }
		public string GroupSeq { get; set; } 
		public int page { get; set; }
		public int size { get; set; }

        public dynamic MapFromModel(int SortBy = 0, string SortDirection = "ASC", int FromNumber = 1, int ToNumber = 10)
        {
            return new
            {
                CategoryCode = this.CategoryCode,
                NominativeType = this.NominativeType,
                JointGroup = this.JointGroup,
                GroupSeq = this.GroupSeq,
                SortBy = SortBy,
                SortDirection = SortDirection,
                FromNumber = FromNumber,
                ToNumber = ToNumber
            };
        }
    }
}