﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.Category_Nominative
{
    public class CategoryNominativeDashboardViewModel
    {
        public int RowNum { get; set; }
        public int Id { get; set; }
        public Guid ParamId { get; set; }
        public string CategoryCode { get; set; }
        public string CategoryCodeas { get; set; }
        public string category_code_old { get; set; }
        public string NominativeType { get; set; }
        public string JointGroup { get; set; }
        public string GroupSeq { get; set; }

        public string PositionSignature { get; set; }

    }
}