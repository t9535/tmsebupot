﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TAM.EFaktur.Web.Models.Category_Nominative
{
    public class Category : BaseModel
    {
        public long RowNum { get; set; }
        public string CategoryCode { get; set; }
        public string category_code_old { get; set; }

        public string NominativeType { get; set; }
        public string JointGroup { get; set; }
        public string GroupSeq { get; set; }
        public Boolean PositionSignature { get; set; }
        public string GlStatusData { get; set; }
        public string AttachmentStatusData { get; set; }
    }
}
