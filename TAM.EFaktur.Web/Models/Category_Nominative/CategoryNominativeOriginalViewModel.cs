﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.Category_Nominative
{
    public class CategoryNominativeOriginalViewModel
    {
        public long DataFieldId { get; set; }
        public string CategoryCode { get; set; }
        public string NominativeType { get; set; }
        public string JointGroup { get; set; }
        public string GroupSeq { get; set; }

    }
}