﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TAM.EFaktur.Web.Models.Master_MenuRole
{
    public class MenuRole : BaseModel
    {
        public System.Guid RoleId { get; set; }
        public System.Guid MenuId { get; set; }
        public string ButtonName { get; set; }

    }
}
