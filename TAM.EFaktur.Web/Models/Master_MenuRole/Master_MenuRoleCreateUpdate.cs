﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.Master_MenuRole
{
    public class Master_MenuRoleCreateUpdate
    {
        public Guid Id { set; get; }
        public string RoleId { set; get; }
        public string MenuId { set; get; }
        public string RoleCode { set; get; }
        public string MenuCode { set; get; }
        public string ButtonName { set; get; }
        public DateTime EvenDate { get; set; }
        public string EvenActor { get; set; }
    }
}