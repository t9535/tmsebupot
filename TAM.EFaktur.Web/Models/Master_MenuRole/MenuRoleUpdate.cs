﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.Master_MenuRole
{
    public class MenuRoleUpdate
    {
        public Guid Id { get; set; }
        public string RoleId { get; set; }
        public string MenuId { get; set; }
        public string ButtonName { get; set; }
        public string ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
    }
}