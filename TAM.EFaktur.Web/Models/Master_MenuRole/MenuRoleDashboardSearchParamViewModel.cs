﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.Master_MenuRole
{
    public class MenuRoleDashboardSearchParamViewModel
    {
        public string RoleCode { get; set; } 
        public string MenuCode { get; set; }
        public string MenuName { get; set; }
        public string ButtonName { get; set; }
        
    }
}