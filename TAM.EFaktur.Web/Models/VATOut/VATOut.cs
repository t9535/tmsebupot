﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TAM.EFaktur.Web.Models.VATOut
{
    public class VATOut : BaseModel
    {
        public System.Guid Id { get; set; }
        public System.Guid SyncId { get; set; }
        public string DANumber { get; set; }
        public string BusinessUnit { get; set; }
        public System.Guid KDJenisTransaksiID { get; set; }
        public string KDJenisTransaksi { get; set; }
        public string FGPengganti { get; set; }
        public string NomorFaktur { get; set; }
        public string NomorFakturGabungan { get; set; }
        public int MasaPajak { get; set; }
        public int TahunPajak { get; set; }
        public System.DateTime TanggalFaktur { get; set; }
        public string NPWPCustomer { get; set; }
        public string NamaCustomer { get; set; }
        public string AlamatLawanTransaksi { get; set; }
        public decimal JumlahDPP { get; set; }
        public decimal JumlahPPN { get; set; }
        public decimal JumlahPPNBM { get; set; }
        public string IDKeteranganTambahan { get; set; }
        public decimal FGUangMuka { get; set; }
        public decimal UangMukaDPP { get; set; }
        public decimal UangMukaPPN { get; set; }
        public decimal UangMukaPPNBM { get; set; }
        public string Referensi { get; set; }
        public string DownloadStatus { get; set; }
        public string ApprovalStatus { get; set; }
        public Nullable<System.DateTime> ApprovalTime { get; set; }
        public string RecordStatus { get; set; }
        public Nullable<System.DateTime> RecordTime { get; set; }
        public string PdfUrl { get; set; }
        public string BatchFileName { get; set; }
        public bool DeleteStatus { get; set; }
        public bool IsApproveOrReject { get; set; }
        public bool RowStatus { get; set; }

        public dynamic MapFromModel(DateTime? EventDate = null, string EventActor = "")
        {
            return new
            {
                Id = this.Id,
                SyncId = this.SyncId,
                DANumber = this.DANumber,
                BusinessUnit = this.BusinessUnit,
                KDJenisTransaksiID = this.KDJenisTransaksiID,
                KDJenisTransaksi = this.KDJenisTransaksi,
                FGPengganti = this.FGPengganti,
                NomorFaktur = this.NomorFaktur,
                NomorFakturGabungan = this.NomorFakturGabungan,
                MasaPajak = this.MasaPajak,
                TahunPajak = this.TahunPajak,
                TanggalFaktur = this.TanggalFaktur,
                NPWPCustomer = this.NPWPCustomer,
                NamaCustomer = this.NamaCustomer,
                AlamatLawanTransaksi = this.AlamatLawanTransaksi,
                JumlahDPP = this.JumlahDPP,
                JumlahPPN = this.JumlahPPN,
                JumlahPPNBM = this.JumlahPPNBM,
                IDKeteranganTambahan = this.IDKeteranganTambahan,
                FGUangMuka = this.FGUangMuka,
                UangMukaDPP = this.UangMukaDPP,
                UangMukaPPN = this.UangMukaPPN,
                UangMukaPPNBM = this.UangMukaPPNBM,
                Referensi = this.Referensi,
                DownloadStatus = string.IsNullOrEmpty(this.DownloadStatus) ? Formatting.NOT_AVAILABLE : this.DownloadStatus,
                ApprovalStatus = string.IsNullOrEmpty(this.ApprovalStatus) ? Formatting.NOT_AVAILABLE : this.ApprovalStatus,
                ApprovalTime = this.ApprovalTime,
                RecordStatus = this.RecordStatus,
                RecordTime = this.RecordTime,
                PdfUrl = this.PdfUrl,
                BatchFileName = this.BatchFileName,
                DeleteStatus = this.DeleteStatus,
                IsApproveOrReject = this.IsApproveOrReject,
                RowStatus = this.RowStatus,
                CreatedBy = this.CreatedBy,
                CreatedOn = this.CreatedOn,
                EventDate = EventDate.HasValue ? EventDate.Value : DateTime.Now,
                EventActor = EventActor,
            };
        }
    }
}
