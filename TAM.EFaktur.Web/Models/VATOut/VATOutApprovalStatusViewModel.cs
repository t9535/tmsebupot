﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.VATOut
{
    public class VATOutApprovalStatusViewModel
    {
        public string NomorFakturGabungan { get; set; }
        public string ApprovalStatus { get; set; }
        public Nullable<DateTime> ApprovalTime { get; set; }
        public DateTime RecordTime { get; set; }
        public string TaxInvoiceStatus { get; set; }

        public dynamic MapFromModel(DateTime EventDate, string EventActor)
        {
            return new
            {
                NomorFakturGabungan = NomorFakturGabungan,
                ApprovalStatus = ApprovalStatus,
                ApprovalTime = ApprovalTime,
                RecordTime = RecordTime,
                TaxInvoiceStatus = TaxInvoiceStatus,
                EventDate = EventDate,
                EventActor = EventActor
                
            };
        }
    }
}