﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TAM.EFaktur.Web.Models.VATOut
{
    public class VATOutDashboardViewModel : BaseModel
    {
        public int RowNum { get; set; }
        public Guid Id { get; set; }
        public string TransactionDataFile { get; set; }
        public string NomorFaktur { get; set; }
        public string DANumber { get; set; }
        public string NPWPCustomer { get; set; }
        public string NamaCustomer { get; set; }
        public decimal JumlahDPP { get; set; }
        public decimal JumlahPPN { get; set; }
        public string BusinessUnit { get; set; }
        public DateTime TanggalFaktur { get; set; }
        public Nullable<DateTime> SyncTime { get; set; }
        //public Nullable<DateTime> ReceiveFileTime { get; set; }
        public string RecordStatus { get; set; }
        public DateTime RecordTime { get; set; }
        public string DownloadStatus { get; set; }
        public Nullable<DateTime>  CSVTime { get; set; }
        //public Nullable<DateTime>  DownloadTime { get; set; }
        public string ApprovalStatus { get; set; }
        public DateTime ApprovalTime { get; set; }
        public string TaxInvoiceStatus { get; set; }
        public string TransitoryStatus { get; set; }
        public string TransitoryNumber { get; set; }
        public DateTime TransitoryDate { get; set; }
        public string BatchFileName { get; set; }
        public string PDFFileStatus { get; set; }
        public string EFakturStatus { get; set; }

    }
}
