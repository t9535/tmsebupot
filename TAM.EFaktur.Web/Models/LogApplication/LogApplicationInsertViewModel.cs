﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.LogApplication
{
    public class LogApplicationInsertViewModel
    {
        public Guid Id { get; set; }
        public String Username { get; set; }
        public String ClientIP { get; set; }
        public String ClientHostName { get; set; }
        public DateTime LoginDate { get; set; }
        public Boolean IsLogged { get; set; }

        public List<LogApplicationDetailInsertViewModel> LogApplicationDetails { get; set; }

        public LogApplicationInsertViewModel CreateModel(string username, string clientIP, string clientHostName, DateTime loginDate, bool isLogged, List<LogApplicationDetailInsertViewModel> logApplicationDetails)
        {
            return new LogApplicationInsertViewModel
            {
                Username = username,
                ClientIP = clientIP,
                ClientHostName = clientHostName,
                LoginDate = loginDate,
                IsLogged = isLogged,
                LogApplicationDetails = logApplicationDetails
            };
        }

        public dynamic MapFromModel(Guid Id, DateTime? EventDate = null, string EventActor = "")
        {
            return new
            {
                Id = Id,
                Username = this.Username,
                ClientIP = this.ClientIP,
                ClientHostName = this.ClientHostName,
                LoginDate = this.LoginDate.FormatSQLDateTime(),
                IsLogged = this.IsLogged,
                EventDate = EventDate.HasValue ? EventDate.Value.FormatSQLDateTime() : DateTime.Now.FormatSQLDateTime(),
                EventActor = EventActor
            };
        }
    }
}