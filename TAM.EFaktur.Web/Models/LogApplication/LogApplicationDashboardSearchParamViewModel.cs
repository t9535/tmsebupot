﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.LogApplication
{
    public class LogApplicationDashboardSearchParamViewModel
    {
        public string Username { get; set; }
        public string LogDateFrom { get; set; }
        public string LogDateTo { get; set; }
        public string LogTimeFrom { get; set; }
        public string LogTimeTo { get; set; }
        public string MessageType { get; set; }
        
    }
}