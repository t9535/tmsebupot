﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.LogApplication
{
    public class LogApplicationRepository : BaseRepository
    {
        #region Singleton
        private LogApplicationRepository() { }
        private static LogApplicationRepository instance = null;
        public static LogApplicationRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new LogApplicationRepository();
                }
                return instance;
            }
        }
        #endregion

        #region Counter Data
        public int Count(LogApplicationDashboardSearchParamViewModel model)
        {
            dynamic args = new
            {

                LogDateFrom = model.LogDateFrom.FormatDefaultDateWhenNullOrEmpty(),
                LogDateTo = model.LogDateTo.FormatDefaultDateWhenNullOrEmpty("TO"),
                LogTimeFrom = model.LogTimeFrom.FormatDefaultTimeWhenNullOrEmpty(),
                LogTimeTo = model.LogTimeTo.FormatDefaultTimeWhenNullOrEmpty("TO"),
                MessageType = model.MessageType
            };

            return db.SingleOrDefault<int>("ApplicationLog/usp_CountApplicationLogListDashboard", args);
        }
        #endregion

        #region Processing Data
        public List<LogApplicationDashboardViewModel> GetList(LogApplicationDashboardSearchParamViewModel model, int SortBy, string SortDirection, int FromNumber, int ToNumber)
        {
            dynamic args = new
            {
                LogDateFrom = model.LogDateFrom.FormatDefaultDateWhenNullOrEmpty(),
                LogDateTo = model.LogDateTo.FormatDefaultDateWhenNullOrEmpty("TO"),
                LogTimeFrom = model.LogTimeFrom.FormatDefaultTimeWhenNullOrEmpty(),
                LogTimeTo = model.LogTimeTo.FormatDefaultTimeWhenNullOrEmpty("TO"),
                MessageType = model.MessageType,
                SortBy = SortBy,
                SortDirection = SortDirection,
                FromNumber = FromNumber,
                ToNumber = ToNumber
            };
            IEnumerable<LogApplicationDashboardViewModel> result = db.Query<LogApplicationDashboardViewModel>("ApplicationLog/usp_GetApplicationLogListDashboard", args);
            return result.ToList();
        }

        public Result Insert(LogApplicationInsertViewModel model, string EventActor, DateTime EventDate)
        {
            try
            {
                db.BeginTransaction();

                Guid ApplicationLogId = Guid.NewGuid();
                db.Execute("ApplicationLog/usp_Insert_TB_R_ApplicationLog", model.MapFromModel(ApplicationLogId, EventDate, EventActor));

                foreach(LogApplicationDetailInsertViewModel detailModel in model.LogApplicationDetails)
                {
                    db.Execute("ApplicationLog/usp_Insert_TB_R_ApplicationLogDetail", detailModel.MapFromModel(ApplicationLogId, EventDate, EventActor));
                }

                db.CommitTransaction();
                return MsgResultSuccessInsert();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                throw e;
            }
        }

        public LogApplication GetById(Guid Id)
        {
            return db.SingleOrDefault<LogApplication>("ApplicationLog/usp_GetApplicationLogByID", new { Id = Id });
        }

        //Get Dropdown Message Type
        public List<DropdownViewModel> GetMessageTypeDropdown()
        {

            IEnumerable<DropdownViewModel> result = db.Query<DropdownViewModel>("ApplicationLog/usp_DropDownMessageType");
            return result.ToList();
        }
        #endregion
    }
}