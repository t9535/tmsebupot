﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models
{
    public class Result
    {
        public bool ResultCode { get; set; }
        public string ResultDesc { get; set; }
        public string ResultDescs { get; set; }
    }
}