﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.ParameterSetup
{
    public class ParameterSetupRepository : BaseRepository
    {
        #region Singleton
        private ParameterSetupRepository() { }
        private static ParameterSetupRepository instance = null;
        public static ParameterSetupRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ParameterSetupRepository();
                }
                return instance;
            }
        }
        #endregion

        #region Counter Data
        public int Count(ParameterSetupSearchParamViewModel model)
        {
            try
            {
                return db.SingleOrDefault<int>("ParameterSetup/usp_CountParameterSetupListDashboard", model.MapFromModel());
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Processing Data
        public List<Guid> GetIdBySearchParam(ParameterSetupSearchParamViewModel model)
        {
            IEnumerable<Guid> result = db.Query<Guid>("ParameterSetup/usp_GetParameterSetupIdBySearchParam", model.MapFromModel());
            return result.ToList();
        }

        public List<ParameterSetupDashBoardViewModel> GetList(ParameterSetupSearchParamViewModel model, int SortBy, string SortDirection, int FromNumber, int ToNumber)
        {
            IEnumerable<ParameterSetupDashBoardViewModel> result = db.Query<ParameterSetupDashBoardViewModel>("ParameterSetup/usp_GetParameterSetupListDashboard", model.MapFromModel(SortBy, SortDirection, FromNumber, ToNumber));
            return result.ToList();
        }

        public Result Save(ParameterSetupParamModel objParameterSetupParamModel, string CeatedBy)
        {
            try
            {
                db.Execute("ParameterSetup/usp_Insert_Update_parameter", objParameterSetupParamModel.MapFromModel(CeatedBy));
                return MsgResultSuccessInsert();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
                //return    e.Message;
                //return MsgResultError(e.LogToApp("Save Parameter", e.Message, "Submit").Message);
            }
        }
       

        public Result Delete(Guid Id, string EventActor)
        {
            try
            {
                db.Execute("ParameterSetup/usp_Delete_parameter", new { Id = Id });
                return MsgResultSuccessDelete();
            }
            catch (Exception e)
            {
                return MsgResultError(e.LogToApp("Delete Parameter Return", MessageType.ERR, EventActor).Message);
            }
        }

        public List<ParameterSetupOriginalViewModel> GetXlsReportById(List<Guid> ParamId)
        {
            dynamic args = new
            {
                ParamId = string.Join(";", ParamId)
            };
            IEnumerable<ParameterSetupOriginalViewModel> result = db.Query<ParameterSetupOriginalViewModel>("ParameterSetup/usp_GetparameterReportById", args);
            return result.ToList();
        }


        public ParameterSetupViewModelById GetParameterById(Guid Id)
        {

            ParameterSetupViewModelById result = db.SingleOrDefault<ParameterSetupViewModelById>("ParameterSetup/usp_getparameterbyid", new { Id = Id });
            return result;
            
        }

        public ParameterSetupViewModel GetParameterByCode(string code)
        {

            ParameterSetupViewModel result = db.SingleOrDefault<ParameterSetupViewModel>("ParameterSetup/usp_getparameterbycode", new { parameter_code = code });
 
            return result;

        }
        #endregion
    }
}