﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models
{
    public class MasterTax
    {
        public float TaxValue { get; set; }
        public DateTime TaxDate { get; set; }
    }
}