﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.ParameterSetup
{
    public class ParameterSetupSearchParamViewModel
    {
		public string ParameterCode { get; set; }
		public string ParameterName { get; set; }
		public string ParameterValue { get; set; }
		public string FromFieldName { get; set; }
		public string ParameterCategoryName { get; set; }
		public string Color { get; set; }
		public string Icon { get; set; }
		public string Note1 { get; set; }
		public string Note2 { get; set; }
		public string Note3 { get; set; }
		public string Note4 { get; set; }
		public string Note5 { get; set; }
		public string TransactionCode { get; set; }
		public string BusinessUnit { get; set; }
		public string ModuleName { get; set; }

		public string ParameterDeadlineOperator { get; set; }
        public Nullable<Int32> ParameterDeadline { get; set; }
        public string ColorStatus { get; set; }
		public int page { get; set; }
		public int size { get; set; }

        public dynamic MapFromModel(int SortBy = 0, string SortDirection = "ASC", int FromNumber = 1, int ToNumber = 10)
        {
            return new
            {
                ParameterCode = this.ParameterCode,
                ParameterName = this.ParameterName,
                ParameterValue = this.ParameterValue,
                FromFieldName = this.FromFieldName,
                ParameterCategoryName = this.ParameterCategoryName,
                Color = this.Color,
                Icon = this.Icon,
                Note1 = this.Note1,
                Note2 = this.Note2,
                Note3 = this.Note3,
                Note4 = this.Note4,
                Note5 = this.Note5,
                TransactionCode = this.TransactionCode,
                BusinessUnit = this.BusinessUnit,
                ModuleName = this.ModuleName,
                ParameterDeadlineOperator = this.ParameterDeadlineOperator,
                ParameterDeadline = this.ParameterDeadline,
                ColorStatus = this.ColorStatus,
                SortBy = SortBy,
                SortDirection = SortDirection,
                FromNumber = FromNumber,
                ToNumber = ToNumber
            };
        }
    }
}