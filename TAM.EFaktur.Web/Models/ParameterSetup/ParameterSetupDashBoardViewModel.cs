﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TAM.EFaktur.Web.Models.ParameterSetup
{
    public class ParameterSetupDashBoardViewModel : Controller
    {
		public int RowNum { get; set; }
		public Guid Id { get; set; }
		public string ParameterCode { get; set; }
		public string ParameterName { get; set; }
		public string ParameterValue { get; set; }
		public string FromFieldName { get; set; }
		public string ParameterCategoryName { get; set; }
		public string Color { get; set; }
		public string Icon { get; set; }
		public string Note1 { get; set; }
		public string Note2 { get; set; }
		public string Note3 { get; set; }
		public string Note4 { get; set; }
		public string Note5 { get; set; }
		public string TransactionCode { get; set; }
		public string TransactionCodeName { get; set; }
		
		public string BusinessUnit { get; set; }
		public string ModuleName { get; set; }
		public Int32 ParameterDeadline { get; set; }
		public string ColorStatus { get; set; }
	}
}