﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.Register
{
    public class Register : BaseModel
    {
        public String SupplierNPWP { get; set; }
        public String RegistrationType { get; set; }
        public String RegistrationNum { get; set; }
        public String SKBType { get; set; }
        public DateTime RegistrationDate { get; set; }
        public String SupplierName { get; set; }
        public String SupplierAddress { get; set; }
        public String SupplierPhoneNum { get; set; }
        public String SupplierCountry { get; set; }
        public DateTime SupplierEstDate { get; set; }
        public DateTime ValidityPeriodFrom { get; set; }
        public DateTime ValidityPeriodTo { get; set; }
        public String CODSignerName { get; set; }
        public String CODSignerTitle { get; set; }
        public String UploadFile { get; set; }

        public dynamic MapFromModel(DateTime? EventDate = null, string EventActor = "")
        {
            dynamic args = new
            {
                Id = this.Id,
                SupplierNPWP = this.SupplierNPWP,
                RegistrationType = this.RegistrationType,
                RegistrationNum = this.RegistrationNum,
                SKBType = this.SKBType,
                RegistrationDate = this.RegistrationDate.FormatSQLDate(),
                SupplierName = this.SupplierName,
                SupplierAddress = this.SupplierAddress,
                SupplierPhoneNum = this.SupplierPhoneNum,
                SupplierCountry = this.SupplierCountry,
                SupplierEstDate = this.SupplierEstDate.FormatSQLDate(),
                ValidityPeriodFrom = this.ValidityPeriodFrom,
                ValidityPeriodTo = this.ValidityPeriodTo,
                CODSignerName = this.CODSignerName,
                CODSignerTitle = this.CODSignerTitle,
                UploadFile = this.UploadFile,
            };
            return args;
        }
    }
}
