﻿namespace TAM.EFaktur.Web.Models.Register
{
    public class RegisterGeneralParamSearchViewModel
    {
        public string ParamCode { get; set; }
        public string ParamName { get; set; }
        public string ParamType { get; set; }
        public string ParamValue { get; set; }

        public dynamic MapFromModel()
        {
            return new
            {
                ParamCode = this.ParamCode,
                ParamName = this.ParamName,
                ParamType = this.ParamType,
                ParamValue = this.ParamValue
            };
        }
    }

    public class RegisterGeneralParamViewModel
    {
        public string Id { get; set; }
        public string ParamCode { get; set; }
        public string ParamName { get; set; }
        public string ParamType { get; set; }
        public string ParamValue { get; set; }

    }


}