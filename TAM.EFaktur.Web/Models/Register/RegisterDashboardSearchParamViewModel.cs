﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.Register
{
    public class RegisterDashboardSearchParamViewModel
    {
        public string TaxFacCategory { get; set; }
        public string TaxFacNo { get; set; }
        public string SupplierName { get; set; }
        public string SupplierAddress { get; set; }
        public string SupplierCountry { get; set; }
        public string SupplierEstDateFrom { get; set; }
        public string SupplierEstDateTo { get; set; }
        public string FacDocSignerName { get; set; }
        public string RecordedDateFrom { get; set; }
        public string RecordedDateTo { get; set; }
        public string ApprovalStatus { get; set; }
        public string PDFFacStatus { get; set; }

        public string SKBType { get; set; }
        public string TaxFacDateFrom { get; set; }
        public string TaxFacDateTo { get; set; }
        public string SupplierNPWP { get; set; }
        public string SupplierPhoneNo { get; set; }
        public string SupplierCountryCode { get; set; }
        public string SupplierCountryName { get; set; }
        public string FacValidPeriodFrom { get; set; }
        public string FacValidPeriodTo { get; set; }
        public string FacDocSignerTitle { get; set; }
        public string RecordedBy { get; set; }
        public string ApprovalDateFrom { get; set; }
        public string ApprovalDateTo { get; set; }
        public string ApprovalDate { get; set; }
        public string UploadFile { get; set; }
        public int Seq { get; set; }

        public string MAPCode { get; set; }
        public string RecordedTimeFrom { get; set; }
        public string RecordedTimeTo { get; set; }

        public dynamic MapFromModel(bool isFullAccess = false, string DivisionName = "", string Username = "", int SortBy = 0, string SortDirection = "ASC", int FromNumber = 1, int ToNumber = 10)
        {
            return new
            {
                TaxFacCategory = this.TaxFacCategory,
                TaxFacNo = this.TaxFacNo,
                SupplierName = this.SupplierName,
                SupplierCountry = this.SupplierCountry,
                SupplierEstDateFrom = this.SupplierEstDateFrom.FormatDefaultDateWhenNullOrEmpty(),
                SupplierEstDateTo = this.SupplierEstDateTo.FormatDefaultDateWhenNullOrEmpty("TO"),
                FacDocSignerName = this.FacDocSignerName,
                RecordedDateFrom = this.RecordedDateFrom.FormatDefaultDateWhenNullOrEmpty(),
                RecordedDateTo = this.RecordedDateTo.FormatDefaultDateWhenNullOrEmpty("TO"),
                ApprovalStatus = this.ApprovalStatus,
                PDFFacStatus = this.PDFFacStatus,

                SKBType = this.SKBType,
                TaxFacDateFrom = this.TaxFacDateFrom.FormatDefaultDateWhenNullOrEmpty(),
                TaxFacDateTo = this.TaxFacDateTo.FormatDefaultDateWhenNullOrEmpty("TO"),
                SupplierNPWP = this.SupplierNPWP,
                SupplierPhoneNo = this.SupplierPhoneNo,
                SupplierCountryCode = this.SupplierCountryCode,
                SupplierCountryName = this.SupplierCountryName,
                FacValidPeriodFrom = this.FacValidPeriodFrom.FormatDefaultDateWhenNullOrEmpty(),
                FacValidPeriodTo = this.FacValidPeriodTo.FormatDefaultDateWhenNullOrEmpty("TO"),
                FacDocSignerTitle = this.FacDocSignerTitle,
                RecordedBy = this.RecordedBy,
                ApprovalDateFrom = this.ApprovalDateFrom.FormatDefaultDateWhenNullOrEmpty(),
                ApprovalDateTo = this.ApprovalDateTo.FormatDefaultDateWhenNullOrEmpty("TO"),
                DivisionName = DivisionName,
                Username = Username,
                SortBy = SortBy,
                SortDirection = SortDirection,
                FromNumber = FromNumber,
                ToNumber = ToNumber,
                isFullAccess = isFullAccess,

                ApprovalDate = this.ApprovalDate.FormatDefaultDateWhenNullOrEmpty(),

                UploadFile = this.UploadFile,
                MAPCode = this.MAPCode,
                RecordedTimeFrom = this.RecordedTimeFrom,
                RecordedTimeTo = this.RecordedTimeTo,
            };
        }
    }
}