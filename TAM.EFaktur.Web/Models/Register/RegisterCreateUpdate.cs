﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.Register
{
    public class RegisterCreateUpdate
    {
        public String TaxFacCategory { get; set; }
        public String TaxFacNo { get; set; }
        public String SKBType { get; set; }
        public String SKBTypeOthers { get; set; }
        public String TaxFacDate { get; set; }
        public String SupplierName { get; set; }
        public String SupplierNPWP { get; set; }
        public String SupplierAddress { get; set; }
        public String SupplierPhoneNo { get; set; }
        public String SupplierCountry { get; set; }
        public String SupplierEstDate { get; set; }
        public String FacValidPeriodFrom { get; set; }
        public String FacValidPeriodTo { get; set; }
        public String FacDocSignerName { get; set; }
        public String FacDocSignerTitle { get; set; }
        public String RecordedDate { get; set; }
        public String RecordedBy { get; set; }
        public String ApprovalStatus { get; set; }
        public String ApprovalDate { get; set; }
        public String UploadFile { get; set; }
        public String RegistrationNo { get; set; }
        public String MapCode { get; set; }
        public int Seq { get; set; }
        public String PDFFacStatus { get; set; }
    }
}