﻿using System;

namespace TAM.EFaktur.Web.Models.Register
{
    public class RegisterApprovalStatusViewModel
    {
        public string TaxFacNo { get; set; }
        public string ApprovalStatus { get; set; }
        public Nullable<DateTime> ApprovalDate { get; set; }
        public DateTime RecordedDate { get; set; }
        public string RegistrationNo { get; set; }

        public dynamic MapFromModel(string EventActor)
        {
            return new
            {
                TaxFacNo = TaxFacNo,
                ApprovalStatus = ApprovalStatus,
                ApprovalDate = ApprovalDate,
                RecordedDate = RecordedDate,
                RecordedBy = EventActor,
                RegistrationNo = RegistrationNo
            };
        }
    }
}