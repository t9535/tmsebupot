﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using TAM.EFaktur.Web.Models.Log_Sync;

namespace TAM.EFaktur.Web.Models.Register
{
    public class RegisterRepository : BaseRepository
    {
        #region Singleton
        private RegisterRepository() { }
        private static RegisterRepository instance = null;
        public static RegisterRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new RegisterRepository();
                }
                return instance;
            }
        }
        #endregion

        enum ApprovalStatus
        {
            [field: Description("Wait For Approval")]
            Wait,

            [field: Description("Approved")]
            Approved,

            [field: Description("Reject")]
            Reject
        }


        //Insert New Tax Fac
        public Result InsertSKB(RegisterCreateUpdate model, string EventActor, DateTime EventDate)
        {
            try
            {
                dynamic args = new
                {
                    TaxFacCategory = "SKB",
                    TaxFacNo = model.TaxFacNo,
                    SKBType = model.SKBType,
                    SKBTypeOthers = model.SKBTypeOthers,
                    TaxFacDate = model.TaxFacDate.FormatSQLDate(),
                    SupplierName = model.SupplierName,
                    SupplierNPWP = model.SupplierNPWP,
                    SupplierAddress = model.SupplierAddress,
                    SupplierPhoneNo = model.SupplierPhoneNo,
                    SupplierCountry = "IDN",
                    SupplierEstDate = model.SupplierEstDate.FormatSQLDate(), //EventDate.FormatSQLDate(),
                    FacValidPeriodFrom = model.FacValidPeriodFrom.FormatSQLDate(),
                    FacValidPeriodTo = model.FacValidPeriodTo.FormatSQLDate(),
                    FacDocSignerName = model.FacDocSignerName,
                    FacDocSignerTitle = model.FacDocSignerTitle,
                    RecordedDate = EventDate.FormatSQLDateTime(),
                    RecordedBy = EventActor,
                    UploadFile = model.UploadFile,
                    Seq = model.Seq == 0 ? 1 : model.Seq,
                    MapCode = model.MapCode,
                    PDFFacStatus = model.PDFFacStatus,
                    ApprovalStatus = "Wait For Approval"
                };
                db.Execute("Register/usp_Insert_TB_M_Facility", args);

                List<LogSyncCreateUpdate> logSyncList = new List<LogSyncCreateUpdate>();

                logSyncList.Add(new LogSyncCreateUpdate().CreateModel(model.UploadFile, 1, "SKB Number : " + model.TaxFacNo + " | Supplier Npwp : " + model.SupplierNPWP, MessageType.INF, "Insert SKB :" + ExceptionHandler.GENERAL_SUCCESS_MESSAGE));

                #region SyncLog Insert
                db.BeginTransaction();
                ExceptionHandler.LogToSync(logSyncList);
                db.CommitTransaction();
                #endregion

                return MsgResultSuccessInsert();
            }
            catch (Exception e)
            {
                return MsgResultError(e.Message.ToString());

            }
        }

        public Result InsertCOD(RegisterCreateUpdate model, string EventActor, DateTime EventDate)
        {
            try
            {
                dynamic args = new
                {
                    TaxFacCategory = "COD",
                    TaxFacNo = model.TaxFacNo,
                    SKBType = model.SKBType,
                    SKBTypeOthers = model.SKBTypeOthers,
                    TaxFacDate = model.TaxFacDate.FormatSQLDate(),
                    SupplierName = model.SupplierName,
                    SupplierNPWP = model.SupplierNPWP,
                    SupplierAddress = model.SupplierAddress,
                    SupplierPhoneNo = model.SupplierPhoneNo,
                    SupplierCountry = model.SupplierCountry,
                    SupplierEstDate = model.SupplierEstDate.FormatSQLDate(),
                    FacValidPeriodFrom = model.FacValidPeriodFrom.FormatSQLDate(),
                    FacValidPeriodTo = model.FacValidPeriodTo.FormatSQLDate(),
                    FacDocSignerName = model.FacDocSignerName,
                    FacDocSignerTitle = model.FacDocSignerTitle,
                    RecordedDate = EventDate.FormatSQLDateTime(),
                    RecordedBy = EventActor,
                    MapCode = '-',
                    UploadFile = model.UploadFile,
                    Seq = model.Seq == 0 ? 1 : model.Seq,
                    PDFFacStatus = model.PDFFacStatus,
                    ApprovalStatus = "Wait For Approval"
                };
                db.Execute("Register/usp_Insert_TB_M_Facility", args);

                List<LogSyncCreateUpdate> logSyncList = new List<LogSyncCreateUpdate>();

                logSyncList.Add(new LogSyncCreateUpdate().CreateModel(model.UploadFile, 1, "COD Number : " + model.TaxFacNo + " | Supplier Npwp : " + model.SupplierNPWP, MessageType.INF, "Insert COD :" + ExceptionHandler.GENERAL_SUCCESS_MESSAGE));

                #region SyncLog Insert
                db.BeginTransaction();
                ExceptionHandler.LogToSync(logSyncList);
                db.CommitTransaction();
                #endregion

                return MsgResultSuccessInsert();
            }
            catch (Exception e)
            {
                return MsgResultError(e.Message.ToString());
            }
        }

        #region Upload Tax Facility Data
        public Result InsertUploadSKB(RegisterCreateUpdate model, string EventActor, DateTime EventDate)
        {
            try
            {
                dynamic args = new
                {
                    TaxFacCategory = "SKB",
                    TaxFacNo = model.TaxFacNo,
                    SKBType = model.SKBType,
                    SKBTypeOthers = model.SKBTypeOthers,
                    TaxFacDate = model.TaxFacDate,//.FormatSQLDate(),
                    SupplierName = model.SupplierName,
                    SupplierNPWP = model.SupplierNPWP,
                    SupplierAddress = model.SupplierAddress,
                    SupplierPhoneNo = model.SupplierPhoneNo,
                    SupplierCountry = "IDN",
                    SupplierEstDate = model.SupplierEstDate,//.FormatSQLDate(), //EventDate.FormatSQLDate(),
                    FacValidPeriodFrom = model.FacValidPeriodFrom,//.FormatSQLDate(),
                    FacValidPeriodTo = model.FacValidPeriodTo,//.FormatSQLDate(),
                    FacDocSignerName = model.FacDocSignerName,
                    FacDocSignerTitle = model.FacDocSignerTitle,
                    RecordedDate = EventDate.FormatSQLDateTime(),
                    RecordedBy = model.RecordedBy == "" ? EventActor : model.RecordedBy,
                    MapCode = model.MapCode,
                    UploadFile = model.UploadFile,
                    Seq = model.Seq == 0 ? 1 : model.Seq,
                    PDFFacStatus = model.PDFFacStatus,
                    ApprovalStatus = model.ApprovalStatus == "" ? "Wait For Approval" : model.ApprovalStatus,
                    ApprovalDate = model.ApprovalDate == "" ? null : model.ApprovalDate,//.FormatSQLDate(),
                    RegistrationNo = model.RegistrationNo
                };
                db.Execute("Register/usp_Insert_Upload_TB_M_Facility", args);

                List<LogSyncCreateUpdate> logSyncList = new List<LogSyncCreateUpdate>();

                //logSyncList.Add(new LogSyncCreateUpdate().CreateModel(model.UploadFile, 1, "SKB Number : " + model.TaxFacNo + " | Supplier Npwp : " + model.SupplierNPWP, MessageType.INF, "Insert SKB :" + ExceptionHandler.GENERAL_SUCCESS_MESSAGE));

                #region SyncLog Insert
                db.BeginTransaction();
                ExceptionHandler.LogToSync(logSyncList);
                db.CommitTransaction();
                #endregion

                return MsgResultSuccessInsert();
            }
            catch (Exception e)
            {
                return MsgResultError(e.Message.ToString());

            }
        }

        public Result InsertUploadCOD(RegisterCreateUpdate model, string EventActor, DateTime EventDate)
        {
            try
            {
                dynamic args = new
                {
                    TaxFacCategory = "COD",
                    TaxFacNo = model.TaxFacNo,
                    SKBType = model.SKBType,
                    SKBTypeOthers = model.SKBTypeOthers,
                    TaxFacDate = model.TaxFacDate,//.FormatSQLDate(),
                    SupplierName = model.SupplierName,
                    SupplierNPWP = model.SupplierNPWP,
                    SupplierAddress = model.SupplierAddress,
                    SupplierPhoneNo = model.SupplierPhoneNo,
                    SupplierCountry = model.SupplierCountry,
                    SupplierEstDate = model.SupplierEstDate,//.FormatSQLDate(),
                    FacValidPeriodFrom = model.FacValidPeriodFrom,//.FormatSQLDate(),
                    FacValidPeriodTo = model.FacValidPeriodTo,//.FormatSQLDate(),
                    FacDocSignerName = model.FacDocSignerName,
                    FacDocSignerTitle = model.FacDocSignerTitle,
                    RecordedDate = EventDate.FormatSQLDateTime(),
                    RecordedBy = model.RecordedBy == "" ? EventActor : model.RecordedBy,
                    MapCode = model.MapCode,
                    UploadFile = model.UploadFile,
                    Seq = model.Seq == 0 ? 1 : model.Seq,
                    PDFFacStatus = model.PDFFacStatus,
                    ApprovalStatus = model.ApprovalStatus == "" ? "Wait For Approval" : model.ApprovalStatus,
                    ApprovalDate = model.ApprovalDate == "" ? null : model.ApprovalDate,//.FormatSQLDate(),
                    RegistrationNo = model.RegistrationNo
                };
                db.Execute("Register/usp_Insert_Upload_TB_M_Facility", args);

                List<LogSyncCreateUpdate> logSyncList = new List<LogSyncCreateUpdate>();

                //logSyncList.Add(new LogSyncCreateUpdate().CreateModel(model.UploadFile, 1, "COD Number : " + model.TaxFacNo + " | Supplier Npwp : " + model.SupplierNPWP, MessageType.INF, "Insert COD :" + ExceptionHandler.GENERAL_SUCCESS_MESSAGE));

                #region SyncLog Insert
                db.BeginTransaction();
                ExceptionHandler.LogToSync(logSyncList);
                db.CommitTransaction();
                #endregion

                return MsgResultSuccessInsert();
            }
            catch (Exception e)
            {
                return MsgResultError(e.Message.ToString());
            }
        }
        #endregion Upload Tax Facility Data
        #region Counter Data
        public int Count(RegisterDashboardSearchParamViewModel model, string DivisionName, string Username, bool isFullAccess)
        {
            try
            {
                return db.SingleOrDefault<int>("Register/usp_CountRegisterListDashboard", model.MapFromModel(isFullAccess, DivisionName, Username));
            }
            catch
            {
                throw;
            }
        }
        #endregion



        public List<RegisterDashboardViewModel> GetList(RegisterDashboardSearchParamViewModel model, string DivisionName, string Username, int SortBy, string SortDirection, int FromNumber, int ToNumber, bool isFullAccess)
        {
            IEnumerable<RegisterDashboardViewModel> result = db.Query<RegisterDashboardViewModel>("Register/usp_GetRegisterListDashboard", model.MapFromModel(isFullAccess, DivisionName, Username, SortBy, SortDirection, FromNumber, ToNumber));
            return result.ToList();
        }

        public List<RegisterGeneralParamViewModel> GetListParam(RegisterGeneralParamSearchViewModel model)
        {
            IEnumerable<RegisterGeneralParamViewModel> result = db.Query<RegisterGeneralParamViewModel>("Register/usp_GetGeneralParam", model);
            return result.ToList();
        }


        public List<RegisterDashboardViewModel> GetTaxFacilityByNo(RegisterDashboardSearchParamViewModel model, string DivisionName, string Username, bool isFullAccess)
        {
            IEnumerable<RegisterDashboardViewModel> result = db.Query<RegisterDashboardViewModel>("Register/usp_GetRegisterIdBySearchParam", model.MapFromModel(isFullAccess, DivisionName, Username));
            return result.ToList();
        }

        public List<RegisterExcelViewModel> GetXlsReportById(List<string> TaxFacNoList)
        {
            dynamic args = new
            {
                TaxFacNoList = string.Join(";", TaxFacNoList)
            };
            IEnumerable<RegisterExcelViewModel> result = db.Query<RegisterExcelViewModel>("Register/usp_GetRegisterByTaxFacNo", args);
            return result.ToList();
        }

        public List<string> GetPDFUrlById(List<string> TaxFacNoList)
        {
            dynamic args = new
            {
                TaxFacNoList = string.Join(";", TaxFacNoList)
            };
            IEnumerable<string> result = db.Query<string>("Register/usp_GetRegisterPDFUrlByTaxFacNo", args);
            return result.ToList();
        }

        public Result RegisterFileInsert(string taxFacNo, string path, string EventActor)
        {
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = "File has been uploaded successfully"
            };

            FileStream file = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite);

            try
            {
                file.Close();

                List<LogSyncCreateUpdate> logSyncList = new List<LogSyncCreateUpdate>();

                logSyncList.Add(new LogSyncCreateUpdate()
                   .CreateModel(Path.GetFileName(path), 0, "", MessageType.INF, ExceptionHandler.GENERAL_SUCCESS_MESSAGE, path));

                Update(taxFacNo, path);

                #region SyncLog Insert
                db.BeginTransaction();
                ExceptionHandler.LogToSync(logSyncList);
                Update(taxFacNo, path);
                db.CommitTransaction();
                #endregion

            }
            catch (Exception e)
            {
                db.AbortTransaction();

                result.ResultCode = false;
                result.ResultDesc = e.LogToSync(1, "", Path.GetFileName(path), "", MessageType.ERR).Message;
            }

            db.Close();
            return result;
        }

        public Result Update(string taxFacNo, string path)
        {
            try
            {
                dynamic args = new
                {
                    TaxFacNo = taxFacNo,
                    UploadFile = path,
                };
                db.Execute("Register/usp_Update_TB_M_Facility", args);

                return MsgResultSuccessInsert();
            }
            catch (Exception e)
            {
                return MsgResultError(e.Message.ToString());
            }
        }

        public Result DeleteRegisterById(List<string> TaxFacNoList, List<string> TaxFacSeqList, string EventActor)
        {
            try
            {
                db.BeginTransaction();
                if (TaxFacSeqList.Count > 0)
                {
                    for (int i = 0; i < TaxFacNoList.Count; i++)
                    {
                        dynamic args = new
                        {
                            TaxFacNoList = TaxFacNoList[i],
                            TaxFacSeqList = TaxFacSeqList[i]
                        };
                        db.Execute("Register/usp_DeleteRegisterByTaxFacNoSeq", args);
                    }
                }
                else {
                    dynamic args = new
                    {
                        TaxFacNoList = string.Join(";", TaxFacNoList)
                    };
                    db.Execute("Register/usp_DeleteRegisterByTaxFacNo", args);
                }
                db.CommitTransaction();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                throw e;

            }
            return MsgResultSuccessDelete();
        }

        public Result ExcelApprovalStatusUpdate(string path, string user, string DivisionName, bool isFullAccess)
        {
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = Formatting.UPLOAD_NOTIFICATION
            };

            FileStream file = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite);

            HSSFWorkbook hssfwb = new HSSFWorkbook(file);
            ISheet sheet = hssfwb.GetSheet("Default");

            string pipedString = string.Empty;
            string approvalTime = string.Empty;
            string recordeTime = string.Empty;

            int row = 0;
            int rowFail = 0;
            int rowSuccess = 0;

            try
            {
                file.Close();
                RegisterApprovalStatusViewModel registerApprovalStatus = new RegisterApprovalStatusViewModel();
                var cellNum = 5; // 5 is the cell amount in one row
                List<int> exceptionCellNum = new List<int>();
                db.BeginTransaction();

                for (row = 3; row <= sheet.LastRowNum; row++) // Data row start at row 5, index 4 on excel template
                {
                    try
                    {
                        #region Set Cell Policy, Cell Type and Save Sync Data
                        pipedString = string.Empty;
                        for (int i = 0; i < cellNum; i++)
                        {
                            sheet.GetRow(row).GetCell(i, MissingCellPolicy.CREATE_NULL_AS_BLANK);
                            sheet.GetRow(row).GetCell(i).SetCellType(CellType.STRING);

                            pipedString += sheet.GetRow(row).GetCell(i).StringCellValue.ToString().Trim() + "|";
                        }
                        pipedString = pipedString.Remove(pipedString.Length - 1);
                        #endregion

                        //if (GlobalFunction.BlankCellCheck(sheet.GetRow(row), cellNum, exceptionCellNum))
                        //{
                        //    var regModel = new RegisterDashboardSearchParamViewModel();
                        //    regModel.TaxFacNo = sheet.GetRow(row).GetCell(0).StringCellValue.ToString().Trim();
                        //    var _taxFacilityData = GetTaxFacilityByNo(regModel, DivisionName, user, isFullAccess) ;
                        //    var maxSeq = _taxFacilityData.Max(a => a.Seq);

                        //    _taxFacilityData = _taxFacilityData.Where(a => a.Seq.Equals(maxSeq)).ToList();

                        //    // if data approved / rejected continue
                        //    if (_taxFacilityData != null && _taxFacilityData.FirstOrDefault().ApprovalStatus.Equals("Approved"))
                        //    {
                        //        continue;
                        //    }

                        //    // if data wait
                        #region Cell Mapping & Insert to DB
                        //    if (_taxFacilityData != null && _taxFacilityData.FirstOrDefault().ApprovalStatus.Equals("Wait For Approval"))
                        //    {

                        registerApprovalStatus = new RegisterApprovalStatusViewModel();
                        registerApprovalStatus.TaxFacNo = sheet.GetRow(row).GetCell(0).StringCellValue.ToString().Trim();
                        registerApprovalStatus.ApprovalStatus = sheet.GetRow(row).GetCell(1).StringCellValue.ToString().Trim();

                        registerApprovalStatus.RegistrationNo = sheet.GetRow(row).GetCell(4).StringCellValue.ToString().Trim();

                        approvalTime = sheet.GetRow(row).GetCell(2).StringCellValue.ToString().Trim();
                        recordeTime = sheet.GetRow(row).GetCell(3).StringCellValue.ToString().Trim();

                        int Emptycount = 0;
                        StringBuilder builder = new StringBuilder();

                        builder.Append("Excel Row : " + Convert.ToString(row + 1) + "  mandatory field(");
                        if (string.IsNullOrEmpty(sheet.GetRow(row).GetCell(0).StringCellValue.ToString().Trim()))
                        {
                            Emptycount += 1;
                            builder.Append("TAX_FACILITY_NO,");
                        }
                        if (string.IsNullOrEmpty(sheet.GetRow(row).GetCell(1).StringCellValue.ToString().Trim()))
                        {
                            Emptycount += 1;
                            builder.Append(" APPROVAL_STATUS,");

                        }
                        if (string.IsNullOrEmpty(sheet.GetRow(row).GetCell(2).StringCellValue.ToString().Trim()))
                        {
                            Emptycount += 1;
                            builder.Append(" TANGGAL_APPROVAL,");

                        }
                        if (string.IsNullOrEmpty(sheet.GetRow(row).GetCell(3).StringCellValue.ToString().Trim()))

                        {
                            Emptycount += 1;
                            builder.Append(" TANGGAL_REKAM,");

                        }
                        if (string.IsNullOrEmpty(sheet.GetRow(row).GetCell(4).StringCellValue.ToString().Trim()))

                        {
                            Emptycount += 1;
                            builder.Append(" REGISTRATION_NUMBER,");

                        }

                        if (Emptycount > 0)
                        {

                            builder.Append(")mandatory field cannot be empty");
                            throw new Exception(builder.ToString());
                        }

                        if (!string.IsNullOrEmpty(approvalTime))
                        {

                            var approvalDate = sheet.GetRow(row).GetCell(2).StringCellValue.ToString().Trim().Split('/');
                            if (approvalDate.Count() > 1) // when the excel column is a text type
                            {
                                if (approvalDate[0].Length == 2 && approvalDate[1].Length == 2 && approvalDate[2].Length == 4)
                                {
                                    registerApprovalStatus.ApprovalDate = DateTime.ParseExact(sheet.GetRow(row).GetCell(2).StringCellValue.ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                }
                                else
                                {
                                    throw new Exception("Date Format must be: dd/mm/yyyy");
                                }
                            }
                            else // when the excel column is a DateTime type
                            {
                                registerApprovalStatus.ApprovalDate = DateTime.FromOADate(Convert.ToDouble(sheet.GetRow(row).GetCell(2).StringCellValue.ToString().Trim()));
                            }
                        }

                        if (!string.IsNullOrEmpty(recordeTime))
                        {

                            var recordeDate = sheet.GetRow(row).GetCell(3).StringCellValue.ToString().Trim().Split('/');
                            if (recordeDate.Count() > 1) // when the excel column is a text type
                            {
                                if (recordeDate[0].Length == 2 && recordeDate[1].Length == 2 && recordeDate[2].Length == 4)
                                {
                                    registerApprovalStatus.RecordedDate = DateTime.ParseExact(sheet.GetRow(row).GetCell(3).StringCellValue.ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                }
                                else
                                {
                                    throw new Exception("Date Format must be: dd/mm/yyyy");
                                }
                            }
                            else // when the excel column is a DateTime type
                            {
                                registerApprovalStatus.RecordedDate = DateTime.FromOADate(Convert.ToDouble(sheet.GetRow(row).GetCell(3).StringCellValue.ToString().Trim()));
                            }
                        }

                        result = db.SingleOrDefault<Result>("Register/usp_UpdateCustom_TB_M_Facility_ApprovalStatus", registerApprovalStatus.MapFromModel(user));
                        //}
                        //else
                        //{                                
                        //    throw new Exception("Immediate duplicate rows of " + registerApprovalStatus.TaxFacNo);
                        //}
                        #endregion

                        if (!result.ResultCode)
                        {
                            throw new Exception(result.ResultDesc);
                        }
                        else
                        {
                            ExceptionHandler.LogToSync(row - 2, pipedString, Path.GetFileName(path), ExceptionHandler.GENERAL_SUCCESS_MESSAGE, path,MessageType.INF);
                            rowSuccess++;
                        }



                    }
                    catch (Exception e)
                    {
                        e.LogToSync(row - 2, pipedString, Path.GetFileName(path), path, MessageType.ERR);
                        rowFail++;
                    }
                }

                db.CommitTransaction();
                result.ResultCode = true;
                result.ResultDesc = Formatting.UPLOAD_NOTIFICATION.Replace("@success", (rowSuccess).ToString()).Replace("@error", rowFail.ToString());
            }
            catch (Exception e)
            {
                db.AbortTransaction();

                result.ResultCode = false;
                result.ResultDesc = e.LogToSync(row - 2, pipedString, Path.GetFileName(path), path, MessageType.ERR).Message;
            }

            db.Close();
            return result;
        }

        public Result ExcelTaxFacilityData(string path, string user, string DivisionName, bool isFullAccess)
        {
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = Formatting.UPLOAD_NOTIFICATION
            };

            FileStream file = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite);

            //HSSFWorkbook hssfwb = new HSSFWorkbook(file);
            IWorkbook hssfwb = NPOI.SS.UserModel.WorkbookFactory.Create(file);

            ISheet sheetSKB = hssfwb.GetSheet("SKB");
            ISheet sheetCOD = hssfwb.GetSheet("COD");

            string pipedString = string.Empty;
            string tempString = String.Empty;
            StringBuilder builder = new StringBuilder();

            int row = 0;
            int rowFail = 0;
            int rowSuccess = 0;

            try
            {
                file.Close();
                RegisterApprovalStatusViewModel registerApprovalStatus = new RegisterApprovalStatusViewModel();

                RegisterCreateUpdate registerCreateUpdate = new RegisterCreateUpdate();

                var cellNumSKB = 15; // 5 is the cell amount in one row
                var cellNumCOD = 17;

                List<int> exceptionCellNumSKB = new List<int>();
                exceptionCellNumSKB.Add(10);
                exceptionCellNumSKB.Add(11);
                exceptionCellNumSKB.Add(12);
                exceptionCellNumSKB.Add(13);
                exceptionCellNumSKB.Add(14);


                List<int> exceptionCellNumCOD = new List<int>();
                exceptionCellNumCOD.Add(12);
                exceptionCellNumCOD.Add(13);
                exceptionCellNumCOD.Add(14);
                exceptionCellNumCOD.Add(15);
                exceptionCellNumCOD.Add(16);

                db.BeginTransaction();

                if (sheetSKB.LastRowNum < 3)
                {
                    throw new Exception("mandatory field cannot be empty");
                }

                string parTaxFacNo, parSKBType, parSKBTypeOthers, parTaxFacDate
                           , parSupplierName, parSupplierNPWP, parSupplierAddress, parSupplierPhoneNo, parSupplierCountry, parSupplierEstDate
                           , parFacValidPeriodFrom, parFacValidPeriodTo, parFacDocSignerName, parFacDocSignerTitle
                           , parRecordedDate, parRecordedBy, parApprovalStatus, parApprovalDate, parUploadFile, parRegistrationNumber, parPDFFacStatus, parSeq, parMAPCode;

                for (row = 3; row <= sheetSKB.LastRowNum; row++) // Data row start at row 5, index 4 on excel template
                {
                    #region Set Cell Policy, Cell Type and Save Sync Data
                    pipedString = string.Empty;
                    for (int i = 0; i < cellNumSKB; i++)
                    {
                        try
                        {
                            if (sheetSKB.GetRow(row).GetCell(i).CellType.ToString() == "NUMERIC")
                            {
                                sheetSKB.GetRow(row).GetCell(i).SetCellType(CellType.STRING);
                            }
                            tempString = sheetSKB.GetRow(row).GetCell(i).StringCellValue.ToString().Trim();

                            ////validate mandatory field
                            //if (tempString == "" && i < 10)
                            //    throw new Exception("Mandatory field cannot be empty");

                            pipedString += tempString + "|";
                        }
                        catch
                        {
                            break;
                            // System.Environment.Exit(0);
                        }
                    }
                    try
                    {
                        pipedString = pipedString.Remove(pipedString.Length - 1);
                        if (pipedString == "")
                        {
                            throw new Exception("mandatory field cannot be empty");
                        }
                    }
                    catch (Exception)
                    {

                        break;
                    }
                    #endregion

                    try
                    {
                        //if (GlobalFunction.BlankCellCheck(sheetSKB.GetRow(row), cellNumSKB, exceptionCellNumSKB))
                        //{
                        #region mandatory validation if empty
                        int Emptycount = 0;
                        
                        parTaxFacNo = sheetSKB.GetRow(row).GetCell(1).StringCellValue.ToString().Trim();
                        parSKBType = sheetSKB.GetRow(row).GetCell(0).StringCellValue.ToString().Trim();
                        parTaxFacDate = sheetSKB.GetRow(row).GetCell(2).StringCellValue.ToString().Trim();
                        parSupplierName = sheetSKB.GetRow(row).GetCell(3).StringCellValue.ToString().Trim();
                        parSupplierNPWP = sheetSKB.GetRow(row).GetCell(4).StringCellValue.ToString().Trim();
                        parSupplierAddress = sheetSKB.GetRow(row).GetCell(5).StringCellValue.ToString().Trim();
                        parSupplierPhoneNo = sheetSKB.GetRow(row).GetCell(6).StringCellValue.ToString().Trim();
                        parFacValidPeriodFrom = sheetSKB.GetRow(row).GetCell(7).StringCellValue.ToString().Trim();
                        parFacValidPeriodTo = sheetSKB.GetRow(row).GetCell(8).StringCellValue.ToString().Trim();
                        parMAPCode = sheetSKB.GetRow(row).GetCell(9).StringCellValue.ToString().Trim();

                        builder.Append("Excel Row : " + Convert.ToString((row-3) + 1) + "  mandatory field(");
                        if (string.IsNullOrEmpty(parTaxFacNo))
                        {
                            Emptycount += 1;
                            builder.Append("TaxFacNo,");
                        }
                        if (string.IsNullOrEmpty(parSKBType))
                        {
                            Emptycount += 1;
                            builder.Append("SKBType,");
                        }
                        if (string.IsNullOrEmpty(parTaxFacDate))
                        {
                            Emptycount += 1;
                            builder.Append("TaxFacDate,");
                        }
                        if (string.IsNullOrEmpty(parSupplierName))
                        {
                            Emptycount += 1;
                            builder.Append("SupplierName,");
                        }
                        if (string.IsNullOrEmpty(parSupplierNPWP))
                        {
                            Emptycount += 1;
                            builder.Append("SupplierNPWP,");
                        }
                        if (string.IsNullOrEmpty(parSupplierAddress))
                        {
                            Emptycount += 1;
                            builder.Append("SupplierAddress,");
                        }
                        if (string.IsNullOrEmpty(parSupplierPhoneNo))
                        {
                            Emptycount += 1;
                            builder.Append("SupplierPhoneNo,");
                        }
                        if (string.IsNullOrEmpty(parFacValidPeriodFrom))
                        {
                            Emptycount += 1;
                            builder.Append("FacValidPeriodFrom,");
                        }
                        if (string.IsNullOrEmpty(parFacValidPeriodTo))
                        {
                            Emptycount += 1;
                            builder.Append("FacValidPeriodTo,");
                        }
                        if (string.IsNullOrEmpty(parMAPCode))
                        {
                            Emptycount += 1;
                            builder.Append("MapCode,");
                        }

                        if (Emptycount > 0)
                        {
                            builder.Append(")mandatory field cannot be empty");
                            throw new Exception(builder.ToString());
                        }

                        #endregion

                        #region validate valid format date

                        builder.Clear();
                        if (!parTaxFacDate.Contains('/') || !parFacValidPeriodFrom.Contains('/') || !parFacValidPeriodTo.Contains('/') || 
                            (!string.IsNullOrEmpty(sheetSKB.GetRow(row).GetCell(11).StringCellValue.ToString().Trim()) && !sheetSKB.GetRow(row).GetCell(11).StringCellValue.ToString().Trim().Contains('/'))
                            || (!string.IsNullOrEmpty(sheetSKB.GetRow(row).GetCell(14).StringCellValue.ToString().Trim()) && !sheetSKB.GetRow(row).GetCell(14).StringCellValue.ToString().Trim().Contains('/'))
                            )
                        {
                            throw new Exception("Format date is not valid");
                        }

                        builder.Clear();
                        int dateNotValid = 0;
                        builder.Append("Excel Row : " + Convert.ToString((row - 3) + 1) + "  date field (");
                        string[] cekdate = parTaxFacDate.Split('/');
                        if (Convert.ToInt32(cekdate[0]) > 31)
                        {
                            dateNotValid += 1;
                            builder.Append("TaxFacDate,");
                        }
                        if (Convert.ToInt32(cekdate[1]) > 12)
                        {
                            dateNotValid += 1;
                            builder.Append("TaxFacDate,");
                        }

                        cekdate = parFacValidPeriodFrom.Split('/');
                        if (Convert.ToInt32(cekdate[0]) > 31)
                        {
                            dateNotValid += 1;
                            builder.Append("TaxFacPeriodFrom,");
                        }
                        if (Convert.ToInt32(cekdate[1]) > 12)
                        {
                            dateNotValid += 1;
                            builder.Append("TaxFacPeriodFrom,");
                        }

                        cekdate = parFacValidPeriodTo.Split('/');
                        if (Convert.ToInt32(cekdate[0]) > 31)
                        {
                            dateNotValid += 1;
                            builder.Append("TaxFacPeriodTo,");
                        }
                        if (Convert.ToInt32(cekdate[1]) > 12)
                        {
                            dateNotValid += 1;
                            builder.Append("TaxFacPeriodTo,");
                        }

                        if (dateNotValid > 0)
                        {
                            builder.Append(") not valid");
                            throw new Exception(builder.ToString());
                        }
                        #endregion

                        var regModel = new RegisterDashboardSearchParamViewModel();
                        regModel.TaxFacNo = sheetSKB.GetRow(row).GetCell(1).StringCellValue.ToString().Trim();
                        var _taxFacilityData = GetTaxFacilityByNo(regModel, DivisionName, user, isFullAccess);
                        var maxSeq = _taxFacilityData.Count() > 0 ? _taxFacilityData.Max(a => a.Seq) : 1;

                        var SKBType = string.Empty;
                        var SKBTypeOthers = string.Empty;

                        var arraySKB = sheetSKB.GetRow(row).GetCell(0).StringCellValue.ToString().Trim().Split(':');

                        SKBType = arraySKB[0].Trim();
                        //SKBTypeOthers = arraySKB.Count() > 0 ? arraySKB[1].Trim() : string.Empty;
                        SKBTypeOthers = arraySKB.Count() > 1 ? arraySKB[1].Trim() : string.Empty;

                        _taxFacilityData = _taxFacilityData.Where(a => a.Seq.Equals(maxSeq)).ToList();

                        var searchParams = new RegisterGeneralParamSearchViewModel();
                        searchParams.ParamType = "SKBType";
                        searchParams.ParamName = SKBType;
                        
                        //GetListParam(searchParams);
                            
                        registerCreateUpdate = new RegisterCreateUpdate()
                        {
                            TaxFacNo = parTaxFacNo,
                            SKBType = SKBType,
                            SKBTypeOthers = SKBTypeOthers,
                            TaxFacDate = parTaxFacDate,
                            SupplierName = parSupplierName,
                            SupplierNPWP = parSupplierNPWP.FormatNPWP(),
                            SupplierAddress = parSupplierAddress,
                            SupplierPhoneNo = sheetSKB.GetRow(row).GetCell(6).StringCellValue.ToString().Trim(),
                            FacValidPeriodFrom = parFacValidPeriodFrom,
                            FacValidPeriodTo = parFacValidPeriodTo,
                            MapCode = parMAPCode,
                            RegistrationNo = sheetSKB.GetRow(row).GetCell(10).StringCellValue.ToString().Trim(),
                            RecordedBy = sheetSKB.GetRow(row).GetCell(12).StringCellValue.ToString().Trim(),
                            ApprovalStatus = sheetSKB.GetRow(row).GetCell(13).StringCellValue.ToString().Trim(),
                            ApprovalDate = sheetSKB.GetRow(row).GetCell(14).StringCellValue.ToString().Trim(),
                            Seq = maxSeq == 0 ? 1 : (maxSeq + 1)
                        };

                        // if data approved / rejected continue
                        if (_taxFacilityData != null && _taxFacilityData.Select(a => a.ApprovalStatus).Equals("Approved"))
                        {
                            continue;
                        }

                        // if data avail wait and want approve/reject from import
                        #region Cell Mapping & Insert to DB
                        if (_taxFacilityData != null && _taxFacilityData.Select(a => a.ApprovalStatus).Equals("Wait For Approval"))
                        {

                            registerApprovalStatus = new RegisterApprovalStatusViewModel();
                            registerApprovalStatus.TaxFacNo = sheetSKB.GetRow(row).GetCell(1).StringCellValue.ToString().Trim();
                            registerApprovalStatus.ApprovalStatus = sheetSKB.GetRow(row).GetCell(13).StringCellValue.ToString().Trim();
                            registerApprovalStatus.ApprovalDate = DateTime.ParseExact(sheetSKB.GetRow(row).GetCell(15).StringCellValue.ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture).Add(new TimeSpan());
                            registerApprovalStatus.RegistrationNo = sheetSKB.GetRow(row).GetCell(10).StringCellValue.ToString().Trim();

                            if (string.IsNullOrEmpty(registerApprovalStatus.ApprovalDate.ToString())
                                && string.IsNullOrEmpty(registerApprovalStatus.TaxFacNo)
                                && string.IsNullOrEmpty(registerApprovalStatus.ApprovalStatus)
                                && string.IsNullOrEmpty(registerApprovalStatus.RegistrationNo))
                            {
                                throw new Exception(ExceptionHandler.MANDATORY_FAIL_MESSAGE);
                            }

                            result = db.SingleOrDefault<Result>("Register/usp_UpdateCustom_TB_M_Facility_ApprovalStatus", registerApprovalStatus.MapFromModel(user));
                        }
                        // if data avail reject and want add new
                        else if (_taxFacilityData != null && _taxFacilityData.Select(a => a.ApprovalStatus).Equals("Reject"))
                        {
                            result = InsertUploadSKB(registerCreateUpdate, user, DateTime.Now);
                        }
                        else
                        {
                            result = InsertUploadSKB(registerCreateUpdate, user, DateTime.Now);
                        }
                        //else
                        //{                                
                        //    throw new Exception("Immediate duplicate rows of " + registerApprovalStatus.TaxFacNo);
                        //}
                        #endregion

                        if (!result.ResultCode)
                        {
                            throw new Exception(result.ResultDesc);
                        }
                        else
                        {
                            ExceptionHandler.LogToSync((row - 3) + 1, pipedString, Path.GetFileName(path), ExceptionHandler.GENERAL_SUCCESS_MESSAGE,path, MessageType.INF);
                            rowSuccess++;
                        }
                        //}
                        //else
                        //{
                        //    throw new Exception(ExceptionHandler.MANDATORY_FAIL_MESSAGE);
                        //}
                    }
                    catch (Exception e)
                    {
                        e.LogToSync((row - 3) + 1, pipedString, Path.GetFileName(path), path, MessageType.ERR);
                        rowFail++;
                    }
                }

                pipedString = string.Empty;
                builder.Clear();

                //row = 0;
                //rowFail = 0;
                //rowSuccess = 0;

                if (sheetCOD.LastRowNum < 3)
                {
                    throw new Exception("mandatory field cannot be empty");
                }

                for (row = 3; row <= sheetCOD.LastRowNum; row++) // Data row start at row 5, index 4 on excel template
                {
                    try
                    {
                        #region Set Cell Policy, Cell Type and Save Sync Data
                        pipedString = string.Empty;
                        for (int i = 0; i < cellNumCOD; i++)
                        {
                            //sheetCOD.GetRow(row).GetCell(i, MissingCellPolicy.CREATE_NULL_AS_BLANK);
                            //sheetCOD.GetRow(row).GetCell(i).SetCellType(CellType.STRING);

                            //string abd = sheetCOD.GetRow(row).GetCell(i).CellType.ToString();
                            if (sheetCOD.GetRow(row).GetCell(i).CellType.ToString() == "NUMERIC")
                            {
                                sheetCOD.GetRow(row).GetCell(i).SetCellType(CellType.STRING);
                            }
                            tempString = sheetCOD.GetRow(row).GetCell(i).StringCellValue.ToString().Trim();

                            //validate mandatory field
                            //if (tempString == "" && i < 10)
                            //    throw new Exception("Mandatory field cannot be empty");

                            pipedString += tempString + "|";
                        }
                        pipedString = pipedString.Remove(pipedString.Length - 1);
                        if (pipedString == "")
                        {
                            throw new Exception("mandatory field cannot be empty");
                        }
                        #endregion

                        //if (GlobalFunction.BlankCellCheck(sheetCOD.GetRow(row), cellNumCOD, exceptionCellNumCOD))
                        //{
                            #region mandatory validation if empty
                            int Emptycount = 0;

                            parTaxFacNo = sheetCOD.GetRow(row).GetCell(0).StringCellValue.ToString().Trim();
                            parTaxFacDate = sheetCOD.GetRow(row).GetCell(1).StringCellValue.ToString().Trim();
                            parSupplierName = sheetCOD.GetRow(row).GetCell(2).StringCellValue.ToString().Trim();
                            parSupplierNPWP = sheetCOD.GetRow(row).GetCell(3).StringCellValue.ToString().Trim();
                            parSupplierAddress = sheetCOD.GetRow(row).GetCell(4).StringCellValue.ToString().Trim();
                            parSupplierPhoneNo = sheetCOD.GetRow(row).GetCell(5).StringCellValue.ToString().Trim();
                            parSupplierCountry = sheetCOD.GetRow(row).GetCell(6).StringCellValue.ToString().Trim();
                            parSupplierEstDate = sheetCOD.GetRow(row).GetCell(7).StringCellValue.ToString().Trim();
                            parFacValidPeriodFrom = sheetCOD.GetRow(row).GetCell(8).StringCellValue.ToString().Trim();
                            parFacValidPeriodTo = sheetCOD.GetRow(row).GetCell(9).StringCellValue.ToString().Trim();
                            parFacDocSignerName = sheetCOD.GetRow(row).GetCell(10).StringCellValue.ToString().Trim();
                            parFacDocSignerTitle = sheetCOD.GetRow(row).GetCell(11).StringCellValue.ToString().Trim();

                            builder.Append("Excel Row : " + Convert.ToString((row-3) + 1) + "  mandatory field(");
                            if (string.IsNullOrEmpty(parTaxFacNo))
                            {
                                Emptycount += 1;
                                builder.Append("TaxFacNo,");
                            }
                            if (string.IsNullOrEmpty(parTaxFacDate))
                            {
                                Emptycount += 1;
                                builder.Append("TaxFacDate,");
                            }
                            if (string.IsNullOrEmpty(parSupplierName))
                            {
                                Emptycount += 1;
                                builder.Append("SupplierName,");
                            }
                            if (string.IsNullOrEmpty(parSupplierNPWP))
                            {
                                Emptycount += 1;
                                builder.Append("SupplierNPWP,");
                            }
                            if (string.IsNullOrEmpty(parSupplierAddress))
                            {
                                Emptycount += 1;
                                builder.Append("SupplierAddress,");
                            }
                            if (string.IsNullOrEmpty(parSupplierPhoneNo))
                            {
                                Emptycount += 1;
                                builder.Append("SupplierPhoneNo,");
                            }
                            if (string.IsNullOrEmpty(parSupplierCountry))
                            {
                                Emptycount += 1;
                                builder.Append("SupplierCountry,");
                            }
                            if (string.IsNullOrEmpty(parSupplierEstDate))
                            {
                                Emptycount += 1;
                                builder.Append("SupplierEstDate,");
                            }
                            if (string.IsNullOrEmpty(parFacValidPeriodFrom))
                            {
                                Emptycount += 1;
                                builder.Append("FacValidPeriodFrom,");
                            }
                            if (string.IsNullOrEmpty(parFacValidPeriodTo))
                            {
                                Emptycount += 1;
                                builder.Append("FacValidPeriodTo,");
                            }
                            if (string.IsNullOrEmpty(parFacDocSignerName))
                            {
                                Emptycount += 1;
                                builder.Append("FacDocSignerName,");
                            }
                            if (string.IsNullOrEmpty(parFacDocSignerTitle))
                            {
                                Emptycount += 1;
                                builder.Append("FacDocSignerTitle,");
                            }
                            
                            if (Emptycount > 0)
                            {
                                builder.Append(")mandatory field cannot be empty");
                                throw new Exception(builder.ToString());
                            }
                            #endregion

                            #region validate valid format date

                            builder.Clear();
                            
                            if (!parTaxFacDate.Contains('/') || !parSupplierEstDate.Contains('/') || !parFacValidPeriodFrom.Contains('/') || !parFacValidPeriodTo.Contains('/') ||
                            (!string.IsNullOrEmpty(sheetCOD.GetRow(row).GetCell(13).StringCellValue.ToString().Trim()) && !sheetCOD.GetRow(row).GetCell(13).StringCellValue.ToString().Trim().Contains('/'))
                            || (!string.IsNullOrEmpty(sheetCOD.GetRow(row).GetCell(16).StringCellValue.ToString().Trim()) && !sheetCOD.GetRow(row).GetCell(16).StringCellValue.ToString().Trim().Contains('/'))
                            )
                            {
                                builder.Clear();
                                builder.Append("Format date is not valid");
                                throw new Exception(builder.ToString());
                            }

                            builder.Clear();
                            int dateNotValid = 0;
                            builder.Append("Excel Row : " + Convert.ToString((row - 3) + 1) + "  date field (");

                            string[] cekdate = parTaxFacDate.Split('/');
                            if (Convert.ToInt32(cekdate[0]) > 31)
                            {
                                dateNotValid += 1;
                                builder.Append("TaxFacDate,");
                            }
                            if (Convert.ToInt32(cekdate[1]) > 12)
                            {
                                dateNotValid += 1;
                                builder.Append("TaxFacDate,");
                            }

                            cekdate = parSupplierEstDate.Split('/');
                            if (Convert.ToInt32(cekdate[0]) > 31)
                            {
                                dateNotValid += 1;
                                builder.Append("SupplierEstDate,");
                            }
                            if (Convert.ToInt32(cekdate[1]) > 12)
                            {
                                dateNotValid += 1;
                                builder.Append("SupplierEstDate,");
                            }

                            cekdate = parFacValidPeriodFrom.Split('/');
                            if (Convert.ToInt32(cekdate[0]) > 31)
                            {
                                dateNotValid += 1;
                                builder.Append("FacValidPeriodFrom,");
                            }
                            if (Convert.ToInt32(cekdate[1]) > 12)
                            {
                                dateNotValid += 1;
                                builder.Append("FacValidPeriodFrom,");
                            }

                            cekdate = parFacValidPeriodTo.Split('/');
                            if (Convert.ToInt32(cekdate[0]) > 31)
                            {
                                dateNotValid += 1;
                                builder.Append("FacValidPeriodTo,");
                            }
                            if (Convert.ToInt32(cekdate[1]) > 12)
                            {
                                dateNotValid += 1;
                                builder.Append("FacValidPeriodTo,");
                            }

                            if (dateNotValid > 0)
                            {
                                builder.Append(") not valid");
                                throw new Exception(builder.ToString());
                            }
                                                        
                            #endregion
                            var regModel = new RegisterDashboardSearchParamViewModel();
                            regModel.TaxFacNo = sheetCOD.GetRow(row).GetCell(0).StringCellValue.ToString().Trim();
                            var _taxFacilityData = GetTaxFacilityByNo(regModel, DivisionName, user, isFullAccess);
                            var maxSeq = _taxFacilityData.Count() > 0 ? _taxFacilityData.Max(a => a.Seq) : 1;

                            var searchParams = new RegisterGeneralParamSearchViewModel();
                            searchParams.ParamType = "Country";
                            searchParams.ParamValue = parSupplierCountry;

                            var _country = parSupplierCountry;

                            registerCreateUpdate = new RegisterCreateUpdate()
                            {
                                TaxFacNo = parTaxFacNo,
                                TaxFacDate = parTaxFacDate,
                                SupplierName = parSupplierName,
                                SupplierNPWP = parSupplierNPWP,
                                SupplierAddress = parSupplierAddress,
                                SupplierPhoneNo = parSupplierPhoneNo,
                                SupplierCountry = _country,
                                SupplierEstDate = parSupplierEstDate,
                                FacValidPeriodFrom = parFacValidPeriodFrom,
                                FacValidPeriodTo = parFacValidPeriodTo,
                                FacDocSignerName = parFacDocSignerName,
                                FacDocSignerTitle = parFacDocSignerTitle,
                                RegistrationNo = sheetCOD.GetRow(row).GetCell(12).StringCellValue.ToString().Trim(),
                                RecordedBy = sheetCOD.GetRow(row).GetCell(14).StringCellValue.ToString().Trim(),
                                ApprovalStatus = sheetCOD.GetRow(row).GetCell(15).StringCellValue.ToString().Trim(),
                                ApprovalDate = sheetCOD.GetRow(row).GetCell(16).StringCellValue.ToString().Trim(),
                                Seq = maxSeq == 0 ? 1 : (maxSeq + 1)
                            };
                            // if data approved / rejected continue
                            if (_taxFacilityData != null && _taxFacilityData.Select(a => a.ApprovalStatus).Equals("Approved"))
                            {
                                continue;
                            }

                            // if data wait
                            #region Cell Mapping & Insert to DB
                            if (_taxFacilityData != null && _taxFacilityData.Select(a => a.ApprovalStatus).Equals("Wait For Approval"))
                            {

                                registerApprovalStatus = new RegisterApprovalStatusViewModel();
                                registerApprovalStatus.TaxFacNo = sheetCOD.GetRow(row).GetCell(0).StringCellValue.ToString().Trim();
                                registerApprovalStatus.ApprovalStatus = sheetCOD.GetRow(row).GetCell(15).StringCellValue.ToString().Trim();
                                registerApprovalStatus.ApprovalDate = DateTime.ParseExact(sheetCOD.GetRow(row).GetCell(16).StringCellValue.ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture).Add(new TimeSpan());
                                registerApprovalStatus.RegistrationNo = sheetCOD.GetRow(row).GetCell(12).StringCellValue.ToString().Trim();

                                if (string.IsNullOrEmpty(registerApprovalStatus.ApprovalDate.ToString())
                                    && string.IsNullOrEmpty(registerApprovalStatus.TaxFacNo)
                                    && string.IsNullOrEmpty(registerApprovalStatus.ApprovalStatus)
                                    && string.IsNullOrEmpty(registerApprovalStatus.RegistrationNo))
                                {
                                    throw new Exception(ExceptionHandler.MANDATORY_FAIL_MESSAGE);
                                }

                                result = db.SingleOrDefault<Result>("Register/usp_UpdateCustom_TB_M_Facility_ApprovalStatus", registerApprovalStatus.MapFromModel(user));
                            }

                            else if (_taxFacilityData != null && _taxFacilityData.Select(a => a.ApprovalStatus).Equals("Reject"))
                            {
                                result = InsertUploadCOD(registerCreateUpdate, user, DateTime.Now);
                            }
                            else
                            {
                                result = InsertUploadCOD(registerCreateUpdate, user, DateTime.Now);
                            }
                            //else
                            //{                                
                            //    throw new Exception("Immediate duplicate rows of " + registerApprovalStatus.TaxFacNo);
                            //}
                            #endregion

                            if (!result.ResultCode)
                            {
                                throw new Exception(result.ResultDesc);
                            }
                            else
                            {
                                ExceptionHandler.LogToSync((row - 3) + 1, pipedString, Path.GetFileName(path), ExceptionHandler.GENERAL_SUCCESS_MESSAGE, path,MessageType.INF);
                                rowSuccess++;
                            }
                        //}
                        //else
                        //{
                        //    throw new Exception(ExceptionHandler.MANDATORY_FAIL_MESSAGE);
                        //}
                    }
                    catch (Exception e)
                    {
                        e.LogToSync((row-3) + 1, pipedString, Path.GetFileName(path), path, MessageType.ERR);
                        rowFail++;
                    }
                }

                db.CommitTransaction();
                result.ResultCode = true;
                result.ResultDesc = Formatting.UPLOAD_NOTIFICATION.Replace("@success", (rowSuccess).ToString()).Replace("@error", rowFail.ToString());
            }
            catch (Exception e)
            {
                db.AbortTransaction();

                result.ResultCode = false;
                result.ResultDesc = e.LogToSync(row + 1, pipedString, Path.GetFileName(path), path, MessageType.ERR).Message;
            }

            db.Close();
            return result;
        }

    }
}