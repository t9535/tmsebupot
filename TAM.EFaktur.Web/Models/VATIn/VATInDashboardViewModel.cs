﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TAM.EFaktur.Web.Models.VATIn
{
    public class VATInDashboardViewModel : Controller
    {
        public int RowNum { get; set; }
        public Guid Id { get; set; }
        public string NomorFaktur { get; set; }
        public string NPWPPenjual { get; set; }
        public string NamaPenjual { get; set; }
        public decimal JumlahDPP { get; set; }
        public decimal JumlahPPN { get; set; }
        public decimal JumlahPPnBM { get; set; }
        public DateTime TanggalFaktur { get; set; }
        public string Perekam { get; set; }
        public Nullable<DateTime> SyncTime { get; set; }
        public string TransitoryStatus { get; set; }
        public DateTime TransitoryDate { get; set; }
        public DateTime RecordTime { get; set; }
        public string RecordStatus { get; set; }
        public string DownloadStatus { get; set; }
        public Nullable<DateTime> CsvTime { get; set; }
        public string ApprovalStatus { get; set; }
        public DateTime ApprovalTime { get; set; }
        public string BatchFileName { get; set; }
        public string FakturType { get; set; }
        public string DaysExpired { get; set; }
        public string NomorInvoice { get; set; }
        public string StatusInvoice { get; set; }
        public string CreatedByPV { get; set; }
        public string SAPDocNumber { get; set; }
        public string StatusEPROC { get; set; }
        public string FileUrl { get; set; }
        public string PVNumber { get; set; }
        public string TransitoryNumber { get; set; }
        public string ApprovalDescription { get; set; }
        public DateTime PostingDate { get; set; }
        public string  TAXPeriodYear { get; set; }
        public string TAXPeriodMonth { get; set; }
        public string RelatedGLAccount { get; set; }
        public string CreatedOn { get; set; }
        public string Remark { get; set; }

    }
}