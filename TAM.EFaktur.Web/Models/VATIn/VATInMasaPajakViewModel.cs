﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.VATIn
{
    public class VATInMasaPajakViewModel
    {
        public string NomorFaktur { get; set; }
        public string Bulan { get; set; }
        public string Tahun { get; set; }

        public dynamic MapFromModel(DateTime EventDate, string EventActor)
        {
            return new
            {
                NomorFaktur = NomorFaktur,
                Bulan = Bulan,
                Tahun = Tahun,
                EventDate = EventDate,
                EventActor = EventActor
            };
        }
    }
}