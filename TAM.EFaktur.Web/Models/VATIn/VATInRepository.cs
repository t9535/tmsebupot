﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TAM.EFaktur.Web.Models.Log_Sync;
using TAM.EFaktur.Web.Models.Master_Config;
using TAM.EFaktur.Web.Models.SyncData;

namespace TAM.EFaktur.Web.Models.VATIn
{
    public class VATInRepository : BaseRepository
    {
        #region Singleton
        private VATInRepository() { }
        private static VATInRepository instance = null;
        public static VATInRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new VATInRepository();
                }
                return instance;
            }
        }
        #endregion

        #region Counter Data
        public int Count(VATInDashboardSearchParamViewModel model, string DivisionName, string Username, bool isFullAccess)
        {
            try
            {
                return db.SingleOrDefault<int>("VATIn/usp_CountVATInListDashboard", model.MapFromModel(isFullAccess,DivisionName, Username));
            }
            catch
            {
                throw;
            }

        }
        #endregion

        #region Processing Data
        public List<VATInDashboardViewModel> GetList(VATInDashboardSearchParamViewModel model, string DivisionName, string Username, int SortBy, string SortDirection, int FromNumber, int ToNumber, bool isFullAccess)
        {
            IEnumerable<VATInDashboardViewModel> result = db.Query<VATInDashboardViewModel>("VATIn/usp_GetVATInListDashboard", model.MapFromModel(isFullAccess, DivisionName, Username, SortBy, SortDirection, FromNumber, ToNumber));
            return result.ToList();
        }

        public VATIn GetById(Guid Id)
        {
            return db.SingleOrDefault<VATIn>("VATIn/usp_GetVATInByID", new { Id = Id });
        }

        public List<Guid> GetIdBySearchParam(VATInDashboardSearchParamViewModel model, string DivisionName, string Username, bool isFullAccess)
        {
            IEnumerable<Guid> result = db.Query<Guid>("VATIn/usp_GetVATInIdBySearchParam", model.MapFromModel(isFullAccess,DivisionName, Username));
            return result.ToList();
        }

        //Get CSV
        public List<VATInCSVViewModel> GetCSVById(List<Guid> VATInIdList)
        {
            dynamic args = new
            {
                VATInIdList = string.Join(";", VATInIdList)
            };
            IEnumerable<VATInCSVViewModel> result = db.Query<VATInCSVViewModel>("VATIn/usp_GetVATInCSVById", args);
            return result.ToList();
        }

        public List<VATInCSVViewModel> GetCSVnonEFakturById(List<Guid> VATInIdList)
        {
            dynamic args = new
            {
                VATInIdList = string.Join(";", VATInIdList)
            };
            IEnumerable<VATInCSVViewModel> result = db.Query<VATInCSVViewModel>("VATIn/usp_GetVATInCSVnonEFakturById", args);
            return result.ToList();
        }

        public List<VATInCSVViewModel> GetCSVnonEFakturOtherById(List<Guid> VATInIdList)
        {
            dynamic args = new
            {
                VATInIdList = string.Join(";", VATInIdList)
            };
            IEnumerable<VATInCSVViewModel> result = db.Query<VATInCSVViewModel>("VATIn/usp_GetVATInCSVnonEFakturOtherById", args);
            return result.ToList();
        }

        //Get Original File
        public List<VATInOriginalViewModel> GetXlsById(List<Guid> VATInIdList)
        {
            dynamic args = new
            {
                VATInIdList = string.Join(";", VATInIdList)
            };
            IEnumerable<VATInOriginalViewModel> result = db.Query<VATInOriginalViewModel>("VATIn/usp_GetVATInOriginalFileById", args);
            return result.ToList();
        }

        //Get Report Excel
        public List<VATInReportExcelViewModel> GetReportExcelById(List<Guid> VATInIdList)
        {
            dynamic args = new
            {
                VATInIdList = string.Join(";", VATInIdList)
            };
            IEnumerable<VATInReportExcelViewModel> result = db.Query<VATInReportExcelViewModel>("VATIn/usp_GetVATInReportFileById", args);
            return result.ToList();
        }

        //Update flags
        public Result UpdateFlagCSV(List<Guid> VATInIdList, string FileUrl, string EventActor)
        {
            try
            {
                db.BeginTransaction();
                dynamic args = new
                {
                    VATInIdList = string.Join(";", VATInIdList),
                    BatchFileName = Path.GetFileName(@FileUrl)
                };
                db.Execute("VATIn/usp_UpdateCustom_TB_R_VATIn_FlagCSV", args);

                args = new
                {
                    Id = Guid.NewGuid(),
                    CsvType = "FM",
                    CsvTime = DateTime.Now.FormatSQLDateTime(),
                    BatchFileName = Path.GetFileName(@FileUrl),
                    FileUrl = FileUrl,
                    EventDate = DateTime.Now.FormatSQLDateTime(),
                    EventActor = EventActor
                };
                db.Execute("CSVData/usp_InsertCustom_TB_R_CsvData", args);
                db.CommitTransaction();
            }
            catch (Exception e)
            {
                db.AbortTransaction();

                throw e;
            }

            return MsgResultSuccessInsert();
        }

        //Get PDF Download
        public List<string> GetPDFUrlById(List<Guid> VATInIdList)
        {
            dynamic args = new
            {
                VATInIdList = string.Join(";", VATInIdList)
            };
            IEnumerable<string> result = db.Query<string>("VATIn/usp_GetVATInPDFUrlById", args);
            return result.ToList();
        }

        public List<VATInDetailManualInputViewModel> GetDetailByVATInId(Guid VATInId)
        {
            IEnumerable<VATInDetailManualInputViewModel> result = db.Query<VATInDetailManualInputViewModel>("VATIn/usp_GetCustomVATInDetailByVATInID", new { VATInId = VATInId });
            return result.ToList();

        }

        public VATInManualInputViewModel GetByFileUrl(string FileUrl)
        {
            VATInManualInputViewModel model = new VATInManualInputViewModel();
            try
            {
                model = db.SingleOrDefault<VATInManualInputViewModel>("VATIn/usp_GetCustomVATInByFileUrl", new { FileUrl = FileUrl });
                if (model != null)
                {
                    model.VATInDetails = GetDetailByVATInId(model.Id);
                    model.InvoiceDate = model.InvoiceDate.FormatViewDate();
                }
                else
                {
                    throw new Exception("Tax Invoice does not exist");
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            return model;
        }

        public Result ValidateVATInTransitoryStatusNotAvailable(List<Guid> VATInIdList)
        {
            try
            {
                dynamic args = new
                {
                    VATInIdList = string.Join(";", VATInIdList)
                };
                return db.SingleOrDefault<Result>("VATIn/usp_ValidateVATInTransitoryStatusNotAvailable", args);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public Result ValidateVATInTransitoryStatusUpdated(List<Guid> VATInIdList)
        {
            try
            {
                dynamic args = new
                {
                    VATInIdList = string.Join(";", VATInIdList)
                };
                return db.SingleOrDefault<Result>("VATIn/usp_ValidateVATInTransitoryStatusUpdated", args);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public Result DeleteVATInById(List<Guid> VATInIdList, string EventActor)
        {
            try
            {
                db.BeginTransaction();
                dynamic args = new
                {
                    VATInIdList = string.Join(";", VATInIdList),
                    EventDate = DateTime.Now.FormatSQLDateTime(),
                    EventActor = EventActor
                };
                db.Execute("VATIn/usp_DeleteVATIn", args);
                db.Execute("VATIn/usp_DeleteVATInDetail", args);
                db.CommitTransaction();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                throw e;

            }
            return MsgResultSuccessDelete();
        }

        public Result Insert(VATInManualInputViewModel model, string XMLUrl, string EventActor, DateTime EventDate)
        {
            try
            {
                if (model.FakturType == "eFaktur" && !string.IsNullOrEmpty(XMLUrl))
                {
                    #region Validasi VAT In khusus scan
                    //validasi npwp
                    var config = MasterConfigRepository.Instance.GetByConfigKey("CompanyNPWP");
                    var configbatam = MasterConfigRepository.Instance.GetByConfigKey("CompanyNPWPBatam");
                    if (config == null)
                    {
                        throw new Exception("Please define the CompanyNPWP Config");
                    }

                    if (configbatam == null)
                    {
                        throw new Exception("Please define the CompanyNPWPBatam Config");
                    }

                    if (!model.NPWPLawanTransaksi.Equals(config.ConfigValue) && !model.NPWPLawanTransaksi.Equals(configbatam.ConfigValue))
                    {
                        throw new Exception("Buyer NPWP '" + model.NPWPLawanTransaksi + "' does not match with Company NPWP");
                    }

                    //validasi status faktur
                    if (model.StatusFakturXML == "Faktur Dibatalkan")
                    {
                        throw new Exception("Cannot save Data with Status Faktur Dibatalkan");
                    }

                    //validasi expire date
                    string InvoiceDate = model.InvoiceDate;

                    DateTime expdate = Convert.ToDateTime(InvoiceDate).AddDays(1).AddMonths(3).AddDays(-1);//
                    DateTime endOfMonth = new DateTime(expdate.Year, expdate.Month, 1).AddMonths(1).AddDays(-1);
                    string tanggalexpired = endOfMonth.ToShortDateString();
                    int SelisihHari = (Convert.ToDateTime(tanggalexpired) - DateTime.Now.Date.AddDays(-1)).Days;//Convert.ToDateTime(tanggalexpired)).Days;
                    var NilaiSelisihHari = MasterConfigRepository.Instance.GetByConfigKey("VATInDaysExpired");

                    if (SelisihHari < Convert.ToInt32(NilaiSelisihHari.ConfigValue))
                    {
                        if (SelisihHari <= 0)
                        {
                            SelisihHari = 0;
                        }

                        throw new Exception("Tax Invoice can’t be saved. Tax Invoice EXPIRED is Already " + SelisihHari + " Days Before Expired.");
                    }

                    //validasi Faktur pengganti
                    Result validateResult = db.SingleOrDefault<Result>("VATIn/usp_ValidateVATInInvoiceNumber", new { NomorFakturGabungan = model.InvoiceNumberFull, NomorFaktur = model.InvoiceNumber, FGPengganti = model.FGPengganti, FakturType = model.FakturType, NPWPPenjual = model.SupplierNPWP, InvoiceDate = model.InvoiceDate });

                    if (!validateResult.ResultCode)
                    {
                        throw new Exception(validateResult.ResultDesc);
                    }

                    #endregion
                }
                else if (model.FakturType == "eFaktur" && string.IsNullOrEmpty(XMLUrl))
                {
                    #region Validasi VATin efaktur manual

                    string InvoiceDate = model.InvoiceDate;

                    DateTime expdate = Convert.ToDateTime(InvoiceDate).AddDays(1).AddMonths(3).AddDays(-1);//
                    DateTime endOfMonth = new DateTime(expdate.Year, expdate.Month, 1).AddMonths(1).AddDays(-1);
                    string tanggalexpired = endOfMonth.ToShortDateString();
                    int SelisihHari = (Convert.ToDateTime(tanggalexpired) - DateTime.Now.Date.AddDays(-1)).Days;//Convert.ToDateTime(tanggalexpired)).Days;
                    var NilaiSelisihHari = MasterConfigRepository.Instance.GetByConfigKey("VATInDaysExpired");

                    model.ExpireDate = tanggalexpired;

                    //validasi npwp
                    var config = MasterConfigRepository.Instance.GetByConfigKey("CompanyNPWP");
                    var configbatam = MasterConfigRepository.Instance.GetByConfigKey("CompanyNPWPBatam");
                    if (config == null)
                    {
                        throw new Exception("Please define the CompanyNPWP Config");
                    }

                    if (configbatam == null)
                    {
                        throw new Exception("Please define the CompanyNPWPBatam Config");
                    }

                    if (!model.NPWPLawanTransaksi.Equals(config.ConfigValue) && !model.NPWPLawanTransaksi.Equals(configbatam.ConfigValue))
                    {
                        throw new Exception("Buyer NPWP '" + model.NPWPLawanTransaksi + "' does not match with Company NPWP");
                    }

                    //validasi expiredate

                    if (SelisihHari < Convert.ToInt32(NilaiSelisihHari.ConfigValue))
                    {
                        if (SelisihHari <= 0)
                        {
                            SelisihHari = 0;
                        }

                        throw new Exception("Tax Invoice can’t be saved. Tax Invoice EXPIRED is Already " + SelisihHari + " Days Before Expired.");
                    }

                    //validasi faktur pengganti
                    Result validateResult = db.SingleOrDefault<Result>("VATIn/usp_ValidateVATInInvoiceNumber", new { NomorFakturGabungan = model.InvoiceNumberFull, NomorFaktur = model.InvoiceNumber, FGPengganti = model.FGPengganti, FakturType = model.FakturType, NPWPPenjual = model.SupplierNPWP, InvoiceDate = model.InvoiceDate });

                    if (!validateResult.ResultCode)
                    {
                        throw new Exception(validateResult.ResultDesc);
                    }
                    #endregion
                }
                else
                {
                    #region Validasi VATin nonEfaktur

                    string InvoiceDate = model.InvoiceDate;

                    DateTime expdate = Convert.ToDateTime(InvoiceDate).AddDays(1).AddMonths(3).AddDays(-1);//
                    DateTime endOfMonth = new DateTime(expdate.Year, expdate.Month, 1).AddMonths(1).AddDays(-1);
                    string tanggalexpired = endOfMonth.ToShortDateString();
                    int SelisihHari = (Convert.ToDateTime(tanggalexpired) - DateTime.Now.Date.AddDays(-1)).Days;//Convert.ToDateTime(tanggalexpired)).Days;
                    var NilaiSelisihHari = MasterConfigRepository.Instance.GetByConfigKey("VATInDaysExpired");

                    model.ExpireDate = tanggalexpired;

                    //validasi npwp
                    var config = MasterConfigRepository.Instance.GetByConfigKey("CompanyNPWP");
                    var configbatam = MasterConfigRepository.Instance.GetByConfigKey("CompanyNPWPBatam");
                    if (config == null)
                    {
                        throw new Exception("Please define the CompanyNPWP Config");
                    }

                    if (configbatam == null)
                    {
                        throw new Exception("Please define the CompanyNPWPBatam Config");
                    }

                    if (!model.NPWPLawanTransaksi.Equals(config.ConfigValue) && !model.NPWPLawanTransaksi.Equals(configbatam.ConfigValue))
                    {
                        throw new Exception("Buyer NPWP '" + model.NPWPLawanTransaksi + "' does not match with Company NPWP");
                    }

                    //validasi tanggal expired

                    if (SelisihHari < Convert.ToInt32(NilaiSelisihHari.ConfigValue))
                    {
                        if (SelisihHari <= 0)
                        {
                            SelisihHari = 0;
                        }

                        throw new Exception("Tax Invoice can’t be saved. Tax Invoice EXPIRED is Already " + SelisihHari + " Days Before Expired.");
                    }

                    Result validateResult = db.SingleOrDefault<Result>("VATIn/usp_ValidateVATInInvoiceNumber", new { NomorFakturGabungan = model.InvoiceNumberFull, NomorFaktur = model.InvoiceNumber, FGPengganti = model.FGPengganti, FakturType = model.FakturType, NPWPPenjual = model.SupplierNPWP, InvoiceDate = model.InvoiceDate });

                    if (!validateResult.ResultCode)
                    {
                        throw new Exception(validateResult.ResultDesc);
                    }

                    #endregion
                }
                db.BeginTransaction();
                Nullable<Guid> SyncId = null;
                string FakturType = model.FakturType;
                if (!string.IsNullOrEmpty(XMLUrl))
                {
                    FakturType = Formatting.TYPE_EFAKTUR;
                    #region Master - SyncData
                    //generate new Guid for Id - SyncData
                    SyncId = Guid.NewGuid();
                    dynamic argsSyncData = new SyncDataViewModel().CreateModel("FM", null, XMLUrl).MapFromModel(SyncId.Value, EventDate, EventActor);
                    db.Execute("SyncData/usp_Insert_TB_R_SyncData", argsSyncData);
                    #endregion

                    #region Detail - SyncDataDetail
                    List<SyncDataDetailViewModel> syncDataDetailsModel = MapSyncDataDetailsModel(model);

                    foreach (SyncDataDetailViewModel syncDataDetailModel in syncDataDetailsModel)
                    {
                        dynamic argsSyncDataDetail = syncDataDetailModel.MapFromModel(SyncId.Value, EventDate, EventActor);
                        db.Execute("SyncData/usp_Insert_TB_R_SyncDataDetail", argsSyncDataDetail);
                    }
                    #endregion
                }
                #region Master - VATIn
                //generate new Guid for Id - VATIn
                Guid VATInID = Guid.NewGuid();
               // model.ExpireDate.Trim().Split('/');
                DateTime _ExpireDate = Convert.ToDateTime(model.ExpireDate.Trim());

             //   DateTime dataExpireDate =Convert.ToDateTime(_ExpireDate[2] + "-" + _ExpireDate[1] + "-" + _ExpireDate[0]);
                
                dynamic args = new
                {
                    Id = VATInID,
                    SyncId = SyncId,
                    InvoiceNumber = model.InvoiceNumber,
                    InvoiceNumberFull = model.InvoiceNumberFull,
                    KDJenisTransaksi = model.KDJenisTransaksi,
                    FGPengganti = model.FGPengganti,
                    InvoiceDate = model.InvoiceDate,
                   // ExpireDate = model.ExpireDate,
                     ExpireDate = _ExpireDate,
                    SupplierNPWP = model.SupplierNPWP,
                    SupplierName = model.SupplierName,
                    SupplierAddress = model.SupplierAddress,
                    //NPWPLawanTransaksi  = model.NPWPLawanTransaksi,
                    StatusApprovalXML = model.StatusApprovalXML,
                    StatusFakturXML = model.StatusFakturXML,
                    FakturType = FakturType,
                    VATBaseAmount = model.VATBaseAmount,
                    VATAmount = model.VATAmount,
                    JumlahPPNBM = model.JumlahPPnBM,
                    EventDate = DateTime.Now.FormatSQLDateTime(),
                    EventActor = EventActor
                };
                db.Execute("VATIn/usp_InsertCustom_TB_R_VATIn", args);
                #endregion
                //kondisi
                #region Detail - VATInDetail
                foreach (VATInDetailManualInputViewModel detailModel in model.VATInDetails)
                {
                    dynamic argsDetail = new
                    {
                        Id = Guid.NewGuid(),
                        VATInId = VATInID,
                        UnitName = detailModel.UnitName,
                        UnitPrice = detailModel.UnitPrice,
                        Quantity = detailModel.Quantity,
                        TotalPrice = detailModel.TotalPrice,
                        Discount = detailModel.Discount,
                        DPP = detailModel.DPP,
                        PPN = detailModel.PPN,
                        TarifPPNBM = detailModel.TarifPPNBM,
                        PPNBM = detailModel.PPNBM,
                        EventDate = DateTime.Now.FormatSQLDateTime(),
                        EventActor = EventActor
                    };
                    db.Execute("VATIn/usp_InsertCustom_TB_R_VATInDetail", argsDetail);
                }
                #endregion
                db.CommitTransaction();
                return MsgResultSuccessInsert();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                throw e;
            }
        }

        public Result Delete(Guid Id)
        {
            try
            {
                db.Execute("VATIn/usp_DeleteVATInByID", new { Id = Id });
                return MsgResultSuccessDelete();
            }
            catch (Exception e)
            {
                return MsgResultError(e.Message.ToString());
            }
        }

        public Result ExcelTaxInvoiceInsert(string path, string EventActor)
        {
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = "File has been uploaded successfully"
            };

            Result resultId = new Result
            {
                ResultCode = true,
                ResultDesc = "File has been uploaded successfully"
            };

            FileStream file = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite);

            HSSFWorkbook hssfwb = new HSSFWorkbook(file);
            ISheet sheet = hssfwb.GetSheet("Default");

            string pipedString = string.Empty;

            int row = 0;
            int rowFail = 0;
            int rowSuccess = 0;


            try
            {
                file.Close();
                VATIn vatIn = new VATIn();
                var cellNum = 30; // 30 is the cell amount in one row
                List<int> exceptionCellNum = new List<int>();
                exceptionCellNum.Add(12);
                exceptionCellNum.Add(13);
                exceptionCellNum.Add(14);
                exceptionCellNum.Add(15);
                exceptionCellNum.Add(16);
                exceptionCellNum.Add(17);
                exceptionCellNum.Add(18);
                exceptionCellNum.Add(19);
                exceptionCellNum.Add(20);
                exceptionCellNum.Add(21);

                exceptionCellNum.Add(22);
                exceptionCellNum.Add(23);
                exceptionCellNum.Add(24);
                exceptionCellNum.Add(25);
                exceptionCellNum.Add(26);
                exceptionCellNum.Add(27);
                exceptionCellNum.Add(28);
                exceptionCellNum.Add(29);
                List<int> exceptionCellNumDetail = new List<int>();
                exceptionCellNumDetail.Add(12);
                exceptionCellNumDetail.Add(13);
                exceptionCellNumDetail.Add(14);
                exceptionCellNumDetail.Add(15);
                exceptionCellNumDetail.Add(16);
                exceptionCellNumDetail.Add(17);
                exceptionCellNumDetail.Add(18);
                exceptionCellNumDetail.Add(19);
                exceptionCellNumDetail.Add(20);
                exceptionCellNumDetail.Add(21);
                var config = MasterConfigRepository.Instance.GetByConfigKey("CompanyNPWP");
                if (config == null)
                {
                    throw new Exception("Please define the CompanyNPWP Config");
                }

                var configbatam = MasterConfigRepository.Instance.GetByConfigKey("CompanyNPWPBatam");
                if (configbatam == null)
                {
                    throw new Exception("Please define the CompanyNPWPBatam Config");
                }

                #region Master - SyncData
                db.BeginTransaction();
                Guid SyncId = Guid.NewGuid(); //generate new Guid for Id - SyncData, to be referenced di VAT dan SyncDataDetail

                dynamic argsSyncData = new SyncDataViewModel() //insert SyncData paling pertama, karena dibutuhkan sebagai reference untuk data VAT dan SyncDataDetail
                    .CreateModel("FM", null, path, Path.GetFileName(@path))
                    .MapFromModel(SyncId, DateTime.Now, EventActor);

                db.Execute("SyncData/usp_Insert_TB_R_SyncData", argsSyncData);

                List<dynamic> argsSyncDataDetailList = new List<dynamic>(); //Declare list di awal untuk syncDataDetail agar menghemat performance
                db.CommitTransaction();
                #endregion

                List<LogSyncCreateUpdate> logSyncList = new List<LogSyncCreateUpdate>();
                List<string> failNomorFaktur = new List<string>();

                for (row = 4; row <= sheet.LastRowNum; row++) // Data row start at row 5, index 4 on excel template
                {
                    #region Set Cell Policy, Cell Type and Save Sync Data
                    pipedString = string.Empty;
                    for (int i = 0; i < cellNum; i++)
                    {
                        sheet.GetRow(row).GetCell(i, MissingCellPolicy.CREATE_NULL_AS_BLANK);
                        sheet.GetRow(row).GetCell(i).SetCellType(CellType.STRING);

                        pipedString += sheet.GetRow(row).GetCell(i).StringCellValue.ToString().Trim() + "|";
                    }
                    pipedString = pipedString.Remove(pipedString.Length - 1);
                    #endregion

                    try
                    {
                        #region Detail - SyncDataDetail Populate List
                        argsSyncDataDetailList.Add(new SyncDataDetailViewModel().CreateModel(row - 3, pipedString).MapFromModel(SyncId, DateTime.Now, EventActor));
                        #endregion

                        if (GlobalFunction.BlankCellCheck(sheet.GetRow(row), cellNum, exceptionCellNum))
                        {
                            #region Validasi jika nomor faktur ini sudah terjadi fail di row sebelumnya
                            if (failNomorFaktur.Contains(sheet.GetRow(row).GetCell(3).StringCellValue.ToString().Trim()))
                            {
                                throw new Exception(ExceptionHandler.PREVIOUS_ROW_FAIL_MESSAGE);
                            }
                            #endregion

                            #region Cell Mapping & Insert to DB
                            //if (vatIn.NomorFaktur != sheet.GetRow(row).GetCell(3).StringCellValue.ToString().Trim())
                            //{
                                if (!string.IsNullOrEmpty(vatIn.NomorFaktur))
                                {
                                    if (failNomorFaktur.Contains(vatIn.NomorFaktur))
                                    {
                                        db.AbortTransaction();
                                    }
                                    else
                                    {
                                        db.CommitTransaction();
                                    }
                                }

                                db.BeginTransaction();
                                vatIn = new VATIn();
                                vatIn.Id = Guid.NewGuid();
                                vatIn.SyncId = SyncId;
                                vatIn.KDJenisTransaksi = sheet.GetRow(row).GetCell(1).StringCellValue.ToString().Trim();
                                vatIn.FGPengganti = sheet.GetRow(row).GetCell(2).StringCellValue.ToString().Trim();
                                vatIn.NomorFaktur = sheet.GetRow(row).GetCell(3).StringCellValue.ToString().Trim();
                                vatIn.NomorFakturGabungan = vatIn.NomorFaktur.FormatNomorFakturGabungan(vatIn.KDJenisTransaksi, vatIn.FGPengganti);

                                var tgl_faktur_split = sheet.GetRow(row).GetCell(4).StringCellValue.ToString().Trim().Split('/');
                                if (tgl_faktur_split.Count() > 1) // when the excel column is a text type
                                {
                                    vatIn.TanggalFaktur = DateTime.ParseExact(sheet.GetRow(row).GetCell(4).StringCellValue.ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                }
                                else // when the excel column is a DateTime type
                                {
                                    vatIn.TanggalFaktur = DateTime.FromOADate(Convert.ToDouble(sheet.GetRow(row).GetCell(4).StringCellValue.ToString().Trim()));
                                }


                                vatIn.NPWPPenjual = sheet.GetRow(row).GetCell(5).StringCellValue.ToString().Trim().FormatNPWP();
                                vatIn.NamaPenjual = sheet.GetRow(row).GetCell(6).StringCellValue.ToString().Trim();
                                vatIn.AlamatPenjual = sheet.GetRow(row).GetCell(7).StringCellValue.ToString().Trim();
                                vatIn.NPWPLawanTransaksi = sheet.GetRow(row).GetCell(8).StringCellValue.ToString().Trim().FormatNPWP();
                                if (!vatIn.NPWPLawanTransaksi.Equals(config.ConfigValue) && !vatIn.NPWPLawanTransaksi.Equals(configbatam.ConfigValue))
                                {
                                    throw new Exception("Buyer NPWP '" + vatIn.NPWPLawanTransaksi + "' does not match with CompanyNPWP '" + config.ConfigValue + "'/'" + config.ConfigValue + "'");
                                }
                                vatIn.JumlahDPP = Decimal.Parse(sheet.GetRow(row).GetCell(9).StringCellValue.ToString().Trim());
                                string jmlDPP = vatIn.JumlahDPP.ToString();
                                if ((jmlDPP.Contains(".") || (jmlDPP.Contains(","))))//String contain " . or , "
                                {
                                    throw new Exception(ExceptionHandler.STRING_FAIL_MESSAGE);
                                }
                                vatIn.JumlahPPN = Decimal.Parse(sheet.GetRow(row).GetCell(10).StringCellValue.ToString().Trim());
                                string jmlPPN = vatIn.JumlahPPN.ToString();
                                if ((jmlPPN.Contains(".") || (jmlPPN.Contains(","))))//String contain " . or , "
                                {
                                    throw new Exception(ExceptionHandler.STRING_FAIL_MESSAGE);
                                }
                                vatIn.JumlahPPNBM = Decimal.Parse(sheet.GetRow(row).GetCell(11).StringCellValue.ToString().Trim());
                                string jmlPPNBM = vatIn.JumlahPPNBM.ToString();
                                if ((jmlPPNBM.Contains(".") || (jmlPPNBM.Contains(","))))//String contain " . or , "
                                {
                                    throw new Exception(ExceptionHandler.STRING_FAIL_MESSAGE);
                                }
                                vatIn.FakturType = Formatting.TYPE_EFAKTUR;

                                vatIn.CreatedBy = EventActor;
                                vatIn.CreatedOn = DateTime.Now;
                                vatIn.RowStatus = false;

                                //new
                                vatIn.InvoiceStatus = sheet.GetRow(row).GetCell(22).StringCellValue.ToString().Trim();
                                vatIn.NomorInvoice = sheet.GetRow(row).GetCell(23).StringCellValue.ToString().Trim();
                                vatIn.PVNumber = sheet.GetRow(row).GetCell(24).StringCellValue.ToString().Trim();
                                vatIn.PVCreatedBy = sheet.GetRow(row).GetCell(25).StringCellValue.ToString().Trim();
                                vatIn.SAPDOCNumber = sheet.GetRow(row).GetCell(26).StringCellValue.ToString().Trim();
                                if (sheet.GetRow(row).GetCell(27).StringCellValue.ToString().Trim() != "" && sheet.GetRow(row).GetCell(27).StringCellValue.ToString().Trim() != null)
                                {
                                    var tgl_posting_split = sheet.GetRow(row).GetCell(27).StringCellValue.ToString().Trim().Split('/');
                                    if (tgl_posting_split.Count() > 1) // when the excel column is a text type
                                    {
                                        vatIn.PostingDate = DateTime.ParseExact(sheet.GetRow(row).GetCell(27).StringCellValue.ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    }
                                    else // when the excel column is a DateTime type
                                    {
                                        vatIn.PostingDate = DateTime.FromOADate(Convert.ToDouble(sheet.GetRow(row).GetCell(27).StringCellValue.ToString().Trim()));
                                    }
                                }
                                vatIn.RelatedGLAccount = sheet.GetRow(row).GetCell(28).StringCellValue.ToString().Trim();

                                DateTime expdate = vatIn.TanggalFaktur.AddDays(1).AddMonths(3).AddDays(-1);
                                DateTime endOfMonth = new DateTime(expdate.Year, expdate.Month, 1).AddMonths(1).AddDays(-1);
                                string tanggalexpired = endOfMonth.ToShortDateString();
                                int SelisihHari = (Convert.ToDateTime(tanggalexpired) - DateTime.Now.Date).Days;//Convert.ToDateTime(tanggalexpired)).Days;
                                var NilaiSelisihHari = MasterConfigRepository.Instance.GetByConfigKey("VATInDaysExpired");
                                if (SelisihHari < Convert.ToInt32(NilaiSelisihHari.ConfigValue))
                                {
                                    if (SelisihHari <= 0)
                                    {
                                        SelisihHari = 0;
                                    }

                                    throw new Exception(vatIn.NomorFaktur + "Invoice can’t be saved. Tax Invoice EXPIRED is Already " + SelisihHari + " Days Before Expired.");
                                }
                                vatIn.ExpiredDate = endOfMonth;
                                vatIn.ApprovalDescription = sheet.GetRow(row).GetCell(29).StringCellValue.ToString().Trim();

                                // Save VAT In
                                result = db.SingleOrDefault<Result>("VATIn/usp_Insert_TB_R_VATIn", vatIn.MapFromModel(DateTime.Now, EventActor));
                                if (!result.ResultCode)
                                {
                                    throw new Exception(result.ResultDesc);
                                }

                                // get VatIn
                                resultId = db.SingleOrDefault<Result>("VATIn/usp_Get_Id_Result", vatIn.MapFromModel(DateTime.Now, EventActor));
                                if (!resultId.ResultCode)
                                {
                                    throw new Exception(resultId.ResultDesc);
                                }
                            //}
                            //else
                            //{
                            //    throw new Exception("Immediate duplicate rows of " + vatIn.NomorFaktur);
                            //}

                            if (!GlobalFunction.IsCellBlank(sheet.GetRow(row), cellNum, exceptionCellNumDetail)) //if at least one optional column is NOT empty
                            {
                                try {
                                VATInDetail vatInDetail = new VATInDetail();
                                vatInDetail.Id = Guid.NewGuid();
                                vatInDetail.VATInId = new Guid(resultId.ResultDesc);// vatIn.Id;
                                vatInDetail.NamaBarang = sheet.GetRow(row).GetCell(13).StringCellValue.ToString().Trim();
                                vatInDetail.HargaSatuan = Decimal.Parse(sheet.GetRow(row).GetCell(14).StringCellValue.ToString().Trim());
                                vatInDetail.JumlahBarang = Decimal.Parse(sheet.GetRow(row).GetCell(15).StringCellValue.ToString().Trim());
                                vatInDetail.HargaTotal = Decimal.Parse(sheet.GetRow(row).GetCell(16).StringCellValue.ToString().Trim());
                                vatInDetail.Diskon = Decimal.Parse(sheet.GetRow(row).GetCell(17).StringCellValue.ToString().Trim());
                                vatInDetail.DPP = Decimal.Parse(sheet.GetRow(row).GetCell(18).StringCellValue.ToString().Trim());
                                vatInDetail.PPN = Decimal.Parse(sheet.GetRow(row).GetCell(19).StringCellValue.ToString().Trim());
                                vatInDetail.TarifPPNBM = Decimal.Parse(sheet.GetRow(row).GetCell(20).StringCellValue.ToString().Trim());
                                vatInDetail.PPNBM = Decimal.Parse(sheet.GetRow(row).GetCell(21).StringCellValue.ToString().Trim());
                                vatInDetail.Status = "00"; // Default Value

                                vatInDetail.CreatedBy = EventActor;
                                vatInDetail.CreatedOn = DateTime.Now;
                                vatInDetail.RowStatus = false;

                                // Save VAT In Detail
                                db.Execute("VATIn/usp_Insert_TB_R_VATInDetail", vatInDetail.MapFromModel());
                                }
                                catch (Exception e)
                                {
                                    throw new Exception("VAT In Detail field(s) cannot be empty due");
                                }
                            }

                            logSyncList.Add(new LogSyncCreateUpdate()
                                .CreateModel(Path.GetFileName(path), row - 3, pipedString, MessageType.INF, ExceptionHandler.GENERAL_SUCCESS_MESSAGE, path));
                            rowSuccess++;
                            #endregion
                        }
                        else
                        {
                            throw new Exception(ExceptionHandler.MANDATORY_FAIL_MESSAGE); //jika mandatory field ada yg blank
                        }
                    }
                    catch (Exception e)
                    {
                        if (!failNomorFaktur.Contains(sheet.GetRow(row).GetCell(3).StringCellValue.ToString().Trim())) //kolom 3 = Nomor Faktur
                        {
                            failNomorFaktur.Add(sheet.GetRow(row).GetCell(3).StringCellValue.ToString().Trim()); //untuk mencatat no faktur yang error
                        }
                        logSyncList.Add(new LogSyncCreateUpdate().CreateModel(Path.GetFileName(path), row - 3, pipedString, MessageType.ERR, e.Message, path));
                        rowFail++;
                    }
                }

                #region execute batch terakhir dari VAT
                if (failNomorFaktur.Contains(vatIn.NomorFaktur))
                {
                    db.AbortTransaction();
                }
                else
                {
                    db.CommitTransaction();
                }
                #endregion

                #region SyncDataDetail Insert
                db.BeginTransaction();
                foreach (dynamic argsSyncDataDetail in argsSyncDataDetailList)
                {
                    db.Execute("SyncData/usp_Insert_TB_R_SyncDataDetail", argsSyncDataDetail);
                }
                db.CommitTransaction();
                #endregion

                #region SyncLog Insert
                db.BeginTransaction();
                ExceptionHandler.LogToSync(logSyncList);
                db.CommitTransaction();
                #endregion

                result.ResultCode = true;
                result.ResultDesc = Formatting
                    .UPLOAD_NOTIFICATION
                    .Replace("@success", (rowSuccess).ToString())
                    .Replace("@error", (rowFail).ToString());
            }
            catch (Exception e)
            {
                db.AbortTransaction();

                result.ResultCode = false;
                result.ResultDesc = e.LogToSync(row - 3, pipedString, Path.GetFileName(path), path, MessageType.ERR).Message;
            }

            db.Close();
            return result;
        }

        public Result ExcelTaxInvoiceInsertPIB(string path, string EventActor)
        {
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = "File has been uploaded successfully"
            };

            FileStream file = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite);

            HSSFWorkbook hssfwb = new HSSFWorkbook(file);
            ISheet sheet = hssfwb.GetSheet("Default");

            string pipedString = string.Empty;

            int row = 0;
            int rowFail = 0;
            int rowSuccess = 0;

            try
            {
                file.Close();
                VATIn vatIn = new VATIn();
                var cellNum = 24; // 22 is the cell amount in one row
                List<int> exceptionCellNum = new List<int>();
                exceptionCellNum.Add(5);
                var config = MasterConfigRepository.Instance.GetByConfigKey("CompanyNPWP");
                if (config == null)
                {
                    throw new Exception("Please define the CompanyNPWP Config");
                }


                #region Master - SyncData
                db.BeginTransaction();
                Guid SyncId = Guid.NewGuid(); //generate new Guid for Id - SyncData, to be referenced di VAT dan SyncDataDetail

                dynamic argsSyncData = new SyncDataViewModel() //insert SyncData paling pertama, karena dibutuhkan sebagai reference untuk data VAT dan SyncDataDetail
                    .CreateModel("DM", null, path, Path.GetFileName(@path))
                    .MapFromModel(SyncId, DateTime.Now, EventActor);

                db.Execute("SyncData/usp_Insert_TB_R_SyncData", argsSyncData);

                List<dynamic> argsSyncDataDetailList = new List<dynamic>(); //Declare list di awal untuk syncDataDetail agar menghemat performance
                db.CommitTransaction();
                #endregion

                List<LogSyncCreateUpdate> logSyncList = new List<LogSyncCreateUpdate>();
                List<string> failNomorFaktur = new List<string>();

                for (row = 4; row <= sheet.LastRowNum; row++) // Data row start at row 5, index 4 on excel template
                {
                    #region Set Cell Policy, Cell Type and Save Sync Data
                    pipedString = string.Empty;
                    for (int i = 0; i < cellNum; i++)
                    {
                        sheet.GetRow(row).GetCell(i, MissingCellPolicy.CREATE_NULL_AS_BLANK);
                        sheet.GetRow(row).GetCell(i).SetCellType(CellType.STRING);

                        pipedString += sheet.GetRow(row).GetCell(i).StringCellValue.ToString().Trim() + "|";
                    }
                    pipedString = pipedString.Remove(pipedString.Length - 1);
                    #endregion

                    try
                    {
                        #region Detail - SyncDataDetail Populate List
                        argsSyncDataDetailList.Add(new SyncDataDetailViewModel().CreateModel(row - 3, pipedString).MapFromModel(SyncId, DateTime.Now, EventActor));
                        #endregion

                        if (GlobalFunction.BlankCellCheck(sheet.GetRow(row), cellNum, exceptionCellNum))
                        {
                            #region Validasi jika nomor faktur ini sudah terjadi fail di row sebelumnya
                            if (failNomorFaktur.Contains(sheet.GetRow(row).GetCell(8).StringCellValue.ToString().Trim()))
                            {
                                throw new Exception(ExceptionHandler.PREVIOUS_ROW_FAIL_MESSAGE);
                            }
                            #endregion

                            #region Cell Mapping & Insert to DB
                            if (vatIn.NomorFaktur != sheet.GetRow(row).GetCell(8).StringCellValue.ToString().Trim())
                            {
                                if (!string.IsNullOrEmpty(vatIn.NomorFaktur))
                                {
                                    if (failNomorFaktur.Contains(vatIn.NomorFaktur))
                                    {
                                        db.AbortTransaction();
                                    }
                                    else
                                    {
                                        db.CommitTransaction();
                                    }
                                }

                                db.BeginTransaction();
                                vatIn = new VATIn();
                                vatIn.Id = Guid.NewGuid();
                                vatIn.SyncId = SyncId;
                                vatIn.KDJenisTransaksi = sheet.GetRow(row).GetCell(3).StringCellValue.ToString().Trim();
                                vatIn.FGPengganti = sheet.GetRow(row).GetCell(4).StringCellValue.ToString().Trim();
                                vatIn.NomorFaktur = sheet.GetRow(row).GetCell(8).StringCellValue.ToString().Trim();
                                vatIn.NomorFakturGabungan = vatIn.NomorFaktur;

                                var tgl_faktur_split = sheet.GetRow(row).GetCell(9).StringCellValue.ToString().Trim().Split('/');
                                if (tgl_faktur_split.Count() > 1) // when the excel column is a text type
                                {
                                    vatIn.TanggalFaktur = DateTime.ParseExact(sheet.GetRow(row).GetCell(9).StringCellValue.ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                }
                                else // when the excel column is a DateTime type
                                {
                                    vatIn.TanggalFaktur = DateTime.FromOADate(Convert.ToDouble(sheet.GetRow(row).GetCell(9).StringCellValue.ToString().Trim()));
                                }


                                vatIn.NPWPPenjual = sheet.GetRow(row).GetCell(10).StringCellValue.ToString().Trim().FormatNPWP();
                                vatIn.NamaPenjual = sheet.GetRow(row).GetCell(11).StringCellValue.ToString().Trim();
                                vatIn.AlamatPenjual = sheet.GetRow(row).GetCell(12).StringCellValue.ToString().Trim();

                                vatIn.JumlahDPP = Decimal.Parse(sheet.GetRow(row).GetCell(13).StringCellValue.ToString().Trim());
                                string jmlDPP = vatIn.JumlahDPP.ToString();
                                if ((jmlDPP.Contains(".") || (jmlDPP.Contains(","))))//String contain " . or , "
                                {
                                    throw new Exception(ExceptionHandler.STRING_FAIL_MESSAGE);
                                }
                                vatIn.JumlahPPN = Decimal.Parse(sheet.GetRow(row).GetCell(14).StringCellValue.ToString().Trim());
                                string jmlPPN = vatIn.JumlahPPN.ToString();
                                if ((jmlPPN.Contains(".") || (jmlPPN.Contains(","))))//String contain " . or , "
                                {
                                    throw new Exception(ExceptionHandler.STRING_FAIL_MESSAGE);
                                }
                                vatIn.JumlahPPNBM = Decimal.Parse(sheet.GetRow(row).GetCell(15).StringCellValue.ToString().Trim());
                                string jmlPPNBM = vatIn.JumlahPPNBM.ToString();
                                if ((jmlPPNBM.Contains(".") || (jmlPPNBM.Contains(","))))//String contain " . or , "
                                {
                                    throw new Exception(ExceptionHandler.STRING_FAIL_MESSAGE);
                                }
                                vatIn.FakturType = Formatting.TYPE_NON_EFAKTUR;

                                if (sheet.GetRow(row).GetCell(16).StringCellValue.ToString().Trim() != "")
                                {
                                    vatIn.JumlahPPH22 = Decimal.Parse(sheet.GetRow(row).GetCell(16).StringCellValue.ToString().Trim());
                                    string jmlPPH22 = vatIn.JumlahPPH22.ToString();
                                    if ((jmlPPH22.Contains(".") || (jmlPPH22.Contains(","))))
                                    {
                                        throw new Exception(ExceptionHandler.STRING_FAIL_MESSAGE);
                                    }
                                }
                                vatIn.InvoiceStatus = sheet.GetRow(row).GetCell(18).StringCellValue.ToString().Trim();
                                vatIn.SAPDOCNumber = sheet.GetRow(row).GetCell(19).StringCellValue.ToString().Trim();
                                vatIn.RelatedGLAccount = sheet.GetRow(row).GetCell(21).StringCellValue.ToString().Trim();
                                vatIn.PVNumber = sheet.GetRow(row).GetCell(22).StringCellValue.ToString().Trim();

                                //if (sheet.GetRow(row).GetCell(22).StringCellValue.ToString().Trim() != "")
                                //{
                                //    vatIn.RemainingDays = Int32.Parse(sheet.GetRow(row).GetCell(22).StringCellValue.ToString().Trim());
                                //}
                                var tgl_posting_split = sheet.GetRow(row).GetCell(20).StringCellValue.ToString().Trim().Split('/');
                                string tgl_post = sheet.GetRow(row).GetCell(20).StringCellValue.ToString().Trim();
                                if (tgl_posting_split.Count() > 1) // when the excel column is a text type
                                {
                                    if (sheet.GetRow(row).GetCell(20).StringCellValue.ToString().Trim() != null && sheet.GetRow(row).GetCell(20).StringCellValue.ToString().Trim() != "")
                                    {
                                        vatIn.PostingDate = DateTime.ParseExact(sheet.GetRow(row).GetCell(20).StringCellValue.ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    }
                                    else
                                    {
                                        vatIn.PostingDate = null;
                                    }
                                }
                                else // when the excel column is a DateTime type
                                {
                                    if (sheet.GetRow(row).GetCell(20).StringCellValue.ToString().Trim() != null && sheet.GetRow(row).GetCell(20).StringCellValue.ToString().Trim() != "")
                                    {
                                        vatIn.PostingDate = DateTime.FromOADate(Convert.ToDouble(sheet.GetRow(row).GetCell(20).StringCellValue.ToString().Trim()));
                                    }
                                    else
                                    {
                                        vatIn.PostingDate = null;
                                    }
                                }
                                vatIn.NomorInvoice = sheet.GetRow(row).GetCell(6).StringCellValue.ToString().Trim();
                                var tgl_invoice_split = sheet.GetRow(row).GetCell(7).StringCellValue.ToString().Trim().Split('/');
                                if (tgl_invoice_split.Count() > 1) // when the excel column is a text type
                                {
                                    if (sheet.GetRow(row).GetCell(7).StringCellValue.ToString().Trim() != null && sheet.GetRow(row).GetCell(7).StringCellValue.ToString().Trim() != "")
                                    {
                                        vatIn.TanggalInvoice = DateTime.ParseExact(sheet.GetRow(row).GetCell(7).StringCellValue.ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    }
                                    else
                                    {
                                        vatIn.TanggalInvoice = null;
                                    }
                                }
                                else // when the excel column is a DateTime type
                                {
                                    if (sheet.GetRow(row).GetCell(7).StringCellValue.ToString().Trim() != null && sheet.GetRow(row).GetCell(7).StringCellValue.ToString().Trim() != "")
                                    {
                                        vatIn.TanggalInvoice = DateTime.FromOADate(Convert.ToDouble(sheet.GetRow(row).GetCell(7).StringCellValue.ToString().Trim()));
                                    }
                                    else
                                    {
                                        vatIn.TanggalInvoice = null;
                                    }
                                }
                                vatIn.CreatedBy = EventActor;
                                vatIn.CreatedOn = DateTime.Now;
                                vatIn.RowStatus = false;
                                vatIn.PVCreatedBy = sheet.GetRow(row).GetCell(23).StringCellValue.ToString().Trim();
                                vatIn.Keterangan = sheet.GetRow(row).GetCell(17).StringCellValue.ToString().Trim();
                                // Save VAT In
                                result = db.SingleOrDefault<Result>("VATIn/usp_Insert_TB_R_VATInPIB", vatIn.MapFromModel(DateTime.Now, EventActor));
                                if (!result.ResultCode)
                                {
                                    throw new Exception(result.ResultDesc);
                                }
                            }
                            else
                            {
                                throw new Exception("Immediate duplicate rows of " + vatIn.NomorFaktur);
                            }

                            logSyncList.Add(new LogSyncCreateUpdate()
                                .CreateModel(Path.GetFileName(path), row - 3, pipedString, MessageType.INF, ExceptionHandler.GENERAL_SUCCESS_MESSAGE, path));
                            rowSuccess++;
                            #endregion
                        }
                        else
                        {
                            throw new Exception(ExceptionHandler.MANDATORY_FAIL_MESSAGE); //jika mandatory field ada yg blank
                        }
                    }
                    catch (Exception e)
                    {
                        if (!failNomorFaktur.Contains(sheet.GetRow(row).GetCell(3).StringCellValue.ToString().Trim())) //kolom 3 = Nomor Faktur
                        {
                            failNomorFaktur.Add(sheet.GetRow(row).GetCell(3).StringCellValue.ToString().Trim()); //untuk mencatat no faktur yang error
                        }
                        logSyncList.Add(new LogSyncCreateUpdate().CreateModel(Path.GetFileName(path), row - 3, pipedString, MessageType.ERR, e.Message, path));
                        rowFail++;
                    }
                }

                #region execute batch terakhir dari VAT
                if (failNomorFaktur.Contains(vatIn.NomorFaktur))
                {
                    db.AbortTransaction();
                }
                else
                {
                    db.CommitTransaction();
                }
                #endregion

                #region SyncDataDetail Insert
                db.BeginTransaction();
                foreach (dynamic argsSyncDataDetail in argsSyncDataDetailList)
                {
                    db.Execute("SyncData/usp_Insert_TB_R_SyncDataDetail", argsSyncDataDetail);
                }
                db.CommitTransaction();
                #endregion

                #region SyncLog Insert
                db.BeginTransaction();
                ExceptionHandler.LogToSync(logSyncList);
                db.CommitTransaction();
                #endregion

                result.ResultCode = true;
                result.ResultDesc = Formatting
                    .UPLOAD_NOTIFICATION
                    .Replace("@success", (rowSuccess).ToString())
                    .Replace("@error", (rowFail).ToString());
            }
            catch (Exception e)
            {
                db.AbortTransaction();

                result.ResultCode = false;
                result.ResultDesc = e.LogToSync(row - 3, pipedString, Path.GetFileName(path), path, MessageType.ERR).Message;
            }

            db.Close();
            return result;
        }

        public Result ExcelApprovalStatusUpdate(string path, string user)
        {
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = Formatting.UPLOAD_NOTIFICATION
            };

            FileStream file = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite);

            HSSFWorkbook hssfwb = new HSSFWorkbook(file);
            ISheet sheet = hssfwb.GetSheet("Default");

            string pipedString = string.Empty;
            string approvalTime = string.Empty;
            string approvaldesc = string.Empty;
            bool isApproveSukses = false;

            int row = 0;
            int rowFail = 0;
            int rowSuccess = 0;

            try
            {
                file.Close();
                VATInApprovalStatusViewModel vatInApprovalStatus = new VATInApprovalStatusViewModel();
                var cellNum = 5; // 5 is the cell amount in one row
                List<int> exceptionCellNum = new List<int>();
                exceptionCellNum.Add(2);
                exceptionCellNum.Add(4);
                db.BeginTransaction();

                for (row = 4; row <= sheet.LastRowNum; row++) // Data row start at row 5, index 4 on excel template
                {
                    try
                    {
                        #region Set Cell Policy, Cell Type and Save Sync Data
                        pipedString = string.Empty;
                        for (int i = 0; i < cellNum; i++)
                        {
                            sheet.GetRow(row).GetCell(i, MissingCellPolicy.CREATE_NULL_AS_BLANK);
                            sheet.GetRow(row).GetCell(i).SetCellType(CellType.STRING);

                            pipedString += sheet.GetRow(row).GetCell(i).StringCellValue.ToString().Trim() + "|";
                        }
                        pipedString = pipedString.Remove(pipedString.Length - 1);
                        #endregion

                        if (GlobalFunction.BlankCellCheck(sheet.GetRow(row), cellNum, exceptionCellNum))
                        {
                            #region Cell Mapping & Insert to DB
                            if (vatInApprovalStatus.NomorFakturGabungan != sheet.GetRow(row).GetCell(0).StringCellValue.ToString().Trim()) //ignore if duplicate
                            {
                                vatInApprovalStatus = new VATInApprovalStatusViewModel();
                                vatInApprovalStatus.NomorFakturGabungan = sheet.GetRow(row).GetCell(0).StringCellValue.ToString().Trim();
                                vatInApprovalStatus.ApprovalStatus = sheet.GetRow(row).GetCell(1).StringCellValue.ToString().Trim();
                                approvalTime = sheet.GetRow(row).GetCell(2).StringCellValue.ToString().Trim();
                                approvaldesc = sheet.GetRow(row).GetCell(4).StringCellValue.ToString().Trim();
                                isApproveSukses = (vatInApprovalStatus.ApprovalStatus.ToLower().Equals("approval sukses"));


                                if (string.IsNullOrEmpty(approvalTime) && isApproveSukses)
                                {
                                    throw new Exception(ExceptionHandler.MANDATORY_FAIL_MESSAGE);
                                }
                                else if (!string.IsNullOrEmpty(approvalTime))
                                {
                                    vatInApprovalStatus.ApprovalTime = DateTime.ParseExact(
                                                                approvalTime.Replace("WIB ", ""),
                                                                "ddd MMM dd HH:mm:ss yyyy",
                                                                System.Globalization.CultureInfo.InvariantCulture);
                                }

                                vatInApprovalStatus.RecordTime = DateTime.ParseExact(
                                    sheet.GetRow(row).GetCell(3).StringCellValue.ToString().Trim().Replace("WIB ", ""),
                                    "ddd MMM dd HH:mm:ss yyyy",
                                    System.Globalization.CultureInfo.InvariantCulture);
                                if (string.IsNullOrEmpty(approvaldesc) && isApproveSukses)
                                {
                                    vatInApprovalStatus.ApprovalDescription = sheet.GetRow(row).GetCell(4).StringCellValue.ToString().Trim();
                                }
                                else if (!string.IsNullOrEmpty(approvaldesc))
                                {
                                    vatInApprovalStatus.ApprovalDescription = sheet.GetRow(row).GetCell(4).StringCellValue.ToString().Trim();
                                }
                                else
                                {
                                    throw new Exception(ExceptionHandler.MANDATORY_FAIL_MESSAGE);
                                }
                                result = db.SingleOrDefault<Result>("VATIn/usp_UpdateCustom_TB_R_VATIn_ApprovalStatus", vatInApprovalStatus.MapFromModel(DateTime.Now, user));
                            }
                            else
                            {
                                throw new Exception("Immediate duplicate rows of " + vatInApprovalStatus.NomorFakturGabungan);
                            }
                            #endregion

                            if (!result.ResultCode)
                            {
                                throw new Exception(result.ResultDesc);
                            }
                            else
                            {
                                ExceptionHandler.LogToSync(row - 3, pipedString, Path.GetFileName(path), ExceptionHandler.GENERAL_SUCCESS_MESSAGE,path, MessageType.INF);
                                rowSuccess++;
                            }
                        }
                        else
                        {
                            throw new Exception(ExceptionHandler.MANDATORY_FAIL_MESSAGE);
                        }
                    }
                    catch (Exception e)
                    {
                        e.LogToSync(row - 3, pipedString, Path.GetFileName(path), path, MessageType.ERR);
                        rowFail++;
                    }
                }

                db.CommitTransaction();
                result.ResultCode = true;
                result.ResultDesc = Formatting.UPLOAD_NOTIFICATION.Replace("@success", (rowSuccess).ToString()).Replace("@error", rowFail.ToString());
            }
            catch (Exception e)
            {
                db.AbortTransaction();

                result.ResultCode = false;
                result.ResultDesc = e.LogToSync(row + 1, pipedString, Path.GetFileName(path), path, MessageType.ERR).Message;
            }

            db.Close();
            return result;
        }

        public Result ExcelTransitoryStatusUpdate(string path, string EventActor)
        {
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = Formatting.UPLOAD_NOTIFICATION
            };

            FileStream file = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite);

            HSSFWorkbook hssfwb = new HSSFWorkbook(file);
            ISheet sheet = hssfwb.GetSheet("Default");

            string pipedString = string.Empty;

            int row = 0;
            int rowFail = 0;
            int rowSuccess = 0;

            try
            {
                file.Close();
                VATInTransitoryStatusViewModel vatInApprovalStatus = new VATInTransitoryStatusViewModel();
                var cellNum = 4; // 3 is the cell amount in one row
                List<int> exceptionCellNum = new List<int>();
                exceptionCellNum.Add(3);
                db.BeginTransaction();

                for (row = 4; row <= sheet.LastRowNum; row++) // Data row start at row 5, index 4 on excel template
                {
                    try
                    {
                        #region Set Cell Policy, Cell Type and Save Sync Data
                        pipedString = string.Empty;
                        for (int i = 0; i < cellNum; i++)
                        {
                            sheet.GetRow(row).GetCell(i, MissingCellPolicy.CREATE_NULL_AS_BLANK);
                            sheet.GetRow(row).GetCell(i).SetCellType(CellType.STRING);

                            pipedString += sheet.GetRow(row).GetCell(i).StringCellValue.ToString().Trim() + "|";
                        }
                        pipedString = pipedString.Remove(pipedString.Length - 1);
                        #endregion
                        if (GlobalFunction.BlankCellCheck(sheet.GetRow(row), cellNum, exceptionCellNum))
                        {
                            #region Cell Mapping & Insert to DB
                            if (vatInApprovalStatus.NomorFakturGabungan != sheet.GetRow(row).GetCell(0).StringCellValue.ToString().Trim()) //ignore if duplicate
                            {
                                vatInApprovalStatus = new VATInTransitoryStatusViewModel();
                                vatInApprovalStatus.NomorFakturGabungan = sheet.GetRow(row).GetCell(0).StringCellValue.ToString().Trim();
                                var tgl_faktur_split = sheet.GetRow(row).GetCell(1).StringCellValue.ToString().Trim().Split('/');
                                if (tgl_faktur_split.Count() > 1) // when the excel column is a text type
                                {
                                    vatInApprovalStatus.TransitoryDate = DateTime.ParseExact(sheet.GetRow(row).GetCell(1).StringCellValue.ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                }
                                else // when the excel column is a DateTime type
                                {
                                    vatInApprovalStatus.TransitoryDate = DateTime.FromOADate(Convert.ToDouble(sheet.GetRow(row).GetCell(1).StringCellValue.ToString().Trim()));
                                }
                                vatInApprovalStatus.TransitoryStatus = sheet.GetRow(row).GetCell(2).StringCellValue.ToString().Trim();
                                vatInApprovalStatus.TransitoryNumber = sheet.GetRow(row).GetCell(3).StringCellValue.ToString().Trim();

                                //if(vatInApprovalStatus.TransitoryStatus.ToLower() == "yes" && vatInApprovalStatus.TransitoryNumber == "")
                                //{
                                //    throw new Exception(ExceptionHandler.TRANSITORY_STATUS);
                                //}
                                //else
                                //{
                                    result = db.SingleOrDefault<Result>("VATIn/usp_UpdateCustom_TB_R_VATIn_TransitoryStatus", vatInApprovalStatus.MapFromModel(DateTime.Now, EventActor));
                                //}
                               
                            }
                            else
                            {
                                throw new Exception("Immediate duplicate rows of " + vatInApprovalStatus.NomorFakturGabungan);
                            }
                            #endregion

                            if (!result.ResultCode)
                            {
                                throw new Exception(result.ResultDesc);
                            }
                            else
                            {
                                ExceptionHandler.LogToSync(row - 4, pipedString, Path.GetFileName(path), ExceptionHandler.GENERAL_SUCCESS_MESSAGE, path,MessageType.INF);
                                rowSuccess++;
                            }
                        }
                        else
                        {
                            throw new Exception(ExceptionHandler.MANDATORY_FAIL_MESSAGE);
                        }
                    }
                    catch (Exception e)
                    {
                        e.LogToSync(row - 4, pipedString, Path.GetFileName(path), path, MessageType.ERR);
                        rowFail++;
                    }
                }

                db.CommitTransaction();
                result.ResultCode = true;
                result.ResultDesc = Formatting.UPLOAD_NOTIFICATION.Replace("@success", (rowSuccess).ToString()).Replace("@error", rowFail.ToString());
            }
            catch (Exception e)
            {
                //db.AbortTransaction();

                result.ResultCode = false;
                result.ResultDesc = e.LogToSync(row + 1, pipedString, Path.GetFileName(path), path, MessageType.ERR).Message;
            }

            db.Close();
            return result;
        }

        public Result ExcelMasaPajakUpdate(string path, string user)
        {
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = Formatting.UPLOAD_NOTIFICATION
            };

            FileStream file = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite);

            HSSFWorkbook hssfwb = new HSSFWorkbook(file);
            ISheet sheet = hssfwb.GetSheet("Default");

            string pipedString = string.Empty;

            int row = 0;
            int rowFail = 0;
            int rowSuccess = 0;

            try
            {
                file.Close();
                VATInMasaPajakViewModel vatInMasaPajak = new VATInMasaPajakViewModel();
                var cellNum = 3; // 3 is the cell amount in one row
                List<int> exceptionCellNum = new List<int>();
                db.BeginTransaction();

                for (row = 4; row <= sheet.LastRowNum; row++) // Data row start at row 5, index 4 on excel template
                {
                    try
                    {
                        #region Set Cell Policy, Cell Type and Save Sync Data
                        pipedString = string.Empty;
                        for (int i = 0; i < cellNum; i++)
                        {
                            sheet.GetRow(row).GetCell(i, MissingCellPolicy.CREATE_NULL_AS_BLANK);
                            sheet.GetRow(row).GetCell(i).SetCellType(CellType.STRING);

                            pipedString += sheet.GetRow(row).GetCell(i).StringCellValue.ToString().Trim() + "|";
                        }
                        pipedString = pipedString.Remove(pipedString.Length - 1);
                        #endregion

                        if (GlobalFunction.BlankCellCheck(sheet.GetRow(row), cellNum, exceptionCellNum))
                        {
                            #region Cell Mapping & Insert to DB
                            if (vatInMasaPajak.NomorFaktur != sheet.GetRow(row).GetCell(0).StringCellValue.ToString().Trim()) //ignore if duplicate
                            {
                                vatInMasaPajak = new VATInMasaPajakViewModel();
                                vatInMasaPajak.NomorFaktur = sheet.GetRow(row).GetCell(0).StringCellValue.ToString().Trim();
                                vatInMasaPajak.Bulan = sheet.GetRow(row).GetCell(1).StringCellValue.ToString().Trim();
                                vatInMasaPajak.Tahun = sheet.GetRow(row).GetCell(2).StringCellValue.ToString().Trim();

                                result = db.SingleOrDefault<Result>("VATIn/usp_UpdateCustom_TB_R_VATIn_MasaPajak", vatInMasaPajak.MapFromModel(DateTime.Now, user));
                            }
                            else
                            {
                                throw new Exception("Immediate duplicate rows of " + vatInMasaPajak.NomorFaktur);
                            }
                            #endregion

                            if (!result.ResultCode)
                            {
                                throw new Exception(result.ResultDesc);
                            }
                            else
                            {
                                ExceptionHandler.LogToSync(row - 3, pipedString, Path.GetFileName(path), ExceptionHandler.GENERAL_SUCCESS_MESSAGE, path,MessageType.INF);
                                rowSuccess++;
                            }
                        }
                        else
                        {
                            throw new Exception(ExceptionHandler.MANDATORY_FAIL_MESSAGE);
                        }
                    }
                    catch (Exception e)
                    {
                        e.LogToSync(row - 3, pipedString, Path.GetFileName(path), path, MessageType.ERR);
                        rowFail++;
                    }
                }

                db.CommitTransaction();
                result.ResultCode = true;
                result.ResultDesc = Formatting.UPLOAD_NOTIFICATION.Replace("@success", (rowSuccess).ToString()).Replace("@error", rowFail.ToString());
            }
            catch (Exception e)
            {
                db.AbortTransaction();

                result.ResultCode = false;
                result.ResultDesc = e.LogToSync(row + 1, pipedString, Path.GetFileName(path), path, MessageType.ERR).Message;
            }

            db.Close();
            return result;
        }

        public Result SyncVATIn()
        {
            #region DB -> Timeout
            //try
            //{
            //    db.BeginTransaction();
            //    db.Execute("efb_sp_ELVIS_VATIN_DataSync_Batch");
            //    db.CommitTransaction();
            //}
            //catch (Exception e)
            //{
            //    db.AbortTransaction();
            //    throw e;
            //}
            #endregion
            
            var conn = System.Configuration.ConfigurationManager.ConnectionStrings["TAM_EFAKTURConnectionString"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(conn))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                SqlTransaction transaction;

                transaction = connection.BeginTransaction();

                command.Connection = connection;
                command.Transaction = transaction;
                try
                {
                    command.CommandText = "efb_sp_ExecuteSync"; //efb_sp_ELVIS_VATIN_DataSync_Batch
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.CommandTimeout = 0;
                    command.ExecuteNonQuery();

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Commit Exception Type: {0}", ex.GetType());
                    Console.WriteLine("  Message: {0}", ex.Message);
                    try
                    {
                        transaction.Rollback();
                    }
                    catch (Exception ex2)
                    {
                        Console.WriteLine("Rollback Exception Type: {0}", ex2.GetType());
                        Console.WriteLine("  Message: {0}", ex2.Message);
                    }
                }
                connection.Close();
                connection.Dispose();
            }
            return MsgResultSuccessSync();
        }

        //public Result CekNPWP(VATInManualInputViewModel model)
        //{
        //    //string npwp = 
        //    var config = MasterConfigRepository.Instance.GetByConfigKey("CompanyNPWP");
        //    Result validateResult = db.SingleOrDefault<Result>("VATIn/usp_ValidateVATInInvoiceNumber", new { NomorFakturGabungan = model.InvoiceNumberFull });
        //}  


        #region Private Method
        private List<SyncDataDetailViewModel> MapSyncDataDetailsModel(VATInManualInputViewModel VATInModel)
        {
            string headerDataPiped =
                VATInModel.KDJenisTransaksi + "|" + VATInModel.FGPengganti + "|" +
                VATInModel.InvoiceNumber + "|" + VATInModel.InvoiceDate + "|" +
                VATInModel.SupplierNPWP + "|" + VATInModel.SupplierName + "|" +
                VATInModel.SupplierAddress + "|" + VATInModel.NPWPLawanTransaksi + "|" +
                VATInModel.NamaLawanTransaksi + "|" + VATInModel.AlamatLawanTransaksi + "|" +
                VATInModel.VATBaseAmount + "|" + VATInModel.VATAmount + "|" +
                VATInModel.JumlahPPnBM + "|" + VATInModel.StatusApprovalXML + "|" + VATInModel.StatusFakturXML + "|";
            string detailDataPiped = string.Empty;

            List<SyncDataDetailViewModel> model = new List<SyncDataDetailViewModel>();

            int i = 1;
            foreach (VATInDetailManualInputViewModel vatInDetail in VATInModel.VATInDetails)
            {
                detailDataPiped =
                    vatInDetail.UnitName + "|" + vatInDetail.UnitPrice + "|" +
                    vatInDetail.Quantity + "|" + vatInDetail.TotalPrice + "|" +
                    vatInDetail.Discount + "|" + vatInDetail.DPP + "|" +
                    vatInDetail.PPN + "|" + vatInDetail.TarifPPNBM + "|" + vatInDetail.PPNBM;

                SyncDataDetailViewModel detailModel = new SyncDataDetailViewModel();
                detailModel.RowId = i + 1;
                detailModel.RowData = headerDataPiped + detailDataPiped;
                detailModel.SyncStatus = true;

                model.Add(detailModel);
                i++;
            }

            return model;
        }
        #endregion
    }
}
#endregion