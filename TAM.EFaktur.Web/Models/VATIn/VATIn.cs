﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.VATIn
{
    public class VATIn : BaseModel
    {
        public Guid SyncId { get; set; }
        public Guid KDJenisTransaksiID { get; set; }
        public String KDJenisTransaksi { get; set; }
        public String FGPengganti { get; set; }
        public String NomorFaktur { get; set; }
        public String NomorFakturGabungan { get; set; }
        public DateTime TanggalFaktur { get; set; }
        public String NPWPPenjual { get; set; }
        public String NamaPenjual { get; set; }
        public String AlamatPenjual { get; set; }
        public string NPWPLawanTransaksi { get; set; }
        public Decimal JumlahDPP { get; set; }
        public Decimal JumlahPPN { get; set; }
        public Decimal JumlahPPNBM { get; set; }
        public String StatusApprovalXML { get; set; }
        public String StatusFakturXML { get; set; }
        public Boolean IsCreditable { get; set; }
        public String TransitoryStatus { get; set; }
        public Nullable<DateTime> TransitoryDate { get; set; }
        public String DownloadStatus { get; set; }
        public String ApprovalStatus { get; set; }
        public Nullable<DateTime> ApprovalTime { get; set; }
        public String RecordStatus { get; set; }
        public Nullable<DateTime> RecordTime { get; set; }
        public String FakturType { get; set; }
        public String PdfUrl { get; set; }
        public String BatchFileName { get; set; }
        public Boolean DeleteStatus { get; set; }
        public Boolean IsApproveOrReject { get; set; }
        public String NomorInvoice { get; set; }
        public Nullable<DateTime> TanggalInvoice { get; set; }
        public Nullable<Decimal> JumlahPPH22 { get; set; }
        public String PVNumber { get; set; }
        public String PVCreatedBy { get; set; }
        public String InvoiceStatus { get; set; }
        public String SAPDOCNumber { get; set; }
        public Nullable<int> RemainingDays { get; set; }
        public Nullable<DateTime> PostingDate { get; set; }
        public String RelatedGLAccount { get; set; }
        public String Keterangan { get; set; }
        public DateTime ExpiredDate { get; set; }
        public String ApprovalDescription { get; set; }
        public dynamic MapFromModel(DateTime? EventDate = null, string EventActor = "")
        {
            dynamic args = new { 
                Id = this.Id,
                SyncId = this.SyncId,
                KDJenisTransaksiID = this.KDJenisTransaksiID,
                KDJenisTransaksi = this.KDJenisTransaksi,
                FGPengganti = this.FGPengganti,
                NomorFaktur = this.NomorFaktur,
                NomorFakturGabungan = this.NomorFakturGabungan,
                TanggalFaktur = this.TanggalFaktur.FormatSQLDateTime(),
                NPWPPenjual = this.NPWPPenjual,
                NamaPenjual = this.NamaPenjual,
                AlamatPenjual = this.AlamatPenjual,
                JumlahDPP = this.JumlahDPP,
                JumlahPPN = this.JumlahPPN,
                JumlahPPNBM = this.JumlahPPNBM,
                StatusApprovalXML = this.StatusApprovalXML,
                StatusFakturXML = this.StatusFakturXML,
                IsCreditable = this.IsCreditable,
                TransitoryStatus = string.IsNullOrEmpty(this.TransitoryStatus) ? Formatting.NOT_AVAILABLE : this.TransitoryStatus,
                TransitoryDate = this.TransitoryDate,
                DownloadStatus = string.IsNullOrEmpty(this.DownloadStatus) ? Formatting.NOT_AVAILABLE : this.DownloadStatus,
                ApprovalStatus = string.IsNullOrEmpty(this.ApprovalStatus) ? Formatting.NOT_AVAILABLE : this.ApprovalStatus,
                ApprovalTime = this.ApprovalTime,
                RecordStatus = string.IsNullOrEmpty(this.RecordStatus) ? Formatting.NOT_AVAILABLE : this.RecordStatus,
                RecordTime = this.RecordTime,
                FakturType = this.FakturType,
                PdfUrl = this.PdfUrl,
                BatchFileName = this.BatchFileName,
                DeleteStatus = this.DeleteStatus,
                IsApproveOrReject = this.IsApproveOrReject,
                CreatedOn = this.CreatedOn.Value.FormatSQLDateTime(),
                CreatedBy = this.CreatedBy,
                RowStatus = false,
                EventDate = EventDate.HasValue ? EventDate.Value : DateTime.Now,
                EventActor = EventActor,
                NomorInvoice = this.NomorInvoice,
                TanggalInvoice = TanggalInvoice.HasValue ? TanggalInvoice.Value : TanggalInvoice,
                JumlahPPH22 = this.JumlahPPH22,
                PVNumber = this.PVNumber,
                PVCreatedBy = this.PVCreatedBy,
                InvoiceStatus = this.InvoiceStatus,
                SAPDOCNumber = this.SAPDOCNumber,
                RemainingDays = this.RemainingDays,
                PostingDate = PostingDate.HasValue ? PostingDate.Value : this.PostingDate,
                RelatedGLAccount = this.RelatedGLAccount,
                Keterangan = this.Keterangan,
                ExpiredDate = this.ExpiredDate,
                ApprovalDescription = this.ApprovalDescription
            };
            return args;
        }
    }
}