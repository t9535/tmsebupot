﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.Master_Config
{
    public class MasterConfigDashboardViewModel
    {
        public int RowNum { get; set; }
        public Guid Id { get; set; }
        public string ConfigKey { get; set; }
        public string ConfigValue { get; set; }

    }
}