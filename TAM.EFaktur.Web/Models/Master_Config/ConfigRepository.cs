﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TAM.EFaktur.Web.Models.Master_Config
{
    public class ConfigRepository : BaseRepository
    {
       #region Singleton
        private ConfigRepository() { }
        private static ConfigRepository instance = null;
        public static ConfigRepository Instance
        {
            get 
            {
                if (instance == null)
                {
                    instance = new ConfigRepository();
                }
                return instance;
            }
        }
        #endregion

        #region Counter Data
        public int Count(string ConfigName, string configValue)
        {
            dynamic args = new
            {
                ConfigName = ConfigName,
                configValue = configValue,
                
            };

            return db.SingleOrDefault<int>("Config/countApplications", args); 
        }
        #endregion

        #region Processing Data

        public List<Config> GetList(string ConfigKey,string ConfigValue)
        {
            dynamic args = new
            {
                ConfigKey = ConfigKey,
                ConfigValue = ConfigValue
               
            };
            IEnumerable<Config> result = db.Query<Config>("Config/nosqlyet", args);
            return result.ToList();
        }

        public Config GetById(Guid Id)
        {
            return db.SingleOrDefault<Config>("Config/usp_GetConfigByID", new { Id = Id });
        }

        public Result Save(Config objConfig, DateTime EventDate, string EventActor)
        {
            try
            {
                dynamic args = new
                {
                    Id= objConfig.Id,
                    ConfigKey = objConfig.ConfigKey,
                    ConfigValue = objConfig.ConfigValue,
                    RowStatus           = objConfig.RowStatus,
                    EventDate           = EventDate,
                    EventActor          = EventActor                
                };
                db.Execute("Config/usp_SaveConfig", args);
                return MsgResultSuccessInsert();
            }
            catch (Exception e)
            {
                return MsgResultError(e.Message.ToString());
            }
        }

        public Result Delete(Guid Id)
        {
            try
            {
                db.Execute("Config/usp_DeleteConfigByID", new { Id = Id });
                return MsgResultSuccessDelete();
            }
            catch (Exception e)
            {
                return MsgResultError(e.Message.ToString());
            }
        }
        #endregion

    }
}
