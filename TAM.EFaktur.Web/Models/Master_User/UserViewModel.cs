﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.Master_User
{
    public class UserViewModel
    {
        public int RowNum { get; set; }
        public Guid Id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string DivisionName { get; set; }
        public string RoleName { get; set; }       
    }
}