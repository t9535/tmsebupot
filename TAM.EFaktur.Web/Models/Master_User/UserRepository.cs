﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TAM.EFaktur.Web.Models.Master_User
{
    public class UserRepository : BaseRepository
    {
        #region Singleton
        private UserRepository() { }
        private static UserRepository instance = null;
        public static UserRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new UserRepository();
                }
                return instance;
            }
        }
        #endregion

        #region Counter Data
        public int Count(UserSearchParamViewModel model)
        {
            dynamic args = new
            {
                Username = model.Username,
                Email = model.Email,
                DivisionId = model.DivisionId,
                RoleId = model.RoleId
            };

            return db.SingleOrDefault<int>("Master_User/usp_CountUserListDashboard", args);
        }

        #endregion

        #region Processing Data GetList for Search

        //list data for search
        public List<UserViewModel> GetList(UserSearchParamViewModel model, int SortBy, string SortDirection, int FromNumber, int ToNumber)
    
        {
            dynamic args = new
            {
             
                Username = model.Username,
                Email = model.Email,
                DivisionId = model.DivisionId,
                RoleId = model.RoleId,
                SortBy = SortBy,
                SortDirection = SortDirection,
                FromNumber = FromNumber,
                ToNumber = ToNumber

            };
            IEnumerable<UserViewModel> result = db.Query<UserViewModel>("Master_User/usp_GetUserListDashboard", args);
            return result.ToList();
        }

        public UserUpdateViewModel GetById(Guid Id)
        {
            return db.SingleOrDefault<UserUpdateViewModel>("Master_User/usp_GetUserByID", new { Id = Id });
        }

        public List<DropdownViewModel> GetUserListDropdown(string username,bool isFullAccess)
        {
            dynamic args = new
            {
                username = username,
                isFullAccess = isFullAccess
            };

            IEnumerable<DropdownViewModel> result = db.Query<DropdownViewModel>("Master_User/usp_GetUserListDropdown",args);
            return result.ToList();
        }
        //Insert New User
        public Result Insert(UserCreateUpdate model, string EventActor, DateTime EventDate)
        {
            try
            {
                var UserId = Guid.NewGuid();
                dynamic args = new
                {
                    Id = UserId,
                    Username = model.Username,
                    Email = model.Email,
                    DivisionId = model.DivisionId,
                    RoleId = model.RoleId,
                    EventDate = EventDate.FormatSQLDate(),
                    EventActor = EventActor,

                };
                db.Execute("Master_User/usp_Insert_TB_M_User", args);

                //dynamic argsRole = new
                //{
                //    Id = Guid.NewGuid(),
                //    UserId = UserId,
                //    RoleId = model.RoleId,
                //    EventDate = EventDate.FormatSQLDate(),
                //    EventActor = EventActor,

                //};
                //db.Execute("Master_User/usp_Insert_TB_M_UserRole", argsRole);

                return MsgResultSuccessInsert();
            }
            catch (Exception e)
            {
                return MsgResultError(e.Message.ToString());
            }
        }
        //Update User
        public Result Update(UserUpdateViewModel model, string EventActor, DateTime EventDate)
        {
            try
            {
                //var UserId = Guid.NewGuid();
                
                dynamic args = new
                {
                    Id = model.Id,
                    Username = model.Username,
                    Email = model.Email,
                    //DivisionId = model.DivisionId,
                    //RoleId = model.RoleId,
                    EventDate = EventDate.FormatSQLDate(),
                    EventActor = EventActor,

                };
                db.Execute("Master_User/usp_Update_TB_M_User", args);

                //dynamic argsRole = new
                //{
                //   UserId = model.UserId,
                //    RoleId = model.RoleId,
                //    EventDate = EventDate.FormatSQLDate(),
                //    EventActor = EventActor,

                //};
                //db.Execute("Master_User/usp_Update_TB_M_UserRole", argsRole);

                return MsgResultSuccessUpdate();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                return MsgResultError(e.Message.ToString());
            }
        }

        //Delete User
        public Result Delete(UserUpdateViewModel model)
        {
            try
            {
                
                //dynamic argsRole = new
                //{
                //    UserId = model.UserId
                //};
                //db.Execute("Master_User/usp_Delete_TB_M_UserRole", argsRole);

                dynamic args = new
                {
                    Id = model.Id
                };
                db.Execute("Master_User/usp_Delete_TB_M_User", args);
                return MsgResultSuccessDelete();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                return MsgResultError(e.Message.ToString());
            }
        }
        //public Result Delete(Guid Id)
        //{
        //    try
        //    {
        //        db.Execute("Master_User/usp_DeleteUserByID", new { Id = Id });
        //        return MsgResultSuccessDelete();
        //    }
        //    catch (Exception e)
        //    {
        //        return MsgResultError(e.Message.ToString());
        //    }
        //}
        #endregion


    }
}
