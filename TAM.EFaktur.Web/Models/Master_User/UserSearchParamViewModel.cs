﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.Master_User
{
    public class UserSearchParamViewModel
    {
        public string Username { get; set; }
        public string Email { get; set; }
        public string DivisionId { get; set; }
        public string RoleId { get; set; }
        
    }
}