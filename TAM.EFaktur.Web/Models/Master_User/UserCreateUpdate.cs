﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.Master_User
{
    public class UserCreateUpdate 
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string DivisionId { get; set; }     
        public string RoleId { get; set; }      
        public DateTime EventDate { get; set; }
        public string EventActor { get; set; }
        
        //public List<Nullable<Guid>> RoleIds { get; set; }
    }
}