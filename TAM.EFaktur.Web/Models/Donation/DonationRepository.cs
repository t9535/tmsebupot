﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TAM.EFaktur.Web.Models.Log_Sync;
using TAM.EFaktur.Web.Models.Master_Config;
using System.Text.RegularExpressions;
using MigraDoc.DocumentObjectModel;
using TAM.EFaktur.Web.Models.Dashboard_Nominative;
using TAM.EFaktur.Web.Models.Master_DataFieldEnum;
using QRCoder;
using System.Drawing;
//using Spire.Doc.Documents;
//using Spire.Doc.Fields;
using Microsoft.Reporting.WebForms;
using System.Drawing.Printing;

namespace TAM.EFaktur.Web.Models.Donation
{
    public class DonationRepository : BaseRepository
    {
        #region Singleton
        private DonationRepository() { }
        private static DonationRepository instance = null;
        public static DonationRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new DonationRepository();
                }
                return instance;
            }
        }
        #endregion

        #region Counter Data
        public int Count(Guid DashboardId)
        {
            dynamic args = new
            {
                DashboardId = DashboardId
            };
            return db.SingleOrDefault<int>("Donation/usp_CountDonation", args);
        }

        #endregion

        #region Processing Data GetList for Search
        //list data for search
        public List<Donation> GetList(Guid DashboardId, int SortBy, string SortDirection, int FromNumber, int ToNumber)
        {
            dynamic args = new
            {
                DashboardId = DashboardId,
                SortBy = SortBy,
                SortDirection = SortDirection,
                FromNumber = FromNumber,
                ToNumber = ToNumber
            };
            IEnumerable<Donation> result = db.Query<Donation>("Donation/usp_GetDonation", args);
            return result.ToList();
        }

        public Donation GetById(Guid Id)
        {
            return db.SingleOrDefault<Donation>("Donation/usp_GetDonationByID", new { ID = Id });
        }

        public Donation GetByDashboardId(Guid DashboardId)
        {
            return db.SingleOrDefault<Donation>("Donation/usp_GetDonationByDashboardID", new { DashboardId = DashboardId });
        }

        public DonationValidation GetValidationDataFillByDashboardId(Guid DashboardId)
        {
            return db.SingleOrDefault<DonationValidation>("Dashboard_Nominative/usp_GetValidationDataFillByDashboardID", new { DashboardId = DashboardId });
        }

        public Result Insert(Donation model, string EventActor)
        {
            try
            {
                db.Execute("Donation/usp_InsertOrUpdate_TB_R_Donation", model.MapFromModel(DateTime.Now, EventActor));
                return MsgResultSuccessInsert();
            }
            catch (Exception e)
            {
                return MsgResultError(e.LogToApp("Create Donation", MessageType.ERR, EventActor).Message);
            }
        }

        public Result Update(Donation model, string EventActor)
        {
            try
            {
                db.Execute("Donation/usp_InsertOrUpdate_TB_R_Donation", model.MapFromModel(DateTime.Now, EventActor));
                return MsgResultSuccessUpdate();
            }
            catch (Exception e)
            {
                return MsgResultError(e.LogToApp("Update Donation", MessageType.ERR, EventActor).Message);
            }
        }

        public Result Delete(Donation model, string EventActor)
        {
            try
            {
                db.Execute("Donation/usp_DeleteDonationByID", new { ID = model.DONATION_ID });
                return MsgResultSuccessDelete();
            }
            catch (Exception e)
            {
                return MsgResultError(e.LogToApp("Delete Donation", MessageType.ERR, EventActor).Message);
            }
        }

        public Result SubmitHeader(Donation model, string EventActor, string path, string PathTemplate)
        {
            try
            {
                db.Execute("Donation/usp_SubmitHeader_TB_R_Donation", model.MapFromModelHeader(DateTime.Now, EventActor));
                SaveFile(path, PathTemplate, model.DASHBOARD_ID);
                return MsgResultSuccessUpdate();
            }
            catch (Exception e)
            {
                return MsgResultError(e.LogToApp("Submit Donation", MessageType.ERR, EventActor).Message);
            }
        }

        public Result ExcelDonationInsert(string path, string EventActor, Donation data)
        {
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = "File has been uploaded successfully"
            };

            FileStream file = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite);

            HSSFWorkbook hssfwb = new HSSFWorkbook(file);
            ISheet sheet = hssfwb.GetSheet("Default");

            string pipedString = string.Empty;

            int row = 0;
            int rowFail = 0;
            int rowSuccess = 0;
            var cellNum = 15;//jumlah header cell +1;
            var rowNum = 3;

            try
            {
                file.Close();
                Donation model = new Donation();
                var validationDataFill = GetValidationDataFillByDashboardId((Guid)data.DASHBOARD_ID);

                List<int> exceptionCellNum = new List<int>();
                //exceptionCellNum.Add(17); 

                //var config = MasterConfigRepository.Instance.GetByConfigKey("UrlTemplateDonation");
                //if (config == null)
                //{
                //    throw new Exception("Please define the UrlTemplateDonation Config");
                //}

                List<LogSyncCreateUpdate> logSyncList = new List<LogSyncCreateUpdate>();
                List<string> failTaxNumber = new List<string>();

                if (sheet.GetRow(rowNum - 1) == null)
                {
                    result.ResultCode = false;
                    result.ResultDesc = "Invalid template donation.";
                    return result;
                }

                if (sheet.GetRow(rowNum - 1).LastCellNum != cellNum)
                {
                    result.ResultCode = false;
                    result.ResultDesc = "Invalid template donation.";
                    return result;
                }

                for (row = rowNum; row <= sheet.LastRowNum; row++)
                {
                    #region Set Cell Policy, Cell Type and Save Sync Data
                    pipedString = string.Empty;
                    for (int i = 0; i < cellNum; i++)
                    {
                        sheet.GetRow(row).GetCell(i, MissingCellPolicy.CREATE_NULL_AS_BLANK);
                        sheet.GetRow(row).GetCell(i).SetCellType(CellType.STRING);

                        pipedString += NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(i)) + "|";
                    }
                    pipedString = pipedString.Remove(pipedString.Length - 1);
                    #endregion

                    try
                    {
                        #region Cell Mapping & Insert to DB 
                        db.BeginTransaction();

                        string validation = "", NPWPFormat = null;
                        DateTime? TANGGAL_SUMBANGAN = null;
                        decimal? NILAI_SUMBANGAN = null, BIAYA_PEMBANGUNAN_INFRASTRUKTUR = null;

                        var JENIS_SUMBANGAN = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(0));
                        var BENTUK_SUMBANGAN = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(1));
                        var NILAI_SUMBANGAN_STRING = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(2));
                        var TANGGAL_SUMBANGAN_STRING = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(3));
                        var NAMA_LEMBAGA_PENERIMA = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(4));
                        var NPWP_LEMBAGA_PENERIMA = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(5));
                        var ALAMAT_LEMBAGA = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(6));
                        var NO_TELP_LEMBAGA = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(7));
                        var SARANA_PRASARANA_INFRASTRUKTUR = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(8));
                        var LOKASI_INFRASTRUKTUR = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(9));
                        var IZIN_MENDIRIKAN_BANGUNAN = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(10));
                        var NOMOR_REKENING = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(11));
                        var NAMA_REKENING_PENERIMA = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(12));
                        var NAMA_BANK = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(13));
                        var BIAYA_PEMBANGUNAN_INFRASTRUKTUR_STRING = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(14));
                        //var NO_KTP = sheet.GetRow(row).GetCell(14));

                        #region Validation
                        if (string.IsNullOrEmpty(JENIS_SUMBANGAN) && string.IsNullOrEmpty(BENTUK_SUMBANGAN) && string.IsNullOrEmpty(NILAI_SUMBANGAN_STRING) && string.IsNullOrEmpty(TANGGAL_SUMBANGAN_STRING) &&
                           string.IsNullOrEmpty(NAMA_LEMBAGA_PENERIMA) && string.IsNullOrEmpty(NPWP_LEMBAGA_PENERIMA) && string.IsNullOrEmpty(ALAMAT_LEMBAGA) && string.IsNullOrEmpty(NO_TELP_LEMBAGA) &&
                           string.IsNullOrEmpty(SARANA_PRASARANA_INFRASTRUKTUR) && string.IsNullOrEmpty(LOKASI_INFRASTRUKTUR) && string.IsNullOrEmpty(IZIN_MENDIRIKAN_BANGUNAN) && string.IsNullOrEmpty(NOMOR_REKENING) &&
                           string.IsNullOrEmpty(NAMA_REKENING_PENERIMA) && string.IsNullOrEmpty(NAMA_BANK) && string.IsNullOrEmpty(BIAYA_PEMBANGUNAN_INFRASTRUKTUR_STRING)

                           )
                        {
                            break;
                        }

                        var header = GetHeaderDonation();
                        //Nullale if (validationDataFill.JENIS_SUMBANGAN) { if (string.IsNullOrEmpty(JENIS_SUMBANGAN)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Jenis Sumbangan can't be empy"; } }
                        if (validationDataFill.BENTUK_SUMBANGAN) { if (string.IsNullOrEmpty(BENTUK_SUMBANGAN)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.BENTUK_SUMBANGAN} can't be empy"; } }
                        if (validationDataFill.NILAI_SUMBANGAN) { if (string.IsNullOrEmpty(NILAI_SUMBANGAN_STRING)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.NILAI_SUMBANGAN} can't be empy"; } }
                        if (validationDataFill.TANGGAL_SUMBANGAN) { if (string.IsNullOrEmpty(TANGGAL_SUMBANGAN_STRING)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.TANGGAL_SUMBANGAN} can't be empy"; } }
                        if (validationDataFill.NAMA_LEMBAGA_PENERIMA) { if (string.IsNullOrEmpty(NAMA_LEMBAGA_PENERIMA)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.NAMA_LEMBAGA_PENERIMA} can't be empy"; } }
                        if (validationDataFill.NPWP_LEMBAGA_PENERIMA) { if (string.IsNullOrEmpty(NPWP_LEMBAGA_PENERIMA)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.NPWP_LEMBAGA_PENERIMA} can't be empy"; } }
                        if (validationDataFill.ALAMAT_LEMBAGA) { if (string.IsNullOrEmpty(ALAMAT_LEMBAGA)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.ALAMAT_LEMBAGA} can't be empy"; } }
                        if (validationDataFill.NO_TELP_LEMBAGA) { if (string.IsNullOrEmpty(NO_TELP_LEMBAGA)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.NO_TELP_LEMBAGA} can't be empy"; } }
                        if (validationDataFill.SARANA_PRASARANA_INFRASTRUKTUR) { if (string.IsNullOrEmpty(SARANA_PRASARANA_INFRASTRUKTUR)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.SARANA_PRASARANA_INFRASTRUKTUR} can't be empy"; } }
                        if (validationDataFill.LOKASI_INFRASTRUKTUR) { if (string.IsNullOrEmpty(LOKASI_INFRASTRUKTUR)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.LOKASI_INFRASTRUKTUR} can't be empy"; } }
                        if (validationDataFill.BIAYA_PEMBANGUNAN_INFRASTRUKTUR) { if (string.IsNullOrEmpty(BIAYA_PEMBANGUNAN_INFRASTRUKTUR_STRING)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.BIAYA_PEMBANGUNAN_INFRASTRUKTUR} can't be empy"; } }
                        if (validationDataFill.IZIN_MENDIRIKAN_BANGUNAN) { if (string.IsNullOrEmpty(IZIN_MENDIRIKAN_BANGUNAN)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.IZIN_MENDIRIKAN_BANGUNAN} can't be empy"; } }
                        if (validationDataFill.NOMOR_REKENING) { if (string.IsNullOrEmpty(NOMOR_REKENING)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.NOMOR_REKENING} can't be empy"; } }
                        if (validationDataFill.NAMA_REKENING_PENERIMA) { if (string.IsNullOrEmpty(NAMA_REKENING_PENERIMA)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.NAMA_REKENING_PENERIMA} can't be empy"; } }
                        if (validationDataFill.NAMA_BANK) { if (string.IsNullOrEmpty(NAMA_BANK)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.NAMA_BANK} can't be empy"; } }
                        //if (validationDataFill.NO_KTP) { if (string.IsNullOrEmpty(NO_KTP)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "No KTP can't be empy"; } }

                        //must be less than or equal to
                        if (!string.IsNullOrEmpty(JENIS_SUMBANGAN)) { if (JENIS_SUMBANGAN.Length >= 100) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.JENIS_SUMBANGAN} must be less than or equal to 100 character"; } }
                        if (!string.IsNullOrEmpty(BENTUK_SUMBANGAN)) { if (BENTUK_SUMBANGAN.Length >= 50) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.BENTUK_SUMBANGAN} must be less than or equal to 50 character"; } }
                        //if (!string.IsNullOrEmpty(NILAI_SUMBANGAN_STRING)) { if (NILAI_SUMBANGAN_STRING.Length >= 18) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Nilai Sumbangan must be less than or equal to 18 character"; } }
                        //if (!string.IsNullOrEmpty(TANGGAL_SUMBANGAN_STRING)) { if (TANGGAL_SUMBANGAN_STRING.Length >= 10) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Tanggal Sumbangan must be less than or equal to 10 character"; } }
                        if (!string.IsNullOrEmpty(NAMA_LEMBAGA_PENERIMA)) { if (NAMA_LEMBAGA_PENERIMA.Length >= 100) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.NAMA_LEMBAGA_PENERIMA} must be less than or equal to 100 character"; } }
                        if (!string.IsNullOrEmpty(NPWP_LEMBAGA_PENERIMA)) { if (NPWP_LEMBAGA_PENERIMA.Length > 15 || NPWP_LEMBAGA_PENERIMA.Length < 15) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.NPWP_LEMBAGA_PENERIMA} must be equal to 15 character"; } }
                        if (!string.IsNullOrEmpty(ALAMAT_LEMBAGA)) { if (ALAMAT_LEMBAGA.Length >= 200) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.ALAMAT_LEMBAGA} must be less than or equal to 200 character"; } }
                        if (!string.IsNullOrEmpty(NO_TELP_LEMBAGA)) { if (NO_TELP_LEMBAGA.Length >= 20) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.NO_TELP_LEMBAGA} must be less than or equal to 20 character"; } }
                        if (!string.IsNullOrEmpty(SARANA_PRASARANA_INFRASTRUKTUR)) { if (SARANA_PRASARANA_INFRASTRUKTUR.Length >= 100) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.SARANA_PRASARANA_INFRASTRUKTUR} must be less than or equal to 100 character"; } }
                        if (!string.IsNullOrEmpty(LOKASI_INFRASTRUKTUR)) { if (LOKASI_INFRASTRUKTUR.Length >= 200) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.LOKASI_INFRASTRUKTUR} must be less than or equal to 200 character"; } }
                        //if (!string.IsNullOrEmpty(BIAYA_PEMBANGUNAN_INFRASTRUKTUR_STRING)) { if (BIAYA_PEMBANGUNAN_INFRASTRUKTUR_STRING.Length >= 18) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Biaya Pembangunan Infrastruktur must be less than or equal to 18 character"; } }
                        if (!string.IsNullOrEmpty(IZIN_MENDIRIKAN_BANGUNAN)) { if (IZIN_MENDIRIKAN_BANGUNAN.Length >= 200) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.IZIN_MENDIRIKAN_BANGUNAN} must be less than or equal to 200 character"; } }
                        if (!string.IsNullOrEmpty(NOMOR_REKENING)) { if (NOMOR_REKENING.Length >= 20) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.NOMOR_REKENING} must be less than or equal to 20 character"; } }
                        if (!string.IsNullOrEmpty(NAMA_REKENING_PENERIMA)) { if (NAMA_REKENING_PENERIMA.Length >= 100) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.NAMA_REKENING_PENERIMA} must be less than or equal to 100 character"; } }
                        if (!string.IsNullOrEmpty(NAMA_BANK)) { if (NAMA_BANK.Length >= 100) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.NAMA_BANK} must be less than or equal to 100 character"; } }
                        //if (!string.IsNullOrEmpty(NO_KTP)) { if (NO_KTP.Length >= 16) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "No KTP must be less than or equal to 16 character"; } }

                        if (!string.IsNullOrEmpty(NPWP_LEMBAGA_PENERIMA))
                        {
                            if (!decimal.TryParse(NPWP_LEMBAGA_PENERIMA, out decimal NPWPNUMBER)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.NPWP_LEMBAGA_PENERIMA} Valid format numeric type"; }
                            if (NPWP_LEMBAGA_PENERIMA.Length == 15)
                            {
                                //Regex npwpFormat = new Regex(@"^[0-9]{2}[.][\d]{3}[.][\d]{3}[.][\d][-][\d]{3}[.][\d]{3}$"); 
                                //if (!npwpFormat.IsMatch(NPWP_LEMBAGA_PENERIMA)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "NPWP Lembaga Penerima format must be 00.000.000.0-000.000"; } 
                                NPWPFormat = $"{NPWP_LEMBAGA_PENERIMA.Substring(0, 2)}.{NPWP_LEMBAGA_PENERIMA.Substring(2, 3)}.{NPWP_LEMBAGA_PENERIMA.Substring(5, 3)}.{NPWP_LEMBAGA_PENERIMA.Substring(8, 1)}-{NPWP_LEMBAGA_PENERIMA.Substring(9, 3)}.{NPWP_LEMBAGA_PENERIMA.Substring(12, 3)}";
                            }
                        }

                        if (!string.IsNullOrEmpty(TANGGAL_SUMBANGAN_STRING))
                        {
                            DateTime TANGGAL_SUMBANGAN1;
                            if (!DateTime.TryParseExact(TANGGAL_SUMBANGAN_STRING, NPOIExcel.FormatDate, CultureInfo.InvariantCulture, DateTimeStyles.None, out TANGGAL_SUMBANGAN1))
                            {
                                double TANGGAL_DOUBLE;
                                if (!double.TryParse(TANGGAL_SUMBANGAN_STRING, out TANGGAL_DOUBLE))
                                {
                                    if (!string.IsNullOrEmpty(validation)) validation += ", ";
                                    validation += $"{header.TANGGAL_SUMBANGAN} format must be DD/MM/YYYY";
                                }
                                else
                                {
                                    TANGGAL_SUMBANGAN = DateTime.FromOADate(TANGGAL_DOUBLE);
                                }
                            }
                            else
                            {
                                TANGGAL_SUMBANGAN = TANGGAL_SUMBANGAN1;
                            }
                        }

                        if (!string.IsNullOrEmpty(NILAI_SUMBANGAN_STRING))
                        {
                            Decimal NILAI_SUMBANGAN1;
                            if (!decimal.TryParse(NILAI_SUMBANGAN_STRING, out NILAI_SUMBANGAN1)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.NILAI_SUMBANGAN} Valid format numeric type"; } else { NILAI_SUMBANGAN = NILAI_SUMBANGAN1; }
                        }

                        if (!string.IsNullOrEmpty(BIAYA_PEMBANGUNAN_INFRASTRUKTUR_STRING))
                        {
                            Decimal BIAYA_PEMBANGUNAN_INFRASTRUKTUR1;
                            if (!decimal.TryParse(BIAYA_PEMBANGUNAN_INFRASTRUKTUR_STRING, out BIAYA_PEMBANGUNAN_INFRASTRUKTUR1))
                            { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.BIAYA_PEMBANGUNAN_INFRASTRUKTUR} Valid format numeric type"; }
                            else { BIAYA_PEMBANGUNAN_INFRASTRUKTUR = BIAYA_PEMBANGUNAN_INFRASTRUKTUR1; }
                        }

                        if (!string.IsNullOrEmpty(NOMOR_REKENING))
                        {
                            if (!long.TryParse(NOMOR_REKENING, out long NO)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.NOMOR_REKENING} Valid format numeric type"; }
                        }

                        if (!string.IsNullOrEmpty(NO_TELP_LEMBAGA))
                        {
                            if (!long.TryParse(NO_TELP_LEMBAGA, out long NO)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.NO_TELP_LEMBAGA} Valid format numeric type"; }
                        }

                        if (!string.IsNullOrEmpty(validation))
                        {
                            throw new Exception(validation);
                        }
                        #endregion

                        model = new Donation();
                        model.DONATION_ID = Guid.NewGuid();
                        model.DASHBOARD_ID = data.DASHBOARD_ID;
                        model.THN_PAJAK = data.THN_PAJAK;
                        model.NAMA_PENANDATANGAN = data.NAMA_PENANDATANGAN;
                        model.TMP_PENANDATANGAN = data.TMP_PENANDATANGAN;
                        model.TGL_PENANDATANGAN = data.TGL_PENANDATANGAN;
                        model.JABATAN_PENANDATANGAN = data.JABATAN_PENANDATANGAN;
                        model.JENIS_SUMBANGAN = JENIS_SUMBANGAN;
                        model.BENTUK_SUMBANGAN = BENTUK_SUMBANGAN;
                        model.NILAI_SUMBANGAN = NILAI_SUMBANGAN;
                        model.TANGGAL_SUMBANGAN = TANGGAL_SUMBANGAN;
                        model.NAMA_LEMBAGA_PENERIMA = NAMA_LEMBAGA_PENERIMA;
                        model.NPWP_LEMBAGA_PENERIMA = NPWPFormat;
                        model.ALAMAT_LEMBAGA = ALAMAT_LEMBAGA;
                        model.NO_TELP_LEMBAGA = NO_TELP_LEMBAGA;
                        model.SARANA_PRASARANA_INFRASTRUKTUR = SARANA_PRASARANA_INFRASTRUKTUR;
                        model.LOKASI_INFRASTRUKTUR = LOKASI_INFRASTRUKTUR;
                        model.BIAYA_PEMBANGUNAN_INFRASTRUKTUR = BIAYA_PEMBANGUNAN_INFRASTRUKTUR;
                        model.IZIN_MENDIRIKAN_BANGUNAN = IZIN_MENDIRIKAN_BANGUNAN;
                        model.NOMOR_REKENING = NOMOR_REKENING;
                        model.NAMA_REKENING_PENERIMA = NAMA_REKENING_PENERIMA;
                        model.NAMA_BANK = NAMA_BANK;
                        //model.NO_KTP = NO_KTP;

                        // Save
                        result = db.SingleOrDefault<Result>("Donation/usp_InsertOrUpdate_TB_R_Donation", model.MapFromModel(DateTime.Now.AddSeconds(row), EventActor));

                        if (!result.ResultCode)
                        {
                            throw new Exception(result.ResultDesc);
                        }

                        logSyncList.Add(new LogSyncCreateUpdate()
                           .CreateModel(Path.GetFileName(path), row + 1, pipedString, MessageType.INF, ExceptionHandler.GENERAL_SUCCESS_MESSAGE, path));
                        rowSuccess++;
                        db.CommitTransaction();
                        #endregion
                    }
                    catch (Exception e)
                    {
                        db.AbortTransaction();
                        //result.ResultCode = false;
                        e.LogToSync(row + 1, pipedString, Path.GetFileName(path), path, MessageType.ERR);
                        rowFail++;
                    }
                }

                #region SyncLog Insert
                db.BeginTransaction();
                ExceptionHandler.LogToSync(logSyncList);
                db.CommitTransaction();
                #endregion

                if (rowFail == 0)
                {
                    result.ResultCode = true;
                }
                else
                {
                    result.ResultCode = false;
                }

                //result.ResultCode = true;
                result.ResultDesc = Formatting
                    .UPLOAD_NOTIFICATION
                    .Replace("@success", (rowSuccess).ToString())
                    .Replace("@error", (rowFail).ToString());
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result.ResultCode = false;
                result.ResultDesc = e.LogToSync(row - 3, pipedString, Path.GetFileName(path), path, MessageType.ERR).Message;
            }

            db.Close();
            return result;
        }

        public void SaveFile(string TempDownloadFolder, string PathTemplate, Guid? DashboardId)
        {
            dynamic args = new { DashboardId = DashboardId };

            List<DonationHeaderTemplate> header = new List<DonationHeaderTemplate>();
            header.Add(GetHeaderDonation());

            IEnumerable<DonationReport> list = db.Query<DonationReport>("Donation/usp_GetDonationFromPdf", args);
            var dashboard = DashboardRepository.Instance.GetById((Guid)DashboardId);
            string data = $"{dashboard.CategoryCode}-{dashboard.NominativeIDNo}";//_CategoryCodeProm + "-" + _TransactionCodeProm + "-" + _ReferenceNoProm;
            List<ImageData> imagedata = new List<ImageData>() { new ImageData { QrCode = DashboardRepository.Instance.GenerateQrCode(data) } };

            LocalReport report = new LocalReport();
            report.ReportPath = PathTemplate;
            report.DataSources.Add(new ReportDataSource() { Name = "DataSetDonationHeader", Value = header });
            report.DataSources.Add(new ReportDataSource() { Name = "DataSetDonationDetail", Value = list });
            report.DataSources.Add(new ReportDataSource() { Name = "DataSetQrCode", Value = imagedata });
            report.Dispose();

            Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string extension;

            //PDF 
            //var pathPdf = Path.Combine(TempDownloadFolder, $"Donation_{list.Select(x => x.REFERENCE_NO).FirstOrDefault() ?? DashboardId.ToString().ToUpper()}.pdf");
            //var pathPdf ="D:/Interface/Temp/Download/Donation_ND202107000180.pdf";
            //byte[] bytesPdf = report.Render("PDF", null, out mimeType, out encoding, out extension, out streamids, out warnings);
            //using (var fs = new FileStream(pathPdf, FileMode.Create, FileAccess.Write))
            //{
            //    fs.Write(bytesPdf, 0, bytesPdf.Length);
            //}

            //WORD 
            var path = Path.Combine(TempDownloadFolder, $"Donation_{list.Select(x => x.REFERENCE_NO).FirstOrDefault() ?? DashboardId.ToString().ToUpper()}.pdf");
            //var path = "D:/Interface/Temp/Download/Donation_ND202107000180.pdf";
            byte[] bytes = report.Render("PDF", null, out mimeType, out encoding, out extension, out streamids, out warnings);
            using (var fs = new FileStream(path, FileMode.Create, FileAccess.Write))
            {
                fs.Write(bytes, 0, bytes.Length);
            }

            dynamic argsUpdate = new { DashboardId = DashboardId, Path = path };
            db.Execute("Dashboard_Nominative/usp_UpdateDashboardFromPdf", argsUpdate);
        }

        //PROCESS GENERATE FILE SPIRE DOC
        //public void SaveFile(string TempDownloadFolder, string PathTemplate, Guid? DashboardId)
        //{
        //    //Load a template document 
        //    var MergeDoc = new Spire.Doc.Document();
        //    var header = GetHeaderDonation();

        //    dynamic args = new { DashboardId = DashboardId };
        //    IEnumerable<DonationReport> list = db.Query<DonationReport>("Donation/usp_GetDonationFromPdf", args);

        //    foreach (var obj in list)
        //    {
        //        var document = new Spire.Doc.Document(PathTemplate);
        //        //Replace Header 
        //        document.Replace("<HdJenisSumbangan>", header.JENIS_SUMBANGAN ?? "", false, true);
        //        document.Replace("<HdBentuksumbangan>", header.BENTUK_SUMBANGAN ?? "", false, true);
        //        document.Replace("<HdNilaiSumbangan>", header.NILAI_SUMBANGAN ?? "", false, true);
        //        document.Replace("<HdTanggalditerima>", header.TANGGAL_SUMBANGAN ?? "", false, true);

        //        document.Replace("<HdNamaLembaga>", header.NAMA_LEMBAGA_PENERIMA ?? "", false, true);
        //        document.Replace("<HdNPWPLembaga>", header.NPWP_LEMBAGA_PENERIMA ?? "", false, true);
        //        document.Replace("<HdAlamat>", header.ALAMAT_LEMBAGA ?? "", false, true);
        //        document.Replace("<HdNoTelp>", header.NO_TELP_LEMBAGA ?? "", false, true);

        //        document.Replace("<HdSaranPrasarana>", header.SARANA_PRASARANA_INFRASTRUKTUR ?? "", false, true);
        //        document.Replace("<HdLokasi>", header.LOKASI_INFRASTRUKTUR ?? "", false, true);
        //        document.Replace("<HdBiayaPembangunan>", header.BIAYA_PEMBANGUNAN_INFRASTRUKTUR ?? "", false, true);
        //        document.Replace("<HdIzinMendirikanBangunan>", header.IZIN_MENDIRIKAN_BANGUNAN ?? "", false, true);

        //        //Replace specified text with the other document
        //        document.Replace("<PVNo>", obj.PV_NO ?? "", false, true);
        //        document.Replace("<ReffNo>", obj.REFERENCE_NO ?? "", false, true);
        //        document.Replace("<NamaPemberiSumbangan>", obj.CompanyName ?? "", false, true);
        //        document.Replace("<AlamatPemberiSumbangan>", obj.CompanyAddress ?? "", false, true);
        //        document.Replace("<NPWPPemberiSumbangan>", obj.CompanyNPWP, false, true);

        //        document.Replace("<JenisSumbangan>", obj.JENIS_SUMBANGAN ?? "", false, true);
        //        document.Replace("<BentukSumbangan>", obj.BENTUK_SUMBANGAN ?? "", false, true);
        //        document.Replace("<NilaiSumbangan>", obj.NILAI_SUMBANGAN == null ? "-" : obj.NILAI_SUMBANGAN.Value.ToString("n2"), false, true);
        //        document.Replace("<TanggalTerima>", obj.TANGGAL_SUMBANGAN == null ? "-" : obj.TANGGAL_SUMBANGAN.Value.ToString("dd MMM yyyy"), false, true);

        //        document.Replace("<NamaPenerimaSumbangan>", obj.NAMA_LEMBAGA_PENERIMA ?? "", false, true);
        //        document.Replace("<NPWPPenerimaSumbangan>", obj.NPWP_LEMBAGA_PENERIMA ?? "", false, true);
        //        document.Replace("<AlamatPenerimaSumbangan>", obj.ALAMAT_LEMBAGA ?? "", false, true);
        //        document.Replace("<NoTelpPenerimaSumbangan>", obj.NO_TELP_LEMBAGA ?? "", false, true);

        //        document.Replace("<SaranPrasarana>", obj.SARANA_PRASARANA_INFRASTRUKTUR ?? "", false, true);
        //        document.Replace("<Lokasi>", obj.LOKASI_INFRASTRUKTUR ?? "", false, true);
        //        document.Replace("<BiayaPembangunan>", obj.BIAYA_PEMBANGUNAN_INFRASTRUKTUR == null ? "-" : obj.BIAYA_PEMBANGUNAN_INFRASTRUKTUR.Value.ToString("n2"), false, true);
        //        document.Replace("<IzinMendirikanBangunan>", obj.IZIN_MENDIRIKAN_BANGUNAN ?? "", false, true);

        //        document.Replace("<NamaKota>", obj.TMP_PENANDATANGAN ?? "", false, true);
        //        document.Replace("<Tanggal>", obj.TGL_PENANDATANGAN == null ? "-" : obj.TGL_PENANDATANGAN.Value.ToString("dd MMM yyyy"), false, true);
        //        document.Replace("<NamaPenandatangan>", obj.NAMA_PENANDATANGAN ?? "", false, true);
        //        document.Replace("<JabatanPenandatangan>", obj.JABATAN_PENANDATANGAN ?? "", false, true);

        //        //add new QR CODE
        //        QRCodeGenerator qrGenerator = new QRCodeGenerator();
        //        var dashboard = DashboardRepository.Instance.GetById((Guid)DashboardId);
        //        string data = $"{dashboard.CategoryCode}-{dashboard.NominativeIDNo}";//_CategoryCodeProm + "-" + _TransactionCodeProm + "-" + _ReferenceNoProm;
        //        QRCodeData qrCodeData = qrGenerator.CreateQrCode(data, QRCodeGenerator.ECCLevel.Q);
        //        QRCoder.QRCode qrCode = new QRCoder.QRCode(qrCodeData);
        //        Bitmap qrCodeImage = qrCode.GetGraphic(20);

        //        DocPicture picture = document.Sections[0].Paragraphs[0].AppendPicture(qrCodeImage);
        //        picture.HorizontalPosition = 150;
        //        picture.VerticalPosition = 500;

        //        //set image's size
        //        picture.Width = 130;
        //        picture.Height = 130;

        //        //set textWrappingStyle with image;
        //        picture.TextWrappingStyle = TextWrappingStyle.Behind;

        //        //TextSelection[] selections = document.FindAllString("<ImgQRCode>", true, true);
        //        //int index = 0;
        //        //TextRange range = null;

        //        //foreach (TextSelection selection in selections)
        //        //{
        //        //    DocPicture pic = new DocPicture(document);
        //        //    pic.LoadImage(qrCodeImage);
        //        //    pic.Width = 130;
        //        //    pic.Height = 130;
        //        //    range = selection.GetAsOneRange();
        //        //    index = range.OwnerParagraph.ChildObjects.IndexOf(range);
        //        //    range.OwnerParagraph.ChildObjects.Insert(index, pic);
        //        //    range.OwnerParagraph.ChildObjects.Remove(range);

        //        //}

        //        foreach (Spire.Doc.Section sec in document.Sections)
        //        {
        //            MergeDoc.Sections.Add(sec.Clone());
        //        }
        //    }

        //    //Save the file 
        //    var path = $"{TempDownloadFolder}/Donation_{list.Select(x => x.REFERENCE_NO).FirstOrDefault() ?? DashboardId.ToString().ToUpper()}.docx";
        //    MergeDoc.SaveToFile(path, Spire.Doc.FileFormat.Docx2013);

        //    dynamic argsUpdate = new { DashboardId = DashboardId, Path = path };
        //    db.Execute("Dashboard_Nominative/usp_UpdateDashboardFromPdf", argsUpdate);
        //}

        public DonationHeaderTemplate GetHeaderDonation()
        {
            var listHeader = DataFieldEnumRepository.Instance.GetOriginalById(new DataFieldEnum() { CategoryCode = EnumCategoryCode.Donation });
            var result = new DonationHeaderTemplate()
            {
                JENIS_SUMBANGAN = listHeader.Where(x => x.FieldNameByTable == nameof(DonationHeaderTemplate.JENIS_SUMBANGAN)).Select(x => x.FieldNameByExcel).FirstOrDefault(),
                BENTUK_SUMBANGAN = listHeader.Where(x => x.FieldNameByTable == nameof(DonationHeaderTemplate.BENTUK_SUMBANGAN)).Select(x => x.FieldNameByExcel).FirstOrDefault(),
                NILAI_SUMBANGAN = listHeader.Where(x => x.FieldNameByTable == nameof(DonationHeaderTemplate.NILAI_SUMBANGAN)).Select(x => x.FieldNameByExcel).FirstOrDefault(),
                TANGGAL_SUMBANGAN = listHeader.Where(x => x.FieldNameByTable == nameof(DonationHeaderTemplate.TANGGAL_SUMBANGAN)).Select(x => x.FieldNameByExcel).FirstOrDefault(),
                NAMA_LEMBAGA_PENERIMA = listHeader.Where(x => x.FieldNameByTable == nameof(DonationHeaderTemplate.NAMA_LEMBAGA_PENERIMA)).Select(x => x.FieldNameByExcel).FirstOrDefault(),
                NPWP_LEMBAGA_PENERIMA = listHeader.Where(x => x.FieldNameByTable == nameof(DonationHeaderTemplate.NPWP_LEMBAGA_PENERIMA)).Select(x => x.FieldNameByExcel).FirstOrDefault(),
                ALAMAT_LEMBAGA = listHeader.Where(x => x.FieldNameByTable == nameof(DonationHeaderTemplate.ALAMAT_LEMBAGA)).Select(x => x.FieldNameByExcel).FirstOrDefault(),
                NO_TELP_LEMBAGA = listHeader.Where(x => x.FieldNameByTable == nameof(DonationHeaderTemplate.NO_TELP_LEMBAGA)).Select(x => x.FieldNameByExcel).FirstOrDefault(),
                SARANA_PRASARANA_INFRASTRUKTUR = listHeader.Where(x => x.FieldNameByTable == nameof(DonationHeaderTemplate.SARANA_PRASARANA_INFRASTRUKTUR)).Select(x => x.FieldNameByExcel).FirstOrDefault(),
                LOKASI_INFRASTRUKTUR = listHeader.Where(x => x.FieldNameByTable == nameof(DonationHeaderTemplate.LOKASI_INFRASTRUKTUR)).Select(x => x.FieldNameByExcel).FirstOrDefault(),
                BIAYA_PEMBANGUNAN_INFRASTRUKTUR = listHeader.Where(x => x.FieldNameByTable == nameof(DonationHeaderTemplate.BIAYA_PEMBANGUNAN_INFRASTRUKTUR)).Select(x => x.FieldNameByExcel).FirstOrDefault(),
                IZIN_MENDIRIKAN_BANGUNAN = listHeader.Where(x => x.FieldNameByTable == nameof(DonationHeaderTemplate.IZIN_MENDIRIKAN_BANGUNAN)).Select(x => x.FieldNameByExcel).FirstOrDefault(),
                NOMOR_REKENING = listHeader.Where(x => x.FieldNameByTable == nameof(DonationHeaderTemplate.NOMOR_REKENING)).Select(x => x.FieldNameByExcel).FirstOrDefault(),
                NAMA_REKENING_PENERIMA = listHeader.Where(x => x.FieldNameByTable == nameof(DonationHeaderTemplate.NAMA_REKENING_PENERIMA)).Select(x => x.FieldNameByExcel).FirstOrDefault(),
                NAMA_BANK = listHeader.Where(x => x.FieldNameByTable == nameof(DonationHeaderTemplate.NAMA_BANK)).Select(x => x.FieldNameByExcel).FirstOrDefault(),
            };
            return result;
        }
        #endregion
    }
}
