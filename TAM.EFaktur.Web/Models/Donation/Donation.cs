﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TAM.EFaktur.Web.Models.Category_Nominative;

namespace TAM.EFaktur.Web.Models.Donation
{
    public class Donation
    {
        public Guid? DONATION_ID { get; set; }
        public Guid? DASHBOARD_ID { get; set; }

        public string CATEGORY_CODE { get; set; }

        public int? THN_PAJAK { get; set; }

        public string TMP_PENANDATANGAN { get; set; }

        public DateTime? TGL_PENANDATANGAN { get; set; }

        public string NAMA_PENANDATANGAN { get; set; }

        public string JABATAN_PENANDATANGAN { get; set; }

        public string JENIS_SUMBANGAN { get; set; }

        public string BENTUK_SUMBANGAN { get; set; }

        public decimal? NILAI_SUMBANGAN { get; set; }

        public DateTime? TANGGAL_SUMBANGAN { get; set; }

        public string NAMA_LEMBAGA_PENERIMA { get; set; }

        public string NPWP_LEMBAGA_PENERIMA { get; set; }

        public string ALAMAT_LEMBAGA { get; set; }

        public string NO_TELP_LEMBAGA { get; set; }

        public string SARANA_PRASARANA_INFRASTRUKTUR { get; set; }

        public string LOKASI_INFRASTRUKTUR { get; set; }

        public decimal? BIAYA_PEMBANGUNAN_INFRASTRUKTUR { get; set; }

        public string IZIN_MENDIRIKAN_BANGUNAN { get; set; }

        public string NOMOR_REKENING { get; set; }

        public string NAMA_REKENING_PENERIMA { get; set; }

        public string NAMA_BANK { get; set; }

        //public string NO_KTP { get; set; }

        public DateTime? CREATED_DT { get; set; }

        public string CREATED_BY { get; set; }

        public DateTime? CHANGED_DT { get; set; }

        public string CHANGED_BY { get; set; }

        public dynamic MapFromModel(DateTime? EventDate = null, string EventActor = "")
        {
            dynamic args = new
            {
                DONATION_ID = this.DONATION_ID,
                DASHBOARD_ID = this.DASHBOARD_ID,
                //CATEGORY_CODE = this.CATEGORY_CODE,
                THN_PAJAK = this.THN_PAJAK,
                TMP_PENANDATANGAN = this.TMP_PENANDATANGAN,
                TGL_PENANDATANGAN = this.TGL_PENANDATANGAN,
                NAMA_PENANDATANGAN = this.NAMA_PENANDATANGAN,
                JABATAN_PENANDATANGAN = this.JABATAN_PENANDATANGAN,
                JENIS_SUMBANGAN = this.JENIS_SUMBANGAN,
                BENTUK_SUMBANGAN = this.BENTUK_SUMBANGAN,
                NILAI_SUMBANGAN = this.NILAI_SUMBANGAN,
                TANGGAL_SUMBANGAN = this.TANGGAL_SUMBANGAN,
                NAMA_LEMBAGA_PENERIMA = this.NAMA_LEMBAGA_PENERIMA,
                NPWP_LEMBAGA_PENERIMA = this.NPWP_LEMBAGA_PENERIMA,
                ALAMAT_LEMBAGA = this.ALAMAT_LEMBAGA,
                NO_TELP_LEMBAGA = this.NO_TELP_LEMBAGA,
                SARANA_PRASARANA_INFRASTRUKTUR = this.SARANA_PRASARANA_INFRASTRUKTUR,
                LOKASI_INFRASTRUKTUR = this.LOKASI_INFRASTRUKTUR,
                BIAYA_PEMBANGUNAN_INFRASTRUKTUR = this.BIAYA_PEMBANGUNAN_INFRASTRUKTUR,
                IZIN_MENDIRIKAN_BANGUNAN = this.IZIN_MENDIRIKAN_BANGUNAN,
                NOMOR_REKENING = this.NOMOR_REKENING,
                NAMA_REKENING_PENERIMA = this.NAMA_REKENING_PENERIMA,
                NAMA_BANK = this.NAMA_BANK,
                //NO_KTP = this.NO_KTP,
                CREATED_DT = (EventDate.HasValue ? EventDate.Value : DateTime.Now).FormatSQLDateTime(),
                CREATED_BY = EventActor,
                CHANGED_DT = (EventDate.HasValue ? EventDate.Value : DateTime.Now).FormatSQLDateTime(),
                CHANGED_BY = EventActor

            };
            return args;
        }

        public dynamic MapFromModelHeader(DateTime? EventDate = null, string EventActor = "")
        {
            dynamic args = new
            {
                DASHBOARD_ID = this.DASHBOARD_ID,
                THN_PAJAK = this.THN_PAJAK,
                TMP_PENANDATANGAN = this.TMP_PENANDATANGAN,
                TGL_PENANDATANGAN = this.TGL_PENANDATANGAN,
                NAMA_PENANDATANGAN = this.NAMA_PENANDATANGAN,
                JABATAN_PENANDATANGAN = this.JABATAN_PENANDATANGAN,
                CREATED_DT = (EventDate.HasValue ? EventDate.Value : DateTime.Now).FormatSQLDateTime(),
                CREATED_BY = EventActor,
                CHANGED_DT = (EventDate.HasValue ? EventDate.Value : DateTime.Now).FormatSQLDateTime(),
                CHANGED_BY = EventActor

            };
            return args;
        }

        public bool? PositionSignature { get; set; }
    }

    public class DonationValidation
    {
        public bool JENIS_SUMBANGAN { get; set; }

        public bool BENTUK_SUMBANGAN { get; set; }

        public bool NILAI_SUMBANGAN { get; set; }

        public bool TANGGAL_SUMBANGAN { get; set; }

        public bool NAMA_LEMBAGA_PENERIMA { get; set; }

        public bool NPWP_LEMBAGA_PENERIMA { get; set; }

        public bool ALAMAT_LEMBAGA { get; set; }

        public bool NO_TELP_LEMBAGA { get; set; }

        public bool SARANA_PRASARANA_INFRASTRUKTUR { get; set; }

        public bool LOKASI_INFRASTRUKTUR { get; set; }

        public bool BIAYA_PEMBANGUNAN_INFRASTRUKTUR { get; set; }

        public bool IZIN_MENDIRIKAN_BANGUNAN { get; set; }

        public bool NOMOR_REKENING { get; set; }

        public bool NAMA_REKENING_PENERIMA { get; set; }

        public bool NAMA_BANK { get; set; }

        //public bool NO_KTP { get; set; }
    }
}
