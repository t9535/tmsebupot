﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TAM.EFaktur.Web.Models.WHTSummary
{
    public class WHTSummaryReportViewModel : Controller
    {
        public int RowNum { get; set; }
        public string KJSMapCode { get; set; }
        public string GLAccount { get; set; }
        public string GLDescription { get; set; }
        public string TaxArticle { get; set; }
        public string eSPTType { get; set; }
        public Nullable<decimal> TaxBasedAmount { get; set; }
        public Nullable<decimal> TaxAmount { get; set; }
        public DateTime DueDatePayment { get; set; }
        public Nullable<int> ETCPaymentDays { get; set; }
        public DateTime DueDateReporting { get; set; }
        public Nullable<int> ETCReportingDays { get; set; }
        public Nullable<decimal> TaxPayment { get; set; }
        public DateTime TaxPaymentDate { get; set; }
        public DateTime TaxReportingDate { get; set; }
        public string NTPN { get; set; }
        public string NTTE { get; set; }
        public Nullable<int> PlanVsActualPaymentDays { get; set; }
        public Nullable<int> PlanVsActualReportingDays { get; set; }
        public int MasaPajakBulan { get; set; }
        public int MasaPajakTahun { get; set; }
    }
}