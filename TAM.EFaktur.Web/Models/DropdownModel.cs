﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models
{
    public class DropdownModel
    {
        public String Id { get; set; }
        public string Name { get; set; }
    }
}