﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TAM.EFaktur.Web.Models.VendorGroup
{
    public class VendorGroupRepository : BaseRepository
    {
        #region Singleton
        private VendorGroupRepository() { }
        private static VendorGroupRepository instance = null;
        public static VendorGroupRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new VendorGroupRepository();
                }
                return instance;
            }
        }
        #endregion
         

        #region Dropdownlist 
        public List<DropdownModel> GetVendorGroupDropdownList()
        {
            IEnumerable<DropdownModel> result = db.Query<DropdownModel>("VendorGroup/usp_DropDownVendorGroup");
            return result.ToList();
        }
        #endregion
    }
}
