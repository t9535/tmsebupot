﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TAM.EFaktur.Web.Models.Master_Company_Details
{
    public class CompanyRepository : BaseRepository
    {
        #region Singleton
        private CompanyRepository() { }
        private static CompanyRepository instance = null;
        public static CompanyRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CompanyRepository();
                }
                return instance;
            }
        }
        #endregion
        #region Counter Data
        public int Count(CompanySearchParamViewModel model)
        {
            dynamic args = new
            {

                Lokasi = model.Lokasi,
                Code = model.Code,
                Nama_pt = model.Nama_pt,
                Nama_ttd = model.Nama_ttd,
                Jabatan_ttd = model.Jabatan_ttd,
                Npwp_pt = model.Npwp_pt,
                Image_ttd = model.Image_ttd,
                Image_cap = model.Image_cap,
                no_pengukuhan_pt = model.no_pengukuhan_pt,
                alamat_pt = model.alamat_pt
            };
            return db.SingleOrDefault<int>("Master_Company_Details/usp_CountCompanyListDashboard", args);

        }
        #endregion
        #region Processing Data GetList for Search
        //list data for search
        public List<CompanyViewModel> GetList(CompanySearchParamViewModel model, int SortBy, string SortDirection, int FromNumber, int ToNumber)
        {
            dynamic args = new
            {
                //Id = model.Id,
                Lokasi = model.Lokasi,
                Code = model.Code,
                Nama_pt = model.Nama_pt,
                Nama_ttd = model.Nama_ttd,
                Npwp_ttd = model.Npwp_ttd,
                Jabatan_ttd = model.Jabatan_ttd,
                Npwp_pt = model.Npwp_pt,
                Image_ttd = model.Image_ttd,
                Image_cap = model.Image_cap,
                SortBy = SortBy,
                SortDirection = SortDirection,
                no_pengukuhan_pt = model.no_pengukuhan_pt,
                alamat_pt = model.alamat_pt,
                FromNumber = FromNumber,
                ToNumber = ToNumber,
                Cut_Off_Start = model.Cut_Off_Start,
                Cut_Off_End = model.Cut_Off_End

            };
            IEnumerable<CompanyViewModel> result = db.Query<CompanyViewModel>("Master_Company_Details/usp_GetCompanyListDashboard", args);
            //IEnumerable<CompanyViewModel> result = db.Query<CompanyViewModel>("Master_Company_Details/usp_GetCompanyListDashboard",args);
            return result.ToList();
        }
        public CompanyUpdate GetById(Guid Id)
        {
            return db.SingleOrDefault<CompanyUpdate>("Master_Company_Details/usp_GetComapnyByID", new { Id = Id });
        }
        public List<DropdownViewModel> GetUserListDropdown(string username, bool isFullAccess)
        {
            dynamic args = new
            {
                username = username,
                isFullAccess = isFullAccess
            };

            IEnumerable<DropdownViewModel> result = db.Query<DropdownViewModel>("Master_Company_Details/usp_GetUserListDropdown", args);
            return result.ToList();
        }
        public Result Insert(CompanyCreateUpdate model, string EventActor, DateTime EventDate)
        {
            try
            {
                //var ransitoryDateStart = model.Cut_Off_Start.Split('-');
                DateTime Start = model.Cut_Off_Start; // Convert.ToDateTime(ransitoryDateStart[0] + "/" + ransitoryDateStart[1] + "/" + ransitoryDateStart[2]);

                //var ransitoryDateEnd = model.Cut_Off_End.Split('-');
                DateTime End = model.Cut_Off_End; // Convert.ToDateTime(ransitoryDateEnd[0] + "/" + ransitoryDateEnd[1] + "/" + ransitoryDateEnd[2]);

                var CompanyId = Guid.NewGuid();
                dynamic args = new
                {
                    Id = CompanyId,
                    Lokasi = model.Lokasi,
                    Npwp_pt = model.Npwp_pt,
                    Nama_pt = model.Nama_pt,
                    Npwp_ttd = model.Npwp_ttd,
                    Nama_ttd = model.Nama_ttd,
                    Jabatan_ttd = model.Jabatan_ttd,
                    Image_ttd = model.Image_ttd,
                    Image_cap = model.Image_cap,
                    Code = model.Code.ToUpper(),
                    no_pengukuhan_pt = model.no_pengukuhan_pt,
                    alamat_pt = model.alamat_pt,
                    Cut_Off_Start = Start,
                    Cut_Off_End = End,
                    //Cut_Off_Start = model.Cut_Off_Start.FormatSQLDate(),
                    //Cut_Off_End = model.Cut_Off_End.FormatSQLDate(),
                    EventDate = EventDate.FormatSQLDate(),
                    EventActor = EventActor,
                };
                db.Execute("Master_Company_Details/usp_Insert_TB_M_Company_Detail", args);
                return MsgResultSuccessInsert();
            }
            catch (Exception e)
            {
                return MsgResultError(e.Message.ToString());
            }
        }
        public List<CompanyCreateUpdate> CekDataDuplicat(CompanyCreateUpdate model)
        {
                dynamic args = new
                {
                    Code = model.Code,
                    Npwp_pt = model.Npwp_pt,
                };
            IEnumerable<CompanyCreateUpdate> result = db.Query<CompanyCreateUpdate>("Master_Company_Details/Get_Usp_Duplicat_Company", args);
            return result.ToList();
        }
        public List<CompanyUpdate> CekDataDuplicatUpdate(CompanyUpdate model)
        {
            dynamic args = new
            {
                Code = model.Code,
                Npwp_pt = model.Npwp_pt,
            };
            IEnumerable<CompanyUpdate> result = db.Query<CompanyUpdate>("Master_Company_Details/Get_Usp_Duplicat_Company", args);
            return result.ToList();
        }
        //Update Company
        public Result Update(CompanyUpdate model, string EventActor, DateTime EventDate)
        {
            try
            {
                //var ransitoryDateStart = model.Cut_Off_Start.Split('-');
                DateTime Start = model.Cut_Off_Start; //Convert.ToDateTime(ransitoryDateStart[0] + "/" + ransitoryDateStart[1] + "/" + ransitoryDateStart[2]);

                //var ransitoryDateEnd = model.Cut_Off_End.Split('-');
                DateTime End = model.Cut_Off_End; // Convert.ToDateTime(ransitoryDateEnd[0] + "/" + ransitoryDateEnd[1] + "/" + ransitoryDateEnd[2]);
                dynamic args = new
                {
                    Id = model.Id,
                    Lokasi = model.Lokasi,
                    Npwp_pt = model.Npwp_pt,
                    Nama_pt = model.Nama_pt,
                    Npwp_ttd = model.Npwp_ttd,
                    Nama_ttd = model.Nama_ttd,
                    Jabatan_ttd = model.Jabatan_ttd,
                    Image_ttd = model.Image_ttd,
                    Image_cap = model.Image_cap,
                    Code = model.Code.ToUpper(),
                    no_pengukuhan_pt = model.no_pengukuhan_pt,
                    alamat_pt = model.alamat_pt,
                    //Cut_Off_Start = Convert.ToDateTime(model.Cut_Off_Start),
                    //Cut_Off_End = Convert.ToDateTime(model.Cut_Off_End),
                    Cut_Off_Start = Start,
                    Cut_Off_End = End,
                    ModifiedOn = EventDate.FormatSQLDate(),
                    ModifiedBy = EventActor,

                };
                db.Execute("Master_Company_Details/usp_Update_TB_M_Company_Detail", args);
                return MsgResultSuccessUpdate();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                return MsgResultError(e.Message.ToString());
            }
        }
        //Delete Company
        public Result Delete(CompanyUpdate model)
        {
            try
            {
                dynamic args = new
                {
                    Id = model.Id
                };
                db.Execute("Master_Company_Details/usp_Delete_TB_M_Company_Detail", args);
                return MsgResultSuccessDelete();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                return MsgResultError(e.Message.ToString());
            }
        }
        #endregion
        //yg lain di timpa aja yg di merge GetByCode by Hendry
        public Master_Company_detail GetByCode(string code)
        {
            return db.SingleOrDefault<Master_Company_detail>("Master_Company_Details/usp_GetCompanyByCode", new { Code = code });
        }

        public List<CompanyViewModel> GetOriginalById(CompanySearchParamViewModel model)
        {
            dynamic args = new
            {
                //Id = model.Id,
                Lokasi = model.Lokasi,
                Code = model.Code,
                Nama_pt = model.Nama_pt,
                Nama_ttd = model.Nama_ttd,
                Npwp_ttd = model.Npwp_ttd,
                Jabatan_ttd = model.Jabatan_ttd,
                Npwp_pt = model.Npwp_pt,
                Image_ttd = model.Image_ttd,
                Image_cap = model.Image_cap,
                SortBy = 1,
                SortDirection = "DESC",
                no_pengukuhan_pt = model.no_pengukuhan_pt,
                alamat_pt = model.alamat_pt

            };
            IEnumerable<CompanyViewModel> result = db.Query<CompanyViewModel>("Master_Company_Details/usp_GetCompanyListExcel", args);
            return result.ToList();
        }
    }
}
