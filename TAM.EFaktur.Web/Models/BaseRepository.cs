﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace TAM.EFaktur.Web.Models
{
    public class BaseRepository
    {
        Result result = new Result();
        public string msgSuccessInsert = "Data has been saved successfully";
        public string msgSuccesssDelete = "Data has been deleted";
        public string msgSuccesssUpdate = "Data has been updated";
        public static readonly string propDBKey = "SINGLE.DBCONTEXT";
        public static IDictionary CuCo
        {
            get
            {
                if (HttpContext.Current != null)
                    return HttpContext.Current.Items;
                else
                    return new Dictionary<string, object>();
            }
        }
        public static IDBContext Db
        {
            get
            {
                return CuCo[propDBKey] as IDBContext;
            }
        }

        public IDBContext db
        {
            get
            {
                if (Db == null)
                {
                    CuCo[propDBKey] = DatabaseManager.Instance.GetContext();
                }
                return Db;
            }
        }

        public void Dispose()
        {
            if (Db != null)
            {
                Db.Close();
            }
        }

        public Result MsgResultSuccessInsert()
        {
            result.ResultCode = true;
            result.ResultDesc = msgSuccessInsert;
            return result;
        }

        public Result MsgResultSuccessSync()
        {
            result.ResultCode = true;
            result.ResultDesc = "Sync Success";
            return result;
        }

        public Result MsgResultSuccessDelete()
        {
            result.ResultCode = true;
            result.ResultDesc = msgSuccesssDelete;
            return result;
        }

        public Result MsgResultSuccessUpdate()
        {
            result.ResultCode = true;
            result.ResultDesc = msgSuccesssUpdate;
            return result;
        }

        public Result MsgResultError(string msg)
        {
            result.ResultCode = false;

            if (msg.Length > 35)
            {
                if (msg.Substring(0, 35) == "Violation of PRIMARY KEY constraint")
                {
                    result.ResultDesc = "Already exist";
                }
                else
                {
                    result.ResultDesc = msg;
                }
            }
            else
            {
                result.ResultDesc = msg;
            }
            return result;
        }   
    }
}