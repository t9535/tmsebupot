﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TAM.EFaktur.Web.Models.Master_Role
{
    public class Role : BaseModel
    {
        public string RoleCode { get; set; }
        public string RoleName { get; set; }

    }
}
