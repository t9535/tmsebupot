﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TAM.EFaktur.Web.Models.Master_Role
{
    public class MasterRoleRepository : BaseRepository
    {
        #region Singleton
        private MasterRoleRepository() { }
        private static MasterRoleRepository instance = null;
        public static MasterRoleRepository Instance
        {
            get 
            {
                if (instance == null)
                {
                    instance = new MasterRoleRepository();
                }
                return instance;
            }
        }
        #endregion

        #region Counter Data
        public int Count(MasterRoleDashboardSearchParamViewModel model)
        {
            dynamic args = new
            {
                
                RoleCode = model.RoleCode,
                RoleName = model.RoleName,
                ApplicationCode = model.ApplicationCode
                
            };

            return db.SingleOrDefault<int>("Master_Role/usp_CountMasterRoleListDashboard", args);
        }
        #endregion

        #region Processing Data

        public List<MasterRoleDashboardViewModel> GetList(MasterRoleDashboardSearchParamViewModel model, int SortBy, string SortDirection, int FromNumber, int ToNumber)
        ///public List<Role> GetList(string RoleCode, string RoleName)
        {
            dynamic args = new
            {
                RoleCode = model.RoleCode,
                RoleName = model.RoleName,
                ApplicationCode=model.ApplicationCode,
                SortBy = SortBy,
                SortDirection = SortDirection,
                FromNumber = FromNumber,
                ToNumber = ToNumber
               
            };
            IEnumerable<MasterRoleDashboardViewModel> result = db.Query<MasterRoleDashboardViewModel>("Master_Role/usp_GetRoleListDashboard", args);
            //IEnumerable<Role> result = db.Query<Role>("Role/nosqlyet", args);
            return result.ToList();
        }

        public MasterRole GetById(Guid Id)
        {
            return db.SingleOrDefault<MasterRole>("Master_Role/usp_GetRoleById", new { Id = Id });
        }

        //insert Data
        public Result Insert (MasterRoleCreateUpdate model, string EventActor, DateTime EventDate)
        {
            try
            {
                dynamic args = new
                {
                    Id = Guid.NewGuid(),
                    RoleCode = model.RoleCode,
                    RoleName = model.RoleName,
                    ApplicationCode = model.ApplicationCode,
                    EventDate = EventDate.FormatSQLDate(),
                    EventActor = EventActor
                };
                db.Execute("Master_Role/usp_Insert_TB_M_Role", args);
                return MsgResultSuccessInsert();
            }
            catch (Exception e)
            {
                return MsgResultError(e.Message.ToString());
            }
        }
        #endregion

        //Update Data
        public Result Update (MasterRoleCreateUpdate model, string EventActor, DateTime EventDate)
        {
            try
            {
                dynamic args = new
                {
                    Id = model.Id,
                    RoleCode = model.RoleCode,
                    RoleName = model.RoleName,
                    ApplicationCode = model.ApplicationCode,
                    EventDate = EventDate.FormatSQLDate(),
                    EventActor = EventActor,
                    
                };
                db.Execute("Master_Role/usp_UpdateMasterRole", args);
                return MsgResultSuccessUpdate();
            }
            catch (Exception e)
            {
                return MsgResultError(e.Message.ToString());
            }
        }

        //Delete Data
        public Result Delete(MasterRoleCreateUpdate model, string EventActor, DateTime EventDate)
        {
            try
            {
                dynamic args = new
                {
                    Id = model.Id,
                    EventDate = EventDate.FormatSQLDate(),
                    EventActor = EventActor,

                };
                db.Execute("Master_Role/usp_Delete_TB_M_Role", args);
                return MsgResultSuccessDelete();
            }
            catch (Exception e)
            {
                return MsgResultError(e.Message.ToString());
            }
        }

        #region GetDropdown

        public List<DropdownViewModel>GetRoleDropdown() 
        {

            IEnumerable<DropdownViewModel> result = db.Query<DropdownViewModel>("Master_Role/usp_DropDownRole");
            return result.ToList();
        }
    }
        #endregion
        
 }

