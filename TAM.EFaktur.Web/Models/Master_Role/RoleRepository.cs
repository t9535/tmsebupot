﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TAM.EFaktur.Web.Models.Master_Role
{
    public class RoleRepository : BaseRepository
    {
        #region Singleton
        private RoleRepository() { }
        private static RoleRepository instance = null;
        public static RoleRepository Instance
        {
            get 
            {
                if (instance == null)
                {
                    instance = new RoleRepository();
                }
                return instance;
            }
        }
        #endregion

        #region Counter Data
        public int Count(string RoleCode, string RoleName)
        {
            dynamic args = new
            {
                RoleCode = RoleCode,
                RoleName = RoleName
                
            };

            return db.SingleOrDefault<int>("Role/countApplications", args); 
        }
        #endregion

        #region Processing Data

        public List<Role> GetList(string RoleCode, string RoleName)
        {
            dynamic args = new
            {
                RoleCode = RoleCode,
                RoleName = RoleName
               
            };
            IEnumerable<Role> result = db.Query<Role>("Role/nosqlyet", args);
            return result.ToList();
        }

        public Role GetById(Guid Id)
        {
            return db.SingleOrDefault<Role>("Role/usp_GetRoleByID", new { Id = Id });
        }

        public Result Save(Role objRole, DateTime EventDate, string EventActor)
        {
            try
            {
                dynamic args = new
                {
                    Id= objRole.Id,
                    RoleCode = objRole.RoleCode,
                    RoleName = objRole.RoleName,
                    RowStatus           = objRole.RowStatus,
                    EventDate           = EventDate,
                    EventActor          = EventActor                
                };
                db.Execute("Role/usp_SaveRole", args);
                return MsgResultSuccessInsert();
            }
            catch (Exception e)
            {
                return MsgResultError(e.Message.ToString());
            }
        }

        public Result Delete(Guid Id)
        {
            try
            {
                db.Execute("Role/usp_DeleteRoleByID", new { Id = Id });
                return MsgResultSuccessDelete();
            }
            catch (Exception e)
            {
                return MsgResultError(e.Message.ToString());
            }
        }

        //Role Dropdown list in Maintain User
       public List<DropdownViewModel> GetRoleDropdownList()
        {

            IEnumerable<DropdownViewModel> result = db.Query<DropdownViewModel>("Master_Role/usp_GetRoleList");
            return result.ToList();
        }
        #endregion

    }
}
