﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TAM.EFaktur.Web.Models.Master_Questionaire
{
    public class QuestionerDropdownViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
