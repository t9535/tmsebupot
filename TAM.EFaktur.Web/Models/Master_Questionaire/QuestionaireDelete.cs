﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.Master_Questionaire
{
    public class QuestionaireDelete
    {
        public int QuestionId { get; set; }
        public string CategoryCode { get; set; }
        public string Question { get; set; }
        public bool IsYes { get; set; }
        public string WarningStatusYes { get; set; }
        public bool IsNo { get; set; }
        public string WarningStatusNo { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string modifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }

    }
}