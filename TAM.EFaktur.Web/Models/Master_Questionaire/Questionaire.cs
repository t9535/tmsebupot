﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TAM.EFaktur.Web.Models.Master_Questionaire
{
    public class Questionaire : BaseModel
    {
        public int QuestionId { get; set; }
        public string CategoryCode { get; set; }
        public string Question { get; set; }
        public string IsYes { get; set; }
        public string WarningStatusYes { get; set; }
        public string IsNo { get; set; }
        public string WarningStatusNo { get; set; }     
    }
}
