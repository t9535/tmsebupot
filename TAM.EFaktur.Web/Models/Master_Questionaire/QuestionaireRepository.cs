﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TAM.EFaktur.Web.Models.Master_Questionaire
{
    public class QuestionaireRepository : BaseRepository
    {
        #region Singleton
        private QuestionaireRepository() { }
        private static QuestionaireRepository instance = null;
        public static QuestionaireRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new QuestionaireRepository();
                }
                return instance;
            }
        }
        #endregion
        #region Counter Data
        public int Count(QuestionaireSearchParamViewModel model)
        {
            dynamic args = new
            {
                NominativeType = model.CategoryCode,
                Question = model.Question
            };

            return db.SingleOrDefault<int>("Master_Questionare/usp_CountQuestionaireListDashboard", args);
        }
        #endregion
        #region Processing Data GetList for Search
        //list data for search
        public List<QuestionaireViewModel> GetList(QuestionaireSearchParamViewModel model, int SortBy, string SortDirection, int FromNumber, int ToNumber)

        {
            dynamic args = new
            {

                CategoryCode = model.CategoryCode,
                Question = model.Question,
                //IsYes = model.IsYes,
                //IsNo = model.IsNo,
                SortBy = SortBy,
                SortDirection = SortDirection,
                FromNumber = FromNumber,
                ToNumber = ToNumber

            };
            IEnumerable<QuestionaireViewModel> result = db.Query<QuestionaireViewModel>("Master_Questionare/usp_GetQuestionListDashboard", args);
            return result.ToList();
        }
        public QuestionaireUpdateViewModel GetById(int QuestionId)
        {
            return db.SingleOrDefault<QuestionaireUpdateViewModel>("Master_Questionare/usp_GetQuestionByID", new { QuestionId = QuestionId });
        }
        public List<DropdownViewModel> GetUserListDropdown(string username, bool isFullAccess)
        {
            dynamic args = new
            {
                username = username,
                isFullAccess = isFullAccess
            };

            IEnumerable<DropdownViewModel> result = db.Query<DropdownViewModel>("Master_User/usp_GetUserListDropdown", args);
            return result.ToList();
        }
        //Insert New Questionaire
        public Result Insert(QuestionaireCreateUpdate model, string EventActor, DateTime EventDate)
        {
            string getwarningstatusyes = model.WarningStatusYes;
            string getvaluestatusyes = getwarningstatusyes.Contains("Basic Question") ? "BQ" : getwarningstatusyes.Contains("Optional Question") ? "OQ" : "NA";

            string getwarningstatusNo = model.WarningStatusNo;
            string getvaluestatusNo = getwarningstatusNo.Contains("Basic Question") ? "BQ" : getwarningstatusNo.Contains("Optional Question") ? "OQ" : "NA";

            try
            {
                dynamic args = new
                {
                    //QuestionId = model.QuestionId,
                    CategoryCode = model.CategoryCode,
                    Question = model.Question,
                    IsYes = model.IsYes,
                    WarningStatusYes = getvaluestatusyes,
                    IsNo = model.IsNo,
                    WarningStatusNo = getvaluestatusNo,
                    EventDate = EventDate.FormatSQLDateTime(),
                    EventActor = EventActor,

                };
                db.Execute("Master_Questionare/usp_Insert_tb_m_question_enum", args);

                return MsgResultSuccessInsert();
            }
            catch (Exception e)
            {
                return MsgResultError(e.Message.ToString());
            }
        }
        //Update Questionaire
        public Result Update(QuestionaireUpdateViewModel model, string EventActor, DateTime EventDate)
        {
            string getwarningstatusyes = model.WarningStatusYes;
            string getvaluestatusyes = getwarningstatusyes.Contains("Basic Question") ? "BQ" : getwarningstatusyes.Contains("Optional Question") ? "OQ" : "NA";

            string getwarningstatusNo = model.WarningStatusNo;
            string getvaluestatusNo = getwarningstatusNo.Contains("Basic Question") ? "BQ" : getwarningstatusNo.Contains("Optional Question") ? "OQ" : "NA";

            try
            {
                dynamic args = new
                {
                    QuestionId = model.QuestionId,
                    CategoryCode = model.CategoryCode,
                    Question = model.Question,
                    IsYes = model.IsYes,
                    WarningStatusYes = getvaluestatusyes,
                    IsNo = model.IsNo,
                    WarningStatusNo = getvaluestatusNo,
                    EventDate = EventDate.FormatSQLDateTime(),
                    EventActor = EventActor,

                };
                db.Execute("Master_Questionare/usp_Update_tb_m_question_enum", args);
                return MsgResultSuccessUpdate();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                return MsgResultError(e.Message.ToString());
            }
        }
        //Delete Questionaire
        public Result Delete(QuestionaireUpdateViewModel model)
        {
            try
            {
                dynamic args = new
                {
                    QuestionId = model.QuestionId
                };
                db.Execute("Master_Questionare/usp_Delete_tb_m_question_enum", args);
                return MsgResultSuccessDelete();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                return MsgResultError(e.Message.ToString());
            }
        }

        public List<Questionaire> GetXlsReportById(Questionaire model)
        {
            dynamic args = new
            {
                CategoryCode = model.CategoryCode,
                Question = model.Question,
                IsYes = model.IsYes,
                IsNo = model.IsNo,
                SortBy = 1,
                SortDirection = "DESC"
            };
            IEnumerable<Questionaire> result = db.Query<Questionaire>("Master_Questionare/usp_GetReportQuestionById_ENUM", args);
            return result.ToList();
        }


        public List<Questionaire> GetIdBySearchParam(QuestionaireSearchParamViewModel model)
        {
            dynamic args = new
            {
                QuestionId = model.QuestionId,
                CategoryCode = model.CategoryCode,
                Question = model.Question,
                WarningStatusYes = model.WarningStatusYes,
                WarningStatusNo = model.WarningStatusNo,
            };
            IEnumerable<Questionaire> result = db.Query<Questionaire>("Master_Questionare/usp_GetReportQuestionById_ENUM", args);
            return result.ToList();
        }
        public List<QuestionerDropdownViewModel> GetQuestionDropdown()
        {
            IEnumerable<QuestionerDropdownViewModel> result = db.Query<QuestionerDropdownViewModel>("Master_Questionare/usp_GetQuestionDropdownList_ENUM");
            return result.ToList();
        }
        public List<QuestionerDropdownViewModel> GetQuestionKategoriDropdown()
        {
            IEnumerable<QuestionerDropdownViewModel> result = db.Query<QuestionerDropdownViewModel>("Master_Questionare/usp_GetQuestionKategoriDropdownList_ENUM");
            return result.ToList();
        }
        public List<QuestionaireUpdateViewModel> CekDataDuplicatUpdate(QuestionaireUpdateViewModel model)
        {
            dynamic args = new
            {
                CategoryCode = model.CategoryCode,
                Question = model.Question,
            };
            IEnumerable<QuestionaireUpdateViewModel> result = db.Query<QuestionaireUpdateViewModel>("Master_Questionare/Get_Usp_Duplicat_Question_Enum", args);

            return result.ToList();
        }
        #endregion
    }
}
