﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models
{
    public class DropdownColor
    {
        public Guid Id { get; set; }
        public string NomorFaktur { get; set; }

        public string EFakturStatus { get; set; }
    }
}