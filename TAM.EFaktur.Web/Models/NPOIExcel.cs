﻿using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models
{
    public static class NPOIExcel
    {
        public static string[] FormatDate = new string[] { "d/M/yyyy", "d/M/yy", "dd/MM/yyyy" };

        public static string GetStringValue(this ICell cell)
        {
            if (cell == null)
            {
                return string.Empty;
            }

            switch (cell.CellType)
            {
                case CellType.NUMERIC:
                    if (DateUtil.IsCellDateFormatted(cell))
                    {
                        try
                        {
                            return cell.DateCellValue.ToString("dd/MM/yyyy");
                        }
                        catch (NullReferenceException)
                        {
                            return DateTime.FromOADate(cell.NumericCellValue).ToString("dd/MM/yyyy");
                        }
                    }
                    return cell.NumericCellValue.ToString();

                case CellType.STRING:
                    return cell.StringCellValue.Trim();

                case CellType.BOOLEAN:
                    return cell.BooleanCellValue.ToString();

                default:
                    return string.Empty;
            }
        }
    }
}