﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TAM.EFaktur.Web.Models.Master_DataFieldEnum
{
    public class DataFieldEnum
    {
        public long DataFieldId { get; set; }
        public string CategoryCode { get; set; }
        public string FieldNameByTable { get; set; }
        public string FieldNameByExcel { get; set; }
        public string VendorGroup { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        //for list
        public int RowNum { get; set; }
        public string NominativeType { get; set; }
    }
}
