﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TAM.EFaktur.Web.Models.Master_DataFieldEnum
{
    public class DataFieldEnumRepository : BaseRepository
    {
        #region Singleton
        private DataFieldEnumRepository() { }
        private static DataFieldEnumRepository instance = null;
        public static DataFieldEnumRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new DataFieldEnumRepository();
                }
                return instance;
            }
        }
        #endregion

        #region Counter Data
        public int Count(DataFieldEnum model)
        {
            dynamic args = new
            {
                CategoryCode = model.CategoryCode,
                FieldNameByTable = model.FieldNameByTable,
                FieldNameByExcel = model.FieldNameByExcel,
                VendorGroup = model.VendorGroup
            };

            return db.SingleOrDefault<int>("Master_DataFieldEnum/usp_CountMasterDataFieldEnum", args);
        }

        #endregion

        #region Processing Data GetList for Search

        //list data for search
        public List<DataFieldEnum> GetList(DataFieldEnum model, int SortBy, string SortDirection, int FromNumber, int ToNumber)

        {
            dynamic args = new
            {
                CategoryCode = model.CategoryCode,
                FieldNameByTable = model.FieldNameByTable,
                FieldNameByExcel = model.FieldNameByExcel,
                VendorGroup = model.VendorGroup,
                SortBy = SortBy,
                SortDirection = SortDirection,
                FromNumber = FromNumber,
                ToNumber = ToNumber

            };
            IEnumerable<DataFieldEnum> result = db.Query<DataFieldEnum>("Master_DataFieldEnum/usp_GetMasterDataFieldEnum", args);
            return result.ToList();
        }

        public DataFieldEnum GetById(long DataFieldId)
        {
            return db.SingleOrDefault<DataFieldEnum>("Master_DataFieldEnum/usp_GetMasterDataFieldEnumByID", new { DataFieldId = DataFieldId });
        }
         
        //Update User
        public Result Update(DataFieldEnum model, string EventActor, DateTime EventDate)
        {
            try
            {
                dynamic args = new
                {
                    DataFieldId = model.DataFieldId,
                    CategoryCode = model.CategoryCode,
                    FieldNameByTable = model.FieldNameByTable,
                    FieldNameByExcel = model.FieldNameByExcel,
                    VendorGroup = model.VendorGroup,
                    ModifiedOn = EventDate.FormatSQLDateTime(),
                    ModifiedBy = EventActor,

                };
                db.Execute("Master_DataFieldEnum/usp_Update_TB_M_DataField_ENUM", args);

                return MsgResultSuccessUpdate();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                return MsgResultError(e.Message.ToString());
            }
        }
        public List<DataFieldEnum> GetOriginalById(DataFieldEnum model)
        {
            dynamic args = new
            {
                CategoryCode = model.CategoryCode,
                FieldNameByTable = model.FieldNameByTable,
                FieldNameByExcel = model.FieldNameByExcel,
                VendorGroup = model.VendorGroup,
                SortBy = 1,
                SortDirection = "Asc"
            };
            IEnumerable<DataFieldEnum> result = db.Query<DataFieldEnum>("Master_DataFieldEnum/usp_GetMasterDataFieldEnumExcel", args);
            return result.ToList();
        }
        public List<DropdownModel> GetCategoryNominativeDropdownList()
        {

            IEnumerable<DropdownModel> result = db.Query<DropdownModel>("Master_DataFieldEnum/usp_DropDownCategoryNominativeDataEnum");
            return result.ToList();
        }
        #endregion
    }
}
