﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.Master_HRIS
{
    public class HRISRepository : BaseRepository
    {
        #region Singleton
        private HRISRepository() { }
        private static HRISRepository instance = null;
        public static HRISRepository Instance
        {
            get 
            {
                if (instance == null)
                {
                    instance = new HRISRepository();
                }
                return instance;
            }
        }
        #endregion

        #region Processing Data

        public HRIS GetHRISByNoReg(string noReg)
        {
            var result = db.SingleOrDefault<HRIS>("Master_HRIS/usp_GetHRISByNoReg", new { NoReg = noReg });
            return result == null ? 
            new HRIS 
            { 
                Title = "",
                DepartmentName = "",
                DivisionName = "",
                EmployeeName = "",
                NoReg = "",
                SectionName= ""
            }
            : result;
        }
    
        #endregion
    }
}