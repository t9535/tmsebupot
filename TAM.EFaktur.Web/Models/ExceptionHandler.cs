﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TAM.EFaktur.Web.Models.Log_Sync;
using TAM.EFaktur.Web.Models.LogApplication;

namespace TAM.EFaktur.Web.Models
{
    public static class ExceptionHandler
    {
        //Tax Facility
        public const string GENERAL_FAIL_MESSAGE = "An error has occured, please check Application Log for detail or contact your system administrator if this problem persists.";
        public const string MESSAGE_WHT_PDF = "PDF file Withholding any not exist.";
        public const string MESSAGE_TAX_FACILITY = "PDF file Tax Facility any not exist.";
        public const string UPLOAD_FAIL_MESSAGE = "Upload has failed, please check Sync Log for detail.";
        public const string MANDATORY_FAIL_MESSAGE = "Mandatory field(s) cannot be empty";
        public const string STRING_FAIL_MESSAGE = "DPP/PPN/PPNBM/PPH field(s) cannot contains ( '.' or ',')";
        public const string PREVIOUS_ROW_FAIL_MESSAGE = "This row is automatically skipped because the previous row has failed";
        public const string GENERAL_SUCCESS_MESSAGE = "Success";
        public const string GENERAL_SUCCESS_MESSAGE_DETAIL = "Data Detail Insert Successfully";
        public const string GENERAL_SUCCESS_MESSAGE_Validation = "Validation Success";
        public const string TRANSITORY_STATUS = "Transitory Number is Mandatory if Transitory Status is Yes";
        public const string DPP_PPN_FAIL_MESSAGE = "Jumlah DPP/PPN is wrong";
        public const string DATE_INVALID_FORMAT = "TANGGAL_FAKTUR|TANGGAL_RETUR Valid format date dd/mm/yyyy";
        public const string NILAI_RETUR_DPP_INVALID = "NILAI_RETUR_DPP must be SUM of HARGA_TOTAL in the same NOMOR_DOKUMEN_RETUR"; 
        public const string NO_DOKUMEN_RETUR_VALIDASI = "File Already Exist Data Excel";

        public static void LogToApp(string module = "", MessageType messageType = MessageType.INF, string messageDetail = "", string username = "")
        {
            //Synchronization Logging
            try
            {
                LogApplicationInsertViewModel model = new LogApplicationInsertViewModel
                {
                    Username = username,
                    ClientIP = GlobalFunction.GetClientIP(),
                    ClientHostName = "", //TODO: Function Get HostName
                    LoginDate = DateTime.Now, //TODO: Function Get LoginDate User
                    IsLogged = true,
                    LogApplicationDetails = new List<LogApplicationDetailInsertViewModel>()
                };

                LogApplicationDetailInsertViewModel detailModel = new LogApplicationDetailInsertViewModel
                {
                    MessageType = messageType.ToString(),
                    MessageDetail = messageDetail,
                    MessageLocation = module
                };

                model.LogApplicationDetails.Add(detailModel);

                LogApplicationRepository.Instance.Insert(model, "System", DateTime.Now);
            }
            catch (Exception err)
            {

                string error = err.Message;
            }
        }

        public static void LogToSync(int rowId, string rowData = "", string fileName = "", string message = "",string messageDetail="",MessageType messageType = MessageType.INF, string showedMessage = UPLOAD_FAIL_MESSAGE)
        {
            //Synchronization Logging
            try
            {
                LogSyncCreateUpdate model = new LogSyncCreateUpdate().CreateModel(fileName, rowId, rowData, messageType, message,messageDetail);
                
                LogSyncRepository.Instance.Insert(model, "System", DateTime.Now);
            }
            catch (Exception err)
            {
                
                string error = err.Message;
            }
        }

        public static void LogToSync(List<LogSyncCreateUpdate> modelList)
        {
            try
            {
                foreach (var model in modelList)
                {
                    LogSyncRepository.Instance.Insert(model, "System", DateTime.Now); 
                }
            }
            catch (Exception err)
            {
                string error = err.Message;
            }
        }

        public static ApplicationException LogToApp(this Exception e, string module = "", MessageType messageType = MessageType.INF, string username = "", string messageDetail = "None", string showedMessage = GENERAL_FAIL_MESSAGE)
        {
            //Synchronization Logging
            try
            {
                LogApplicationInsertViewModel model = new LogApplicationInsertViewModel()
                    .CreateModel(
                        username, 
                        GlobalFunction.GetClientIP(), 
                        "", //TODO: Function Get HostName
                        DateTime.Now, //TODO: Function Get LoginDate User
                        true, 
                        new List<LogApplicationDetailInsertViewModel>());

                LogApplicationDetailInsertViewModel detailModel = new LogApplicationDetailInsertViewModel
                {
                    MessageType = messageType.ToString(),
                    MessageDetail = GetRecursiveInnerExceptionMessage(e) + "\nDetail: "+ messageDetail,
                    MessageLocation = module
                };

                model.LogApplicationDetails.Add(detailModel);

                LogApplicationRepository.Instance.Insert(model, "System", DateTime.Now);
            }
            catch (Exception err)
            {
                string error = err.Message;
            }
            
            return new ApplicationException(showedMessage, e);
        }

        public static ApplicationException LogToSync(this Exception e, int rowId, string rowData = "", string fileName = "", string messageDetail = "", MessageType messageType = MessageType.INF, string showedMessage = UPLOAD_FAIL_MESSAGE)
        {
            //Synchronization Logging
            LogSyncCreateUpdate model = new LogSyncCreateUpdate
            {
                Id = Guid.NewGuid(),
                FileName = fileName,
                Message = GetRecursiveInnerExceptionMessage(e) + "\nDetail: " + messageDetail,
                MessageType = messageType.ToString(),
                RowId = rowId,
                RowData = rowData
            };
            LogSyncRepository.Instance.Insert(model, "System", DateTime.Now);
            return new ApplicationException(showedMessage, e);
        }

        public static string GetRecursiveInnerExceptionMessage(Exception e)
        {
            string message = e.Message;
            if (e.InnerException != null)
            {
                message += " -> " + GetRecursiveInnerExceptionMessage(e.InnerException);
            }
            return message;
        }
    }


    public enum LogType
    {
        Application,
        Sync
    }

    public enum MessageType
    {
        ERR,
        INF
    }
}