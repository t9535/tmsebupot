﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TAM.EFaktur.Web.Models.Category_Nominative;

namespace TAM.EFaktur.Web.Models.Dashboard_Nominative
{
    public class NominativeTypeReport
    {
        public string CategoryCode { get; set; }
    }

    public class ReportPathFrom
    {
        public Guid DashboardId { get; set; }
        public string CategoryCode { get; set; }
        public string FromPdf { get; set; }
    }

    public class DonationReportXls
    {
        //public string CompanyName { get; set; }
        //public string CompanyNPWP { get; set; }
        //public string CompanyAddress { get; set; }

        //public string REFERENCE_NO { get; set; }
        //public string PV_NO { get; set; }
        ////public int? THN_PAJAK { get; set; }

        //public string TMP_PENANDATANGAN { get; set; }

        //public DateTime? TGL_PENANDATANGAN { get; set; }

        //public string NAMA_PENANDATANGAN { get; set; }

        //public string JABATAN_PENANDATANGAN { get; set; }

        public string NominativeIDNo { get; set; }

        public string JENIS_SUMBANGAN { get; set; }

        public string BENTUK_SUMBANGAN { get; set; }

        public decimal? NILAI_SUMBANGAN { get; set; }

        public DateTime? TANGGAL_SUMBANGAN { get; set; }

        public string NAMA_LEMBAGA_PENERIMA { get; set; }

        public string NPWP_LEMBAGA_PENERIMA { get; set; }

        public string ALAMAT_LEMBAGA { get; set; }

        public string NO_TELP_LEMBAGA { get; set; }

        public string SARANA_PRASARANA_INFRASTRUKTUR { get; set; }

        public string LOKASI_INFRASTRUKTUR { get; set; }

        public decimal? BIAYA_PEMBANGUNAN_INFRASTRUKTUR { get; set; }

        public string IZIN_MENDIRIKAN_BANGUNAN { get; set; }

        public string NOMOR_REKENING { get; set; }

        public string NAMA_REKENING_PENERIMA { get; set; }

        public string NAMA_BANK { get; set; }
    }

    public class PromotionReportXls
    {
        //public string CompanyName { get; set; }
        //public string CompanyNPWP { get; set; }
        //public string CompanyAddress { get; set; }
        //public string REFERENCE_NO { get; set; }
        //public string PV_NO { get; set; }
        //public string ENOM_NO { get; set; }

        //public int? THN_PAJAK { get; set; }

        //public string TMP_PENANDATANGAN { get; set; }

        //public DateTime? TGL_PENANDATANGAN { get; set; }

        //public string NAMA_PENANDATANGAN { get; set; }

        //public string JABATAN_PENANDATANGAN { get; set; }

        public string NominativeIDNo { get; set; }

        public int? NO { get; set; }

        public string NAMA { get; set; }

        public string NPWP { get; set; }

        public string ALAMAT { get; set; }
        public DateTime? TANGGAL { get; set; }
        public string BENTUK_DAN_JENIS_BIAYA { get; set; }
        public decimal? JUMLAH { get; set; }
        public decimal? JUMLAH_GROSS_UP { get; set; }

        public string KETERANGAN { get; set; }
        public decimal? JUMLAH_PPH { get; set; }

        public string JENIS_PPH { get; set; }
        public decimal? JUMLAH_NET { get; set; }

        public string NOMOR_REKENING { get; set; }

        public string NAMA_REKENING_PENERIMA { get; set; }

        public string NAMA_BANK { get; set; }

        public string NO_KTP { get; set; }

        public string NOMOR_BUKTI_POTONG { get; set; }
    }


    public class EntertainmentReportXls
    {
        public string NominativeIDNo { get; set; }

        public int? NO { get; set; }

        public DateTime? TANGGAL { get; set; }
        public string TEMPAT { get; set; }

        public string ALAMAT { get; set; }

        public string BENTUK_DAN_JENIS_ENTERTAINMENT { get; set; }

        public decimal? JUMLAH { get; set; }

        public string NAMA_RELASI { get; set; }

        public string POSISI_RELASI { get; set; }

        public string PERUSAHAAN_RELASI { get; set; }

        public string JENIS_USAHA_RELASI { get; set; }

        public string KETERANGAN { get; set; }
    }

    public class ImageData
    {
        public string QrCode { get; set; }
    }
}
