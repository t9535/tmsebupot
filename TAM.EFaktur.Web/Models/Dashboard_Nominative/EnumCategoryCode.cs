﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.Dashboard_Nominative
{
    public class EnumCategoryCode
    {
        public const string Promotion = "enom2";
        public const string Entertainment = "enom1";
        public const string Donation = "enom3";
    }
}