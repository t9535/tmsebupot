﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.Dashboard_Nominative
{
    public class EnumSystem
    {
        public const string Manual = "Manual";
        public const string ELVIS = "ELVIS";
        public const string EPROC = "EPROC";
    }
}