﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using QRCoder;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using TAM.EFaktur.Web.Models.Entertainment;
using TAM.EFaktur.Web.Models.Log_Sync;
using TAM.EFaktur.Web.Models.Master_Config;
using TAM.EFaktur.Web.Models.Promotion;

namespace TAM.EFaktur.Web.Models.Dashboard_Nominative
{
    public class DashboardRepository : BaseRepository
    {
        #region Singleton
        private DashboardRepository() { }
        private static DashboardRepository instance = null;
        public static DashboardRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new DashboardRepository();
                }
                return instance;
            }
        }
        #endregion

        #region Counter Data
        public int Count(DashboardSearch model)
        {
            return db.SingleOrDefault<int>("Dashboard_Nominative/usp_CountDashboard", model.MapFromModelSearch());
        }

        #endregion

        #region Processing Data GetList for Search
        //list data for search
        public List<Dashboard> GetList(DashboardSearch model, int SortBy, string SortDirection, int FromNumber, int ToNumber)
        {
            IEnumerable<Dashboard> result = db.Query<Dashboard>("Dashboard_Nominative/usp_GetDashboard", model.MapFromModelSearch("", SortBy, SortDirection, FromNumber, ToNumber));
            return result.ToList();
        }

        public List<Guid> GetIdBySearchParam(DashboardSearch model)
        {
            IEnumerable<Guid> result = db.Query<Guid>("Dashboard_Nominative/usp_GetDashboardIdBySearchParam", model.MapFromModelSearch());
            return result.ToList();
        }

        public Dashboard GetById(Guid Id)
        {
            return db.SingleOrDefault<Dashboard>("Dashboard_Nominative/usp_GetDashboardByID", new { ID = Id });
        }

        public Result Delete(Guid Id, string EventActor)
        {
            try
            {
                db.Execute("Dashboard_Nominative/usp_DeleteDashboardByID", new { ID = Id });
                return MsgResultSuccessDelete();
            }
            catch (Exception e)
            {
                return MsgResultError(e.LogToApp("Delete Dashboard", MessageType.ERR, EventActor).Message);
            }
        }

        public Result ExcelNominativeDataInsert(string path, string EventActor)
        {
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = "File has been uploaded successfully"
            };

            FileStream file = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite);

            HSSFWorkbook hssfwb = new HSSFWorkbook(file);
            ISheet sheet = hssfwb.GetSheet("Default");
            Regex regex = new Regex(@"^[0-9]*$");
            string pipedString = string.Empty;

            int row = 0;
            int rowFail = 0;
            int rowSuccess = 0;

            try
            {
                file.Close();
                var cellNum = 26;//jumlah header cell +1;
                var rowNum = 3;
                List<int> exceptionCellNum = new List<int>();
                //exceptionCellNum.Add(17); 

                var config = MasterConfigRepository.Instance.GetByConfigKey("UrlTemplateNominativeData");
                if (config == null)
                {
                    result.ResultCode = false;
                    result.ResultDesc = "Please define the UrlTemplateNominativeData Config.";
                    return result;
                }

                if (sheet.GetRow(rowNum - 1) == null)
                {
                    result.ResultCode = false;
                    result.ResultDesc = "Invalid template upload nominative data.";
                    return result;
                }

                if (sheet.GetRow(rowNum - 1).LastCellNum != cellNum)
                {
                    result.ResultCode = false;
                    result.ResultDesc = "Invalid template upload nominative data.";
                    return result;
                }

                List<LogSyncCreateUpdate> logSyncList = new List<LogSyncCreateUpdate>();
                List<string> failTaxNumber = new List<string>();

                for (row = rowNum; row <= sheet.LastRowNum; row++)
                {
                    #region Set Cell Policy, Cell Type and Save Sync Data
                    pipedString = string.Empty;
                    for (int i = 0; i < cellNum; i++)
                    {
                        sheet.GetRow(row).GetCell(i, MissingCellPolicy.CREATE_NULL_AS_BLANK);
                        sheet.GetRow(row).GetCell(i).SetCellType(CellType.STRING);

                        pipedString += NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(i)) + "|";
                    }
                    pipedString = pipedString.Remove(pipedString.Length - 1);
                    #endregion

                    try
                    {
                        #region Cell Mapping & Insert to DB 
                        db.BeginTransaction();

                        var formatDate = NPOIExcel.FormatDate;
                        string validation = "", NPWPFormat = null, TaxFormat = null;
                        DateTime? InvoiceDate = null, BookPeriod = null, WitholdingTaxDate = null, SAPGRPostingDate = null, SAPInvoicePostingDate = null, ReceiveFileDate = null, NominativeDate = null; //DownloadedDate = null, 
                        decimal TurnoverAmount = 0;
                        Nullable<decimal> VATAmount = null, WHTAmount = null;

                        var NominativeType = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(0));
                        var StatusInvoice = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(1));
                        var PVType = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(2));
                        var AccruedYearEndStatus = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(3));
                        var AccruedBookingNo = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(4));
                        //var SettlementSuspenseStatus = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(5));
                        var DescriptionTransaction = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(5));
                        var GLAccount = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(6));
                        var VendorName = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(7));
                        var Npwp = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(8));
                        var InvoiceNo = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(9));
                        var InvoiceDateString = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(10));
                        var BookPeriodString = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(11));
                        var TurnoverAmountString = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(12));
                        var VATAmountString = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(13));
                        var WHTAmountString = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(14));
                        var TaxInvoiceNo = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(15));
                        var WitholdingTaxNo = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(16));
                        var WitholdingTaxDateString = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(17));
                        var WHTArticle = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(18));
                        var PVNo = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(19));
                        var PVCreatedBy = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(20));
                        var SAPGRDocNo = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(21));
                        var SAPGRPostingDateString = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(22));
                        var SAPInvoiceDocNo = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(23));
                        var SAPInvoicePostingDateString = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(24));
                        //var ReceiveFileDateString = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(25));
                        //var DownloadedDateString = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(26));
                        //var DownloadStatus = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(27));
                        //var NominativeFormAttachment = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(28));
                        //var NominativeIDNo = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(29));
                        //var NominativeStatus = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(26));
                        var NominativeDateString = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(25));

                        #region Validation
                        //jika ada space di semua kolom maka tidak di proses
                        if (string.IsNullOrEmpty(NominativeType) && string.IsNullOrEmpty(StatusInvoice) && string.IsNullOrEmpty(PVType) && string.IsNullOrEmpty(AccruedYearEndStatus) &&
                            string.IsNullOrEmpty(AccruedBookingNo) && string.IsNullOrEmpty(DescriptionTransaction) && string.IsNullOrEmpty(GLAccount) && string.IsNullOrEmpty(VendorName) &&
                            string.IsNullOrEmpty(Npwp) && string.IsNullOrEmpty(InvoiceNo) && string.IsNullOrEmpty(InvoiceDateString) && string.IsNullOrEmpty(BookPeriodString) &&
                            string.IsNullOrEmpty(TurnoverAmountString) && string.IsNullOrEmpty(VATAmountString) && string.IsNullOrEmpty(WHTAmountString) && string.IsNullOrEmpty(TaxInvoiceNo) &&
                            string.IsNullOrEmpty(WitholdingTaxNo) && string.IsNullOrEmpty(WitholdingTaxDateString) && string.IsNullOrEmpty(WHTArticle) && string.IsNullOrEmpty(PVNo) &&
                            string.IsNullOrEmpty(PVCreatedBy) && string.IsNullOrEmpty(SAPGRDocNo) && string.IsNullOrEmpty(SAPGRPostingDateString) && string.IsNullOrEmpty(SAPInvoiceDocNo) &&
                            string.IsNullOrEmpty(SAPInvoicePostingDateString) && string.IsNullOrEmpty(NominativeDateString)
                            )
                        {
                            break;
                        }

                        //Nullale
                        if (string.IsNullOrEmpty(NominativeType)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Nominative Type can't be empy"; }
                        if (string.IsNullOrEmpty(StatusInvoice)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Invoice Status can't be empy"; }
                        if (string.IsNullOrEmpty(PVType)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "PV Type can't be empy"; }

                        if (PVType.Replace(" ", "").ToLower() == "accruedyearend")
                        {
                            if (string.IsNullOrEmpty(AccruedYearEndStatus)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Accrued Year End Status can't be empy"; }
                            if (string.IsNullOrEmpty(AccruedBookingNo)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Accrued Booking No can't be empy"; }
                        }

                        // if (string.IsNullOrEmpty(AccruedYearEndStatus)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Accrued Year End Status can't be empy"; }
                        //if (string.IsNullOrEmpty(AccruedBookingNo)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Accrued Booking No can't be empy"; }
                        //if (string.IsNullOrEmpty(SettlementSuspenseStatus)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Settlement Suspense Status can't be empy"; }
                        if (string.IsNullOrEmpty(DescriptionTransaction)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Description Transaction can't be empy"; }
                        if (string.IsNullOrEmpty(GLAccount)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "GL Account can't be empy"; }
                        if (string.IsNullOrEmpty(VendorName)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Vendor Name can't be empy"; }
                        if (string.IsNullOrEmpty(Npwp)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "NPWP can't be empy"; }

                        //if (!string.IsNullOrEmpty(InvoiceNo)) {
                        //    // if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Invoice No can't be empy";
                        //    if (string.IsNullOrEmpty(InvoiceDateString)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Invoice Date can't be empy"; }
                        //}
                        if (string.IsNullOrEmpty(InvoiceNo)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Invoice No can't be empy"; }

                        if (string.IsNullOrEmpty(InvoiceDateString)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Invoice Date can't be empy"; }

                        if (string.IsNullOrEmpty(BookPeriodString)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Book Period can't be empy"; }
                        if (string.IsNullOrEmpty(TurnoverAmountString)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "VAT Based Amount can't be empy"; }
                        //if (string.IsNullOrEmpty(VATAmountString)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "VAT Amount can't be empy"; }
                        //if (string.IsNullOrEmpty(WHTAmountString)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "WHT Amount can't be empy"; }
                        // if (string.IsNullOrEmpty(TaxInvoiceNo)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Tax Invoice No can't be empy"; }

                        if (!string.IsNullOrEmpty(VATAmountString))
                        {
                            decimal VatValue;
                            if (Decimal.TryParse(VATAmountString, out VatValue))
                            {
                                if (VatValue >= 0)
                                {
                                    if (string.IsNullOrEmpty(TaxInvoiceNo)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Tax Invoice No can't be empy"; }
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "VAT Amount Valid format numeric type";
                            }

                        }
                        if (!string.IsNullOrEmpty(WHTAmountString))
                        {
                            decimal WhtValue;
                            if (Decimal.TryParse(WHTAmountString, out WhtValue))
                            {
                                if (WhtValue >= 0)
                                {
                                    if (string.IsNullOrEmpty(WitholdingTaxNo)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Witholding Tax No can't be empy"; }
                                    if (string.IsNullOrEmpty(WitholdingTaxDateString)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Witholding Tax Date can't be empy"; }
                                    if (string.IsNullOrEmpty(WHTArticle)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "WHT Article can't be empy"; }
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "WHT Amount Valid format numeric type";
                            }
                        }


                        // Kolom PV no tidak terisi jika Accrued Year End Status :
                        //Accrued List – Direct
                        //   Accrued List – Suspense
                        //   Accrued List – PR / PO
                        //if (!AccruedYearEndStatus.Contains("Accrued List"))
                        //{
                        //    if (string.IsNullOrEmpty(PVNo)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "PV No can't be empy"; }
                        //}
                        //else
                        //{
                        //    PVNo = null;
                        //}

                        //if (string.IsNullOrEmpty(PVCreatedBy)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "PV Created By can't be empy"; }
                        //if (string.IsNullOrEmpty(SAPGRDocNo)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "SAP GR Doc No can't be empy"; }
                        //if (string.IsNullOrEmpty(SAPGRPostingDateString)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "SAP GR Posting Date can't be empy"; }
                        if (string.IsNullOrEmpty(SAPInvoiceDocNo)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "SAP Invoice Doc No can't be empy"; }
                        if (string.IsNullOrEmpty(SAPInvoicePostingDateString)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "SAP Invoice Posting Date can't be empy"; }
                        //if (string.IsNullOrEmpty(ReceiveFileDateString)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Receive File Date can't be empy"; }
                        //if (string.IsNullOrEmpty(DownloadedDateString)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Downloaded Date can't be empy"; }
                        //if (string.IsNullOrEmpty(DownloadStatus)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Download Status can't be empy"; }
                        //if (string.IsNullOrEmpty(NominativeFormAttachment)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Nominative Form Attachment can't be empy"; }
                        //if (string.IsNullOrEmpty(NominativeIDNo)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Nominative ID No can't be empy"; }
                        //if (string.IsNullOrEmpty(NominativeStatus)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Nominative Status can't be empy"; }
                        if (string.IsNullOrEmpty(NominativeDateString)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Nominative Date can't be empy"; }

                        //must be less than or equal to
                        if (!string.IsNullOrEmpty(NominativeType)) { if (NominativeType.Length >= 30) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Nominative Type must be less than or equal to 30 character"; } }
                        if (!string.IsNullOrEmpty(StatusInvoice)) { if (StatusInvoice.Length >= 20) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Invoice Status must be less than or equal to 20 character"; } }
                        if (!string.IsNullOrEmpty(PVType)) { if (PVType.Length >= 30) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "PV Type must be less than or equal to 30 character"; } }

                        if (!string.IsNullOrEmpty(AccruedYearEndStatus)) { if (AccruedYearEndStatus.Length >= 30) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Accrued Year End Status must be less than or equal to 30 character"; } }
                        if (!string.IsNullOrEmpty(AccruedBookingNo)) { if (AccruedBookingNo.Length >= 30) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Accrued Booking No must be less than or equal to 30 character"; } }

                        //if (!string.IsNullOrEmpty(SettlementSuspenseStatus)) { if (SettlementSuspenseStatus.Length >= 30) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Settlement Suspense Status must be less than or equal to 30 character"; } }
                        if (!string.IsNullOrEmpty(DescriptionTransaction)) { if (DescriptionTransaction.Length >= 100) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Description Transaction must be less than or equal to 100 character"; } }
                        if (!string.IsNullOrEmpty(GLAccount)) { if (GLAccount.Length >= 20) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "GL Account must be less than or equal to 20 character"; } }
                        if (!string.IsNullOrEmpty(VendorName)) { if (VendorName.Length >= 200) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Vendor Name must be less than or equal to 200 character"; } }
                        //if (!string.IsNullOrEmpty(Npwp)) { if (Npwp.Length >= 20) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "NPWP must be less than or equal to 20 character"; } }
                        if (!string.IsNullOrEmpty(Npwp)) { if (Npwp.Length > 15 || Npwp.Length < 15) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "NPWP must be equal to 15 character"; } }
                        if (!string.IsNullOrEmpty(InvoiceNo)) { if (InvoiceNo.Length >= 30) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Invoice No must be less than or equal to 30 character"; } }
                        //if (!string.IsNullOrEmpty(InvoiceDateString)) { if (InvoiceDateString.Length >= 10) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Invoice Date must be less than or equal to 10 character";} }
                        //if (!string.IsNullOrEmpty(BookPeriodString)) { if (BookPeriodString.Length >= 10) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Book Period must be less than or equal to 10 character"; } }
                        //if (!string.IsNullOrEmpty(TurnoverAmountString)) { if (TurnoverAmountString.Length >= 12) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "VAT Based Amount must be less than or equal to 12 character";} }
                        //if (!string.IsNullOrEmpty(VATAmountString)) { if (VATAmountString.Length >= 12) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "VAT Amount must be less than or equal to 12 character";} }
                        //if (!string.IsNullOrEmpty(WHTAmountString)) { if (WHTAmountString.Length >= 12) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "WHT Amount must be less than or equal to 12 character";} }
                        //if (!string.IsNullOrEmpty(TaxInvoiceNo)) { if (TaxInvoiceNo.Length >= 190) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Tax Invoice No must be less than or equal to 19 character"; } }
                        if (!string.IsNullOrEmpty(TaxInvoiceNo))
                        {

                            if (TaxInvoiceNo.Length > 16 || TaxInvoiceNo.Length < 16)
                            {
                                if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Tax Invoice No must be equal to 16 character";
                            }

                        }
                        if (!string.IsNullOrEmpty(WitholdingTaxNo)) { if (WitholdingTaxNo.Length >= 30) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Witholding Tax No must be less than or equal to 30 character"; } }
                        //if (!string.IsNullOrEmpty(WitholdingTaxDateString)) { if (WitholdingTaxDateString.Length >= 10) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Witholding Tax Date must be less than or equal to 10 character";} }
                        if (!string.IsNullOrEmpty(WHTArticle)) { if (WHTArticle.Length >= 30) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "WHT Article must be less than or equal to 30 character"; } }
                        if (!string.IsNullOrEmpty(PVNo)) { if (PVNo.Length >= 30) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "PV No must be less than or equal to 30 character"; } }
                        if (!string.IsNullOrEmpty(PVCreatedBy)) { if (PVCreatedBy.Length >= 30) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "PV Created By must be less than or equal to 30 character"; } }
                        if (!string.IsNullOrEmpty(SAPGRDocNo)) { if (SAPGRDocNo.Length >= 30) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "SAP GR Doc No must be less than or equal to 30 character"; } }
                        //if (!string.IsNullOrEmpty(SAPGRPostingDateString)) { if (SAPGRPostingDateString.Length >= 10) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "SAP GR Posting Date must be less than or equal to 10 character";} }
                        if (!string.IsNullOrEmpty(SAPInvoiceDocNo)) { if (SAPInvoiceDocNo.Length >= 30) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "SAP Invoice Doc No must be less than or equal to 30 character"; } }
                        //if (!string.IsNullOrEmpty(SAPInvoicePostingDateString)) { if (SAPInvoicePostingDateString.Length >= 10) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "SAP Invoice Posting Date must be less than or equal to 10 character";} }
                        //if (!string.IsNullOrEmpty(ReceiveFileDateString)) { if (ReceiveFileDateString.Length >= 10) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Receive File Date must be less than or equal to 10 character";} }
                        //if (!string.IsNullOrEmpty(DownloadedDateString)) { if (DownloadedDateString.Length >= 10) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Downloaded Date must be less than or equal to 10 character";} }
                        //if (!string.IsNullOrEmpty(DownloadStatus)) { if (DownloadStatus.Length >= 30) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Download Status must be less than or equal to 30 character"; } }
                        //if (!string.IsNullOrEmpty(NominativeFormAttachment)) { if (NominativeFormAttachment.Length >= 30) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Nominative Form Attachment must be less than or equal to 30 character"; } } //di database varchar(max)
                        //if (!string.IsNullOrEmpty(NominativeIDNo)) { if (NominativeIDNo.Length >= 30) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Nominative ID No must be less than or equal to 30 character"; } }
                        // if (!string.IsNullOrEmpty(NominativeStatus)) { if (NominativeStatus.Length >= 30) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Nominative Status must be less than or equal to 30 character"; } }
                        //if (!string.IsNullOrEmpty(NominativeDateString)) { if (NominativeDateString.Length >= 10) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Nominative Date must be less than or equal to 10 character"; } }


                        //validasi tidak di pakai dikarenakan npwp dan tax number ketika upload yg di input hanya numbernya saja ketika proses upload baru membentuk format npwp dan tax number
                        ////validation format NPWP (00.000.000.0-000.000) and Tax Invoice No (000.000-00.00000000) 
                        //Regex npwpFormat = new Regex(@"^[0-9]{2}[.][\d]{3}[.][\d]{3}[.][\d][-][\d]{3}[.][\d]{3}$");
                        //Regex taxInvoiceNoFormat = new Regex(@"^[0-9]{3}[.][\d]{3}[-][\d]{2}[.][\d]{8}$");
                        //if (!npwpFormat.IsMatch(Npwp)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "NPWP format must be 00.000.000.0-000.000"; }
                        //if (!taxInvoiceNoFormat.IsMatch(TaxInvoiceNo)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Tax Invoice No format must be 000.000-00.00000000"; }

                        //if (!string.IsNullOrEmpty(AccruedBookingNo))
                        //{
                        //    if (AccruedBookingNo.Length > 2)
                        //    {
                        //        if (AccruedBookingNo.Substring(0, 2) != "BN")
                        //        {
                        //            if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Accrued Booking format must be BNXXXXXXXX";
                        //        }
                        //    }
                        //    else
                        //    {
                        //        if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Accrued Booking format must be BNXXXXXXXX";
                        //    }
                        //}
                        if (!string.IsNullOrEmpty(AccruedBookingNo)) { if (AccruedBookingNo.Length >= 30) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Accrued Booking must be less than or equal to 30 character"; } }

                      

                        if (!string.IsNullOrEmpty(GLAccount))
                        {
                            if (!regex.IsMatch(GLAccount))
                            {
                                if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "GL Account Valid format numeric type";
                            }
                        }

                        if (!string.IsNullOrEmpty(Npwp))
                        {
                            if (!decimal.TryParse(Npwp, out decimal NPWPNUMBER)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "NPWP Valid format numeric type"; }
                            if (Npwp.Length == 15)
                            {
                                NPWPFormat = $"{Npwp.Substring(0, 2)}.{Npwp.Substring(2, 3)}.{Npwp.Substring(5, 3)}.{Npwp.Substring(8, 1)}-{Npwp.Substring(9, 3)}.{Npwp.Substring(12, 3)}";
                            }
                        }

                        if (!string.IsNullOrEmpty(TaxInvoiceNo))
                        {

                            if (!decimal.TryParse(TaxInvoiceNo, out decimal TaxInvoiceNonumber)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Tax Invoice No Valid format numeric type"; }
                            if (TaxInvoiceNo.Length == 16)
                            {
                                TaxFormat = $"{TaxInvoiceNo.Substring(0, 3)}.{TaxInvoiceNo.Substring(3, 3)}-{TaxInvoiceNo.Substring(6, 2)}.{TaxInvoiceNo.Substring(8, 8)}";
                            }

                        }

                        if (!string.IsNullOrEmpty(InvoiceDateString))
                        {
                            double InvoiceDateDouble;
                            if (!double.TryParse(InvoiceDateString, out InvoiceDateDouble))
                            {
                                DateTime InvoiceDate1;
                                if (!DateTime.TryParseExact(InvoiceDateString, formatDate, CultureInfo.InvariantCulture, DateTimeStyles.None, out InvoiceDate1))
                                {
                                    if (!string.IsNullOrEmpty(validation)) validation += ", ";
                                    validation += "Invoice Date format must be DD/MM/YYYY";
                                    break;
                                }
                                else
                                {
                                    InvoiceDate = InvoiceDate1;
                                }

                            }
                            else
                            {
                                InvoiceDate = DateTime.FromOADate(InvoiceDateDouble);
                            }
                        }

                        if (!string.IsNullOrEmpty(BookPeriodString))
                        {
                            double BookPeriodDouble;
                            if (!double.TryParse(BookPeriodString, out BookPeriodDouble))
                            {
                                DateTime BookPeriod1;
                                if (!DateTime.TryParseExact(BookPeriodString, formatDate, CultureInfo.InvariantCulture, DateTimeStyles.None, out BookPeriod1))
                                {
                                    if (!string.IsNullOrEmpty(validation)) validation += ", ";
                                    validation += "Book Period format must be DD/MM/YYYY";
                                }
                                else
                                {
                                    BookPeriod = BookPeriod1;
                                }

                            }
                            else
                            {
                                BookPeriod = DateTime.FromOADate(BookPeriodDouble);
                            }
                        }

                        if (!string.IsNullOrEmpty(TurnoverAmountString)) { if (!decimal.TryParse(TurnoverAmountString, out TurnoverAmount)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "VAT Based Amount Valid format numeric type"; } }
                        //if (!string.IsNullOrEmpty(VATAmountString)) { if (!decimal.TryParse(VATAmountString, out VATAmount)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "VAT Amount Valid format numeric type"; } }
                        //if (!string.IsNullOrEmpty(WHTAmountString)) { if (!decimal.TryParse(WHTAmountString, out WHTAmount)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "WHT Amount Valid format numeric type"; } }

                        if (!string.IsNullOrEmpty(WitholdingTaxDateString))
                        {
                            double WitholdingTaxDateDouble;
                            if (!double.TryParse(WitholdingTaxDateString, out WitholdingTaxDateDouble))
                            {
                                DateTime WitholdingTaxDate1;
                                if (!DateTime.TryParseExact(WitholdingTaxDateString, formatDate, CultureInfo.InvariantCulture, DateTimeStyles.None, out WitholdingTaxDate1))
                                {
                                    if (!string.IsNullOrEmpty(validation)) validation += ", ";
                                    validation += "Witholding Tax Date format must be DD/MM/YYYY";
                                }
                                else
                                {
                                    WitholdingTaxDate = WitholdingTaxDate1;
                                }

                            }
                            else
                            {
                                WitholdingTaxDate = DateTime.FromOADate(WitholdingTaxDateDouble);
                            }
                        }

                        //if (!string.IsNullOrEmpty(PVNo))
                        //{

                        //    if (decimal.TryParse(PVNo, out decimal PVNoNumber))
                        //    {
                        //        PVNo = Convert.ToString(PVNoNumber);
                        //    }
                        //    else
                        //    {
                        //        if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "PV No Valid format numeric type";
                        //    }
                        //}

                        if (!string.IsNullOrEmpty(PVNo))
                        {
                            if (!regex.IsMatch(PVNo))
                            {
                                if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "PV No Valid format numeric type";
                            }
                        }

                        if (!string.IsNullOrEmpty(SAPGRPostingDateString))
                        {
                            double SAPGRPostingDateDouble;
                            if (!double.TryParse(SAPGRPostingDateString, out SAPGRPostingDateDouble))
                            {
                                DateTime SAPGRPostingDate1;
                                if (!DateTime.TryParseExact(SAPGRPostingDateString, formatDate, CultureInfo.InvariantCulture, DateTimeStyles.None, out SAPGRPostingDate1))
                                {
                                    if (!string.IsNullOrEmpty(validation)) validation += ", ";
                                    validation += "SAP GR Posting Date format must be DD/MM/YYYY";
                                }
                                else
                                {
                                    SAPGRPostingDate = SAPGRPostingDate1;
                                }
                            }
                            else
                            {
                                SAPGRPostingDate = DateTime.FromOADate(SAPGRPostingDateDouble);
                            }
                        }

                        if (!string.IsNullOrEmpty(SAPInvoicePostingDateString))
                        {
                            double SAPInvoicePostingDateDouble;
                            if (!double.TryParse(SAPInvoicePostingDateString, out SAPInvoicePostingDateDouble))
                            {
                                DateTime SAPInvoicePostingDate1;
                                if (!DateTime.TryParseExact(SAPInvoicePostingDateString, formatDate, CultureInfo.InvariantCulture, DateTimeStyles.None, out SAPInvoicePostingDate1))
                                {
                                    if (!string.IsNullOrEmpty(validation)) validation += ", ";
                                    validation += "SAP Invoice Posting Date format must be DD/MM/YYYY";
                                }
                                else
                                {
                                    SAPInvoicePostingDate = SAPInvoicePostingDate1;
                                }
                            }
                            else
                            {
                                SAPInvoicePostingDate = DateTime.FromOADate(SAPInvoicePostingDateDouble);
                            }
                        }

                        //if (!string.IsNullOrEmpty(ReceiveFileDateString))
                        //{
                        //    double ReceiveFileDateDouble;
                        //    if (!double.TryParse(ReceiveFileDateString, out ReceiveFileDateDouble))
                        //    {
                        //        DateTime ReceiveFileDate1;
                        //        if (!DateTime.TryParseExact(ReceiveFileDateString, "d/M/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out ReceiveFileDate1))
                        //        {
                        //            if (!DateTime.TryParseExact(ReceiveFileDateString, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out ReceiveFileDate1))
                        //            {
                        //                if (!DateTime.TryParseExact(ReceiveFileDateString, "dd.MM.yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out ReceiveFileDate1))
                        //                {
                        //                    if (!string.IsNullOrEmpty(validation)) validation += ", ";
                        //                    validation += "Receive File Date format must be DD/MM/YYYY";
                        //                }
                        //                else
                        //                {
                        //                    ReceiveFileDate = ReceiveFileDate1;
                        //                }
                        //            }
                        //            else
                        //            {
                        //                ReceiveFileDate = ReceiveFileDate1;
                        //            }
                        //        }
                        //        else
                        //        {
                        //            ReceiveFileDate = ReceiveFileDate1;
                        //        }
                        //    }
                        //    else
                        //    {
                        //        ReceiveFileDate = DateTime.FromOADate(ReceiveFileDateDouble);
                        //    }
                        //}

                        //if (!string.IsNullOrEmpty(DownloadedDateString))
                        //{
                        //    double DownloadedDateDouble;
                        //    if (!double.TryParse(DownloadedDateString, out DownloadedDateDouble))
                        //    {
                        //        DateTime DownloadedDate1; 
                        //        if (!DateTime.TryParseExact(DownloadedDateString, formatDate, CultureInfo.InvariantCulture, DateTimeStyles.None, out DownloadedDate1))
                        //        {
                        //            if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Downloaded Date format must be DD/MM/YYYY";
                        //        }
                        //        else
                        //        {
                        //            DownloadedDate = DownloadedDate1;
                        //        } 
                        //    }
                        //    else
                        //    {
                        //        DownloadedDate = DateTime.FromOADate(DownloadedDateDouble);
                        //    }
                        //}

                        if (!string.IsNullOrEmpty(SAPGRDocNo))
                        {
                            if (!regex.IsMatch(SAPGRDocNo))
                            {
                                if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "SAP GR DOC NO Valid format numeric type";
                            }
                        }

                        if (!string.IsNullOrEmpty(SAPInvoiceDocNo))
                        {
                            if (!regex.IsMatch(SAPInvoiceDocNo))
                            {
                                if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "SAP INVOICE DOC NO Valid format numeric type";
                            }
                        }


                        if (!string.IsNullOrEmpty(NominativeDateString))
                        {
                            double NominativeDateDouble;
                            if (!double.TryParse(NominativeDateString, out NominativeDateDouble))
                            {
                                DateTime NominativeDate1;
                                if (!DateTime.TryParseExact(NominativeDateString, formatDate, CultureInfo.InvariantCulture, DateTimeStyles.None, out NominativeDate1))
                                {
                                    if (!string.IsNullOrEmpty(validation)) validation += ", ";
                                    validation += "(Book Period format must be DD/MM/YYYY";
                                }
                                else
                                {
                                    NominativeDate = NominativeDate1;
                                }
                            }
                            else
                            {
                                NominativeDate = DateTime.FromOADate(NominativeDateDouble);
                            }
                        }

                        //var  model2 = new Dashboard();
                        //  model2.NominativeType = NominativeType;
                        //  model2.StatusInvoice = StatusInvoice;
                        //  model2.PVType = PVType;
                        //  model2.AccruedYearEndStatus = AccruedYearEndStatus; 
                        //  model2.DescriptionTransaction = DescriptionTransaction;
                        //  model2.GLAccount = GLAccount;
                        //  model2.TaxInvoiceNo = TaxFormat;
                        //  model2.WHTArticle = WHTArticle;
                        //  model2.NominativeStatus = "Manual";

                        //  // Save
                        //  result = db.SingleOrDefault<Result>("Dashboard_Nominative/usp_Validation_TB_R_Dashboard_Nominative", model2.MapFromModelValidation());



                        //  //end validasi//
                        //  if (!string.IsNullOrEmpty(validation) && result.ResultCode == false)
                        //  {

                        //      throw new Exception(validation+"," + result.ResultDesc);
                        //  }

                        //  if (!string.IsNullOrEmpty(validation))
                        //  {

                        //      throw new Exception(validation);
                        //  }
                        #endregion

                        var model = new Dashboard();
                        model.Id = Guid.NewGuid();
                        model.NominativeType = NominativeType;
                        model.StatusInvoice = StatusInvoice;
                        model.PVType = PVType;
                        model.AccruedYearEndStatus = AccruedYearEndStatus;
                        model.AccruedBookingNo = AccruedBookingNo;
                        //model.SettlementStatus = SettlementSuspenseStatus;
                        model.DescriptionTransaction = DescriptionTransaction;
                        model.GLAccount = GLAccount;
                        model.VendorName = VendorName;
                        //model.Npwp = Npwp;
                        model.Npwp = NPWPFormat;
                        model.InvoiceNo = InvoiceNo;
                        model.InvoiceDate = defaultDate(InvoiceDate);
                        model.BookPeriod = defaultDate(BookPeriod);
                        model.TurnoverAmount = TurnoverAmount;
                        model.VATAmount = VATAmount;
                        model.WHTAmount = WHTAmount;
                        //model.TaxInvoiceNo = TaxInvoiceNo;
                        model.TaxInvoiceNo = TaxFormat;
                        model.WitholdingTaxNo = WitholdingTaxNo;
                        model.WitholdingTaxDate = defaultDate(WitholdingTaxDate);
                        model.WHTArticle = WHTArticle;
                        model.PVNo = PVNo;
                        model.PVCreatedBy = PVCreatedBy;
                        model.SAPGRDocNo = SAPGRDocNo;
                        model.SAPGRPostingDate = defaultDate(SAPGRPostingDate);
                        model.SAPInvoiceDocNo = SAPInvoiceDocNo;
                        model.SAPInvoicePostingDate = defaultDate(SAPInvoicePostingDate);
                        model.ReceiveFileDate = DateTime.Now;
                        //model.DownloadedDate = DownloadedDate;
                        //model.DownloadStatus = DownloadStatus;
                        //model.NominativeFormAttachment = NominativeFormAttachment;
                        //model.NominativeIDNo = NominativeIDNo;
                        model.NominativeStatus = "Manual";
                        model.NominativeDate = defaultDate(NominativeDate);
                        model.Validation = validation;


                        // Save
                        result = db.SingleOrDefault<Result>("Dashboard_Nominative/usp_Insert_TB_R_Dashboard_Nominative", model.MapFromModel(DateTime.Now, EventActor));

                        if (!result.ResultCode)
                        {
                            throw new Exception(result.ResultDesc);
                        }

                        logSyncList.Add(new LogSyncCreateUpdate()
                           .CreateModel(Path.GetFileName(path), row + 1, pipedString, MessageType.INF, ExceptionHandler.GENERAL_SUCCESS_MESSAGE, path));
                        rowSuccess++;
                        db.CommitTransaction();
                        #endregion
                    }
                    catch (Exception e)
                    {
                        db.AbortTransaction();
                        e.LogToSync(row + 1, pipedString, Path.GetFileName(path), path, MessageType.ERR);
                        rowFail++;
                    }

                }

                #region SyncLog Insert
                db.BeginTransaction();
                ExceptionHandler.LogToSync(logSyncList);
                db.CommitTransaction();
                #endregion

                if (rowFail == 0)
                {
                    result.ResultCode = true;
                }
                else
                {
                    result.ResultCode = false;
                }
                result.ResultDesc = Formatting
                    .UPLOAD_NOTIFICATION
                    .Replace("@success", (rowSuccess).ToString())
                    .Replace("@error", (rowFail).ToString());
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result.ResultCode = false;
                result.ResultDesc = e.LogToSync(row - 3, pipedString, Path.GetFileName(path), path, MessageType.ERR).Message;
            }

            db.Close();
            return result;
        }
        #endregion
        public DateTime defaultDate(DateTime? _date)
        {
            DateTime? datedata;
            if (_date == null)
            {
                datedata = Convert.ToDateTime("01/01/1900");
            }
            else
            {
                datedata = _date;
            }
            return (DateTime)datedata;
        }


        #region Report
        public void LastDownloaded(string DashboardIdList)
        {
            dynamic argsUpdate = new { DashboardIdList = DashboardIdList };
            db.Execute("Dashboard_Nominative/usp_UpdateDashboardDownloaded", argsUpdate);
        }

        public List<Dashboard> GetOriginalById(DashboardSearch model)
        {
            IEnumerable<Dashboard> result = db.Query<Dashboard>("Dashboard_Nominative/usp_GetDashboardExcel", model.MapFromModelSearch("", 1, "DESC"));
            return result.ToList();
        }

        public List<NominativeTypeReport> GetNominativeTypeDashboard(DashboardSearch model)
        {
            IEnumerable<NominativeTypeReport> result = db.Query<NominativeTypeReport>("Dashboard_Nominative/usp_GetNominativeTypeDashboard", model.MapFromModelSearch("", 1, "DESC"));
            return result.ToList();
        }

        public List<ReportPathFrom> GetNullablePathFromPdf(DashboardSearch model)
        {
            IEnumerable<ReportPathFrom> result = db.Query<ReportPathFrom>("Dashboard_Nominative/usp_GetNullablePathFromPdf", model.MapFromModelSearch("", 1, "DESC"));
            return result.ToList();
        }

        public List<ReportPathFrom> GetPathFromPdf(DashboardSearch model)
        {
            IEnumerable<ReportPathFrom> result = db.Query<ReportPathFrom>("Dashboard_Nominative/usp_GetPathFromPdf", model.MapFromModelSearch("", 1, "DESC"));
            return result.ToList();
        }

        public List<PromotionReportXls> GetPromotionFromExcel(DashboardSearch model)
        {
            IEnumerable<PromotionReportXls> result = db.Query<PromotionReportXls>("Dashboard_Nominative/usp_GetPromotionExcel", model.MapFromModelSearch("", 1, "DESC"));
            return result.ToList();
        }

        public List<PromotionReport> GetPromotionReport(DashboardSearch model)
        {
            IEnumerable<PromotionReport> result = db.Query<PromotionReport>("Dashboard_Nominative/usp_GetPromotionReport", model.MapFromModelSearch("", 1, "DESC"));
            return result.ToList();
        }

        public List<EntertainmentReport> GetEntertainmentReport(DashboardSearch model)
        {
            IEnumerable<EntertainmentReport> result = db.Query<EntertainmentReport>("Dashboard_Nominative/usp_GetEntertainmentReport", model.MapFromModelSearch("", 1, "DESC"));
            return result.ToList();
        }

        public List<DonationReportXls> GetDonationFrom(DashboardSearch model)
        {
            IEnumerable<DonationReportXls> result = db.Query<DonationReportXls>("Dashboard_Nominative/usp_GetDonationExcel", model.MapFromModelSearch("", 1, "DESC"));
            return result.ToList();
        }

        public List<EntertainmentReportXls> GetEntertainmentFrom(DashboardSearch model)
        {
            IEnumerable<EntertainmentReportXls> result = db.Query<EntertainmentReportXls>("Dashboard_Nominative/usp_GetEntertainmentExcel", model.MapFromModelSearch("", 1, "DESC"));
            return result.ToList();
        }
        #endregion

        #region Dropdownlist 
        public List<DropdownModel> GetTransactionTypeDropdownList()
        {
            IEnumerable<DropdownModel> result = db.Query<DropdownModel>("Dashboard_Nominative/usp_DropDownTransactionType");
            return result.ToList();
        }

        public List<DropdownModel> GetSystemDropdownList()
        {
            IEnumerable<DropdownModel> result = db.Query<DropdownModel>("Dashboard_Nominative/usp_DropDownSystemDashboard");
            return result.ToList();
        }

        public List<DropdownModel> GetGLAccountDropdownList()
        {
            IEnumerable<DropdownModel> result = db.Query<DropdownModel>("Dashboard_Nominative/usp_DropDownGLAccount");
            return result.ToList();
        }
        public List<DropdownModel> GetCategoryNominativeDropdownList()
        {

            IEnumerable<DropdownModel> result = db.Query<DropdownModel>("Dashboard_Nominative/usp_DropDownCategoryNominativeDashbord");
            return result.ToList();
        }
        #endregion


        #region QR CODE
        public string GenerateQrCode(string text)
        {
            byte[] imgArray;
            // string name = "</? xml version = \"1.0\" encoding = \"UTF - 8\" ?>< PrintLetterBarcodeData uid = \"358655541160\" name = \"St. Paul Premkumar\" gender = \"MALE\" yob =  \"1981\" co = \"S/O Steepan\" lm = \"SN.Nagar\" loc = \"null\" vtc = \"Anna Nagar\" po = \"Anna Nagar West\" dist = \"Chennai\" state = \"Tamil Nadu\" pc = \"601502\" dob = \"12-06-1981\" />";

            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(text, QRCodeGenerator.ECCLevel.Q);
            QRCoder.QRCode qrCode = new QRCoder.QRCode(qrCodeData);
            Bitmap qrCodeImage = qrCode.GetGraphic(20);

            using (MemoryStream imageStream = new MemoryStream())
            {
                qrCodeImage.Save(imageStream, ImageFormat.Png);
                imgArray = new byte[imageStream.Length];
                imageStream.Seek(0, SeekOrigin.Begin);
                imageStream.Read(imgArray, 0, (int)imageStream.Length);
            }
            // return imgArray;
            return Convert.ToBase64String(imgArray);
        }
        #endregion

        //Update Data (create by Ckristian 27 Mei 2021)
        public Result UpdatePathUpload(string Reffno, string fullPath)
        {
            try
            {
                dynamic args = new
                {
                    NominativeIDNo = Reffno,
                    NominativeFormAttachment = fullPath
                };
                db.Execute("Dashboard_Nominative/usp_UpdatePathDashboardUpload", args);
                return MsgResultSuccessUpdate();
            }
            catch (Exception e)
            {
                return MsgResultError(e.Message.ToString());
            }
        }

        public Dashboard GetRefferenceNumber(string GuidId)
        {
            return db.SingleOrDefault<Dashboard>("Dashboard_Nominative/usp_GetReffNoDashboardUpload", new { ID = GuidId });
        }
    }
}
