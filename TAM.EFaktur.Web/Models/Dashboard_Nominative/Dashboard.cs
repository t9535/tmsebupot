﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TAM.EFaktur.Web.Models.Dashboard_Nominative
{
    public class Dashboard : BaseModel
    {
        [MaxLength(20)]
        public string CategoryCode { get; set; }

        //not mapped
        public string NominativeType { get; set; }

        [MaxLength(20)]
        public string StatusInvoice { get; set; }

        [MaxLength(30)]
        public string PVType { get; set; }

        [MaxLength(30)]
        public string SettlementStatus { get; set; }

        [MaxLength(30)]
        public string AccruedYearEndStatus { get; set; }

        [MaxLength(30)]
        public string AccruedBookingNo { get; set; }

        [MaxLength(100)]
        public string DescriptionTransaction { get; set; }

        [MaxLength(20)]
        public string GLAccount { get; set; }

        [MaxLength(200)]
        public string VendorName { get; set; }

        [MaxLength(30)]
        public string Npwp { get; set; }

        [MaxLength(30)]
        public string InvoiceNo { get; set; }

        public Nullable<DateTime>  InvoiceDate { get; set; }

        public Nullable<DateTime> BookPeriod { get; set; }

        public Nullable<decimal> TurnoverAmount { get; set; }

        public Nullable<decimal> VATAmount { get; set; }

        public Nullable<decimal> WHTAmount { get; set; }

        [MaxLength(30)]
        public string TaxInvoiceNo { get; set; }

        [MaxLength(30)]
        public string WitholdingTaxNo { get; set; }

        public Nullable<DateTime> WitholdingTaxDate { get; set; }

        [MaxLength(30)]
        public string WHTArticle { get; set; }

        [MaxLength(30)]
        public string System { get; set; }

        [MaxLength(30)]
        public string PVNo { get; set; }

        [MaxLength(30)]
        public string PVCreatedBy { get; set; }

        [MaxLength(30)]
        public string SAPGRDocNo { get; set; }

        public Nullable<DateTime> SAPGRPostingDate { get; set; }

        [MaxLength(30)]
        public string SAPInvoiceDocNo { get; set; }

        public Nullable<DateTime> SAPInvoicePostingDate { get; set; }

        public Nullable<DateTime> ReceiveFileDate { get; set; }

        public Nullable<DateTime> DownloadedDate { get; set; }

        [MaxLength(30)]
        public string DownloadStatus { get; set; }

        public Nullable<DateTime> NominativeDate { get; set; }

        public string NominativeFormAttachment { get; set; }

        [MaxLength(30)]
        public string NominativeIDNo { get; set; }

        [MaxLength(30)]
        public string NominativeStatus { get; set; }

        public string Validation { get; set; }

        public dynamic MapFromModel(DateTime? EventDate = null, string EventActor = "")
        {
            dynamic args = new
            {
                Id = this.Id,
                CategoryCode = this.CategoryCode,
                NominativeType = this.NominativeType,
                StatusInvoice = this.StatusInvoice,
                PVType = this.PVType,
                AccruedYearEndStatus = this.AccruedYearEndStatus,
                AccruedBookingNo = this.AccruedBookingNo,
                SettlementStatus = this.SettlementStatus,
                DescriptionTransaction = this.DescriptionTransaction,
                GLAccount = this.GLAccount,
                VendorName = this.VendorName,
                Npwp = this.Npwp,
                InvoiceNo = this.InvoiceNo,
                InvoiceDate = this.InvoiceDate.Value,
                BookPeriod = this.BookPeriod,
                TurnoverAmount = this.TurnoverAmount,
                VATAmount = this.VATAmount,
                WHTAmount = this.WHTAmount,
                TaxInvoiceNo = this.TaxInvoiceNo,
                WitholdingTaxNo = this.WitholdingTaxNo,
                WitholdingTaxDate = this.WitholdingTaxDate,
                WHTArticle = this.WHTArticle,
                //System = this.System,
                PVNo = this.PVNo,
                PVCreatedBy = this.PVCreatedBy,
                SAPGRDocNo = this.SAPGRDocNo,
                SAPGRPostingDate = this.SAPGRPostingDate,
                SAPInvoiceDocNo = this.SAPInvoiceDocNo,
                SAPInvoicePostingDate = this.SAPInvoicePostingDate,
                ReceiveFileDate = this.ReceiveFileDate,
                DownloadedDate = this.DownloadedDate,
                DownloadStatus = this.DownloadStatus,
                NominativeFormAttachment = this.NominativeFormAttachment,
                NominativeIDNo = this.NominativeIDNo,
                NominativeStatus = this.NominativeStatus,
                NominativeDate = this.NominativeDate,
                CreatedOn = (EventDate.HasValue ? EventDate.Value : DateTime.Now).FormatSQLDateTime(),
                CreatedBy = EventActor,
                ModifiedOn = (EventDate.HasValue ? EventDate.Value : DateTime.Now).FormatSQLDateTime(),
                ModifiedBy = EventActor,
                RowStatus = false,
                EventDate = EventDate.HasValue ? EventDate.Value : DateTime.Now,
                EventActor = EventActor,
                Validation = this.Validation
            };
            return args;
        }

        public dynamic MapFromModelValidation()
        {
            dynamic args = new
            {
                //Id = this.Id,
                //CategoryCode = this.CategoryCode,
                NominativeType = this.NominativeType,
                StatusInvoice = this.StatusInvoice,
                PVType = this.PVType,
                AccruedYearEndStatus = this.AccruedYearEndStatus,
                //AccruedBookingNo = this.AccruedBookingNo,
                //SettlementStatus = this.SettlementStatus,
                DescriptionTransaction = this.DescriptionTransaction,
                GLAccount = this.GLAccount,
                //VendorName = this.VendorName,
                //Npwp = this.Npwp,
                //InvoiceNo = this.InvoiceNo,
                //InvoiceDate = this.InvoiceDate.Value,
                //BookPeriod = this.BookPeriod,
                //TurnoverAmount = this.TurnoverAmount,
                //VATAmount = this.VATAmount,
                //WHTAmount = this.WHTAmount,
                TaxInvoiceNo = this.TaxInvoiceNo,
                //WitholdingTaxNo = this.WitholdingTaxNo,
                //WitholdingTaxDate = this.WitholdingTaxDate,
                WHTArticle = this.WHTArticle,
                //System = this.System,
                //PVNo = this.PVNo,
                //PVCreatedBy = this.PVCreatedBy,
                //SAPGRDocNo = this.SAPGRDocNo,
                //SAPGRPostingDate = this.SAPGRPostingDate,
                //SAPInvoiceDocNo = this.SAPInvoiceDocNo,
                //SAPInvoicePostingDate = this.SAPInvoicePostingDate,
                //ReceiveFileDate = this.ReceiveFileDate,
                //DownloadedDate = this.DownloadedDate,
                //DownloadStatus = this.DownloadStatus,
                //NominativeFormAttachment = this.NominativeFormAttachment,
                //NominativeIDNo = this.NominativeIDNo,
                NominativeStatus = this.NominativeStatus,
                //NominativeDate = this.NominativeDate,
                //CreatedOn = (EventDate.HasValue ? EventDate.Value : DateTime.Now).FormatSQLDateTime(),
                //CreatedBy = EventActor,
                //ModifiedOn = (EventDate.HasValue ? EventDate.Value : DateTime.Now).FormatSQLDateTime(),
                //ModifiedBy = EventActor,
                //RowStatus = false,
                //EventDate = EventDate.HasValue ? EventDate.Value : DateTime.Now,
                //EventActor = EventActor,
            };
            return args;
        }
        //for list
        public int RowNum { get; set; }
    }

    public class DashboardSearch : Dashboard
    {
        public string InvoiceDateFrom { get; set; }
        public string InvoiceDateTo { get; set; }

        public string BookPeriodFrom { get; set; }
        public string BookPeriodTo { get; set; }

        public string WitholdingTaxDateFrom { get; set; }
        public string WitholdingTaxDateTo { get; set; }

        public string SAPGRPostingDateFrom { get; set; }
        public string SAPGRPostingDateTo { get; set; }

        public string SAPInvoicePostingDateFrom { get; set; }
        public string SAPInvoicePostingDateTo { get; set; }

        public string ReceiveFileDateFrom { get; set; }
        public string ReceiveFileDateTo { get; set; }

        public string DownloadedDateFrom { get; set; }
        public string DownloadedDateTo { get; set; }

        public string TurnoverAmountOperator { get; set; }
        public string VATAmountOperator { get; set; }
        public string WHTAmountOperator { get; set; }

        public string NominativeDateFrom { get; set; }
        public string NominativeDateTo { get; set; }
        public string DashboardIdList { get; set; } //for download from xls

        public dynamic MapFromModelSearch(string DivisionName = "", int SortBy = 0, string SortDirection = "ASC", int FromNumber = 1, int ToNumber = 10, string Id = "")
        {
            return new
            {
                CategoryCode = this.CategoryCode,
                StatusInvoice = this.StatusInvoice,
                PVType = this.PVType,
                AccruedYearEndStatus = this.AccruedYearEndStatus,
                AccruedBookingNo = this.AccruedBookingNo,
                SettlementStatus = this.SettlementStatus,
                DescriptionTransaction = this.DescriptionTransaction,
                GLAccount = this.GLAccount,
                VendorName = this.VendorName,
                Npwp = this.Npwp,
                InvoiceNo = this.InvoiceNo,
                InvoiceDateFrom = this.InvoiceDateFrom.FormatDefaultDateWhenNullOrEmpty(),
                InvoiceDateTo = this.InvoiceDateTo.FormatDefaultDateWhenNullOrEmpty("TO"),
                BookPeriodFrom = this.BookPeriodFrom.FormatDefaultDateWhenNullOrEmpty(),
                BookPeriodTo = this.BookPeriodTo.FormatDefaultDateWhenNullOrEmpty("TO"),
                TurnoverAmountOperator = this.TurnoverAmountOperator,
                TurnoverAmount = this.TurnoverAmount,
                VATAmountOperator = this.VATAmountOperator,
                VATAmount = this.VATAmount,
                WHTAmountOperator = this.WHTAmountOperator,
                WHTAmount = this.WHTAmount,
                TaxInvoiceNo = this.TaxInvoiceNo,
                WitholdingTaxNo = this.WitholdingTaxNo,
                WitholdingTaxDateFrom = this.WitholdingTaxDateFrom.FormatDefaultDateWhenNullOrEmpty(),
                WitholdingTaxDateTo = this.WitholdingTaxDateTo.FormatDefaultDateWhenNullOrEmpty("TO"),
                WHTArticle = this.WHTArticle,
                System = this.System,
                PVNo = this.PVNo,
                PVCreatedBy = this.PVCreatedBy,
                SAPGRDocNo = this.SAPGRDocNo,
                SAPGRPostingDateFrom = this.SAPGRPostingDateFrom.FormatDefaultDateWhenNullOrEmpty(),
                SAPGRPostingDateTo = this.SAPGRPostingDateTo.FormatDefaultDateWhenNullOrEmpty("TO"),
                SAPInvoiceDocNo = this.SAPInvoiceDocNo,
                SAPInvoicePostingDateFrom = this.SAPInvoicePostingDateFrom.FormatDefaultDateWhenNullOrEmpty(),
                SAPInvoicePostingDateTo = this.SAPInvoicePostingDateTo.FormatDefaultDateWhenNullOrEmpty("TO"),
                ReceiveFileDateFrom = this.ReceiveFileDateFrom.FormatDefaultDateWhenNullOrEmpty(),
                ReceiveFileDateTo = this.ReceiveFileDateTo.FormatDefaultDateWhenNullOrEmpty("TO"),
                DownloadedDateFrom = this.DownloadedDateFrom.FormatDefaultDateWhenNullOrEmpty(),
                DownloadedDateTo = this.DownloadedDateTo.FormatDefaultDateWhenNullOrEmpty("TO"),
                DownloadStatus = this.DownloadStatus,
                NominativeFormAttachment = this.NominativeFormAttachment,
                NominativeIDNo = this.NominativeIDNo,
                NominativeStatus = this.NominativeStatus,
                NominativeDateFrom = this.NominativeDateFrom.FormatDefaultDateWhenNullOrEmpty(),
                NominativeDateTo = this.NominativeDateTo.FormatDefaultDateWhenNullOrEmpty("TO"),
                DashboardIdList = this.DashboardIdList,

                //for list data
                SortBy = SortBy,
                SortDirection = SortDirection,
                FromNumber = FromNumber,
                ToNumber = ToNumber
            };
        }

     
    }
}
