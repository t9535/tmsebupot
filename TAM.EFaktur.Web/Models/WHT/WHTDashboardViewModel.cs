﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TAM.EFaktur.Web.Models.WHT
{
    public class WHTDashboardViewModel : Controller
    {
        public int RowNum { get; set; }
        public Guid Id { get; set; }
        public string NomorFaktur { get; set; }
        public string NPWPPenjual { get; set; }
        public string NamaPenjual { get; set; }
        public decimal JumlahDPP { get; set; }
        public decimal JumlahPPH { get; set; }
        public DateTime TanggalFaktur { get; set; }
        public string Perekam { get; set; }
        public Nullable<DateTime> SyncTime { get; set; }
        public string TransitoryStatus { get; set; }
        public DateTime TransitoryDate { get; set; }
        public DateTime RecordTime { get; set; }
        public string RecordStatus { get; set; }
        public string DownloadStatus { get; set; }
        public Nullable<DateTime> CsvTime { get; set; }
        public string ApprovalStatus { get; set; }
        public DateTime ApprovalTime { get; set; }
        public string BatchFileName { get; set; }
        public string FakturType { get; set; }
        public string DaysExpired { get; set; }
        public string NomorInvoice { get; set; }
        public string StatusInvoice { get; set; }
        public string CreatedByPV { get; set; }
        public string NomorDocSAP { get; set; }
        public string StatusEPROC { get; set; }
        public string PDFWithholding { get; set; }
        public string NomorBuktiPotong { get; set; }
        public string NomorPV { get; set; }
        public string TransactionType { get; set; }
        public string StatusPostingSAP { get; set; }
        public DateTime CreatedOn { get; set; }
        public string JenisPajak { get; set; }
        public decimal Tarif { get; set; }
        public string TaxArticle { get; set; }
        public string TaxAdditionalInfo { get; set; }
        public string SPTType { get; set; }
        public DateTime PostingDate { get; set; }
        public string MasaPajak { get; set; }
        public string TahunPajak { get; set; }
        public DateTime TanggalBuktiPotong { get; set; }
        public DateTime TanggalPenerimaan { get; set; }
        public DateTime DownloadDate { get; set; }
        public string RelatedGLAccount { get; set; }
        public string PVCreatedBy { get; set; }
        public string Description { get; set; }
        public DateTime ApprovalDate { get; set; }
        public string ApprovalDesc { get; set; }
        public string NamaPenjualOriginal { get; set; }
        public decimal AccumulatedWHTBase { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string ErrorData { get; set; }
        public string eBupotNumber { get; set; }
        public string eBupotReferenceNo { get; set; }
        public string MapCode { get; set; }

        public string AlamatNPWP { get; set; }

        public string EFakturStatus { get; set; }

    }
}