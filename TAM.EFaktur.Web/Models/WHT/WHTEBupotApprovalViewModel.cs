﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.WHT
{
    public class WHTEBupotApprovalViewModel
    {
        public string NomorBuktiPotong { get; set; }
        public string eBupotNumber { get; set; }
        public string ApprovalStatus { get; set; }
        public Nullable<DateTime> ApprovalTime { get; set; }
        public DateTime RecordTime { get; set; }
        public string ApprovalDescription { get; set; }

        public string PDFWithholding { get; set; }

        public dynamic MapFromModel(DateTime EventDate, string EventActor)
        {
            return new
            {
                NomorBuktiPotong = NomorBuktiPotong,
               
                ApprovalStatus = ApprovalStatus,
                ApprovalTime = ApprovalTime,
                RecordTime = RecordTime,
                EventDate = EventDate,
                EventActor = EventActor,
                ApprovalDescription = ApprovalDescription,
                PDFWithholding = PDFWithholding,
            };
        }
    }
}