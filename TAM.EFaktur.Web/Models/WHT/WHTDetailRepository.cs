﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.WHT
{
    public class WHTDetailRepository : BaseRepository
    {
        #region Singleton
        private WHTDetailRepository() { }
        private static WHTDetailRepository instance = null;
        public static WHTDetailRepository Instance
        {
            get 
            {
                if (instance == null)
                {
                    instance = new WHTDetailRepository();
                }
                return instance;
            }
        }
        #endregion


    }
}