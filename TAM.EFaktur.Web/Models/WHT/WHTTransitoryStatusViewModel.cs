﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.WHT
{
    public class WHTTransitoryStatusViewModel
    {
        public string NomorFakturGabungan { get; set; }
        public string TransitoryStatus { get; set; }
        public DateTime TransitoryDate { get; set; }

        public dynamic MapFromModel(DateTime EventDate, string EventActor)
        {
            return new
            {
                NomorFakturGabungan = NomorFakturGabungan,
                TransitoryStatus = TransitoryStatus.ToUpper(),
                TransitoryDate = TransitoryDate,
                EventDate = EventDate,
                EventActor = EventActor
            };
        }
    }
}