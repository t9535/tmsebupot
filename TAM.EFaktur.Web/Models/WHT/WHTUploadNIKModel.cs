﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.WHT
{
    public class WHTUploadNIKModel
    {
        public string status { get; set; }
        public string eBupotReferenceNo { get; set; }
        public string NomorInvoice { get; set; }
        public string NomorPV { get; set; }
	    public string NamaPenjual { get; set; }
        public string TaxAdditionalInfo { get; set; }
        public dynamic MapFromModel()
        {
            return new
            {
                status= status,
                eBupotReferenceNo = eBupotReferenceNo,
                NomorInvoice= NomorInvoice,
                NomorPV = NomorPV,
                NamaPenjual = NamaPenjual,
                TaxAdditionalInfo = TaxAdditionalInfo
               
            };
        }
    }
}