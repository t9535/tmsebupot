﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.WHT
{
    public class WHTDetailManualInputViewModel
    {
        public int RowNum { get; set; }
        public String UnitName { get; set; }
        public Decimal UnitPrice { get; set; }
        public Decimal Quantity { get; set; }
        public Decimal TotalPrice { get; set; }
        public Decimal Discount { get; set; }
        public Decimal DPP { get; set; }
        public Decimal PPN { get; set; }
        public Decimal TarifPPNBM { get; set; }
        public Decimal PPNBM { get; set; }
    }
}