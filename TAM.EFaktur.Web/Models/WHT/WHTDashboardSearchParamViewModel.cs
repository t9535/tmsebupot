﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.WHT
{
    public class WHTDashboardSearchParamViewModel
    {
        public string TanggalFakturFrom { get; set; } 
        public string TanggalFakturTo { get; set; }
        public string TanggalFileDiterimaFrom { get; set; } 
        public string TanggalFileDiterimaTo { get; set; }
        public string WaktuFileDiterimaFrom { get; set; } 
        public string WaktuFileDiterimaTo { get; set; }
        public string TransitoryDateFrom { get; set; } 
        public string TransitoryDateTo { get; set; }
        public string TanggalDownloadFrom { get; set; } 
        public string TanggalDownloadTo { get; set; }
        public string RecordTimeFrom { get; set; } 
        public string RecordTimeTo { get; set; }
        public string ApprovalTimeFrom { get; set; } 
        public string ApprovalTimeTo { get; set; }
        public string NPWPPenjual { get; set; } 
        public string NamaPenjual { get; set; }
        public string NamaPenjualOriginal { get; set; }
        public string Perekam { get; set; } 
        public string FakturType { get; set; } 
        public string RecordStatus { get; set; } 
        public string TransitoryStatus { get; set; }
        public string DownloadStatus { get; set; } 
        public string ApprovalStatus { get; set; }
        public Nullable<Decimal> WHTBaseAmount { get; set; } 
        public string WHTBaseAmountOperator { get; set; }
        public Nullable<Decimal> WHTAmount { get; set; } 
        public string WHTAmountOperator { get; set; }
        public string BatchFileName { get; set; }
        public int page { get; set; }
        public int size { get; set; }
        public string NomorBuktiPotong { get; set; }
        public string NomorInvoice { get; set; }
        public string NomorPV { get; set; }
        public string PDFWithHolding { get; set; }
        public Nullable<Decimal> Tarif { get; set; }
        public string TarifOperator { get; set; }
        public string StatusInvoice { get; set; }
        public string TaxArticle { get; set; }
        public string JenisPajak { get; set; }
        public string TaxAdditionalInfo { get; set; }
        public string SPTType { get; set; }
        public string NomorDocSAP { get; set; }
        public string PostingDateTo { get; set; }
        public string TahunPajak { get; set; }
        public string MasaPajak { get; set; }
        public string TanggalBuktiPotongFrom { get; set; }
        public string TanggalBuktiPotongTo { get; set; }
        public string TanggalPenerimaanfrom { get; set; }
        public string TanggalPenerimaanto { get; set; }
        public string RecordTime { get; set; }
        public string RelatedGLAccount { get; set; }
        public string PostingDateFrom { get; set; }
        public string NomorFakturPajak { get; set; }
        public string PVCreatedBy { get; set; }
        public string Description { get; set; }
        public string ApprovalDateFrom { get; set; }
        public string ApprovalDateTo { get; set; }
        public string ApprovalDesc { get; set; }
        public string ApprovalRecordDateFrom { get; set; }
        public string ApprovalRecordDateTo { get; set; }
        public string InvoiceDateFrom { get; set; }
        public string InvoiceDateTo { get; set;}
        public string eBupotNumber { get; set; }
        public string eBupotReferenceNo { get; set; }
        public string MapCode { get; set; }
        //ADD AD
        public string EFakturStatus { get; set; }
        //END
        public dynamic MapFromModel(bool isFullAccess, string DivisionName = "", string Username = "", int SortBy = 0, string SortDirection = "ASC", int FromNumber = 1, int ToNumber = 10, string Id = "")
        {
            return new
            {
                
                SortBy = SortBy,
                SortDirection = SortDirection,
                TanggalFileDiterimaFrom = this.TanggalFileDiterimaFrom.FormatDefaultDateWhenNullOrEmpty(),
                TanggalFileDiterimaTo = this.TanggalFileDiterimaTo.FormatDefaultDateWhenNullOrEmpty("TO"),
                WaktuFileDiterimaFrom = this.WaktuFileDiterimaFrom.FormatDefaultTimeWhenNullOrEmpty(),
                WaktuFileDiterimaTo = this.WaktuFileDiterimaTo.FormatDefaultTimeWhenNullOrEmpty("TO"),
                TanggalDownloadFrom = this.TanggalDownloadFrom.FormatDefaultDateWhenNullOrEmpty(),
                TanggalDownloadTo = this.TanggalDownloadTo.FormatDefaultDateWhenNullOrEmpty("TO"),
                ApprovalTimeFrom = this.ApprovalTimeFrom.FormatDefaultDateWhenNullOrEmpty(),
                ApprovalTimeTo = this.ApprovalTimeTo.FormatDefaultDateWhenNullOrEmpty("TO"),
                FromNumber = FromNumber,
                ToNumber =  ToNumber,
                Perekam = this.Perekam,
                DivisionName = DivisionName,
                Username = Username,
                FakturType = this.FakturType,
                NomorInvoice = this.NomorInvoice,
                NomorPV = this.NomorPV,
                PDFWithHolding = this.PDFWithHolding,
                NomorBuktiPotong = this.NomorBuktiPotong,
                NPWPPenjual = this.NPWPPenjual,
                NamaPenjual = this.NamaPenjual,
                NamaPenjualOriginal = this.NamaPenjualOriginal,
                WHTBaseAmount = this.WHTBaseAmount,
                WHTBaseAmountOperator = this.WHTBaseAmountOperator,
                WHTAmount = this.WHTAmount,
                WHTAmountOperator = this.WHTAmountOperator,
                Tarif = this.Tarif,
                TarifOperator = this.TarifOperator,
                StatusInvoice = this.StatusInvoice,
                TaxArticle = this.TaxArticle,
                JenisPajak = this.JenisPajak,
                TaxAdditionalInfo = this.TaxAdditionalInfo,
                SPTType = this.SPTType,
                NomorDocSAP = this.NomorDocSAP,
                PostingDateFrom = this.PostingDateFrom.FormatDefaultDateWhenNullOrEmpty(),
                PostingDateTo = this.PostingDateTo.FormatDefaultDateWhenNullOrEmpty("TO"),
                TanggalBuktiPotongFrom = this.TanggalBuktiPotongFrom.FormatDefaultDateWhenNullOrEmpty(),
                TanggalBuktiPotongTo = this.TanggalBuktiPotongTo.FormatDefaultDateWhenNullOrEmpty("TO"),
                TahunPajak = this.TahunPajak,
                MasaPajak = this.MasaPajak,
                TanggalPenerimaanfrom = this.TanggalPenerimaanfrom.FormatDefaultDateWhenNullOrEmpty(),
                TanggalPenerimaanto = this.TanggalPenerimaanto.FormatDefaultDateWhenNullOrEmpty("TO"),
                DownloadStatus = this.DownloadStatus,
                RelatedGLAccount = this.RelatedGLAccount,
                NomorFakturPajak = this.NomorFakturPajak,
                BatchFileName = this.BatchFileName,
                RecordStatus = this.RecordStatus,
                PVCreatedBy = this.PVCreatedBy,
                Description = this.Description,
                ApprovalStatus = this.ApprovalStatus,
                ApprovalDateFrom = this.ApprovalDateFrom.FormatDefaultDateWhenNullOrEmpty(),
                ApprovalDateTo = this.ApprovalDateTo.FormatDefaultDateWhenNullOrEmpty("TO"),
                ApprovalDesc = this.ApprovalDesc,
                RecordTimeFrom = this.RecordTimeFrom.FormatDefaultDateWhenNullOrEmpty(),
                RecordTimeTo = this.RecordTimeTo.FormatDefaultDateWhenNullOrEmpty("TO"),
                InvoiceDateFrom = this.InvoiceDateFrom.FormatDefaultDateWhenNullOrEmpty(),
                InvoiceDateTo = this.InvoiceDateTo.FormatDefaultDateWhenNullOrEmpty("TO"),
                eBupotNumber= this.eBupotNumber,
                eBupotReferenceNo = this.eBupotReferenceNo,
                MapCode = this.MapCode,
                isFullAccess = isFullAccess,
                EFakturStatus = this.EFakturStatus,
                Id = Id
            };
        }
    }
}