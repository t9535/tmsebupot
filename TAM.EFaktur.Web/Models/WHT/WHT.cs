﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.WHT
{
    public class WHT : BaseModel
    {
        public String TaxArticle { get; set; }
        public String NomorBuktiPotong { get; set; }
        public String NomorInvoice { get; set; }
        public String StatusInvoice { get; set; }
        public String SupplierName { get; set; }
        public String SupplierAddress { get; set; }
        public String SupplierNPWP { get; set; }
        public Decimal WHTAmount { get; set; }
        public Decimal WHTTarif { get; set; }
        public Decimal WHTTaxAmount { get; set; }
        public String Description { get; set; }
        public String TaxType { get; set; }
        public String SPTType { get; set; }
        public DateTime WithholdingTaxDate { get; set; }
        public String TaxPeriodMonth { get; set; }
        public String TaxPeriodYear { get; set; }
        public Nullable<DateTime> ReceiveFileDate { get; set; }
        public String PVNumber { get; set; }
        public string PVCreatedBy { get; set; }
        public string SAPDocNumber { get; set; }
        public string GLAccount { get; set; }
        public String PDFStatus { get; set; }
        public string AdditionalInfo { get; set; }
        public Nullable<DateTime> PostingDate { get; set; }
        public String OriginalSupplierName { get; set; }
        public String ApprovalStatus { get; set; }
        public String ApprovalDesc { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string Composite { get; set; }

        public dynamic MapFromModel(DateTime? EventDate = null, string EventActor = "")
        {
            dynamic args = new {
                Id = this.Id,
                TaxArticle = this.TaxArticle,
                NomorBuktiPotong = this.NomorBuktiPotong,
                NomorInvoice = this.NomorInvoice,
                StatusInvoice = this.StatusInvoice,
                SupplierName = this.SupplierName,
                SupplierNPWP = this.SupplierNPWP,
                WHTAmount = this.WHTAmount,
                WHTTarif = this.WHTTarif,
                WHTTaxAmount = this.WHTTaxAmount,
                Description = this.Description,
                TaxType = this.TaxType,
                SPTType = this.SPTType,
                WithholdingTaxDate = this.WithholdingTaxDate.FormatSQLDate(),
                TaxPeriodMonth = this.TaxPeriodMonth,
                TaxPeriodYear = this.TaxPeriodYear,
                ReceiveFileDate = this.ReceiveFileDate,
                PVNumber = this.PVNumber,
                PVCreatedBy = this.PVCreatedBy,
                SAPDocNumber = this.SAPDocNumber,
                PostingDate = this.PostingDate,
                GLAccount = this.GLAccount,
                AdditionalInfo = this.AdditionalInfo,
                CreatedOn = this.CreatedOn.Value.FormatSQLDateTime(),
                CreatedBy = this.CreatedBy,
                RowStatus = false,
                EventDate = EventDate.HasValue ? EventDate.Value : DateTime.Now,
                EventActor = EventActor,
                OriginalSupplierName = this.OriginalSupplierName,
                ApprovalStatus = this.ApprovalStatus,
                ApprovalDesc = this.ApprovalDesc,
                InvoiceDate = this.InvoiceDate.FormatSQLDate(),
                SupplierAddress = this.SupplierAddress,
            };
            return args;
        }
    }
}