﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TAM.EFaktur.Web.Models.Category_Nominative;

namespace TAM.EFaktur.Web.Models.Promotion
{
    public class Promotion
    {
        public Guid? PROMOTION_ID { get; set; }
        public Guid? DASHBOARD_ID { get; set; }

        public string CATEGORY_CODE { get; set; }

        public int? THN_PAJAK { get; set; }

        public string TMP_PENANDATANGAN { get; set; }

        public DateTime? TGL_PENANDATANGAN { get; set; }

        public string NAMA_PENANDATANGAN { get; set; }

        public string JABATAN_PENANDATANGAN { get; set; }

        public int? NO { get; set; }

        public string NAMA { get; set; }

        public string NPWP { get; set; }

        public string ALAMAT { get; set; }

        public DateTime? CREATED_DT { get; set; }

        public string CREATED_BY { get; set; }

        public DateTime? CHANGED_DT { get; set; }

        public string CHANGED_BY { get; set; }


        public dynamic MapFromModel(DateTime? EventDate = null, string EventActor = "")
        {
            dynamic args = new
            {
                PROMOTION_ID = this.PROMOTION_ID,
                DASHBOARD_ID = this.DASHBOARD_ID,
                //CATEGORY_CODE = this.CATEGORY_CODE,
                THN_PAJAK = this.THN_PAJAK,
                TMP_PENANDATANGAN = this.TMP_PENANDATANGAN,
                TGL_PENANDATANGAN = this.TGL_PENANDATANGAN,
                NAMA_PENANDATANGAN = this.NAMA_PENANDATANGAN,
                JABATAN_PENANDATANGAN = this.JABATAN_PENANDATANGAN,
                NO = this.NO,
                NAMA = this.NAMA,
                NPWP = this.NPWP,
                ALAMAT = this.ALAMAT,
                CREATED_DT = (EventDate.HasValue ? EventDate.Value : DateTime.Now).FormatSQLDateTime(),
                CREATED_BY = EventActor,
                CHANGED_DT = (EventDate.HasValue ? EventDate.Value : DateTime.Now).FormatSQLDateTime(),
                CHANGED_BY = EventActor
            };
            return args;
        }
        public dynamic MapFromModelHeader(DateTime? EventDate = null, string EventActor = "")
        {
            dynamic args = new
            {
                DASHBOARD_ID = this.DASHBOARD_ID,
                THN_PAJAK = this.THN_PAJAK,
                TMP_PENANDATANGAN = this.TMP_PENANDATANGAN,
                TGL_PENANDATANGAN = this.TGL_PENANDATANGAN,
                NAMA_PENANDATANGAN = this.NAMA_PENANDATANGAN,
                JABATAN_PENANDATANGAN = this.JABATAN_PENANDATANGAN,
                CREATED_DT = (EventDate.HasValue ? EventDate.Value : DateTime.Now).FormatSQLDateTime(),
                CREATED_BY = EventActor,
                CHANGED_DT = (EventDate.HasValue ? EventDate.Value : DateTime.Now).FormatSQLDateTime(),
                CHANGED_BY = EventActor
            };
            return args;
        }

        public bool? PositionSignature { get; set; }
    }

    public class PromotionDetail
    {
        public Guid? PROMOTION_DETAIL_ID { get; set; }
        public Guid? PROMOTION_ID { get; set; }

        public DateTime? TANGGAL { get; set; }

        public string BENTUK_DAN_JENIS_BIAYA { get; set; }
        public decimal? JUMLAH { get; set; }
        public decimal? JUMLAH_GROSS_UP { get; set; }

        public string KETERANGAN { get; set; }
        public decimal? JUMLAH_PPH { get; set; }

        public string JENIS_PPH { get; set; }
        public decimal? JUMLAH_NET { get; set; }

        public string NOMOR_REKENING { get; set; }

        public string NAMA_REKENING_PENERIMA { get; set; }

        public string NAMA_BANK { get; set; }

        public string NO_KTP { get; set; }

        public DateTime? CREATED_DT { get; set; }

        public string CREATED_BY { get; set; }

        public DateTime? CHANGED_DT { get; set; }

        public string CHANGED_BY { get; set; }

        public Promotion Promotion { get; set; }

        public dynamic MapFromModel(DateTime? EventDate = null, string EventActor = "")
        {
            dynamic args = new
            {
                //Header
                PROMOTION_ID = this.Promotion.PROMOTION_ID,
                DASHBOARD_ID = this.Promotion.DASHBOARD_ID,
                //CATEGORY_CODE = this.Promotion.CATEGORY_CODE,
                THN_PAJAK = this.Promotion.THN_PAJAK,
                TMP_PENANDATANGAN = this.Promotion.TMP_PENANDATANGAN,
                TGL_PENANDATANGAN = this.Promotion.TGL_PENANDATANGAN,
                NAMA_PENANDATANGAN = this.Promotion.NAMA_PENANDATANGAN,
                JABATAN_PENANDATANGAN = this.Promotion.JABATAN_PENANDATANGAN,
                NO = this.Promotion.NO,
                NAMA = this.Promotion.NAMA,
                NPWP = this.Promotion.NPWP,
                ALAMAT = this.Promotion.ALAMAT,

                //Detail
                PROMOTION_DETAIL_ID = this.PROMOTION_DETAIL_ID,
                TANGGAL = this.TANGGAL,
                BENTUK_DAN_JENIS_BIAYA = this.BENTUK_DAN_JENIS_BIAYA,
                JUMLAH = this.JUMLAH,
                JUMLAH_GROSS_UP = this.JUMLAH_GROSS_UP,
                KETERANGAN = this.KETERANGAN,
                JUMLAH_PPH = this.JUMLAH_PPH,
                JENIS_PPH = this.JENIS_PPH,
                JUMLAH_NET = this.JUMLAH_NET,
                NOMOR_REKENING = this.NOMOR_REKENING,
                NAMA_REKENING_PENERIMA = this.NAMA_REKENING_PENERIMA,
                NAMA_BANK = this.NAMA_BANK,
                NO_KTP = this.NO_KTP,

                CREATED_DT = (EventDate.HasValue ? EventDate.Value : DateTime.Now).FormatSQLDateTime(),
                CREATED_BY = EventActor,
                CHANGED_DT = (EventDate.HasValue ? EventDate.Value : DateTime.Now).FormatSQLDateTime(),
                CHANGED_BY = EventActor

            };
            return args;
        }
    }

    public class PromotionValidation
    {
        public bool NO { get; set; }
        public bool NAMA { get; set; }
        public bool NPWP { get; set; }
        public bool ALAMAT { get; set; }
        public bool TANGGAL { get; set; }
        public bool BENTUK_DAN_JENIS_BIAYA { get; set; }
        public bool JUMLAH { get; set; }
        public bool JUMLAH_GROSS_UP { get; set; }
        public bool KETERANGAN { get; set; }
        public bool JUMLAH_PPH { get; set; }
        public bool JENIS_PPH { get; set; }
        public bool JUMLAH_NET { get; set; }
        public bool NOMOR_REKENING { get; set; }
        public bool NAMA_REKENING_PENERIMA { get; set; }
        public bool NAMA_BANK { get; set; }
        public bool NO_KTP { get; set; }
    }
}
