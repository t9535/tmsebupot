﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TAM.EFaktur.Web.Models.Log_Sync;
using TAM.EFaktur.Web.Models.Master_Config;
using System.Text.RegularExpressions;
using NPOI.SS.Util;
using TAM.EFaktur.Web.Models.Dashboard_Nominative;
using TAM.EFaktur.Web.Models.Master_DataFieldEnum;
using Microsoft.Reporting.WebForms;

namespace TAM.EFaktur.Web.Models.Promotion
{
    public class PromotionRepository : BaseRepository
    {
        #region Singleton
        private PromotionRepository() { }
        private static PromotionRepository instance = null;
        public static PromotionRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new PromotionRepository();
                }
                return instance;
            }
        }
        #endregion

        #region Counter Data
        public int Count(Guid DashboardId)
        {
            dynamic args = new
            {
                DashboardId = DashboardId
            };
            return db.SingleOrDefault<int>("Promotion/usp_CountPromotion", args);
        }

        public int CountDetail(Guid? PromotionId)
        {
            dynamic args = new
            {
                PromotionId = PromotionId
            };
            return db.SingleOrDefault<int>("Promotion/usp_CountPromotionDetail", args);
        }
        #endregion

        #region Processing Data GetList for Search
        //list data for search
        public List<Promotion> GetList(Guid DashboardId, int SortBy, string SortDirection, int FromNumber, int ToNumber)
        {
            dynamic args = new
            {
                DashboardId = DashboardId,
                SortBy = SortBy,
                SortDirection = SortDirection,
                FromNumber = FromNumber,
                ToNumber = ToNumber
            };
            IEnumerable<Promotion> result = db.Query<Promotion>("Promotion/usp_GetPromotion", args);
            return result.ToList();
        }

        public List<PromotionDetail> GetDetailList(Guid? PromotionId, int SortBy, string SortDirection, int FromNumber, int ToNumber)
        {
            dynamic args = new
            {
                PromotionId = PromotionId,
                SortBy = SortBy,
                SortDirection = SortDirection,
                FromNumber = FromNumber,
                ToNumber = ToNumber
            };
            IEnumerable<PromotionDetail> result = db.Query<PromotionDetail>("Promotion/usp_GetPromotionDetail", args);
            return result.ToList();
        }

        public Promotion GetById(Guid Id)
        {
            return db.SingleOrDefault<Promotion>("Promotion/usp_GetPromotionByID", new { ID = Id });
        }

        public PromotionDetail GetDetailById(Guid? Id)
        {
            return db.SingleOrDefault<PromotionDetail>("Promotion/usp_GetPromotionDetailByID", new { ID = Id });
        }

        public Promotion GetByDashboardId(Guid DashboardId)
        {
            return db.SingleOrDefault<Promotion>("Promotion/usp_GetPromotionByDashboardID", new { DashboardId = DashboardId });
        }

        public PromotionValidation GetValidationDataFillByDashboardId(Guid DashboardId)
        {
            return db.SingleOrDefault<PromotionValidation>("Dashboard_Nominative/usp_GetValidationDataFillByDashboardID", new { DashboardId = DashboardId });
        }

        public Result Insert(Promotion model, string EventActor)
        {
            try
            {
                db.Execute("Promotion/usp_InsertOrUpdate_TB_R_Promotion", model.MapFromModel(DateTime.Now, EventActor));
                return MsgResultSuccessInsert();
            }
            catch (Exception e)
            {
                return MsgResultError(e.LogToApp("Create Promotion", MessageType.ERR, EventActor).Message);
            }
        }

        public Result Update(Promotion model, string EventActor)
        {
            try
            {
                db.Execute("Promotion/usp_InsertOrUpdate_TB_R_Promotion", model.MapFromModel(DateTime.Now, EventActor));
                return MsgResultSuccessUpdate();
            }
            catch (Exception e)
            {
                return MsgResultError(e.LogToApp("Update Promotion", MessageType.ERR, EventActor).Message);
            }
        }

        public Result Delete(Promotion model, string EventActor)
        {
            try
            {
                db.Execute("Promotion/usp_DeletePromotionByID", new { ID = model.PROMOTION_ID });
                return MsgResultSuccessDelete();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                return MsgResultError(e.LogToApp("Delete Promotion", MessageType.ERR, EventActor).Message);
            }
        }

        public Result SubmitHeader(Promotion model, string EventActor, string path, string pathRDLC)
        {
            try
            {
                db.Execute("Promotion/usp_SubmitHeader_TB_R_Promotion", model.MapFromModelHeader(DateTime.Now, EventActor));
                SaveFile(path, pathRDLC, model.DASHBOARD_ID);
                return MsgResultSuccessUpdate();
            }
            catch (Exception e)
            {
                return MsgResultError(e.Message.ToString());
            }
        }


        public Result InsertDetail(PromotionDetail model, string EventActor)
        {
            try
            {
                Result result = db.SingleOrDefault<Result>("Promotion/usp_InsertOrUpdate_TB_R_Promotion_Detail", model.MapFromModel(DateTime.Now, EventActor));
                if (result.ResultCode == true) { result.ResultDesc = msgSuccessInsert; }
                return result;
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                return MsgResultError(e.LogToApp("Create Promotion Detail", MessageType.ERR, EventActor).Message);
            }
        }

        public Result UpdateDetail(PromotionDetail model, string EventActor)
        {
            try
            {
                db.Execute("Promotion/usp_InsertOrUpdate_TB_R_Promotion_Detail", model.MapFromModel(DateTime.Now, EventActor));
                return MsgResultSuccessUpdate();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                return MsgResultError(e.LogToApp("Update Promotion Detail", MessageType.ERR, EventActor).Message);
            }
        }

        public Result DeleteDetail(PromotionDetail model, string EventActor)
        {
            try
            {
                db.Execute("Promotion/usp_DeletePromotionDetailByID", new { ID = model.PROMOTION_DETAIL_ID });
                return MsgResultSuccessDelete();
            }
            catch (Exception e)
            {
                return MsgResultError(e.LogToApp("Delete Promotion Detail", MessageType.ERR, EventActor).Message);
            }
        }

        public Result ExcelPromotionInsert(string path, string EventActor, Promotion data)
        {
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = "File has been uploaded successfully"
            };

            FileStream file = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite);

            HSSFWorkbook hssfwb = new HSSFWorkbook(file);
            ISheet sheet = hssfwb.GetSheet("Default");

            string pipedString = string.Empty;

            int row = 0;
            int rowFail = 0;
            int rowSuccess = 0;

            try
            {
                file.Close();
                var model = new Promotion();
                var validationDataFill = GetValidationDataFillByDashboardId((Guid)data.DASHBOARD_ID);
                var cellNum = 16;////jumlah header cell +1;
                var rowNum = 6;
                List<int> exceptionCellNum = new List<int>();
                //exceptionCellNum.Add(17); 

                //var config = MasterConfigRepository.Instance.GetByConfigKey("UrlTemplatePromotion");
                //if (config == null)
                //{
                //    throw new Exception("Please define the UrlTemplateNominativeData Config");
                //}

                if (sheet.GetRow(rowNum - 1) == null)
                {
                    result.ResultCode = false;
                    result.ResultDesc = "Invalid template promotion.";
                    return result;
                }

                if (sheet.GetRow(rowNum - 1).LastCellNum != cellNum)
                {
                    result.ResultCode = false;
                    result.ResultDesc = "Invalid template promotion.";
                    return result;
                }


                List<GroupingExcel> groups = new List<GroupingExcel>();
                int mergedRegions = sheet.NumMergedRegions;
                for (int regions = 0; regions < mergedRegions; regions++)
                {
                    CellRangeAddress mergedRegionIndex = sheet.GetMergedRegion(regions);
                    if (mergedRegionIndex.FirstRow >= rowNum)
                    {
                        groups.Add(new GroupingExcel { FirstRow = mergedRegionIndex.FirstRow, LastRow = mergedRegionIndex.LastRow, FirstColumn = mergedRegionIndex.FirstColumn, LastColumn = mergedRegionIndex.LastColumn });
                    }
                }


                List<LogSyncCreateUpdate> logSyncList = new List<LogSyncCreateUpdate>();
                List<string> failTaxNumber = new List<string>();

                for (row = rowNum; row <= sheet.LastRowNum; row++)
                {
                    #region Set Cell Policy, Cell Type and Save Sync Data
                    pipedString = string.Empty;
                    for (int i = 0; i < cellNum; i++)
                    {
                        sheet.GetRow(row).GetCell(i, MissingCellPolicy.CREATE_NULL_AS_BLANK);
                        sheet.GetRow(row).GetCell(i).SetCellType(CellType.STRING);

                        pipedString += NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(i)) + "|";
                    }
                    pipedString = pipedString.Remove(pipedString.Length - 1);
                    #endregion

                    try
                    {
                        #region Cell Mapping & Insert to DB 
                        db.BeginTransaction();

                        string validation = "", NPWPFormat = null;
                        DateTime? TANGGAL = null;
                        decimal? JUMLAH = null, JUMLAH_GROSS_UP = null, JUMLAH_PPH = null, JUMLAH_NET = null;
                        int? NO = null;
                        //bool IsMergedCell = sheet.GetRow(row).GetCell(0).IsMergedCell;
                        bool IsMergedCell = groups.Where(x => (row >= x.FirstRow && row <= x.LastRow) && (x.FirstColumn >= 0 && x.LastColumn < 4)).Count() > 0 ? true : false;
                        bool group = groups.Where(x => x.FirstRow == row && x.FirstColumn >= 0 && x.LastColumn < 4).Count() > 0 ? true : false;

                        //Header 
                        var NO_STRING = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(0));
                        var NAMA = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(1));
                        var NPWP = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(2));
                        var ALAMAT = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(3));

                        //Details
                        var TANGGAL_STRING = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(4));
                        var BENTUK_DAN_JENIS_BIAYA = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(5));
                        var JUMLAH_STRING = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(6));
                        var JUMLAH_GROSS_UP_STRING = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(7));
                        var KETERANGAN = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(8));
                        var JUMLAH_PPH_STRING = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(9));
                        var JENIS_PPH = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(10));
                        var JUMLAH_NET_STRING = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(11));
                        var NOMOR_REKENING = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(12));
                        var NAMA_REKENING_PENERIMA = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(13));
                        var NAMA_BANK = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(14));
                        var NO_KTP = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(15));

                        #region Validation
                        //jika ada space di semua kolom maka tidak di proses
                        if (string.IsNullOrEmpty(NO_STRING) && string.IsNullOrEmpty(NAMA) && string.IsNullOrEmpty(NPWP) && string.IsNullOrEmpty(ALAMAT) &&
                            string.IsNullOrEmpty(TANGGAL_STRING) && string.IsNullOrEmpty(BENTUK_DAN_JENIS_BIAYA) && string.IsNullOrEmpty(JUMLAH_STRING) && string.IsNullOrEmpty(JUMLAH_GROSS_UP_STRING) &&
                            string.IsNullOrEmpty(KETERANGAN) && string.IsNullOrEmpty(JUMLAH_PPH_STRING) && string.IsNullOrEmpty(JENIS_PPH) && string.IsNullOrEmpty(JUMLAH_NET_STRING) &&
                            string.IsNullOrEmpty(NOMOR_REKENING) && string.IsNullOrEmpty(NAMA_REKENING_PENERIMA) && string.IsNullOrEmpty(NAMA_BANK) && string.IsNullOrEmpty(NO_KTP)
                            )
                        {
                            break;
                        }

                        var header = GetHeaderPromotion();
                        //Nullale
                        //Header
                        if ((IsMergedCell == true && group == true) || IsMergedCell == false)
                        {
                            if (validationDataFill.NO) { if (string.IsNullOrEmpty(NO_STRING)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.NO} can't be empy"; } }
                            if (validationDataFill.NAMA) { if (string.IsNullOrEmpty(NAMA)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.NAMA} can't be empy"; } }
                            if (validationDataFill.NPWP) { if (string.IsNullOrEmpty(NPWP)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.NPWP} can't be empy"; } }
                            if (validationDataFill.ALAMAT) { if (string.IsNullOrEmpty(ALAMAT)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.ALAMAT} can't be empy"; } }
                        }

                        //Detail
                        if (validationDataFill.TANGGAL) { if (string.IsNullOrEmpty(TANGGAL_STRING)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.TANGGAL} can't be empy"; } }
                        if (validationDataFill.BENTUK_DAN_JENIS_BIAYA) { if (string.IsNullOrEmpty(BENTUK_DAN_JENIS_BIAYA)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.BENTUK_DAN_JENIS_BIAYA} can't be empy"; } }
                        if (validationDataFill.JUMLAH) { if (string.IsNullOrEmpty(JUMLAH_STRING)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.JUMLAH} can't be empy"; } }
                        if (validationDataFill.JUMLAH_GROSS_UP) { if (string.IsNullOrEmpty(JUMLAH_GROSS_UP_STRING)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.JUMLAH_GROSS_UP} can't be empy"; } }
                        if (validationDataFill.KETERANGAN) { if (string.IsNullOrEmpty(KETERANGAN)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.KETERANGAN} can't be empy"; } }
                        if (validationDataFill.JUMLAH_PPH) { if (string.IsNullOrEmpty(JUMLAH_PPH_STRING)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.JUMLAH_PPH} can't be empy"; } }
                        if (validationDataFill.JENIS_PPH) { if (string.IsNullOrEmpty(JENIS_PPH)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.JENIS_PPH} can't be empy"; } }
                        if (validationDataFill.JUMLAH_NET) { if (string.IsNullOrEmpty(JUMLAH_NET_STRING)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.JUMLAH_NET} can't be empy"; } }
                        if (validationDataFill.NOMOR_REKENING) { if (string.IsNullOrEmpty(NOMOR_REKENING)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.NOMOR_REKENING} can't be empy"; } }
                        if (validationDataFill.NAMA_REKENING_PENERIMA) { if (string.IsNullOrEmpty(NAMA_REKENING_PENERIMA)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.NAMA_REKENING_PENERIMA} can't be empy"; } }
                        if (validationDataFill.NAMA_BANK) { if (string.IsNullOrEmpty(NAMA_BANK)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.NAMA_BANK} can't be empy"; } }
                        if (validationDataFill.NO_KTP) { if (string.IsNullOrEmpty(NO_KTP)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.NO_KTP} can't be empy"; } }

                        //must be less than or equal to
                        //Header
                        if ((IsMergedCell == true && group == true) || IsMergedCell == false)
                        {
                            if (!string.IsNullOrEmpty(NO_STRING)) { if (NO_STRING.Length >= 8) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.NO} must be less than or equal to 8 character"; } }
                            if (!string.IsNullOrEmpty(NAMA)) { if (NAMA.Length >= 100) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.NAMA} must be less than or equal to 100 character"; } }
                            if (!string.IsNullOrEmpty(NPWP)) { if (NPWP.Length > 15 || NPWP.Length < 15) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.NPWP} must be equal to 15 character"; } }
                            if (!string.IsNullOrEmpty(ALAMAT)) { if (ALAMAT.Length >= 200) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.ALAMAT} must be less than or equal to 200 character"; } }
                        }


                        //if (!string.IsNullOrEmpty(TANGGAL_STRING)) { if (TANGGAL_STRING.Length >= 10) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Tanggal must be less than or equal to 10 character"; } }
                        if (!string.IsNullOrEmpty(BENTUK_DAN_JENIS_BIAYA)) { if (BENTUK_DAN_JENIS_BIAYA.Length >= 50) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.BENTUK_DAN_JENIS_BIAYA} must be less than or equal to 50 character"; } }
                        //if (!string.IsNullOrEmpty(JUMLAH_STRING)) { if (JUMLAH_STRING.Length >= 18) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Jumlah must be less than or equal to 18 character"; } }
                        //if (!string.IsNullOrEmpty(JUMLAH_GROSS_UP_STRING)) { if (JUMLAH_GROSS_UP_STRING.Length >= 18) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Jumlah GrossUp must be less than or equal to 18 character"; } }
                        if (!string.IsNullOrEmpty(KETERANGAN)) { if (KETERANGAN.Length >= 300) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.KETERANGAN} must be less than or equal to 300 character"; } }
                        //if (!string.IsNullOrEmpty(JUMLAH_PPH_STRING)) { if (JUMLAH_PPH_STRING.Length >= 18) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Jumlah PPh must be less than or equal to 18 character"; } }
                        if (!string.IsNullOrEmpty(JENIS_PPH)) { if (JENIS_PPH.Length >= 50) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.JENIS_PPH} must be less than or equal to 50 character"; } }
                        //if (!string.IsNullOrEmpty(JUMLAH_NET_STRING)) { if (JUMLAH_NET_STRING.Length >= 18) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Jumlah Net must be less than or equal to 18 character"; } }
                        if (!string.IsNullOrEmpty(NOMOR_REKENING)) { if (NOMOR_REKENING.Length >= 20) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.NOMOR_REKENING} must be less than or equal to 20 character"; } }
                        if (!string.IsNullOrEmpty(NAMA_REKENING_PENERIMA)) { if (NAMA_REKENING_PENERIMA.Length >= 100) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.NAMA_REKENING_PENERIMA} must be less than or equal to 100 character"; } }
                        if (!string.IsNullOrEmpty(NAMA_BANK)) { if (NAMA_BANK.Length >= 100) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.NAMA_BANK} must be less than or equal to 100 character"; } }
                        if (!string.IsNullOrEmpty(NO_KTP)) { if (NO_KTP.Length > 16 || NO_KTP.Length < 16) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.NO_KTP} must be equal to 16 character"; } }

                        if (!string.IsNullOrEmpty(NPWP))
                        {
                            if (!decimal.TryParse(NPWP, out decimal NPWPNUMBER)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.NPWP} Valid format numeric type"; }
                            if (NPWP.Length == 15)
                            {
                                NPWPFormat = $"{NPWP.Substring(0, 2)}.{NPWP.Substring(2, 3)}.{NPWP.Substring(5, 3)}.{NPWP.Substring(8, 1)}-{NPWP.Substring(9, 3)}.{NPWP.Substring(12, 3)}";
                            }
                        }

                        if (!string.IsNullOrEmpty(TANGGAL_STRING))
                        {
                            double TANGGAL_DOUBLE;
                            if (!double.TryParse(TANGGAL_STRING, out TANGGAL_DOUBLE))
                            {
                                DateTime TANGGAL1;
                                if (!DateTime.TryParseExact(TANGGAL_STRING, NPOIExcel.FormatDate, CultureInfo.InvariantCulture, DateTimeStyles.None, out TANGGAL1))
                                {
                                    if (!string.IsNullOrEmpty(validation)) validation += ", ";
                                    validation += $"{header.TANGGAL} format must be DD/MM/YYYY";
                                }
                                else
                                {
                                    TANGGAL = TANGGAL1;
                                }

                            }
                            else
                            {
                                TANGGAL = DateTime.FromOADate(TANGGAL_DOUBLE);
                            }
                        }

                        if (!string.IsNullOrEmpty(JUMLAH_STRING))
                        {
                            Decimal JUMLAH1;
                            if (!decimal.TryParse(JUMLAH_STRING, out JUMLAH1)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.JUMLAH} Valid format numeric type"; } else { JUMLAH = JUMLAH1; }
                        }

                        if (!string.IsNullOrEmpty(JUMLAH_GROSS_UP_STRING))
                        {
                            Decimal JUMLAH_GROSS_UP1;
                            if (!decimal.TryParse(JUMLAH_GROSS_UP_STRING, out JUMLAH_GROSS_UP1)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.JUMLAH_GROSS_UP} Valid format numeric type"; } else { JUMLAH_GROSS_UP = JUMLAH_GROSS_UP1; }
                        }

                        if (!string.IsNullOrEmpty(JUMLAH_PPH_STRING))
                        {
                            Decimal JUMLAH_PPH1;
                            if (!decimal.TryParse(JUMLAH_PPH_STRING, out JUMLAH_PPH1)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.JUMLAH_PPH} Valid format numeric type"; } else { JUMLAH_PPH = JUMLAH_PPH1; }
                        }

                        if (!string.IsNullOrEmpty(JUMLAH_NET_STRING))
                        {
                            Decimal JUMLAH_NET1;
                            if (!decimal.TryParse(JUMLAH_NET_STRING, out JUMLAH_NET1)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.JUMLAH_NET} Valid format numeric type"; } else { JUMLAH_NET = JUMLAH_NET1; }
                        }

                        if (!string.IsNullOrEmpty(NO_STRING))
                        {
                            Int32 NO1;
                            if (!int.TryParse(NO_STRING, out NO1)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.NO} Valid format numeric type"; } else { NO = NO1; }
                        }

                        if (!string.IsNullOrEmpty(NO_KTP))
                        {
                            if (!long.TryParse(NO_KTP, out long NO_KTP1)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.NO_KTP} Valid format numeric type"; }
                        }

                        if (!string.IsNullOrEmpty(NOMOR_REKENING))
                        {
                            if (!long.TryParse(NOMOR_REKENING, out long NO1)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.NOMOR_REKENING} Valid format numeric type"; }
                        }

                        if (!string.IsNullOrEmpty(validation))
                        {
                            throw new Exception(validation);
                        }
                        #endregion

                        if ((IsMergedCell == true && group == true) || IsMergedCell == false)
                        {
                            model = new Promotion()
                            {
                                DASHBOARD_ID = data.DASHBOARD_ID,
                                THN_PAJAK = data.THN_PAJAK,
                                NAMA_PENANDATANGAN = data.NAMA_PENANDATANGAN,
                                TMP_PENANDATANGAN = data.TMP_PENANDATANGAN,
                                TGL_PENANDATANGAN = data.TGL_PENANDATANGAN,
                                JABATAN_PENANDATANGAN = data.JABATAN_PENANDATANGAN,
                                NO = NO,
                                NAMA = NAMA,
                                NPWP = NPWPFormat,
                                ALAMAT = ALAMAT
                            };
                        }

                        var modelDetail = new PromotionDetail()
                        {
                            TANGGAL = TANGGAL,
                            BENTUK_DAN_JENIS_BIAYA = BENTUK_DAN_JENIS_BIAYA,
                            JUMLAH = JUMLAH,
                            JUMLAH_GROSS_UP = JUMLAH_GROSS_UP,
                            KETERANGAN = KETERANGAN,
                            JUMLAH_PPH = JUMLAH_PPH,
                            JENIS_PPH = JENIS_PPH,
                            JUMLAH_NET = JUMLAH_NET,
                            NOMOR_REKENING = NOMOR_REKENING,
                            NAMA_REKENING_PENERIMA = NAMA_REKENING_PENERIMA,
                            NAMA_BANK = NAMA_BANK,
                            NO_KTP = NO_KTP,
                            Promotion = model
                        };

                        // Save
                        result = db.SingleOrDefault<Result>("Promotion/usp_InsertOrUpdate_TB_R_Promotion_Detail", modelDetail.MapFromModel(DateTime.Now.AddSeconds(row), EventActor));

                        if (!result.ResultCode)
                        {
                            throw new Exception(result.ResultDesc);
                        }
                        else
                        {
                            if ((IsMergedCell == true && group == true) || IsMergedCell == false)
                            {
                                model.PROMOTION_ID = new Guid(result.ResultDescs);
                            }
                        }

                        logSyncList.Add(new LogSyncCreateUpdate()
                           .CreateModel(Path.GetFileName(path), row + 1, pipedString, MessageType.INF, ExceptionHandler.GENERAL_SUCCESS_MESSAGE, path));
                        rowSuccess++;
                        db.CommitTransaction();
                        #endregion
                    }
                    catch (Exception e)
                    {
                        db.AbortTransaction();
                        e.LogToSync(row + 1, pipedString, Path.GetFileName(path), path, MessageType.ERR);
                        rowFail++;
                    }
                }
                #region SyncLog Insert
                db.BeginTransaction();
                ExceptionHandler.LogToSync(logSyncList);
                db.CommitTransaction();
                #endregion

                if (rowFail == 0)
                {
                    result.ResultCode = true;
                }
                else
                {
                    result.ResultCode = false;
                }

                //result.ResultCode = true;
                result.ResultDesc = Formatting
                    .UPLOAD_NOTIFICATION
                    .Replace("@success", (rowSuccess).ToString())
                    .Replace("@error", (rowFail).ToString());
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result.ResultCode = false;
                result.ResultDesc = e.LogToSync(row - 3, pipedString, Path.GetFileName(path), path, MessageType.ERR).Message;
            }

            db.Close();
            return result;
        }

        public void SaveFile(string TempDownloadFolder, string pathRDLC, Guid? DashboardId)
        {
            dynamic args = new { DashboardId = DashboardId };

            List<PromotionHeaderTemplate> header = new List<PromotionHeaderTemplate>();
            header.Add(GetHeaderPromotion());

            IEnumerable<PromotionReport> list = db.Query<PromotionReport>("Promotion/usp_GetPromotionFromPdf", args);
            var dashboard = DashboardRepository.Instance.GetById((Guid)DashboardId);
            string data = $"{dashboard.CategoryCode}-{dashboard.NominativeIDNo}";//_CategoryCodeProm + "-" + _TransactionCodeProm + "-" + _ReferenceNoProm;
            List<ImageData> imagedata = new List<ImageData>() { new ImageData { QrCode = DashboardRepository.Instance.GenerateQrCode(data) } };

            LocalReport report = new LocalReport();
            report.ReportPath = pathRDLC;
            report.DataSources.Add(new ReportDataSource() { Name = "DataSetPromotionHeader", Value = header });
            report.DataSources.Add(new ReportDataSource() { Name = "DataSetPromotionDetail", Value = list });
            report.DataSources.Add(new ReportDataSource() { Name = "DataSetQrCode", Value = imagedata });

            report.Dispose();

            Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string extension;

            var path = Path.Combine(TempDownloadFolder, $"Promotion_{list.Select(x => x.REFERENCE_NO).FirstOrDefault() ?? DashboardId.ToString().ToUpper()}.pdf");
            byte[] bytes = report.Render("PDF", null, out mimeType, out encoding, out extension, out streamids, out warnings);
            using (var fs = new FileStream(path, FileMode.Create, FileAccess.Write))
            {
                fs.Write(bytes, 0, bytes.Length);
            }

            dynamic argsUpdate = new { DashboardId = DashboardId, Path = path };
            db.Execute("Dashboard_Nominative/usp_UpdateDashboardFromPdf", argsUpdate);
        }

        public PromotionHeaderTemplate GetHeaderPromotion()
        {
            var listHeader = DataFieldEnumRepository.Instance.GetOriginalById(new DataFieldEnum() { CategoryCode = EnumCategoryCode.Promotion });
            var result = new PromotionHeaderTemplate()
            {
                NO = listHeader.Where(x => x.FieldNameByTable == nameof(PromotionHeaderTemplate.NO)).Select(x => x.FieldNameByExcel).FirstOrDefault(),
                NAMA = listHeader.Where(x => x.FieldNameByTable == nameof(PromotionHeaderTemplate.NAMA)).Select(x => x.FieldNameByExcel).FirstOrDefault(),
                NPWP = listHeader.Where(x => x.FieldNameByTable == nameof(PromotionHeaderTemplate.NPWP)).Select(x => x.FieldNameByExcel).FirstOrDefault(),
                ALAMAT = listHeader.Where(x => x.FieldNameByTable == nameof(PromotionHeaderTemplate.ALAMAT)).Select(x => x.FieldNameByExcel).FirstOrDefault(),
                TANGGAL = listHeader.Where(x => x.FieldNameByTable == nameof(PromotionHeaderTemplate.TANGGAL)).Select(x => x.FieldNameByExcel).FirstOrDefault(),
                BENTUK_DAN_JENIS_BIAYA = listHeader.Where(x => x.FieldNameByTable == nameof(PromotionHeaderTemplate.BENTUK_DAN_JENIS_BIAYA)).Select(x => x.FieldNameByExcel).FirstOrDefault(),
                JUMLAH = listHeader.Where(x => x.FieldNameByTable == nameof(PromotionHeaderTemplate.JUMLAH)).Select(x => x.FieldNameByExcel).FirstOrDefault(),
                JUMLAH_GROSS_UP = listHeader.Where(x => x.FieldNameByTable == nameof(PromotionHeaderTemplate.JUMLAH_GROSS_UP)).Select(x => x.FieldNameByExcel).FirstOrDefault(),
                KETERANGAN = listHeader.Where(x => x.FieldNameByTable == nameof(PromotionHeaderTemplate.KETERANGAN)).Select(x => x.FieldNameByExcel).FirstOrDefault(),
                JUMLAH_PPH = listHeader.Where(x => x.FieldNameByTable == nameof(PromotionHeaderTemplate.JUMLAH_PPH)).Select(x => x.FieldNameByExcel).FirstOrDefault(),
                JENIS_PPH = listHeader.Where(x => x.FieldNameByTable == nameof(PromotionHeaderTemplate.JENIS_PPH)).Select(x => x.FieldNameByExcel).FirstOrDefault(),
                JUMLAH_NET = listHeader.Where(x => x.FieldNameByTable == nameof(PromotionHeaderTemplate.JUMLAH_NET)).Select(x => x.FieldNameByExcel).FirstOrDefault(),
                NOMOR_REKENING = listHeader.Where(x => x.FieldNameByTable == nameof(PromotionHeaderTemplate.NOMOR_REKENING)).Select(x => x.FieldNameByExcel).FirstOrDefault(),
                NAMA_REKENING_PENERIMA = listHeader.Where(x => x.FieldNameByTable == nameof(PromotionHeaderTemplate.NAMA_REKENING_PENERIMA)).Select(x => x.FieldNameByExcel).FirstOrDefault(),
                NAMA_BANK = listHeader.Where(x => x.FieldNameByTable == nameof(PromotionHeaderTemplate.NAMA_BANK)).Select(x => x.FieldNameByExcel).FirstOrDefault(),
                NO_KTP = listHeader.Where(x => x.FieldNameByTable == nameof(PromotionHeaderTemplate.NO_KTP)).Select(x => x.FieldNameByExcel).FirstOrDefault()
            };
            return result;
        }
        #endregion
    }
}
