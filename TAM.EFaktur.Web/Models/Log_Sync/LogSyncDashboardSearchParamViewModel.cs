﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.Log_Sync
{
    public class LogSyncDashboardSearchParamViewModel
    {
        public string Id { get; set; }
        public string LogDateFrom { get; set; }
        public string LogDateTo { get; set; }
        public string LogTimeFrom { get; set; }
        public string LogTimeTo { get; set; }
        public string MessageType { get; set; }
    }
}