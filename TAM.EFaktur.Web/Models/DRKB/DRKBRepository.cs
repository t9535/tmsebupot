﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using TAM.EFaktur.Web.Models.SyncData;
using TAM.EFaktur.Web.Models.Log_Sync;
using System.Globalization;

namespace TAM.EFaktur.Web.Models.DRKB
{
    public class DRKBRepository : BaseRepository
    {
        #region Singleton
        private DRKBRepository() { }
        private static DRKBRepository instance = null;
        public static DRKBRepository Instance
        {
            get 
            {
                if (instance == null)
                {
                    instance = new DRKBRepository();
                }
                return instance;
            }
        }
        #endregion

        #region Counter Data
        public int Count(DRKBDashboardSearchParamViewModel model,string DivisionName)
        {
            try
            {
                return db.SingleOrDefault<int>("DRKB/usp_CountDRKBListDashboard", model.MapFromModel(DivisionName));
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Processing Data
        public List<String> GetIdBySearchParam(DRKBDashboardSearchParamViewModel model, string DivisionName)
        {
            IEnumerable<String> result = db.Query<String>("DRKB/usp_GetDRKBIdBySearchParam", model.MapFromModel(DivisionName));
            return result.ToList();
        }

        public List<DRKBReportExcelViewModel> GetReportExcelById(List<Guid> DRKBIdList)
        {
            dynamic args = new
            {
                DRKBIdList = string.Join(";", DRKBIdList)
            };
            IEnumerable<DRKBReportExcelViewModel> result = db.Query<DRKBReportExcelViewModel>("DRKB/usp_GetDRKBReportExcelById", args);
            return result.ToList();
        }

        //public List<DRKBReportExcelViewModel> GetReportExcelAllByIdSearchParam(DRKBDashboardSearchParamViewModel model, string DivisionName)
        //{
        //    IEnumerable<DRKBReportExcelViewModel> result = db.Query<DRKBReportExcelViewModel>("DRKB/usp_GetDRKBReportExcelAllBySearchParam", model.MapFromModel(DivisionName));
        //    return result.ToList();
        //}

        public List<DRKBCSVViewModel> GetCSVById(List<Guid> DRKBIdList)
        {
            dynamic args = new 
            {
                DRKBIdList =  string.Join(";", DRKBIdList)
            };
            IEnumerable<DRKBCSVViewModel> result = db.Query<DRKBCSVViewModel>("DRKB/usp_GetDRKBCSVById", args);
            return result.ToList();
        }

        public int GetCountCSVById(List<Guid> DRKBIdList)
        {
            dynamic args = new 
            {
                DRKBIdList =  string.Join(";", DRKBIdList)
            };
            return db.SingleOrDefault<int>("DRKB/usp_GetCountDRKBCSVById", args);
        }

        public List<DRKBOriginalViewModel> GetOriginalById(List<String> DRKBIdList)
        {
            dynamic args = new
            {
                DRKBIdList = string.Join(";", DRKBIdList)
            };
            IEnumerable<DRKBOriginalViewModel> result = db.Query<DRKBOriginalViewModel>("DRKB/usp_GetDRKBOriginalFileById", args);
            return result.ToList();
        }

        public Result UpdateFlagCSV(List<Guid> DRKBIdList, string FileUrl, string EventActor)
        {
            try
            {
                db.BeginTransaction();
                dynamic args = new
                {
                    DRKBIdList = string.Join(";", DRKBIdList),
                    BatchFileName = Path.GetFileName(@FileUrl)
                };
                db.Execute("DRKB/usp_UpdateCustom_TB_R_DRKB_FlagCSV", args);

                args = new
                {
                    Id = Guid.NewGuid(),
                    CsvType = "FM",
                    CsvTime = DateTime.Now.FormatSQLDateTime(),
                    BatchFileName = Path.GetFileName(@FileUrl),
                    FileUrl = FileUrl,
                    EventDate = DateTime.Now.FormatSQLDateTime(),
                    EventActor = EventActor
                };
                db.Execute("CSVData/usp_InsertCustom_TB_R_CsvData", args);
                db.CommitTransaction();
            }
            catch (Exception e)
            {
                db.AbortTransaction();

                throw e;
            }

            return MsgResultSuccessInsert();
        }

        public List<string> GetPDFUrlById(List<String> DRKBIdList)
        {
            dynamic args = new
            {
                DRKBIdList = string.Join(";", DRKBIdList)
            };
            IEnumerable<string> result = db.Query<string>("DRKB/usp_GetDRKBPDFUrlById", args);
            return result.ToList();
        }

        public List<DRKBDashboardViewModel> GetList(DRKBDashboardSearchParamViewModel model, string DivisionName, string SortBy, string SortDirection, int FromNumber, int ToNumber)
        {
            IEnumerable<DRKBDashboardViewModel> result = db.Query<DRKBDashboardViewModel>("DRKB/usp_GetDRKBListDashboard", model.MapFromModel(DivisionName, SortBy, SortDirection, FromNumber, ToNumber));
            return result.ToList();
        }

        public DRKB GetById(Guid Id)
        {
            return db.SingleOrDefault<DRKB>("DRKB/usp_GetDRKBByID", new { Id = Id });
        }
         
 
        #endregion
    }
}