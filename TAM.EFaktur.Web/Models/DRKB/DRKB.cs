﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TAM.EFaktur.Web.Models.DRKB
{
    public class DRKB : BaseModel
    {
        public System.Guid Id { get; set; }
        public string TaxNumber { get; set; }
        public string TanggalFaktur { get; set; }
        public string NPWPCustomer { get; set; }
        public string NamaCustomer { get; set; }
        public string FrameNumber { get; set; }
        public string EngineNumber { get; set; }
        public string MerkOrType { get; set; }
        public string Model { get; set; }
        public int Year { get; set; }
        public string BusinessUnit { get; set; }

        public Nullable<Decimal> JumlahDPP { get; set; }
       
        public Nullable<Decimal> JumlahPPN { get; set; }
       
        public Nullable<Decimal> LuxTaxAmount { get; set; }
        
        public Nullable<Decimal> PrevLuxTaxAmount { get; set; }
        
        public string Remarks { get; set; }

        public dynamic MapFromModel(DateTime? EventDate = null, string EventActor = "")
        {
            return new
            {
                Id = this.Id,
                TaxNumber = this.TaxNumber,
                TanggalFaktur = this.TanggalFaktur,
                NPWPCustomer = this.NPWPCustomer,
                NamaCustomer = this.NamaCustomer,
                FrameNumber = this.FrameNumber,
                EngineNumber = this.EngineNumber,
                MerkOrType = this.MerkOrType,
                Model = this.Model,
                Year = this.Year,
                BusinessUnit = this.BusinessUnit,
                JumlahDPP = this.JumlahDPP,
                JumlahPPN = this.JumlahPPN,
                LuxTaxAmount = this.LuxTaxAmount,
                PrevLuxTaxAmount = this.PrevLuxTaxAmount,
                Remarks = this.Remarks
            };
        }
    }
}
