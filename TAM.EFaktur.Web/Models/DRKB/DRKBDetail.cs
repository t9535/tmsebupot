﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TAM.EFaktur.Web.Models.DRKB
{
    public class DRKBDetail : BaseModel
    {
        //
        // GET: /DRKBDetail/

        public System.Guid Id { get; set; }
        public System.Guid DRKBId { get; set; }
        public string KodeObjek { get; set; }
        public string NamaObjek { get; set; }
        public decimal HargaSatuan { get; set; }
        public decimal JumlahBarang { get; set; }
        public decimal HargaTotal { get; set; }
        public decimal Diskon { get; set; }
        public decimal DPP { get; set; }
        public decimal PPN { get; set; }
        public decimal TarifPPNBM { get; set; }
        public decimal PPNBM { get; set; }
        public string Status { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public bool RowStatus { get; set; }

        public dynamic MapFromModel()
        {
            return new
            {
                Id = this.Id,
                DRKBId = this.DRKBId,
                KodeObjek = this.KodeObjek,
                NamaObjek = this.NamaObjek,
                HargaSatuan = this.HargaSatuan,
                JumlahBarang = this.JumlahBarang,
                HargaTotal = this.HargaTotal,
                Diskon = this.Diskon,
                DPP = this.DPP,
                PPN = this.PPN,
                TarifPPNBM = this.TarifPPNBM,
                PPNBM = this.PPNBM,
                Status = this.Status,
                CreatedOn = this.CreatedOn,
                CreatedBy = this.CreatedBy,
                ModifiedOn = this.ModifiedOn,
                ModifiedBy = this.ModifiedBy,
                RowStatus = this.RowStatus
            };
        }
    }
}
