﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.SyncData
{
    public class SyncDataDetail :BaseModel
    {
        public Guid SyncId { get; set; }
        public Guid RowId { get; set; }
        public Guid RowData { get; set; }
        public Guid SyncStatus { get; set; }
    }
}