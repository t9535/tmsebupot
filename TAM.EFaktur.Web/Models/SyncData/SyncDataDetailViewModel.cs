﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.SyncData
{
    public class SyncDataDetailViewModel
    {
        public int RowId { get; set; }
        public string RowData { get; set; }
        public bool SyncStatus { get; set; }

        public SyncDataDetailViewModel CreateModel(int RowId, string pipedString)
        {
            SyncDataDetailViewModel model = new SyncDataDetailViewModel();
            model.RowId = RowId;
            // model.RowData = pipedString;
            model.RowData = pipedString;
            model.SyncStatus = true;

            return model;
        }

        public dynamic MapFromModel(Guid SyncId, DateTime EventDate, string EventActor)
        {
            dynamic argsSyncDataDetail = new
            {
                Id = Guid.NewGuid(),
                SyncId = SyncId,
                RowId = this.RowId,
                RowData = this.RowData,
                SyncStatus = this.SyncStatus,
                CreatedOn = EventDate.FormatSQLDateTime(),
                CreatedBy = EventActor
            };

            return argsSyncDataDetail;
        }
        public dynamic MapFromModelCreate(Guid SyncId, DateTime EventDate, string EventActor)
        {
            dynamic argsSyncDataDetail = new
            {
                Id = Guid.NewGuid(),
                SyncId = SyncId,
                RowId = this.RowId,
                RowData = this.RowData,
                SyncStatus = this.SyncStatus,
                CreatedOn = EventDate.FormatSQLDateTime(),
                CreatedBy = EventActor
            };

            return argsSyncDataDetail;
        }
    }
}