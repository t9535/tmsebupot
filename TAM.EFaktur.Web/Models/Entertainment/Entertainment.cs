﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TAM.EFaktur.Web.Models.Entertainment
{
    public class Entertainment
    {
        public Guid? ENTERTAINMENT_ID { get; set; }
        public Guid? DASHBOARD_ID { get; set; }

        public string CATEGORY_CODE { get; set; }

        public int THN_PAJAK { get; set; }

        public string TMP_PENANDATANGAN { get; set; }

        public DateTime? TGL_PENANDATANGAN { get; set; }

        public string NAMA_PENANDATANGAN { get; set; }

        public string JABATAN_PENANDATANGAN { get; set; }

        public int? NO { get; set; }

        public DateTime? TANGGAL { get; set; }
        public string TEMPAT { get; set; }

        public string ALAMAT { get; set; }

        public string BENTUK_DAN_JENIS_ENTERTAINMENT { get; set; }

        public decimal? JUMLAH { get; set; }

        public DateTime? CREATED_DT { get; set; }

        public string CREATED_BY { get; set; }

        public DateTime? CHANGED_DT { get; set; }

        public string CHANGED_BY { get; set; }

        public dynamic MapFromModel(DateTime? EventDate = null, string EventActor = "")
        {
            dynamic args = new
            {
                ENTERTAINMENT_ID = this.ENTERTAINMENT_ID,
                DASHBOARD_ID = this.DASHBOARD_ID,
                //CATEGORY_CODE = this.CATEGORY_CODE,
                THN_PAJAK = this.THN_PAJAK,
                TMP_PENANDATANGAN = this.TMP_PENANDATANGAN,
                TGL_PENANDATANGAN = this.TGL_PENANDATANGAN,
                NAMA_PENANDATANGAN = this.NAMA_PENANDATANGAN,
                JABATAN_PENANDATANGAN = this.JABATAN_PENANDATANGAN,
                NO = this.NO,
                TANGGAL = this.TANGGAL,
                TEMPAT = this.TEMPAT,
                ALAMAT = this.ALAMAT,
                BENTUK_DAN_JENIS_ENTERTAINMENT = this.BENTUK_DAN_JENIS_ENTERTAINMENT,
                JUMLAH = this.JUMLAH,
                CREATED_DT = (EventDate.HasValue ? EventDate.Value : DateTime.Now).FormatSQLDateTime(),
                CREATED_BY = EventActor,
                CHANGED_DT = (EventDate.HasValue ? EventDate.Value : DateTime.Now).FormatSQLDateTime(),
                CHANGED_BY = EventActor
            };
            return args;
        }
        public dynamic MapFromModelHeader(DateTime? EventDate = null, string EventActor = "")
        {
            dynamic args = new
            {
                DASHBOARD_ID = this.DASHBOARD_ID,
                THN_PAJAK = this.THN_PAJAK,
                TMP_PENANDATANGAN = this.TMP_PENANDATANGAN,
                TGL_PENANDATANGAN = this.TGL_PENANDATANGAN,
                NAMA_PENANDATANGAN = this.NAMA_PENANDATANGAN,
                JABATAN_PENANDATANGAN = this.JABATAN_PENANDATANGAN,
                CREATED_DT = (EventDate.HasValue ? EventDate.Value : DateTime.Now).FormatSQLDateTime(),
                CREATED_BY = EventActor,
                CHANGED_DT = (EventDate.HasValue ? EventDate.Value : DateTime.Now).FormatSQLDateTime(),
                CHANGED_BY = EventActor
            };
            return args;
        }

        public bool? PositionSignature { get; set; }
    }

    public class EntertainmentDetail
    {
        public Guid? ENTERTAINMENT_DETAIL_ID { get; set; }
        public Guid? ENTERTAINMENT_ID { get; set; }

        public string NAMA_RELASI { get; set; }

        public string POSISI_RELASI { get; set; }

        public string PERUSAHAAN_RELASI { get; set; }

        public string JENIS_USAHA_RELASI { get; set; }

        public string KETERANGAN { get; set; }

        public DateTime? CREATED_DT { get; set; }

        public string CREATED_BY { get; set; }

        public DateTime? CHANGED_DT { get; set; }

        public string CHANGED_BY { get; set; }

        public Entertainment Entertainment { get; set; }

        public dynamic MapFromModel(DateTime? EventDate = null, string EventActor = "")
        {
            dynamic args = new
            {
                //Header
                ENTERTAINMENT_ID = this.Entertainment.ENTERTAINMENT_ID,
                DASHBOARD_ID = this.Entertainment.DASHBOARD_ID,
                //CATEGORY_CODE = this.CATEGORY_CODE,
                THN_PAJAK = this.Entertainment.THN_PAJAK,
                TMP_PENANDATANGAN = this.Entertainment.TMP_PENANDATANGAN,
                TGL_PENANDATANGAN = this.Entertainment.TGL_PENANDATANGAN,
                NAMA_PENANDATANGAN = this.Entertainment.NAMA_PENANDATANGAN,
                JABATAN_PENANDATANGAN = this.Entertainment.JABATAN_PENANDATANGAN,
                NO = this.Entertainment.NO,
                TANGGAL = this.Entertainment.TANGGAL,
                TEMPAT = this.Entertainment.TEMPAT,
                ALAMAT = this.Entertainment.ALAMAT,
                BENTUK_DAN_JENIS_ENTERTAINMENT = this.Entertainment.BENTUK_DAN_JENIS_ENTERTAINMENT,
                JUMLAH = this.Entertainment.JUMLAH,

                //Detail
                ENTERTAINMENT_DETAIL_ID = this.ENTERTAINMENT_DETAIL_ID,
                NAMA_RELASI = this.NAMA_RELASI,
                POSISI_RELASI = this.POSISI_RELASI,
                PERUSAHAAN_RELASI = this.PERUSAHAAN_RELASI,
                JENIS_USAHA_RELASI = this.JENIS_USAHA_RELASI,
                KETERANGAN = this.KETERANGAN,

                CREATED_DT = (EventDate.HasValue ? EventDate.Value : DateTime.Now).FormatSQLDateTime(),
                CREATED_BY = EventActor,
                CHANGED_DT = (EventDate.HasValue ? EventDate.Value : DateTime.Now).FormatSQLDateTime(),
                CHANGED_BY = EventActor

            };
            return args;
        }
    }

    public class EntertainmentValidation
    {
        public bool NO { get; set; }
        public bool TANGGAL { get; set; }
        public bool TEMPAT { get; set; }
        public bool ALAMAT { get; set; }
        public bool TIPE_ENTERTAINMENT { get; set; }
        public bool JUMLAH { get; set; }
        public bool NAMA_RELASI { get; set; }
        public bool JABATAN { get; set; }
        public bool ALAMAT_PERUSAHAAN { get; set; }
        public bool TIPE_BISNIS { get; set; }
        public bool KETERANGAN { get; set; }
    }
}
