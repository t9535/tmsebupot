﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.Entertainment
{
    public class EntertainmentHeaderTemplate
    {
        public string NO { get; set; }

        public string TANGGAL { get; set; }
        public string TEMPAT { get; set; }

        public string ALAMAT { get; set; }

        public string TIPE_ENTERTAINMENT { get; set; }

        public string JUMLAH { get; set; }

        public string NAMA_RELASI { get; set; }

        public string JABATAN { get; set; }

        public string ALAMAT_PERUSAHAAN { get; set; }

        public string TIPE_BISNIS { get; set; }

        public string KETERANGAN { get; set; }
    }
}