﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.Entertainment
{
    public class EntertainmentReport
    {
        public int NONominative { get; set; }
        public string CompanyName { get; set; }
        public string CompanyNPWP { get; set; }
        public string CompanyAddress { get; set; }
        public string REFERENCE_NO { get; set; }
        public string PV_NO { get; set; }

        public int THN_PAJAK { get; set; }

        public DateTime? PAJAK_FROM { get; set; }
        public DateTime? PAJAK_TO { get; set; }

        public string TMP_PENANDATANGAN { get; set; }

        public DateTime? TGL_PENANDATANGAN { get; set; }

        public string NAMA_PENANDATANGAN { get; set; }

        public string JABATAN_PENANDATANGAN { get; set; }

        public int NO { get; set; }

        public DateTime? TANGGAL { get; set; }
        public string TEMPAT { get; set; }

        public string ALAMAT { get; set; }

        public string BENTUK_DAN_JENIS_ENTERTAINMENT { get; set; }

        public decimal? JUMLAH { get; set; }

        public string NAMA_RELASI { get; set; }

        public string POSISI_RELASI { get; set; }

        public string PERUSAHAAN_RELASI { get; set; }

        public string JENIS_USAHA_RELASI { get; set; }

        public string KETERANGAN { get; set; }

        public decimal? TOTAL { get; set; }
    }
}