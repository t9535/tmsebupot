﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TAM.EFaktur.Web.Models.Log_Sync;
using TAM.EFaktur.Web.Models.Master_Config;
using System.Text.RegularExpressions;
using TAM.EFaktur.Web.Models.Dashboard_Nominative;
using NPOI.SS.Util;
using TAM.EFaktur.Web.Models.Master_DataFieldEnum;
using Microsoft.Reporting.WebForms;

namespace TAM.EFaktur.Web.Models.Entertainment
{
    public class EntertainmentRepository : BaseRepository
    {
        #region Singleton
        private EntertainmentRepository() { }
        private static EntertainmentRepository instance = null;
        public static EntertainmentRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new EntertainmentRepository();
                }
                return instance;
            }
        }
        #endregion

        #region Counter Data
        public int Count(Guid DashboardId)
        {
            dynamic args = new
            {
                DashboardId = DashboardId
            };
            return db.SingleOrDefault<int>("Entertainment/usp_CountEntertainment", args);
        }

        public int CountDetail(Guid? EntertainmentId)
        {
            dynamic args = new
            {
                EntertainmentId = EntertainmentId
            };
            return db.SingleOrDefault<int>("Entertainment/usp_CountEntertainmentDetail", args);
        }
        #endregion

        #region Processing Data GetList for Search
        //list data for search
        public List<Entertainment> GetList(Guid DashboardId, int SortBy, string SortDirection, int FromNumber, int ToNumber)
        {
            dynamic args = new
            {
                DashboardId = DashboardId,
                SortBy = SortBy,
                SortDirection = SortDirection,
                FromNumber = FromNumber,
                ToNumber = ToNumber
            };
            IEnumerable<Entertainment> result = db.Query<Entertainment>("Entertainment/usp_GetEntertainment", args);
            return result.ToList();
        }

        public List<EntertainmentDetail> GetDetailList(Guid? EntertainmentId, int SortBy, string SortDirection, int FromNumber, int ToNumber)
        {
            dynamic args = new
            {
                EntertainmentId = EntertainmentId,
                SortBy = SortBy,
                SortDirection = SortDirection,
                FromNumber = FromNumber,
                ToNumber = ToNumber
            };
            IEnumerable<EntertainmentDetail> result = db.Query<EntertainmentDetail>("Entertainment/usp_GetEntertainmentDetail", args);
            return result.ToList();
        }

        public Entertainment GetById(Guid Id)
        {
            return db.SingleOrDefault<Entertainment>("Entertainment/usp_GetEntertainmentByID", new { ID = Id });
        }

        public EntertainmentDetail GetDetailById(Guid? Id)
        {
            return db.SingleOrDefault<EntertainmentDetail>("Entertainment/usp_GetEntertainmentDetailByID", new { ID = Id });
        }

        public Entertainment GetByDashboardId(Guid DashboardId)
        {
            return db.SingleOrDefault<Entertainment>("Entertainment/usp_GetEntertainmentByDashboardID", new { DashboardId = DashboardId });
        }

        public EntertainmentValidation GetValidationDataFillByDashboardId(Guid DashboardId)
        {
            return db.SingleOrDefault<EntertainmentValidation>("Dashboard_Nominative/usp_GetValidationDataFillByDashboardID", new { DashboardId = DashboardId });
        }

        public Result Insert(Entertainment model, string EventActor)
        {
            try
            {
                db.Execute("Entertainment/usp_InsertOrUpdate_TB_R_Entertainment", model.MapFromModel(DateTime.Now, EventActor));
                return MsgResultSuccessInsert();
            }
            catch (Exception e)
            {
                return MsgResultError(e.LogToApp("Create Entertainment", MessageType.ERR, EventActor).Message);
            }
        }

        public Result Update(Entertainment model, string EventActor)
        {
            try
            {
                db.Execute("Entertainment/usp_InsertOrUpdate_TB_R_Entertainment", model.MapFromModel(DateTime.Now, EventActor));
                return MsgResultSuccessUpdate();
            }
            catch (Exception e)
            {
                return MsgResultError(e.LogToApp("Update Entertainment", MessageType.ERR, EventActor).Message);
            }
        }

        public Result Delete(Entertainment model, string EventActor)
        {
            try
            {
                db.Execute("Entertainment/usp_DeleteEntertainmentByID", new { ID = model.ENTERTAINMENT_ID });
                return MsgResultSuccessDelete();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                return MsgResultError(e.LogToApp("Delete Entertainment", MessageType.ERR, EventActor).Message);
            }
        }

        public Result SubmitHeader(Entertainment model, string EventActor, string path, string pathRDLC)
        {
            try
            {
                db.Execute("Entertainment/usp_SubmitHeader_TB_R_Entertainment", model.MapFromModelHeader(DateTime.Now, EventActor));
                SaveFile(path, pathRDLC, model.DASHBOARD_ID);
                return MsgResultSuccessUpdate();
            }
            catch (Exception e)
            {
                return MsgResultError(e.Message.ToString());
            }
        }


        public Result InsertDetail(EntertainmentDetail model, string EventActor)
        {
            try
            {
                Result result = db.SingleOrDefault<Result>("Entertainment/usp_InsertOrUpdate_TB_R_Entertainment_Detail", model.MapFromModel(DateTime.Now, EventActor));
                if (result.ResultCode == true) { result.ResultDesc = msgSuccessInsert; }
                return result;
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                return MsgResultError(e.LogToApp("Create Entertainment Detail", MessageType.ERR, EventActor).Message);
            }
        }

        public Result UpdateDetail(EntertainmentDetail model, string EventActor)
        {
            try
            {
                db.Execute("Entertainment/usp_InsertOrUpdate_TB_R_Entertainment_Detail", model.MapFromModel(DateTime.Now, EventActor));
                return MsgResultSuccessUpdate();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                return MsgResultError(e.LogToApp("Update Entertainment Detail", MessageType.ERR, EventActor).Message);
            }
        }

        public Result DeleteDetail(EntertainmentDetail model, string EventActor)
        {
            try
            {
                db.Execute("Entertainment/usp_DeleteEntertainmentDetailByID", new { ID = model.ENTERTAINMENT_DETAIL_ID });
                return MsgResultSuccessDelete();
            }
            catch (Exception e)
            {
                return MsgResultError(e.LogToApp("Delete Entertainment Detail", MessageType.ERR, EventActor).Message);
            }
        }


        public Result ExcelEntertainmentInsert(string path, string EventActor, Entertainment data)
        {
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = "File has been uploaded successfully"
            };

            FileStream file = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite);

            HSSFWorkbook hssfwb = new HSSFWorkbook(file);
            ISheet sheet = hssfwb.GetSheet("Default");

            string pipedString = string.Empty;

            int row = 0;
            int rowFail = 0;
            int rowSuccess = 0;

            try
            {
                file.Close();
                var model = new Entertainment();
                var validationDataFill = GetValidationDataFillByDashboardId((Guid)data.DASHBOARD_ID);
                var cellNum = 11;////jumlah header cell +1;
                var rowNum = 6;
                List<int> exceptionCellNum = new List<int>();
                //exceptionCellNum.Add(17); 

                //var config = MasterConfigRepository.Instance.GetByConfigKey("UrlTemplateEntertainment");
                //if (config == null)
                //{
                //    throw new Exception("Please define the UrlTemplateNominativeData Config");
                //}

                if (sheet.GetRow(rowNum - 1) == null)
                {
                    result.ResultCode = false;
                    result.ResultDesc = "Invalid template entertainment.";
                    return result;
                }

                if (sheet.GetRow(rowNum - 1).LastCellNum != cellNum)
                {
                    result.ResultCode = false;
                    result.ResultDesc = "Invalid template entertainment.";
                    return result;
                }

                List<GroupingExcel> groups = new List<GroupingExcel>();
                int mergedRegions = sheet.NumMergedRegions;
                for (int regions = 0; regions < mergedRegions; regions++)
                {
                    CellRangeAddress mergedRegionIndex = sheet.GetMergedRegion(regions);
                    if (mergedRegionIndex.FirstRow >= rowNum)
                    {
                        groups.Add(new GroupingExcel { FirstRow = mergedRegionIndex.FirstRow, LastRow = mergedRegionIndex.LastRow, FirstColumn = mergedRegionIndex.FirstColumn, LastColumn = mergedRegionIndex.LastColumn });
                    }
                }

                List<LogSyncCreateUpdate> logSyncList = new List<LogSyncCreateUpdate>();
                List<string> failTaxNumber = new List<string>();

                for (row = rowNum; row <= sheet.LastRowNum; row++)
                {
                    #region Set Cell Policy, Cell Type and Save Sync Data
                    pipedString = string.Empty;
                    for (int i = 0; i < cellNum; i++)
                    {
                        sheet.GetRow(row).GetCell(i, MissingCellPolicy.CREATE_NULL_AS_BLANK);
                        sheet.GetRow(row).GetCell(i).SetCellType(CellType.STRING);

                        pipedString += NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(i)) + "|";
                    }
                    pipedString = pipedString.Remove(pipedString.Length - 1);
                    #endregion

                    try
                    {
                        #region Cell Mapping & Insert to DB 
                        db.BeginTransaction();

                        string validation = "";
                        DateTime? TANGGAL = null;
                        decimal? JUMLAH = null;
                        int? NO = null;
                        //bool IsMergedCell = sheet.GetRow(row).GetCell(0).IsMergedCell;
                        bool IsMergedCell = groups.Where(x => (row >= x.FirstRow && row <= x.LastRow) && (x.FirstColumn >= 0 && x.LastColumn < 6)).Count() > 0 ? true : false;
                        bool group = groups.Where(x => x.FirstRow == row && x.FirstColumn >= 0 && x.LastColumn < 6).Count() > 0 ? true : false;

                        //Header
                        var NO_STRING = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(0));
                        var TANGGAL_STRING = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(1));
                        var TEMPAT = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(2));
                        var ALAMAT = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(3));
                        var BENTUK_DAN_JENIS_ENTERTAINMENT = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(4));
                        var JUMLAH_STRING = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(5));

                        //Details
                        var NAMA_RELASI = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(6));
                        var POSISI_RELASI = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(7));
                        var PERUSAHAAN_RELASI = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(8));
                        var JENIS_USAHA_RELASI = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(9));
                        var KETERANGAN = NPOIExcel.GetStringValue(sheet.GetRow(row).GetCell(10));

                        #region Validation
                        //jika ada space di semua kolom maka tidak di proses
                        if (string.IsNullOrEmpty(NO_STRING) && string.IsNullOrEmpty(TANGGAL_STRING) && string.IsNullOrEmpty(TEMPAT) && string.IsNullOrEmpty(ALAMAT) &&
                            string.IsNullOrEmpty(BENTUK_DAN_JENIS_ENTERTAINMENT) && string.IsNullOrEmpty(JUMLAH_STRING) && string.IsNullOrEmpty(NAMA_RELASI) && string.IsNullOrEmpty(POSISI_RELASI) &&
                            string.IsNullOrEmpty(PERUSAHAAN_RELASI) && string.IsNullOrEmpty(JENIS_USAHA_RELASI) && string.IsNullOrEmpty(KETERANGAN)
                            )
                        {
                            break;
                        }

                        var header = GetHeaderEntertainment();
                        //Nullale
                        //Header
                        if ((IsMergedCell == true && group == true) || IsMergedCell == false)
                        {
                            if (validationDataFill.NO) { if (string.IsNullOrEmpty(NO_STRING)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.NO} can't be empy"; } }
                            if (validationDataFill.TANGGAL) { if (string.IsNullOrEmpty(TANGGAL_STRING)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.TANGGAL} can't be empy"; } }
                            if (validationDataFill.TEMPAT) { if (string.IsNullOrEmpty(TEMPAT)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.TEMPAT} can't be empy"; } }
                            if (validationDataFill.ALAMAT) { if (string.IsNullOrEmpty(ALAMAT)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.ALAMAT} can't be empy"; } }
                            if (validationDataFill.TIPE_ENTERTAINMENT) { if (string.IsNullOrEmpty(BENTUK_DAN_JENIS_ENTERTAINMENT)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.TIPE_ENTERTAINMENT} can't be empy"; } }
                            if (validationDataFill.JUMLAH) { if (string.IsNullOrEmpty(JUMLAH_STRING)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.JUMLAH} can't be empy"; } }
                        }

                        //Detail
                        if (validationDataFill.NAMA_RELASI) { if (string.IsNullOrEmpty(NAMA_RELASI)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.NAMA_RELASI} can't be empy"; } }
                        if (validationDataFill.JABATAN) { if (string.IsNullOrEmpty(POSISI_RELASI)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.JABATAN} can't be empy"; } }
                        if (validationDataFill.ALAMAT_PERUSAHAAN) { if (string.IsNullOrEmpty(PERUSAHAAN_RELASI)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.ALAMAT_PERUSAHAAN} can't be empy"; } }
                        if (validationDataFill.TIPE_BISNIS) { if (string.IsNullOrEmpty(JENIS_USAHA_RELASI)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.TIPE_BISNIS} can't be empy"; } }
                        if (validationDataFill.KETERANGAN) { if (string.IsNullOrEmpty(KETERANGAN)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.KETERANGAN} can't be empy"; } }

                        //must be less than or equal to
                        //Header
                        if ((IsMergedCell == true && group == true) || IsMergedCell == false)
                        {
                            if (!string.IsNullOrEmpty(NO_STRING)) { if (NO_STRING.Length >= 8) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.NO} must be less than or equal to 8 character"; } }
                            //if (!string.IsNullOrEmpty(TANGGAL_STRING)) { if (TANGGAL_STRING.Length >= 10) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += "Tanggal must be less than or equal to 10 character"; } }
                            if (!string.IsNullOrEmpty(TEMPAT)) { if (TEMPAT.Length >= 100) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.TEMPAT} must be less than or equal to 100 character"; } }
                            if (!string.IsNullOrEmpty(ALAMAT)) { if (ALAMAT.Length >= 200) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.ALAMAT} must be less than or equal to 200 character"; } }
                            if (!string.IsNullOrEmpty(BENTUK_DAN_JENIS_ENTERTAINMENT)) { if (BENTUK_DAN_JENIS_ENTERTAINMENT.Length >= 20) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.TIPE_ENTERTAINMENT} must be less than or equal to 20 character"; } }
                        }

                        //detail
                        if (!string.IsNullOrEmpty(NAMA_RELASI)) { if (NAMA_RELASI.Length >= 100) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.NAMA_RELASI} must be less than or equal to 100 character"; } }
                        if (!string.IsNullOrEmpty(POSISI_RELASI)) { if (POSISI_RELASI.Length >= 100) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.JABATAN} must be less than or equal to 100 character"; } }
                        if (!string.IsNullOrEmpty(PERUSAHAAN_RELASI)) { if (PERUSAHAAN_RELASI.Length >= 100) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.ALAMAT_PERUSAHAAN} must be less than or equal to 100 character"; } }
                        if (!string.IsNullOrEmpty(JENIS_USAHA_RELASI)) { if (JENIS_USAHA_RELASI.Length >= 20) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.TIPE_BISNIS} must be less than or equal to 20 character"; } }
                        if (!string.IsNullOrEmpty(KETERANGAN)) { if (KETERANGAN.Length >= 200) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.KETERANGAN} must be less than or equal to 200 character"; } }

                        if (!string.IsNullOrEmpty(TANGGAL_STRING))
                        {
                            double TANGGAL_DOUBLE;
                            if (!double.TryParse(TANGGAL_STRING, out TANGGAL_DOUBLE))
                            {
                                DateTime TANGGAL1;
                                if (!DateTime.TryParseExact(TANGGAL_STRING, NPOIExcel.FormatDate, CultureInfo.InvariantCulture, DateTimeStyles.None, out TANGGAL1))
                                {
                                    if (!string.IsNullOrEmpty(validation)) validation += ", ";
                                    validation += $"{header.TANGGAL} format must be DD/MM/YYYY";
                                }
                                else
                                {
                                    TANGGAL = TANGGAL1;
                                }
                            }
                            else
                            {
                                TANGGAL = DateTime.FromOADate(TANGGAL_DOUBLE);
                            }
                        }

                        if (!string.IsNullOrEmpty(JUMLAH_STRING))
                        {
                            Decimal JUMLAH1;
                            if (!decimal.TryParse(JUMLAH_STRING, out JUMLAH1)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.JUMLAH} Valid format numeric type"; } else { JUMLAH = JUMLAH1; }
                        }

                        if (!string.IsNullOrEmpty(NO_STRING))
                        {
                            Int32 NO1;
                            if (!int.TryParse(NO_STRING, out NO1)) { if (!string.IsNullOrEmpty(validation)) validation += ", "; validation += $"{header.NO} Valid format numeric type"; } else { NO = NO1; }
                        }


                        if (!string.IsNullOrEmpty(validation))
                        {
                            throw new Exception(validation);
                        }
                        #endregion

                        if ((IsMergedCell == true && group == true) || IsMergedCell == false)
                        {
                            model = new Entertainment()
                            {
                                DASHBOARD_ID = data.DASHBOARD_ID,
                                THN_PAJAK = data.THN_PAJAK,
                                NAMA_PENANDATANGAN = data.NAMA_PENANDATANGAN,
                                TMP_PENANDATANGAN = data.TMP_PENANDATANGAN,
                                TGL_PENANDATANGAN = data.TGL_PENANDATANGAN,
                                JABATAN_PENANDATANGAN = data.JABATAN_PENANDATANGAN,
                                NO = NO,
                                TANGGAL = TANGGAL,
                                TEMPAT = TEMPAT,
                                ALAMAT = ALAMAT,
                                BENTUK_DAN_JENIS_ENTERTAINMENT = BENTUK_DAN_JENIS_ENTERTAINMENT,
                                JUMLAH = JUMLAH
                            };
                        }

                        var modelDetail = new EntertainmentDetail()
                        {
                            NAMA_RELASI = NAMA_RELASI,
                            POSISI_RELASI = POSISI_RELASI,
                            PERUSAHAAN_RELASI = PERUSAHAAN_RELASI,
                            JENIS_USAHA_RELASI = JENIS_USAHA_RELASI,
                            KETERANGAN = KETERANGAN,
                            Entertainment = model
                        };

                        // Save
                        result = db.SingleOrDefault<Result>("Entertainment/usp_InsertOrUpdate_TB_R_Entertainment_Detail", modelDetail.MapFromModel(DateTime.Now.AddSeconds(row), EventActor));

                        if (!result.ResultCode)
                        {
                            throw new Exception(result.ResultDesc);
                        }
                        else
                        {
                            if ((IsMergedCell == true && group == true) || IsMergedCell == false)
                            {
                                model.ENTERTAINMENT_ID = new Guid(result.ResultDescs);
                            }
                        }

                        logSyncList.Add(new LogSyncCreateUpdate()
                           .CreateModel(Path.GetFileName(path), row + 1, pipedString, MessageType.INF, ExceptionHandler.GENERAL_SUCCESS_MESSAGE, path));
                        rowSuccess++;
                        db.CommitTransaction();
                        #endregion
                    }
                    catch (Exception e)
                    {
                        db.AbortTransaction();
                        e.LogToSync(row + 1, pipedString, Path.GetFileName(path), path, MessageType.ERR);
                        rowFail++;
                    }
                }
                #region SyncLog Insert
                db.BeginTransaction();
                ExceptionHandler.LogToSync(logSyncList);
                db.CommitTransaction();
                #endregion

                if (rowFail == 0)
                {
                    result.ResultCode = true;
                }
                else
                {
                    result.ResultCode = false;
                }

                //result.ResultCode = true;
                result.ResultDesc = Formatting
                    .UPLOAD_NOTIFICATION
                    .Replace("@success", (rowSuccess).ToString())
                    .Replace("@error", (rowFail).ToString());
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result.ResultCode = false;
                result.ResultDesc = e.LogToSync(row - 3, pipedString, Path.GetFileName(path), path, MessageType.ERR).Message;
            }

            db.Close();
            return result;
        }

        public void SaveFile(string TempDownloadFolder, string pathRDLC, Guid? DashboardId)
        {
            dynamic args = new { DashboardId = DashboardId };

            List<EntertainmentHeaderTemplate> header = new List<EntertainmentHeaderTemplate>();
            header.Add(GetHeaderEntertainment());

            IEnumerable<EntertainmentReport> list = db.Query<EntertainmentReport>("Entertainment/usp_GetEntertainmentFromPdf", args);
            var dashboard = DashboardRepository.Instance.GetById((Guid)DashboardId);
            string data = $"{dashboard.CategoryCode}-{dashboard.NominativeIDNo}";//_CategoryCodeProm + "-" + _TransactionCodeProm + "-" + _ReferenceNoProm;
            List<ImageData> imagedata = new List<ImageData>() { new ImageData { QrCode = DashboardRepository.Instance.GenerateQrCode(data) } };

            LocalReport report = new LocalReport();
            report.ReportPath = pathRDLC;
            report.DataSources.Add(new ReportDataSource() { Name = "DataSetEntertainmentHeader", Value = header });
            report.DataSources.Add(new ReportDataSource() { Name = "DataSetEntertainmentDetail", Value = list });
            report.DataSources.Add(new ReportDataSource() { Name = "DataSetQrCode", Value = imagedata });

            report.Dispose();

            Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string extension;

            var path = Path.Combine(TempDownloadFolder, $"Entertainment_{list.Select(x => x.REFERENCE_NO).FirstOrDefault() ?? DashboardId.ToString().ToUpper()}.pdf");
            //var path = "D:/Interface/Temp/Download/TEs.pdf";
            byte[] bytes = report.Render("PDF", null, out mimeType, out encoding, out extension, out streamids, out warnings);
            using (var fs = new FileStream(path, FileMode.Create, FileAccess.Write))
            {
                fs.Write(bytes, 0, bytes.Length);
            }

            dynamic argsUpdate = new { DashboardId = DashboardId, Path = path };
            db.Execute("Dashboard_Nominative/usp_UpdateDashboardFromPdf", argsUpdate);
        }

        public EntertainmentHeaderTemplate GetHeaderEntertainment()
        {
            var listHeader = DataFieldEnumRepository.Instance.GetOriginalById(new DataFieldEnum() { CategoryCode = EnumCategoryCode.Entertainment });
            var result = new EntertainmentHeaderTemplate()
            {
                NO = listHeader.Where(x => x.FieldNameByTable == nameof(EntertainmentHeaderTemplate.NO)).Select(x => x.FieldNameByExcel).FirstOrDefault(),
                TANGGAL = listHeader.Where(x => x.FieldNameByTable == nameof(EntertainmentHeaderTemplate.TANGGAL)).Select(x => x.FieldNameByExcel).FirstOrDefault(),
                TEMPAT = listHeader.Where(x => x.FieldNameByTable == nameof(EntertainmentHeaderTemplate.TEMPAT)).Select(x => x.FieldNameByExcel).FirstOrDefault(),
                ALAMAT = listHeader.Where(x => x.FieldNameByTable == nameof(EntertainmentHeaderTemplate.ALAMAT)).Select(x => x.FieldNameByExcel).FirstOrDefault(),
                TIPE_ENTERTAINMENT = listHeader.Where(x => x.FieldNameByTable == nameof(EntertainmentHeaderTemplate.TIPE_ENTERTAINMENT)).Select(x => x.FieldNameByExcel).FirstOrDefault(),
                JUMLAH = listHeader.Where(x => x.FieldNameByTable == nameof(EntertainmentHeaderTemplate.JUMLAH)).Select(x => x.FieldNameByExcel).FirstOrDefault(),
                NAMA_RELASI = listHeader.Where(x => x.FieldNameByTable == nameof(EntertainmentHeaderTemplate.NAMA_RELASI)).Select(x => x.FieldNameByExcel).FirstOrDefault(),
                JABATAN = listHeader.Where(x => x.FieldNameByTable == nameof(EntertainmentHeaderTemplate.JABATAN)).Select(x => x.FieldNameByExcel).FirstOrDefault(),
                ALAMAT_PERUSAHAAN = listHeader.Where(x => x.FieldNameByTable == nameof(EntertainmentHeaderTemplate.ALAMAT_PERUSAHAAN)).Select(x => x.FieldNameByExcel).FirstOrDefault(),
                TIPE_BISNIS = listHeader.Where(x => x.FieldNameByTable == nameof(EntertainmentHeaderTemplate.TIPE_BISNIS)).Select(x => x.FieldNameByExcel).FirstOrDefault(),
                KETERANGAN = listHeader.Where(x => x.FieldNameByTable == nameof(EntertainmentHeaderTemplate.KETERANGAN)).Select(x => x.FieldNameByExcel).FirstOrDefault()
            };
            return result;
        }
        #endregion
    }
}
