﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.ApplicationLog
{
    public class ApplicationLogRepository : BaseRepository
    {
        #region Singleton
        private ApplicationLogRepository() { }
        private static ApplicationLogRepository instance = null;
        public static ApplicationLogRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ApplicationLogRepository();
                }
                return instance;
            }
        }
        #endregion

        #region Counter Data
        public int Count(ApplicationLogDashboardSearchParamViewModel model)
        {
            dynamic args = new
            {

                LogDateFrom = model.LogDateFrom.FormatDefaultDateWhenNullOrEmpty(),
                LogDateTo = model.LogDateTo.FormatDefaultDateWhenNullOrEmpty("TO"),
                LogTimeFrom = model.LogTimeFrom.FormatDefaultTimeWhenNullOrEmpty(),
                LogTimeTo = model.LogTimeTo.FormatDefaultTimeWhenNullOrEmpty("TO"),
                MessageType = model.MessageType
            };

            return db.SingleOrDefault<int>("ApplicationLog/usp_CountApplicationLogListDashboard", args);
        }
        #endregion
        #region Processing Data

        public List<ApplicationLogDashboardViewModel> GetList(ApplicationLogDashboardSearchParamViewModel model, int FromNumber, int ToNumber)
        {
            dynamic args = new
            {
                LogDateFrom = model.LogDateFrom.FormatDefaultDateWhenNullOrEmpty(),
                LogDateTo = model.LogDateTo.FormatDefaultDateWhenNullOrEmpty("TO"),
                LogTimeFrom = model.LogTimeFrom.FormatDefaultTimeWhenNullOrEmpty(),
                LogTimeTo = model.LogTimeTo.FormatDefaultTimeWhenNullOrEmpty("TO"),
                MessageType = model.MessageType,
                FromNumber = FromNumber,
                ToNumber = ToNumber
            };
            IEnumerable<ApplicationLogDashboardViewModel> result = db.Query<ApplicationLogDashboardViewModel>("ApplicationLog/usp_GetApplicationLogListDashboard", args);
            return result.ToList();
        }

        public ApplicationLog GetById(Guid Id)
        {
            return db.SingleOrDefault<ApplicationLog>("ApplicationLog/usp_GetApplicationLogByID", new { Id = Id });
        }

        //Get Dropdown Message Type
        public List<DropdownViewModel> GetMessageTypeDropdown()
        {

            IEnumerable<DropdownViewModel> result = db.Query<DropdownViewModel>("ApplicationLog/usp_DropDownMessageType");
            return result.ToList();
        }
        #endregion
    }
}