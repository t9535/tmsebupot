﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.ApplicationLog
{
    public class ApplicationLogDashboardViewModel
    {
        public int RowNum { get; set; }
        public Guid Id { get; set; }
        public DateTime LoginDate { get; set; }
        public string ClientIP { get; set; }
        public string ClientHostName { get; set; }
        public string MessageType { get; set; }
        public string MessageLocation { get; set; }
        public string MessageDetail { get; set; }
    }
}