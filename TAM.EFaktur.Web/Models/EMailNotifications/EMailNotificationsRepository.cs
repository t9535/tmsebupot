﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using TAM.EFaktur.Web.Models.SyncData;
using TAM.EFaktur.Web.Models.Log_Sync;
using System.Globalization;

namespace TAM.EFaktur.Web.Models.EMailNotifications
{
    public class EMailNotificationsRepository : BaseRepository
    {
        #region Singleton
        private EMailNotificationsRepository() { }
        private static EMailNotificationsRepository instance = null;
        public static EMailNotificationsRepository Instance
        {
            get 
            {
                if (instance == null)
                {
                    instance = new EMailNotificationsRepository();
                }
                return instance;
            }
        }
        #endregion
        public Result Delete(EMailNotifications model)
        {
            try
            {
                dynamic args = new
                {
                    email_body = model.email_body
                };
                db.Execute("EMailNotifications/usp_Update_tbl_email_notifications", args);
                return MsgResultSuccessInsert();
            }
            catch (Exception e)
            {
                return MsgResultError(e.Message.ToString());
            }
        }


        public Result Update(EMailNotifications model, string EventActor, DateTime EventDate)
        {
            try
            {
                dynamic args = new
                {
                    email_body = model.email_body,
                    isSend = model.isSend,
                    ModifiedOn = EventDate.FormatSQLDateTime(),
                    ModifiedBy = EventActor
                };
                db.Execute("EMailNotifications/usp_Update_tbl_email_notifications", args);
                return MsgResultSuccessInsert();
            }
            catch (Exception e)
            {
                return MsgResultError(e.Message.ToString());
            }
        }

        public Result Insert(EMailNotifications model, string EventActor, DateTime EventDate)
        {
            try
            {
                dynamic args = new
                {
                    Id = model.Id,
                    email_from = model.email_from,
                    email_to = model.email_to,
                    email_subject = model.email_subject,
                    email_body = model.email_body,
                    isSend = model.isSend,
                    CreatedOn= EventDate.FormatSQLDateTime(),
                    CreatedBy = EventActor
                };
                db.Execute("EMailNotifications/usp_Insert_tbl_email_notifications", args);
                return MsgResultSuccessInsert();
            }
            catch (Exception e)
            {
                return MsgResultError(e.Message.ToString());
            }
        }

        public List<EMailNotifications> GetList(EMailNotifications model, string DivisionName, string SortBy, string SortDirection, int FromNumber, int ToNumber)
        {
            IEnumerable<EMailNotifications> result = db.Query<EMailNotifications>("EMailNotifications/usp_GetEMailNotificationsList", model.MapFromModel());
            return result.ToList();
        }

        public EMailNotifications GetById(Guid Id)
        {
            return db.SingleOrDefault<EMailNotifications>("EMailNotifications/usp_GetEMailNotificationsByID", new { Id = Id });
        }
         
  
    }
}