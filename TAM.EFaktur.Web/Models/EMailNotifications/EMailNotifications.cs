﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TAM.EFaktur.Web.Models.EMailNotifications
{
    public class EMailNotifications : BaseModel
    {
        public string email_from { get; set; }
        public string email_to { get; set; }
        public string email_subject { get; set; }
        public int isSend { get; set; }
        public string email_body { get; set; }
         

        public dynamic MapFromModel(DateTime? EventDate = null, string EventActor = "")
        {
            return new
            {
                Id = this.Id,
                email_from = this.email_from,
                email_to = this.email_to,
                email_subject = this.email_subject,
                send = this.isSend,
                email_body = this.email_body
            };
        }
    }
}
