﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TAM.EFaktur.Web.Models
{
    public class NotifRepository : BaseRepository
    {
        #region Singleton
        private NotifRepository() { }
        private static NotifRepository instance = null;
        public static NotifRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new NotifRepository();
                }
                return instance;
            }
        }
        #endregion
        public string GetNotif()
        {
            try
            {
                return db.SingleOrDefault<string>("Notif/efb_sp_GetAllNotifications");
            }
            catch
            {
                throw;
            }
        }
    }
}
