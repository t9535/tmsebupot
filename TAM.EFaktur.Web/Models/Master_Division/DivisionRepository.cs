﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.Master_Division
{
    public class DivisionRepository : BaseRepository
    {
         #region Singleton
        private DivisionRepository() { }
        private static DivisionRepository instance = null;
        public static DivisionRepository Instance
        {
            get 
            {
                if (instance == null)
                {
                    instance = new DivisionRepository();
                }
                return instance;
            }
        }
        #endregion

        

        #region Processing Data

        public List<DivisionDropdownViewModel>GetList(DivisionSearchParamViewModel model)
        {
            dynamic args = new
            {

                DivisionName = model.DivisionName,
                DivisionCode = model.DivisionCode
                
            };
            IEnumerable<DivisionDropdownViewModel> result = db.Query<DivisionDropdownViewModel>("Master_Division/usp_GetDivisionList", args);
            return result.ToList();
        }
    

        #endregion
        
        #region GetDivisionDropdown

        public List<DropdownViewModel>GetDivisionDropdown() 
        {

            IEnumerable<DropdownViewModel> result = db.Query<DropdownViewModel>("Master_Division/usp_GetDivisionList");
            return result.ToList();
        }
        #endregion
    }
       
     
}