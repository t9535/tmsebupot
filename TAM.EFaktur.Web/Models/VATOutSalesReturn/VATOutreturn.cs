﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.VATOutSalesReturn
{
    public class VATOutreturn : BaseModel
    {
        public System.Guid Id { get; set; }
        public string AlamatLawanTransaksi { get; set; }
        public string AlamatLawanTransaksi2 { get; set; }
        public string KodeObjek { get; set; }
        public string Nama_pt { get; set; }
        public string no_pengukuhan_pt { get; set; }
        public string alamat { get; set; }
        public string alamat_2 { get; set; }
        public string nama_barang1 { get; set; }
        public string nama_barang2 { get; set; }
        public string AlamatPembeli { get; set; }
        public string AlamatPembeli2 { get; set; }
        public string KodeObjek1 { get; set; }
        public string KodeObjek2 { get; set; }

        public dynamic MapFromModel(DateTime? EventDate = null, string EventActor = "")
        {
            return new
            {
                Id = this.Id,
                CreatedBy = this.CreatedBy,
                CreatedOn = this.CreatedOn,
                EventDate = EventDate.HasValue ? EventDate.Value : DateTime.Now,
                EventActor = EventActor,
                AlamatLawanTransaksi = this.AlamatLawanTransaksi,
                AlamatLawanTransaksi2 =this.AlamatLawanTransaksi2,
                KodeObjek=this.KodeObjek,
                Nama_pt=this.Nama_pt,
                no_pengukuhan_pt=this.no_pengukuhan_pt,
                alamat=this.alamat,
                alamat_2=this.alamat_2,
                nama_barang1 = this.nama_barang1,
                nama_barang2 = this.nama_barang2,
                AlamatPembeli=this.AlamatPembeli,
                AlamatPembeli2 = this.AlamatPembeli2,
                KodeObjek1 = this.KodeObjek1,
                KodeObjek2 = this.KodeObjek2,


            };
        }
    }
}