﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TAM.EFaktur.Web.Models.VATOutSalesReturn
{
    public class VATOutSalesReturnViewModel : Controller
    {
        public int RowNum { get; set; }
        public Guid Id { get; set; }
        public int ToNumber { get; set; }
        public string TransactionDataFile { get; set; }
        public string CreditNoteNumber { get; set; }
        public DateTime ReturnDate { get; set; }
        public string ReturnTaxInvoiceNumber { get; set; }
        public string CANumber { get; set; }
        public string ReturnDA { get; set; }
        public DateTime DaDate { get; set; }
        public string NPWPCustomer { get; set; }
        public string NamaCustomer { get; set; }
        public decimal ReturnVATBasedAmount { get; set; }
        public decimal ReturnVATAmount { get; set; }
        public string BusinessUnit { get; set; }
        public Nullable<DateTime> ReceiveFile { get; set; }
        public string RecordStatus { get; set; }
        public DateTime RecordDate { get; set; }
        public string DownloadStatus { get; set; }
        public Nullable<DateTime> DownloadDate { get; set; }
        public string ApprovalStatus { get; set; }
        public DateTime ApprovalTime { get; set; }
        public string TaxInvoiceStatus { get; set; }
        public string TransitoryStatus { get; set; }
        public string TransitoryNumber { get; set; }
        //public string TransitoryDate { get; set; }
        public DateTime TransitoryDate { get; set; }
        public string BatchFileName { get; set; }
        public string EFakturStatus { get; set; }
        //public string CreditNoteTTD { get; set; }
        //public string CreditNoteDGT { get; set; }

        //Add ridwan //
        //public string Id { get; set; }
        public string CreditNoteOriginal { get; set; }
        public string CreditNoteTTD { get; set; }
        public string CreditNoteDGT { get; set; }
       

    }
}
