﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TAM.EFaktur.Web.Models.Attachment_Type
{
    public class AttachmentTypeRepository : BaseRepository
    {
        #region Singletonh
        private AttachmentTypeRepository() { }
        private static AttachmentTypeRepository instance = null;
        public static AttachmentTypeRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new AttachmentTypeRepository();
                }
                return instance;
            }
        }
        #endregion

        //#region Counter Data
        ////public int Count(string ConfigName, string configValue)
        //public int Count(VATInDashboardSearchParamViewModel model)
        //{
        //    dynamic args = new
        //    {
        //        ConfigName = ConfigName,
        //        configValue = configValue,

        //    };

        //    return db.SingleOrDefault<int>("Config/countApplications", args); 
        //}
        //#endregion

        #region Counter Data
        public int Count(AttachmentTypeDashboardSearchParamViewModel model)
        {
            dynamic args = new
            {
                CategoryCode = model.CategoryCode,
                Description = model.Description,
                FileType = model.FileType,
                Vendor_Group = model.Vendor_Group,
                Aplication = model.Aplication
                
            };

            return db.SingleOrDefault<int>("Attachment_Type/usp_CountAttachmentTypeListDashboard", args);
        }
        #endregion
        #region Processing Data

        public List<AttachmentTypeDashboardViewModel> GetList(AttachmentTypeDashboardSearchParamViewModel model, int SortBy, string SortDirection, int FromNumber, int ToNumber)
        {
            dynamic args = new
            {
                CategoryCode = model.CategoryCode,
                Description = model.Description,
                FileType = model.FileType,
                Vendor_Group = model.Vendor_Group,
                Ebiling = model.Ebiling,
                Elvis = model.Elvis,
                Aplication = model.Aplication,
                SortBy = SortBy,
                SortDirection = SortDirection,
                FromNumber = FromNumber,
                ToNumber = ToNumber
            };
            IEnumerable<AttachmentTypeDashboardViewModel> result = db.Query<AttachmentTypeDashboardViewModel>("Attachment_Type/usp_GetAttachmentListDashboard", args);
            return result.ToList();
        }
        #endregion

        public Attachment GetById(int AttachmentId)
        {
            return db.SingleOrDefault<Attachment>("Attachment_Type/usp_GetAttachmentTypeByID", new { AttachmentId = AttachmentId });
        }

        public Attachment GetByConfigKey(String ConfigKey)
        {
            return db.SingleOrDefault<Attachment>("Master_Config/usp_GetConfigurationDataByConfigKey", new { ConfigKey = ConfigKey });
        }

        //insert Data
        public Result Save_Update(AttachmentTypeParamModel objParameterSetupParamModel, string CreatedBy)
        {
            try
            {
                db.Execute("Attachment_Type/usp_Insert_Update_TB_M_AttachmentType", objParameterSetupParamModel.MapFromModel(CreatedBy));
                return MsgResultSuccessInsert();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);

            }
        }
        ////Update Data
        //public Result Update(AttachmentTypeParamModel objParameterSetupParamModel, string CreatedBy)
        //{
        //    try
        //    {
        //        db.Execute("Attachment_Type/usp_Insert_Update_TB_M_AttachmentType", objParameterSetupParamModel.MapFromModel(CreatedBy));
        //        return MsgResultSuccessUpdate();
        //    }
        //    catch (Exception e)
        //    {
        //        throw new Exception(e.Message);

        //    }
        //}
        public Result Update(AttachmentTypeCreateUpdate Model, string EventActor, DateTime EventDate)
        {
            try
            {
                dynamic args = new
                {
                    AttachmentId=Model.AttachmentId,
                    CategoryCode=Model.CategoryCode,
                    CategoryDesc=Model.CategoryDesc,
                    Description=Model.Description,
                    FileType=Model.FileType,
                    FileTypeDesc=Model.FileTypeDesc,
                    Vendor_Group=Model.Vendor_Group,
                    Vendor_GroupDesc=Model.Vendor_GroupDesc,
                    Ebiling=Model.Ebiling,
                    Elvis=Model.Elvis,
                    //EventDate = EventDate.FormatSQLDateTime(),
                    ModifiedBy = EventActor,

                };
                db.Execute("Attachment_Type/usp_Update_TB_M_AttachmentType_ENUM_ENUM", args);
                return MsgResultSuccessUpdate();
            }
            catch (Exception e)
            {
                return MsgResultError(e.Message.ToString());
            }
        }
        //Delete Data
        public Result Delete(AttachmentTypeCreateUpdate model)
        {
            try
            {
                dynamic args = new
                {
                    AttachmentId = model.AttachmentId
                };
                db.Execute("Attachment_Type/usp_Delete_TB_M_AttachmentType", args);
                return MsgResultSuccessDelete();
            }
            catch (Exception e)
            {
                return MsgResultError(e.Message.ToString());
            }
        }

        public List<Attachment> GetXlsReportById(Attachment model /*,int sortBy, string sortDirection*/)
        {
            dynamic args = new
            {
                //Id = model.Id,
                CategoryDesc = model.CategoryDesc,
                Description = model.Description,
                FileTypeDesc = model.FileTypeDesc,
                Vendor_GroupDesc = model.Vendor_GroupDesc,
                SortBy = 1,
                SortDirection = "DESC"
                //SortBy = sortBy,
                //SortDirection =sortDirection
            };
            IEnumerable<Attachment> result = db.Query<Attachment>("Attachment_Type/usp_AttachmentTypeReport", args);
            return result.ToList();
        }
        public List<DropdownAttachmentCategoryModel> GetCategoryDropdownField()
        {
            IEnumerable<DropdownAttachmentCategoryModel> result = db.Query<DropdownAttachmentCategoryModel>("Attachment_Type/usp_GetCategoryField");
            return result.ToList();
        }

        public List<DropdownAttachmentTypeModel> GetDocumentTypeDropdownField()
        {
            IEnumerable<DropdownAttachmentTypeModel> result = db.Query<DropdownAttachmentTypeModel>("Attachment_Type/usp_GetDocumentType");
            return result.ToList();
        }
        //public List<DropdownAttachmentEbilingElvis> GetDropdownAttachmentEbilingElvis()
        //{
        //    IEnumerable<DropdownAttachmentEbilingElvis> result = db.Query<DropdownAttachmentEbilingElvis>("Attachment_Type/usp_GetDocumentEbilElvisDropdown_ENUM");
        //    return result.ToList();
        //}
        public List<DropdownAttachmentTypeModel> GetVendorGroupDropdownField()
        {
            IEnumerable<DropdownAttachmentTypeModel> result = db.Query<DropdownAttachmentTypeModel>("Attachment_Type/usp_GetVendorGroupField");
            return result.ToList();
        }

        public List<AttachmentTypeCreateUpdate> CekDataDuplicatUpdate(AttachmentTypeCreateUpdate model)
        {
            dynamic args = new
            {
                CategoryCode = model.CategoryCode,
                Description = model.Description,
            };
            IEnumerable<AttachmentTypeCreateUpdate> result = db.Query<AttachmentTypeCreateUpdate>("Attachment_Type/Get_Usp_Duplicat_Attachment_Enum", args);

            return result.ToList();
        }
    }
}
