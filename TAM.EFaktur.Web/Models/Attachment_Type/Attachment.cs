﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TAM.EFaktur.Web.Models.Attachment_Type
{
    public class Attachment : BaseModel
    {
        public int AttachmentId { get; set; }
        public string KodeID { get; set; }
        public string CategoryCode { get; set; }
        public string CategoryDesc { get; set; }
        public string Description { get; set; }
        public string FileType { get; set; }
        public string FileTypeDesc { get; set; }
        public string Vendor_Group { get; set; }
        public string Vendor_GroupDesc { get; set; }

        public Boolean Ebiling { get; set; }
        public Boolean Elvis { get; set; }
    }
    public class DropdownAttachmentTypeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class DropdownAttachmentCategoryModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
    //public class DropdownAttachmentEbilingElvis
    //{
    //    public string Ebiling { get; set; }
    //    public string Elvis { get; set; }
    //}
}
