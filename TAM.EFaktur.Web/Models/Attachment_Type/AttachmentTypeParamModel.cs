﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.Attachment_Type
{
    public class AttachmentTypeParamModel
    {
        //public Int64 AttachmentId { get; set; }
        public int AttachmentId { get; set; }
        public string CategoryCode { get; set; }
        public string CategoryDesc { get; set; }
        public string Description { get; set; }
		public string FileType { get; set; }
        public string FileTypeDesc { get; set; }
        public string Vendor_Group { get; set; }
        public string Vendor_GroupDesc { get; set; }
        public Boolean Ebiling { get; set; }
        //public string Ebiling { get; set; }
        public Boolean Elvis { get; set; }
        //public string Elvis { get; set; }
        public string CreatedBy { get; set; }

        public dynamic MapFromModel(string CeatedBy)
        {
            return new
            {
                AttachmentId = this.AttachmentId,
                CategoryCode = this.CategoryCode,
                CategoryDesc = this.CategoryDesc,
                Description = this.Description,
                FileType = this.FileType,
                FileTypeDesc = this.FileTypeDesc,
                Vendor_Group = this.Vendor_Group,
                Vendor_GroupDesc = this.Vendor_GroupDesc,
                Ebiling = this.Ebiling,
                Elvis = this.Elvis,
                CreatedBy = this.CreatedBy
            };
        }
    }
}