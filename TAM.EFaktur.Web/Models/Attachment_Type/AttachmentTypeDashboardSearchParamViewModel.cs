﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.Attachment_Type
{
    public class AttachmentTypeDashboardSearchParamViewModel
    {
        public string CategoryCode { get; set; }
        public string CategoryDesc { get; set; }
        public string Description { get; set; }
        public string FileType { get; set; }
        public string FileTypeFileTypeDesc { get; set; }
        public string Vendor_Group { get; set; }
        public string Vendor_GroupDesc { get; set; }

        public string Ebiling { get; set; }
        public string Elvis { get; set; }
        public string Aplication { get; set; }

    }
}