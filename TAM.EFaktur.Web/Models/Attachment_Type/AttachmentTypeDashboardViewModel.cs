﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.Attachment_Type
{
    public class AttachmentTypeDashboardViewModel
    {
        public int RowNum { get; set; }
        public int AttachmentId { get; set; }
        public string CategoryCode { get; set; }
        public string CategoryDesc { get; set; }
        public string Description { get; set; }
        public string FileType { get; set; }
        public string FileTypeDesc { get; set; }
        public string Vendor_Group { get; set; }
        public string Vendor_GroupDesc { get; set; }
        public bool Ebiling { get; set; }
        public bool Elvis { get; set; }
        public string Aplication { get; set; }
        

    }
}