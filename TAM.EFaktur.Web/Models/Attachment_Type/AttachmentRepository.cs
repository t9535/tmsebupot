﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TAM.EFaktur.Web.Models.Attachment_Type
{
    public class AttachmentRepository : BaseRepository
    {
       #region Singleton
        private AttachmentRepository() { }
        private static AttachmentRepository instance = null;
        public static AttachmentRepository Instance
        {
            get 
            {
                if (instance == null)
                {
                    instance = new AttachmentRepository();
                }
                return instance;
            }
        }
        #endregion

        #region Counter Data
        public int Count(string ConfigName, string configValue)
        {
            dynamic args = new
            {
                ConfigName = ConfigName,
                configValue = configValue,
                
            };

            return db.SingleOrDefault<int>("Config/countApplications", args); 
        }
        #endregion

        #region Processing Data

        public List<Attachment> GetList(string ConfigKey,string ConfigValue)
        {
            dynamic args = new
            {
                ConfigKey = ConfigKey,
                ConfigValue = ConfigValue
               
            };
            IEnumerable<Attachment> result = db.Query<Attachment>("Config/nosqlyet", args);
            return result.ToList();
        }

        public Attachment GetById(Guid Id)
        {
            return db.SingleOrDefault<Attachment>("Config/usp_GetConfigByID", new { Id = Id });
        }

        //public Result Save(Attachment objConfig, DateTime EventDate, string EventActor)
        //{
        //    try
        //    {
        //        dynamic args = new
        //        {
        //            Id= objConfig.Id,
        //            CategoryCode = objConfig.CategoryCode,
        //            Description = objConfig.Description,
        //            FileType = objConfig.FileType,
        //            Vendor_Group = objConfig.Vendor_Group,
        //            RowStatus           = objConfig.RowStatus,
        //            EventDate           = EventDate,
        //            EventActor          = EventActor                
        //        };
        //        db.Execute("Config/usp_SaveConfig", args);
        //        return MsgResultSuccessInsert();
        //    }
        //    catch (Exception e)
        //    {
        //        return MsgResultError(e.Message.ToString());
        //    }
        //}

        //public Result Delete(Guid Id)
        //{
        //    try
        //    {
        //        db.Execute("Config/usp_DeleteConfigByID", new { Id = Id });
        //        return MsgResultSuccessDelete();
        //    }
        //    catch (Exception e)
        //    {
        //        return MsgResultError(e.Message.ToString());
        //    }
        //}

        
        #endregion

    }
}
