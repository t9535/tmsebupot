﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.Group_Nominative
{
    public class GroupNominativeDashboardViewModel
    {
        public int RowNum { get; set; }
        public int NominativeId { get; set; }
        public string CategoryCode { get; set; }
        public string CategoryName { get; set; }
        public string GLAccount { get; set; }
        public string DescriptionTransaction { get; set; }
        public string Type { get; set; }

    }
}