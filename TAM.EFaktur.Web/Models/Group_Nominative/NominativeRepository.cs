﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TAM.EFaktur.Web.Models.Group_Nominative
{
    public class NominativeRepository : BaseRepository
    {
       #region Singleton
        private NominativeRepository() { }
        private static NominativeRepository instance = null;
        public static NominativeRepository Instance
        {
            get 
            {
                if (instance == null)
                {
                    instance = new NominativeRepository();
                }
                return instance;
            }
        }
        #endregion

        #region Counter Data
        public int Count(string ConfigName, string configValue)
        {
            dynamic args = new
            {
                ConfigName = ConfigName,
                configValue = configValue,
                
            };

            return db.SingleOrDefault<int>("Config/countApplications", args); 
        }
        #endregion

        #region Processing Data

        public List<Nominative> GetIdBySearchParam(GLAccountSearchParamViewModel model)
        {
            IEnumerable<Nominative> result = db.Query<Nominative>("Group_Nominative/usp_GetGLAccountIdBySearchParam", model.MapFromModel());
            return result.ToList();
        }

        public List<Nominative> GetXlsReportById(Nominative model)
        {
            dynamic args = new
            {
                //Id = model.Id,
                CategoryCode=model.CategoryCode,
                //CategoryName = model.CategoryName,
                GLAccount = model.GLAccount,
                DescriptionTransaction = model.DescriptionTransaction,
                Type = model.Type,
                SortBy = 1,
                SortDirection = "DESC"
            };
            IEnumerable<Nominative> result = db.Query<Nominative>("Group_Nominative/usp_GetGLAccountReportById", args);
            return result.ToList();
        }
        public List<Nominative> GetList(string CategoryCode, string GLAccount, string DescriptionTransaction, string Type)
        {
            dynamic args = new
            {
                CategoryCode = CategoryCode,
                GLAccount = GLAccount,
                DescriptionTransaction = DescriptionTransaction,
                Type = Type
            };
            IEnumerable<Nominative> result = db.Query<Nominative>("Config/nosqlyet", args);
            return result.ToList();
        }

        public Nominative GetById(Guid Id)
        {
            return db.SingleOrDefault<Nominative>("Config/usp_GetConfigByID", new { Id = Id });
        }

        public Result Save(Nominative objNominative, DateTime EventDate, string EventActor)
        {
            try
            {
                dynamic args = new
                {
                    Id= objNominative.Id,
                    CategoryCode = objNominative.CategoryCode,
                    GLAccount = objNominative.GLAccount,
                    DescriptionTransaction = objNominative.DescriptionTransaction,
                    Type = objNominative.Type,
                    RowStatus           = objNominative.RowStatus,
                    EventDate           = EventDate,
                    EventActor          = EventActor                
                };
                db.Execute("Config/usp_SaveConfig", args);
                return MsgResultSuccessInsert();
            }
            catch (Exception e)
            {
                return MsgResultError(e.Message.ToString());
            }
        }

        public Result Delete(Guid Id)
        {
            try
            {
                db.Execute("Config/usp_DeleteConfigByID", new { Id = Id });
                return MsgResultSuccessDelete();
            }
            catch (Exception e)
            {
                return MsgResultError(e.Message.ToString());
            }
        }

        public List<KategoriDropdownViewModel> GetKategoriDropdown()
        {
            IEnumerable<KategoriDropdownViewModel> result = db.Query<KategoriDropdownViewModel>("Group_Nominative/usp_GetGLAccountDropdownList");
            return result.ToList();
        }

        public List<KategoriDropdownViewModel> GetGLAccountTypeDropdownField()
        {
            IEnumerable<KategoriDropdownViewModel> result = db.Query<KategoriDropdownViewModel>("Group_Nominative/usp_GetTypeDropdownList");
            return result.ToList();
        }
        #endregion

    }
}
