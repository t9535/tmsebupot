﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.Group_Nominative
{
    public class KategoriDropdownViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}