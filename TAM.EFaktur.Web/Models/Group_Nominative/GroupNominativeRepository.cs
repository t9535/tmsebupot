﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TAM.EFaktur.Web.Models.Group_Nominative
{
    public class GroupNominativeRepository : BaseRepository
    {
        #region Singletonh
        private GroupNominativeRepository() { }
        private static GroupNominativeRepository instance = null;
        public static GroupNominativeRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new GroupNominativeRepository();
                }
                return instance;
            }
        }
        #endregion

        //#region Counter Data
        ////public int Count(string ConfigName, string configValue)
        //public int Count(VATInDashboardSearchParamViewModel model)
        //{
        //    dynamic args = new
        //    {
        //        ConfigName = ConfigName,
        //        configValue = configValue,

        //    };

        //    return db.SingleOrDefault<int>("Config/countApplications", args); 
        //}
        //#endregion

        #region Counter Data
        public int Count(GroupNominativeDashboardSearchParamViewModel model)
        {
            dynamic args = new
            {
                CategoryCode = model.CategoryCode,
                GLAccount = model.GLAccount,
                DescriptionTransaction = model.DescriptionTransaction,
                Type = model.Type
            };

            return db.SingleOrDefault<int>("Group_Nominative/usp_CountGroupNominativeListDashboard", args);
        }
        #endregion
        #region Processing Data

        public List<GroupNominativeDashboardViewModel> GetList(GroupNominativeDashboardSearchParamViewModel model, int SortBy, string SortDirection, int FromNumber, int ToNumber)
        {
            dynamic args = new
            {
                NominativeId=model.NominativeId,
                CategoryCode = model.CategoryCode,
                GLAccount = model.GLAccount,
                DescriptionTransaction = model.DescriptionTransaction,
                Type = model.Type,
                SortBy = SortBy,
                SortDirection = SortDirection,
                FromNumber = FromNumber,
                ToNumber = ToNumber
            };
            IEnumerable<GroupNominativeDashboardViewModel> result = db.Query<GroupNominativeDashboardViewModel>("Group_Nominative/usp_GetNominativeListDashboard", args);
            return result.ToList();
        }
        #endregion

        public Nominative GetById(int NominativeId)
        {
            return db.SingleOrDefault<Nominative>("Group_Nominative/usp_GetNominativeDataByID", new { NominativeId = NominativeId });
        }

        public Nominative GetByConfigKey(String NominativeKey)
        {
            return db.SingleOrDefault<Nominative>("Group_Nominative/usp_GetNominativeDataByNominativeKey", new { NominativeKey = NominativeKey });
        }
 
        //insert Data
        public Result Insert(GroupNominativeCreateUpdate model, string EventActor, DateTime EventDate)
        {
            try
            {
                dynamic args = new
                {
                    CategoryCode = model.CategoryCode,
                    GLAccount = model.GLAccount,
                    DescriptionTransaction = model.DescriptionTransaction,
                    Type = model.Type,
                    EventDate = EventDate.FormatSQLDateTime(),
                    EventActor = EventActor,
                    Message = ""
                };
                db.Execute("Group_Nominative/usp_Insert_TB_M_Nominative_ENUM", args);
                return MsgResultSuccessInsert();
            }
            catch (Exception e)
            {
                return MsgResultError(e.Message.ToString());
            }
        }

        //Update Data
        public Result Update(GroupNominativeCreateUpdate model, string EventActor, DateTime EventDate)
        {
            try
            {
                dynamic args = new
                {
                    NominativeId = model.NominativeId,
                    CategoryCode = model.CategoryCode,
                    GLAccount = model.GLAccount,
                    DescriptionTransaction = model.DescriptionTransaction,
                    Type = model.Type,
                    EventDate = EventDate.FormatSQLDateTime(),
                    EventActor = EventActor,

                };
                db.Execute("Group_Nominative/usp_Update_TB_M_Nominative_ENUM", args);
                return MsgResultSuccessUpdate();
            }
            catch (Exception e)
            {
                return MsgResultError(e.Message.ToString());
            }
        }

        //Delete Data
        public Result Delete(GroupNominativeCreateUpdate model)
        {
            try
            {
                dynamic args = new
                {
                    NominativeId = model.NominativeId

                };
                db.Execute("Group_Nominative/usp_Delete_TB_M_Nominative_ENUM", args);
                return MsgResultSuccessDelete();
            }
            catch (Exception e)
            {
                return MsgResultError(e.Message.ToString());
            }
        }
        //
        public Result Save(Nominative objConfig, DateTime EventDate, string EventActor)
        {
            try
            {
                dynamic args = new
                {
                    Id = objConfig.Id,
                    CategoryCode = objConfig.CategoryCode,
                    ConfigGLAccountValue = objConfig.GLAccount,
                    DescriptionTransaction = objConfig.DescriptionTransaction,
                    Type = objConfig.Type,
                    RowStatus = objConfig.RowStatus,
                    EventDate = EventDate,
                    EventActor = EventActor
                };
                db.Execute("Nominative/usp_SaveConfig", args);
                return MsgResultSuccessInsert();
            }
            catch (Exception e)
            {
                return MsgResultError(e.Message.ToString());
            }
        }

        public Result Delete(Guid Id)
        {
            try
            {
                db.Execute("Nominative/usp_DeleteConfigByID", new { Id = Id });
                return MsgResultSuccessDelete();
            }
            catch (Exception e)
            {
                return MsgResultError(e.Message.ToString());
            }
        }
        public List<GroupNominativeCreateUpdate> CekDataDuplicatUpdate(GroupNominativeCreateUpdate model)
        {
            dynamic args = new
            {
                CategoryCode = model.CategoryCode,
                GLAccount = model.GLAccount,
            };
            IEnumerable<GroupNominativeCreateUpdate> result = db.Query<GroupNominativeCreateUpdate>("Group_Nominative/Get_Usp_Duplicat_GLAccount_Enum", args);
            
            return result.ToList();
        }
        //#endregion

    }
}
