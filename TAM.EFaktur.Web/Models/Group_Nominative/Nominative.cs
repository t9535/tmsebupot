﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TAM.EFaktur.Web.Models.Group_Nominative
{
    public class Nominative : BaseModel
    {
        public int NominativeId { get; set; }
        public string CategoryCode { get; set; }
        public string CategoryName { get; set; }
        public string GLAccount { get; set; }
        public string DescriptionTransaction { get; set; }
        public string Type { get; set; }

    }
}
