﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TAM.EFaktur.Web.Models.Group_Nominative
{
    public class GLAccountSearchParamViewModel
    {
		public string CategoryCode { get; set; }
		public string GLAccount { get; set; }
		public string DescriptionTransaction { get; set; }
		public string Type { get; set; }
		
		public int page { get; set; }
		public int size { get; set; }

        public dynamic MapFromModel(int SortBy = 0, string SortDirection = "ASC", int FromNumber = 1, int ToNumber = 10)
        {
            return new
            {
                CategoryCode = this.CategoryCode,
                GLAccount = this.GLAccount,
                DescriptionTransaction = this.DescriptionTransaction,
                Type = this.Type,
                SortBy = SortBy,
                SortDirection = SortDirection,
                FromNumber = FromNumber,
                ToNumber = ToNumber
            };
        }
    }
}