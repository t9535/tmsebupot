﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using TAM.EFaktur.Web.Controllers;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
 
namespace TAM.EFaktur.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    #region Original Generated Code by Microsoft
    //public class MvcApplication : System.Web.HttpApplication
    //{
    //    protected void Application_Start()
    //    {
    //        AreaRegistration.RegisterAllAreas();

    //        WebApiConfig.Register(GlobalConfiguration.Configuration);
    //        FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
    //        RouteConfig.RegisterRoutes(RouteTable.Routes);
    //        BundleConfig.RegisterBundles(BundleTable.Bundles);
    //    }
    //}
    #endregion

    #region Toyota Framework Custom ASAX Code
    public class MvcApplication : WebApplication
    {
        public MvcApplication()
        {
            ApplicationSettings.Instance.Name = "TAM - Tax Management System";
            ApplicationSettings.Instance.Alias = "eFaktur";
            ApplicationSettings.Instance.OwnerName = "PT.Toyota Astra Motor";
            ApplicationSettings.Instance.OwnerAlias = "PT.TAM";
            ApplicationSettings.Instance.OwnerEmail = "tdk@toyota.co.id";
            ApplicationSettings.Instance.Menu.Enabled = true;
            ApplicationSettings.Instance.Runtime.HomeController = "Home";
            //ApplicationSettings.Instance.Security.LoginController = "Login";
            ApplicationSettings.Instance.Security.EnableAuthentication = true;
            ApplicationSettings.Instance.Security.IgnoreAuthorization = true; // Jika di SC nya sudah ada Role, boleh di false, otherwise di true saja
            //ApplicationSettings.Instance.Security.UseCustomAuthenticationRule = false;
            ApplicationSettings.Instance.Security.EnableSingleSignOn = false; // RYAN: if this is set to TRUE, will cause infinite loop!

            //ApplicationSettings.Instance.Security.SimulateAuthenticatedSession = true;
            //ApplicationSettings.Instance.Security.SimulatedAuthenticatedUser = new User()
            //{
            //    Username = "user.test",
            //    Password = "toyota",
            //    FirstName = "User",
            //    LastName = "Test"
            //};
        }

        protected override void Startup()
        {
            ProviderRegistry.Instance.Register<IUserProvider>(typeof(UserProvider), DatabaseManager.Instance, "SecurityCenter");
        }

        //protected void Application_Error(object sender, EventArgs e)
        //{
        //    var exception = Server.GetLastError();

        //    // Log exception to database if you want to.

        //    // Process 404 HTTP errors
        //    var httpException = exception as HttpException;
        //    if (httpException != null && httpException.GetHttpCode() == 404)
        //    {
        //        Response.Clear();
        //        Server.ClearError();
        //        Response.TrySkipIisCustomErrors = true;

        //        IController controller = new ErrorController();

        //        var routeData = new RouteData();
        //        routeData.Values.Add("controller", "Error");
        //        routeData.Values.Add("action", "NotFound");

        //        var requestContext = new RequestContext(
        //             new HttpContextWrapper(Context), routeData);
        //        controller.Execute(requestContext);
        //    }
        //}

        //for automatic end request every call database
        protected void Application_EndRequest(object sender, EventArgs e)
        {
            if ((HttpContext.Current.Items != null) && (HttpContext.Current.Items[TAM.EFaktur.Web.Models.BaseRepository.propDBKey] != null))
            {
                IDisposable d = HttpContext.Current.Items[TAM.EFaktur.Web.Models.BaseRepository.propDBKey] as IDisposable;
                if (d != null)
                    d.Dispose();
            }
        }
    }
    #endregion
}