﻿var DEBUG = true;
var currencyFormat = "0,0.00";
var charMaskNoFaktur = "999.999-99.99999999";
var charMaskNPWP = "99.999.999.9-999.999";
var charMaskcurrency = "99.999.999";

function defaultFor(arg, val) {
    return typeof arg !== 'undefined' ? arg : val;
}

function checkLoginTimeout(r) {
    var timeout = ($(r).find('form#login-form').length > 0);
    if (timeout) {
        window.location.href = "/Login";
    }
    if (DEBUG) {
        console.log("session timeout:" + timeout);
    }
    return timeout;
}

function getDropdownData(URL, elementId, selectedValue) {
    $.ajax({
        type: "GET",
        url: URL,
        success: function (result) {
            if (result != null && result.length > 0) {
                $.each(result, function (i, item) {
                    var row = "<option value='" + item.Id + "'>"; //kolom ViewModel : Id
                    row += item.Name; //kolom ViewModel : Name
                    row += "</option>";
                    $('#' + elementId).append(row);
                });
                var lookupIdElement = '<input type="hidden" class="form-control" id="lookupId' + elementId + '" />';
                var lookupNameElement = '<input type="hidden" class="form-control" id="lookupName' + elementId + '" />';

                $('#' + elementId).parent().append(lookupIdElement).append(lookupNameElement);

                //set selectedValue
                defaultFor(selectedValue, "");
                $('#' + elementId).val(selectedValue);
                $('#' + elementId).multiselect({
                    dropLeft: true,
                    buttonWidth: '100%',
                    enableFiltering: true,
                    enableCaseInsensitiveFiltering: true,
                    onChange: function (element, checked) {
                        var brands = $('#' + elementId + ' option:selected');
                        var selectedId = [];
                        var selectedName = [];
                        $(brands).each(function (index, brand) {
                            selectedName.push([$(this).text()]);
                            selectedId.push([$(this).val()]);
                            if (DEBUG) {
                                console.log(selectedId[index]);
                                console.log(selectedName[index]);
                            }
                        });

                        $('#lookupId' + elementId).val(selectedId.join(';'));
                        $('#lookupName' + elementId).val(selectedName.join(';'));
                    }
                });

                if (selectedValue != "") {//untuk mengatasi masalah jika edit data dan langsung melakukan save data
                    var brands = $('#' + elementId + ' option:selected');
                    var selectedId = [];
                    var selectedName = [];
                    $(brands).each(function (index, brand) {
                        selectedName.push([$(this).text()]);
                        selectedId.push([$(this).val()]);
                        if (DEBUG) {
                            console.log(selectedId[index]);
                            console.log(selectedName[index]);
                        }
                    });

                    $('#lookupId' + elementId).val(selectedId.join(';'));
                    $('#lookupName' + elementId).val(selectedName.join(';'));
                }
            }
            //else {
            //msgError("No dropdown data found");
            //} //20181012
            if (DEBUG) {
                console.log('Dropdown Data Loaded');
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            if (DEBUG) {
                console.log("error: " + errorThrown);
            }
            msgError("An error has occured");
        },
    });
}

function getDropdownDataByText(URL, elementId, selectedValue) {
    $.ajax({
        type: "GET",
        url: URL,
        success: function (result) {
            if (result != null && result.length > 0) {
                $.each(result, function (i, item) {
                    var row = "<option value='" + item.Id.replace(' ', '-') + "'>"; //kolom ViewModel : Id
                    row += item.Name; //kolom ViewModel : Name
                    row += "</option>";
                    $('#' + elementId).append(row);
                });
                var lookupIdElement = '<input type="hidden" class="form-control" id="lookupId' + elementId + '" />';
                var lookupNameElement = '<input type="hidden" class="form-control" id="lookupName' + elementId + '" />';

                $('#' + elementId).parent().append(lookupIdElement).append(lookupNameElement);

                //set selectedValue
                defaultFor(selectedValue, "");
                $('#' + elementId).val(selectedValue);
                $('#' + elementId).multiselect({
                    dropLeft: true,
                    buttonWidth: '100%',
                    enableFiltering: true,
                    enableCaseInsensitiveFiltering: true,
                    onChange: function (element, checked) {
                        var brands = $('#' + elementId + ' option:selected');
                        var selectedId = [];
                        var selectedName = [];
                        $(brands).each(function (index, brand) {
                            selectedName.push([$(this).text()]);
                            selectedId.push([$(this).val()]);
                            if (DEBUG) {
                                console.log(selectedId[index]);
                                console.log(selectedName[index]);
                            }
                        });

                        $('#lookupId' + elementId).val(selectedId.join(','));
                        $('#lookupName' + elementId).val(selectedName.join(','));
                    }
                });

                if (selectedValue != "") { //untuk mengatasi masalah jika edit data dan langsung melakukan save data
                    var brands = $('#' + elementId + ' option:selected');
                    var selectedId = [];
                    var selectedName = [];
                    $(brands).each(function (index, brand) {
                        selectedName.push([$(this).text()]);
                        selectedId.push([$(this).val()]);
                        if (DEBUG) {
                            console.log(selectedId[index]);
                            console.log(selectedName[index]);
                        }
                    });

                    $('#lookupId' + elementId).val(selectedId.join(','));
                    $('#lookupName' + elementId).val(selectedName.join(','));
                }

            }
            //else {
            //msgError("No dropdown data found");
            //} //20181012
            if (DEBUG) {
                console.log('Dropdown Data Loaded');
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            if (DEBUG) {
                console.log("error: " + errorThrown);
            }
            msgError("An error has occured");
        },
    });
}

function getDropdownlist(URL, elementId) {
    $.ajax({
        type: "GET",
        url: URL,
        success: function (response) {

            if (response.length > 0) {
                $('#' + elementId).html('');
                var options = '';
                options += '<option value="">--Select--</option>';
                for (var i = 0; i < response.length; i++) {
                    options += '<option value="' + response[i].Id + '">' + response[i].Name + '</option>';
                }
                $('#' + elementId).append(options);
            }
            //else {
            //msgError("No dropdown data found");
            //} //20181012
            if (DEBUG) {
                console.log('Dropdown Data Loaded');
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            if (DEBUG) {
                console.log("error: " + errorThrown);
            }
            msgError("An error has occured");
        },
    });
}

function getDropdownlistVal(URL, elementId, val) {
    $.ajax({
        type: "GET",
        url: URL,
        success: function (response) {

            if (response.length > 0) {
                $('#' + elementId).html('');
                var options = '';
                options += '<option value="">--Select--</option>';
                for (var i = 0; i < response.length; i++) {
                    options += '<option value="' + response[i].Id + '">' + response[i].Name + '</option>';
                }
                $('#' + elementId).append(options);
            }
            //else {
            //msgError("No dropdown data found");
            //} //20181012
            $('#' + elementId).val(val);
            if (DEBUG) {
                console.log('Dropdown Data Loaded');
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            if (DEBUG) {
                console.log("error: " + errorThrown);
            }
            msgError("An error has occured");
        },
    });
}


$(function () {
    // Initialize modal dialog
    // attach modal-container bootstrap attributes to links with .modal-link class.
    // when a link is clicked with these attributes, bootstrap will display the href content in a modal dialog.
    $('body').on('click', '.modal-link', function (e) {
        e.preventDefault();
        $(this).attr('data-target', '#modal-container');
        $(this).attr('data-toggle', 'modal');
    });
    // Attach listener to .modal-close-btn's so that when the button is pressed the modal dialog disappears
    $('body').on('click', '.modal-close-btn', function () {
        $('#modal-container').modal('hide');
    });
    //clear modal cache, so that new content can be loaded
    $('#modal-container').on('hidden.bs.modal', function () {
        $(this).removeData('bs.modal');
    });
    $('#CancelModal').on('click', function () {
        return false;
    });
});

$(document).ready(function () {
    //convert HTML element to a specific date/time format
    var dateNow = new Date();

    $("._datetimepicker").datetimepicker({
    }); // _datetimepicker class into full datetime type

    $("._datepicker").datetimepicker({ // _datepicker class into date type
        format: 'DD-MM-YYYY'
    });

    $("._datepicker-default").datetimepicker({ // _datepicker class into date type
        format: 'DD-MM-YYYY',
        defaultDate: dateNow
    });

    $("._timepicker").datetimepicker({ // _timepicker class into time type
        format: 'LT'
    });

    $("._timepicker-default").datetimepicker({ // _datepicker class into date type
        format: 'LT',
        defaultDate: dateNow
    });

    //masking format Nomor Faktur
    $("._mask-nofaktur").mask(charMaskNoFaktur);

    ///masking format NPWP
    $("._mask-npwp").mask(charMaskNPWP);

    $("._mask-currency").mask(currencyFormat);
});

function getDropdownColor(URL, elementId, selectedValue) {
    $.ajax({
        type: "GET",
        url: URL,
        success: function (response) {

            if (response.length > 0) {
                $('#' + elementId).html('');
                var options = '';

                for (var i = 0; i < response.length; i++) {
                    var _colorValur = '';
                    let result = ntc.name(response[i].Id);
                    if (response[i].Id == 'No Color' || response[i].Id == 'ALL') {
                        _colorValur = response[i].Id;
                    }
                    else {
                        _colorValur = result[1];
                    }

                    options += "<li><a href='#'><span style='width:15px;height:15px;background-color:" + response[i].Id + ";display:inline-block;'></span><input type='hidden'  id='" + response[i].Id + "' value='" + response[i].Id + "' /> " + _colorValur + "</a></li>";
                }

                $('#' + elementId).append(options);
            }
            //else {
            //msgError("No dropdown data found");
            //} //20181012
            if (DEBUG) {
                console.log('Dropdown Data Loaded');
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            if (DEBUG) {
                console.log("error: " + errorThrown);
            }
            msgError("An error has occured");
        },
    });
}



//Format Currency
$("input[data-type='currency']").on({
    keyup: function () {
        formatCurrency($(this));
    },
    blur: function () {
        formatCurrency($(this), "blur");
    }
});

function formatNumber(n) {
    // format number 1000000 to 1,234,567
    return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}


function getTax(URL, fromdate) {
    var keyValue = "";
    $.ajax({
        async: false,
        type: "GET",
        url: URL,
        data: {
            fromdate: fromdate
        },
        success: function (response) {
            console.log("error test: " + response);
            keyValue = response;
          
            if (DEBUG) {
                console.log('Get Data Loaded');
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            if (DEBUG) {
                console.log("error: " + errorThrown);
            }
            msgError("An error has occured");
        },
    });
    // format number 1000000 to 1,234,567
    return keyValue;
}
function someFunction(data) {
    return data;
}
function formatCurrency(input, blur) {
    // appends $ to value, validates decimal side
    // and puts cursor back in right position.

    // get input value
    var input_val = input.val();

    // don't validate empty input
    if (input_val === "") { return; }

    // original length
    var original_len = input_val.length;

    // initial caret position
    var caret_pos = input.prop("selectionStart");

    // check for decimal
    if (input_val.indexOf(".") >= 0) {

        // get position of first decimal
        // this prevents multiple decimals from
        // being entered
        var decimal_pos = input_val.indexOf(".");

        // split number by decimal point
        var left_side = input_val.substring(0, decimal_pos);
        var right_side = input_val.substring(decimal_pos);

        // add commas to left side of number
        left_side = formatNumber(left_side);

        // validate right side
        right_side = formatNumber(right_side);

        // On blur make sure 2 numbers after decimal
        if (blur === "blur") {
            right_side += "00";
        }

        // Limit decimal to only 2 digits
        right_side = right_side.substring(0, 2);

        // join number by .
        input_val = left_side;// + "." + right_side;//"$" + left_side + "." + right_side;

    } else {
        // no decimal entered
        // add commas to number
        // remove all non-digits
        input_val = formatNumber(input_val);
        input_val = input_val;//"$" + input_val;

        //// final formatting
        //if (blur === "blur") {
        //    input_val += ".00";
        //}
    }

    // send updated string to input
    input.val(input_val);

    // put caret back in the right position
    var updated_len = input_val.length;
    caret_pos = updated_len - original_len + caret_pos;
    input[0].setSelectionRange(caret_pos, caret_pos);
}