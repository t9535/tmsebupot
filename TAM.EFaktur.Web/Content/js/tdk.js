$(document).ready(function () {

    /** start - event for tree grid with table grid **/
    initTableToggle();
    $(".rowtoggle").click(function () {
        $(this).toggleClass("rowexpand", "");
        $(this).toggleClass("rowcollapse", "");

        var p1 = $(this).parentsUntil("tr");
        var p2 = $(p1).parent()
        $(p2).toggleClass("tb-collapse", "");
        $(p2).toggleClass("tb-expand", "");
        setTableToogle(p2);
    });
    /** end - event for tree grid with table grid **/


    /** start - check all checkbox **/
    //$("#checkall").click(function () {
    //    console.log("check all detected");
    //    $(".check").prop("checked", $("#checkall").is(":checked"));
    //});
    /** end - check all checkbox **/

});

function setTableToogle(p) 
{
    $(".rowexpand").html('<i class="fa fa-minus-square-o fa-fw"></i> ');
    $(".rowcollapse").html('<i class="fa fa-plus-square-o fa-fw"></i> ');

    $(p).next().toggleClass("hide", "");
}

function initTableToggle() 
{
    $(".tb-expand td:first-child").prepend('<span class="rowtoggle rowexpand"><i class="fa fa-minus-square-o fa-fw"></i></span> ');
    $(".tb-collapse td:first-child").prepend('<span class="rowtoggle rowcollapse"><i class="fa fa-plus-square-o fa-fw"></i></span> ');

    var header = $(".tb-collapse");
    header.each(function () {
        $(this).next().toggleClass("hide", "");
    });

}