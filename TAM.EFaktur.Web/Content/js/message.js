﻿function msgOk(s) {
    $.bootstrapGrowl(s, { type: 'success'
     , allowdismiss: 'true'
      , delay: 5000
     , align: 'center'
    });
}

function msgError(s) {
    $.bootstrapGrowl(s, { type: 'danger'
    , allowdismiss: 'true'
    , delay: 5000
    , align: 'center'
    });
}

function msgInfo(s) {
    $.bootstrapGrowl(s, { type: 'info'
    , allowdismiss: 'true'
    , delay: 5000
    , align: 'center'
    });
}

function msgWarning(s) {
    $.bootstrapGrowl(s, { type: 'warning'
    , allowdismiss: 'true'
    , delay: 5000
    , align: 'center'
    });
}