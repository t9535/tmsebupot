﻿Exec usp_Insert_TB_R_WHTEbupot 
    @TaxArticle ,
    @eBupotNumber ,
    @StatusInvoice  ,
    @SupplierName  ,
    @OriginalSupplierName ,
    @SupplierNPWP ,
    @SupplierAddress ,
    @WHTAmount ,
    @WHTTarif ,
    @WHTTaxAmount ,
    @Description  ,
    @InvoiceDate ,
    @NomorInvoice ,
    @eBupotReferenceNo  ,
    @TaxType ,
    @WithholdingTaxDate  ,
    @TaxPeriodMonth  ,
    @TaxPeriodYear  ,
    @AdditionalInfo  ,
    @PVNumber  ,
    @PVCreatedBy  ,
    @SAPDocNumber  ,
    @PostingDate  ,
    @ReceiveFileDate  ,
    @ApprovalStatus  ,
    @ApprovalDesc  ,
    @GLAccount ,
	@CreatedBy ,
    @CreatedOn  ,
    @RowStatus 