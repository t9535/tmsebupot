﻿Exec usp_CountParameterSetupListDashboard
	
	@ParameterCode,
	@ParameterName,
	@ParameterValue,
	@FromFieldName,
	@ParameterCategoryName,
	@Color,
	@Icon,
	@Note1,
	@Note2,
	@Note3,
	@Note4,
	@Note5,
	@TransactionCode,
	@BusinessUnit,
	@ModuleName,
	@ParameterDeadlineOperator,
	@ParameterDeadline,
	@ColorStatus