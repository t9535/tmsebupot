﻿exec [usp_Validation_TB_R_Dashboard_Nominative]
 @NominativeType,
    @StatusInvoice,
    @PVType,
    @AccruedYearEndStatus,
    @DescriptionTransaction,
    @GLAccount,
    @TaxInvoiceNo,
    @WHTArticle,
    @NominativeStatus 