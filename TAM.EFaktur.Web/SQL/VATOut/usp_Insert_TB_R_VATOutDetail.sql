﻿Exec usp_Insert_TB_R_VATOutDetail @Id ,
	@VATOutId ,
	@KodeObjek ,
	@NamaObjek ,
	@HargaSatuan ,
	@JumlahBarang ,
	@HargaTotal ,
	@Diskon ,
	@DPP ,
	@PPN ,
	@TarifPPNBM ,
	@PPNBM ,
	@Status ,
	@CreatedOn ,
	@CreatedBy ,
	@RowStatus,
	@FrameNo,
	@EngineNo,
	@ModelType,
	@PPNBMLINI