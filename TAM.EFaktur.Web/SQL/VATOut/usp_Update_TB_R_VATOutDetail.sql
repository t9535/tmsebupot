﻿Exec usp_Update_TB_R_VATOutDetail @Id ,
	@VATOutId ,
	@KodeObjek ,
	@NamaObjek ,
	@HargaSatuan ,
	@JumlahBarang ,
	@HargaTotal ,
	@Diskon ,
	@DPP ,
	@PPN ,
	@TarifPPNBM ,
	@PPNBM ,
	@Status ,
	@ModifiedOn ,
	@ModifiedBy  ,
	@RowStatus
	