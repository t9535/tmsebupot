﻿USE [TAM_EFAKTUR]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetVATOutSalesReturnCSVById]    Script Date: 30/09/2020 16:04:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[usp_GetVATOutSalesReturnCSVById] 
	@VATOutSalesReturnId VARCHAR(MAX)
AS
BEGIN
	SELECT ColumnData 
	INTO #tempSelectedVATOutSalesReturnId 
	FROM uf_SplitString(@VATOutSalesReturnId, ';')

	--SELECT 
	--	--ROW_NUMBER() OVER (ORDER BY VO.TanggalFaktur ASC) AS RowNum,
	--	RVO.Id AS VATOutSalesReturnId,
	--	'RK' as Field1,
	--	dbo.uf_Only_Numbers (RVO.NPWPCustomer) as Field2,
	--	RVO.NamaCustomer as Field3,
	--	KJT.KDJenisTransaksiCode as Field4,
	--	RVO.FGPengganti as Field5,
	--	RVO.NomorFaktur as Field6,
	--	ISNULL(CONVERT(VARCHAR, RVO.TanggalFaktur, 103), '') AS Field7,
	--	RVO.NomorDokumenRetur as Field8,
	--	ISNULL(CONVERT(VARCHAR, RVO.TanggalRetur, 103), '') AS Field9,
	--	ISNULL(CONVERT(VARCHAR, RVO.MasaPajakRetur, 103), '') as Field10, 
	--	ISNULL(CONVERT(VARCHAR, RVO.TahunPajakRetur, 103), '') as Field11,
	--	CONVERT(VARCHAR, RVO.JumlahReturDPP)as Field12,
	--	CONVERT(VARCHAR, RVO.JumlahReturPPN)as Field13,
	--	CONVERT(VARCHAR, RVO.JumlahReturPPNBM)as Field14
	--	FROM [dbo].[TB_R_ReturnVATOut] RVO
	--	LEFT JOIN dbo.TB_M_KDJenisTransaksi KJT ON KJT.Id=RVO.KDJenisTransaksiID
	--	WHERE RVO.Id IN (SELECT ColumnData From #tempSelectedVATOutSalesReturnId ) and RVO.ApprovalStatus <> 'Reject'
	--DROP TABLE #tempSelectedVATOutSalesReturnId

	SELECT 
		--ROW_NUMBER() OVER (ORDER BY VO.TanggalFaktur ASC) AS RowNum,
		RVO.Id AS VATOutSalesReturnId,
		'RK' as Field1,
		dbo.uf_Only_Numbers (RVO.NPWPCustomer) as Field2,
		RVO.NamaCustomer as Field3,
		KJT.KDJenisTransaksiCode as Field4,
		RVO.FGPengganti as Field5,
		RVO.NomorFaktur as Field6,
		ISNULL(CONVERT(VARCHAR, RVO.TanggalFaktur, 103), '') AS Field7,
		RVO.NomorDokumenRetur as Field8,
		ISNULL(CONVERT(VARCHAR, RVO.TanggalRetur, 103), '') AS Field9,
		ISNULL(CONVERT(VARCHAR, RVO.MasaPajakRetur, 103), '') as Field10, 
		ISNULL(CONVERT(VARCHAR, RVO.TahunPajakRetur, 103), '') as Field11,
		CONVERT(VARCHAR, RVO.JumlahReturDPP)as Field12,
		CONVERT(VARCHAR, RVO.JumlahReturPPN)as Field13,
		CONVERT(VARCHAR, RVO.JumlahReturPPNBM)as Field14,
		RVOD.NamaObjek as Field15,
		RVOD.HargaSatuan as Field16,
		RVOD.JumlahBarang as Field17,
		RVOD.HargaTotal as Field18
		FROM [dbo].[TB_R_ReturnVATOut] RVO
		LEFT JOIN dbo.TB_R_VATOutDetail RVOD ON RVOD.[VATOutId]= RVO.Id
		LEFT JOIN dbo.TB_M_KDJenisTransaksi KJT ON KJT.Id=RVO.KDJenisTransaksiID
		WHERE RVO.Id IN (SELECT ColumnData From #tempSelectedVATOutSalesReturnId ) and RVO.ApprovalStatus <> 'Reject'
	DROP TABLE #tempSelectedVATOutSalesReturnId
END


