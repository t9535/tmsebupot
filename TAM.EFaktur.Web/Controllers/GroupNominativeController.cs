﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using TAM.EFaktur.Web.Models;
using TAM.EFaktur.Web.Models.Group_Nominative;
using Toyota.Common.Web.Platform;

namespace TAM.EFaktur.Web.Controllers
{
    public class GroupNominativeController : BaseController
    {
        //
        // GET: /Group_Nominative/

        public GroupNominativeController()
        {
            Settings.Title = "GL Account Nominative";
        }

        public ActionResult PopupCreateNominative()
        {
            return PartialView("_PopupCreateNominative");
        }

        [OutputCache(Duration = 0)]
        public ActionResult PopupUpdateNominative(int NominativeId)
        {

            ViewData["UpdateNominative"] = GroupNominativeRepository.Instance.GetById(NominativeId);
            return PartialView("_PopupUpdateNominative");
        }

        public ActionResult PopupDeleteNominative(int NominativeId)
        {

            ViewData["DeleteNominative"] = GroupNominativeRepository.Instance.GetById(NominativeId);
            return PartialView("_PopupDeleteNominative");
        }

        public ActionResult PopupMultipleSearch(string NominativeId)
        {
            ViewBag.ElementId = NominativeId;
            return PartialView("_PopupMultipleSearch");
        }
        
        [HttpPost]
        public ActionResult GetGroupNominativeList(FormCollection collection, int sortBy, bool sortDirection, int page, int size, bool editDeleteGLAccountEnom)
        {
            var data = collection["data"];
            var model = new GroupNominativeDashboardSearchParamViewModel();

            if (!string.IsNullOrEmpty(data))
                model = new JavaScriptSerializer().Deserialize<GroupNominativeDashboardSearchParamViewModel>(data);

            Paging pg = new Paging
            (
                GroupNominativeRepository.Instance.Count(model),
                page,
                size
            );
            ViewData["Paging"] = pg;
            ViewData["GroupNominativeListDashboard"] = GroupNominativeRepository.Instance.GetList(model, sortBy, sortDirection ? "DESC" : "ASC", pg.StartData, pg.EndData);
            ViewBag.EditDeleteGLAccountEnom = editDeleteGLAccountEnom;

            return PartialView("_SimpleGrid");
        }

        ////Insert Data
        [HttpPost]
        public JsonResult Insert(FormCollection collection)
        {
            var data = collection["data"];

            Result result = new Result();
            if (!string.IsNullOrEmpty(data))
            {
                try
                {
                    var model = new JavaScriptSerializer().Deserialize<GroupNominativeCreateUpdate>(data);
                    result = GroupNominativeRepository.Instance.Insert(model, CurrentLogin.Username, DateTime.Now);

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                result.ResultCode = false;
                result.ResultDesc = "Data not found or not valid";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //Update Data
        [HttpPost]
        public JsonResult Update(FormCollection collection)
        {
            var data = collection["data"];

            Result result = new Result();
            if (!string.IsNullOrEmpty(data))
            {
                try
                {
                    var model = new JavaScriptSerializer().Deserialize<GroupNominativeCreateUpdate>(data);
                    var dataExisting = GroupNominativeRepository.Instance.GetById(model.NominativeId);
                    var check = model;
                    var duplicat = GroupNominativeRepository.Instance.CekDataDuplicatUpdate(check).FirstOrDefault();

                    if (duplicat == null || (duplicat != null && duplicat.NominativeId == model.NominativeId))
                    {
                        result = GroupNominativeRepository.Instance.Update(model, CurrentLogin.Username, DateTime.Now);
                        result.ResultCode = true;
                        result.ResultDesc = "Data successfully saved";
                    }
                    //else if (duplicat.Code == dataExisting.Code.ToUpper() && duplicat.Id != companyUpdate.Id)
                    else if (duplicat.CategoryCode == model.CategoryCode && duplicat.GLAccount == model.GLAccount && duplicat.NominativeId != model.NominativeId) 
                   {
                        //var _code = duplicat.Code;
                        result.ResultCode = false;
                        result.ResultDesc = "Category and GL Account already exist";
                    }

                    // result = GroupNominativeRepository.Instance.Update(model, CurrentLogin.Username, DateTime.Now);

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                result.ResultCode = false;
                result.ResultDesc = "Data not found or not valid";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //Delete Tampilan Data
        [HttpPost]
        public JsonResult Delete(FormCollection collection)
        {
            var data = collection["data"];

            Result result = new Result();
            if (!string.IsNullOrEmpty(data))
            {
                try
                {
                    var model = new JavaScriptSerializer().Deserialize<GroupNominativeCreateUpdate>(data);
                    result = GroupNominativeRepository.Instance.Delete(model);

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                result.ResultCode = false;
                result.ResultDesc = "Data not found or not valid";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult GenerateDownloadFile(FormCollection collection, bool checkAll)
        {
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = "File for download generated successfully"
            };
            string fullPath = string.Empty;
            int totalDataCount = 0;
            try
            {
                var searchParam = collection["data"];
                var dataFieldIdParams = collection["dataFieldIdParams"];
                var searchParamModel = new Nominative();
                List<long> dataFieldIdList = new List<long>();

                if (!string.IsNullOrEmpty(searchParam))
                {
                    searchParamModel = new JavaScriptSerializer().Deserialize<Nominative>(searchParam);
                }

                if (!string.IsNullOrEmpty(dataFieldIdParams))
                {
                    dataFieldIdList = new JavaScriptSerializer().Deserialize<List<long>>(dataFieldIdParams);
                }

                List<string[]> ListArr = new List<string[]>(); // array for the data

                #region xls type 

                String[] header = new string[]
                { "Category", "GL Account Nominative", "Account Name", "Type" }; //for header

                ListArr.Add(header);

                var list = NominativeRepository.Instance.GetXlsReportById(searchParamModel);
                if (!checkAll) { list = list.Where(x => dataFieldIdList.Any(y => y ==  x.NominativeId)).ToList(); }

                foreach (var obj in list)
                {
                    String[] myArr = new string[] {
                            obj.CategoryName,
                            obj.GLAccount,
                            obj.DescriptionTransaction,
                            obj.Type
                         };
                    ListArr.Add(myArr);
                }

                fullPath = XlsHelper.PutExcel(ListArr, new Nominative(), TempDownloadFolder, "GLAccount");
                totalDataCount = list.Count();
                result.ResultDesc = totalDataCount + " data successfully downloaded";
                #endregion
                return Json(result.AsDynamic(fullPath), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                result.ResultCode = false;
                result.ResultDesc = e.LogToApp("GLAccount.xls", MessageType.ERR, CurrentLogin.Username).Message;
                return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);
            }
        }

        
        [HttpGet]
        public JsonResult GetKategoriDropdown()
        {
            List<KategoriDropdownViewModel> model = new List<KategoriDropdownViewModel>();
            try
            {
                model = NominativeRepository.Instance.GetKategoriDropdown();
            }
            catch (Exception e)
            {

                throw e;
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetDropdownGLAccountType()
        {
            List<KategoriDropdownViewModel> model = new List<KategoriDropdownViewModel>();
            try
            {
                model = NominativeRepository.Instance.GetGLAccountTypeDropdownField();
            }
            catch (Exception e)
            {

                throw e;
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }

    }
}
