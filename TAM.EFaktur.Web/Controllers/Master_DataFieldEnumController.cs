﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using TAM.EFaktur.Web.Models;
using TAM.EFaktur.Web.Models.Master_DataFieldEnum;
using Toyota.Common.Web.Platform;

namespace TAM.EFaktur.Web.Controllers
{
    public class Master_DataFieldEnumController : BaseController
    {
        //
        // GET: /Master_DataFieldEnum/

        public Master_DataFieldEnumController()
        {
            Settings.Title = "Data Fill Nominative";
        }

        public ActionResult PopupUpdateDataFieldEnum(long DataFieldId)
        {

            ViewData["UpdateDataFieldEnum"] = DataFieldEnumRepository.Instance.GetById(DataFieldId);
            return PartialView("_PopupUpdateDataFieldEnum");
        }

        public ActionResult PopupMultipleSearch(string Id)
        {
            ViewBag.ElementId = Id;
            return PartialView("_PopupMultipleSearch");
        }
        [HttpPost]
        public ActionResult GetDataFieldEnumList(FormCollection collection, int sortBy, bool sortDirection, int page, int size, bool editDataFillEnom)
        {
            var data = collection["data"];
            var model = new DataFieldEnum();

            if (!string.IsNullOrEmpty(data))
                model = new JavaScriptSerializer().Deserialize<DataFieldEnum>(data);

            Paging pg = new Paging
            (
                DataFieldEnumRepository.Instance.Count(model),
                page,
                size
            );
            ViewData["Paging"] = pg;
            ViewData["UserListDashboard"] = DataFieldEnumRepository.Instance.GetList(model, sortBy, sortDirection ? "DESC" : "ASC", pg.StartData, pg.EndData);
            ViewBag.EditDataFillEnom = editDataFillEnom;

            return PartialView("_SimpleGrid");
        }

        [HttpPost]
        public JsonResult Update(FormCollection collection)
        {
            var data = collection["data"];

            Result result = new Result();
            if (!string.IsNullOrEmpty(data))
            {
                try
                {
                    var model = new JavaScriptSerializer().Deserialize<DataFieldEnum>(data);

                   if (model.VendorGroup == "")
                    {
                        result.ResultCode = false;
                        result.ResultDesc = "Vendor Group is required and can\'t be empty";
                    }
                    else
                    {
                        result = DataFieldEnumRepository.Instance.Update(model, CurrentLogin.Username, DateTime.Now);
                    }
                    //result = DataFieldEnumRepository.Instance.Update(model, CurrentLogin.Username, DateTime.Now);

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                result.ResultCode = false;
                result.ResultDesc = "Data not found or not valid";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GenerateDownloadFile(FormCollection collection, bool checkAll)
        {
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = "File for download generated successfully"
            };
            string fullPath = string.Empty;
            int totalDataCount = 0;
            try
            {
                var searchParam = collection["data"];
                var dataFieldIdParams = collection["dataFieldIdParams"];
                var searchParamModel = new DataFieldEnum();
                List<long> dataFieldIdList = new List<long>();

                if (!string.IsNullOrEmpty(searchParam))
                {
                    searchParamModel = new JavaScriptSerializer().Deserialize<DataFieldEnum>(searchParam);
                }

                if (!string.IsNullOrEmpty(dataFieldIdParams))
                {
                    dataFieldIdList = new JavaScriptSerializer().Deserialize<List<long>>(dataFieldIdParams);
                }


                List<string[]> ListArr = new List<string[]>(); // array for the data

                #region xls type 

                String[] header = new string[]
                {"Category", "Field Name by Table", "Field Name by Excel", "Vendor Group" }; //for header

                ListArr.Add(header);

                var list = DataFieldEnumRepository.Instance.GetOriginalById(searchParamModel);
                if (!checkAll) { list = list.Where(x => dataFieldIdList.Any(y => y == x.DataFieldId)).ToList(); }

                foreach (var obj in list)
                {
                    String[] myArr = new string[] {
                        obj.NominativeType,
                        obj.FieldNameByTable,
                        obj.FieldNameByExcel,
                        obj.VendorGroup
                    };
                    ListArr.Add(myArr);
                }

                fullPath = XlsHelper.PutExcel(ListArr, new DataFieldEnum(), TempDownloadFolder, "Data_Fill_Nominative");
                totalDataCount = list.Count();
                result.ResultDesc = totalDataCount + " data successfully downloaded";
                #endregion
                return Json(result.AsDynamic(fullPath), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                result.ResultCode = false;
                result.ResultDesc = e.LogToApp("Data_Fill_Nominative.xls", MessageType.ERR, CurrentLogin.Username).Message;
                return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetCategoryNominativeDropdownList()
        {
            List<DropdownModel> model = new List<DropdownModel>();

            model = DataFieldEnumRepository.Instance.GetCategoryNominativeDropdownList();
            return Json(model, JsonRequestBehavior.AllowGet);
        }
    }
}
