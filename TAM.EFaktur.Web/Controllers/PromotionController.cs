﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using TAM.EFaktur.Web.Models;
using TAM.EFaktur.Web.Models.Promotion;
using TAM.EFaktur.Web.Models.Master_Config;
using Toyota.Common.Web.Platform;
using NPOI.SS.UserModel;
using NPOI.HSSF.Util;
using NPOI.HSSF.UserModel;
using TAM.EFaktur.Web.Models.Dashboard_Nominative;

namespace TAM.EFaktur.Web.Controllers
{
    public class PromotionController : BaseController
    {
        public ActionResult GetPromotionAjax(Guid DashboardId)
        {
            var model = PromotionRepository.Instance.GetByDashboardId(DashboardId);
            return Json(model);
        }

        [HttpPost]
        public ActionResult Promotion(Guid DashboardId, string System)
        {
            var model = PromotionRepository.Instance.GetByDashboardId(DashboardId);
            model.PositionSignature = (System == EnumSystem.Manual) ? model.PositionSignature : false;
            ViewData["InsetUpdatePromotion"] = model;
            ViewData["ValidationPromotion"] = PromotionRepository.Instance.GetValidationDataFillByDashboardId(DashboardId);
            ViewData["Header"] = PromotionRepository.Instance.GetHeaderPromotion() ?? new PromotionHeaderTemplate();
            ViewBag.IsManual = (System == EnumSystem.Manual).ToString();
            return PartialView("Promotion");
        }

        [HttpPost]
        public ActionResult GetPromotionList(Guid DashboardId, string IsManual, int sortBy, bool sortDirection, int page, int size)
        {
            Paging pg = new Paging
            (
                PromotionRepository.Instance.Count(DashboardId),
                page,
                size
            );
            ViewData["Paging"] = pg;
            ViewData["List"] = PromotionRepository.Instance.GetList(DashboardId, sortBy, sortDirection ? "ASC" : "DESC", pg.StartData, pg.EndData);
            ViewData["Header"] = PromotionRepository.Instance.GetHeaderPromotion() ?? new PromotionHeaderTemplate();
            ViewBag.IsManual = IsManual;

            return PartialView("_SimpleGrid");
        }

        public ActionResult PopupCreate(Guid DashboardId)
        {
            ViewData["ValidationPromotion"] = PromotionRepository.Instance.GetValidationDataFillByDashboardId(DashboardId);
            ViewData["Header"] = PromotionRepository.Instance.GetHeaderPromotion() ?? new PromotionHeaderTemplate();
            return PartialView("_PopupCreate");
        }

        public ActionResult PopupUpdate(Guid PromotionId, Guid DashboardId, string IsManual)
        {
            ViewData["ValidationPromotion"] = PromotionRepository.Instance.GetValidationDataFillByDashboardId(DashboardId);
            ViewData["Update"] = PromotionRepository.Instance.GetById(PromotionId);
            ViewData["Header"] = PromotionRepository.Instance.GetHeaderPromotion() ?? new PromotionHeaderTemplate();
            ViewBag.IsManual = IsManual;
            return PartialView("_PopupUpdate");
        }

        public ActionResult PopupDelete(Guid PromotionId)
        {
            ViewData["Delete"] = PromotionRepository.Instance.GetById(PromotionId);
            ViewData["Header"] = PromotionRepository.Instance.GetHeaderPromotion() ?? new PromotionHeaderTemplate();
            return PartialView("_PopupDelete");
        }

        public ActionResult PopupUpload(Guid dashboardId)
        {
            //Config config = MasterConfigRepository.Instance.GetByConfigKey("UrlTemplatePromotion");
            //ViewBag.TemplateLink = config != null ? config.ConfigValue : "#";
            //ViewBag.TemplateLink = "/FileStorage/TemplateUpload/Upload Promotion.xls";
            ViewBag.DashboardId = dashboardId;
            return PartialView("_PopupUpload");
        }

        public ActionResult DownloadTemplate(Guid dashboardId)
        {
            //Config config = MasterConfigRepository.Instance.GetByConfigKey("UrlTemplateDonation");
            //ViewBag.TemplateLink = config != null ? config.ConfigValue : "#";
            var path = Server.MapPath("/FileStorage/TemplateUpload/Upload Promotion.xls");
            var header = PromotionRepository.Instance.GetHeaderPromotion();

            FileStream file = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite);

            HSSFWorkbook workbook = new HSSFWorkbook(file);
            ISheet sheet = workbook.GetSheet("Default");
            sheet.GetRow(5).GetCell(0).SetCellValue(header.NO);
            sheet.GetRow(5).GetCell(1).SetCellValue(header.NAMA);
            sheet.GetRow(5).GetCell(2).SetCellValue(header.NPWP);
            sheet.GetRow(5).GetCell(3).SetCellValue(header.ALAMAT);
            sheet.GetRow(5).GetCell(4).SetCellValue(header.TANGGAL);
            sheet.GetRow(5).GetCell(5).SetCellValue(header.BENTUK_DAN_JENIS_BIAYA);
            sheet.GetRow(5).GetCell(6).SetCellValue(header.JUMLAH);
            sheet.GetRow(5).GetCell(7).SetCellValue(header.JUMLAH_GROSS_UP);
            sheet.GetRow(5).GetCell(8).SetCellValue(header.KETERANGAN);
            sheet.GetRow(5).GetCell(9).SetCellValue(header.JUMLAH_PPH);
            sheet.GetRow(5).GetCell(10).SetCellValue(header.JENIS_PPH);
            sheet.GetRow(5).GetCell(11).SetCellValue(header.JUMLAH_NET);
            sheet.GetRow(5).GetCell(12).SetCellValue(header.NOMOR_REKENING);
            sheet.GetRow(5).GetCell(13).SetCellValue(header.NAMA_REKENING_PENERIMA);
            sheet.GetRow(5).GetCell(14).SetCellValue(header.NAMA_BANK);
            sheet.GetRow(5).GetCell(15).SetCellValue(header.NO_KTP);

            ISheet sheet2 = workbook.GetSheet("Petunjuk Pengisian");
            sheet2.GetRow(1).GetCell(0).SetCellValue(header.NO);
            sheet2.GetRow(3).GetCell(0).SetCellValue(header.NAMA);
            sheet2.GetRow(5).GetCell(0).SetCellValue(header.NPWP);
            sheet2.GetRow(7).GetCell(0).SetCellValue(header.ALAMAT);
            sheet2.GetRow(9).GetCell(0).SetCellValue(header.TANGGAL);
            sheet2.GetRow(11).GetCell(0).SetCellValue(header.BENTUK_DAN_JENIS_BIAYA);
            sheet2.GetRow(13).GetCell(0).SetCellValue(header.JUMLAH);
            sheet2.GetRow(15).GetCell(0).SetCellValue(header.JUMLAH_GROSS_UP);
            sheet2.GetRow(17).GetCell(0).SetCellValue(header.KETERANGAN);
            sheet2.GetRow(19).GetCell(0).SetCellValue(header.JUMLAH_PPH);
            sheet2.GetRow(21).GetCell(0).SetCellValue(header.JENIS_PPH);
            sheet2.GetRow(23).GetCell(0).SetCellValue(header.JUMLAH_NET);
            sheet2.GetRow(25).GetCell(0).SetCellValue(header.NOMOR_REKENING);
            sheet2.GetRow(27).GetCell(0).SetCellValue(header.NAMA_REKENING_PENERIMA);
            sheet2.GetRow(29).GetCell(0).SetCellValue(header.NAMA_BANK);
            sheet2.GetRow(31).GetCell(0).SetCellValue(header.NO_KTP);

            var boldFont = workbook.CreateFont();
            boldFont.Boldweight = (short)FontBoldWeight.BOLD;
            boldFont.Color = HSSFColor.WHITE.index;
            boldFont.FontHeightInPoints = 11;
            boldFont.FontName = "Calibri";

            ICellStyle redStyle = workbook.CreateCellStyle();
            redStyle.SetFont(boldFont);
            redStyle.FillForegroundColor = HSSFColor.RED.index;
            redStyle.FillPattern = FillPatternType.SOLID_FOREGROUND;

            redStyle.BorderBottom = BorderStyle.THIN;
            redStyle.BottomBorderColor = HSSFColor.BLACK.index;
            redStyle.BorderLeft = BorderStyle.THIN;
            redStyle.LeftBorderColor = HSSFColor.BLACK.index;
            redStyle.BorderRight = BorderStyle.THIN;
            redStyle.RightBorderColor = HSSFColor.BLACK.index;
            redStyle.BorderTop = BorderStyle.THIN;
            redStyle.TopBorderColor = HSSFColor.BLACK.index;

            redStyle.Alignment = HorizontalAlignment.CENTER;
            redStyle.VerticalAlignment = VerticalAlignment.CENTER;

            var validationDataFill = PromotionRepository.Instance.GetValidationDataFillByDashboardId(dashboardId);
            if (validationDataFill.NO) { sheet.GetRow(5).GetCell(0).CellStyle = redStyle; }
            if (validationDataFill.NAMA) { sheet.GetRow(5).GetCell(1).CellStyle = redStyle; }
            if (validationDataFill.NPWP) { sheet.GetRow(5).GetCell(2).CellStyle = redStyle; }
            if (validationDataFill.ALAMAT) { sheet.GetRow(5).GetCell(3).CellStyle = redStyle; }
            if (validationDataFill.TANGGAL) { sheet.GetRow(5).GetCell(4).CellStyle = redStyle; }
            if (validationDataFill.BENTUK_DAN_JENIS_BIAYA) { sheet.GetRow(5).GetCell(5).CellStyle = redStyle; }
            if (validationDataFill.JUMLAH) { sheet.GetRow(5).GetCell(6).CellStyle = redStyle; }
            if (validationDataFill.JUMLAH_GROSS_UP) { sheet.GetRow(5).GetCell(7).CellStyle = redStyle; }
            if (validationDataFill.KETERANGAN) { sheet.GetRow(5).GetCell(8).CellStyle = redStyle; }
            if (validationDataFill.JUMLAH_PPH) { sheet.GetRow(5).GetCell(9).CellStyle = redStyle; }
            if (validationDataFill.JENIS_PPH) { sheet.GetRow(5).GetCell(10).CellStyle = redStyle; }
            if (validationDataFill.JUMLAH_NET) { sheet.GetRow(5).GetCell(11).CellStyle = redStyle; }
            if (validationDataFill.NOMOR_REKENING) { sheet.GetRow(5).GetCell(12).CellStyle = redStyle; }
            if (validationDataFill.NAMA_REKENING_PENERIMA) { sheet.GetRow(5).GetCell(13).CellStyle = redStyle; }
            if (validationDataFill.NAMA_BANK) { sheet.GetRow(5).GetCell(14).CellStyle = redStyle; }
            if (validationDataFill.NO_KTP) { sheet.GetRow(5).GetCell(15).CellStyle = redStyle; }

            // code to create workbook 
            using (var exportData = new MemoryStream())
            {
                workbook.Write(exportData);
                byte[] bytes = exportData.ToArray();
                return File(bytes, "application/vnd.ms-excel", "Upload Promotion.xls");
            }
        }

        [HttpPost]
        public JsonResult Insert(FormCollection collection)
        {
            var data = collection["data"];
            Result result = new Result();
            if (!string.IsNullOrEmpty(data))
            {
                try
                {
                    var model = new JavaScriptSerializer().Deserialize<Promotion>(data);
                    result = PromotionRepository.Instance.Insert(model, CurrentLogin.Username);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                result.ResultCode = false;
                result.ResultDesc = "Data not found or not valid";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Update(FormCollection collection)
        {
            var data = collection["data"];
            Result result = new Result();
            if (!string.IsNullOrEmpty(data))
            {
                try
                {
                    var model = new JavaScriptSerializer().Deserialize<Promotion>(data);
                    result = PromotionRepository.Instance.Update(model, CurrentLogin.Username);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                result.ResultCode = false;
                result.ResultDesc = "Data not found or not valid";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Delete(FormCollection collection)
        {
            var data = collection["data"];
            Result result = new Result();
            if (!string.IsNullOrEmpty(data))
            {
                try
                {
                    var model = new JavaScriptSerializer().Deserialize<Promotion>(data);
                    result = PromotionRepository.Instance.Delete(model, CurrentLogin.Username);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                result.ResultCode = false;
                result.ResultDesc = "Data not found or not valid";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SubmitHeader(FormCollection collection)
        {
            var data = collection["data"];
            Result result = new Result();
            if (!string.IsNullOrEmpty(data))
            {
                try
                {
                    var model = new JavaScriptSerializer().Deserialize<Promotion>(data);
                    result = PromotionRepository.Instance.SubmitHeader(model, CurrentLogin.Username, TempDownloadFolder, Server.MapPath("~/Report/RDLC/DataFieldPromotion.rdlc"));
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                result.ResultCode = false;
                result.ResultDesc = "Data not found or not valid";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult UploadPromotion(FormCollection collection)
        {
            var data = collection["data"];
            var model = new Promotion();
            if (!string.IsNullOrEmpty(data))
            {
                try
                {
                    model = new JavaScriptSerializer().Deserialize<Promotion>(data);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            var r = new List<UploadResultViewModel>();

            int i = 0;
            foreach (string file in Request.Files)
            {
                var statuses = new List<UploadResultViewModel>();
                var headers = Request.Headers;
                var now = DateTime.Now;
                var fileName = StampFileName(Request.Files[i].FileName, now);
                var fullPath = Path.Combine(TempUploadFolder, Path.GetFileName(fileName));
                var extension = Path.GetExtension(fileName);

                Result result = new Result();
                if (extension.Equals(".xls") || extension.Equals(".xlsx"))
                {
                    #region Upload File
                    if (string.IsNullOrEmpty(headers["X-File-Name"]))
                    {
                        UploadWholeFile(Request, statuses, TempUploadFolder, now);
                    }
                    else
                    {
                        UploadPartialFile(headers["X-File-Name"], Request, statuses);
                    }
                    #endregion

                    #region Insert Promotion Data
                    result = PromotionRepository.Instance.ExcelPromotionInsert(fullPath, CurrentLogin.Username, model);
                    #endregion
                }
                else
                {
                    result.ResultCode = false;
                    result.ResultDesc = "Only Excel Filetype allowed";
                }
                JsonResult jsonObj = Json(result.AsDynamic(statuses));
                jsonObj.ContentType = "text/plain";
                i++;

                return jsonObj;
            }

            return Json(r);
        }

        [HttpPost]
        public ActionResult GetPromotionDetailList(Guid? PromotionId, int sortBy, bool sortDirection, int page, int size, bool isDelete, string IsManual)
        {
            Paging pg = new Paging
            (
                PromotionRepository.Instance.CountDetail(PromotionId),
                page,
                size
            );
            ViewData["Paging"] = pg;
            ViewData["ListDetail"] = PromotionRepository.Instance.GetDetailList(PromotionId, sortBy, sortDirection ? "ASC" : "DESC", pg.StartData, pg.EndData);
            ViewData["Header"] = PromotionRepository.Instance.GetHeaderPromotion() ?? new PromotionHeaderTemplate();
            ViewBag.IsDelete = isDelete;
            ViewBag.IsManual = IsManual;

            return PartialView("_SimpleGridDetail");
        }


        [HttpPost]
        public ActionResult PopupCreateDetail(Guid DashboardId)
        {
            ViewData["ValidationPromotion"] = PromotionRepository.Instance.GetValidationDataFillByDashboardId(DashboardId);
            ViewData["Header"] = PromotionRepository.Instance.GetHeaderPromotion() ?? new PromotionHeaderTemplate();
            return PartialView("_PopupCreateDetail");
        }

        [HttpPost]
        public ActionResult PopupUpdateDetail(Guid PromotionDetailId, Guid DashboardId)
        {
            ViewData["ValidationPromotion"] = PromotionRepository.Instance.GetValidationDataFillByDashboardId(DashboardId);
            ViewData["Header"] = PromotionRepository.Instance.GetHeaderPromotion() ?? new PromotionHeaderTemplate();
            ViewData["UpdateDetail"] = PromotionRepository.Instance.GetDetailById(PromotionDetailId);
            return PartialView("_PopupUpdateDetail");
        }

        [HttpPost]
        public ActionResult PopupDeleteDetail(Guid PromotionDetailId)
        {
            ViewData["DeleteDetail"] = PromotionRepository.Instance.GetDetailById(PromotionDetailId);
            ViewData["Header"] = PromotionRepository.Instance.GetHeaderPromotion() ?? new PromotionHeaderTemplate();
            return PartialView("_PopupDeleteDetail");
        }

        [HttpPost]
        public JsonResult InsertDetail(FormCollection collection)
        {
            var data = collection["data"];
            Result result = new Result();
            if (!string.IsNullOrEmpty(data))
            {
                try
                {
                    var model = new JavaScriptSerializer().Deserialize<PromotionDetail>(data);
                    result = PromotionRepository.Instance.InsertDetail(model, CurrentLogin.Username);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                result.ResultCode = false;
                result.ResultDesc = "Data not found or not valid";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdateDetail(FormCollection collection)
        {
            var data = collection["data"];
            Result result = new Result();
            if (!string.IsNullOrEmpty(data))
            {
                try
                {
                    var model = new JavaScriptSerializer().Deserialize<PromotionDetail>(data);
                    result = PromotionRepository.Instance.UpdateDetail(model, CurrentLogin.Username);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                result.ResultCode = false;
                result.ResultDesc = "Data not found or not valid";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteDetail(FormCollection collection)
        {
            var data = collection["data"];
            Result result = new Result();
            if (!string.IsNullOrEmpty(data))
            {
                try
                {
                    var model = new JavaScriptSerializer().Deserialize<PromotionDetail>(data);
                    result = PromotionRepository.Instance.DeleteDetail(model, CurrentLogin.Username);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                result.ResultCode = false;
                result.ResultDesc = "Data not found or not valid";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}
