﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using TAM.EFaktur.Web.Models;
using Toyota.Common.Web.Platform;
using TAM.EFaktur.Web.Models.Master_Company_Details;
using System.IO;
using TAM.EFaktur.Web.Models.Master_Config;

namespace TAM.EFaktur.Web.Controllers
{
    public class Master_Company_DetailsController : BaseController
    {
        //
        // GET: /Master_Company_Details/

        public Master_Company_DetailsController()
        {
            Settings.Title = "Master Company Details";
        }

        public ActionResult PopupCreateCompanyDetail()
        {
            return PartialView("_PopupCreateCompanyDetail");
        }

        [HttpPost]
        public ActionResult PopupCreateCompanyDetail(FormCollection formCollection)
        {
            var companyData = new CompanyCreateUpdate
            {
                Lokasi = formCollection["Lokasi"],
                Npwp_pt = formCollection["Npwp_pt"],
                Nama_pt = formCollection["Nama_pt"],
                Npwp_ttd = formCollection["Npwp_ttd"],
                Nama_ttd = formCollection["Nama_ttd"],
                Jabatan_ttd = formCollection["Jabatan_ttd"],
                Code = formCollection["Code"],
                alamat_pt = formCollection["alamat_pt"],
                no_pengukuhan_pt = formCollection["no_pengukuhan_pt"],
                Cut_Off_Start = Convert.ToDateTime(formCollection["Cut_Off_Start"].FormatSQLDate()),
                Cut_Off_End = Convert.ToDateTime(formCollection["Cut_Off_End"].FormatSQLDate()),

            };
            Result result = new Result();

            if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
            {
                var file = System.Web.HttpContext.Current.Request.Files["fileUpload"];
                var fileimagecap = System.Web.HttpContext.Current.Request.Files["fileUploadCap"];


                var dirFile = Path.Combine(Server.MapPath("~/Images/"));
                if (!Directory.Exists(dirFile))
                    Directory.CreateDirectory(dirFile);

                if (file.ContentLength > 0 )
                {

                    #region save file ttd
                    string Filename = Path.GetFileNameWithoutExtension(file.FileName);
                    string extension = Path.GetExtension(file.FileName);
                    var fullPathToDrive = dirFile + Filename + DateTime.Now.ToString("ddMMyyyy") + extension;
                    //var fullPathToDrive = urlImage + Filename + DateTime.Now.ToString("ddMMyyyy") + extension;
                    var fullPathToTable = "Images/" + Filename + DateTime.Now.ToString("ddMMyyyy") + extension;
                    file.SaveAs(Path.Combine(fullPathToDrive));
                    companyData.Image_ttd = fullPathToTable;
                    #endregion

                }

                string filenameCap = "";
                if (fileimagecap == null)
                {
                    filenameCap = "Images/DefaultCap.png";
                    companyData.Image_cap = filenameCap; 
                }
                else
                {
                    filenameCap = Path.GetFileNameWithoutExtension(fileimagecap.FileName);
                    if (fileimagecap.ContentLength > 0)
                    {
                        #region save file caps
                        filenameCap = Path.GetFileNameWithoutExtension(fileimagecap.FileName);
                        string extensionCap = Path.GetExtension(fileimagecap.FileName);
                        var fullPathToDriveCap = dirFile + filenameCap + DateTime.Now.ToString("ddMMyyyy") + extensionCap;
                        //var fullPathToDriveCap = urlImage + filenameCap + DateTime.Now.ToString("ddMMyyyy") + extensionCap;
                        var fullPathToTableCap = "Images/" + filenameCap + DateTime.Now.ToString("ddMMyyyy") + extensionCap;

                        fileimagecap.SaveAs(Path.Combine(fullPathToDriveCap));
                        //fileimagecap.SaveAs(fullPathToDriveCap);
                        companyData.Image_cap = fullPathToTableCap;
                        #endregion

                    }
                }

                


                var duplicat = CompanyRepository.Instance.CekDataDuplicat(companyData);

                if (duplicat.Count == 0)
                {
                    result = CompanyRepository.Instance.Insert(companyData, "TAM Efaktur", DateTime.Now);
                    result.ResultCode = true;
                    result.ResultDesc = "Data successfully saved";
                }
                else if (duplicat[0].Code == companyData.Code.ToUpper())
                {
                    var _code = duplicat[0].Code;
                    result.ResultCode = false;
                    result.ResultDesc = "Data Already Exist" + " Key Invoice Number "+ _code ;
                }
                else if (duplicat[0].Npwp_pt == companyData.Npwp_pt)
                {
                    var _npwp = duplicat[0].Npwp_pt;
                    result.ResultCode = false;
                    result.ResultDesc = "Data Already Exist" + " Company NPWP " + _npwp;
                }

            }

            companyData.Cut_Off_Start = Convert.ToDateTime(companyData.Cut_Off_Start.FormatSQLDate());
            companyData.Cut_Off_End = Convert.ToDateTime(companyData.Cut_Off_End.FormatSQLDate());

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PopupUpdateCompanyDetail(Guid Id)
        {
            ViewData["UpdateCompany"] = CompanyRepository.Instance.GetById(Id);
            return PartialView("_PopupUpdateCompanyDetail");
        }

        public ActionResult PopupDeleteCompanyDetail(Guid Id)
        {

            ViewData["DeleteCompanyDetail"] = CompanyRepository.Instance.GetById(Id);
            return PartialView("_PopupDeleteCompanyDetail");
        }

        public ActionResult PopupMultipleSearch(string Id)
        {
            ViewBag.ElementId = Id;
            return PartialView("_PopupMultipleSearch");
        }
        [HttpPost]

        public ActionResult GetUserList(FormCollection collection, int sortBy, bool sortDirection, int page, int size)
        {

            var data = collection["data"];
            var model = new CompanySearchParamViewModel();

            if (!string.IsNullOrEmpty(data))
                model = new JavaScriptSerializer().Deserialize<CompanySearchParamViewModel>(data);

            //edo
            if (model.Cut_Off_Start == "" && model.Cut_Off_End == "")
            {
                model.Cut_Off_Start = "01-01-1999";
                model.Cut_Off_End = "30-12-9999";
            }

            Paging pg = new Paging
            (
                CompanyRepository.Instance.Count(model),
                page,
                size
            );
            ViewData["Paging"] = pg;
            ViewData["ListCompanyDetail"] = CompanyRepository.Instance.GetList(model, sortBy, sortDirection ? "ASC" : "DESC", pg.StartData, pg.EndData);

            return PartialView("_SimpleGrid");
        }
        [HttpPost]

        public JsonResult Insert(FormCollection collection)
        {
            var data = collection["data"];

            Result result = new Result();
            if (!string.IsNullOrEmpty(data))
            {
                try
                {
                    var model = new JavaScriptSerializer().Deserialize<CompanyCreateUpdate>(data);
                    //result = CompanyRepository.Instance.Insert(model, "TAM Efaktur", DateTime);
                    result = CompanyRepository.Instance.Insert(model, "TAM Efaktur", DateTime.Now);
                    result.ResultCode = true;
                    result.ResultDesc = "Data successfully saved";
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                result.ResultCode = false;
                result.ResultDesc = "Data not found or not valid";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]

        public JsonResult Update(FormCollection collection)
        {
            var companyUpdate = new CompanyUpdate
            {
                Lokasi = collection["Lokasi"],
                Npwp_pt = collection["Npwp_pt"],
                Nama_pt = collection["Nama_pt"],
                Npwp_ttd = collection["Npwp_ttd"],
                Nama_ttd = collection["Nama_ttd"],
                Jabatan_ttd = collection["Jabatan_ttd"],
                Code = collection["Code"],
                alamat_pt = collection["alamat_pt"],
                no_pengukuhan_pt = collection["no_pengukuhan_pt"],
                Cut_Off_Start = Convert.ToDateTime(collection["Cut_Off_Start"].FormatSQLDate()),
                Cut_Off_End = Convert.ToDateTime(collection["Cut_Off_End"].FormatSQLDate()),
                Id = new Guid(collection["Id"]),
                isCapRemove = collection["isCapRemove"]
            };
            Result result = new Result();
            if (companyUpdate != null)
            {
                try
                {
                    var dataExisting = CompanyRepository.Instance.GetById(companyUpdate.Id);

                    if (companyUpdate.isCapRemove == "true")
                    {
                        companyUpdate.Image_cap = "Images/DefaultCap.png";
                        dataExisting.Image_cap = "Images/DefaultCap.png";

                    }

                    if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
                    {
                        var fileimagecap = System.Web.HttpContext.Current.Request.Files["fileUploadCap"];
                        if (fileimagecap == null)
                        {
                            string fileimagecapnull = "Images/DefaultCap.png"; dataExisting.Image_cap.ToString();

                            var dirFile = Path.Combine(Server.MapPath("~/Images/"));
                            string filenameCap = Path.GetFileNameWithoutExtension(fileimagecapnull);
                            string extensionCap = Path.GetExtension(fileimagecapnull);
                            var fullPathToDriveCap = dirFile + filenameCap + extensionCap;
                            var fullPathToTableCap = "Images/" + filenameCap + extensionCap;
                            //fileimagecap.SaveAs(Path.Combine(fullPathToDriveCap));
                            companyUpdate.Image_cap = fullPathToTableCap;
                            dataExisting.Image_cap = fullPathToTableCap;
                        }
                        else
                        {
                             
                            var dirFile = Path.Combine(Server.MapPath("~/Images/"));
                            string filenameCap = Path.GetFileNameWithoutExtension(fileimagecap.FileName);
                            string extensionCap = Path.GetExtension(fileimagecap.FileName);
                            var fullPathToDriveCap = dirFile + filenameCap + DateTime.Now.ToString("ddMMyyyy") + extensionCap;
                            var fullPathToTableCap = "Images/" + filenameCap + DateTime.Now.ToString("ddMMyyyy") + extensionCap;
                            fileimagecap.SaveAs(Path.Combine(fullPathToDriveCap));
                            companyUpdate.Image_cap = fullPathToTableCap;
                            dataExisting.Image_cap = fullPathToTableCap;
                            
                            
                        }

                        var file = System.Web.HttpContext.Current.Request.Files["fileUpload"];
                        
                        //if (file.ContentLength > 0 || fileimagecap.ContentLength > 0)
                        if (file != null)
                        {
                            var dirFile = Path.Combine(Server.MapPath("~/Images/"));
                            if (!Directory.Exists(dirFile))
                                Directory.CreateDirectory(dirFile);
                            #region save file caps
                            //string filenameCap = Path.GetFileNameWithoutExtension(fileimagecap.FileName);
                            //string extensionCap = Path.GetExtension(fileimagecap.FileName);
                            //var fullPathToDriveCap = dirFile + filenameCap + DateTime.Now.ToString("ddMMyyyy") + extensionCap;
                            //var fullPathToTableCap = "Images/" + filenameCap + DateTime.Now.ToString("ddMMyyyy") + extensionCap;
                            //fileimagecap.SaveAs(Path.Combine(fullPathToDriveCap));
                            //companyUpdate.Image_cap = fullPathToTableCap;
                            #endregion

                            #region save file ttd
                            string Filename = Path.GetFileNameWithoutExtension(file.FileName);
                            string extension = Path.GetExtension(file.FileName);
                            var fullPathToDrive = dirFile + Filename + DateTime.Now.ToString("ddMMyyyy") + extension;
                            var fullPathToTable = "Images/" + Filename + DateTime.Now.ToString("ddMMyyyy") + extension;
                            file.SaveAs(Path.Combine(fullPathToDrive));
                            companyUpdate.Image_ttd = fullPathToTable;

                            //dataExisting.Image_cap = fullPathToTableCap; // file fisik harus sudah kesimpan
                            dataExisting.Image_ttd = fullPathToTable; // file fisik harus sudah kesimpan
                            #endregion                            
                        }
                        else
                        {
                            string fileimagettdnull = dataExisting.Image_ttd.ToString();

                            var dirFile = Path.Combine(Server.MapPath("~/Images/"));
                            string Filename = Path.GetFileNameWithoutExtension(fileimagettdnull);
                            string extension = Path.GetExtension(fileimagettdnull);
                            var fullPathToDrive = dirFile + Filename + extension;
                            var fullPathToTable = "Images/" + Filename + extension;
                            //file.SaveAs(Path.Combine(fullPathToDrive));
                            companyUpdate.Image_ttd = fullPathToTable;
                            dataExisting.Image_ttd = fullPathToTable;
                        }
                    }

                    var checkcode = dataExisting.Code;
                    var checknpwp = dataExisting.Npwp_pt;

                    dataExisting.Lokasi = companyUpdate.Lokasi;
                    dataExisting.Code = companyUpdate.Code;
                    dataExisting.Jabatan_ttd = companyUpdate.Jabatan_ttd;
                    dataExisting.Nama_pt = companyUpdate.Nama_pt;
                    dataExisting.Npwp_pt = companyUpdate.Npwp_pt;
                    dataExisting.Nama_ttd = companyUpdate.Nama_ttd;
                    dataExisting.Npwp_ttd = companyUpdate.Npwp_ttd;
                    dataExisting.alamat_pt = companyUpdate.alamat_pt;
                    dataExisting.no_pengukuhan_pt = companyUpdate.no_pengukuhan_pt;
                    dataExisting.Cut_Off_Start = companyUpdate.Cut_Off_Start;
                    dataExisting.Cut_Off_End = companyUpdate.Cut_Off_End;

                    var check = companyUpdate;
                       if (dataExisting.Code == checkcode)
                        {
                            var code = "";
                        }
                        else
                        {
                            var code = dataExisting.Code;
                        }
                        if (dataExisting.Npwp_pt == checknpwp)
                        {
                            var npwp = "";
                        }
                        else
                        {
                            var npwp = dataExisting.Npwp_pt;
                        }
                    
                    //var duplicat = CompanyRepository.Instance.CekDataDuplicatUpdate(dataExisting);
                    var duplicat = CompanyRepository.Instance.CekDataDuplicatUpdate(check).FirstOrDefault();
                    if (duplicat == null || (duplicat != null && duplicat.Id == companyUpdate.Id))
                    {
                        result = CompanyRepository.Instance.Update(dataExisting, "TAM efaktur", DateTime.Now);
                        result.ResultCode = true;
                        result.ResultDesc = "Data successfully saved";
                    }

                   else if (duplicat.Code == dataExisting.Code.ToUpper() && duplicat.Id != companyUpdate.Id)
                    {
                        var _code = duplicat.Code;
                        result.ResultCode = false;
                        result.ResultDesc = "Data Already Exist" + " Key Invoice Number " + _code;
                    }
                    else if (duplicat.Npwp_pt == dataExisting.Npwp_pt && duplicat.Id != companyUpdate.Id)
                    {
                        var _npwp = duplicat.Npwp_pt;
                        result.ResultCode = false;
                        result.ResultDesc = "Data Already Exist" + " Company NPWP " + _npwp;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                //result.ResultCode = true;
                //result.ResultDesc = "Data successfully saved";
            }
            else
            {
                result.ResultCode = false;
                result.ResultDesc = "Data not found or not valid";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult Delete(FormCollection collection)
        {
            var data = collection["data"];
            Result result = new Result();
            if (!string.IsNullOrEmpty(data))
            {
                try
                {
                    var model = new JavaScriptSerializer().Deserialize<CompanyUpdate>(data);
                    if (model.Code != "J")
                    {
                        result = CompanyRepository.Instance.Delete(model);
                    }
                    else
                    {
                        result.ResultDesc = "Data Invoice key Number J cannot be delete";
                    }
                    
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                result.ResultCode = false;
                result.ResultDesc = "Data not found or not valid";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GenerateDownloadFile(FormCollection collection, string type)
        {
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = "File for download generated successfully"
            };
            string fullPath = string.Empty;
            int totalDataCount = 0;
            try
            {
                var searchParam = collection["data"];
                var CompanyList = collection["CompanyList"];
                var searchParamModel = new CompanySearchParamViewModel();

                if (!string.IsNullOrEmpty(searchParam))
                {
                    searchParamModel = new JavaScriptSerializer().Deserialize<CompanySearchParamViewModel>(searchParam);
                    // CompanyListData = CompanyRepository.Instance.GetById(searchParamModel, CurrentHRIS.DivisionName);
                }


                List<string[]> ListArr = new List<string[]>(); // array for the data

                #region xls type

                if (type == "xls")
                {

                    String[] header = new string[]
                    {"Key Invoice Number", "Location","Company NPWP ", "Company Name", "Sign Off NPWP", "Sign Off Name",
                              "Sign Off Position", "Cut Off Start", "Cut Off End", "Company Address",
                              "Nomor Pengukuhan PT"
                    }; //for header

                    ListArr.Add(header);

                    //ViewData["ListCompanyDetail"] = CompanyRepository.Instance.GetList(model, sortBy, sortDirection ? "ASC" : "DESC", pg.StartData, pg.EndData);

                    var list = CompanyRepository.Instance.GetOriginalById(searchParamModel);
                    foreach (CompanyViewModel obj in list)
                    {
                        String[] myArr = new string[] {
                        obj.Code,
                        obj.Lokasi,
                        obj.Npwp_pt,
                        obj.Nama_pt,
                        obj.Npwp_ttd,
                        obj.Nama_ttd,
                        obj.Jabatan_ttd,
                        obj.Cut_Off_Start.ToString(),
                        obj.Cut_Off_End.ToString(),
                        obj.alamat_pt,
                        obj.no_pengukuhan_pt
                    };
                        ListArr.Add(myArr);
                    }
                    fullPath = XlsHelper.PutExcel(ListArr, new CompanyViewModel(), TempDownloadFolder, "Mater_Company_Details");
                    totalDataCount = list.Count();
                    result.ResultDesc = totalDataCount + " data successfully downloaded";
                }
                #endregion
                return Json(result.AsDynamic(fullPath), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                result.ResultCode = false;
                result.ResultDesc = e.LogToApp("Mater_Company_Details Download " + type, MessageType.ERR, CurrentLogin.Username).Message;
                return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);
            }
        }
    }
}