﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using TAM.EFaktur.Web.Models;
using TAM.EFaktur.Web.Models.Master_Config;
using Toyota.Common.Web.Platform;

namespace TAM.EFaktur.Web.Controllers
{
    public class Master_ConfigController : BaseController
    {
        //
        // GET: /Master_Config/

        public Master_ConfigController()
        {
            Settings.Title = "Maintain-Config";
        }

        public ActionResult PopupCreateConfig()
        {
            return PartialView("_PopupCreateConfig");
        }

        [OutputCache(Duration=0)]
        public ActionResult PopupUpdateConfig(Guid Id)
        {

            ViewData["UpdateConfig"] = MasterConfigRepository.Instance.GetById(Id);
            return PartialView("_PopupUpdateConfig");
        }

        public ActionResult PopupDeleteConfig(Guid Id)
        {

            ViewData["DeleteConfig"] = MasterConfigRepository.Instance.GetById(Id);
            return PartialView("_PopupDeleteConfig");
        }

        public ActionResult PopupMultipleSearch(string Id)
        {
            ViewBag.ElementId = Id;
            return PartialView("_PopupMultipleSearch");
        }
        [HttpPost]
        public ActionResult GetMasterConfigList(FormCollection collection, int sortBy, bool sortDirection, int page, int size)
        {
            var data = collection["data"];
            var model = new MasterConfigDashboardSearchParamViewModel();

            if (!string.IsNullOrEmpty(data))
                model = new JavaScriptSerializer().Deserialize<MasterConfigDashboardSearchParamViewModel>(data);

            Paging pg = new Paging
            (
                MasterConfigRepository.Instance.Count(model),
                page,
                size
            );
            ViewData["Paging"] = pg;
            ViewData["MasterConfigListDashboard"] = MasterConfigRepository.Instance.GetList(model, sortBy, sortDirection ? "ASC" : "DESC", pg.StartData, pg.EndData);

            return PartialView("_SimpleGrid");
        }

        //Insert Data
        [HttpPost]
        public JsonResult Insert(FormCollection collection)
        {
            var data = collection["data"];

            Result result = new Result();
            if (!string.IsNullOrEmpty(data))
            {
                try
                {
                    var model = new JavaScriptSerializer().Deserialize<MasterConfigCreateUpdate>(data);



                    result = MasterConfigRepository.Instance.Insert(model,CurrentLogin.Username, DateTime.Now);

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                result.ResultCode = false;
                result.ResultDesc = "Data not found or not valid";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //Update Data
        [HttpPost]
        public JsonResult Update(FormCollection collection)
        {
            var data = collection["data"];

            Result result = new Result();
            if (!string.IsNullOrEmpty(data))
            {
                try
                {
                    var model = new JavaScriptSerializer().Deserialize<MasterConfigCreateUpdate>(data);
                    result = MasterConfigRepository.Instance.Update(model, CurrentLogin.Username, DateTime.Now);

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                result.ResultCode = false;
                result.ResultDesc = "Data not found or not valid";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //Delete Tampilan Data
        [HttpPost]
        public JsonResult Delete(FormCollection collection)
        {
            var data = collection["data"];

            Result result = new Result();
            if (!string.IsNullOrEmpty(data))
            {
                try
                {
                    var model = new JavaScriptSerializer().Deserialize<MasterConfigCreateUpdate>(data);
                    result = MasterConfigRepository.Instance.Delete(model, CurrentLogin.Username, DateTime.Now);

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                result.ResultCode = false;
                result.ResultDesc = "Data not found or not valid";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }
}
