﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using TAM.EFaktur.Web.Models;
using TAM.EFaktur.Web.Models.VendorGroup;
using Toyota.Common.Web.Platform;

namespace TAM.EFaktur.Web.Controllers
{
    public class VendorGroupController : BaseController
    {       
        public JsonResult GetVendorGroupDropdownList()
        {
            List<DropdownModel> model = new List<DropdownModel>();

            model = VendorGroupRepository.Instance.GetVendorGroupDropdownList();
            return Json(model, JsonRequestBehavior.AllowGet);
        }
    }
}
