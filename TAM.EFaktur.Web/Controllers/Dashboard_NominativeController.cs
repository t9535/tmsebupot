﻿using Microsoft.Reporting.WebForms;
using NPOI.HSSF.UserModel;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using Spire.Doc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using TAM.EFaktur.Web.Models;
using TAM.EFaktur.Web.Models.Dashboard_Nominative;
using TAM.EFaktur.Web.Models.Donation;
using TAM.EFaktur.Web.Models.Entertainment;
using TAM.EFaktur.Web.Models.Master_Config;
using TAM.EFaktur.Web.Models.Master_DataFieldEnum;
using TAM.EFaktur.Web.Models.Promotion;
using Toyota.Common.Web.Platform;
using TAM.EFaktur.Web.Models.Attachment_Type;
using System.Globalization;
using NPOI.SS.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.Util;

namespace TAM.EFaktur.Web.Controllers
{
    public class Dashboard_NominativeController : BaseController
    {
        //
        // GET: /Dashboard_Nominative/

        public Dashboard_NominativeController()
        {
            Settings.Title = "Dashboard e-Nominative";
        }

        [HttpPost]
        public ActionResult GetDashoardList(FormCollection collection, int sortBy, bool sortDirection, int page, int size, bool createNominativeList)

        {
            var data = collection["data"];
            var model = new DashboardSearch();

            if (!string.IsNullOrEmpty(data))
                model = new JavaScriptSerializer().Deserialize<DashboardSearch>(data);

            Paging pg = new Paging
            (
                DashboardRepository.Instance.Count(model),
                page,
                size
            );
            ViewData["Paging"] = pg;
            ViewData["UserListDashboard"] = DashboardRepository.Instance.GetList(model, sortBy, sortDirection ? "DESC" : "ASC", pg.StartData, pg.EndData);
            ViewBag.CreateNominativeList = createNominativeList;

            return PartialView("_SimpleGrid");
        }

        public ActionResult PopupUploadNominativeData()
        {
            //Config config = MasterConfigRepository.Instance.GetByConfigKey("UrlTemplateNominativeData");
            //ViewBag.TemplateLink = config != null ? config.ConfigValue : "";
            ViewBag.TemplateLink = "/FileStorage/TemplateUpload/Upload Nominative Data.xls";
            return PartialView("_PopupUploadNominativeData");
        }

        //public ActionResult PopupUploadNominativeFromPDFFile()
        //{
        //    ViewBag.TemplateLink = "/FileStorage/TemplateUpload/Upload Nominative Data.xls";
        //    return PartialView("_PopupUploadNominativeFromPDFFile");
        //}

        [HttpPost]
        public ActionResult UploadNominativeData()
        {
            var r = new List<UploadResultViewModel>();

            int i = 0;
            foreach (string file in Request.Files)
            {
                var statuses = new List<UploadResultViewModel>();
                var headers = Request.Headers;
                var now = DateTime.Now;
                var fileName = StampFileName(Request.Files[i].FileName, now);
                var fullPath = Path.Combine(TempUploadFolder, Path.GetFileName(fileName));
                var extension = Path.GetExtension(fileName);

                Result result = new Result();
                if (extension.Equals(".xls") || extension.Equals(".xlsx"))
                {
                    #region Upload File
                    if (string.IsNullOrEmpty(headers["X-File-Name"]))
                    {
                        UploadWholeFile(Request, statuses, TempUploadFolder, now);
                    }
                    else
                    {
                        UploadPartialFile(headers["X-File-Name"], Request, statuses);
                    }
                    #endregion

                    #region Insert Nominative Data

                    result = DashboardRepository.Instance.ExcelNominativeDataInsert(fullPath, CurrentLogin.Username);
                    #endregion
                }
                else
                {
                    result.ResultCode = false;
                    result.ResultDesc = "Only Excel Filetype allowed";

                }
                JsonResult jsonObj = Json(result.AsDynamic(statuses));
                jsonObj.ContentType = "text/plain";
                i++;

                return jsonObj;
            }

            return Json(r);
        }

        [HttpPost]
        public ActionResult UploadNominativeFromPDFFile()
        {
            Result result = new Result();
            for (int i = 0; i < Request.Files.Count; i++)
            {
                var file = Request.Files[i];
                var fileName = file.FileName;
                var extension = Path.GetExtension(fileName);
                if (extension == ".pdf")
                {
                    var path = Path.Combine(TempUploadFolder, fileName);
                    string RefferenceNo = fileName.Replace(".pdf", "");

                    file.SaveAs(path);
                    result = DashboardRepository.Instance.UpdatePathUpload(RefferenceNo, path);
                }
                else
                {
                    result.ResultCode = false;
                    result.ResultDesc = "Only PDF Filetype allowed";
                }
            }

            return Json(result, JsonRequestBehavior.AllowGet);

            //var statuses = new List<UploadResultViewModel>();
            //int i = 0;
            //Result result = new Result();
            //foreach (string file in Request.Files)
            //{
            //    statuses = new List<UploadResultViewModel>();
            //    var headers = Request.Headers;
            //    var now = DateTime.Now;
            //    var fileName = StampFileName(Request.Files[i].FileName, now);
            //    var fullPath = Path.Combine(TempUploadFolder, Path.GetFileName(fileName));
            //    var getRefNo = Request.Files[i].FileName;

            //    string RefferenceNo = getRefNo.Replace(".pdf", "");
            //    var extension = Path.GetExtension(fileName);
            //    if (extension.Equals(".pdf"))
            //    {
            //        #region Upload File
            //        if (string.IsNullOrEmpty(headers["X-File-Name"]))
            //        {
            //            UploadWholeFile(Request, statuses, TempUploadFolder, now);
            //        }
            //        else
            //        {
            //            UploadPartialFile(headers["X-File-Name"], Request, statuses);
            //        }
            //        #endregion

            //        #region Update File Upload Dashboard

            //        //update path File Upload Dashboard
            //        result = DashboardRepository.Instance.UpdatePathUpload(RefferenceNo, fullPath);

            //        #endregion
            //    }
            //    else
            //    {
            //        result.ResultCode = false;
            //        result.ResultDesc = "Only PDF Filetype allowed";
            //    }

            //    i++;
            //}

            //JsonResult jsonObj = Json(result.AsDynamic(statuses));
            //jsonObj.ContentType = "text/plain";

            //return jsonObj;
        }

        public JsonResult Delete(FormCollection collection, bool checkAll)
        {
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = "Delete successfully"
            };

            try
            {
                var searchParam = collection["data"];
                var dashboardIdParams = collection["dashboardIdParams"];
                var searchParamModel = new DashboardSearch();
                List<Guid> DashboardOutIdList = new List<Guid>();
                if (!string.IsNullOrEmpty(searchParam) && checkAll)
                {
                    searchParamModel = new JavaScriptSerializer().Deserialize<DashboardSearch>(searchParam);
                    DashboardOutIdList = DashboardRepository.Instance.GetIdBySearchParam(searchParamModel);
                }
                else
                {
                    DashboardOutIdList = new JavaScriptSerializer().Deserialize<List<Guid>>(dashboardIdParams);
                }

                foreach (var g in DashboardOutIdList)
                {
                    DashboardRepository.Instance.Delete(g, CurrentHRIS.DivisionName);
                }

                return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);


            }
            catch (Exception e)
            {
                result.ResultCode = false;
                result.ResultDesc = e.LogToApp("Delete Dashboard", MessageType.ERR, CurrentLogin.Username).Message;
                return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult Download(FormCollection collection, bool checkAll, string type)
        {
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = "File for download generated successfully"
            };
            string fullPath = string.Empty;
            try
            {
                var searchParam = collection["data"];
                var dashboardIdParams = collection["dashboardIdParams"];
                var searchParamModel = new DashboardSearch();
                List<Guid> dashboardIdList = new List<Guid>();

                if (!string.IsNullOrEmpty(searchParam))
                {
                    searchParamModel = new JavaScriptSerializer().Deserialize<DashboardSearch>(searchParam);
                }

                if (!string.IsNullOrEmpty(dashboardIdParams))
                {
                    dashboardIdList = new JavaScriptSerializer().Deserialize<List<Guid>>(dashboardIdParams);
                }


                searchParamModel.DashboardIdList = string.Join(";", dashboardIdList);

                //List<string[]> ListArr = new List<string[]>(); // array for the data

                if (type == "DownloadReport")
                {
                    HSSFWorkbook workbook = new HSSFWorkbook();
                    var headerStyle = HeaderStyleExcel(workbook);
                    var bodyStyle_Left = BodyStyleExcel(workbook);

                    var bodyStyle_Center = BodyStyleExcel(workbook);
                    bodyStyle_Center.Alignment = HorizontalAlignment.CENTER;

                    var bodyStyle_Right = BodyStyleExcel(workbook);
                    bodyStyle_Right.Alignment = HorizontalAlignment.RIGHT;

                    ISheet sheet = workbook.CreateSheet("Default");
                    var rowHeader = sheet.CreateRow(0);

                    String[] header = new string[]
                    {
                       "No",
                       "Nominative Type",
                       "Invoice Status",
                       "PV Type",
                       //"Settlement Status",
                       "Accrued Year End Status",
                       "Accrued Booking No",
                       "Description Transaction",
                       "GL Account",
                       "Vendor Name",
                       "NPWP",
                       "Invoice No",
                       "Invoice Date",
                       "Book Period",
                       "Turnover Amount",
                       "VAT Amount",
                       "WHT Amount",
                       "Tax Invoice No",
                       "Witholding Tax No",
                       "Witholding Tax Date",
                       "WHT Article",
                       "System",
                       "PV No",
                       "PV Created By",
                       "SAP GR Doc No",
                       "SAP GR Posting Date",
                       "SAP Invoice Doc No",
                       "SAP Invoice Posting Date",
                       "Receive File Date",
                       "Downloaded Date",
                       "Download Status",
                       "Nominative Form Attachment",
                       "Nominative ID No",
                       "Nominative Status",
                       "Nominative Date"
                    };

                    int indexCell = 0;
                    foreach (var item in header)
                    {
                        rowHeader.CreateCell(indexCell).SetCellValue(item);
                        rowHeader.GetCell(indexCell).CellStyle = headerStyle;

                        //default width
                        sheet.SetColumnWidth(indexCell, 5500);

                        //custome width
                        sheet.SetColumnWidth(0, 1500);
                        sheet.SetColumnWidth(4, 7000);
                        sheet.SetColumnWidth(6, 8000);
                        sheet.SetColumnWidth(8, 9000);
                        sheet.SetColumnWidth(26, 6000);
                        sheet.SetColumnWidth(30, 9000);
                        indexCell++;
                    }

                    sheet.GetRow(0).Height = 500;

                    var list = DashboardRepository.Instance.GetOriginalById(searchParamModel);
                    int i = 1;
                    foreach (var item in list)
                    {
                        var myArr = new string[] {
                            item.RowNum.ToString(),
                            item.NominativeType,
                            item.StatusInvoice,
                            item.PVType,
                            //item.SettlementStatus,
                            item.AccruedYearEndStatus,
                            item.AccruedBookingNo,
                            item.DescriptionTransaction,
                            item.GLAccount,
                            item.VendorName,
                            item.Npwp,
                            item.InvoiceNo,
                            item.InvoiceDate==null?"":item.InvoiceDate.Value.ToString("dd MMMM yyyy", CultureInfo.CreateSpecificCulture("id-ID")),
                            item.BookPeriod==null?"":item.BookPeriod.Value.ToString("dd MMMM yyyy", CultureInfo.CreateSpecificCulture("id-ID")),
                            item.TurnoverAmount==null?"":item.TurnoverAmount.Value.ToString("n2"),
                            item.VATAmount==null?"":item.VATAmount.Value.ToString("n2"),
                            item.WHTAmount==null?"":item.WHTAmount.Value.ToString("n2"),
                            item.TaxInvoiceNo,
                            item.WitholdingTaxNo,
                            item.WitholdingTaxDate==null?"":item.WitholdingTaxDate.Value.ToString("dd MMMM yyyy", CultureInfo.CreateSpecificCulture("id-ID")),
                            item.WHTArticle,
                            item.System,
                            item.PVNo,
                            item.PVCreatedBy,
                            item.SAPGRDocNo,
                            item.SAPGRPostingDate==null?"":item.SAPGRPostingDate.Value.ToString("dd MMMM yyyy", CultureInfo.CreateSpecificCulture("id-ID")),
                            item.SAPInvoiceDocNo,
                            item.SAPInvoicePostingDate==null?"":item.SAPInvoicePostingDate.Value.ToString("dd MMMM yyyy", CultureInfo.CreateSpecificCulture("id-ID")),
                            item.ReceiveFileDate==null?"":item.ReceiveFileDate.Value.ToString("dd MMMM yyyy", CultureInfo.CreateSpecificCulture("id-ID")),
                            item.DownloadedDate==null?"":item.DownloadedDate.Value.ToString("dd MMMM yyyy", CultureInfo.CreateSpecificCulture("id-ID")),
                            item.DownloadStatus,
                            item.NominativeFormAttachment,
                            item.NominativeIDNo,
                            item.NominativeStatus,
                            item.NominativeDate==null?"":item.NominativeDate.Value.ToString("dd MMMM yyyy", CultureInfo.CreateSpecificCulture("id-ID"))
                        };

                        var rowDetail = sheet.CreateRow(i);
                        indexCell = 0;
                        foreach (var value in myArr)
                        {
                            rowDetail.CreateCell(indexCell).SetCellValue(value);
                            rowDetail.GetCell(indexCell).CellStyle = bodyStyle_Left;
                            indexCell++;
                        }

                        //center position
                        var centerIndex = new int[] { 0, 7, 9, 11, 12, 18, 21, 24, 26, 27, 28, 31, 33 }; //No,  gl account, npwp,  invoice date, book period, Witholding TaxDate, PV No, SAP GRPosting Date, SAPInvoicePostingDate,  ReceiveFileDate, DownloadedDate, NominativeIDNo, NominativeDate
                        foreach (var z in centerIndex)
                        {
                            rowDetail.GetCell(z).CellStyle = bodyStyle_Center;
                        }

                        //right position
                        var rightIndex = new int[] { 13, 14, 15 }; //Turnover Amount, VAT Amount, WHT Amount
                        foreach (var z in rightIndex)
                        {
                            rowDetail.GetCell(z).CellStyle = bodyStyle_Right;
                        }

                        i++;
                    }

                    byte[] bytesXls;
                    using (var exportData = new MemoryStream())
                    {
                        workbook.Write(exportData);
                        bytesXls = exportData.ToArray();
                    }

                    fullPath = Path.Combine(TempDownloadFolder, "Dashboard_Report.xls");
                    using (var fs = new FileStream(fullPath, FileMode.Create, FileAccess.Write))
                    {
                        fs.Write(bytesXls, 0, bytesXls.Length);
                    }

                    #region Old
                    //String[] header = new string[]
                    //{
                    //   "Nominative Type",
                    //   "Invoice Status",
                    //   "PV Type",
                    //   //"Settlement Status",
                    //   "Accrued Year End Status",
                    //   "Accrued Booking No",
                    //   "Description Transaction",
                    //   "GL Account",
                    //   "Vendor Name",
                    //   "NPWP",
                    //   "Invoice No",
                    //   "Invoice Date",
                    //   "Book Period",
                    //   "Turnover Amount",
                    //   "VAT Amount",
                    //   "WHT Amount",
                    //   "Tax Invoice No",
                    //   "Witholding Tax No",
                    //   "Witholding Tax Date",
                    //   "WHT Article",
                    //   "System",
                    //   "PV No",
                    //   "PV Created By",
                    //   "SAP GR Doc No",
                    //   "SAP GR Posting Date",
                    //   "SAP Invoice Doc No",
                    //   "SAP Invoice Posting Date",
                    //   "Receive File Date",
                    //   "Downloaded Date",
                    //   "Download Status",
                    //   "Nominative Form Attachment",
                    //   "Nominative ID No",
                    //   "Nominative Status",
                    //   "Nominative Date"
                    //}; //for header

                    //ListArr.Add(header);

                    //var list = DashboardRepository.Instance.GetOriginalById(searchParamModel);
                    //foreach (var obj in list)
                    //{
                    //    String[] myArr = new string[] {
                    //    obj.NominativeType,
                    //    obj.StatusInvoice,
                    //    obj.PVType,
                    //    //obj.SettlementStatus,
                    //    obj.AccruedYearEndStatus,
                    //    obj.AccruedBookingNo,
                    //    obj.DescriptionTransaction,
                    //    obj.GLAccount,
                    //    obj.VendorName,
                    //    obj.Npwp,
                    //    obj.InvoiceNo,
                    //    obj.InvoiceDate==null?"":obj.InvoiceDate.Value.ToString("dd MMMM yyyy", CultureInfo.CreateSpecificCulture("id-ID")),
                    //    obj.BookPeriod==null?"":obj.BookPeriod.Value.ToString("dd MMMM yyyy", CultureInfo.CreateSpecificCulture("id-ID")),
                    //    obj.TurnoverAmount==null?"":obj.TurnoverAmount.Value.ToString("n2"),
                    //    obj.VATAmount==null?"":obj.VATAmount.Value.ToString("n2"),
                    //    obj.WHTAmount==null?"":obj.WHTAmount.Value.ToString("n2"),
                    //    obj.TaxInvoiceNo,
                    //    obj.WitholdingTaxNo,
                    //    obj.WitholdingTaxDate==null?"":obj.WitholdingTaxDate.Value.ToString("dd MMMM yyyy", CultureInfo.CreateSpecificCulture("id-ID")),
                    //    obj.WHTArticle,
                    //    obj.System,
                    //    obj.PVNo,
                    //    obj.PVCreatedBy,
                    //    obj.SAPGRDocNo,
                    //    obj.SAPGRPostingDate==null?"":obj.SAPGRPostingDate.Value.ToString("dd MMMM yyyy", CultureInfo.CreateSpecificCulture("id-ID")),
                    //    obj.SAPInvoiceDocNo,
                    //    obj.SAPInvoicePostingDate==null?"":obj.SAPInvoicePostingDate.Value.ToString("dd MMMM yyyy", CultureInfo.CreateSpecificCulture("id-ID")),
                    //    obj.ReceiveFileDate==null?"":obj.ReceiveFileDate.Value.ToString("dd MMMM yyyy", CultureInfo.CreateSpecificCulture("id-ID")),
                    //    obj.DownloadedDate==null?"":obj.DownloadedDate.Value.ToString("dd MMMM yyyy", CultureInfo.CreateSpecificCulture("id-ID")),
                    //    obj.DownloadStatus,
                    //    obj.NominativeFormAttachment,
                    //    obj.NominativeIDNo,
                    //    obj.NominativeStatus,
                    //    obj.NominativeDate==null?"":obj.NominativeDate.Value.ToString("dd MMMM yyyy", CultureInfo.CreateSpecificCulture("id-ID"))
                    //};
                    //    ListArr.Add(myArr);
                    //}
                    //fullPath = XlsHelper.PutExcel(ListArr, new Dashboard(), TempDownloadFolder, "Dashboard_Report");
                    ////totalDataCount = list.Count();
                    ////result.ResultDesc = totalDataCount + " data successfully downloaded";
                    #endregion
                }
                else if (type == "DownloadFromXls")
                {
                    List<string> filePaths = new List<string>();

                    var listCategoryCode = DashboardRepository.Instance.GetNominativeTypeDashboard(searchParamModel);
                    if (listCategoryCode.Where(x => x.CategoryCode == EnumCategoryCode.Promotion).Count() > 0)
                    {
                        ////PROMOTION
                        var header = PromotionRepository.Instance.GetHeaderPromotion();
                        var listPromotion = DashboardRepository.Instance.GetPromotionFromExcel(searchParamModel);
                        filePaths.Add(GenerateFileExcelPromotion(header, listPromotion));

                        #region Old
                        //var header = PromotionRepository.Instance.GetHeaderPromotion();
                        //List<string[]> ListArrPromotion = new List<string[]>(); // array for the data
                        //var headerPromotion = new string[]
                        //{
                        //    header.NO,
                        //    header.NAMA,
                        //    header.NPWP,
                        //    header.ALAMAT,
                        //    header.TANGGAL,
                        //    header.BENTUK_DAN_JENIS_BIAYA,
                        //    header.JUMLAH,
                        //    header.JUMLAH_GROSS_UP,
                        //    header.KETERANGAN,
                        //    header.JUMLAH_PPH,
                        //    header.JENIS_PPH,
                        //   "Nomor Bukti Potong",
                        //    header.JUMLAH_NET,
                        //    header.NOMOR_REKENING,
                        //    header.NAMA_REKENING_PENERIMA,
                        //    header.NAMA_BANK,
                        //    header.NO_KTP
                        //};
                        //ListArrPromotion.Add(headerPromotion);

                        //var listPromotion = DashboardRepository.Instance.GetPromotionFromExcel(searchParamModel);
                        //foreach (var obj in listPromotion)
                        //{
                        //    String[] myArr = new string[] {
                        //    obj.NO.ToString(),
                        //    obj.NAMA,
                        //    obj.NPWP,
                        //    obj.ALAMAT,
                        //    obj.TANGGAL==null?"":obj.TANGGAL.Value.ToString("dd MMMM yyyy", CultureInfo.CreateSpecificCulture("id-ID")),
                        //    obj.BENTUK_DAN_JENIS_BIAYA,
                        //    obj.JUMLAH==null?"":obj.JUMLAH.Value.ToString("n2"),
                        //    obj.JUMLAH_GROSS_UP==null?"":obj.JUMLAH_GROSS_UP.Value.ToString("n2"),
                        //    obj.KETERANGAN,
                        //    obj.JUMLAH_PPH==null?"":obj.JUMLAH_PPH.Value.ToString("n2"),
                        //    obj.JENIS_PPH,
                        //    obj.NOMOR_BUKTI_POTONG,
                        //    obj.JUMLAH_NET==null?"":obj.JUMLAH_NET.Value.ToString("n2"),
                        //    obj.NOMOR_REKENING,
                        //    obj.NAMA_REKENING_PENERIMA,
                        //    obj.NAMA_BANK,
                        //    obj.NO_KTP,
                        //};

                        //    ListArrPromotion.Add(myArr);
                        //}

                        //filePaths.Add(XlsHelper.PutExcel(ListArrPromotion, new PromotionReportXls(), TempDownloadFolder, "Promotion.xls", true));
                        #endregion
                    }

                    if (listCategoryCode.Where(x => x.CategoryCode == EnumCategoryCode.Donation).Count() > 0)
                    {
                        ////DONATION
                        var header = DonationRepository.Instance.GetHeaderDonation();
                        var listDonation = DashboardRepository.Instance.GetDonationFrom(searchParamModel);
                        filePaths.Add(GenerateFileExcelDonation(header, listDonation));

                        #region Old
                        //var header = DonationRepository.Instance.GetHeaderDonation();
                        //List<string[]> ListArrDonation = new List<string[]>(); // array for the data
                        //var headerDonation = new string[]
                        //{
                        //    header.JENIS_SUMBANGAN,
                        //    header.BENTUK_SUMBANGAN,
                        //    header.NILAI_SUMBANGAN,
                        //    header.TANGGAL_SUMBANGAN,
                        //    header.NAMA_LEMBAGA_PENERIMA,
                        //    header.NPWP_LEMBAGA_PENERIMA,
                        //    header.ALAMAT_LEMBAGA,
                        //    header.NO_TELP_LEMBAGA,
                        //    header.SARANA_PRASARANA_INFRASTRUKTUR,
                        //    header.LOKASI_INFRASTRUKTUR,
                        //    header.IZIN_MENDIRIKAN_BANGUNAN,
                        //    header.NOMOR_REKENING,
                        //    header.NAMA_REKENING_PENERIMA,
                        //    header.NAMA_BANK
                        //};
                        //ListArrDonation.Add(headerDonation);

                        //var listDonation = DashboardRepository.Instance.GetDonationFrom(searchParamModel);
                        //foreach (var obj in listDonation)
                        //{
                        //    String[] myArr = new string[] {
                        //    obj.JENIS_SUMBANGAN,
                        //    obj.BENTUK_SUMBANGAN,
                        //    obj.NILAI_SUMBANGAN==null?"":obj.NILAI_SUMBANGAN.Value.ToString("n2"),
                        //    obj.TANGGAL_SUMBANGAN==null?"":obj.TANGGAL_SUMBANGAN.Value.ToString("dd MMMM yyyy", CultureInfo.CreateSpecificCulture("id-ID")),
                        //    obj.NAMA_LEMBAGA_PENERIMA,
                        //    obj.NPWP_LEMBAGA_PENERIMA,
                        //    obj.ALAMAT_LEMBAGA,
                        //    obj.NO_TELP_LEMBAGA,
                        //    obj.SARANA_PRASARANA_INFRASTRUKTUR,
                        //    obj.LOKASI_INFRASTRUKTUR,
                        //    obj.IZIN_MENDIRIKAN_BANGUNAN,
                        //    obj.NOMOR_REKENING,
                        //    obj.NAMA_REKENING_PENERIMA,
                        //    obj.NAMA_BANK,
                        //};

                        //    ListArrDonation.Add(myArr);
                        //}

                        //filePaths.Add(XlsHelper.PutExcel(ListArrDonation, new DonationReportXls(), TempDownloadFolder, "Donation.xls", true));
                        #endregion
                    }

                    if (listCategoryCode.Where(x => x.CategoryCode == EnumCategoryCode.Entertainment).Count() > 0)
                    {
                        //ENTERTAINMENT
                        var header = EntertainmentRepository.Instance.GetHeaderEntertainment();
                        var listEntertainment = DashboardRepository.Instance.GetEntertainmentFrom(searchParamModel);
                        filePaths.Add(GenerateFileExcelEntertainment(header, listEntertainment));

                        #region Old
                        //var header = EntertainmentRepository.Instance.GetHeaderEntertainment();
                        //List<string[]> ListArrEntertainment = new List<string[]>(); // array for the data
                        //var headerEntertainment = new string[]
                        //{
                        //    header.NO,
                        //    header.TANGGAL,
                        //    header.TEMPAT,
                        //    header.ALAMAT,
                        //    header.TIPE_ENTERTAINMENT,
                        //    header.JUMLAH,
                        //    header.NAMA_RELASI,
                        //    header.JABATAN,
                        //    header.ALAMAT_PERUSAHAAN,
                        //    header.TIPE_BISNIS,
                        //    header.KETERANGAN
                        //};
                        //ListArrEntertainment.Add(headerEntertainment);

                        //var listEntertainment = DashboardRepository.Instance.GetEntertainmentFrom(searchParamModel);
                        //foreach (var obj in listEntertainment)
                        //{
                        //    String[] myArr = new string[] {
                        //    obj.NO.ToString(),
                        //    obj.TANGGAL==null?"":obj.TANGGAL.Value.ToString("dd MMMM yyyy", CultureInfo.CreateSpecificCulture("id-ID")),
                        //    obj.TEMPAT,
                        //    obj.ALAMAT,
                        //    obj.BENTUK_DAN_JENIS_ENTERTAINMENT,
                        //    obj.JUMLAH==null?"":obj.JUMLAH.Value.ToString("n2"),
                        //    obj.NAMA_RELASI,
                        //    obj.POSISI_RELASI,
                        //    obj.PERUSAHAAN_RELASI,
                        //    obj.JENIS_USAHA_RELASI,
                        //    obj.KETERANGAN,
                        //};

                        //    ListArrEntertainment.Add(myArr);
                        //}

                        //filePaths.Add(XlsHelper.PutExcel(ListArrEntertainment, new EntertainmentReportXls(), TempDownloadFolder, "Entertainment.xls", true));
                        #endregion
                    }

                    if (filePaths.Count > 1)
                    {
                        fullPath = GlobalFunction.GenerateZipFile(filePaths, TempZipFolder);
                        var newPath = Path.Combine(TempZipFolder, $"Nominative_Form_xls_{DateTime.Now.ToString("dd - MM - yyyy_HHmmss")}.zip");
                        if (!string.IsNullOrEmpty(fullPath))
                        {
                            System.IO.File.Move(fullPath, newPath);
                            fullPath = newPath;
                        }
                    }
                    else
                    {
                        fullPath = filePaths.FirstOrDefault();
                    }
                }
                else if (type == "DownloadFromPdf")
                {
                    //proses save file pdf jika path masih kosong
                    var listPathNullable = DashboardRepository.Instance.GetNullablePathFromPdf(searchParamModel);
                    foreach (var item in listPathNullable.Where(x => x.CategoryCode == EnumCategoryCode.Donation))
                    {
                        //DonationRepository.Instance.SaveFile(TempDownloadFolder, Server.MapPath("~/FileStorage/TemplateGenerate/TemplateDonation.docx"), item.DashboardId);
                        DonationRepository.Instance.SaveFile(TempDownloadFolder, Server.MapPath("~/Report/RDLC/DataFieldDonation.rdlc"), item.DashboardId);
                    }

                    foreach (var item in listPathNullable.Where(x => x.CategoryCode == EnumCategoryCode.Promotion))
                    {
                        PromotionRepository.Instance.SaveFile(TempDownloadFolder, Server.MapPath("~/Report/RDLC/DataFieldPromotion.rdlc"), item.DashboardId);
                    }

                    foreach (var item in listPathNullable.Where(x => x.CategoryCode == EnumCategoryCode.Entertainment))
                    {
                        EntertainmentRepository.Instance.SaveFile(TempDownloadFolder, Server.MapPath("~/Report/RDLC/DataFieldEntertainment.rdlc"), item.DashboardId);
                    }

                    List<string> filePaths = new List<string>();
                    var listPathFromPdf = DashboardRepository.Instance.GetPathFromPdf(searchParamModel);
                    var dataDonation = listPathFromPdf.Where(x => x.CategoryCode == EnumCategoryCode.Donation);
                    if (dataDonation.Count() > 0)
                    {
                        var donationDOC = dataDonation.Where(x => Path.GetExtension(x.FromPdf) == ".doc");
                        if (donationDOC.Count() > 0)
                        {
                            var MergeDoc = new Spire.Doc.Document();
                            foreach (var item in donationDOC)
                            {
                                var document = new Spire.Doc.Document();
                                document.LoadFromFile(item.FromPdf, FileFormat.Docx);

                                foreach (Spire.Doc.Section sec in document.Sections)
                                {
                                    MergeDoc.Sections.Add(sec.Clone());
                                }
                            }

                            var path = Path.Combine(TempDownloadFolder, "Donation.doc");
                            if (donationDOC.Count() == 1) path = Path.Combine(TempDownloadFolder, Path.GetFileName(donationDOC.Select(x => x.FromPdf).FirstOrDefault()));

                            MergeDoc.SaveToFile(path, Spire.Doc.FileFormat.Docx2013);
                            filePaths.Add(path);
                        }

                        var donationPDF = dataDonation.Where(x => Path.GetExtension(x.FromPdf) == ".pdf").Select(x => x.FromPdf);
                        if (donationPDF.Count() > 0)
                        {
                            var pathPdf = Path.Combine(TempDownloadFolder, "Donation.pdf");
                            if (donationPDF.Count() == 1) pathPdf = Path.Combine(TempDownloadFolder, Path.GetFileName(donationPDF.FirstOrDefault()));

                            MergePDF(pathPdf, donationPDF.ToArray());
                            filePaths.Add(pathPdf);
                        }
                    }

                    var dataPromotion = listPathFromPdf.Where(x => x.CategoryCode == EnumCategoryCode.Promotion);
                    if (dataPromotion.Count() > 0)
                    {
                        var path = Path.Combine(TempDownloadFolder, "Promotion.pdf");
                        if (dataPromotion.Count() == 1) path = Path.Combine(TempDownloadFolder, Path.GetFileName(dataPromotion.Select(x => x.FromPdf).FirstOrDefault()));
                        MergePDF(path, dataPromotion.Select(x => x.FromPdf).ToArray());
                        filePaths.Add(path);
                    }

                    var dataEntertainment = listPathFromPdf.Where(x => x.CategoryCode == EnumCategoryCode.Entertainment);
                    if (dataEntertainment.Count() > 0)
                    {

                        var path = Path.Combine(TempDownloadFolder, "Entertainment.pdf");
                        if (dataEntertainment.Count() == 1) path = Path.Combine(TempDownloadFolder, Path.GetFileName(dataEntertainment.Select(x => x.FromPdf).FirstOrDefault()));
                        MergePDF(path, dataEntertainment.Select(x => x.FromPdf).ToArray());
                        filePaths.Add(path);
                    }

                    if (filePaths.Count > 1)
                    {
                        fullPath = GlobalFunction.GenerateZipFile(filePaths, TempZipFolder);

                        var newPath = Path.Combine(TempZipFolder, $"Nominative_Form_Pdf_{DateTime.Now.ToString("dd-MM-yyyy_HHmmss")}.zip");
                        if (!string.IsNullOrEmpty(fullPath))
                        {
                            System.IO.File.Move(fullPath, newPath);
                            fullPath = newPath;
                        }
                    }
                    else
                    {
                        fullPath = filePaths.FirstOrDefault();
                    }
                    DashboardRepository.Instance.LastDownloaded(searchParamModel.DashboardIdList);

                }
                else if (type == "DownloadReportXls")
                {
                    var filePaths = GenerateReport(searchParamModel, "xls");
                    if (filePaths.Count > 1)
                    {
                        fullPath = GlobalFunction.GenerateZipFile(filePaths, TempZipFolder);
                        var newPath = Path.Combine(TempZipFolder, $"Nominative_Report_Xls_{DateTime.Now.ToString("dd - MM - yyyy_HHmmss")}.zip");
                        if (!string.IsNullOrEmpty(fullPath))
                        {
                            System.IO.File.Move(fullPath, newPath);
                            fullPath = newPath;
                        }
                    }
                    else
                    {
                        fullPath = filePaths.FirstOrDefault();
                    }
                }
                else if (type == "DownloadReportPdf")
                {
                    var filePaths = GenerateReport(searchParamModel, "pdf");
                    if (filePaths.Count > 1)
                    {
                        fullPath = GlobalFunction.GenerateZipFile(filePaths, TempZipFolder);
                        var newPath = Path.Combine(TempZipFolder, $"Nominative_Report_Pdf_{DateTime.Now.ToString("dd - MM - yyyy_HHmmss")}.zip");
                        if (!string.IsNullOrEmpty(fullPath))
                        {
                            System.IO.File.Move(fullPath, newPath);
                            fullPath = newPath;
                        }
                    }
                    else
                    {
                        fullPath = filePaths.FirstOrDefault();
                    }
                }

                return Json(result.AsDynamic(fullPath), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                result.ResultCode = false;
                result.ResultDesc = "There is no file found";
                string dataerror = e.LogToApp(Path.GetFileName(fullPath), MessageType.ERR, CurrentLogin.Username).Message;
                return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);
            }
        }

        public ICellStyle HeaderStyleExcel(HSSFWorkbook workbook)
        {
            var headerFont = workbook.CreateFont();
            headerFont.Boldweight = (short)FontBoldWeight.BOLD;
            headerFont.Color = HSSFColor.BLACK.index;
            headerFont.FontHeightInPoints = 11;
            headerFont.FontName = "Calibri";

            ICellStyle headerStyle = workbook.CreateCellStyle();
            headerStyle.SetFont(headerFont);
            headerStyle.FillForegroundColor = HSSFColor.LIGHT_GREEN.index;
            headerStyle.FillPattern = FillPatternType.SOLID_FOREGROUND;

            headerStyle.BorderBottom = BorderStyle.THIN;
            headerStyle.BottomBorderColor = HSSFColor.BLACK.index;
            headerStyle.BorderLeft = BorderStyle.THIN;
            headerStyle.LeftBorderColor = HSSFColor.BLACK.index;
            headerStyle.BorderRight = BorderStyle.THIN;
            headerStyle.RightBorderColor = HSSFColor.BLACK.index;
            headerStyle.BorderTop = BorderStyle.THIN;
            headerStyle.TopBorderColor = HSSFColor.BLACK.index;

            headerStyle.Alignment = HorizontalAlignment.CENTER;
            headerStyle.VerticalAlignment = VerticalAlignment.CENTER;

            return headerStyle;
        }

        public ICellStyle BodyStyleExcel(HSSFWorkbook workbook)
        {
            var bodyFont = workbook.CreateFont();
            //bodyFont.Boldweight = (short)FontBoldWeight.BOLD;
            bodyFont.Color = HSSFColor.BLACK.index;
            bodyFont.FontHeightInPoints = 10;
            bodyFont.FontName = "Calibri";

            ICellStyle bodyStyle = workbook.CreateCellStyle();
            bodyStyle.SetFont(bodyFont);
            //bodyStyle.FillForegroundColor = HSSFColor.LightGreen.Index;
            //bodyStyle.FillPattern = FillPattern.SolidForeground;

            bodyStyle.BorderBottom = BorderStyle.THIN;
            bodyStyle.BottomBorderColor = HSSFColor.BLACK.index;
            bodyStyle.BorderLeft = BorderStyle.THIN;
            bodyStyle.LeftBorderColor = HSSFColor.BLACK.index;
            bodyStyle.BorderRight = BorderStyle.THIN;
            bodyStyle.RightBorderColor = HSSFColor.BLACK.index;
            bodyStyle.BorderTop = BorderStyle.THIN;
            bodyStyle.TopBorderColor = HSSFColor.BLACK.index;

            bodyStyle.Alignment = HorizontalAlignment.LEFT;
            bodyStyle.VerticalAlignment = VerticalAlignment.CENTER;

            return bodyStyle;
        }

        public ICellStyle MergeStyleExcel(HSSFWorkbook workbook)
        {
            var bodyFont = workbook.CreateFont();
            //bodyFont.Boldweight = (short)FontBoldWeight.BOLD;
            bodyFont.Color = HSSFColor.BLACK.index;
            bodyFont.FontHeightInPoints = 10;
            bodyFont.FontName = "Calibri";

            ICellStyle mergeStyle = workbook.CreateCellStyle();
            mergeStyle.SetFont(bodyFont);
            //bodyStyle.FillForegroundColor = HSSFColor.LightGreen.Index;
            //bodyStyle.FillPattern = FillPattern.SolidForeground;

            mergeStyle.BorderBottom = BorderStyle.THIN;
            mergeStyle.BottomBorderColor = HSSFColor.BLACK.index;
            mergeStyle.BorderLeft = BorderStyle.THIN;
            mergeStyle.LeftBorderColor = HSSFColor.BLACK.index;
            mergeStyle.BorderRight = BorderStyle.THIN;
            mergeStyle.RightBorderColor = HSSFColor.BLACK.index;
            mergeStyle.BorderTop = BorderStyle.THIN;
            mergeStyle.TopBorderColor = HSSFColor.BLACK.index;

            mergeStyle.Alignment = HorizontalAlignment.LEFT;
            mergeStyle.VerticalAlignment = VerticalAlignment.CENTER;

            return mergeStyle;
        }

        public string GenerateFileExcelEntertainment(EntertainmentHeaderTemplate headerValue, List<EntertainmentReportXls> list)
        {
            HSSFWorkbook workbook = new HSSFWorkbook();
            var headerStyle = HeaderStyleExcel(workbook);
            var bodyStyle = BodyStyleExcel(workbook);

            foreach (var nominativeNo in list.Select(x => x.NominativeIDNo).Distinct())
            {
                ISheet sheet = workbook.CreateSheet(nominativeNo);

                var rowHeader = sheet.CreateRow(0);
                rowHeader.CreateCell(0).SetCellValue(headerValue.NO);
                rowHeader.CreateCell(1).SetCellValue(headerValue.TANGGAL);
                rowHeader.CreateCell(2).SetCellValue(headerValue.TEMPAT);
                rowHeader.CreateCell(3).SetCellValue(headerValue.ALAMAT);
                rowHeader.CreateCell(4).SetCellValue(headerValue.TIPE_ENTERTAINMENT);
                rowHeader.CreateCell(5).SetCellValue(headerValue.JUMLAH);
                rowHeader.CreateCell(6).SetCellValue(headerValue.NAMA_RELASI);
                rowHeader.CreateCell(7).SetCellValue(headerValue.JABATAN);
                rowHeader.CreateCell(8).SetCellValue(headerValue.ALAMAT_PERUSAHAAN);
                rowHeader.CreateCell(9).SetCellValue(headerValue.TIPE_BISNIS);
                rowHeader.CreateCell(10).SetCellValue(headerValue.KETERANGAN);

                for (int z = 0; z <= 10; z++)
                {
                    rowHeader.GetCell(z).CellStyle = headerStyle;
                    sheet.SetColumnWidth(z, 6000);
                }

                sheet.SetColumnWidth(0, 1500);
                sheet.SetColumnWidth(2, 5000);
                sheet.SetColumnWidth(3, 8000);
                sheet.SetColumnWidth(4, 8000);
                sheet.SetColumnWidth(5, 5000);
                sheet.SetColumnWidth(10, 8000);
                sheet.GetRow(0).Height = 500;

                var GroupListRow = new Dictionary<int, int>();//Key = first row, Value = Last row
                int GroupNo = 0, firtRow = 0, lastRow = 0, IndexNo = 0;
                int i = 1;
                foreach (var item in list.Where(x => x.NominativeIDNo == nominativeNo).OrderBy(x => x.NO).ThenBy(x => x.TANGGAL))
                {
                    if (GroupNo == item.NO)
                    {
                        IndexNo += 1;
                    }

                    if (GroupNo != item.NO)
                    {
                        if (firtRow != i)
                        {
                            lastRow = i - 1;//dikurangi 1 dikarenakan proses add groupnya ketika pindah no
                            if (firtRow != lastRow)
                            {
                                GroupListRow.Add(firtRow, lastRow);
                            }
                            IndexNo = 0;
                        }

                        if (IndexNo == 0)
                        {
                            firtRow = i;
                        }

                        GroupNo = (item.NO ?? 0);
                    }

                    var rowDetail = sheet.CreateRow(i);
                    rowDetail.CreateCell(0).SetCellValue((item.NO ?? 0));
                    rowDetail.CreateCell(1).SetCellValue(item.TANGGAL == null ? "" : item.TANGGAL.Value.ToString("dd MMMM yyyy", CultureInfo.CreateSpecificCulture("id-ID")));
                    rowDetail.CreateCell(2).SetCellValue(item.TEMPAT);
                    rowDetail.CreateCell(3).SetCellValue(item.ALAMAT);
                    rowDetail.CreateCell(4).SetCellValue(item.BENTUK_DAN_JENIS_ENTERTAINMENT);
                    rowDetail.CreateCell(5).SetCellValue(item.JUMLAH == null ? "" : item.JUMLAH.Value.ToString("n0"));
                    rowDetail.CreateCell(6).SetCellValue(item.NAMA_RELASI);
                    rowDetail.CreateCell(7).SetCellValue(item.POSISI_RELASI);
                    rowDetail.CreateCell(8).SetCellValue(item.PERUSAHAAN_RELASI);
                    rowDetail.CreateCell(9).SetCellValue(item.JENIS_USAHA_RELASI);
                    rowDetail.CreateCell(10).SetCellValue(item.KETERANGAN);

                    for (int z = 0; z <= 10; z++) { rowDetail.GetCell(z).CellStyle = bodyStyle; }

                    i++;
                }

                var lastIndex = list.Where(x => x.NominativeIDNo == nominativeNo).Count();
                if (firtRow != lastIndex)
                {
                    if (GroupListRow.Where(x => x.Key == firtRow).Count() == 0)
                    {
                        GroupListRow.Add(firtRow, lastIndex);
                    }
                }

                var mergeStyle_Left = MergeStyleExcel(workbook);

                var mergeStyle_Center = MergeStyleExcel(workbook);
                mergeStyle_Center.Alignment = HorizontalAlignment.CENTER;

                var mergeStyle_Right = MergeStyleExcel(workbook);
                mergeStyle_Right.Alignment = HorizontalAlignment.RIGHT;

                foreach (var merge in GroupListRow)
                {
                    sheet.AddMergedRegion(new CellRangeAddress(merge.Key, merge.Value, 0, 0));//No
                    sheet.AddMergedRegion(new CellRangeAddress(merge.Key, merge.Value, 1, 1));//Tanggal Entertain
                    sheet.AddMergedRegion(new CellRangeAddress(merge.Key, merge.Value, 2, 2));//Tempat
                    sheet.AddMergedRegion(new CellRangeAddress(merge.Key, merge.Value, 3, 3));//Alamat
                    sheet.AddMergedRegion(new CellRangeAddress(merge.Key, merge.Value, 4, 4));//Bentuk dan Jenis Entertain
                    sheet.AddMergedRegion(new CellRangeAddress(merge.Key, merge.Value, 5, 5));//Jumlah

                    var rowDetailMerge = sheet.GetRow(merge.Key);
                    for (int z = 0; z <= 5; z++) { rowDetailMerge.GetCell(z).CellStyle = mergeStyle_Left; }

                    //center position
                    var centerIndex = new int[] { 0, 1 }; //No, Tanggal, 
                    foreach (var z in centerIndex)
                    {
                        rowDetailMerge.GetCell(z).CellStyle = mergeStyle_Center;
                    }

                    //right position
                    var rightIndex = new int[] { 5 }; //Jumlah
                    foreach (var z in rightIndex)
                    {
                        rowDetailMerge.GetCell(z).CellStyle = mergeStyle_Right;
                    }
                }
            }

            byte[] bytesXls;
            using (var exportData = new MemoryStream())
            {
                workbook.Write(exportData);
                bytesXls = exportData.ToArray();
            }

            var path = Path.Combine(TempDownloadFolder, "Entertainment.xls");
            using (var fs = new FileStream(path, FileMode.Create, FileAccess.Write))
            {
                fs.Write(bytesXls, 0, bytesXls.Length);
            }

            return path;
        }

        public string GenerateFileExcelPromotion(PromotionHeaderTemplate headerValue, List<PromotionReportXls> list)
        {
            HSSFWorkbook workbook = new HSSFWorkbook();
            var headerStyle = HeaderStyleExcel(workbook);
            var bodyStyle_Left = BodyStyleExcel(workbook);

            var bodyStyle_Center = BodyStyleExcel(workbook);
            bodyStyle_Center.Alignment = HorizontalAlignment.CENTER;

            var bodyStyle_Right = BodyStyleExcel(workbook);
            bodyStyle_Right.Alignment = HorizontalAlignment.RIGHT;

            foreach (var nominativeNo in list.Select(x => x.NominativeIDNo).Distinct())
            {
                ISheet sheet = workbook.CreateSheet(nominativeNo);
                var rowHeader = sheet.CreateRow(0);
                rowHeader.CreateCell(0).SetCellValue(headerValue.NO);
                rowHeader.CreateCell(1).SetCellValue(headerValue.NAMA);
                rowHeader.CreateCell(2).SetCellValue(headerValue.NPWP);
                rowHeader.CreateCell(3).SetCellValue(headerValue.ALAMAT);
                rowHeader.CreateCell(4).SetCellValue(headerValue.TANGGAL);
                rowHeader.CreateCell(5).SetCellValue(headerValue.BENTUK_DAN_JENIS_BIAYA);
                rowHeader.CreateCell(6).SetCellValue(headerValue.JUMLAH);
                rowHeader.CreateCell(7).SetCellValue(headerValue.JUMLAH_GROSS_UP);
                rowHeader.CreateCell(8).SetCellValue(headerValue.KETERANGAN);
                rowHeader.CreateCell(9).SetCellValue(headerValue.JUMLAH_PPH);
                rowHeader.CreateCell(10).SetCellValue(headerValue.JENIS_PPH);
                rowHeader.CreateCell(11).SetCellValue("Nomor Bukti Potong");
                rowHeader.CreateCell(12).SetCellValue(headerValue.JUMLAH_NET);
                rowHeader.CreateCell(13).SetCellValue(headerValue.NOMOR_REKENING);
                rowHeader.CreateCell(14).SetCellValue(headerValue.NAMA_REKENING_PENERIMA);
                rowHeader.CreateCell(15).SetCellValue(headerValue.NAMA_BANK);
                rowHeader.CreateCell(16).SetCellValue(headerValue.NO_KTP);

                for (int z = 0; z <= 16; z++)
                {
                    rowHeader.GetCell(z).CellStyle = headerStyle;
                    sheet.SetColumnWidth(z, 5000);
                }

                sheet.SetColumnWidth(0, 1500);
                sheet.SetColumnWidth(3, 8000);
                sheet.SetColumnWidth(4, 4000);
                sheet.SetColumnWidth(5, 8000);
                sheet.SetColumnWidth(8, 8000);
                sheet.SetColumnWidth(14, 7000);
                sheet.GetRow(0).Height = 500;

                var GroupListRow = new Dictionary<int, int>();//Key = first row, Value = Last row
                int GroupNo = 0, firtRow = 0, lastRow = 0, IndexNo = 0;
                int i = 1;
                foreach (var item in list.Where(x => x.NominativeIDNo == nominativeNo).OrderBy(x => x.NO).ThenBy(x => x.TANGGAL))
                {
                    if (GroupNo == item.NO)
                    {
                        IndexNo += 1;
                    }

                    if (GroupNo != item.NO)
                    {
                        if (firtRow != i)
                        {
                            lastRow = i - 1;//dikurangi 1 dikarenakan proses add groupnya ketika pindah no
                            if (firtRow != lastRow)
                            {
                                GroupListRow.Add(firtRow, lastRow);
                            }
                            IndexNo = 0;
                        }

                        if (IndexNo == 0)
                        {
                            firtRow = i;
                        }

                        GroupNo = (item.NO ?? 0);
                    }

                    var rowDetail = sheet.CreateRow(i);
                    rowDetail.CreateCell(0).SetCellValue((item.NO ?? 0));
                    rowDetail.CreateCell(1).SetCellValue(item.NAMA);
                    rowDetail.CreateCell(2).SetCellValue(item.NPWP);
                    rowDetail.CreateCell(3).SetCellValue(item.ALAMAT);
                    rowDetail.CreateCell(4).SetCellValue(item.TANGGAL == null ? "" : item.TANGGAL.Value.ToString("dd MMMM yyyy", CultureInfo.CreateSpecificCulture("id-ID")));
                    rowDetail.CreateCell(5).SetCellValue(item.BENTUK_DAN_JENIS_BIAYA);
                    rowDetail.CreateCell(6).SetCellValue(item.JUMLAH == null ? "" : item.JUMLAH.Value.ToString("n0"));
                    rowDetail.CreateCell(7).SetCellValue(item.JUMLAH_GROSS_UP == null ? "" : item.JUMLAH_GROSS_UP.Value.ToString("n0"));
                    rowDetail.CreateCell(8).SetCellValue(item.KETERANGAN);
                    rowDetail.CreateCell(9).SetCellValue(item.JUMLAH_PPH == null ? "" : item.JUMLAH_PPH.Value.ToString("n0"));
                    rowDetail.CreateCell(10).SetCellValue(item.JENIS_PPH);
                    rowDetail.CreateCell(11).SetCellValue(item.NOMOR_BUKTI_POTONG);
                    rowDetail.CreateCell(12).SetCellValue(item.JUMLAH_NET == null ? "" : item.JUMLAH_NET.Value.ToString("n0"));
                    rowDetail.CreateCell(13).SetCellValue(item.NOMOR_REKENING);
                    rowDetail.CreateCell(14).SetCellValue(item.NAMA_REKENING_PENERIMA);
                    rowDetail.CreateCell(15).SetCellValue(item.NAMA_BANK);
                    rowDetail.CreateCell(16).SetCellValue(item.NO_KTP);

                    for (int z = 0; z <= 16; z++) { rowDetail.GetCell(z).CellStyle = bodyStyle_Left; }

                    //center position
                    var centerIndex = new int[] { 4, 16 }; //Tanggal, NO KTP
                    foreach (var z in centerIndex)
                    {
                        rowDetail.GetCell(z).CellStyle = bodyStyle_Center;
                    }

                    //right position
                    var rightIndex = new int[] { 6, 7, 9, 12 }; //Jumlah, Jumlah GrossUp, Jumlah PPH, Jumlah Net, 
                    foreach (var z in rightIndex)
                    {
                        rowDetail.GetCell(z).CellStyle = bodyStyle_Right;
                    }

                    i++;
                }

                var lastIndex = list.Where(x => x.NominativeIDNo == nominativeNo).Count();
                if (firtRow != lastIndex)
                {
                    if (GroupListRow.Where(x => x.Key == firtRow).Count() == 0)
                    {
                        GroupListRow.Add(firtRow, lastIndex);
                    }
                }

                var mergeStyle_Left = MergeStyleExcel(workbook);

                var mergeStyle_Center = MergeStyleExcel(workbook);
                mergeStyle_Center.Alignment = HorizontalAlignment.CENTER;

                foreach (var merge in GroupListRow)
                {
                    sheet.AddMergedRegion(new CellRangeAddress(merge.Key, merge.Value, 0, 0));//no
                    sheet.AddMergedRegion(new CellRangeAddress(merge.Key, merge.Value, 1, 1));//nama
                    sheet.AddMergedRegion(new CellRangeAddress(merge.Key, merge.Value, 2, 2));//npwp
                    sheet.AddMergedRegion(new CellRangeAddress(merge.Key, merge.Value, 3, 3));//alamat

                    var rowDetailMerge = sheet.GetRow(merge.Key);
                    for (int z = 0; z <= 3; z++) { rowDetailMerge.GetCell(z).CellStyle = mergeStyle_Left; }

                    //center position
                    var centerIndex = new int[] { 0, 2 }; //No, npwp, 
                    foreach (var z in centerIndex)
                    {
                        rowDetailMerge.GetCell(z).CellStyle = mergeStyle_Center;
                    }
                }
            }

            byte[] bytesXls;
            using (var exportData = new MemoryStream())
            {
                workbook.Write(exportData);
                bytesXls = exportData.ToArray();
            }

            var path = Path.Combine(TempDownloadFolder, "Promotion.xls");
            using (var fs = new FileStream(path, FileMode.Create, FileAccess.Write))
            {
                fs.Write(bytesXls, 0, bytesXls.Length);
            }

            return path;
        }

        public string GenerateFileExcelDonation(DonationHeaderTemplate headerValue, List<DonationReportXls> list)
        {
            HSSFWorkbook workbook = new HSSFWorkbook();
            var headerStyle = HeaderStyleExcel(workbook);
            var bodyStyle_Left = BodyStyleExcel(workbook);

            var bodyStyle_Center = BodyStyleExcel(workbook);
            bodyStyle_Center.Alignment = HorizontalAlignment.CENTER;

            var bodyStyle_Right = BodyStyleExcel(workbook);
            bodyStyle_Right.Alignment = HorizontalAlignment.RIGHT;

            foreach (var nominativeNo in list.Select(x => x.NominativeIDNo).Distinct())
            {
                ISheet sheet = workbook.CreateSheet(nominativeNo);
                var rowHeader = sheet.CreateRow(0);
                rowHeader.CreateCell(0).SetCellValue(headerValue.JENIS_SUMBANGAN);
                rowHeader.CreateCell(1).SetCellValue(headerValue.BENTUK_SUMBANGAN);
                rowHeader.CreateCell(2).SetCellValue(headerValue.NILAI_SUMBANGAN);
                rowHeader.CreateCell(3).SetCellValue(headerValue.TANGGAL_SUMBANGAN);
                rowHeader.CreateCell(4).SetCellValue(headerValue.NAMA_LEMBAGA_PENERIMA);
                rowHeader.CreateCell(5).SetCellValue(headerValue.NPWP_LEMBAGA_PENERIMA);
                rowHeader.CreateCell(6).SetCellValue(headerValue.ALAMAT_LEMBAGA);
                rowHeader.CreateCell(7).SetCellValue(headerValue.NO_TELP_LEMBAGA);
                rowHeader.CreateCell(8).SetCellValue(headerValue.SARANA_PRASARANA_INFRASTRUKTUR);
                rowHeader.CreateCell(9).SetCellValue(headerValue.LOKASI_INFRASTRUKTUR);
                rowHeader.CreateCell(10).SetCellValue(headerValue.BIAYA_PEMBANGUNAN_INFRASTRUKTUR);
                rowHeader.CreateCell(11).SetCellValue(headerValue.IZIN_MENDIRIKAN_BANGUNAN);
                rowHeader.CreateCell(12).SetCellValue(headerValue.NOMOR_REKENING);
                rowHeader.CreateCell(13).SetCellValue(headerValue.NAMA_REKENING_PENERIMA);
                rowHeader.CreateCell(14).SetCellValue(headerValue.NAMA_BANK);

                for (int z = 0; z <= 14; z++)
                {
                    rowHeader.GetCell(z).CellStyle = headerStyle;
                    sheet.SetColumnWidth(z, 5000);
                }

                sheet.SetColumnWidth(4, 7000);
                sheet.SetColumnWidth(5, 7000);
                sheet.SetColumnWidth(6, 8000);
                sheet.SetColumnWidth(8, 11000);
                sheet.SetColumnWidth(9, 6000);
                sheet.SetColumnWidth(10, 8500);
                sheet.SetColumnWidth(11, 9500);
                sheet.SetColumnWidth(13, 7000);
                sheet.SetColumnWidth(14, 6000);
                sheet.GetRow(0).Height = 500;

                int i = 1;
                foreach (var item in list.Where(x => x.NominativeIDNo == nominativeNo))
                {
                    var rowDetail = sheet.CreateRow(i);
                    rowDetail.CreateCell(0).SetCellValue(item.JENIS_SUMBANGAN);
                    rowDetail.CreateCell(1).SetCellValue(item.BENTUK_SUMBANGAN);
                    rowDetail.CreateCell(2).SetCellValue(item.NILAI_SUMBANGAN == null ? "" : item.NILAI_SUMBANGAN.Value.ToString("n0"));
                    rowDetail.CreateCell(3).SetCellValue(item.TANGGAL_SUMBANGAN == null ? "" : item.TANGGAL_SUMBANGAN.Value.ToString("dd MMMM yyyy", CultureInfo.CreateSpecificCulture("id-ID")));
                    rowDetail.CreateCell(4).SetCellValue(item.NAMA_LEMBAGA_PENERIMA);
                    rowDetail.CreateCell(5).SetCellValue(item.NPWP_LEMBAGA_PENERIMA);
                    rowDetail.CreateCell(6).SetCellValue(item.ALAMAT_LEMBAGA);
                    rowDetail.CreateCell(7).SetCellValue(item.NO_TELP_LEMBAGA);
                    rowDetail.CreateCell(8).SetCellValue(item.SARANA_PRASARANA_INFRASTRUKTUR);
                    rowDetail.CreateCell(9).SetCellValue(item.LOKASI_INFRASTRUKTUR);
                    rowDetail.CreateCell(10).SetCellValue(item.BIAYA_PEMBANGUNAN_INFRASTRUKTUR == null ? "" : item.BIAYA_PEMBANGUNAN_INFRASTRUKTUR.Value.ToString("n0"));
                    rowDetail.CreateCell(11).SetCellValue(item.IZIN_MENDIRIKAN_BANGUNAN);
                    rowDetail.CreateCell(12).SetCellValue(item.NOMOR_REKENING);
                    rowDetail.CreateCell(13).SetCellValue(item.NAMA_REKENING_PENERIMA);
                    rowDetail.CreateCell(14).SetCellValue(item.NAMA_BANK);

                    for (int z = 0; z <= 14; z++) { rowDetail.GetCell(z).CellStyle = bodyStyle_Left; }

                    //center position
                    var centerIndex = new int[] { 3, 5, 7, 12 }; //Tanggal Sumbangan, NPWP, No Telp, nomor rekening
                    foreach (var z in centerIndex)
                    {
                        rowDetail.GetCell(z).CellStyle = bodyStyle_Center;
                    }

                    //right position
                    var rightIndex = new int[] { 2, 10 }; //Nilai Sumbangan, Biaya Pembangunan Infrastruktur
                    foreach (var z in rightIndex)
                    {
                        rowDetail.GetCell(z).CellStyle = bodyStyle_Right;
                    }

                    i++;
                }
            }

            byte[] bytesXls;
            using (var exportData = new MemoryStream())
            {
                workbook.Write(exportData);
                bytesXls = exportData.ToArray();
            }

            var path = Path.Combine(TempDownloadFolder, "Donation.xls");
            using (var fs = new FileStream(path, FileMode.Create, FileAccess.Write))
            {
                fs.Write(bytesXls, 0, bytesXls.Length);
            }

            return path;
        }

        [HttpPost]
        public JsonResult FileUploadPDF(FormCollection collection, bool checkAll)
        {

            try
            {
                var searchParam = collection["data"];
                var dashboardIdParams = collection["dashboardIdParams"];
                var searchParamModel = new DashboardSearch();
                List<Guid> dashboardIdList = new List<Guid>();

                if (!string.IsNullOrEmpty(searchParam))
                {
                    searchParamModel = new JavaScriptSerializer().Deserialize<DashboardSearch>(searchParam);
                }

                if (!string.IsNullOrEmpty(dashboardIdParams))
                {
                    dashboardIdList = new JavaScriptSerializer().Deserialize<List<Guid>>(dashboardIdParams);
                }

                // searchParamModel.DashboardIdList = string.Join(",", dashboardIdList);

                //List<string[]> ListArr = new List<string[]>(); // array for the data

                string GetIDGuid = string.Join(",", dashboardIdList);
                Dashboard oData = new Dashboard();
                oData = DashboardRepository.Instance.GetRefferenceNumber(GetIDGuid); //TODO: Later change  to real user
                return Json(oData, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                Result result = new Result();

                result.ResultCode = false;
                result.ResultDesc = e.LogToApp("Get File Upload Dashboard", MessageType.ERR, CurrentLogin.Username).Message;
                return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);
            }
            //}
        }

        [HttpPost]
        public JsonResult Print(FormCollection collection, bool checkAll, string type)
        {
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = "File for download generated successfully"
            };

            string fullPath = string.Empty;
            try
            {
                var searchParam = collection["data"];
                var dashboardIdParams = collection["dashboardIdParams"];
                var searchParamModel = new DashboardSearch();
                List<Guid> dashboardIdList = new List<Guid>();

                if (!string.IsNullOrEmpty(searchParam))
                {
                    searchParamModel = new JavaScriptSerializer().Deserialize<DashboardSearch>(searchParam);
                }

                if (!string.IsNullOrEmpty(dashboardIdParams))
                {
                    dashboardIdList = new JavaScriptSerializer().Deserialize<List<Guid>>(dashboardIdParams);
                }

                searchParamModel.DashboardIdList = string.Join(";", dashboardIdList);

                List<string[]> ListArr = new List<string[]>(); // array for the data

                if (type == "PrintFromPDF")
                {
                    var listPathFromPdf = DashboardRepository.Instance.GetPathFromPdf(searchParamModel);
                    if (listPathFromPdf.Count() > 0)
                    {
                        fullPath = Path.Combine(TempDownloadFolder, $"Nominative_Form_Pdf_{DateTime.Now.ToString("dd-MM-yyyy_HHmmss")}.pdf");
                        MergePDF(fullPath, listPathFromPdf.OrderBy(x => x.CategoryCode).Select(x => Path.GetExtension(x.FromPdf) == ".doc" ? Path.ChangeExtension(x.FromPdf, ".pdf") : x.FromPdf).ToArray());

                    }

                    //List<string> filePaths = new List<string>();
                    //var listPathFromPdf = DashboardRepository.Instance.GetPathFromPdf(searchParamModel);

                    //var dataDonationDoc = listPathFromPdf.Where(x => x.CategoryCode == EnumCategoryCode.Donation && Path.GetExtension(x.FromPdf) == ".doc");
                    //if (dataDonationDoc.Count() > 0)
                    //{
                    //    var MergeDoc = new Spire.Doc.Document();
                    //    var path = $"{TempDownloadFolder}/Donation.doc";

                    //    foreach (var item in dataDonationDoc)
                    //    {
                    //        var document = new Spire.Doc.Document();
                    //        document.LoadFromFile(item.FromPdf, FileFormat.Docx);

                    //        foreach (Spire.Doc.Section sec in document.Sections)
                    //        {
                    //            MergeDoc.Sections.Add(sec.Clone());
                    //        }
                    //    }

                    //    MergeDoc.SaveToFile(path, Spire.Doc.FileFormat.Docx2013);
                    //    filePaths.Add(path);
                    //}

                    //var list = listPathFromPdf.Where(x => Path.GetExtension(x.FromPdf) == ".pdf").OrderBy(x => x.CategoryCode);
                    //if (list.Count() > 0)
                    //{
                    //    var path = $"{TempDownloadFolder}/Nominative_Form_Pdf_{DateTime.Now.ToString("dd-MM-yyyy_HHmmss")}.pdf";
                    //    MergePDF(path, list.Select(x => x.FromPdf).ToArray());
                    //    filePaths.Add(path);
                    //}

                    //if (filePaths.Count > 1)
                    //{
                    //    fullPath = GlobalFunction.GenerateZipFile(filePaths, TempZipFolder);
                    //    var newPath = $"{TempZipFolder}/Nominative_Form_Pdf_{DateTime.Now.ToString("dd-MM-yyyy_HHmmss")}.zip";
                    //    if (!string.IsNullOrEmpty(fullPath))
                    //    {
                    //        System.IO.File.Move(fullPath, newPath);
                    //        fullPath = newPath;
                    //    }
                    //}
                    //else
                    //{
                    //    fullPath = filePaths.FirstOrDefault();
                    //}
                }
                else if (type == "PrintReportPDF")
                {
                    fullPath = Path.Combine(TempDownloadFolder, $"Nominative_Report_Pdf_{DateTime.Now.ToString("dd-MM-yyyy_HHmmss")}.pdf");
                    var filePaths = GenerateReport(searchParamModel, "pdf");
                    MergePDF(fullPath, filePaths.ToArray());
                }

                return Json(result.AsDynamic(fullPath), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                result.ResultCode = false;
                result.ResultDesc = "There is no file found";
                string dataerror = e.LogToApp("Dashboard_E_Nominative.xls", MessageType.ERR, CurrentLogin.Username).Message;

                return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);
            }
        }

        public List<string> GenerateReport(DashboardSearch searchParamModel, string format)
        {
            List<string> filePaths = new List<string>();
            var listCategoryCode = DashboardRepository.Instance.GetNominativeTypeDashboard(searchParamModel);
            if (listCategoryCode.Where(x => x.CategoryCode == EnumCategoryCode.Promotion).Count() > 0 ||
                listCategoryCode.Where(x => x.CategoryCode == EnumCategoryCode.Donation).Count() > 0)
            {
                List<PromotionHeaderTemplate> header = new List<PromotionHeaderTemplate>();
                header.Add(PromotionRepository.Instance.GetHeaderPromotion());

                var list = DashboardRepository.Instance.GetPromotionReport(searchParamModel);
                LocalReport report = new LocalReport();
                report.ReportPath = Server.MapPath("~/Report/RDLC/DataFieldPromotionReport.rdlc");
                report.DataSources.Add(new ReportDataSource() { Name = "DataSetPromotionHeader", Value = header });
                report.DataSources.Add(new ReportDataSource() { Name = "DataSetPromotionDetail", Value = list });
                report.Dispose();

                Warning[] warnings;
                string[] streamids;
                string mimeType;
                string encoding;
                string extension;

                byte[] bytes = report.Render((format == "xls" ? "EXCEL" : format == "doc" ? "WORD" : format).ToUpper(), null, out mimeType, out encoding, out extension, out streamids, out warnings);
                Stream stream = new MemoryStream(bytes);

                var path = Path.Combine(TempDownloadFolder, $"Promotion.{format.ToLower()}");
                using (FileStream fs = new FileStream(path, FileMode.Create))
                {
                    fs.Write(bytes, 0, bytes.Length);
                }

                filePaths.Add(path);
            }

            if (listCategoryCode.Where(x => x.CategoryCode == EnumCategoryCode.Entertainment).Count() > 0)
            {
                List<EntertainmentHeaderTemplate> header = new List<EntertainmentHeaderTemplate>();
                header.Add(EntertainmentRepository.Instance.GetHeaderEntertainment());

                var list = DashboardRepository.Instance.GetEntertainmentReport(searchParamModel);
                LocalReport report = new LocalReport();
                report.ReportPath = Server.MapPath("~/Report/RDLC/DataFieldEntertainmentReport.rdlc");
                report.DataSources.Add(new ReportDataSource() { Name = "DataSetEntertainmentHeader", Value = header });
                report.DataSources.Add(new ReportDataSource() { Name = "DataSetEntertainmentDetail", Value = list });
                report.Dispose();

                Warning[] warnings;
                string[] streamids;
                string mimeType;
                string encoding;
                string extension;

                byte[] bytes = report.Render((format == "xls" ? "EXCEL" : format == "doc" ? "WORD" : format).ToUpper(), null, out mimeType, out encoding, out extension, out streamids, out warnings);
                Stream stream = new MemoryStream(bytes);

                var path = Path.Combine(TempDownloadFolder, $"Entertainment.{format.ToLower()}");
                using (FileStream fs = new FileStream(path, FileMode.Create))
                {
                    fs.Write(bytes, 0, bytes.Length);
                }

                filePaths.Add(path);
            }
            return filePaths;
        }

        public void MergePDF(string targetPath, params string[] pdfs)
        {
            using (PdfDocument targetDoc = new PdfDocument())
            {
                foreach (string pdf in pdfs)
                {
                    using (PdfDocument pdfDoc = PdfReader.Open(pdf, PdfDocumentOpenMode.Import))
                    {
                        for (int i = 0; i < pdfDoc.PageCount; i++)
                        {
                            targetDoc.AddPage(pdfDoc.Pages[i]);
                        }
                    }
                }
                targetDoc.Save(targetPath);
            }
        }

        //public void MergePDF(string targetPath, Dictionary<int?, Stream> data)
        //{
        //    using (PdfDocument targetDoc = new PdfDocument())
        //    {
        //        foreach (var item in data)
        //        {
        //            using (PdfDocument pdfDoc = PdfReader.Open(item.Value, PdfDocumentOpenMode.Import))
        //            {
        //                for (int i = 0; i < pdfDoc.PageCount; i++)
        //                {
        //                    targetDoc.AddPage(pdfDoc.Pages[i]);
        //                }
        //            }
        //        }
        //        targetDoc.Save(targetPath);
        //    }
        //}

        //public void MergeExcel(string targetPath, Dictionary<int?, Stream> data)
        //{
        //    //https://www.programmersought.com/article/86155308947/
        //    HSSFWorkbook targetDoc = new HSSFWorkbook();
        //    foreach (var item in data)
        //    {
        //        HSSFWorkbook book1 = new HSSFWorkbook(item.Value);
        //        //for (int i = 0; i < book1.NumberOfSheets; i++)
        //        //{
        //        //    HSSFSheet sheet1 = book1.GetSheetAt(i) as HSSFSheet;
        //        //    sheet1.CopyTo(targetDoc, item.Key.ToString(), true, true);
        //        //}
        //        using (FileStream fs = new FileStream(targetPath, FileMode.Create, FileAccess.Write))
        //        {
        //            book1.Write(fs);
        //        }
        //    }

        //    //using (FileStream fs = new FileStream(targetPath, FileMode.Create, FileAccess.Write))
        //    //{
        //    //    targetDoc.Write(fs);
        //    //}
        //}

        #region Dropdown
        public JsonResult GetTransactionTypeDropdownList()
        {
            List<DropdownModel> model = new List<DropdownModel>();
            model = DashboardRepository.Instance.GetTransactionTypeDropdownList();
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSystemDropdownList()
        {
            List<DropdownModel> model = new List<DropdownModel>();
            model = DashboardRepository.Instance.GetSystemDropdownList();
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetGLAccountDropdownList()
        {
            List<DropdownModel> model = new List<DropdownModel>();
            model = DashboardRepository.Instance.GetGLAccountDropdownList();
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        //public JsonResult GetJabatanPenandatangan()
        //{
        //    List<DropdownModel> model = new List<DropdownModel>();
        //    //model = DashboardRepository.Instance.GetTransactionTypeDropdownList();
        //    model.Add(new DropdownModel { Id = "Direktur", Name = "Direktur" });
        //    return Json(model, JsonRequestBehavior.AllowGet);
        //}
        public JsonResult GetCategoryNominativeDropdownListDashbord()
        {
            List<DropdownModel> model = new List<DropdownModel>();

            model = DashboardRepository.Instance.GetCategoryNominativeDropdownList();
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}
