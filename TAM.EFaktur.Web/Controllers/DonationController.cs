﻿using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using TAM.EFaktur.Web.Models;
using TAM.EFaktur.Web.Models.Dashboard_Nominative;
using TAM.EFaktur.Web.Models.Donation;
using TAM.EFaktur.Web.Models.Master_Config;
using Toyota.Common.Web.Platform;

namespace TAM.EFaktur.Web.Controllers
{
    public class DonationController : BaseController
    {
        public ActionResult GetDonationAjax(Guid DashboardId)
        {
            var model = DonationRepository.Instance.GetByDashboardId(DashboardId);
            return Json(model);
        }

        [HttpPost]
        public ActionResult Donation(Guid DashboardId, string System)
        {
            var model = DonationRepository.Instance.GetByDashboardId(DashboardId);
            model.PositionSignature = (System == EnumSystem.Manual) ? model.PositionSignature : false;
            ViewData["InsetUpdateDonation"] = model;
            ViewData["ValidationDonation"] = DonationRepository.Instance.GetValidationDataFillByDashboardId(DashboardId);
            ViewData["Header"] = DonationRepository.Instance.GetHeaderDonation() ?? new DonationHeaderTemplate();
            ViewBag.IsManual = (System == EnumSystem.Manual).ToString();
            return PartialView("Donation");
        }

        [HttpPost]
        public ActionResult GetDontionList(Guid DashboardId, string IsManual, int sortBy, bool sortDirection, int page, int size)
        {
            Paging pg = new Paging
            (
                DonationRepository.Instance.Count(DashboardId),
                page,
                size
            );
            ViewData["Paging"] = pg;
            ViewData["List"] = DonationRepository.Instance.GetList(DashboardId, sortBy, sortDirection ? "ASC" : "DESC", pg.StartData, pg.EndData);
            ViewData["Header"] = DonationRepository.Instance.GetHeaderDonation() ?? new DonationHeaderTemplate();
            ViewBag.IsManual = IsManual;

            return PartialView("_SimpleGrid");
        }

        public ActionResult PopupCreate(Guid DashboardId)
        {
            ViewData["ValidationDonation"] = DonationRepository.Instance.GetValidationDataFillByDashboardId(DashboardId);
            ViewData["Header"] = DonationRepository.Instance.GetHeaderDonation() ?? new DonationHeaderTemplate();
            return PartialView("_PopupCreate");
        }

        public ActionResult PopupUpdate(Guid DonationId, Guid DashboardId)
        {
            ViewData["ValidationDonation"] = DonationRepository.Instance.GetValidationDataFillByDashboardId(DashboardId);
            ViewData["Update"] = DonationRepository.Instance.GetById(DonationId);
            ViewData["Header"] = DonationRepository.Instance.GetHeaderDonation() ?? new DonationHeaderTemplate();
            return PartialView("_PopupUpdate");
        }

        public ActionResult PopupDelete(Guid DonationId)
        {
            ViewData["Delete"] = DonationRepository.Instance.GetById(DonationId);
            ViewData["Header"] = DonationRepository.Instance.GetHeaderDonation() ?? new DonationHeaderTemplate();
            return PartialView("_PopupDelete");
        }

        public ActionResult PopupUpload(Guid dashboardId)
        {
            //Config config = MasterConfigRepository.Instance.GetByConfigKey("UrlTemplateDonation");
            //ViewBag.TemplateLink = config != null ? config.ConfigValue : "#";
            //ViewBag.TemplateLink = "/FileStorage/TemplateUpload/Upload Donation.xls";
            ViewBag.DashboardId = dashboardId;
            return PartialView("_PopupUpload");
        }

        public ActionResult DownloadTemplate(Guid dashboardId)
        {
            //Config config = MasterConfigRepository.Instance.GetByConfigKey("UrlTemplateDonation");
            //ViewBag.TemplateLink = config != null ? config.ConfigValue : "#";
            var path = Server.MapPath("/FileStorage/TemplateUpload/Upload Donation.xls");
            var header = DonationRepository.Instance.GetHeaderDonation();

            FileStream file = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite);

            HSSFWorkbook workbook = new HSSFWorkbook(file);
            ISheet sheet = workbook.GetSheet("Default");
            sheet.GetRow(2).GetCell(0).SetCellValue(header.JENIS_SUMBANGAN);
            sheet.GetRow(2).GetCell(1).SetCellValue(header.BENTUK_SUMBANGAN);
            sheet.GetRow(2).GetCell(2).SetCellValue(header.NILAI_SUMBANGAN);
            sheet.GetRow(2).GetCell(3).SetCellValue(header.TANGGAL_SUMBANGAN);
            sheet.GetRow(2).GetCell(4).SetCellValue(header.NAMA_LEMBAGA_PENERIMA);
            sheet.GetRow(2).GetCell(5).SetCellValue(header.NPWP_LEMBAGA_PENERIMA);
            sheet.GetRow(2).GetCell(6).SetCellValue(header.ALAMAT_LEMBAGA);
            sheet.GetRow(2).GetCell(7).SetCellValue(header.NO_TELP_LEMBAGA);
            sheet.GetRow(2).GetCell(8).SetCellValue(header.SARANA_PRASARANA_INFRASTRUKTUR);
            sheet.GetRow(2).GetCell(9).SetCellValue(header.LOKASI_INFRASTRUKTUR);
            sheet.GetRow(2).GetCell(14).SetCellValue(header.BIAYA_PEMBANGUNAN_INFRASTRUKTUR);
            sheet.GetRow(2).GetCell(10).SetCellValue(header.IZIN_MENDIRIKAN_BANGUNAN);
            sheet.GetRow(2).GetCell(11).SetCellValue(header.NOMOR_REKENING);
            sheet.GetRow(2).GetCell(12).SetCellValue(header.NAMA_REKENING_PENERIMA);
            sheet.GetRow(2).GetCell(13).SetCellValue(header.NAMA_BANK);

            ISheet sheet2 = workbook.GetSheet("Petunjuk Pengisian");
            sheet2.GetRow(1).GetCell(0).SetCellValue(header.JENIS_SUMBANGAN);
            sheet2.GetRow(3).GetCell(0).SetCellValue(header.BENTUK_SUMBANGAN);
            sheet2.GetRow(5).GetCell(0).SetCellValue(header.NILAI_SUMBANGAN);
            sheet2.GetRow(7).GetCell(0).SetCellValue(header.TANGGAL_SUMBANGAN);
            sheet2.GetRow(9).GetCell(0).SetCellValue(header.NAMA_LEMBAGA_PENERIMA);
            sheet2.GetRow(11).GetCell(0).SetCellValue(header.NPWP_LEMBAGA_PENERIMA);
            sheet2.GetRow(13).GetCell(0).SetCellValue(header.ALAMAT_LEMBAGA);
            sheet2.GetRow(15).GetCell(0).SetCellValue(header.NO_TELP_LEMBAGA);
            sheet2.GetRow(17).GetCell(0).SetCellValue(header.SARANA_PRASARANA_INFRASTRUKTUR);
            sheet2.GetRow(19).GetCell(0).SetCellValue(header.LOKASI_INFRASTRUKTUR);
            sheet2.GetRow(21).GetCell(0).SetCellValue(header.BIAYA_PEMBANGUNAN_INFRASTRUKTUR);
            sheet2.GetRow(23).GetCell(0).SetCellValue(header.IZIN_MENDIRIKAN_BANGUNAN);
            sheet2.GetRow(25).GetCell(0).SetCellValue(header.NOMOR_REKENING);
            sheet2.GetRow(27).GetCell(0).SetCellValue(header.NAMA_REKENING_PENERIMA);
            sheet2.GetRow(29).GetCell(0).SetCellValue(header.NAMA_BANK);

            var boldFont = workbook.CreateFont();
            boldFont.Boldweight = (short)FontBoldWeight.BOLD;
            boldFont.Color = HSSFColor.WHITE.index;
            boldFont.FontHeightInPoints = 11;
            boldFont.FontName = "Calibri";

            ICellStyle redStyle = workbook.CreateCellStyle();
            redStyle.SetFont(boldFont);
            redStyle.FillForegroundColor = HSSFColor.RED.index;
            redStyle.FillPattern = FillPatternType.SOLID_FOREGROUND;

            redStyle.BorderBottom = BorderStyle.THIN;
            redStyle.BottomBorderColor = HSSFColor.BLACK.index;
            redStyle.BorderLeft = BorderStyle.THIN;
            redStyle.LeftBorderColor = HSSFColor.BLACK.index;
            redStyle.BorderRight = BorderStyle.THIN;
            redStyle.RightBorderColor = HSSFColor.BLACK.index;
            redStyle.BorderTop = BorderStyle.THIN;
            redStyle.TopBorderColor = HSSFColor.BLACK.index;

            redStyle.Alignment = HorizontalAlignment.CENTER;
            redStyle.VerticalAlignment = VerticalAlignment.CENTER;

            var validationDataFill = DonationRepository.Instance.GetValidationDataFillByDashboardId(dashboardId);
            if (validationDataFill.JENIS_SUMBANGAN) { sheet.GetRow(2).GetCell(0).CellStyle = redStyle; }
            if (validationDataFill.BENTUK_SUMBANGAN) { sheet.GetRow(2).GetCell(1).CellStyle = redStyle; }
            if (validationDataFill.NILAI_SUMBANGAN) { sheet.GetRow(2).GetCell(2).CellStyle = redStyle; }
            if (validationDataFill.TANGGAL_SUMBANGAN) { sheet.GetRow(2).GetCell(3).CellStyle = redStyle; }
            if (validationDataFill.NAMA_LEMBAGA_PENERIMA) { sheet.GetRow(2).GetCell(4).CellStyle = redStyle; }
            if (validationDataFill.NPWP_LEMBAGA_PENERIMA) { sheet.GetRow(2).GetCell(5).CellStyle = redStyle; }
            if (validationDataFill.ALAMAT_LEMBAGA) { sheet.GetRow(2).GetCell(6).CellStyle = redStyle; }
            if (validationDataFill.NO_TELP_LEMBAGA) { sheet.GetRow(2).GetCell(7).CellStyle = redStyle; }
            if (validationDataFill.SARANA_PRASARANA_INFRASTRUKTUR) { sheet.GetRow(2).GetCell(8).CellStyle = redStyle; }
            if (validationDataFill.LOKASI_INFRASTRUKTUR) { sheet.GetRow(2).GetCell(9).CellStyle = redStyle; }
            if (validationDataFill.BIAYA_PEMBANGUNAN_INFRASTRUKTUR) { sheet.GetRow(2).GetCell(14).CellStyle = redStyle; }
            if (validationDataFill.IZIN_MENDIRIKAN_BANGUNAN) { sheet.GetRow(2).GetCell(10).CellStyle = redStyle; }
            if (validationDataFill.NOMOR_REKENING) { sheet.GetRow(2).GetCell(11).CellStyle = redStyle; }
            if (validationDataFill.NAMA_REKENING_PENERIMA) { sheet.GetRow(2).GetCell(12).CellStyle = redStyle; }
            if (validationDataFill.NAMA_BANK) { sheet.GetRow(2).GetCell(13).CellStyle = redStyle; }

            // code to create workbook 
            using (var exportData = new MemoryStream())
            {
                workbook.Write(exportData);
                byte[] bytes = exportData.ToArray();
                return File(bytes, "application/vnd.ms-excel", "Upload Donation.xls");
            }
        }

        [HttpPost]
        public JsonResult Insert(FormCollection collection)
        {
            var data = collection["data"];
            Result result = new Result();
            if (!string.IsNullOrEmpty(data))
            {
                try
                {
                    var model = new JavaScriptSerializer().Deserialize<Donation>(data);
                    result = DonationRepository.Instance.Insert(model, CurrentLogin.Username);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                result.ResultCode = false;
                result.ResultDesc = "Data not found or not valid";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Update(FormCollection collection)
        {
            var data = collection["data"];
            Result result = new Result();
            if (!string.IsNullOrEmpty(data))
            {
                try
                {
                    var model = new JavaScriptSerializer().Deserialize<Donation>(data);
                    result = DonationRepository.Instance.Update(model, CurrentLogin.Username);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                result.ResultCode = false;
                result.ResultDesc = "Data not found or not valid";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Delete(FormCollection collection)
        {
            var data = collection["data"];
            Result result = new Result();
            if (!string.IsNullOrEmpty(data))
            {
                try
                {
                    var model = new JavaScriptSerializer().Deserialize<Donation>(data);
                    result = DonationRepository.Instance.Delete(model, CurrentLogin.Username);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                result.ResultCode = false;
                result.ResultDesc = "Data not found or not valid";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SubmitHeader(FormCollection collection)
        {
            var data = collection["data"];
            Result result = new Result();
            if (!string.IsNullOrEmpty(data))
            {
                try
                {
                    var model = new JavaScriptSerializer().Deserialize<Donation>(data);
                    //result = DonationRepository.Instance.SubmitHeader(model, CurrentLogin.Username, TempDownloadFolder, Server.MapPath("~/FileStorage/TemplateGenerate/TemplateDonation.docx"));
                    result = DonationRepository.Instance.SubmitHeader(model, CurrentLogin.Username, TempDownloadFolder, Server.MapPath("~/Report/RDLC/DataFieldDonation.rdlc"));
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                result.ResultCode = false;
                result.ResultDesc = "Data not found or not valid";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult UploadDonation(FormCollection collection)
        {
            var data = collection["data"];
            var model = new Donation();
            if (!string.IsNullOrEmpty(data))
            {
                try
                {
                    model = new JavaScriptSerializer().Deserialize<Donation>(data);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            var r = new List<UploadResultViewModel>();

            int i = 0;
            foreach (string file in Request.Files)
            {
                var statuses = new List<UploadResultViewModel>();
                var headers = Request.Headers;
                var now = DateTime.Now;
                var fileName = StampFileName(Request.Files[i].FileName, now);
                var fullPath = Path.Combine(TempUploadFolder, Path.GetFileName(fileName));
                var extension = Path.GetExtension(fileName);

                Result result = new Result();
                if (extension.Equals(".xls") || extension.Equals(".xlsx"))
                {
                    #region Upload File
                    if (string.IsNullOrEmpty(headers["X-File-Name"]))
                    {
                        UploadWholeFile(Request, statuses, TempUploadFolder, now);
                    }
                    else
                    {
                        UploadPartialFile(headers["X-File-Name"], Request, statuses);
                    }
                    #endregion

                    #region Insert Donation Data
                    result = DonationRepository.Instance.ExcelDonationInsert(fullPath, CurrentLogin.Username, model);
                    #endregion
                }
                else
                {
                    result.ResultCode = false;
                    result.ResultDesc = "Only Excel Filetype allowed";
                }
                JsonResult jsonObj = Json(result.AsDynamic(statuses));
                jsonObj.ContentType = "text/plain";
                i++;

                return jsonObj;
            }

            return Json(r);
        }

    }
}
