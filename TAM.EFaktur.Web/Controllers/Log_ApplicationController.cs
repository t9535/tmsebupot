﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using TAM.EFaktur.Web.Models;
using TAM.EFaktur.Web.Models.LogApplication;
using Toyota.Common.Web.Platform;

namespace TAM.EFaktur.Web.Controllers
{
    public class Log_ApplicationController : PageController
    {
        //
        // GET: /Log_Application/

        public Log_ApplicationController()
        {
            Settings.Title = "Application Log";
        }

         [HttpPost]
        public ActionResult GetLogApplicationList(FormCollection collection, int sortBy, bool sortDirection, int page, int size)
        {
            var data = collection["data"];
            var model = new LogApplicationDashboardSearchParamViewModel();

            if (!string.IsNullOrEmpty(data))
                model = new JavaScriptSerializer().Deserialize<LogApplicationDashboardSearchParamViewModel>(data);

            Paging pg = new Paging
            (
                LogApplicationRepository.Instance.Count(model),
                page,
                size
            );
            ViewData["Paging"] = pg;
            ViewData["LogApplicationListDashboard"] = LogApplicationRepository.Instance.GetList(model, sortBy, sortDirection ? "ASC" : "DESC", pg.StartData, pg.EndData);

            return PartialView("_SimpleGrid");
        }

         [HttpGet]
         public JsonResult GetMessageTypeDropdown()
         {


             List<DropdownViewModel> model = new List<DropdownViewModel>();

             model = LogApplicationRepository.Instance.GetMessageTypeDropdown();
             return Json(model, JsonRequestBehavior.AllowGet);
         }
    }
}
