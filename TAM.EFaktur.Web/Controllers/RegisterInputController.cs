﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TAM.EFaktur.Web.Models;
using TAM.EFaktur.Web.Models.Master_Config;
using TAM.EFaktur.Web.Models.WHT;
using Toyota.Common.Web.Platform;
using TAM.EFaktur.Web.Models.Register;
using System.Web.Script.Serialization;
using System.IO;
using TAM.EFaktur.Web.Models.Master_GeneralParam;

namespace TAM.EFaktur.Web.Controllers
{
    public class RegisterInputController : BaseController
    {
        bool isFullAccess { get; set; }

        protected override void Startup()
        {
            Settings.Title = "Input Data Tax Facility";

            int countArr = CurrentLogin.Roles.Count();

            List<string> RoleId = new List<string>();

            for (int i = 0; i < countArr; i++)
            {
                RoleId.Add(CurrentLogin.Roles[i].Id);
            }

            Config config = MasterConfigRepository.Instance.GetByConfigKey("FacilityDataFullAccessRole");//new by role


            string[] WHTFullAccessRole = { "" };
            string[] RoleIds = RoleId.ToArray();

            if (config != null)
            {
                WHTFullAccessRole = config.ConfigValue.Split(';');
                foreach (var item in RoleIds)
                {
                    if (WHTFullAccessRole.Contains(item))
                    {
                        isFullAccess = true;
                    }
                }
            }

        

            ViewBag.IsFullAccess = isFullAccess;
            List<DropdownViewModel> model = new List<DropdownViewModel>();
           
            model = GeneralParamRepository.Instance.GetGeneralParamDropdown("SKBType");
            //model.Add(new DropdownViewModel() { Id = Guid.Empty, Name = "None selected" });
            //var DataList= model.OrderBy(m => m.Id).ToList();

            //SelectList _SKBType = new SelectList(DataList, "Id", "Name", Guid.Empty);
            SelectList _SKBType = new SelectList(model, "Id", "Name");
            ViewData["ComboSKBType"] = _SKBType;
        }

        [HttpPost]
        public JsonResult Insert(FormCollection collection)
        {
            var data = collection["data"];
            string fname = "";

            Result result = new Result();
            if (!string.IsNullOrEmpty(data))
            {
                if (Request.Files.Count > 0)
                {
                    try
                    {
                        //  Get all files from Request object  
                        HttpFileCollectionBase files = Request.Files;
                        //for (int i = 0; i < files.Count; i++)
                        //{
                        //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
                        //string filename = Path.GetFileName(Request.Files[i].FileName);  
                       //if files.Count>0
                        HttpPostedFileBase file = files[0];

                            // Checking for Internet Explorer  
                            if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                            {
                                string[] testfiles = file.FileName.Split(new char[] { '\\' });
                                fname = testfiles[testfiles.Length - 1];
                            }
                            else
                            {
                                fname = file.FileName;
                            }

                            // Get the complete folder path and store the file inside it. 
                            var pathSKB = MasterConfigRepository.Instance.GetByConfigKey("UrlUploadFileTaxFac");
                            fname = Path.Combine(pathSKB.ConfigValue, fname);
                            file.SaveAs(fname); 
                       // }
                    }
                    catch (Exception e)
                    {
                        result.ResultCode = false;
                        result.ResultDesc = e.LogToApp("Insert Error " , MessageType.ERR, CurrentLogin.Username).Message;
                        return Json("Error occurred. Error details: " + e.Message);
                    }
                }
                else
                {
                    result.ResultCode = false;
                    result.ResultDesc = "No files selected.";
                    return Json("No files selected.");
                }

                try
                {
                   
                    var model = new JavaScriptSerializer().Deserialize<RegisterCreateUpdate>(data);
                    model.UploadFile = fname;
                    model.PDFFacStatus = "1";
                    result = RegisterRepository.Instance.InsertSKB(model, CurrentLogin.Username, DateTime.Now);
                    
                    return Json(result, JsonRequestBehavior.AllowGet);

                }
                catch (Exception e)
                {
                    result.ResultCode = false;
                    result.ResultDesc = e.LogToApp("SKB Insert Data ", MessageType.ERR, CurrentLogin.Username).Message;
                    return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);
                    
                }
            }
            else
            {
                result.ResultCode = false;
                result.ResultDesc = "Data not found or not valid";
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            
        }

        [HttpPost]
        public ActionResult UploadFiles2(FormCollection collection)
        {
            var data = collection["data"];

            // Checking no of files injected in Request object  
            if (Request.Files.Count > 0)
            {
                try
                {
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
                        //string filename = Path.GetFileName(Request.Files[i].FileName);  

                        HttpPostedFileBase file = files[i];
                        string fname;

                        // Checking for Internet Explorer  
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }

                        // Get the complete folder path and store the file inside it.  
                        fname = Path.Combine(Server.MapPath("~/Uploads/"), fname);
                        file.SaveAs(fname);
                    }
                    // Returns message that successfully uploaded  
                    return Json("File Uploaded Successfully!");
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details: " + ex.Message);
                }
            }
            else
            {
                return Json("No files selected.");
            }
        }


        //[HttpPost]
        //public JsonResult Insert(FormCollection collection, HttpPostedFileBase postedFile)
        //{

        //    var data = collection["data"];
        //    var pic = System.Web.HttpContext.Current.Request.Files["HelpSectionImages"];
        //    //var fileupload = collection[""];

        //    Result result = new Result();
        //    if (!string.IsNullOrEmpty(data))
        //    {
        //        try
        //        {
        //            var model = new JavaScriptSerializer().Deserialize<RegisterCreateUpdate>(data);
        //            result = RegisterRepository.Instance.InsertSKB(model, "TAM Efaktur", DateTime.Now);

        //        }
        //        catch (Exception ex)
        //        {
        //            throw ex;
        //        }
        //    }
        //    else
        //    {
        //        result.ResultCode = false;
        //        result.ResultDesc = "Data not found or not valid";
        //    }
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}

        [HttpPost]
        public JsonResult InsertCOD(FormCollection collection)
        {
            var data = collection["data"];
            string fname = "";

            Result result = new Result();

            if (Request.Files.Count > 0)
            {
                try
                {
                    if (!string.IsNullOrEmpty(data))
                    {
                        try
                        {       
                            //  Get all files from Request object  
                            HttpFileCollectionBase files = Request.Files;
                            for (int i = 0; i < files.Count; i++)
                            {
                                //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
                                //string filename = Path.GetFileName(Request.Files[i].FileName);  

                                HttpPostedFileBase file = files[i];

                                // Checking for Internet Explorer  
                                if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                                {
                                    string[] testfiles = file.FileName.Split(new char[] { '\\' });
                                    fname = testfiles[testfiles.Length - 1];
                                }
                                else
                                {
                                    fname = file.FileName;
                                }

                                // Get the complete folder path and store the file inside it.  
                                var pathCOD = MasterConfigRepository.Instance.GetByConfigKey("UrlUploadFileTaxFac");
                                fname = Path.Combine(pathCOD.ConfigValue, fname);
                                file.SaveAs(fname);
                            }
                        }
                        catch (Exception ex)
                        {
                            return Json("Error occurred. Error details: " + ex.Message);
                        }

                        var model = new JavaScriptSerializer().Deserialize<RegisterCreateUpdate>(data);
                        model.UploadFile = fname;
                        model.PDFFacStatus = "1";
                        result = RegisterRepository.Instance.InsertCOD(model, CurrentLogin.Username, DateTime.Now);
                    }
                    else
                    {
                        result.ResultCode = false;
                        result.ResultDesc = "Data not found or not valid";
                    }

                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                catch (Exception e)
                {
                    result.ResultCode = false;
                    result.ResultDesc = e.LogToApp("COD Insert Data ", MessageType.ERR, CurrentLogin.Username).Message;
                    return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);
                    //return Json("Error occurred. Error details: " + ex.Message);
                }
            }
            else
            {
                return Json("No files selected.");
            }
        }
    }
}
