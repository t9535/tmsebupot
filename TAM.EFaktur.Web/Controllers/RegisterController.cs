﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using TAM.EFaktur.Web.Models;
using TAM.EFaktur.Web.Models.Master_Config;
using TAM.EFaktur.Web.Models.Register;
using Toyota.Common.Web.Platform;

namespace TAM.EFaktur.Web.Controllers
{
    public class RegisterController : BaseController
    {
        bool isFullAccess { get; set; }

        protected override void Startup()
        {
            Settings.Title = "Dashboard Register Tax Facility";

            int countArr = CurrentLogin.Roles.Count();

            List<string> RoleId = new List<string>();

            for (int i = 0; i < countArr; i++)
            {
                RoleId.Add(CurrentLogin.Roles[i].Id);
            }

            Config config = MasterConfigRepository.Instance.GetByConfigKey("FacilityDataFullAccessRole");//new by role


            string[] RegisterFullAccessRole = { "" };
            string[] RoleIds = RoleId.ToArray();

            if (config != null)
            {
                RegisterFullAccessRole = config.ConfigValue.Split(';');
                foreach (var item in RoleIds)
                {
                    if (RegisterFullAccessRole.Contains(item))
                    {
                        isFullAccess = true;
                    }
                }
            }
            ViewBag.IsFullAccess = isFullAccess;
        }
        
        public ActionResult PopupCreateEbupot()
        {
            return PartialView("_PopupCreateEbupot");
        }

        #region PopUp
        public ActionResult PopupUploadRegisterFile(string taxFaxNo)
        {
            //Config config = MasterConfigRepository.Instance.GetByConfigKey("UrlTemplateUploadWHT");
            //ViewBag.TemplateLink = config != null ? config.ConfigValue : "#";
            ViewBag.TaxFacNo = taxFaxNo;
            return PartialView("_PopupUploadRegisterFile");
        }

        public ActionResult PopupMultipleSearch(string Id)
        {
            ViewBag.ElementId = Id;
            return PartialView("_PopupMultipleSearch");
        }

        public ActionResult PopupTextAreaSearch(string Id)
        {
            ViewBag.ElementId = Id;
            return PartialView("_PopupTextAreaSearch");
        }

        public ActionResult PopupUploadApprovalStatus()
        {
            Config config = MasterConfigRepository.Instance.GetByConfigKey("UrlTemplateApprovalStatusTaxFacility");
            ViewBag.TemplateLink = config != null ? config.ConfigValue : "#";
            return PartialView("_PopupUploadApprovalStatus");
        }

        public ActionResult PopupUploadTaxFacilityDataFile()
        {
            Config config = MasterConfigRepository.Instance.GetByConfigKey("UrlTemplateTaxFacilityFile");
            ViewBag.TemplateLink = config != null ? config.ConfigValue : "#";
            return PartialView("_PopupUploadTaxfacilityDataFile");
        }
        #endregion

        [HttpPost]
        public ActionResult GetRegisterList(FormCollection collection, int sortBy, bool sortDirection, int page, int size)
        {
            #region cek isFullAccess
            int countArr = CurrentLogin.Roles.Count();

            List<string> RoleId = new List<string>();

            for (int i = 0; i < countArr; i++)
            {
                RoleId.Add(CurrentLogin.Roles[i].Id);
            }

            Config config = MasterConfigRepository.Instance.GetByConfigKey("FacilityDataFullAccessRole"); //new by role

            string[] RegisterFullAccessRole = { "" };
            string[] RoleIds = RoleId.ToArray();

            //if (config != null)
            //{
            //    RegisterFullAccessRole = config.ConfigValue.Split(';');
            //    foreach (var item in RoleIds)
            //    {
            //        if (RegisterFullAccessRole.Contains(item))//abc;acc;abb
            //        {
            //            isFullAccess = true;
            //            break;
            //        }
            //    }

            //}
            #endregion
            isFullAccess = true;
            var data = collection["data"];
            var model = new RegisterDashboardSearchParamViewModel();

            if (!string.IsNullOrEmpty(data))
                model = new JavaScriptSerializer().Deserialize<RegisterDashboardSearchParamViewModel>(data);

            Paging pg = new Paging
            (
                RegisterRepository.Instance.Count(model, CurrentHRIS.DivisionName, CurrentLogin.Username, isFullAccess),
                page,
                size
            );
            ViewData["Paging"] = pg;
            ViewData["RegisterListDashboard"] = RegisterRepository.Instance.GetList(model, CurrentHRIS.DivisionName, CurrentLogin.Username, sortBy, sortDirection ? "ASC" : "DESC", pg.StartData, pg.EndData, isFullAccess);

            return PartialView("_SimpleGrid");
        }

        public JsonResult DeleteRegister(FormCollection collection, bool checkAll)
        {
            #region Cek Register Full Access Role
            int countArr = CurrentLogin.Roles.Count();

            List<string> RoleId = new List<string>();

            for (int i = 0; i < countArr; i++)
            {
                RoleId.Add(CurrentLogin.Roles[i].Id);
            }

            Config config = MasterConfigRepository.Instance.GetByConfigKey("WHTFullAccessRole");//new by role

            string[] RegisterFullAccessRole = { "" };
            string[] RoleIds = RoleId.ToArray();

            if (config != null)
            {
                RegisterFullAccessRole = config.ConfigValue.Split(';');
                foreach (var item in RoleIds)
                {
                    if (RegisterFullAccessRole.Contains(item))
                    {
                        isFullAccess = true;
                    }
                }
            }
            #endregion

            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = "Delete successfully"
            };

            try
            {
                var searchParam = collection["data"];
                var taxFacNoList = collection["taxFacNoList"];
                var taxFacSeqList = collection["taxFacSeqList"];
                var searchParamModel = new RegisterDashboardSearchParamViewModel();
                List<string> TaxFacNoList = new List<string>();
                List<string> TaxFacSeqList = new List<string>();
                if (!string.IsNullOrEmpty(searchParam) && checkAll)
                {
                    searchParamModel = new JavaScriptSerializer().Deserialize<RegisterDashboardSearchParamViewModel>(searchParam);
                    TaxFacNoList = RegisterRepository.Instance.GetTaxFacilityByNo(searchParamModel, CurrentHRIS.DivisionName, CurrentLogin.Username, isFullAccess).Select(a=>a.TaxFacNo).ToList();
                }
                else
                {
                    TaxFacNoList = new JavaScriptSerializer().Deserialize<List<string>>(taxFacNoList);
                    TaxFacSeqList = new JavaScriptSerializer().Deserialize<List<string>>(taxFacSeqList);
                }

                List<string[]> ListArr = new List<string[]>();

                result = RegisterRepository.Instance.DeleteRegisterById(TaxFacNoList, TaxFacSeqList, CurrentLogin.Username);

                return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                result.ResultCode = false;
                result.ResultDesc = e.LogToApp("Register Delete", MessageType.ERR, CurrentLogin.Username).Message;
                return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GenerateDownloadFile(FormCollection collection, string type, bool checkAll)
        {
            #region Cek Register Full Access Role
            int countArr = CurrentLogin.Roles.Count();

            List<string> RoleId = new List<string>();

            for (int i = 0; i < countArr; i++)
            {
                RoleId.Add(CurrentLogin.Roles[i].Id);
            }

            Config config = MasterConfigRepository.Instance.GetByConfigKey("FacilityDataFullAccessRole");//new by role


            string[] RegisterFullAccessRole = { "" };
            string[] RoleIds = RoleId.ToArray();

            //if (config != null)
            //{
            //    RegisterFullAccessRole = config.ConfigValue.Split(';');
            //    foreach (var item in RoleIds)
            //    {
            //        if (RegisterFullAccessRole.Contains(item))
            //        {
            //            isFullAccess = true;
            //        }
            //    }
            //}
            #endregion
            isFullAccess = true;
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = "File for download generated successfully"
            };

            string fullPath = string.Empty;
            int totalDataCount = 0;
            int totalDataFailed = 0;
            try
            {
                var searchParam = collection["data"];
                var taxFacNoList = collection["taxFacNoList"];
                var searchParamModel = new RegisterDashboardSearchParamViewModel();
                List<string> TaxFacNoList = new List<string>();
                if (!string.IsNullOrEmpty(searchParam) && checkAll)
                {
                    searchParamModel = new JavaScriptSerializer().Deserialize<RegisterDashboardSearchParamViewModel>(searchParam);
                    TaxFacNoList = RegisterRepository.Instance.GetTaxFacilityByNo(searchParamModel, CurrentHRIS.DivisionName, CurrentLogin.Username, isFullAccess).Select(a => a.TaxFacNo).ToList();
                }
                else
                {
                    TaxFacNoList = new JavaScriptSerializer().Deserialize<List<string>>(taxFacNoList);
                }

                List<string[]> ListArr = new List<string[]>();

                #region xls report
                if (type == "xlsreport")
                {
                    String[] header = new string[]
                    {
                      "TAX_FACILITY_CATEGORY","SKB_TYPE","TAX_FACILITY_NO.","TAX_FACILITY_DATE","SUPPLIER_NAME","SUPPLIER_NPWP/TIN",
                      "SUPPLIER_ADDRESS","SUPPLIER_PHONE_NO.","SUPPLIER_COUNTRY","SUPPLIER_COUNTRY_CODE","REGISTRATION_NUMBER","SUPPLIER_ESTABLISHMENT_DATE","SKB_VALIDITY_PERIOD_FROM",
                      "SKB_VALIDITY_PERIOD_TO","FACILITY_DOC._SIGNER_NAME","FACILITY_DOC._SIGNER_TITLE","MAP_CODE","RECORDED_DATE","RECORDED_TIME","RECORDED_BY","APPROVAL_STATUS","APPROVAL_DATE","PDF_FACILITY_STATUS"
                    }; //for header 


                    ListArr.Add(header);


                    var list = RegisterRepository.Instance.GetXlsReportById(TaxFacNoList);
                    foreach (RegisterExcelViewModel obj in list)
                    {
                        String[] myArr = new string[] {
                            obj.Field1,
                            obj.Field2,
                            obj.Field3,
                            obj.Field4,
                            obj.Field5,
                            obj.Field6,
                            obj.Field7,
                            obj.Field8,
                            obj.Field9,
                            obj.Field10,
                            obj.Field21,
                            obj.Field11,
                            obj.Field12,
                            obj.Field13,
                            obj.Field14,
                            obj.Field15,
                            obj.Field22,
                            obj.Field16,
                            obj.Field23,
                            obj.Field17,
                            obj.Field18,
                            obj.Field19,
                            obj.Field20
                         };
                        ListArr.Add(myArr);
                    }
                    totalDataCount = ListArr.Count() - 1;
                    totalDataFailed = TaxFacNoList.Count() - totalDataCount;
                    result.ResultDesc = totalDataCount + " data successfully downloaded <br/>";
                    result.ResultDesc += totalDataFailed + " data failed downloaded";
                    fullPath = XlsHelper.PutExcel(ListArr, new RegisterExcelViewModel(), TempDownloadFolder, "Register");
                }
                #endregion
                #region pdf zip
                if (type == "pdf")
                {
                    var filePaths = RegisterRepository.Instance.GetPDFUrlById(TaxFacNoList);
                    fullPath = GlobalFunction.GenerateZipFile(filePaths, TempZipFolder);
                    var newPath = TempZipFolder + "\\Register_" + DateTime.Now.ToString("dd-MM-yyyy_HHmmss") + ".zip";
                    System.IO.File.Move(fullPath, newPath);
                    fullPath = newPath;

                    totalDataCount = filePaths.Count();
                    totalDataFailed = TaxFacNoList.Count() - totalDataCount;

                    result.ResultDesc = totalDataCount + " data successfully downloaded <br/>";
                    result.ResultDesc += totalDataFailed + " data failed downloaded";
                }
                #endregion

                return Json(result.AsDynamic(fullPath), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                result.ResultCode = false;
                result.ResultDesc = e.LogToApp("Register Download " + type, MessageType.ERR, CurrentLogin.Username).Message;
                return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);
                throw e;
            }
        }

        [HttpPost]
        public ActionResult UploadRegisterFile()
        {
            string TaxFacNo = Request.Form["hTaxFacNo"].ToString(); ;

            var r = new List<UploadResultViewModel>();
           
            int i = 0;
            foreach (string file in Request.Files)
            {
                var statuses = new List<UploadResultViewModel>();
                var headers = Request.Headers;
                var now = DateTime.Now;
                var pathSKB = MasterConfigRepository.Instance.GetByConfigKey("TempUploadFileReg");
                var fileName = StampFileName(Request.Files[i].FileName, now);
                var fullPath = Path.Combine(pathSKB.ConfigValue, Path.GetFileName(fileName));
                var extension = Path.GetExtension(fileName);

                Result result = new Result();
                if (extension.Equals(".pdf"))
                {
                    #region Upload File
                    if (string.IsNullOrEmpty(headers["X-File-Name"]))
                    {
                        UploadWholeFile(Request, statuses, pathSKB.ConfigValue, now);
                    }
                    else
                    {
                        UploadPartialFile(headers["X-File-Name"], Request, statuses);
                    }
                    #endregion

                    #region Insert Register

                    result = RegisterRepository.Instance.RegisterFileInsert(TaxFacNo ,fullPath, CurrentLogin.Username);
                    #endregion
                }
                else
                {
                    result.ResultCode = false;
                    result.ResultDesc = "Only PDF Filetype allowed";
                }
                JsonResult jsonObj = Json(result.AsDynamic(statuses));
                jsonObj.ContentType = "text/plain";
                i++;

                return jsonObj;
            }

            return Json(r);
        }

        [HttpPost]
        public JsonResult GeneratePrintFile(FormCollection collection, bool checkAll)
        {
            #region Cek register Full Access Role
            int countArr = CurrentLogin.Roles.Count();

            List<string> RoleId = new List<string>();

            for (int i = 0; i < countArr; i++)
            {
                RoleId.Add(CurrentLogin.Roles[i].Id);
            }

            Config config = MasterConfigRepository.Instance.GetByConfigKey("WHTFullAccessRole");//new by role


            string[] RegisterFullAccessRole = { "" };
            string[] RoleIds = RoleId.ToArray();

            if (config != null)
            {
                RegisterFullAccessRole = config.ConfigValue.Split(';');
                foreach (var item in RoleIds)
                {
                    if (RegisterFullAccessRole.Contains(item))
                    {
                        isFullAccess = true;
                    }
                }
            }
            #endregion

            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = "File for printing is generated successfully"
            };
            string fullName = string.Empty;
            string fullPath = string.Empty;
            try
            {
                var searchParam = collection["data"];
                var taxFacNoList = collection["taxFacNoList"];
                var searchParamModel = new RegisterDashboardSearchParamViewModel();
                List<string> TaxFacNoList = new List<string>();
                if (!string.IsNullOrEmpty(searchParam) && checkAll)
                {
                    searchParamModel = new JavaScriptSerializer().Deserialize<RegisterDashboardSearchParamViewModel>(searchParam);
                    TaxFacNoList = RegisterRepository.Instance.GetTaxFacilityByNo(searchParamModel, CurrentHRIS.DivisionName, CurrentLogin.Username, isFullAccess).Select(a => a.TaxFacNo).ToList();
                }
                else
                {
                    TaxFacNoList = new JavaScriptSerializer().Deserialize<List<string>>(taxFacNoList);
                }

                
                var filePaths = RegisterRepository.Instance.GetPDFUrlById(TaxFacNoList);
                fullName = GlobalFunction.MergePDFFilesNameable(filePaths, TempPrintFolder, CurrentLogin.Username, "Register");
                if (!string.IsNullOrEmpty(fullName))
                {
                    fullPath = VirtualDirectoryFilePrintURL + fullName;
                }
                return Json(result.AsDynamic(fullPath), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                result.ResultCode = false;
                result.ResultDesc = e.LogToApp("Register Print e-Faktur", MessageType.ERR, CurrentLogin.Username).Message;
                return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UploadApprovalStatus()
        {
            #region cek isFullAccess
            int countArr = CurrentLogin.Roles.Count();

            List<string> RoleId = new List<string>();

            for (int j = 0; j < countArr; j++)
            {
                RoleId.Add(CurrentLogin.Roles[j].Id);
            }

            Config config = MasterConfigRepository.Instance.GetByConfigKey("WHTFullAccessRole"); //new by role

            string[] RegisterFullAccessRole = { "" };
            string[] RoleIds = RoleId.ToArray();

            if (config != null)
            {
                RegisterFullAccessRole = config.ConfigValue.Split(';');
                foreach (var item in RoleIds)
                {
                    if (RegisterFullAccessRole.Contains(item))//abc;acc;abb
                    {
                        isFullAccess = true;
                        break;
                    }
                }

            }
            #endregion

            var r = new List<UploadResultViewModel>();

            int i = 0;
            foreach (string file in Request.Files)
            {
                var statuses = new List<UploadResultViewModel>();
                var headers = Request.Headers;
                var now = DateTime.Now;
                var fileName = StampFileName(Request.Files[i].FileName, now);
                var fullPath = Path.Combine(TempUploadFolder, Path.GetFileName(fileName));
                var extension = Path.GetExtension(fileName);
                Result result = new Result();
                if (extension.Equals(".xls") || extension.Equals(".xlsx"))
                {
                    #region Upload File
                    if (string.IsNullOrEmpty(headers["X-File-Name"]))
                    {
                        UploadWholeFile(Request, statuses, TempUploadFolder, now);
                    }
                    else
                    {
                        UploadPartialFile(headers["X-File-Name"], Request, statuses);
                    }
                    #endregion

                    #region Insert Tax Facility In

                    result = RegisterRepository.Instance.ExcelApprovalStatusUpdate(fullPath, CurrentLogin.Username, CurrentHRIS.DivisionName, isFullAccess);
                    #endregion

                }
                else
                {
                    result.ResultCode = false;
                    result.ResultDesc = "Only Excel Filetype allowed";
                }
                JsonResult jsonObj = Json(result.AsDynamic(statuses));
                jsonObj.ContentType = "text/plain";
                i++;

                return jsonObj;
            }

            return Json(r);
        }

        [HttpPost]
        public ActionResult UploadTaxFacilityDataFile()
        {
            var r = new List<UploadResultViewModel>();

            //string TempUploadFolder2 = "D:\\Project\\";
            int i = 0;
            foreach (string file in Request.Files)
            {
                var statuses = new List<UploadResultViewModel>();
                var headers = Request.Headers;
                var now = DateTime.Now;
                var fileName = StampFileName(Request.Files[i].FileName, now);
                var fullPath = Path.Combine(TempUploadFolder, Path.GetFileName(fileName));
                var extension = Path.GetExtension(fileName);
                Result result = new Result();
                if (extension.Equals(".xls") || extension.Equals(".xlsx"))
                {
                    #region Upload File
                    if (string.IsNullOrEmpty(headers["X-File-Name"]))
                    {
                        UploadWholeFile(Request, statuses, TempUploadFolder, now);
                    }
                    else
                    {
                        UploadPartialFile(headers["X-File-Name"], Request, statuses);
                    }
                    #endregion

                    #region Insert approval

                    result = RegisterRepository.Instance.ExcelTaxFacilityData(fullPath, CurrentLogin.Username, CurrentHRIS.DivisionName, isFullAccess);
                    #endregion

                }
                else
                {
                    result.ResultCode = false;
                    result.ResultDesc = "Only Excel Filetype allowed";
                }
                JsonResult jsonObj = Json(result.AsDynamic(statuses));
                jsonObj.ContentType = "text/plain";
                i++;

                return jsonObj;
            }

            return Json(r);
        }

    }
}
