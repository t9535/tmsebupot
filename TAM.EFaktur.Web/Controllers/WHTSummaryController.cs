﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using TAM.EFaktur.Web.Models;
using TAM.EFaktur.Web.Models.Master_Config;
using TAM.EFaktur.Web.Models.WHTSummary;
using Toyota.Common.Web.Platform;

namespace TAM.EFaktur.Web.Controllers
{

    public class WHTSummaryController : BaseController
    {
        //
        // GET: /WHT/

        protected override void Startup()
        {
            Settings.Title = "Summary Tax Payment and Reporting";
        }

        #region PopUp
        public ActionResult PopupMultipleSearch(string Id)
        {
            ViewBag.ElementId = Id;
            return PartialView("_PopupMultipleSearch");
        }
        public ActionResult PopupUploadSummaryReport()
        {
            Config config = MasterConfigRepository.Instance.GetByConfigKey("UrlTemplateReportPaymentSummaryData");
            ViewBag.TemplateLink = config != null ? config.ConfigValue : "#";
            return PartialView("_PopupUploadSummaryReport");
        }
        #endregion

        [HttpPost]
        public ActionResult GetWHTList(FormCollection collection, int sortBy, bool sortDirection)
        {
            var data = collection["data"];
            var model = new WHTSummaryReportSearchParamViewModel();

            if (!string.IsNullOrEmpty(data))
                model = new JavaScriptSerializer().Deserialize<WHTSummaryReportSearchParamViewModel>(data);

            //Paging pg = new Paging
            //(
            //    WHTSummaryRepository.Instance.Count(model, CurrentHRIS.DivisionName, CurrentLogin.Username),
            //    page,
            //    size
            //);
            //ViewData["Paging"] = pg;
            ViewData["WHTSummaryList"] = WHTSummaryRepository.Instance.GetList(model, CurrentHRIS.DivisionName, CurrentLogin.Username, sortBy, sortDirection ? "ASC" : "DESC");

            return PartialView("_SimpleGrid");
        }

        [HttpPost]
        public JsonResult GenerateDownloadFile(FormCollection collection, string type, bool checkAll)
        {
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = "File for download generated successfully"
            };
            string fullPath = string.Empty;
            int totalDataCount = 0;
            try
            {
                var searchParam = collection["data"];
                var searchParamModel = new WHTSummaryReportSearchParamViewModel();
                if (!string.IsNullOrEmpty(searchParam) && checkAll)
                {
                    searchParamModel = new JavaScriptSerializer().Deserialize<WHTSummaryReportSearchParamViewModel>(searchParam);
                }

                List<string[]> ListArr = new List<string[]>();// array for data

                #region xls type


                if (type == "xls")
                {
                    String[] header = new string[]
                    { "No.","Tax Period Month","Tax Period Year" ,"KJS MAP Code", "GL Account", "GL Description", "Tax Article",
                      "SPT Type", "Tax Base Amount", "Tax Amount", "*Due Date Payment",
                      "ETC Payment (days)", "*Due Date Reporting", "ETC Reporting (days)", "Tax Payment", "Tax Payment Date", "Tax Reporting Date",
                      "NTPN", "NTTE", "Planning cs Actual Payment (days)", "Planning vs Actual Reporting  (days)"
                    }; //for header 

                    ListArr.Add(header);

                    var list = WHTSummaryRepository.Instance.DownloadList(searchParamModel, CurrentHRIS.DivisionName, CurrentLogin.Username);
                    int no = 0;
                    foreach (WHTSummaryReportViewModel obj in list)
                    {
                        no++;
                        String[] myArr = new string[] {
                            no.ToString(),
                            searchParamModel.Masa.ToString(),
                            searchParamModel.Tahun.ToString(),
                            obj.KJSMapCode,
                            obj.GLAccount,
                            obj.GLDescription,
                            obj.TaxArticle,
                            obj.eSPTType,
                            obj.TaxBasedAmount.ToString(),
                            obj.TaxAmount.ToString(),
                            obj.DueDatePayment.FormatViewDate(),
                            obj.ETCPaymentDays.ToString(),
                            obj.DueDateReporting.FormatViewDate(),
                            obj.ETCReportingDays.ToString(),
                            obj.TaxPayment.ToString(),
                            obj.TaxPaymentDate.FormatViewDate(),
                            obj.TaxReportingDate.FormatViewDate(),
                            obj.NTPN,
                            obj.NTTE,
                            obj.PlanVsActualPaymentDays.ToString(),
                            obj.PlanVsActualReportingDays.ToString()
                            //obj.Field1,
                            //obj.Field2,
                            //obj.Field3,
                            //obj.Field4,
                            //obj.Field5,
                            //obj.Field6,
                            //obj.Field7,
                            //obj.Field8,
                            //obj.Field9,
                            //obj.Field10,
                            //obj.Field11,
                            //obj.Field12,
                            //obj.Field13,
                            //obj.Field14,
                            //obj.Field15,
                            //obj.Field16,
                            //obj.Field17,
                            //obj.Field18
                         };
                        ListArr.Add(myArr);
                    }

                    string monthName = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(searchParamModel.Masa);
                    string Title = "Summary Tax Payment and Reporting Period " + monthName + " " + searchParamModel.Tahun.ToString();

                    totalDataCount = list.Count();
                    result.ResultDesc = totalDataCount + " data successfully downloaded <br/>";
                    fullPath = XlsHelper.PutExcelWithTitle(ListArr, new WHTSummaryReportViewModel(), TempDownloadFolder, Title, "Tax_Summary_Report");
                }
                #endregion
                #region report excel
                //else if (type == "report")
                //{
                //    String[] header = new string[]
                //    { "No.", "KJS MAP Code", "GL Account", "GL Description", "Tax Article",
                //      "e-SPT Type", "Tax Base Amount", "Tax Amount","*Due Date Payment",
                //      "ETC Payment(days)", "*Due Date Reporting", "ETC Reporting(days)", "Tax Payment","Tax Payment Date", "Tax Reporting Date",
                //      "NTPN","Planning cs Actual Payment(days)", "Planning vs Actual Reporting(days)"
                //    }; //for header 

                //    ListArr.Add(header);

                //    var list = WHTSummaryRepository.Instance.DownloadList(searchParamModel, CurrentHRIS.DivisionName, CurrentLogin.Username);
                //    foreach (WHTSummaryDownloadViewModel obj in list)
                //    {
                //        String[] myArr = new string[] {
                //            obj.Field1,
                //            obj.Field2,
                //            obj.Field3,
                //            obj.Field4,
                //            obj.Field5,
                //            obj.Field6,
                //            obj.Field7,
                //            obj.Field8,
                //            obj.Field9,
                //            obj.Field10,
                //            obj.Field11,
                //            obj.Field12,
                //            obj.Field13,
                //            obj.Field14,
                //            obj.Field15,
                //            obj.Field16,
                //            obj.Field17
                //        };
                //        ListArr.Add(myArr);
                //    }
                //    totalDataCount = list.Count();
                //    result.ResultDesc = totalDataCount + " data successfully downloaded <br/>";
                //    fullPath = XlsHelper.PutExcel(ListArr, new WHTSummaryReportViewModel(), TempDownloadFolder, "WHTSummary");
                //}
                #endregion
                #region csv type
                //else if (type == "csv")
                //{
                //    String[] header = new string[]
                //    { "No.", "KJS MAP Code", "GL Account", "GL Description", "Tax Article",
                //      "e-SPT Type", "Tax Base Amount", "Tax Amount","*Due Date Payment",
                //      "ETC Payment(days)", "*Due Date Reporting", "ETC Reporting(days)", "Tax Payment","Tax Payment Date", "Tax Reporting Date",
                //      "NTPN","Planning cs Actual Payment(days)", "Planning vs Actual Reporting(days)"
                //    }; //for header 

                //    ListArr.Add(header);

                //    var list = WHTSummaryRepository.Instance.DownloadList(searchParamModel, CurrentHRIS.DivisionName, CurrentLogin.Username);
                //    foreach (WHTSummaryDownloadViewModel obj in list)
                //    {
                //        String[] myArr = new string[] {
                //            obj.Field1,
                //            obj.Field2,
                //            obj.Field3,
                //            obj.Field4,
                //            obj.Field5,
                //            obj.Field6,
                //            obj.Field7,
                //            obj.Field8,
                //            obj.Field9,
                //            obj.Field10,
                //            obj.Field11,
                //            obj.Field12,
                //            obj.Field13,
                //            obj.Field14,
                //            obj.Field15,
                //            obj.Field16,
                //            obj.Field17
                //        };
                //        ListArr.Add(myArr);
                //    }
                //    totalDataCount = list.Count();
                //    result.ResultDesc = totalDataCount + " data successfully downloaded <br/>";
                //    fullPath = CSVHelper.PutCSV(ListArr, new WHTSummaryReportViewModel(), TempDownloadFolder, "WHTSummary");
                //}
                #endregion

                return Json(result.AsDynamic(fullPath), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                result.ResultCode = false;
                result.ResultDesc = e.LogToApp("WHT Summary Download " + type, MessageType.ERR, CurrentLogin.Username).Message;
                return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UploadSummaryReport()
        {
            var r = new List<UploadResultViewModel>();

            int i = 0;
            foreach (string file in Request.Files)
            {
                var statuses = new List<UploadResultViewModel>();
                var headers = Request.Headers;
                var now = DateTime.Now;
                var fileName = StampFileName(Request.Files[i].FileName, now);
                var fullPath = Path.Combine(TempUploadFolder, Path.GetFileName(fileName));
                var extension = Path.GetExtension(fileName);
                Result result = new Result();
                if (extension.Equals(".xls") || extension.Equals(".xlsx"))
                {
                    #region Upload File
                    if (string.IsNullOrEmpty(headers["X-File-Name"]))
                    {
                        UploadWholeFile(Request, statuses, TempUploadFolder, now);
                    }
                    else
                    {
                        UploadPartialFile(headers["X-File-Name"], Request, statuses);
                    }
                    #endregion

                    #region Insert WHT Summary

                    result = WHTSummaryRepository.Instance.ExcelSummaryReportInsert(fullPath, CurrentLogin.Username);
                    #endregion

                }
                else
                {
                    result.ResultCode = false;
                    result.ResultDesc = "Only Excel Filetype allowed";
                }
                JsonResult jsonObj = Json(result.AsDynamic(statuses));
                jsonObj.ContentType = "text/plain";
                i++;

                return jsonObj;
            }

            return Json(r);
        }

        public JsonResult CheckFileExists(string name)
        {
            Result result = new Result();

            Config config = MasterConfigRepository.Instance.GetByConfigKey("DestinationPDFTaxReport");
            result.ResultDescs = config != null ? config.ConfigValue : "#";

            try
            {
                if (System.IO.File.Exists(result.ResultDescs + name))
                {
                    result.ResultCode = true;
                }
                else
                {
                    result.ResultCode = false;
                }

            }
            catch (Exception e)
            {
                result.ResultCode = false;
                result.ResultDesc = e.Message;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}
