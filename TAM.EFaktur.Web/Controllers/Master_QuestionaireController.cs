﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.Platform;
using System.Web.Script.Serialization;
using System.IO;
using TAM.EFaktur.Web.Models;
using TAM.EFaktur.Web.Models.Master_Questionaire;

namespace TAM.EFaktur.Web.Controllers
{
    public class Master_QuestionaireController : BaseController
    {
        //
        // GET: /Master_Questionaire/

        public Master_QuestionaireController()
        {
            Settings.Title = "Master Questionaire";
        }
        public ActionResult PopupCreateQuestionaire()
        {
            return PartialView("_PopupCreateQuestionaire");
        }
        public ActionResult PopupUpdateQuestionaire(int QuestionId)
        {

            ViewData["UpdateQuestionaire"] = QuestionaireRepository.Instance.GetById(QuestionId);
            return PartialView("_PopupUpdateQuestionaire");
        }
        public ActionResult PopupDeleteQuestionaire(int QuestionId)
        {

            ViewData["DeleteQuestionaire"] = QuestionaireRepository.Instance.GetById(QuestionId);
            return PartialView("_PopupDeleteQuestionaire");
        }
        public ActionResult PopupMultipleSearch(string Id)
        {
            ViewBag.ElementId = Id;
            return PartialView("_PopupMultipleSearch");
        }
        [HttpPost]
        public ActionResult GetUserList(FormCollection collection, int sortBy, bool sortDirection, int page, int size, bool editDeleteQuestionaireEnom)
        {
            var data = collection["data"];
            var model = new QuestionaireSearchParamViewModel();

            if (!string.IsNullOrEmpty(data))
                model = new JavaScriptSerializer().Deserialize<QuestionaireSearchParamViewModel>(data);

            Paging pg = new Paging
            (
                QuestionaireRepository.Instance.Count(model),
                page,
                size
            );
            ViewData["Paging"] = pg;
            ViewData["UserListDashboard"] = QuestionaireRepository.Instance.GetList(model, sortBy, sortDirection ? "DESC" : "ASC", pg.StartData, pg.EndData);
            ViewBag.EditDeleteQuestionaireEnom = editDeleteQuestionaireEnom;
            return PartialView("_SimpleGrid");
        }
        [HttpPost]
        public JsonResult Insert(FormCollection collection)
        {
            var data = collection["data"];

            Result result = new Result();
            if (!string.IsNullOrEmpty(data))
            {
                try
                {
                    var model = new JavaScriptSerializer().Deserialize<QuestionaireCreateUpdate>(data);

                    var checkyes = model.IsYes;
                    if (checkyes == true && model.WarningStatusYes == "--Select--")
                    {
                        //result = QuestionaireRepository.Instance.Insert(model, "TAM Efaktur", DateTime.Now);
                        result.ResultCode = false;
                        result.ResultDesc = "Warning Status Yes is required and can\'t be empty";
                    }
                   else  if (model.IsNo == true && model.WarningStatusNo == "--Select--")
                    {
                        //result = QuestionaireRepository.Instance.Insert(model, "TAM Efaktur", DateTime.Now);
                        result.ResultCode = false;
                        result.ResultDesc = "Warning Status No is required and can\'t be empty";
                    }
                    else
                    {
                        result = QuestionaireRepository.Instance.Insert(model, "TAM Efaktur", DateTime.Now);
                    }
                   

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                result.ResultCode = false;
                result.ResultDesc = "Data not found or not valid";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult Update(FormCollection collection)
        {
            var data = collection["data"];

            Result result = new Result();
            if (!string.IsNullOrEmpty(data))
            {
                try
                {
                    var model = new JavaScriptSerializer().Deserialize<QuestionaireUpdateViewModel>(data);

                    var dataExisting = QuestionaireRepository.Instance.GetById(model.QuestionId);
                    var check = model;
                    var duplicat = QuestionaireRepository.Instance.CekDataDuplicatUpdate(check).FirstOrDefault();

                    var checkyes = model.IsYes;
                    if (checkyes == true && model.WarningStatusYes == "--Select--")
                    {
                        //result = QuestionaireRepository.Instance.Insert(model, "TAM Efaktur", DateTime.Now);
                        result.ResultCode = false;
                        result.ResultDesc = "Warning Status Yes Is required and can\'t be empty";
                    }

                    else if (checkyes == true && model.WarningStatusYes == "")
                    {
                        //result = QuestionaireRepository.Instance.Insert(model, "TAM Efaktur", DateTime.Now);
                        result.ResultCode = false;
                        result.ResultDesc = "Warning Status Yes Is required and can\'t be empty";
                    }

                    else if (model.IsNo == true && model.WarningStatusNo == "--Select--")
                    {
                        //result = QuestionaireRepository.Instance.Insert(model, "TAM Efaktur", DateTime.Now);
                        result.ResultCode = false;
                        result.ResultDesc = "Warning Status No Is required and can\'t be empty";
                    }
                    else if (model.IsNo == true && model.WarningStatusNo == "")
                    {
                        //result = QuestionaireRepository.Instance.Insert(model, "TAM Efaktur", DateTime.Now);
                        result.ResultCode = false;
                        result.ResultDesc = "Warning Status No Is required and can\'t be empty";
                    }

                    else if (duplicat == null || (duplicat != null && duplicat.QuestionId == model.QuestionId))
                    {
                        result = QuestionaireRepository.Instance.Update(model, CurrentLogin.Username, DateTime.Now);
                        result.ResultCode = true;
                        result.ResultDesc = "Data successfully saved";
                    }
                    //else if (duplicat.Code == dataExisting.Code.ToUpper() && duplicat.Id != companyUpdate.Id)
                    else if (duplicat.CategoryCode == model.CategoryCode && duplicat.Question == model.Question && duplicat.QuestionId != model.QuestionId)
                    {
                        //var _code = duplicat.Code;
                        result.ResultCode = false;
                        result.ResultDesc = "Category and Question  already exist";
                    }


                    else
                    {
                        result = QuestionaireRepository.Instance.Update(model, "TAM efaktur", DateTime.Now);
                    }
                    //result = QuestionaireRepository.Instance.Update(model, "TAM efaktur", DateTime.Now);

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                result.ResultCode = false;
                result.ResultDesc = "Data not found or not valid";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult Delete(FormCollection collection)
        {
            var data = collection["data"];

            Result result = new Result();
            if (!string.IsNullOrEmpty(data))
            {
                try
                {
                    var model = new JavaScriptSerializer().Deserialize<QuestionaireUpdateViewModel>(data);



                    result = QuestionaireRepository.Instance.Delete(model);

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                result.ResultCode = false;
                result.ResultDesc = "Data not found or not valid";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GenerateDownloadFile(FormCollection collection, bool checkAll)
        {
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = "File for download generated successfully"
            };
            string fullPath = string.Empty;
            int totalDataCount = 0;
            try
            {
                var searchParam = collection["data"];
                var dataFieldIdParams = collection["dataFieldIdParams"];
                var searchParamModel = new Questionaire();
                List<long> dataFieldIdList = new List<long>();

                if (!string.IsNullOrEmpty(searchParam))
                {
                    searchParamModel = new JavaScriptSerializer().Deserialize<Questionaire>(searchParam);
                }

                if (!string.IsNullOrEmpty(dataFieldIdParams))
                {
                    dataFieldIdList = new JavaScriptSerializer().Deserialize<List<long>>(dataFieldIdParams);
                }

                List<string[]> ListArr = new List<string[]>(); // array for the data

                #region xls type 

                String[] header = new string[]
                { "Category", "Question", "Yes", " No" }; //for header

                ListArr.Add(header);

                var list = QuestionaireRepository.Instance.GetXlsReportById(searchParamModel);
                if (!checkAll) { list = list.Where(x => dataFieldIdList.Any(y => y == x.QuestionId)).ToList(); }

                foreach (var obj in list)
                {

                    if (obj.IsYes == "True")
                    {
                        obj.IsYes = "X";
                    }
                    else
                        obj.IsYes = "";

                    if (obj.IsNo == "True")
                    {
                        obj.IsNo = "X";
                    }
                    else
                        obj.IsNo = "";


                    String[] myArr = new string[] {
                            obj.CategoryCode,
                            obj.Question,
                            obj.IsYes,
                            obj.IsNo
                         };
                    ListArr.Add(myArr);
                }

                fullPath = XlsHelper.PutExcel(ListArr, new Questionaire(), TempDownloadFolder, "Master_Questioner");
                totalDataCount = list.Count();
                result.ResultDesc = totalDataCount + " data successfully downloaded";
                #endregion
                return Json(result.AsDynamic(fullPath), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                result.ResultCode = false;
                result.ResultDesc = e.LogToApp("Master_Questioner.xls", MessageType.ERR, CurrentLogin.Username).Message;
                return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetQuestionDropdown()
        {
            List<QuestionerDropdownViewModel> model = new List<QuestionerDropdownViewModel>();
            try
            {
                model = QuestionaireRepository.Instance.GetQuestionDropdown();
            }
            catch (Exception e)
            {

                throw e;
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetQuestionKategoriDropdown()
        {
            List<QuestionerDropdownViewModel> model = new List<QuestionerDropdownViewModel>();
            try
            {
                model = QuestionaireRepository.Instance.GetQuestionKategoriDropdown();
            }
            catch (Exception e)
            {

                throw e;
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }
    }
}
