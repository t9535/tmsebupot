﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TAM.EFaktur.Web.Models;
using TAM.EFaktur.Web.Models.Master_Config;
using TAM.EFaktur.Web.Models.Master_HRIS;
using Toyota.Common.Web.Platform;

namespace TAM.EFaktur.Web.Controllers
{
    
    public class HomeController : BaseController
    {
      
        protected override void Startup()
        {
            Settings.Title = "Home";

            try
            {
                if (CurrentHRIS == null)
                {
                    //Lookup.Add(HRISRepository.Instance.GetHRISByNoReg(CurrentLogin.RegistrationNumber));
                    //ExceptionHandler.LogToApp("Get HRIS Data", MessageType.INF, CurrentHRIS.DivisionName, CurrentLogin.Username);
                    Lookup.Add(new HRIS
                    {
                        Title = "",
                        DepartmentName = "",
                        DivisionName = "",
                        EmployeeName = "",
                        NoReg = "",
                        SectionName = ""
                    });
                }
            }
            catch (Exception e)
            {
                e.LogToApp("Get HRIS Data", MessageType.ERR, CurrentLogin.Username);
                Lookup.Add(new HRIS
                {
                    Title = "",
                    DepartmentName = "",
                    DivisionName = "",
                    EmployeeName = "",
                    NoReg = "",
                    SectionName = ""
                });
            }
        }
    }
}
