﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using TAM.EFaktur.Web.Models.VATIn;
using TAM.EFaktur.Web.Models;
using Toyota.Common.Web.Platform;
using System.Globalization;

namespace TAM.EFaktur.Web.Controllers
{
    public class VATInManualInputController : BaseController
    {
        //
        // GET: /VATInManualInput/

        public VATInManualInputController()
        {
            Settings.Title = "VAT-In Manual Input Non e-Faktur";
        }

        public ActionResult PopupDetail(int Id = 0)
        {
            ViewBag.RowEdit = Id;
            return PartialView("_PopupDetail");
        }

        public JsonResult ValidateInvoiceDateManual(FormCollection collection)
        {
            var data = collection["data"];
            var model = new VATInManualInputViewModel();
            Result result = new Result();
            if (!string.IsNullOrEmpty(data))
            {
                model = new JavaScriptSerializer().Deserialize<VATInManualInputViewModel>(data);
            }
            //Split to get variable date now
            result.ResultCode = false;
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Insert(FormCollection collection)
        {
            var data = collection["data"];
            var model = new VATInManualInputViewModel();
            Result result = new Result();
            if (!string.IsNullOrEmpty(data))
            {
                model = new JavaScriptSerializer().Deserialize<VATInManualInputViewModel>(data);
            }

            string fakturtype = model.FakturType;

            try
            {
                if (fakturtype == "Non eFaktur")
                {
                    model.InvoiceNumber = model.InvoiceNumberFull;
                    model.FGPengganti = "0";
                    model.InvoiceDate = DateTime.ParseExact(model.InvoiceDate, "dd-MM-yyyy", CultureInfo.InvariantCulture).FormatSQLDate();
                }
                else
                {
                    model.KDJenisTransaksi = model.InvoiceNumberFull.Substring(0, 2);
                    model.FGPengganti = model.InvoiceNumberFull.Substring(2, 1);
                    model.InvoiceNumber = model.InvoiceNumberFull.FormatNomorFakturPolos();
                    model.InvoiceDate = DateTime.ParseExact(model.InvoiceDate, "dd-MM-yyyy", CultureInfo.InvariantCulture).FormatSQLDate();
                }
                result = VATInRepository.Instance.Insert(model, null, CurrentLogin.Username, DateTime.Now); //TODO: Later change  to real user
            }
            catch (Exception e)
            {
                e.LogToApp("VATIn Manual Input Non eFaktur", MessageType.ERR, CurrentLogin.Username);
                result.ResultCode = false;
                result.ResultDesc = e.Message;
            }
                //}
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}
