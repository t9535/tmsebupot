﻿using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using TAM.EFaktur.Web.Models;
using TAM.EFaktur.Web.Models.Dashboard_Nominative;
using TAM.EFaktur.Web.Models.Entertainment;
using TAM.EFaktur.Web.Models.Master_Config;
using Toyota.Common.Web.Platform;

namespace TAM.EFaktur.Web.Controllers
{
    public class EntertainmentController : BaseController
    {
        public ActionResult GetEntertainmentAjax(Guid DashboardId)
        {
            var model = EntertainmentRepository.Instance.GetByDashboardId(DashboardId);
            return Json(model);
        }

        [HttpPost]
        public ActionResult Entertainment(Guid DashboardId, string System)
        {
            var model = EntertainmentRepository.Instance.GetByDashboardId(DashboardId);
            model.PositionSignature = (System == EnumSystem.Manual) ? model.PositionSignature : false;
            ViewData["InsetUpdateEntertainment"] = model;
            ViewData["ValidationEntertainment"] = EntertainmentRepository.Instance.GetValidationDataFillByDashboardId(DashboardId);
            ViewData["Header"] = EntertainmentRepository.Instance.GetHeaderEntertainment() ?? new EntertainmentHeaderTemplate();
            ViewBag.IsManual = (System == EnumSystem.Manual).ToString();
            return PartialView("Entertainment");
        }


        [HttpPost]
        public ActionResult GetEntertainmentList(Guid DashboardId, string IsManual, int sortBy, bool sortDirection, int page, int size)
        {
            Paging pg = new Paging
            (
                EntertainmentRepository.Instance.Count(DashboardId),
                page,
                size
            );
            ViewData["Paging"] = pg;
            ViewData["List"] = EntertainmentRepository.Instance.GetList(DashboardId, sortBy, sortDirection ? "ASC" : "DESC", pg.StartData, pg.EndData);
            ViewData["Header"] = EntertainmentRepository.Instance.GetHeaderEntertainment() ?? new EntertainmentHeaderTemplate();
            ViewBag.IsManual = IsManual;

            return PartialView("_SimpleGrid");
        }

        public ActionResult PopupCreate(Guid DashboardId)
        {
            ViewData["ValidationEntertainment"] = EntertainmentRepository.Instance.GetValidationDataFillByDashboardId(DashboardId);
            ViewData["Header"] = EntertainmentRepository.Instance.GetHeaderEntertainment() ?? new EntertainmentHeaderTemplate();
            return PartialView("_PopupCreate");
        }

        public ActionResult PopupUpdate(Guid EntertainmentId, Guid DashboardId, string IsManual)
        {
            ViewData["ValidationEntertainment"] = EntertainmentRepository.Instance.GetValidationDataFillByDashboardId(DashboardId);
            ViewData["Update"] = EntertainmentRepository.Instance.GetById(EntertainmentId);
            ViewData["Header"] = EntertainmentRepository.Instance.GetHeaderEntertainment() ?? new EntertainmentHeaderTemplate();
            ViewBag.IsManual = IsManual;
            return PartialView("_PopupUpdate");
        }

        public ActionResult PopupDelete(Guid EntertainmentId)
        {
            ViewData["Delete"] = EntertainmentRepository.Instance.GetById(EntertainmentId);
            ViewData["Header"] = EntertainmentRepository.Instance.GetHeaderEntertainment() ?? new EntertainmentHeaderTemplate();
            return PartialView("_PopupDelete");
        }

        public ActionResult PopupUpload(Guid dashboardId)
        {
            //Config config = MasterConfigRepository.Instance.GetByConfigKey("UrlTemplateEntertainment");
            //ViewBag.TemplateLink = config != null ? config.ConfigValue : "#";
            //ViewBag.TemplateLink = "/FileStorage/TemplateUpload/Upload Entertainment.xls";
            ViewBag.DashboardId = dashboardId;
            return PartialView("_PopupUpload");
        }


        public ActionResult DownloadTemplate(Guid dashboardId)
        {
            //Config config = MasterConfigRepository.Instance.GetByConfigKey("UrlTemplateDonation");
            //ViewBag.TemplateLink = config != null ? config.ConfigValue : "#";
            var path = Server.MapPath("/FileStorage/TemplateUpload/Upload Entertainment.xls");
            var header = EntertainmentRepository.Instance.GetHeaderEntertainment();

            FileStream file = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite);

            HSSFWorkbook workbook = new HSSFWorkbook(file);
            ISheet sheet = workbook.GetSheet("Default");
            sheet.GetRow(5).GetCell(0).SetCellValue(header.NO);
            sheet.GetRow(5).GetCell(1).SetCellValue(header.TANGGAL);
            sheet.GetRow(5).GetCell(2).SetCellValue(header.TEMPAT);
            sheet.GetRow(5).GetCell(3).SetCellValue(header.ALAMAT);
            sheet.GetRow(5).GetCell(4).SetCellValue(header.TIPE_ENTERTAINMENT);
            sheet.GetRow(5).GetCell(5).SetCellValue(header.JUMLAH);
            sheet.GetRow(5).GetCell(6).SetCellValue(header.NAMA_RELASI);
            sheet.GetRow(5).GetCell(7).SetCellValue(header.JABATAN);
            sheet.GetRow(5).GetCell(8).SetCellValue(header.ALAMAT_PERUSAHAAN);
            sheet.GetRow(5).GetCell(9).SetCellValue(header.TIPE_BISNIS);
            sheet.GetRow(5).GetCell(10).SetCellValue(header.KETERANGAN);

            ISheet sheet2 = workbook.GetSheet("Petunjuk Pengisian");
            sheet2.GetRow(1).GetCell(0).SetCellValue(header.NO);
            sheet2.GetRow(3).GetCell(0).SetCellValue(header.TANGGAL);
            sheet2.GetRow(5).GetCell(0).SetCellValue(header.TEMPAT);
            sheet2.GetRow(7).GetCell(0).SetCellValue(header.ALAMAT);
            sheet2.GetRow(9).GetCell(0).SetCellValue(header.TIPE_ENTERTAINMENT);
            sheet2.GetRow(11).GetCell(0).SetCellValue(header.JUMLAH);
            sheet2.GetRow(13).GetCell(0).SetCellValue(header.NAMA_RELASI);
            sheet2.GetRow(15).GetCell(0).SetCellValue(header.JABATAN);
            sheet2.GetRow(17).GetCell(0).SetCellValue(header.ALAMAT_PERUSAHAAN);
            sheet2.GetRow(19).GetCell(0).SetCellValue(header.TIPE_BISNIS);
            sheet2.GetRow(21).GetCell(0).SetCellValue(header.KETERANGAN);

            var boldFont = workbook.CreateFont();
            boldFont.Boldweight = (short)FontBoldWeight.BOLD;
            boldFont.Color = HSSFColor.WHITE.index;
            boldFont.FontHeightInPoints = 11;
            boldFont.FontName = "Calibri";

            ICellStyle redStyle = workbook.CreateCellStyle();
            redStyle.SetFont(boldFont);
            redStyle.FillForegroundColor = HSSFColor.RED.index;
            redStyle.FillPattern = FillPatternType.SOLID_FOREGROUND;

            redStyle.BorderBottom = BorderStyle.THIN;
            redStyle.BottomBorderColor = HSSFColor.BLACK.index;
            redStyle.BorderLeft = BorderStyle.THIN;
            redStyle.LeftBorderColor = HSSFColor.BLACK.index;
            redStyle.BorderRight = BorderStyle.THIN;
            redStyle.RightBorderColor = HSSFColor.BLACK.index;
            redStyle.BorderTop = BorderStyle.THIN;
            redStyle.TopBorderColor = HSSFColor.BLACK.index;

            redStyle.Alignment = HorizontalAlignment.CENTER;
            redStyle.VerticalAlignment = VerticalAlignment.CENTER;

            var validationDataFill = EntertainmentRepository.Instance.GetValidationDataFillByDashboardId(dashboardId);
            if (validationDataFill.NO) { sheet.GetRow(5).GetCell(0).CellStyle = redStyle; }
            if (validationDataFill.TANGGAL) { sheet.GetRow(5).GetCell(1).CellStyle = redStyle; }
            if (validationDataFill.TEMPAT) { sheet.GetRow(5).GetCell(2).CellStyle = redStyle; }
            if (validationDataFill.ALAMAT) { sheet.GetRow(5).GetCell(3).CellStyle = redStyle; }
            if (validationDataFill.TIPE_ENTERTAINMENT) { sheet.GetRow(5).GetCell(4).CellStyle = redStyle; }
            if (validationDataFill.JUMLAH) { sheet.GetRow(5).GetCell(5).CellStyle = redStyle; }
            if (validationDataFill.NAMA_RELASI) { sheet.GetRow(5).GetCell(6).CellStyle = redStyle; }
            if (validationDataFill.JABATAN) { sheet.GetRow(5).GetCell(7).CellStyle = redStyle; }
            if (validationDataFill.ALAMAT_PERUSAHAAN) { sheet.GetRow(5).GetCell(8).CellStyle = redStyle; }
            if (validationDataFill.TIPE_BISNIS) { sheet.GetRow(5).GetCell(9).CellStyle = redStyle; }
            if (validationDataFill.KETERANGAN) { sheet.GetRow(5).GetCell(10).CellStyle = redStyle; }

            // code to create workbook 
            using (var exportData = new MemoryStream())
            {
                workbook.Write(exportData);
                byte[] bytes = exportData.ToArray();
                return File(bytes, "application/vnd.ms-excel", "Upload Entertainment.xls");
            }
        }


        [HttpPost]
        public JsonResult Insert(FormCollection collection)
        {
            var data = collection["data"];
            Result result = new Result();
            if (!string.IsNullOrEmpty(data))
            {
                try
                {
                    var model = new JavaScriptSerializer().Deserialize<Entertainment>(data);
                    result = EntertainmentRepository.Instance.Insert(model, CurrentLogin.Username);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                result.ResultCode = false;
                result.ResultDesc = "Data not found or not valid";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Update(FormCollection collection)
        {
            var data = collection["data"];
            Result result = new Result();
            if (!string.IsNullOrEmpty(data))
            {
                try
                {
                    var model = new JavaScriptSerializer().Deserialize<Entertainment>(data);
                    result = EntertainmentRepository.Instance.Update(model, CurrentLogin.Username);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                result.ResultCode = false;
                result.ResultDesc = "Data not found or not valid";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Delete(FormCollection collection)
        {
            var data = collection["data"];
            Result result = new Result();
            if (!string.IsNullOrEmpty(data))
            {
                try
                {
                    var model = new JavaScriptSerializer().Deserialize<Entertainment>(data);
                    result = EntertainmentRepository.Instance.Delete(model, CurrentLogin.Username);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                result.ResultCode = false;
                result.ResultDesc = "Data not found or not valid";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SubmitHeader(FormCollection collection)
        {
            var data = collection["data"];
            Result result = new Result();
            if (!string.IsNullOrEmpty(data))
            {
                try
                {
                    var model = new JavaScriptSerializer().Deserialize<Entertainment>(data);
                    result = EntertainmentRepository.Instance.SubmitHeader(model, CurrentLogin.Username, TempDownloadFolder, Server.MapPath("~/Report/RDLC/DataFieldEntertainment.rdlc"));
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                result.ResultCode = false;
                result.ResultDesc = "Data not found or not valid";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult UploadEntertainment(FormCollection collection)
        {
            var data = collection["data"];
            var model = new Entertainment();
            if (!string.IsNullOrEmpty(data))
            {
                try
                {
                    model = new JavaScriptSerializer().Deserialize<Entertainment>(data);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            var r = new List<UploadResultViewModel>();

            int i = 0;
            foreach (string file in Request.Files)
            {
                var statuses = new List<UploadResultViewModel>();
                var headers = Request.Headers;
                var now = DateTime.Now;
                var fileName = StampFileName(Request.Files[i].FileName, now);
                var fullPath = Path.Combine(TempUploadFolder, Path.GetFileName(fileName));
                var extension = Path.GetExtension(fileName);

                Result result = new Result();
                if (extension.Equals(".xls") || extension.Equals(".xlsx"))
                {
                    #region Upload File
                    if (string.IsNullOrEmpty(headers["X-File-Name"]))
                    {
                        UploadWholeFile(Request, statuses, TempUploadFolder, now);
                    }
                    else
                    {
                        UploadPartialFile(headers["X-File-Name"], Request, statuses);
                    }
                    #endregion

                    #region Insert Entertainment Data
                    result = EntertainmentRepository.Instance.ExcelEntertainmentInsert(fullPath, CurrentLogin.Username, model);
                    #endregion
                }
                else
                {
                    result.ResultCode = false;
                    result.ResultDesc = "Only Excel Filetype allowed";
                }
                JsonResult jsonObj = Json(result.AsDynamic(statuses));
                jsonObj.ContentType = "text/plain";
                i++;

                return jsonObj;
            }

            return Json(r);
        }

        [HttpPost]
        public ActionResult GetEntertainmentDetailList(Guid? EntertainmentId, int sortBy, bool sortDirection, int page, int size, bool isDelete, string IsManual)
        {
            Paging pg = new Paging
            (
                EntertainmentRepository.Instance.CountDetail(EntertainmentId),
                page,
                size
            );
            ViewData["Paging"] = pg;
            ViewData["ListDetail"] = EntertainmentRepository.Instance.GetDetailList(EntertainmentId, sortBy, sortDirection ? "ASC" : "DESC", pg.StartData, pg.EndData);
            ViewData["Header"] = EntertainmentRepository.Instance.GetHeaderEntertainment() ?? new EntertainmentHeaderTemplate();
            ViewBag.IsDelete = isDelete;
            ViewBag.IsManual = IsManual;

            return PartialView("_SimpleGridDetail");
        }


        [HttpPost]
        public ActionResult PopupCreateDetail(Guid DashboardId)
        {
            ViewData["ValidationEntertainment"] = EntertainmentRepository.Instance.GetValidationDataFillByDashboardId(DashboardId);
            ViewData["Header"] = EntertainmentRepository.Instance.GetHeaderEntertainment() ?? new EntertainmentHeaderTemplate();
            return PartialView("_PopupCreateDetail");
        }

        [HttpPost]
        public ActionResult PopupUpdateDetail(Guid EntertainmentDetailId, Guid DashboardId)
        {
            ViewData["ValidationEntertainment"] = EntertainmentRepository.Instance.GetValidationDataFillByDashboardId(DashboardId);
            ViewData["Header"] = EntertainmentRepository.Instance.GetHeaderEntertainment() ?? new EntertainmentHeaderTemplate();
            ViewData["UpdateDetail"] = EntertainmentRepository.Instance.GetDetailById(EntertainmentDetailId);
            return PartialView("_PopupUpdateDetail");
        }

        [HttpPost]
        public ActionResult PopupDeleteDetail(Guid EntertainmentDetailId)
        {
            ViewData["DeleteDetail"] = EntertainmentRepository.Instance.GetDetailById(EntertainmentDetailId);
            ViewData["Header"] = EntertainmentRepository.Instance.GetHeaderEntertainment() ?? new EntertainmentHeaderTemplate();
            return PartialView("_PopupDeleteDetail");
        }

        [HttpPost]
        public JsonResult InsertDetail(FormCollection collection)
        {
            var data = collection["data"];
            Result result = new Result();
            if (!string.IsNullOrEmpty(data))
            {
                try
                {
                    var model = new JavaScriptSerializer().Deserialize<EntertainmentDetail>(data);
                    result = EntertainmentRepository.Instance.InsertDetail(model, CurrentLogin.Username);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                result.ResultCode = false;
                result.ResultDesc = "Data not found or not valid";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdateDetail(FormCollection collection)
        {
            var data = collection["data"];
            Result result = new Result();
            if (!string.IsNullOrEmpty(data))
            {
                try
                {
                    var model = new JavaScriptSerializer().Deserialize<EntertainmentDetail>(data);
                    result = EntertainmentRepository.Instance.UpdateDetail(model, CurrentLogin.Username);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                result.ResultCode = false;
                result.ResultDesc = "Data not found or not valid";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteDetail(FormCollection collection)
        {
            var data = collection["data"];
            Result result = new Result();
            if (!string.IsNullOrEmpty(data))
            {
                try
                {
                    var model = new JavaScriptSerializer().Deserialize<EntertainmentDetail>(data);
                    result = EntertainmentRepository.Instance.DeleteDetail(model, CurrentLogin.Username);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                result.ResultCode = false;
                result.ResultDesc = "Data not found or not valid";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}
