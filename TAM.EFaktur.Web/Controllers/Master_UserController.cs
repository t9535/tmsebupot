﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using TAM.EFaktur.Web.Models;
using TAM.EFaktur.Web.Models.Master_User;
using Toyota.Common.Web.Platform;

namespace TAM.EFaktur.Web.Controllers
{
    public class Master_UserController : PageController
    {
        //
        // GET: /Master_MaintainUser/

        public Master_UserController()
        {
            Settings.Title = "Maintain-User";
        }

        public ActionResult PopupCreateUser()
        {
            return PartialView("_PopupCreateUser");
        }
        public ActionResult PopupUpdateUser(Guid Id)
        {

            ViewData["UpdateUser"] = UserRepository.Instance.GetById(Id);
            return PartialView("_PopupUpdateUser");
        }
        public ActionResult PopupDeleteUser(Guid Id)
        {

            ViewData["DeleteUser"] = UserRepository.Instance.GetById(Id);
            return PartialView("_PopupDeleteUser");
        }

        public ActionResult PopupMultipleSearch(string Id)
        {
            ViewBag.ElementId = Id;
            return PartialView("_PopupMultipleSearch");
        }
        [HttpPost]
        public ActionResult GetUserList(FormCollection collection, int sortBy, bool sortDirection, int page, int size)
        {
            var data = collection["data"];
            var model = new UserSearchParamViewModel();

            if (!string.IsNullOrEmpty(data))
                model = new JavaScriptSerializer().Deserialize<UserSearchParamViewModel>(data);

            Paging pg = new Paging
            (
                UserRepository.Instance.Count(model),
                page,
                size
            );
            ViewData["Paging"] = pg;
            ViewData["UserListDashboard"] = UserRepository.Instance.GetList(model, sortBy, sortDirection ? "ASC" : "DESC", pg.StartData, pg.EndData);

            return PartialView("_SimpleGrid");
        }
        [HttpPost]
        public JsonResult Insert(FormCollection collection)
        {
            var data = collection["data"];

            Result result = new Result();
            if (!string.IsNullOrEmpty(data))
            {
                try
                {
                    var model = new JavaScriptSerializer().Deserialize<UserCreateUpdate>(data);
                    result = UserRepository.Instance.Insert(model, "TAM Efaktur", DateTime.Now);

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                result.ResultCode = false;
                result.ResultDesc = "Data not found or not valid";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult Update(FormCollection collection)
        {
            var data = collection["data"];

            Result result = new Result();
            if (!string.IsNullOrEmpty(data))
            {
                try
                {
                    var model = new JavaScriptSerializer().Deserialize<UserUpdateViewModel>(data);



                    result = UserRepository.Instance.Update(model, "TAM efaktur", DateTime.Now);

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                result.ResultCode = false;
                result.ResultDesc = "Data not found or not valid";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Delete(FormCollection collection)
        {
            var data = collection["data"];

            Result result = new Result();
            if (!string.IsNullOrEmpty(data))
            {
                try
                {
                    var model = new JavaScriptSerializer().Deserialize<UserUpdateViewModel>(data);



                    result = UserRepository.Instance.Delete(model);

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                result.ResultCode = false;
                result.ResultDesc = "Data not found or not valid";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}
