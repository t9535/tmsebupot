﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using TAM.EFaktur.Web.Models;
using TAM.EFaktur.Web.Models.Category_Nominative;
using Toyota.Common.Web.Platform;

namespace TAM.EFaktur.Web.Controllers
{
    public class Category_NominativeController : BaseController
    {
        //
        // GET: /Master_Config/

        public Category_NominativeController()
        {
            Settings.Title = "Category Nominative";
        }

        public ActionResult PopupCreateCategory()
        {
            return PartialView("_PopupCreateCategory");
        }

        [OutputCache(Duration = 0)]
        public ActionResult PopupUpdateCategory(string CategoryCode)
        {

            ViewData["UpdateCategory"] = CategoryNominativeRepository.Instance.GetById(CategoryCode);
            return PartialView("_PopupUpdateCategory");
        }

        public ActionResult PopupDeleteCategory(string CategoryCode)
        {

            ViewData["DeleteCategory"] = CategoryNominativeRepository.Instance.GetById(CategoryCode);
            return PartialView("_PopupDeleteCategory");
        }

        public ActionResult PopupMultipleSearch(string CategoryCode)
        {
            ViewBag.ElementId = CategoryCode;
            return PartialView("_PopupMultipleSearch");
        }
        [HttpPost]
        public ActionResult GetCategoryNominativeList(FormCollection collection, int sortBy, bool sortDirection, int page, int size, bool editDeleteCategoryEnom)
        {
            var data = collection["data"];
            var model = new CategoryNominativeDashboardSearchParamViewModel();

            if (!string.IsNullOrEmpty(data))
                model = new JavaScriptSerializer().Deserialize<CategoryNominativeDashboardSearchParamViewModel>(data);

            Paging pg = new Paging
            (
                CategoryNominativeRepository.Instance.Count(model),
                page,
                size
            );
            ViewData["Paging"] = pg;
            ViewData["CategoryNominativeListDashboard"] = CategoryNominativeRepository.Instance.GetList(model, sortBy, sortDirection ? "DESC" : "ASC", pg.StartData, pg.EndData);
            ViewBag.EditDeleteCategoryEnom = editDeleteCategoryEnom;
            return PartialView("_SimpleGrid");
        }

        //Insert Data
        [HttpPost]
        public JsonResult Insert(FormCollection collection)
        {
            var data = collection["data"];

            Result result = new Result();
            if (!string.IsNullOrEmpty(data))
            {
                try
                {
                    var model = new JavaScriptSerializer().Deserialize<CategoryNominativeCreateUpdate>(data);
                    result = CategoryNominativeRepository.Instance.Insert(model, CurrentLogin.Username, DateTime.Now);

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                result.ResultCode = false;
                result.ResultDesc = "Data not found or not valid";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //Update Data
        [HttpPost]
        public JsonResult Update(FormCollection collection)
        {
            var data = collection["data"];

            Result result = new Result();
            if (!string.IsNullOrEmpty(data))
            {
                try
                {
                    var model = new JavaScriptSerializer().Deserialize<CategoryNominativeCreateUpdate>(data);
                    //var duplicat = CategoryNominativeRepository.Instance.CekDataDuplicatUpdate(model.CategoryCodeOld).FirstOrDefault();
                    //var check = duplicat;

                    //if (check ==null)
                    //{
                    //    result = CategoryNominativeRepository.Instance.Update(model, CurrentLogin.Username, DateTime.Now);
                    //}
                    // else if (check != null)
                    //{
                    //    result.ResultCode = false;
                    //    result.ResultDesc = "Category Code cannot be changed, please check the data";
                    //}
                    result = CategoryNominativeRepository.Instance.Update(model, CurrentLogin.Username, DateTime.Now);

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                result.ResultCode = false;
                result.ResultDesc = "Data not found or not valid";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //Delete Tampilan Data
        [HttpPost]
        public JsonResult Delete(FormCollection collection)
        {
            var data = collection["data"];

            Result result = new Result();
            if (!string.IsNullOrEmpty(data))
            {
                try
                {
                    var model = new JavaScriptSerializer().Deserialize<CategoryNominativeCreateUpdate>(data);
                    //ridwan //
                    var duplicat = CategoryNominativeRepository.Instance.CekDataDuplicatUpdate(model.CategoryCode).FirstOrDefault();
                    var check = duplicat;

                    if (check == null)
                    {
                        //result = CategoryNominativeRepository.Instance.Update(model, CurrentLogin.Username, DateTime.Now);
                        result = CategoryNominativeRepository.Instance.Delete(model);
                    }
                    else if (check != null)
                    {
                        result.ResultCode = false;
                        result.ResultDesc = "Category Code cannot be Delete, please check the data";
                    }

                    //batas//

                    //result = CategoryNominativeRepository.Instance.Delete(model);

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                result.ResultCode = false;
                result.ResultDesc = "Data not found or not valid";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GenerateDownloadFile(FormCollection collection, bool checkAll)
        {
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = "File for download generated successfully"
            };
            string fullPath = string.Empty;
            int totalDataCount = 0;
            try
            {
                var searchParam = collection["data"];
                var dataFieldIdParams = collection["dataFieldIdParams"];
                var searchParamModel = new Category();
                List<long> dataFieldIdList = new List<long>();

                if (!string.IsNullOrEmpty(searchParam))
                {
                    searchParamModel = new JavaScriptSerializer().Deserialize<Category>(searchParam);
                }

                if (!string.IsNullOrEmpty(dataFieldIdParams))
                {
                    dataFieldIdList = new JavaScriptSerializer().Deserialize<List<long>>(dataFieldIdParams);
                }


                List<string[]> ListArr = new List<string[]>(); // array for the data

                #region xls type 

                String[] header = new string[]
                { "Category Code", "Grouping Nominative", "Sequence Group", "Category Name", "Signature Column" }; //for header

                ListArr.Add(header);

                var list = CategoryNominativeRepository.Instance.GetXlsReportById(searchParamModel);
                if (!checkAll) { list = list.Where(x => dataFieldIdList.Any(y => y == x.RowNum)).ToList(); }

                foreach (var obj in list)
                {
                    String[] myArr = new string[] {
                        obj.NominativeType,
                        obj.JointGroup,
                        obj.GroupSeq,
                        obj.NominativeType,
                        obj.PositionSignature==true?"Enable":"Disable"
                    };
                    ListArr.Add(myArr);
                }

                fullPath = XlsHelper.PutExcel(ListArr, new CategoryNominativeOriginalViewModel(), TempDownloadFolder, "Category_Nominative");
                totalDataCount = list.Count();
                result.ResultDesc = totalDataCount + " data successfully downloaded";
                #endregion
                return Json(result.AsDynamic(fullPath), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                result.ResultCode = false;
                result.ResultDesc = e.LogToApp("Category_Nominative.xls", MessageType.ERR, CurrentLogin.Username).Message;
                return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetCategoryNominativeDropdownList()
        {
            List<DropdownModel> model = new List<DropdownModel>();

            model = CategoryNominativeRepository.Instance.GetCategoryNominativeDropdownList();
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetDropdownListGrouping()
        {
            List<DropdownModel> model = new List<DropdownModel>();

            model = CategoryNominativeRepository.Instance.GetCategoryNominativeDropdownListGrouping();
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCategoryCodeNominativeDDL()
        {
            List<DropdownModel> model = new List<DropdownModel>();

            model = CategoryNominativeRepository.Instance.GetCategoryCodeNominativeDDL();
            return Json(model, JsonRequestBehavior.AllowGet);
        }
    }
}
