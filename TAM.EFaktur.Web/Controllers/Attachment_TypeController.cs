﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using TAM.EFaktur.Web.Models;
using TAM.EFaktur.Web.Models.Attachment_Type;
using Toyota.Common.Web.Platform;

namespace TAM.EFaktur.Web.Controllers
{
    public class Attachment_TypeController : BaseController
    {
        //
        // GET: /AttachmentType/xxxx

        public Attachment_TypeController()
        {
            Settings.Title = "Attachment Type";
        }

        public ActionResult PopupCreateAttachment()
        {
            return PartialView("_PopupCreateAttachment");
        }

        //[OutputCache(Duration = 0)]
        //public ActionResult PopupUpdateAttachment(int AttachmentId)
        //{

        //    ViewData["UpdateAttachment"] = AttachmentTypeRepository.Instance.GetById(AttachmentId);
        //    return PartialView("_PopupUpdateAttachment");
        //}

        public ActionResult PopupDeleteAttachment(int AttachmentId)
        {

            ViewData["DeleteAttachment"] = AttachmentTypeRepository.Instance.GetById(AttachmentId);
            return PartialView("_PopupDeleteAttachment");
        }

        public ActionResult PopupMultipleSearch(string CategoryCode)
        {
            ViewBag.ElementId = CategoryCode;
            return PartialView("_PopupMultipleSearch");
        }

        [HttpPost]
        public ActionResult GetAttachmentTypeList(FormCollection collection, int sortBy, bool sortDirection, int page, int size, bool editDeleteAttachment_TypeEnom)
        {
            var data = collection["data"];
            var model = new AttachmentTypeDashboardSearchParamViewModel();

            if (!string.IsNullOrEmpty(data))
                model = new JavaScriptSerializer().Deserialize<AttachmentTypeDashboardSearchParamViewModel>(data);

            Paging pg = new Paging
            (
                AttachmentTypeRepository.Instance.Count(model),
                page,
                size
            );
            ViewData["Paging"] = pg;
            ViewData["AttachmentTypeListDashboard"] = AttachmentTypeRepository.Instance.GetList(model, sortBy, sortDirection ? "DESC" : "ASC", pg.StartData, pg.EndData);
            ViewBag.EditDeleteAttachment_TypeEnom = editDeleteAttachment_TypeEnom;

            return PartialView("_SimpleGrid");
        }


        public JsonResult Insert(FormCollection collection)
        {
            
            var data = collection["data"];
            var model = new AttachmentTypeParamModel();
            Result result = new Result();
            if (!string.IsNullOrEmpty(data))
            {
                model = new JavaScriptSerializer().Deserialize<AttachmentTypeParamModel>(data);
            }
            try
            {

                if (model.FileType =="")
                {
                    //result = QuestionaireRepository.Instance.Insert(model, "TAM Efaktur", DateTime.Now);
                    result.ResultCode = false;
                    result.ResultDesc = "File Type is required and can\'t be empty";
                }
                else if (model.Vendor_Group =="")
                {
                    //result = QuestionaireRepository.Instance.Insert(model, "TAM Efaktur", DateTime.Now);
                    result.ResultCode = false;
                    result.ResultDesc = "Vendor Group is required and can\'t be empty";
                }
                else
                {
                    result = AttachmentTypeRepository.Instance.Save_Update(model, CurrentLogin.Username); //TODO: Later change  to real user
                }

                /*result = AttachmentTypeRepository.Instance.Save_Update (model, CurrentLogin.Username);*/ //TODO: Later change  to real user
            }
            catch (Exception e)
            {
                e.LogToApp("Attachment Type", MessageType.ERR, CurrentLogin.Username);
                result.ResultCode = false;
                result.ResultDesc = e.Message;
            }
            
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Update(FormCollection collection)
        {
            var data = collection["data"];
            //var model = new AttachmentTypeParamModel();
            Result result = new Result();
            if (!string.IsNullOrEmpty(data))
            {
                //model = new JavaScriptSerializer().Deserialize<AttachmentTypeParamModel>(data);
                try
                {
                     var model = new JavaScriptSerializer().Deserialize<AttachmentTypeCreateUpdate>(data);

                    //result = AttachmentTypeRepository.Instance.Update(model, CurrentLogin.Username); //TODO: Later change  to real user

                    //var model = new JavaScriptSerializer().Deserialize<GroupNominativeCreateUpdate>(data);
                    var dataExisting = AttachmentTypeRepository.Instance.GetById(model.AttachmentId);
                    var check = model;
                    var duplicat = AttachmentTypeRepository.Instance.CekDataDuplicatUpdate(check).FirstOrDefault();

                    if (model.FileType == "")
                    {
                        //result = QuestionaireRepository.Instance.Insert(model, "TAM Efaktur", DateTime.Now);
                        result.ResultCode = false;
                        result.ResultDesc = "File Type is required and can\'t be empty";
                    }
                    else if (model.Vendor_Group == "")
                    {
                        //result = QuestionaireRepository.Instance.Insert(model, "TAM Efaktur", DateTime.Now);
                        result.ResultCode = false;
                        result.ResultDesc = "Vendor Group is required and can\'t be empty";
                    }

                   

                   else if (duplicat == null || (duplicat != null && duplicat.AttachmentId == model.AttachmentId))
                    {
                        result = AttachmentTypeRepository.Instance.Update(model, CurrentLogin.Username, DateTime.Now);
                        result.ResultCode = true;
                        result.ResultDesc = "Data successfully saved";
                    }
                    //else if (duplicat.Code == dataExisting.Code.ToUpper() && duplicat.Id != companyUpdate.Id)
                    else if (duplicat.CategoryCode == model.CategoryCode && duplicat.Description == model.Description && duplicat.AttachmentId != model.AttachmentId)
                    {
                        //var _code = duplicat.Code;
                        result.ResultCode = false;
                        result.ResultDesc = "Category and Dokument name already exist";
                    }
                }
                catch (Exception e)
                {
                    e.LogToApp("Attachment Type", MessageType.ERR, CurrentLogin.Username);
                    result.ResultCode = false;
                    result.ResultDesc = e.Message;
                }
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        //Delete Tampilan Data
        [HttpPost]
        public JsonResult Delete(FormCollection collection)
        {
            var data = collection["data"];

            Result result = new Result();
            if (!string.IsNullOrEmpty(data))
            {
                try
                {
                    var model = new JavaScriptSerializer().Deserialize<AttachmentTypeCreateUpdate>(data);
                    result = AttachmentTypeRepository.Instance.Delete(model);

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                result.ResultCode = false;
                result.ResultDesc = "Data not found or not valid";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GenerateDownloadFile(FormCollection collection, bool checkAll/*, int sortBy, bool sortDirection*/)
        {
            Result result = new Result
            {
                ResultCode = true,
                ResultDesc = "File for download generated successfully"
            };
            string fullPath = string.Empty;
            int totalDataCount = 0;
            try
            {
                var searchParam = collection["data"];
                var dataFieldIdParams = collection["dataFieldIdParams"];
                var searchParamModel = new Attachment();
                List<long> dataFieldIdList = new List<long>();

                if (!string.IsNullOrEmpty(searchParam))
                {
                    searchParamModel = new JavaScriptSerializer().Deserialize<Attachment>(searchParam);
                }

                if (!string.IsNullOrEmpty(dataFieldIdParams))
                {
                    dataFieldIdList = new JavaScriptSerializer().Deserialize<List<long>>(dataFieldIdParams);
                }


                List<string[]> ListArr = new List<string[]>(); // array for the data

                #region xls type 

                String[] header = new string[]
                { "Category", "Document Name", "Attachment Type", "Vendor Group" }; //for header

                ListArr.Add(header);

                var list = AttachmentTypeRepository.Instance.GetXlsReportById(searchParamModel/*, sortBy, sortDirection ? "DESC" : "ASC"*/);
                if (!checkAll) { list = list.Where(x => dataFieldIdList.Any(y => y == x.AttachmentId)).ToList(); }

                foreach (var obj in list)
                {
                    String[] myArr = new string[] {
                            obj.CategoryDesc,
                            obj.Description,
                            obj.FileTypeDesc,
                            obj.Vendor_GroupDesc
                         };
                    ListArr.Add(myArr);
                }

                fullPath = XlsHelper.PutExcelAttachment(ListArr, new Attachment(), TempDownloadFolder, "AttachmentType");
                totalDataCount = list.Count();
                result.ResultDesc = totalDataCount + " data successfully downloaded";
                #endregion
                return Json(result.AsDynamic(fullPath), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                result.ResultCode = false;
                result.ResultDesc = e.LogToApp("AttachmentType.xls", MessageType.ERR, CurrentLogin.Username).Message;
                return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);
            }
        }
       

        [HttpGet]
        public JsonResult GetCategoryDropdown()
        {
            List<DropdownAttachmentCategoryModel> model = new List<DropdownAttachmentCategoryModel>();
            try
            {
                model = AttachmentTypeRepository.Instance.GetCategoryDropdownField();
            }
            catch (Exception e)
            {

                throw e;
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetDocumentTypeDropdown()
        {
            List<DropdownAttachmentTypeModel> model = new List<DropdownAttachmentTypeModel>();
            try
            {
                model = AttachmentTypeRepository.Instance.GetDocumentTypeDropdownField();
            }
            catch (Exception e)
            {

                throw e;
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        //[HttpGet]
        //public JsonResult GetDocumentEbilingElvis()
        //{
        //    List<DropdownAttachmentEbilingElvis> model = new List<DropdownAttachmentEbilingElvis>();
        //    try
        //    {
        //        model = AttachmentTypeRepository.Instance.GetDropdownAttachmentEbilingElvis();
        //    }
        //    catch (Exception e)
        //    {

        //        throw e;
        //    }
        //    return Json(model, JsonRequestBehavior.AllowGet);
        //}

        [HttpGet]
        public JsonResult GetVendorGroupDropdown()
        {
            List<DropdownAttachmentTypeModel> model = new List<DropdownAttachmentTypeModel>();
            try
            {
                model = AttachmentTypeRepository.Instance.GetVendorGroupDropdownField();
            }
            catch (Exception e)
            {

                throw e;
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetParamById(int AttachmentId)
        {
            try
            {
                Attachment oData = new Attachment();
                oData = AttachmentTypeRepository.Instance.GetById(AttachmentId); //TODO: Later change  to real user
                return Json(oData, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                Result result = new Result();

                result.ResultCode = false;
                result.ResultDesc = e.LogToApp("Get Attachment Type", MessageType.ERR, CurrentLogin.Username).Message;
                return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);
            }
            //}

        }

        [HttpGet]
        public JsonResult GetDataAtt(int AttachmentId)
        {

            try
            {
                Attachment oData = new Attachment();
                oData = AttachmentTypeRepository.Instance.GetById(AttachmentId); //TODO: Later change  to real user
                return Json(oData, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                Result result = new Result();

                result.ResultCode = false;
                result.ResultDesc = e.LogToApp("Get Parameter Setup", MessageType.ERR, CurrentLogin.Username).Message;
                return Json(result.AsDynamic(), JsonRequestBehavior.AllowGet);
            }
            //}

        }

    }
}
