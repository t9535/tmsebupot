﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TAM.EFaktur.Web.Models;
using TAM.EFaktur.Web.Models.Master_Division;

namespace TAM.EFaktur.Web.Controllers
{
    public class Master_DivisionController : Controller
    {
        
        [HttpGet]
        public JsonResult GetDivisionDropdown()
        {

            List<DropdownViewModel> model = new List<DropdownViewModel>();

            model = DivisionRepository.Instance.GetDivisionDropdown();
            return Json(model, JsonRequestBehavior.AllowGet);
        }
    }
}
