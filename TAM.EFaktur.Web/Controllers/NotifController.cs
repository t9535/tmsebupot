﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TAM.EFaktur.Web.Models;
using TAM.EFaktur.Web.Models.Master_Config;
using TAM.EFaktur.Web.Models.Master_HRIS;
using Toyota.Common.Web.Platform;

namespace TAM.EFaktur.Web.Controllers
{
    public class NotifController : PageController
    {
        public JsonResult GetNotif()
        {
            Result result = new Result();

            try
            {
                Config config = MasterConfigRepository.Instance.GetByConfigKey("UrlSummaryReportPage");

                result.ResultCode = true;
                result.ResultDesc = NotifRepository.Instance.GetNotif();
                result.ResultDescs = config != null ? config.ConfigValue : "#";
            }
            catch (Exception e)
            {
                //e.LogToApp("Home Get Notif", MessageType.ERR, CurrentLogin.Username);
                result.ResultCode = false;
                result.ResultDesc = e.Message;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}
