﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using TAM.EFaktur.Web.Models;
using TAM.EFaktur.Web.Models.Master_MenuRole;
using Toyota.Common.Web.Platform;


namespace TAM.EFaktur.Web.Controllers
{
    public class Master_MenuRoleController : PageController
    {
        //
        // GET: /Master_MenuRole/

        public Master_MenuRoleController()
        {
            Settings.Title = "Maintain-Config";
        }

        public ActionResult PopupCreateMenuRole()
        {
            return PartialView("_PopupCreateMenuRole");
        }

        public ActionResult PopupUpdateMenuRole(Guid Id)
        {

            ViewData["UpdateMenuRole"] = MenuRoleRepository.Instance.GetById(Id);
            return PartialView("_PopupUpdateMenuRole");
        }

        public ActionResult PopupDeleteMenuRole(Guid Id)
        {

            ViewData["DeleteMenuRole"] = MenuRoleRepository.Instance.GetById(Id);
            return PartialView("_PopupDeleteMenuRole");
        }

        public ActionResult PopupMultipleSearch(string Id)
        {
            ViewBag.ElementId = Id;
            return PartialView("_PopupMultipleSearch");
        }
        [HttpPost]
        public ActionResult GetMenuRoleList(FormCollection collection, int sortBy, bool sortDirection, int page, int size)
        {
            var data = collection["data"];
            var model = new MenuRoleDashboardSearchParamViewModel();

            if (!string.IsNullOrEmpty(data))
                model = new JavaScriptSerializer().Deserialize<MenuRoleDashboardSearchParamViewModel>(data);

            Paging pg = new Paging
            (
                MenuRoleRepository.Instance.Count(model),
                page,
                size
            );
            ViewData["Paging"] = pg;
            ViewData["MenuRoleListDashboard"] = MenuRoleRepository.Instance.GetList(model, sortBy, sortDirection ? "ASC" : "DESC", pg.StartData, pg.EndData);

            return PartialView("_SimpleGrid");
        }

        //Insert Data
        [HttpPost]
        public JsonResult Insert(FormCollection collection)
        {
            var data = collection["data"];

            Result result = new Result();
            if (!string.IsNullOrEmpty(data))
            {
                try
                {
                    var model = new JavaScriptSerializer().Deserialize<Master_MenuRoleCreateUpdate>(data);



                    result = MenuRoleRepository.Instance.Insert(model, "Handika Satwika R", DateTime.Now);

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                result.ResultCode = false;
                result.ResultDesc = "Data not found or not valid";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //Update Data

        [HttpPost]
        public JsonResult Update(FormCollection collection)
        {
            var data = collection["data"];

            Result result = new Result();
            if (!string.IsNullOrEmpty(data))
            {
                try
                {
                    var model = new JavaScriptSerializer().Deserialize<Master_MenuRoleCreateUpdate>(data);



                    result = MenuRoleRepository.Instance.Update(model, "Handik Satwika R", DateTime.Now);

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                result.ResultCode = false;
                result.ResultDesc = "Data not found or not valid";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Delete(FormCollection collection)
        {
            var data = collection["data"];

            Result result = new Result();
            if (!string.IsNullOrEmpty(data))
            {
                try
                {
                    var model = new JavaScriptSerializer().Deserialize<Master_MenuRoleCreateUpdate>(data);



                    result = MenuRoleRepository.Instance.Delete(model, "Handik Satwika R", DateTime.Now);

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                result.ResultCode = false;
                result.ResultDesc = "Data not found or not valid";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetMenuDropdown()
        {


            List<DropdownViewModel> model = new List<DropdownViewModel>();

            model = MenuRoleRepository.Instance.GetMenuDropdown();
            return Json(model, JsonRequestBehavior.AllowGet);
        }
    }
}
