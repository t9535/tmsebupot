﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TAM.Passport.LegacySdk
{
    public class LoginClaims
    {
        [JsonProperty("iss")]
        public string Issuer { set; get; }

        [JsonProperty("aud")]
        public Guid Audience { set; get; }

        [JsonProperty("sub")]
        public string Username { set; get; }

        [JsonProperty("iat")]
        public long IssuedAtUnix { set; get; }

        public DateTimeOffset IssuedAt
        {
            get
            {
                return FromUnixTimeSeconds(IssuedAtUnix);
            }
            set
            {
                IssuedAtUnix = ToUnixTimeSeconds(value);
            }
        }

        [JsonProperty("exp")]
        public long ExpirationUnix { set; get; }

        public DateTimeOffset Expiration
        {
            get
            {
                return FromUnixTimeSeconds(ExpirationUnix);
            }
            set
            {
                ExpirationUnix = ToUnixTimeSeconds(value);
            }
        }

        private DateTime EpochTime
        {
            get
            {
                return new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            }
        }

        private long ToUnixTimeSeconds(DateTimeOffset dto)
        {
            return (long)dto.UtcDateTime.Subtract(EpochTime).TotalSeconds;
        }

        private DateTimeOffset FromUnixTimeSeconds(long seconds)
        {
            var t = EpochTime.AddSeconds(seconds);
            return new DateTimeOffset(t);
        }

        [JsonProperty("jti")]
        public Guid SessionId { get; set; }

        [JsonProperty("unique_name")]
        public string Name { set; get; }

        [JsonProperty("email")]
        public string Email { set; get; }

        public string EmployeeId { set; get; }

        [JsonProperty("roles")]
        public List<string> Roles { set; get; }
    }
}
