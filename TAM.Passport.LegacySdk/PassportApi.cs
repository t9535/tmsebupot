﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;

namespace TAM.Passport.LegacySdk
{
    /// <summary>
    /// PassportAPI SDK to use TAM Passport SSO
    /// </summary>
    public class PassportApi
    {
        private readonly Guid AppId;
        private readonly string AppUrl;
        private readonly HttpClient Client = new HttpClient();

        /// <summary>
        /// Public Constructor PassportAPI
        /// </summary>
        /// <param name="url">TAM Passport Url</param>
        /// <param name="appId">Application Guid</param>
        public PassportApi(string url, Guid appId)
        {
            this.AppUrl = url;
            this.AppId = appId;
        }

        /// <summary>
        /// Asynchronously Direct Login with username and password
        /// </summary>
        /// <param name="username">Username</param>
        /// <param name="password">Password</param>
        /// <returns>TAM Sign On Token</returns>
        public string Login(string username, string password)
        {
            var content = new JsonContent(new
            {
                Username = username,
                Password = password,
                AppId = this.AppId
            });
            
            var tokenUrl = new Uri(new Uri(AppUrl), "api/v1/token").ToString();
            var response = Client.PostAsync(tokenUrl, content).Result;

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                return null;
            }

            var body = response.Content.ReadAsStringAsync().Result;
            return JsonConvert.DeserializeObject<string>(body);
        }

        /// <summary>
        /// Asynchronously Post to verify Token to get LoginClaims
        /// </summary>
        /// <param name="token">Token Retrieved from SSO Passport Login</param>
        /// <returns>LoginClaims Deserialized JSON</returns>
        public LoginClaims VerifyToken(string token)
        {
            var content = new JsonContent(token);

            var verifyUrl = new Uri(new Uri(AppUrl), "api/v1/verify").ToString();
            var response = Client.PostAsync(verifyUrl, content).Result;

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                return null;
            }

            var body = response.Content.ReadAsStringAsync().Result;
            return JsonConvert.DeserializeObject<LoginClaims>(body);
        }

        /// <summary>
        /// Verify if claim is correct by checking
        /// iss = issuer (Tam.Passport)
        /// aud = audience, current application id
        /// exp = session expiration date
        /// </summary>
        /// <param name="claims">Login Claims for User</param>
        /// <returns>True if token verified</returns>
        public bool VerifyClaims(LoginClaims claims)
        {
            if (claims.Issuer != "TAM.Passport")
            {
                return false;
            }

            if (claims.Audience != this.AppId)
            {
                return false;
            }
            
            if (claims.Expiration < DateTimeOffset.UtcNow)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Check Session Status on Passport
        /// </summary>
        /// <param name="jti">JWT Token ID</param>
        /// <returns>True if session still active</returns>
        public bool CheckSession(Guid jti)
        {
            var verifyUrl = new Uri(new Uri(AppUrl), "api/v1/session/" + jti).ToString();
            var response = Client.GetAsync(verifyUrl).Result;

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                return false;
            }

            var body = response.Content.ReadAsStringAsync().Result;
            return JsonConvert.DeserializeObject<bool>(body);
        }
    }
}
