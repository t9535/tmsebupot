---$ Alter Procedure [dbo].[usp_CountPromotionDetail] 
IF OBJECT_ID(N'[dbo].[usp_CountPromotionDetail]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_CountPromotionDetail]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_CountPromotionDetail] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_CountPromotionDetail]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

ALTER PROCEDURE usp_CountPromotionDetail
	@PromotionId uniqueidentifier
	AS
BEGIN
SELECT COUNT(PROMOTION_DETAIL_ID) FROM TB_R_Promotion_Detail WHERE PROMOTION_ID=@PromotionId
END

GO
