---$ Alter Procedure [dbo].[usp_GetDocumentEbilElvisDropdown_ENUM] 
IF OBJECT_ID(N'[dbo].[usp_GetDocumentEbilElvisDropdown_ENUM]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_GetDocumentEbilElvisDropdown_ENUM]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_GetDocumentEbilElvisDropdown_ENUM] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_GetDocumentEbilElvisDropdown_ENUM]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go


ALTER PROCEDURE [dbo].[usp_GetDocumentEbilElvisDropdown_ENUM]
	
AS
BEGIN
	SELECT distinct Ebiling as Ebiling, Elvis as Elvis
	FROM TB_M_AttachmentType_ENUM
	--ORDER BY AttachmentId ASC
END

GO
