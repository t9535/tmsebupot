---$ Alter Procedure [dbo].[usp_GetTaxArticleNominativeByDropdownlist] 
IF OBJECT_ID(N'[dbo].[usp_GetTaxArticleNominativeByDropdownlist]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_GetTaxArticleNominativeByDropdownlist]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_GetTaxArticleNominativeByDropdownlist] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_GetTaxArticleNominativeByDropdownlist]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go


ALTER PROCEDURE [dbo].[usp_GetTaxArticleNominativeByDropdownlist]
AS
    BEGIN
        SELECT  NEWID() AS Id ,
                TaxArticle AS Name
        FROM    ( SELECT DISTINCT
                            trdn.WHTArticle AS TaxArticle
                  FROM      dbo.TB_R_Dashboard_Nominative AS trdn
                  WHERE     ISNULL(trdn.WHTArticle, '') <> ''
                ) A
        ORDER BY TaxArticle ASC
    END

GO
