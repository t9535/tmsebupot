---$ Alter Procedure [dbo].[Get_Usp_Duplicat_Question_Enum] 
IF OBJECT_ID(N'[dbo].[Get_Usp_Duplicat_Question_Enum]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[Get_Usp_Duplicat_Question_Enum]'
    EXECUTE('CREATE PROCEDURE [dbo].[Get_Usp_Duplicat_Question_Enum] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[Get_Usp_Duplicat_Question_Enum]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[Get_Usp_Duplicat_Question_Enum]
	-- Add the parameters for the stored procedure here
	@CategoryCode varchar(10),
	@Question varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select * from tb_m_question_enum WHERE CategoryCode=@CategoryCode AND Question=@Question
END

GO