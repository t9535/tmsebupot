---$ Alter Procedure [dbo].[usp_CountGroupNominativeListDashboard_ENUM] 
IF OBJECT_ID(N'[dbo].[usp_CountGroupNominativeListDashboard_ENUM]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_CountGroupNominativeListDashboard_ENUM]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_CountGroupNominativeListDashboard_ENUM] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_CountGroupNominativeListDashboard_ENUM]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

ALTER PROCEDURE [dbo].[usp_CountGroupNominativeListDashboard_ENUM]
	@CategoryCode varchar(50) = '',
	@GLAccount varchar(50) = '',
	@DescriptionTransaction varchar(200) = '',
	@Type varchar(200) = ''
AS
BEGIN
	DECLARE 
	@Query VARCHAR(MAX) = 'SELECT
		COUNT(NominativeId)
	FROM dbo.TB_M_Nominative_ENUM where 1=1',
	@QueryCategoryCode varchar(MAX) = '',
	@QueryGLAccount varchar(MAX) = '',
	@QueryDescriptionTransaction varchar(MAX) = '',
	@QueryType varchar(MAX) = ''

SELECT @QueryCategoryCode = 
dbo.uf_LookupDynamicQueryGenerator(@CategoryCode, 'CategoryCode')
SELECT @QueryGLAccount = 
dbo.uf_LookupDynamicQueryGenerator(@GLAccount, 'GLAccount')
SELECT @QueryDescriptionTransaction = 
dbo.uf_LookupDynamicQueryGenerator(@DescriptionTransaction, 'DescriptionTransaction')
SELECT @QueryType = 
dbo.uf_LookupDynamicQueryGenerator(@Type, 'Type')

SET @Query = 
		@Query + 
		@QueryCategoryCode +
		@QueryGLAccount +
		@QueryDescriptionTransaction +
		@QueryType

EXEC(@Query)
END

GO
