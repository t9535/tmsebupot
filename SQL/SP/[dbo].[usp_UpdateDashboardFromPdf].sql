---$ Alter Procedure [dbo].[usp_UpdateDashboardFromPdf] 
IF OBJECT_ID(N'[dbo].[usp_UpdateDashboardFromPdf]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_UpdateDashboardFromPdf]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_UpdateDashboardFromPdf] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_UpdateDashboardFromPdf]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go


ALTER PROCEDURE [dbo].[usp_UpdateDashboardFromPdf]
	@DashboardId uniqueidentifier,
	@Path VARCHAR(150)
	AS
BEGIN	
	 UPDATE TB_R_Dashboard_Nominative
	 SET NominativeFormAttachment=@Path
	 WHERE Id = @DashboardId
END

GO
