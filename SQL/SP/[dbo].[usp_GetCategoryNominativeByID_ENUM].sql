---$ Alter Procedure [dbo].[usp_GetCategoryNominativeByID_ENUM] 
IF OBJECT_ID(N'[dbo].[usp_GetCategoryNominativeByID_ENUM]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_GetCategoryNominativeByID_ENUM]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_GetCategoryNominativeByID_ENUM] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_GetCategoryNominativeByID_ENUM]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[usp_GetCategoryNominativeByID_ENUM]

	-- Add the parameters for the stored procedure here
	@CategoryCode varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT a.CategoryCode,
	a.CategoryCode as category_code_old,
	NominativeType, 
	JointGroup,
	GroupSeq,
	PositionSignature,
	a.CreatedDate, 
	a.CreatedBy, 
	a.ModifiedDate, 
	a.ModifiedBy,
	isnull((select top 1 CategoryCode from TB_M_Nominative_ENUM where CategoryCode=a.CategoryCode),'KOSONG') as GlStatusData,
	isnull((select top 1 CategoryCode from TB_M_AttachmentType_ENUM where CategoryCode=a.CategoryCode),'KOSONG') as AttachmentStatusData
	FROM TB_M_CategoryNominative_ENUM as a 
	WHERE a.CategoryCode = @CategoryCode


END

GO