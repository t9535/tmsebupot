---$ Alter Procedure [dbo].[usp_UpdateCustomEnom_TB_R_VATIn] 
IF OBJECT_ID(N'[dbo].[usp_UpdateCustomEnom_TB_R_VATIn]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_UpdateCustomEnom_TB_R_VATIn]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_UpdateCustomEnom_TB_R_VATIn] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_UpdateCustomEnom_TB_R_VATIn]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

ALTER PROCEDURE [dbo].[usp_UpdateCustomEnom_TB_R_VATIn]
    @TanggalFaktur DATETIME ,
    @ExpireDate DATE ,
    @NPWPPenjual VARCHAR(30) ,
    @NamaPenjual VARCHAR(100) ,
    @AlamatPenjual VARCHAR(260) ,
    @JumlahDPP DECIMAL ,
    @JumlahPPN DECIMAL ,
    @JumlahPPNBM DECIMAL ,
    @IdNuminative BIGINT = 0
AS
    BEGIN


        SET NOCOUNT ON;

        UPDATE  dbo.TB_R_VATIn
        SET     [TanggalFaktur] = CONVERT(DATE, @TanggalFaktur) ,
                [NPWPPenjual] = @NPWPPenjual ,
                [NamaPenjual] = @NamaPenjual ,
                [AlamatPenjual] = @AlamatPenjual ,
                [JumlahDPP] = @JumlahDPP ,
                [JumlahPPN] = @JumlahPPN ,
                [JumlahPPNBM] = @JumlahPPNBM ,
                [TanggalExpired] = @ExpireDate
        WHERE   IdNuminative = @IdNuminative 
    END

GO
