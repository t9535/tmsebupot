---$ Alter Procedure [dbo].[usp_CountPromotion] 
IF OBJECT_ID(N'[dbo].[usp_CountPromotion]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_CountPromotion]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_CountPromotion] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_CountPromotion]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

ALTER PROCEDURE usp_CountPromotion
	@DashboardId uniqueidentifier
	AS
BEGIN
SELECT COUNT(PROMOTION_ID) FROM TB_R_Promotion WHERE DASHBOARD_ID=@DashboardId AND ISNULL(CATEGORY_CODE,'')<>''
END

GO