---$ Alter Procedure [dbo].[usp_SyncToDashboardNominative] 
IF OBJECT_ID(N'[dbo].[usp_SyncToDashboardNominative]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_SyncToDashboardNominative]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_SyncToDashboardNominative] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_SyncToDashboardNominative]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

ALTER PROC usp_SyncToDashboardNominative
AS
BEGIN TRAN
	DECLARE @NominativeIDNo VARCHAR(30)

	DECLARE cursor_nominative CURSOR
	FOR 
		SELECT DISTINCT NominativeIDNo FROM TB_R_Dashboard_Nominative_Temp WHERE ISNULL(RowStatus,0)<>1;
	OPEN cursor_nominative;
	FETCH NEXT FROM cursor_nominative INTO @NominativeIDNo;
	WHILE @@FETCH_STATUS = 0
		BEGIN
			DECLARE @DashboardId UNIQUEIDENTIFIER = NEWID()
			
			----DASHBOARD NOMINATIVE
			SELECT NominativeIDNo, 
				   MAX(ISNULL(ModifiedOn,CreatedOn)) AS CreatedOn 
			INTO #LastDashboard
			FROM TB_R_Dashboard_Nominative_Temp 
			WHERE ISNULL(RowStatus,0)<>1 
				  AND NominativeIDNo = @NominativeIDNo
			GROUP BY NominativeIDNo

			IF EXISTS(SELECT TOP 1 * FROM TB_R_Dashboard_Nominative WHERE NominativeIDNo = @NominativeIDNo)
			BEGIN
				UPDATE a 
					SET CategoryCode=b.CategoryCode,
						StatusInvoice=b.StatusInvoice,
						PVType=b.PVType,
						SettlementStatus=b.SettlementStatus,
						AccruedYearEndStatus=b.AccruedYearEndStatus,
						AccruedBookingNo=b.AccruedBookingNo,
						DescriptionTransaction=b.DescriptionTransaction,
						GLAccount=b.GLAccount,
						VendorName=b.VendorName,
						Npwp=b.Npwp,
						InvoiceNo=b.InvoiceNo,
						InvoiceDate=b.InvoiceDate,
						BookPeriod=b.BookPeriod, 
						TurnoverAmount=b.TurnoverAmount, 
						VATAmount=b.VATAmount, 
						WHTAmount=b.WHTAmount,
						TaxInvoiceNo=b.TaxInvoiceNo,
						WitholdingTaxNo=b.WitholdingTaxNo,
						WitholdingTaxDate=b.WitholdingTaxDate,
						WHTArticle=b.WHTArticle,
						System=ISNULL(b.System,'EPROC'),
						PVNo=b.PVNo,
						PVCreatedBy=b.PVCreatedBy,
						SAPGRDocNo=b.SAPGRDocNo,
						SAPGRPostingDate=b.SAPGRPostingDate,
						SAPInvoiceDocNo=b.SAPInvoiceDocNo,
						SAPInvoicePostingDate=b.SAPInvoicePostingDate,
						ReceiveFileDate=b.ReceiveFileDate,
						DownloadedDate=b.DownloadedDate,
						DownloadStatus=ISNULL(b.DownloadStatus,'N/A'),
						NominativeFormAttachment=ISNULL(b.NominativeFormAttachment,'N/A'),
						--NominativeIDNo=b.NominativeIDNo,
						NominativeStatus=b.NominativeStatus,
						NominativeDate=b.NominativeDate,
						ModifiedOn=ISNULL(b.ModifiedOn,b.CreatedOn),
						ModifiedBy=ISNULL(b.ModifiedBy,b.CreatedBy)
				FROM TB_R_Dashboard_Nominative a
				INNER JOIN TB_R_Dashboard_Nominative_Temp b ON a.NominativeIDNo = b.NominativeIDNo
				INNER JOIN #LastDashboard c ON b.NominativeIDNo = c.NominativeIDNo AND ISNULL(b.ModifiedOn,b.CreatedOn)=c.CreatedOn
				WHERE  ISNULL(b.RowStatus,0)<>1 AND b.NominativeIDNo = @NominativeIDNo
									
				SELECT TOP 1 @DashboardId=Id FROM TB_R_Dashboard_Nominative 
				WHERE NominativeIDNo = @NominativeIDNo
			END
			ELSE
			BEGIN
				INSERT  INTO TB_R_Dashboard_Nominative
					( Id ,
						NominativeIDNo ,
						CategoryCode ,
						StatusInvoice ,
						PVType ,
						AccruedYearEndStatus ,
						AccruedBookingNo ,
						SettlementStatus ,
						DescriptionTransaction ,
						GLAccount ,
						VendorName ,
						NPWP ,
						InvoiceNo ,
						InvoiceDate ,
						BookPeriod ,
						TurnoverAmount ,
						VATAmount ,
						WHTAmount ,
						TaxInvoiceNo ,
						WitholdingTaxNo ,
						WitholdingTaxDate ,
						WHTArticle ,
						System ,
						PVNo ,
						PVCreatedBy ,
						SAPGRDocNo ,
						SAPGRPostingDate ,
						SAPInvoiceDocNo ,
						SAPInvoicePostingDate ,
						ReceiveFileDate ,
						DownloadedDate ,
						DownloadStatus ,
						NominativeFormAttachment ,
						NominativeStatus ,
						NominativeDate ,
						CreatedOn ,
						CreatedBy 
					)
				SELECT @DashboardId AS Id ,
                    b.NominativeIDNo ,
                    CategoryCode ,
                    StatusInvoice ,
                    PVType ,
                    AccruedYearEndStatus ,
                    AccruedBookingNo ,
                    SettlementStatus ,
                    DescriptionTransaction ,
                    GLAccount ,
                    VendorName ,
                    NPWP ,
                    InvoiceNo ,
                    InvoiceDate ,
                    BookPeriod ,
                    TurnoverAmount ,
                    VATAmount ,
                    WHTAmount ,
                    TaxInvoiceNo ,
                    WitholdingTaxNo ,
                    WitholdingTaxDate ,
                    WHTArticle ,
                    ISNULL(System,'EPROC') AS System ,
                    PVNo ,
                    PVCreatedBy ,
                    SAPGRDocNo ,
                    SAPGRPostingDate ,
                    SAPInvoiceDocNo ,
                    SAPInvoicePostingDate ,
                    ReceiveFileDate ,
                    DownloadedDate ,
                    ISNULL(DownloadStatus,'N/A') ,
                    ISNULL(NominativeFormAttachment,'N/A') ,
                    NominativeStatus ,
                    NominativeDate ,
                    b.CreatedOn ,
                    CreatedBy 
				FROM TB_R_Dashboard_Nominative_Temp b
				INNER JOIN #LastDashboard c ON b.NominativeIDNo = c.NominativeIDNo AND ISNULL(b.ModifiedOn,b.CreatedOn)=c.CreatedOn
				WHERE  ISNULL(b.RowStatus,0)<>1 AND b.NominativeIDNo = @NominativeIDNo
			END
			
			----DONATION
			DELETE FROM TB_R_Donation WHERE DASHBOARD_ID=@DashboardId

			INSERT  INTO TB_R_Donation
                    (   DONATION_ID,
						DASHBOARD_ID ,
						CATEGORY_CODE,
						THN_PAJAK,
						TMP_PENANDATANGAN,
						TGL_PENANDATANGAN,
						NAMA_PENANDATANGAN,
						JABATAN_PENANDATANGAN,
						JENIS_SUMBANGAN,
						BENTUK_SUMBANGAN,
						NILAI_SUMBANGAN,
						TANGGAL_SUMBANGAN,
						NAMA_LEMBAGA_PENERIMA,
						NPWP_LEMBAGA_PENERIMA,
						ALAMAT_LEMBAGA,
						NO_TELP_LEMBAGA,
						SARANA_PRASARANA_INFRASTRUKTUR,
						LOKASI_INFRASTRUKTUR,						
						BIAYA_PEMBANGUNAN_INFRASTRUKTUR,
						IZIN_MENDIRIKAN_BANGUNAN,
						NOMOR_REKENING,
						NAMA_REKENING_PENERIMA,
						NAMA_BANK,
						CREATED_DT ,
						CREATED_BY 
	                )
			SELECT  NEWID() AS DONATION_ID,
					@DashboardId AS DASHBOARD_ID ,
					CATEGORY_CODE,
					THN_PAJAK,
					TMP_PENANDATANGAN,
					TGL_PENANDATANGAN,
					NAMA_PENANDATANGAN,
					JABATAN_PENANDATANGAN,
					JENIS_SUMBANGAN,
					BENTUK_SUMBANGAN,
					NILAI_SUMBANGAN,
					TANGGAL_SUMBANGAN,
					NAMA_LEMBAGA_PENERIMA,
					NPWP_LEMBAGA_PENERIMA,
					ALAMAT_LEMBAGA,
					NO_TELP_LEMBAGA,
					SARANA_PRASARANA_INFRASTRUKTUR,
					LOKASI_INFRASTRUKTUR,						
					BIAYA_PEMBANGUNAN_INFRASTRUKTUR,
					IZIN_MENDIRIKAN_BANGUNAN,
					NOMOR_REKENING,
					NAMA_REKENING_PENERIMA,
					NAMA_BANK,
					ISNULL(CHANGED_DT,CREATED_DT),
					ISNULL(CHANGED_BY,CREATED_BY) 
			FROM TB_R_Donation_Temp a
				INNER JOIN TB_R_Dashboard_Nominative_Temp b ON a.DASHBOARD_ID=b.Id AND  ISNULL(a.RowStatus,0)<>1 
				INNER JOIN #LastDashboard c ON b.NominativeIDNo = c.NominativeIDNo AND ISNULL(b.ModifiedOn,b.CreatedOn)=c.CreatedOn
			WHERE  ISNULL(b.RowStatus,0)<>1 AND b.NominativeIDNo = @NominativeIDNo
				  
			UPDATE a
			SET RowStatus = 1
			FROM TB_R_Donation_Temp a
				INNER JOIN TB_R_Dashboard_Nominative_Temp b ON a.DASHBOARD_ID=b.Id AND  ISNULL(a.RowStatus,0)<>1  
			WHERE  ISNULL(b.RowStatus,0)<>1 AND b.NominativeIDNo = @NominativeIDNo 
			
			----ENTERTAINMENT			
			DELETE b 
			FROM TB_R_Entertainment a
			INNER JOIN  TB_R_Entertainment_Detail b ON a.ENTERTAINMENT_ID=b.ENTERTAINMENT_ID
			WHERE a.DASHBOARD_ID=@DashboardId
			DELETE FROM TB_R_Entertainment WHERE DASHBOARD_ID=@DashboardId
			
			DECLARE @EntertainmentParamId UNIQUEIDENTIFIER
			DECLARE cursor_entertainment CURSOR
			FOR SELECT a.ENTERTAINMENT_ID
				FROM TB_R_Entertainment_Temp a
				INNER JOIN TB_R_Dashboard_Nominative_Temp b ON a.DASHBOARD_ID=b.Id AND  ISNULL(a.RowStatus,0)<>1 
				INNER JOIN #LastDashboard c ON b.NominativeIDNo = c.NominativeIDNo AND ISNULL(b.ModifiedOn,b.CreatedOn)=c.CreatedOn
			WHERE  ISNULL(b.RowStatus,0)<>1 AND b.NominativeIDNo = @NominativeIDNo;

			OPEN cursor_entertainment;
			FETCH NEXT FROM cursor_entertainment INTO @EntertainmentParamId
			WHILE @@FETCH_STATUS = 0
				BEGIN				
					DECLARE @EntertainmentId UNIQUEIDENTIFIER = NEWID()	
					INSERT  INTO TB_R_Entertainment
							(   ENTERTAINMENT_ID,
								DASHBOARD_ID ,
								CATEGORY_CODE,
								THN_PAJAK,
								TMP_PENANDATANGAN,
								TGL_PENANDATANGAN,
								NAMA_PENANDATANGAN,
								JABATAN_PENANDATANGAN,
								NO,
								TANGGAL,
								TEMPAT,
								ALAMAT,
								BENTUK_DAN_JENIS_ENTERTAINMENT,
								JUMLAH,
								CREATED_DT,
								CREATED_BY 
							)
					SELECT  @EntertainmentId AS ENTERTAINMENT_ID,
							@DashboardId AS DASHBOARD_ID ,
							CATEGORY_CODE,
							THN_PAJAK,
							TMP_PENANDATANGAN,
							TGL_PENANDATANGAN,
							NAMA_PENANDATANGAN,
							JABATAN_PENANDATANGAN,
							NO,
							TANGGAL,
							TEMPAT,
							ALAMAT,
							BENTUK_DAN_JENIS_ENTERTAINMENT,
							JUMLAH,					
							ISNULL(CHANGED_DT,CREATED_DT),
							ISNULL(CHANGED_BY,CREATED_BY)
					FROM TB_R_Entertainment_Temp a
					WHERE a.ENTERTAINMENT_ID = @EntertainmentParamId
					
					INSERT  INTO TB_R_Entertainment_Detail
						(   ENTERTAINMENT_DETAIL_ID,
							ENTERTAINMENT_ID,
							NAMA_RELASI,
							POSISI_RELASI,
							PERUSAHAAN_RELASI,
							JENIS_USAHA_RELASI,
							KETERANGAN,
							CREATED_DT,
							CREATED_BY
						)
					SELECT  NEWID() AS ENTERTAINMENT_DETAIL_ID,
							@EntertainmentId AS ENTERTAINMENT_ID,
							NAMA_RELASI,
							POSISI_RELASI,
							PERUSAHAAN_RELASI,
							JENIS_USAHA_RELASI,
							KETERANGAN,							
							ISNULL(CHANGED_DT,CREATED_DT),
							ISNULL(CHANGED_BY,CREATED_BY)
					FROM TB_R_Entertainment_Detail_Temp a
					WHERE a.ENTERTAINMENT_ID = @EntertainmentParamId

					FETCH NEXT FROM cursor_entertainment INTO @EntertainmentParamId;
				END;

			CLOSE cursor_entertainment;
			DEALLOCATE cursor_entertainment;
										  
			UPDATE c
			SET RowStatus = 1
			FROM TB_R_Entertainment_Detail_Temp c
				INNER JOIN TB_R_Entertainment_Temp a ON C.ENTERTAINMENT_ID=A.ENTERTAINMENT_ID AND ISNULL(c.RowStatus,0)<>1
				INNER JOIN TB_R_Dashboard_Nominative_Temp b ON a.DASHBOARD_ID=b.Id AND ISNULL(a.RowStatus,0)<>1  
			WHERE  ISNULL(b.RowStatus,0)<>1 AND b.NominativeIDNo = @NominativeIDNo 

			UPDATE a
			SET RowStatus = 1
			FROM TB_R_Entertainment_Temp a
				INNER JOIN TB_R_Dashboard_Nominative_Temp b ON a.DASHBOARD_ID=b.Id AND  ISNULL(a.RowStatus,0)<>1  
			WHERE  ISNULL(b.RowStatus,0)<>1 AND b.NominativeIDNo = @NominativeIDNo 

			
			----PROMOTION			
			DELETE b 
			FROM TB_R_Promotion a
			INNER JOIN  TB_R_Promotion_Detail b ON a.Promotion_ID=b.Promotion_ID
			WHERE a.DASHBOARD_ID=@DashboardId
			DELETE FROM TB_R_Promotion WHERE DASHBOARD_ID=@DashboardId
			
			DECLARE @PromotionParamId UNIQUEIDENTIFIER = NEWID()
			DECLARE cursor_promotion CURSOR
			FOR SELECT a.PROMOTION_ID
				FROM TB_R_Promotion_Temp a
				INNER JOIN TB_R_Dashboard_Nominative_Temp b ON a.DASHBOARD_ID=b.Id AND  ISNULL(a.RowStatus,0)<>1 
				INNER JOIN #LastDashboard c ON b.NominativeIDNo = c.NominativeIDNo AND ISNULL(b.ModifiedOn,b.CreatedOn)=c.CreatedOn
			WHERE  ISNULL(b.RowStatus,0)<>1 AND b.NominativeIDNo = @NominativeIDNo;

			OPEN cursor_promotion;
			FETCH NEXT FROM cursor_promotion INTO @PromotionParamId
			WHILE @@FETCH_STATUS = 0
				BEGIN		
					DECLARE @PromotionId UNIQUEIDENTIFIER = NEWID()			
					INSERT  INTO TB_R_Promotion
							(   Promotion_ID,
								DASHBOARD_ID ,
								CATEGORY_CODE,
								THN_PAJAK,
								TMP_PENANDATANGAN,
								TGL_PENANDATANGAN,
								NAMA_PENANDATANGAN,
								JABATAN_PENANDATANGAN,
								NO,
								NAMA,
								NPWP,
								ALAMAT,
								CREATED_DT ,
								CREATED_BY 
							)
					SELECT  @PromotionId AS Promotion_ID,
							@DashboardId AS DASHBOARD_ID ,
							CATEGORY_CODE,
							THN_PAJAK,
							TMP_PENANDATANGAN,
							TGL_PENANDATANGAN,
							NAMA_PENANDATANGAN,
							JABATAN_PENANDATANGAN,
							NO,
							NAMA,
							NPWP,
							ALAMAT,			
							ISNULL(CHANGED_DT,CREATED_DT),
							ISNULL(CHANGED_BY,CREATED_BY)
					FROM TB_R_Promotion_Temp a
					WHERE a.Promotion_ID = @PromotionParamId
					
					INSERT  INTO TB_R_Promotion_Detail
						(   PROMOTION_DETAIL_ID,
							PROMOTION_ID,
							TANGGAL,
							BENTUK_DAN_JENIS_BIAYA,
							JUMLAH,
							JUMLAH_GROSS_UP,
							KETERANGAN,
							JUMLAH_PPH,
							JENIS_PPH,
							JUMLAH_NET,
							NOMOR_REKENING,
							NAMA_REKENING_PENERIMA,
							NAMA_BANK,
							NO_KTP, 
							CREATED_DT,
							CREATED_BY
						)
					SELECT  NEWID() AS PROMOTION_DETAIL_ID,
							@PromotionId AS PROMOTION_ID, 
							TANGGAL,
							BENTUK_DAN_JENIS_BIAYA,
							JUMLAH,
							JUMLAH_GROSS_UP,
							KETERANGAN,
							JUMLAH_PPH,
							JENIS_PPH,
							JUMLAH_NET,
							NOMOR_REKENING,
							NAMA_REKENING_PENERIMA,
							NAMA_BANK,
							NO_KTP,  	
							ISNULL(CHANGED_DT,CREATED_DT),
							ISNULL(CHANGED_BY,CREATED_BY)
					FROM TB_R_Promotion_Detail_Temp a
					WHERE a.PROMOTION_ID = @PromotionParamId

					FETCH NEXT FROM cursor_promotion INTO @PromotionParamId;
				END;

			CLOSE cursor_promotion;
			DEALLOCATE cursor_promotion;
										  
			UPDATE c
			SET RowStatus = 1
			FROM TB_R_Promotion_Detail_Temp c
				INNER JOIN TB_R_Promotion_Temp a ON C.PROMOTION_ID=A.PROMOTION_ID AND ISNULL(c.RowStatus,0)<>1
				INNER JOIN TB_R_Dashboard_Nominative_Temp b ON a.DASHBOARD_ID=b.Id AND ISNULL(a.RowStatus,0)<>1  
			WHERE  ISNULL(b.RowStatus,0)<>1 AND b.NominativeIDNo = @NominativeIDNo 

			UPDATE a
			SET RowStatus = 1
			FROM TB_R_Promotion_Temp a
				INNER JOIN TB_R_Dashboard_Nominative_Temp b ON a.DASHBOARD_ID=b.Id AND  ISNULL(a.RowStatus,0)<>1  
			WHERE  ISNULL(b.RowStatus,0)<>1 AND b.NominativeIDNo = @NominativeIDNo 


			UPDATE b
			SET RowStatus = 1
			FROM TB_R_Dashboard_Nominative_Temp b
			WHERE  ISNULL(b.RowStatus,0)<>1 
			AND b.NominativeIDNo = @NominativeIDNo

			DROP TABLE #LastDashboard
			FETCH NEXT FROM cursor_nominative INTO  @NominativeIDNo;
		END;

	CLOSE cursor_nominative;
	DEALLOCATE cursor_nominative;
COMMIT

GO