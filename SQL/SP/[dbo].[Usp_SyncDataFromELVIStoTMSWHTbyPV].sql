---$ Alter Procedure [dbo].[Usp_SyncDataFromELVIStoTMSWHTbyPV] 
IF OBJECT_ID(N'[dbo].[Usp_SyncDataFromELVIStoTMSWHTbyPV]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[Usp_SyncDataFromELVIStoTMSWHTbyPV]'
    EXECUTE('CREATE PROCEDURE [dbo].[Usp_SyncDataFromELVIStoTMSWHTbyPV] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[Usp_SyncDataFromELVIStoTMSWHTbyPV]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[Usp_SyncDataFromELVIStoTMSWHTbyPV] 
	-- Add the parameters for the stored procedure here
    @pvNumber INT
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

        UPDATE  dbo.TB_R_Dashboard_Nominative
        SET     StatusInvoice = a.StatusInvoice ,
                WitholdingTaxNo = trvi.NomorBuktiPotong ,
                WitholdingTaxDate = trvi.WHTTaxDate ,
                SAPInvoiceDocNo = a.SAP_DOC_NO ,
				SAPInvoicePostingDate= ISNULL(a.POSTING_DATE, trvi.PostingDate) ,
                BookPeriod = ISNULL(a.POSTING_DATE, trvi.PostingDate) ,
                ModifiedBy = 'JOBWHT' ,
                ModifiedOn = GETDATE()
        FROM    dbo.TB_R_Dashboard_Nominative AS trdn
                INNER JOIN ( SELECT CONVERT(VARCHAR(50), ph.PV_NO) PV_NO ,
                                    CASE WHEN ( ph.POSTING_DATE IS NULL
                                                OR ph.STATUS_CD <> '27'
                                              )--ph.POSTING_DATE IS NULL
                                              AND ph.SUBMIT_DATE IS NULL
                                              AND ISNULL(ph.CANCEL_FLAG, 0) = 0
                                         THEN 'DRAFT'
                                         WHEN ( ph.POSTING_DATE IS NULL
                                                OR ph.STATUS_CD <> '27'
                                              )
                                              AND ph.SUBMIT_DATE IS NOT NULL
                                              AND ISNULL(ph.CANCEL_FLAG, 0) = 0
                                         THEN 'REGISTERED'
                                         WHEN ph.POSTING_DATE IS NOT NULL
                                              AND ph.PAID_DATE IS NULL
                                              AND ISNULL(ph.CANCEL_FLAG, 0) = 0
                                              AND ph.STATUS_CD = '27'
                                         THEN 'POSTED'
                                         WHEN ISNULL(ph.CANCEL_FLAG, 0) = 1
                                         THEN 'CANCELLED'
                                         WHEN ph.PAID_DATE IS NOT NULL
                                              AND ph.CANCEL_FLAG = 0
                                         THEN 'PAID'
                                    END AS StatusInvoice ,
                                    ph.SAP_DOC_NO ,
                                    ph.POSTING_DATE ,
                                    ISNULL(ph.DELETED, 0) DELETED
                             FROM   ELVIS_DB.dbo.vw_PV_List AS ph
                             WHERE  ph.PV_NO = @pvNumber
                                    AND ISNULL(ph.DELETED, 0) = 0
                                    AND ISNULL(ph.ENOM_NO, '') != ''
                           ) AS a ON trdn.PVNo = a.PV_NO
                LEFT JOIN ( SELECT  trw.NomorPV ,
                                    trw.NomorBuktiPotong ,
                                    trw.PostingDate ,
                                    trw.TanggalBuktiPotong WHTTaxDate
                            FROM    dbo.TB_R_WHT AS trw
                            WHERE   trw.NomorPV = CONVERT(VARCHAR(50), @pvNumber)
                            GROUP BY trw.NomorPV ,
                                    trw.NomorBuktiPotong ,
                                    trw.PostingDate ,
                                    trw.TanggalBuktiPotong
                          ) AS trvi ON a.PV_NO = trvi.NomorPV
        WHERE   trdn.PVNo = CONVERT(VARCHAR(50), @pvNumber)
    END

GO
