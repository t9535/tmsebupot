---$ Alter Procedure [dbo].[usp_GetDonationByID] 
IF OBJECT_ID(N'[dbo].[usp_GetDonationByID]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_GetDonationByID]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_GetDonationByID] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_GetDonationByID]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

ALTER PROCEDURE usp_GetDonationByID

	-- Add the parameters for the stored procedure here
	@ID UNIQUEIDENTIFIER
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT A.*
	FROM dbo.TB_R_Donation A
	WHERE A.DONATION_ID=@ID 
END

GO