---$ Alter Procedure [dbo].[spGetNoTransactionEnom] 
IF OBJECT_ID(N'[dbo].[spGetNoTransactionEnom]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[spGetNoTransactionEnom]'
    EXECUTE('CREATE PROCEDURE [dbo].[spGetNoTransactionEnom] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[spGetNoTransactionEnom]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

ALTER PROCEDURE [dbo].[spGetNoTransactionEnom]  
    @branchID char(3) ,  
    @businessdate DATETIME ,  
    @ID AS VARCHAR(10) ,  
    @sequenceNo VARCHAR(100) OUTPUT  
AS   
    DECLARE @Error AS INT  
    SET @Error = 0   
    SET @sequenceno = dbo.spGenerateNoTransactionEnom(@branchID, @Businessdate,@ID)  
    UPDATE  dbo.TB_M_TransSequence  
    SET     sequenceno = sequenceno + 1  
    WHERE   mssequenceid = @ID  
           -- AND BranchID = @branchID  
    SET @Error = @@Error  
    IF @error > 0   
        GOTO ExitSP  
  
    RETURN @Error  
    ExitSP:  
    BEGIN   
        RETURN @Error  
    END

GO