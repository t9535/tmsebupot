---$ Alter Procedure [dbo].[usp_CountEntertainmentDetail] 
IF OBJECT_ID(N'[dbo].[usp_CountEntertainmentDetail]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_CountEntertainmentDetail]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_CountEntertainmentDetail] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_CountEntertainmentDetail]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

ALTER PROCEDURE usp_CountEntertainmentDetail
	@EntertainmentId uniqueidentifier
	AS
BEGIN
SELECT COUNT(ENTERTAINMENT_DETAIL_ID) FROM TB_R_Entertainment_Detail WHERE Entertainment_ID=@EntertainmentId
END

GO