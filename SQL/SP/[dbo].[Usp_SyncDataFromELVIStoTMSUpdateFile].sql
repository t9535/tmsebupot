---$ Alter Procedure [dbo].[Usp_SyncDataFromELVIStoTMSUpdateFile] 
IF OBJECT_ID(N'[dbo].[Usp_SyncDataFromELVIStoTMSUpdateFile]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[Usp_SyncDataFromELVIStoTMSUpdateFile]'
    EXECUTE('CREATE PROCEDURE [dbo].[Usp_SyncDataFromELVIStoTMSUpdateFile] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[Usp_SyncDataFromELVIStoTMSUpdateFile]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[Usp_SyncDataFromELVIStoTMSUpdateFile] 
	-- Add the parameters for the stored procedure here
	@pvNumber INT,
	@SourceFile VARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

UPDATE  dbo.TB_R_Dashboard_Nominative
SET     NominativeFormAttachment=@SourceFile, ReceiveFileDate=GETDATE()
WHERE   PVNo = CONVERT(VARCHAR(50), @pvNumber)
end

GO
