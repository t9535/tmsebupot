---$ Alter Procedure [dbo].[usp_GetPromotionByID] 
IF OBJECT_ID(N'[dbo].[usp_GetPromotionByID]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_GetPromotionByID]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_GetPromotionByID] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_GetPromotionByID]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

ALTER PROCEDURE usp_GetPromotionByID

	-- Add the parameters for the stored procedure here
	@ID UNIQUEIDENTIFIER
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT A.*
	FROM dbo.TB_R_Promotion A
	WHERE A.Promotion_ID=@ID 
END

GO