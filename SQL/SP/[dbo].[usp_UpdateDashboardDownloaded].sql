---$ Alter Procedure [dbo].[usp_UpdateDashboardDownloaded] 
IF OBJECT_ID(N'[dbo].[usp_UpdateDashboardDownloaded]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_UpdateDashboardDownloaded]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_UpdateDashboardDownloaded] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_UpdateDashboardDownloaded]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go


ALTER PROCEDURE [dbo].[usp_UpdateDashboardDownloaded]
	@DashboardIdList VARCHAR(MAX)
	AS
BEGIN	
	 UPDATE c
	 SET DownloadedDate =GETDATE(),
		 DownloadStatus ='Yes'
	 FROM TB_R_Dashboard_Nominative c
	 WHERE 
	 CASE WHEN ISNULL(@DashboardIdList,'') = '' THEN 1
		   ELSE CASE WHEN EXISTS(SELECT * FROM dbo.SplitString(@DashboardIdList,';') d WHERE c.Id = d.Item) THEN 1 ELSE 0 END
		   END=1 
		   AND ISNULL(c.NominativeFormAttachment,'N/A') <> 'N/A' 
END

GO
