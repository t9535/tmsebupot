---$ Alter Procedure [dbo].[usp_DropDownCategorygrouping] 
IF OBJECT_ID(N'[dbo].[usp_DropDownCategorygrouping]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_DropDownCategorygrouping]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_DropDownCategorygrouping] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_DropDownCategorygrouping]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

ALTER PROCEDURE [dbo].[usp_DropDownCategorygrouping]

	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		NominativeType Id, 
		NominativeType As Name 
	FROM TB_M_CategoryNominative_ENUM
	ORDER BY NominativeType
	
END

GO
