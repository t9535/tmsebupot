---$ Alter Procedure [dbo].[usp_GetReffNoDashboardUpload] 
IF OBJECT_ID(N'[dbo].[usp_GetReffNoDashboardUpload]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_GetReffNoDashboardUpload]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_GetReffNoDashboardUpload] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_GetReffNoDashboardUpload]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go



ALTER PROCEDURE [dbo].[usp_GetReffNoDashboardUpload] 
@Id varchar(max)
AS
    BEGIN	 
		--create by Ckristian sinulingga 27 Mei 2021		
		
		SELECT dbo.udf_DisplayUploadfile(@Id) as NominativeIDNo
	
    END

GO
