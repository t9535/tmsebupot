---$ Alter Procedure [dbo].[usp_GetAttachmentTypeReport_ENUM] 
IF OBJECT_ID(N'[dbo].[usp_GetAttachmentTypeReport_ENUM]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_GetAttachmentTypeReport_ENUM]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_GetAttachmentTypeReport_ENUM] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_GetAttachmentTypeReport_ENUM]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

ALTER PROCEDURE [dbo].[usp_GetAttachmentTypeReport_ENUM]
	@CategoryDesc varchar(100) = '',
	@Description varchar(100) = '',
	@FileTypeDesc varchar(500) = '',
	@Vendor_GroupDesc varchar(500),
	@SortBy int = 0,
	@SortDirection varchar(4) = 'DESC'
	
AS
BEGIN
	DECLARE @SortField varchar(50)
	SET @SortField = 
	CASE @SortBy 
	--WHEN 1 THEN 'CategoryCode'
	--WHEN 2 THEN 'Description'
	--WHEN 3 THEN 'FileType'
	--WHEN 4 THEN 'Vendor_Group'
	--ELSE 'CategoryCode' end; 

	WHEN 1 THEN 'AttachmentId'
	WHEN 2 THEN 'CategoryDesc'
	WHEN 3 THEN 'Description'
	WHEN 4 THEN 'FileTypeDesc'
	WHEN 5 THEN 'Vendor_GroupDesc'
	ELSE 'AttachmentId' end; 
	
	DECLARE 
	@Query VARCHAR(MAX) = 'SELECT
	ROW_NUMBER() OVER (ORDER BY '+ @SortField + ' ' + @SortDirection + ') AS RowNum,
	AttachmentId,
	CategoryDesc,
	Description,
	FileTypeDesc,
	Vendor_GroupDesc
	FROM dbo.TB_M_AttachmentType_ENUM
	WHERE AttachmentId > 0',
	@QueryCategoryDesc varchar(MAX) = '',
	@QueryDescription varchar(MAX) = '',
	@QueryFileTypeDesc varchar(MAX) = '',
	@QueryVendor_GroupDesc varchar(MAX) = ''

SELECT @QueryCategoryDesc = 
dbo.uf_LookupDynamicQueryGenerator(@CategoryDesc, 'CategoryDesc')
SELECT @QueryDescription = 
dbo.uf_LookupDynamicQueryGenerator(@Description, 'Description')
SELECT @QueryFileTypeDesc = 
dbo.uf_LookupDynamicQueryGenerator(@FileTypeDesc, 'FileTypeDesc')
SELECT @QueryVendor_GroupDesc = 
dbo.uf_LookupDynamicQueryGenerator(@Vendor_GroupDesc, 'Vendor_GroupDesc')

SET @Query = 'SELECT * FROM (' +
		ISNULL(@Query,'') + 
		ISNULL(@QueryCategoryDesc,'') +
		ISNULL(@QueryDescription,'') +
		ISNULL(@QueryFileTypeDesc,'') +
		ISNULL(@QueryVendor_GroupDesc,'') +
		' ) AS TBL '
EXEC(@Query)
END

GO
