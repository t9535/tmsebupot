---$ Alter Procedure [dbo].[usp_GetEntertainmentExcel] 
IF OBJECT_ID(N'[dbo].[usp_GetEntertainmentExcel]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_GetEntertainmentExcel]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_GetEntertainmentExcel] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_GetEntertainmentExcel]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go



ALTER PROCEDURE [dbo].[usp_GetEntertainmentExcel]
	@CategoryCode VARCHAR(MAX) = '', 
    @StatusInvoice VARCHAR(MAX) = '',
    @PVType VARCHAR(MAX) = '',
	@AccruedYearEndStatus VARCHAR(MAX) = '',
	@AccruedBookingNo VARCHAR(MAX) = '',
	@SettlementStatus VARCHAR(MAX) = '',
    @DescriptionTransaction VARCHAR(MAX) = '',
    @GLAccount VARCHAR(20) = '',
    @VendorName VARCHAR(200) = '',
    @Npwp VARCHAR(30) = '',
    @InvoiceNo VARCHAR(30) = '',
    @InvoiceDateFrom VARCHAR(50) = '',
    @InvoiceDateTo VARCHAR(50) = '',
    @BookPeriodFrom VARCHAR(50) = '',
    @BookPeriodTo VARCHAR(50) = '',
	@TurnoverAmountOperator VARCHAR(50) = '',
    @TurnoverAmount DECIMAL(18,0),
    @VATAmountOperator VARCHAR(50) = '',
    @VATAmount DECIMAL(18,0),
    @WHTAmountOperator VARCHAR(50) = '',
    @WHTAmount  DECIMAL(18,0), 
    @TaxInvoiceNo VARCHAR(30) ='',
    @WitholdingTaxNo VARCHAR(30) = '',
    @WitholdingTaxDateFrom VARCHAR(50) = '',
    @WitholdingTaxDateTo VARCHAR(50) = '',
    @WHTArticle VARCHAR(MAX) = '',
    @System VARCHAR(MAX) = '',
    @PVNo VARCHAR(30) = '',
    @PVCreatedBy VARCHAR(30) = '',
    @SAPGRDocNo VARCHAR(MAX) = '',
    @SAPGRPostingDateFrom VARCHAR(50) = '',
    @SAPGRPostingDateTo VARCHAR(50) = '',
    @SAPInvoiceDocNo VARCHAR(MAX) = '',
    @SAPInvoicePostingDateFrom VARCHAR(50) = '',
    @SAPInvoicePostingDateTo VARCHAR(50) = '',
    @ReceiveFileDateFrom VARCHAR(50) = '',
    @ReceiveFileDateTo VARCHAR(50) = '',
    @DownloadedDateFrom VARCHAR(50) = '',
    @DownloadedDateTo VARCHAR(50) = '',
    @DownloadStatus VARCHAR(MAX) = '',
    @NominativeFormAttachment VARCHAR(MAX) = '',
    @NominativeIDNo VARCHAR(30) = '',
    @NominativeStatus VARCHAR(MAX) = '',
    @NominativeDateFrom VARCHAR(50) = '',
    @NominativeDateTo VARCHAR(50) = '',
	@DashboardIdList VARCHAR(MAX)
	AS
BEGIN
	  DECLARE 
	@Query VARCHAR(MAX) = 'SELECT c.NominativeIDNo, a.NO, a.TANGGAL, a.TEMPAT, a.ALAMAT, a.BENTUK_DAN_JENIS_ENTERTAINMENT, a.JUMLAH,
			b.NAMA_RELASI, b.POSISI_RELASI, b.PERUSAHAAN_RELASI, b.JENIS_USAHA_RELASI, b.KETERANGAN, NO_BUKTI_POTONG = c.WitholdingTaxNo
	 FROM TB_R_Entertainment a 
	 INNER JOIN TB_R_Entertainment_Detail b ON a.ENTERTAINMENT_ID=b.ENTERTAINMENT_ID 
	 INNER JOIN TB_R_Dashboard_Nominative c ON a.DASHBOARD_ID = c.Id 
	 WHERE CASE WHEN '''+ISNULL(@NominativeFormAttachment,'')+'''=''Yes'' THEN CASE WHEN (CASE WHEN ISNULL(c.NominativeFormAttachment,'''')='''' THEN ''N/A'' ELSE c.NominativeFormAttachment END) <> ''N/A'' THEN 1 ELSE 0 END
		  WHEN '''+ISNULL(@NominativeFormAttachment,'')+'''=''N/A'' THEN CASE WHEN (CASE WHEN ISNULL(c.NominativeFormAttachment,'''')='''' THEN ''N/A'' ELSE c.NominativeFormAttachment END) = ''N/A'' THEN 1 ELSE 0 END
		  ELSE 1 END = 1 
	 AND CASE WHEN ISNULL('''+@DashboardIdList+''','''') = '''' THEN 1
		   ELSE CASE WHEN EXISTS(SELECT * FROM dbo.SplitString('''+@DashboardIdList+''','';'') d WHERE a.DASHBOARD_ID = d.Item) THEN 1 ELSE 0 END
		   END=1',
	@QueryCategoryCode varchar(MAX) = '', 
	@QueryStatusInvoice varchar(MAX) = '',
	@QueryPVType varchar(MAX) = '',
	@QueryAccruedYearEndStatus varchar(MAX) = '',
	@QueryAccruedBookingNo varchar(MAX) = '',
	@QuerySettlementStatus varchar(MAX) = '',
	@QueryDescriptionTransaction varchar(MAX) = '',
	@QueryGLAccount varchar(MAX) = '',
	@QueryVendorName varchar(MAX) = '',
	@QueryNpwp varchar(MAX) = '',
	@QueryInvoiceNo varchar(MAX) = '',
	@QueryInvoiceDate varchar(MAX) = '',
	@QueryBookPeriod varchar(MAX) = '', 
	@QueryTurnoverAmount varchar(MAX) = '',
	@QueryVATAmount varchar(MAX) = '',
	@QueryWHTAmount varchar(MAX) = '',
	@QueryTaxInvoiceNo varchar(MAX) = '',
	@QueryWitholdingTaxNo varchar(MAX) = '',
	@QueryWitholdingTaxDate varchar(MAX) = '',
	@QueryWHTArticle varchar(MAX) = '',
	@QuerySystem varchar(MAX) = '',
	@QueryPVNo varchar(MAX) = '',
	@QueryPVCreatedBy varchar(MAX) = '',
	@QuerySAPGRDocNo varchar(MAX) = '',
	@QuerySAPGRPostingDate varchar(MAX) = '',
	@QuerySAPInvoiceDocNo varchar(MAX) = '',
	@QuerySAPInvoicePostingDate varchar(MAX) = '',
	@QueryReceiveFileDate varchar(MAX) = '',
	@QueryDownloadedDate varchar(MAX) = '',
	@QueryDownloadStatus varchar(MAX) = '',
	@QueryNominativeFormAttachment varchar(MAX) = '',
	@QueryNominativeIDNo varchar(MAX) = '',
	@QueryNominativeStatus varchar(MAX) = '',
	@QueryNominativeDate varchar(MAX) = ''

SELECT 
@QueryCategoryCode = dbo.uf_LookupDynamicQueryGenerator(@CategoryCode, 'c.CategoryCode'), 
@QueryStatusInvoice = dbo.uf_LookupDynamicQueryGenerator(@StatusInvoice, 'c.StatusInvoice'),
@QueryPVType = dbo.uf_LookupDynamicQueryGenerator(@PVType, 'c.PVType'),
@QueryAccruedYearEndStatus = dbo.uf_LookupDynamicQueryGenerator(@AccruedYearEndStatus, 'c.AccruedYearEndStatus'),
@QueryAccruedBookingNo = dbo.uf_LookupDynamicQueryGenerator(@AccruedBookingNo, 'c.AccruedBookingNo'),
--@QuerySettlementStatus = dbo.uf_LookupDynamicQueryGenerator(@PVType, 'c.SettlementStatus'),
@QuerySettlementStatus = dbo.uf_LookupDynamicQueryGenerator(@SettlementStatus, 'c.SettlementStatus'),
@QueryDescriptionTransaction = dbo.uf_LookupDynamicQueryGenerator(@DescriptionTransaction, 'c.DescriptionTransaction'), 
@QueryGLAccount = dbo.uf_LookupDynamicQueryGenerator(@GLAccount, 'c.GLAccount'),
@QueryVendorName = dbo.uf_LookupDynamicQueryGenerator(@VendorName, 'c.VendorName'),
@QueryNpwp = dbo.uf_LookupDynamicQueryGenerator(@Npwp, 'c.Npwp'),
@QueryInvoiceNo = dbo.uf_LookupDynamicQueryGenerator(@InvoiceNo, 'c.InvoiceNo'),
@QueryInvoiceDate = dbo.uf_DateRangeDynamicQueryGenerator(@InvoiceDateFrom + ' ' + '0:00 AM', @InvoiceDateTo + ' ' + '11:59 PM', 'c.InvoiceDate'),
@QueryBookPeriod = dbo.uf_DateRangeDynamicQueryGenerator(@BookPeriodFrom + ' ' + '0:00 AM', @BookPeriodTo + ' ' + '11:59 PM', 'c.BookPeriod'), 
@QueryTurnoverAmount = dbo.uf_DecimalRangeDynamicQueryGenerator(@TurnoverAmountOperator, @TurnoverAmount, 'c.TurnoverAmount'),
@QueryVATAmount = dbo.uf_DecimalRangeDynamicQueryGenerator(@VATAmountOperator, @VATAmount, 'c.VATAmount'),
@QueryWHTAmount = dbo.uf_DecimalRangeDynamicQueryGenerator(@WHTAmountOperator, @WHTAmount, 'c.WHTAmount'),
@QueryTaxInvoiceNo = dbo.uf_LookupDynamicQueryGenerator(@TaxInvoiceNo, 'c.TaxInvoiceNo'),
@QueryWitholdingTaxNo = dbo.uf_LookupDynamicQueryGenerator(@WitholdingTaxNo, 'c.WitholdingTaxNo'), 
@QueryWitholdingTaxDate = dbo.uf_DateRangeDynamicQueryGenerator(@WitholdingTaxDateFrom + ' ' + '0:00 AM', @WitholdingTaxDateTo + ' ' + '11:59 PM', 'c.WitholdingTaxDate'),
@QueryWHTArticle = dbo.uf_LookupDynamicQueryGenerator(@WHTArticle, 'c.WHTArticle'),
@QuerySystem = dbo.uf_LookupDynamicQueryGenerator(@System, 'c.System'),
@QueryPVNo = dbo.uf_LookupDynamicQueryGenerator(@PVNo, 'c.PVNo'),
@QueryPVCreatedBy = dbo.uf_LookupDynamicQueryGenerator(@PVCreatedBy, 'c.CreatedBy'),
@QuerySAPGRDocNo = dbo.uf_LookupDynamicQueryGenerator(@SAPGRDocNo, 'c.SAPGRDocNo'),
@QuerySAPGRPostingDate = dbo.uf_DateRangeDynamicQueryGenerator(@SAPGRPostingDateFrom + ' ' + '0:00 AM', @SAPGRPostingDateTo + ' ' + '11:59 PM', 'c.SAPGRPostingDate'),
@QuerySAPInvoiceDocNo = dbo.uf_LookupDynamicQueryGenerator(@SAPInvoiceDocNo, 'c.SAPInvoiceDocNo'),
@QuerySAPInvoicePostingDate = dbo.uf_DateRangeDynamicQueryGenerator(@SAPInvoicePostingDateFrom + ' ' + '0:00 AM', @SAPInvoicePostingDateTo + ' ' + '11:59 PM', 'c.SAPInvoicePostingDate'),
@QueryReceiveFileDate = dbo.uf_DateRangeDynamicQueryGenerator(@ReceiveFileDateFrom + ' ' + '0:00 AM', @ReceiveFileDateTo + ' ' + '11:59 PM', 'c.ReceiveFileDate'),
@QueryDownloadedDate = dbo.uf_DateRangeDynamicQueryGenerator(@DownloadedDateFrom + ' ' + '0:00 AM', @DownloadedDateTo + ' ' + '11:59 PM', 'c.DownloadedDate'), 
@QueryDownloadStatus = dbo.uf_LookupDynamicQueryGenerator(@DownloadStatus, 'c.DownloadStatus'),
--@QueryNominativeFormAttachment = dbo.uf_LookupDynamicQueryGenerator(@NominativeFormAttachment, 'c.NominativeFormAttachment'),
@QueryNominativeIDNo = dbo.uf_LookupDynamicQueryGenerator(@NominativeIDNo, 'c.NominativeIDNo'),
@QueryNominativeStatus = dbo.uf_LookupDynamicQueryGenerator(@NominativeStatus, 'c.NominativeStatus'),
@QueryNominativeDate = dbo.uf_DateRangeDynamicQueryGenerator(@NominativeDateFrom + ' ' + '0:00 AM', @NominativeDateTo + ' ' + '11:59 PM', 'c.NominativeDate')

SET @Query =  @Query + 
		@QueryCategoryCode + 
		@QueryStatusInvoice +
		@QueryPVType +
		@QueryAccruedYearEndStatus +
		@QueryAccruedBookingNo +
		@QuerySettlementStatus +
		@QueryDescriptionTransaction +
		@QueryGLAccount +
		@QueryVendorName +
		@QueryNpwp +
		@QueryInvoiceNo +
		@QueryInvoiceDate +
		@QueryBookPeriod +
		@QueryTurnoverAmount +
		@QueryVATAmount +
		@QueryWHTAmount +
		@QueryTaxInvoiceNo +
		@QueryWitholdingTaxNo +
		@QueryWitholdingTaxDate +
		@QueryWHTArticle +
		@QuerySystem +
		@QueryPVNo +
		@QueryPVCreatedBy +
		@QuerySAPGRDocNo +
		@QuerySAPGRPostingDate +
		@QuerySAPInvoiceDocNo +
		@QuerySAPInvoicePostingDate +
		@QueryReceiveFileDate +
		@QueryDownloadedDate +
		@QueryDownloadStatus +
		@QueryNominativeFormAttachment +
		@QueryNominativeIDNo +
		@QueryNominativeStatus +
		@QueryNominativeDate +
		' ORDER BY a.DASHBOARD_ID,a.NO' 
EXEC(@Query) 
END

GO
