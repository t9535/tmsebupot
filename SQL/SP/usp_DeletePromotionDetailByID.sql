---$ Alter Procedure [dbo].[usp_DeletePromotionDetailByID] 
IF OBJECT_ID(N'[dbo].[usp_DeletePromotionDetailByID]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_DeletePromotionDetailByID]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_DeletePromotionDetailByID] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_DeletePromotionDetailByID]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

ALTER PROCEDURE usp_DeletePromotionDetailByID

	-- Add the parameters for the stored procedure here
	@ID UNIQUEIDENTIFIER
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE A
	FROM dbo.TB_R_Promotion_Detail A
	WHERE A.PROMOTION_DETAIL_ID=@ID 
END

GO