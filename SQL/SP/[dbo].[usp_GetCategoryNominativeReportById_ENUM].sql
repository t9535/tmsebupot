---$ Alter Procedure [dbo].[usp_GetCategoryNominativeReportById_ENUM] 
IF OBJECT_ID(N'[dbo].[usp_GetCategoryNominativeReportById_ENUM]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_GetCategoryNominativeReportById_ENUM]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_GetCategoryNominativeReportById_ENUM] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_GetCategoryNominativeReportById_ENUM]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

ALTER PROCEDURE [dbo].[usp_GetCategoryNominativeReportById_ENUM]

	@CategoryCode varchar(10) = '',
	@NominativeType varchar(50) = '',
	@JointGroup varchar(50) = '',
	@GroupSeq varchar(10),
	@SortBy int = 1,
	@SortDirection varchar(4) = 'DESC'
	
AS
BEGIN
	DECLARE @SortField varchar(50)
	SET @SortField = 
	CASE @SortBy 
	WHEN 1 THEN 'CreatedDate'
	WHEN 2 THEN 'CategoryCode'
	WHEN 3 THEN 'JointGroup'
	WHEN 4 THEN 'GroupSeq'
	WHEN 5 THEN 'NominativeType'
	WHEN 6 THEN 'PositionSignature'
	ELSE 'CategoryCode' end; 
	
	DECLARE 
	@Query VARCHAR(MAX) = 'SELECT
	ROW_NUMBER() OVER (ORDER BY '+ @SortField + ' ' + @SortDirection + ') AS RowNum,
	CategoryCode,
	JointGroup,
	GroupSeq,
	NominativeType,
	PositionSignature
	FROM dbo.TB_M_CategoryNominative_ENUM
	WHERE 1=1',
	@QueryCategoryCode varchar(MAX) = '',
	@QueryNominativeType varchar(MAX) = '',
	@QueryJointGroup varchar(MAX) = '',
	@QueryGroupSeq varchar(MAX) = ''

SELECT @QueryCategoryCode = 
dbo.uf_LookupDynamicQueryGenerator(@CategoryCode, 'CategoryCode')
SELECT @QueryNominativeType = 
dbo.uf_LookupDynamicQueryGenerator(@NominativeType, 'NominativeType')
SELECT @QueryJointGroup = 
dbo.uf_LookupDynamicQueryGenerator(@JointGroup, 'JointGroup')
SELECT @QueryGroupSeq = 
dbo.uf_LookupDynamicQueryGenerator(@GroupSeq, 'GroupSeq')

SET @Query = 'SELECT * FROM (' +
		@Query + 
		@QueryCategoryCode +
		@QueryNominativeType +
		@QueryJointGroup +
		@QueryGroupSeq +
		' ) AS TBL '
EXEC(@Query)
END

GO