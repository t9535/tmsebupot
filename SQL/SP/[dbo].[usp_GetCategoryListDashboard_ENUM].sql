---$ Alter Procedure [dbo].[usp_GetCategoryListDashboard_ENUM] 
IF OBJECT_ID(N'[dbo].[usp_GetCategoryListDashboard_ENUM]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_GetCategoryListDashboard_ENUM]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_GetCategoryListDashboard_ENUM] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_GetCategoryListDashboard_ENUM]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

--Execute usp_GetCategoryListDashboard_ENUM '','','','','1','ASC','1','10'

ALTER PROCEDURE [dbo].[usp_GetCategoryListDashboard_ENUM]
	@CategoryCode varchar(100) = '',
	@NominativeType varchar(50) = '',
	@JointGroup varchar(50) = '',
	@GroupSeq varchar(10),
	@SortBy int = 1,
	@SortDirection varchar(4) = 'DESC',
	@FromNumber int=0,
	@ToNumber int
	
AS
BEGIN
	DECLARE @SortField varchar(50)
	SET @SortField = 
	CASE @SortBy 
	WHEN 1 THEN 'CreatedDate'
	WHEN 2 THEN 'NominativeType'
	WHEN 3 THEN 'JointGroup'
	WHEN 4 THEN 'GroupSeq'
	WHEN 5 THEN 'NominativeType'
	WHEN 6 THEN 'PositionSignature'
	ELSE 'NominativeType' end; 
	
	DECLARE 
	@Query VARCHAR(MAX) = 'SELECT
	ROW_NUMBER() OVER (ORDER BY '+ @SortField + ' ' + @SortDirection + ') AS RowNum,
	CategoryCode,
	NominativeType as CategoryCodeas,
	JointGroup,
	CreatedDate,
	GroupSeq,
	NominativeType,
	case when PositionSignature =1 then ''Enable'' else ''Disable'' end PositionSignature
	FROM dbo.TB_M_CategoryNominative_ENUM
	WHERE 1=1',
	@QueryCategoryCode varchar(MAX) = '',
	@QueryNominativeType varchar(MAX) = '',
	@QueryJointGroup varchar(MAX) = '',
	@QueryGroupSeq varchar(MAX) = ''

SELECT @QueryCategoryCode = 
dbo.uf_LookupDynamicQueryGenerator(@CategoryCode, 'NominativeType')
SELECT @QueryNominativeType = 
dbo.uf_LookupDynamicQueryGenerator(@NominativeType, 'NominativeType')
SELECT @QueryJointGroup = 
dbo.uf_LookupDynamicQueryGenerator(@JointGroup, 'JointGroup')
SELECT @QueryGroupSeq = 
dbo.uf_LookupDynamicQueryGenerator(@GroupSeq, 'GroupSeq')

SET @Query = 'SELECT * FROM (' +
		@Query + 
		@QueryCategoryCode +
		@QueryNominativeType +
		@QueryJointGroup +
		@QueryGroupSeq +
		' ) AS TBL WHERE RowNum BETWEEN ' +
		CONVERT( VARCHAR,@FromNumber) + ' AND ' + CONVERT( VARCHAR,@ToNumber) 

EXEC(@Query)
--print @query
END

GO