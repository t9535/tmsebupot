---$ Alter Procedure [dbo].[usp_GetNominativeDataByID_ENUM] 
IF OBJECT_ID(N'[dbo].[usp_GetNominativeDataByID_ENUM]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_GetNominativeDataByID_ENUM]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_GetNominativeDataByID_ENUM] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_GetNominativeDataByID_ENUM]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE usp_GetNominativeDataByID_ENUM

	-- Add the parameters for the stored procedure here
	@NominativeId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
		select NominativeId,a.CategoryCode,b.NominativeType as CategoryName,JointGroup,GroupSeq,PositionSignature
		, GLAccount,DescriptionTransaction,Type 
		,a.CreatedBy,a.CreatedDate,a.ModifiedBy,a.ModifiedDate from TB_M_Nominative_ENUM as a
		inner join TB_M_CategoryNominative_ENUM as b on a.CategoryCode=b.CategoryCode WHERE [NominativeId] = @NominativeId
	
END

GO
