---$ Alter Procedure [dbo].[usp_GetPromotionFromPdf] 
IF OBJECT_ID(N'[dbo].[usp_GetPromotionFromPdf]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_GetPromotionFromPdf]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_GetPromotionFromPdf] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_GetPromotionFromPdf]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

 
ALTER PROCEDURE [dbo].[usp_GetPromotionFromPdf]
	@DashboardId uniqueidentifier
	AS
BEGIN	
	SELECT DENSE_RANK() OVER (PARTITION  BY ISNULL(a.THN_PAJAK,0) ORDER BY a.NO,a.CREATED_DT) NO,
			(SELECT TOP 1 CD.ConfigValue FROM TB_M_ConfigurationData CD WHERE CD.ConfigKey = 'CompanyName') AS CompanyName, 
			(SELECT TOP 1 CD.ConfigValue FROM TB_M_ConfigurationData CD WHERE CD.ConfigKey = 'CompanyNPWP') AS CompanyNPWP,
			(SELECT TOP 1 CD.ConfigValue FROM TB_M_ConfigurationData CD WHERE CD.ConfigKey = 'CompanyAddress') AS CompanyAddress, 
			c.NominativeIDNo AS REFERENCE_NO, ISNULL(c.PVNO,'-') AS PV_NO, c.WitholdingTaxNo AS NOMOR_BUKTI_POTONG,
			ISNULL(a.THN_PAJAK,0) AS THN_PAJAK, a.TMP_PENANDATANGAN, a.TGL_PENANDATANGAN, a.NAMA_PENANDATANGAN, a.JABATAN_PENANDATANGAN,
			--ISNULL(a.NO,0) AS NO,
			 a.NAMA, a.NPWP, a.ALAMAT, b.TANGGAL, b.BENTUK_DAN_JENIS_BIAYA,
			ISNULL(b.JUMLAH,0) AS JUMLAH, ISNULL(b.JUMLAH_GROSS_UP,0) AS JUMLAH_GROSS_UP, b.KETERANGAN, ISNULL(b.JUMLAH_PPH,0) AS JUMLAH_PPH, 
			b.JENIS_PPH, ISNULL(b.JUMLAH_NET,0) AS JUMLAH_NET,
			b.NOMOR_REKENING, b.NAMA_REKENING_PENERIMA, b.NAMA_BANK, b.NO_KTP			
	 FROM TB_R_Promotion a 
	 INNER JOIN TB_R_Promotion_Detail b ON a.PROMOTION_ID=b.PROMOTION_ID 
	 INNER JOIN TB_R_Dashboard_Nominative c ON a.DASHBOARD_ID = c.Id
	 WHERE a.DASHBOARD_ID = @DashboardId
END

GO

---$ Alter Procedure [dbo].[usp_GetPromotionReport] 
IF OBJECT_ID(N'[dbo].[usp_GetPromotionReport]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_GetPromotionReport]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_GetPromotionReport] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_GetPromotionReport]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go



ALTER PROCEDURE [dbo].[usp_GetPromotionReport]
	@CategoryCode VARCHAR(MAX) = '', 
    @StatusInvoice VARCHAR(MAX) = '',
    @PVType VARCHAR(MAX) = '',
	@AccruedYearEndStatus VARCHAR(MAX) = '',
	@AccruedBookingNo VARCHAR(MAX) = '',
	@SettlementStatus VARCHAR(MAX) = '',
    @DescriptionTransaction VARCHAR(MAX) = '',
    @GLAccount VARCHAR(20) = '',
    @VendorName VARCHAR(200) = '',
    @Npwp VARCHAR(30) = '',
    @InvoiceNo VARCHAR(30) = '',
    @InvoiceDateFrom VARCHAR(50) = '',
    @InvoiceDateTo VARCHAR(50) = '',
    @BookPeriodFrom VARCHAR(50) = '',
    @BookPeriodTo VARCHAR(50) = '',
	@TurnoverAmountOperator VARCHAR(50) = '',
    @TurnoverAmount DECIMAL(18,0),
    @VATAmountOperator VARCHAR(50) = '',
    @VATAmount DECIMAL(18,0),
    @WHTAmountOperator VARCHAR(50) = '',
    @WHTAmount  DECIMAL(18,0), 
    @TaxInvoiceNo VARCHAR(30) ='',
    @WitholdingTaxNo VARCHAR(30) = '',
    @WitholdingTaxDateFrom VARCHAR(50) = '',
    @WitholdingTaxDateTo VARCHAR(50) = '',
    @WHTArticle VARCHAR(MAX) = '',
    @System VARCHAR(MAX) = '',
    @PVNo VARCHAR(30) = '',
    @PVCreatedBy VARCHAR(30) = '',
    @SAPGRDocNo VARCHAR(MAX) = '',
    @SAPGRPostingDateFrom VARCHAR(50) = '',
    @SAPGRPostingDateTo VARCHAR(50) = '',
    @SAPInvoiceDocNo VARCHAR(MAX) = '',
    @SAPInvoicePostingDateFrom VARCHAR(50) = '',
    @SAPInvoicePostingDateTo VARCHAR(50) = '',
    @ReceiveFileDateFrom VARCHAR(50) = '',
    @ReceiveFileDateTo VARCHAR(50) = '',
    @DownloadedDateFrom VARCHAR(50) = '',
    @DownloadedDateTo VARCHAR(50) = '',
    @DownloadStatus VARCHAR(MAX) = '',
    @NominativeFormAttachment VARCHAR(MAX) = '',
    @NominativeIDNo VARCHAR(30) = '',
    @NominativeStatus VARCHAR(MAX) = '',
    @NominativeDateFrom VARCHAR(50) = '',
    @NominativeDateTo VARCHAR(50) = '',
	@DashboardIdList VARCHAR(MAX)
	AS
BEGIN 
	 DECLARE @Query VARCHAR(MAX) = '
		SELECT CONVERT(uniqueidentifier,NULL) AS DASHBOARD_ID,
				   NominativeIDNo
			INTO #Groupnominative
			FROM dbo.TB_R_Dashboard_Nominative 
			WHERE ISNULL(NominativeIDNo,'''')<>''''
			 AND CASE WHEN ISNULL('''+@DashboardIdList+''','''') = '''' THEN 1
				   ELSE CASE WHEN EXISTS(SELECT * FROM dbo.SplitString('''+@DashboardIdList+''','';'') d WHERE CONVERT(VARCHAR(MAX),ID) = d.Item) THEN 1 ELSE 0 END
				   END=1
			GROUP BY NominativeIDNo
			HAVING count(0)>1

			UPDATE A
			SET DASHBOARD_ID = B.ID
			FROM #Groupnominative A
			INNER JOIN dbo.TB_R_Dashboard_Nominative B ON A.NominativeIDNo = B.NominativeIDNo 

		SELECT DENSE_RANK() OVER (PARTITION  BY y.THN_PAJAK ORDER BY c.NominativeIDNo) NONominative,
			(SELECT TOP 1 CD.ConfigValue FROM TB_M_ConfigurationData CD WHERE CD.ConfigKey = ''CompanyName'') AS CompanyName, 
			(SELECT TOP 1 CD.ConfigValue FROM TB_M_ConfigurationData CD WHERE CD.ConfigKey = ''CompanyNPWP'') AS CompanyNPWP,
			(SELECT TOP 1 CD.ConfigValue FROM TB_M_ConfigurationData CD WHERE CD.ConfigKey = ''CompanyAddress'') AS CompanyAddress, 
			c.NominativeIDNo AS REFERENCE_NO, ISNULL(c.PVNO,''-'') AS PV_NO, c.WitholdingTaxNo AS NOMOR_BUKTI_POTONG,
			datefromparts(y.THN_PAJAK,1,1) PAJAK_FROM, datefromparts(y.THN_PAJAK,12,DAY(EOMONTH(datefromparts(y.THN_PAJAK,12,1)))) PAJAK_TO,	
			DENSE_RANK() OVER (PARTITION  BY ISNULL(y.THN_PAJAK,0) ORDER BY c.NominativeIDNo, y.NO, y.CREATED_DT) NO,
			y.THN_PAJAK, y.TMP_PENANDATANGAN, y.TGL_PENANDATANGAN, y.NAMA_PENANDATANGAN, y.JABATAN_PENANDATANGAN,
			y.NAMA, y.NPWP, y.ALAMAT, y.TANGGAL, y.BENTUK_DAN_JENIS_BIAYA,
			y.JUMLAH, y.JUMLAH_GROSS_UP, y.KETERANGAN, y.JUMLAH_PPH, y.JENIS_PPH, y.JUMLAH_NET,
			y.NOMOR_REKENING, y.NAMA_REKENING_PENERIMA, y.NAMA_BANK, y.NO_KTP	 
	 FROM (
			SELECT a.DASHBOARD_ID, ISNULL(a.THN_PAJAK,0) AS THN_PAJAK, a.TMP_PENANDATANGAN, a.TGL_PENANDATANGAN, a.NAMA_PENANDATANGAN, a.JABATAN_PENANDATANGAN,
				a.NO, a.NAMA, a.NPWP, a.ALAMAT, b.TANGGAL, b.BENTUK_DAN_JENIS_BIAYA,
				ISNULL(b.JUMLAH,0) AS JUMLAH, ISNULL(b.JUMLAH_GROSS_UP,0) AS JUMLAH_GROSS_UP, b.KETERANGAN, 
				ISNULL(b.JUMLAH_PPH,0) AS JUMLAH_PPH, 
				b.JENIS_PPH, ISNULL(b.JUMLAH_NET,0) AS JUMLAH_NET,
				b.NOMOR_REKENING, b.NAMA_REKENING_PENERIMA, b.NAMA_BANK, b.NO_KTP, a.CREATED_DT
			FROM TB_R_Promotion a
			INNER JOIN TB_R_Promotion_Detail b ON a.PROMOTION_ID=b.PROMOTION_ID
			INNER JOIN TB_R_Dashboard_Nominative w ON a.DASHBOARD_ID = w.Id 
			WHERE NOT EXISTS (SELECT * FROM #Groupnominative o WHERE w.NominativeIDNo = o.NominativeIDNo)
			UNION ALL
			SELECT a.DASHBOARD_ID, ISNULL(a.THN_PAJAK,0) AS THN_PAJAK, a.TMP_PENANDATANGAN, a.TGL_PENANDATANGAN, a.NAMA_PENANDATANGAN, a.JABATAN_PENANDATANGAN,
					0 NO, a.NAMA_LEMBAGA_PENERIMA AS NAMA, NPWP_LEMBAGA_PENERIMA AS NPWP, ALAMAT_LEMBAGA AS ALAMAT, TANGGAL_SUMBANGAN AS TANGGAL, 
					JENIS_SUMBANGAN AS BENTUK_DAN_JENIS_BIAYA, NILAI_SUMBANGAN AS JUMLAH, 0 JUMLAH_GROSS_UP, NULL KETERANGAN, 0 JUMLAH_PPH,
					NULL JENIS_PPH, 0 JUMLAH_NET, a.NOMOR_REKENING, A.NAMA_REKENING_PENERIMA, a.NAMA_BANK, NULL NO_KTP, a.CREATED_DT
			FROM TB_R_Donation a 
			INNER JOIN TB_R_Dashboard_Nominative w ON a.DASHBOARD_ID = w.Id 
			WHERE NOT EXISTS (SELECT * FROM #Groupnominative o WHERE w.NominativeIDNo = o.NominativeIDNo)

			UNION ALL
			SELECT a.DASHBOARD_ID, ISNULL(a.THN_PAJAK,0) AS THN_PAJAK, a.TMP_PENANDATANGAN, a.TGL_PENANDATANGAN, a.NAMA_PENANDATANGAN, a.JABATAN_PENANDATANGAN,
				a.NO, a.NAMA, a.NPWP, a.ALAMAT, b.TANGGAL, b.BENTUK_DAN_JENIS_BIAYA,
				ISNULL(b.JUMLAH,0) AS JUMLAH, ISNULL(b.JUMLAH_GROSS_UP,0) AS JUMLAH_GROSS_UP, b.KETERANGAN, 
				ISNULL(b.JUMLAH_PPH,0) AS JUMLAH_PPH, 
				b.JENIS_PPH, ISNULL(b.JUMLAH_NET,0) AS JUMLAH_NET,
				b.NOMOR_REKENING, b.NAMA_REKENING_PENERIMA, b.NAMA_BANK, b.NO_KTP, a.CREATED_DT
			FROM TB_R_Promotion a
			INNER JOIN TB_R_Promotion_Detail b ON a.PROMOTION_ID=b.PROMOTION_ID
			WHERE EXISTS (SELECT * FROM #Groupnominative o WHERE o.DASHBOARD_ID=a.DASHBOARD_ID)
			UNION ALL
			SELECT a.DASHBOARD_ID, ISNULL(a.THN_PAJAK,0) AS THN_PAJAK, a.TMP_PENANDATANGAN, a.TGL_PENANDATANGAN, a.NAMA_PENANDATANGAN, a.JABATAN_PENANDATANGAN,
					0 NO, a.NAMA_LEMBAGA_PENERIMA AS NAMA, NPWP_LEMBAGA_PENERIMA AS NPWP, ALAMAT_LEMBAGA AS ALAMAT, TANGGAL_SUMBANGAN AS TANGGAL, 
					JENIS_SUMBANGAN AS BENTUK_DAN_JENIS_BIAYA, NILAI_SUMBANGAN AS JUMLAH, 0 JUMLAH_GROSS_UP, NULL KETERANGAN, 0 JUMLAH_PPH,
					NULL JENIS_PPH, 0 JUMLAH_NET, a.NOMOR_REKENING, A.NAMA_REKENING_PENERIMA, a.NAMA_BANK, NULL NO_KTP, a.CREATED_DT
			FROM TB_R_Donation a 
			WHERE EXISTS (SELECT * FROM #Groupnominative o WHERE o.DASHBOARD_ID=a.DASHBOARD_ID)
	 ) y
	 INNER JOIN TB_R_Dashboard_Nominative c ON y.DASHBOARD_ID = c.Id 
	 WHERE CASE WHEN '''+ISNULL(@NominativeFormAttachment,'')+'''=''Yes'' THEN CASE WHEN (CASE WHEN ISNULL(c.NominativeFormAttachment,'''')='''' THEN ''N/A'' ELSE c.NominativeFormAttachment END) <> ''N/A'' THEN 1 ELSE 0 END
		  WHEN '''+ISNULL(@NominativeFormAttachment,'')+'''=''N/A'' THEN CASE WHEN (CASE WHEN ISNULL(c.NominativeFormAttachment,'''')='''' THEN ''N/A'' ELSE c.NominativeFormAttachment END) = ''N/A'' THEN 1 ELSE 0 END
		  ELSE 1 END = 1 
	 AND CASE WHEN ISNULL('''+@DashboardIdList+''','''') = '''' THEN 1
		   ELSE CASE WHEN EXISTS(SELECT * FROM dbo.SplitString('''+@DashboardIdList+''','';'') d WHERE y.DASHBOARD_ID = d.Item) THEN 1 ELSE 0 END
		   END=1',
	@QueryCategoryCode varchar(MAX) = '', 
	@QueryStatusInvoice varchar(MAX) = '',
	@QueryPVType varchar(MAX) = '',
	@QueryAccruedYearEndStatus varchar(MAX) = '',
	@QueryAccruedBookingNo varchar(MAX) = '',
	@QuerySettlementStatus varchar(MAX) = '',
	@QueryDescriptionTransaction varchar(MAX) = '',
	@QueryGLAccount varchar(MAX) = '',
	@QueryVendorName varchar(MAX) = '',
	@QueryNpwp varchar(MAX) = '',
	@QueryInvoiceNo varchar(MAX) = '',
	@QueryInvoiceDate varchar(MAX) = '',
	@QueryBookPeriod varchar(MAX) = '', 
	@QueryTurnoverAmount varchar(MAX) = '',
	@QueryVATAmount varchar(MAX) = '',
	@QueryWHTAmount varchar(MAX) = '',
	@QueryTaxInvoiceNo varchar(MAX) = '',
	@QueryWitholdingTaxNo varchar(MAX) = '',
	@QueryWitholdingTaxDate varchar(MAX) = '',
	@QueryWHTArticle varchar(MAX) = '',
	@QuerySystem varchar(MAX) = '',
	@QueryPVNo varchar(MAX) = '',
	@QueryPVCreatedBy varchar(MAX) = '',
	@QuerySAPGRDocNo varchar(MAX) = '',
	@QuerySAPGRPostingDate varchar(MAX) = '',
	@QuerySAPInvoiceDocNo varchar(MAX) = '',
	@QuerySAPInvoicePostingDate varchar(MAX) = '',
	@QueryReceiveFileDate varchar(MAX) = '',
	@QueryDownloadedDate varchar(MAX) = '',
	@QueryDownloadStatus varchar(MAX) = '',
	@QueryNominativeFormAttachment varchar(MAX) = '',
	@QueryNominativeIDNo varchar(MAX) = '',
	@QueryNominativeStatus varchar(MAX) = '',
	@QueryNominativeDate varchar(MAX) = ''

SELECT 
@QueryCategoryCode = dbo.uf_LookupDynamicQueryGenerator(@CategoryCode, 'c.CategoryCode'), 
@QueryStatusInvoice = dbo.uf_LookupDynamicQueryGenerator(@StatusInvoice, 'c.StatusInvoice'),
@QueryPVType = dbo.uf_LookupDynamicQueryGenerator(@PVType, 'c.PVType'),
@QueryAccruedYearEndStatus = dbo.uf_LookupDynamicQueryGenerator(@AccruedYearEndStatus, 'c.AccruedYearEndStatus'),
@QueryAccruedBookingNo = dbo.uf_LookupDynamicQueryGenerator(@AccruedBookingNo, 'c.AccruedBookingNo'),
--@QuerySettlementStatus = dbo.uf_LookupDynamicQueryGenerator(@PVType, 'c.SettlementStatus'),
@QuerySettlementStatus = dbo.uf_LookupDynamicQueryGenerator(@SettlementStatus, 'c.SettlementStatus'),
@QueryDescriptionTransaction = dbo.uf_LookupDynamicQueryGenerator(@DescriptionTransaction, 'c.DescriptionTransaction'), 
@QueryGLAccount = dbo.uf_LookupDynamicQueryGenerator(@GLAccount, 'c.GLAccount'),
@QueryVendorName = dbo.uf_LookupDynamicQueryGenerator(@VendorName, 'c.VendorName'),
@QueryNpwp = dbo.uf_LookupDynamicQueryGenerator(@Npwp, 'c.Npwp'),
@QueryInvoiceNo = dbo.uf_LookupDynamicQueryGenerator(@InvoiceNo, 'c.InvoiceNo'),
@QueryInvoiceDate = dbo.uf_DateRangeDynamicQueryGenerator(@InvoiceDateFrom + ' ' + '0:00 AM', @InvoiceDateTo + ' ' + '11:59 PM', 'c.InvoiceDate'),
@QueryBookPeriod = dbo.uf_DateRangeDynamicQueryGenerator(@BookPeriodFrom + ' ' + '0:00 AM', @BookPeriodTo + ' ' + '11:59 PM', 'c.BookPeriod'), 
@QueryTurnoverAmount = dbo.uf_DecimalRangeDynamicQueryGenerator(@TurnoverAmountOperator, @TurnoverAmount, 'c.TurnoverAmount'),
@QueryVATAmount = dbo.uf_DecimalRangeDynamicQueryGenerator(@VATAmountOperator, @VATAmount, 'c.VATAmount'),
@QueryWHTAmount = dbo.uf_DecimalRangeDynamicQueryGenerator(@WHTAmountOperator, @WHTAmount, 'c.WHTAmount'),
@QueryTaxInvoiceNo = dbo.uf_LookupDynamicQueryGenerator(@TaxInvoiceNo, 'c.TaxInvoiceNo'),
@QueryWitholdingTaxNo = dbo.uf_LookupDynamicQueryGenerator(@WitholdingTaxNo, 'c.WitholdingTaxNo'), 
@QueryWitholdingTaxDate = dbo.uf_DateRangeDynamicQueryGenerator(@WitholdingTaxDateFrom + ' ' + '0:00 AM', @WitholdingTaxDateTo + ' ' + '11:59 PM', 'c.WitholdingTaxDate'),
@QueryWHTArticle = dbo.uf_LookupDynamicQueryGenerator(@WHTArticle, 'c.WHTArticle'),
@QuerySystem = dbo.uf_LookupDynamicQueryGenerator(@System, 'c.System'),
@QueryPVNo = dbo.uf_LookupDynamicQueryGenerator(@PVNo, 'c.PVNo'),
@QueryPVCreatedBy = dbo.uf_LookupDynamicQueryGenerator(@PVCreatedBy, 'c.CreatedBy'),
@QuerySAPGRDocNo = dbo.uf_LookupDynamicQueryGenerator(@SAPGRDocNo, 'c.SAPGRDocNo'),
@QuerySAPGRPostingDate = dbo.uf_DateRangeDynamicQueryGenerator(@SAPGRPostingDateFrom + ' ' + '0:00 AM', @SAPGRPostingDateTo + ' ' + '11:59 PM', 'c.SAPGRPostingDate'),
@QuerySAPInvoiceDocNo = dbo.uf_LookupDynamicQueryGenerator(@SAPInvoiceDocNo, 'c.SAPInvoiceDocNo'),
@QuerySAPInvoicePostingDate = dbo.uf_DateRangeDynamicQueryGenerator(@SAPInvoicePostingDateFrom + ' ' + '0:00 AM', @SAPInvoicePostingDateTo + ' ' + '11:59 PM', 'c.SAPInvoicePostingDate'),
@QueryReceiveFileDate = dbo.uf_DateRangeDynamicQueryGenerator(@ReceiveFileDateFrom + ' ' + '0:00 AM', @ReceiveFileDateTo + ' ' + '11:59 PM', 'c.ReceiveFileDate'),
@QueryDownloadedDate = dbo.uf_DateRangeDynamicQueryGenerator(@DownloadedDateFrom + ' ' + '0:00 AM', @DownloadedDateTo + ' ' + '11:59 PM', 'c.DownloadedDate'), 
@QueryDownloadStatus = dbo.uf_LookupDynamicQueryGenerator(@DownloadStatus, 'c.DownloadStatus'),
--@QueryNominativeFormAttachment = dbo.uf_LookupDynamicQueryGenerator(@NominativeFormAttachment, 'c.NominativeFormAttachment'),
@QueryNominativeIDNo = dbo.uf_LookupDynamicQueryGenerator(@NominativeIDNo, 'c.NominativeIDNo'),
@QueryNominativeStatus = dbo.uf_LookupDynamicQueryGenerator(@NominativeStatus, 'c.NominativeStatus'),
@QueryNominativeDate = dbo.uf_DateRangeDynamicQueryGenerator(@NominativeDateFrom + ' ' + '0:00 AM', @NominativeDateTo + ' ' + '11:59 PM', 'c.NominativeDate')


SET @Query =  @Query + 
		@QueryCategoryCode + 
		@QueryStatusInvoice +
		@QueryPVType +
		@QueryAccruedYearEndStatus +
		@QueryAccruedBookingNo +
		@QuerySettlementStatus +
		@QueryDescriptionTransaction +
		@QueryGLAccount +
		@QueryVendorName +
		@QueryNpwp +
		@QueryInvoiceNo +
		@QueryInvoiceDate +
		@QueryBookPeriod +
		@QueryTurnoverAmount +
		@QueryVATAmount +
		@QueryWHTAmount +
		@QueryTaxInvoiceNo +
		@QueryWitholdingTaxNo +
		@QueryWitholdingTaxDate +
		@QueryWHTArticle +
		@QuerySystem +
		@QueryPVNo +
		@QueryPVCreatedBy +
		@QuerySAPGRDocNo +
		@QuerySAPGRPostingDate +
		@QuerySAPInvoiceDocNo +
		@QuerySAPInvoicePostingDate +
		@QueryReceiveFileDate +
		@QueryDownloadedDate +
		@QueryDownloadStatus +
		@QueryNominativeFormAttachment +
		@QueryNominativeIDNo +
		@QueryNominativeStatus +
		@QueryNominativeDate
EXEC(@Query)
END

GO
