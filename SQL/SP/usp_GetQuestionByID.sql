
---$ Alter Procedure [dbo].[usp_GetQuestionByID] 
IF OBJECT_ID(N'[dbo].[usp_GetQuestionByID]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_GetQuestionByID]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_GetQuestionByID] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_GetQuestionByID]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

ALTER PROCEDURE usp_GetQuestionByID

	-- Add the parameters for the stored procedure here
	@QuestionId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select QuestionId,a.CategoryCode,b.NominativeType as CategoryName, Question,IsYes,WarningStatusYes,IsNo,WarningStatusNo,a.Createdby,a.createddate From TB_M_Question_ENUM as a
	inner join TB_M_CategoryNominative_ENUM as b on a.CategoryCode=b.CategoryCode
	Where QuestionId = @QuestionId
END

GO
