---$ Alter Procedure [dbo].[usp_GetKategoriCodeDDList_ENUM] 
IF OBJECT_ID(N'[dbo].[usp_GetKategoriCodeDDList_ENUM]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_GetKategoriCodeDDList_ENUM]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_GetKategoriCodeDDList_ENUM] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_GetKategoriCodeDDList_ENUM]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go


ALTER PROCEDURE [dbo].[usp_GetKategoriCodeDDList_ENUM]
AS
BEGIN
	
	SELECT 
		CategoryCode AS	Id,
		NominativeType AS Name
	FROM TB_M_CategoryNominative_ENUM 

END

GO
