---$ Alter Procedure [dbo].[usp_GetDashboard] 
IF OBJECT_ID(N'[dbo].[usp_GetDashboard]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_GetDashboard]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_GetDashboard] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_GetDashboard]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go




ALTER PROCEDURE [dbo].[usp_GetDashboard]
	@CategoryCode VARCHAR(MAX) = '', 
    @StatusInvoice VARCHAR(MAX) = '',
    @PVType VARCHAR(MAX) = '',
	@AccruedYearEndStatus VARCHAR(MAX) = '',
	@AccruedBookingNo VARCHAR(MAX) = '',
	@SettlementStatus VARCHAR(MAX) = '',
    @DescriptionTransaction VARCHAR(MAX) = '',
    @GLAccount VARCHAR(MAX) = '',
    @VendorName VARCHAR(200) = '',
    @Npwp VARCHAR(30) = '',
    @InvoiceNo VARCHAR(30) = '',
    @InvoiceDateFrom VARCHAR(50) = '',
    @InvoiceDateTo VARCHAR(50) = '',
    @BookPeriodFrom VARCHAR(50) = '',
    @BookPeriodTo VARCHAR(50) = '',
	@TurnoverAmountOperator VARCHAR(50) = '',
    @TurnoverAmount DECIMAL(18,0),
    @VATAmountOperator VARCHAR(50) = '',
    @VATAmount DECIMAL(18,0),
    @WHTAmountOperator VARCHAR(50) = '',
    @WHTAmount  DECIMAL(18,0), 
    @TaxInvoiceNo VARCHAR(30) ='',
    @WitholdingTaxNo VARCHAR(30) = '',
    @WitholdingTaxDateFrom VARCHAR(50) = '',
    @WitholdingTaxDateTo VARCHAR(50) = '',
    @WHTArticle VARCHAR(MAX) = '',
    @System VARCHAR(MAX) = '',
    @PVNo VARCHAR(30) = '',
    @PVCreatedBy VARCHAR(30) = '',
    @SAPGRDocNo VARCHAR(MAX) = '',
    @SAPGRPostingDateFrom VARCHAR(50) = '',
    @SAPGRPostingDateTo VARCHAR(50) = '',
    @SAPInvoiceDocNo VARCHAR(MAX) = '',
    @SAPInvoicePostingDateFrom VARCHAR(50) = '',
    @SAPInvoicePostingDateTo VARCHAR(50) = '',
    @ReceiveFileDateFrom VARCHAR(50) = '',
    @ReceiveFileDateTo VARCHAR(50) = '',
    @DownloadedDateFrom VARCHAR(50) = '',
    @DownloadedDateTo VARCHAR(50) = '',
    @DownloadStatus VARCHAR(MAX) = '',
    @NominativeFormAttachment VARCHAR(MAX) = '',
    @NominativeIDNo VARCHAR(30) = '',
    @NominativeStatus VARCHAR(MAX) = '',
    @NominativeDateFrom VARCHAR(50) = '',
    @NominativeDateTo VARCHAR(50) = '',
	@SortBy int = 1,
	@SortDirection varchar(4) = 'DESC', 
	@FromNumber int,
	@ToNumber int
	AS
BEGIN
	DECLARE @SortField varchar(50)
	SET @SortField = 
	CASE @SortBy 
	When 1 Then 'A.createdon'
	WHEN 2 THEN 'B.NominativeType'
	WHEN 3 THEN 'A.StatusInvoice'
	WHEN 4 THEN 'A.PVType'
	WHEN 5 THEN 'A.AccruedYearEndStatus'
	WHEN 6 THEN 'A.AccruedBookingNo'
	--WHEN 6 THEN 'A.SettlementStatus'
	WHEN 7 THEN 'A.DescriptionTransaction'
	WHEN 8 THEN 'A.GLAccount'
	WHEN 9 THEN 'A.VendorName'
	WHEN 10 THEN 'A.Npwp'
	WHEN 11 THEN 'A.InvoiceNo'
	WHEN 12 THEN 'A.InvoiceDate'
	WHEN 13 THEN 'A.BookPeriod'
	WHEN 14 THEN 'A.TurnoverAmount'
	WHEN 15 THEN 'A.VATAmount'
	WHEN 16 THEN 'A.WHTAmount'
	WHEN 17 THEN 'A.TaxInvoiceNo'
	WHEN 18 THEN 'A.WitholdingTaxNo'
	WHEN 19 THEN 'A.WitholdingTaxDate'
	WHEN 20 THEN 'A.WHTArticle'
	WHEN 21 THEN 'A.System'
	WHEN 22 THEN 'A.PVNo'
	WHEN 23 THEN 'A.CreatedBy'
	--WHEN 24 THEN 'A.SAPGRDocNo'
	WHEN 24 THEN 'CONVERT(int ,SAPGRDocNo)'
	
	WHEN 25 THEN 'A.SAPGRPostingDate'
	--WHEN 26 THEN 'A.SAPInvoiceDocNo'
	WHEN 26 THEN 'CONVERT(int ,SAPInvoiceDocNo)'
	
	WHEN 27 THEN 'A.SAPInvoicePostingDate'
	WHEN 28 THEN 'A.ReceiveFileDate'
	WHEN 29 THEN 'A.DownloadedDate'
	WHEN 30 THEN 'A.DownloadStatus'
	WHEN 31 THEN 'A.NominativeFormAttachment'
	WHEN 32 THEN 'A.NominativeIDNo'
	WHEN 33 THEN 'A.NominativeStatus'
	WHEN 34 THEN 'A.NominativeDate'
	
	ELSE 'A.NominativeType' end; 

	DECLARE 
	@Query VARCHAR(MAX) = 'SELECT 
		ROW_NUMBER() OVER (ORDER BY '+ @SortField + ' ' + @SortDirection + ') AS RowNum,
		A.Id,
		A.CategoryCode,
		A.StatusInvoice,
		A.PVType,
		A.SettlementStatus,
		A.AccruedYearEndStatus,
		A.AccruedBookingNo,
		A.DescriptionTransaction,
		A.GLAccount,
		A.VendorName,
		A.NPWP,
		A.InvoiceNo,
		A.InvoiceDate,
		A.BookPeriod,
		A.TurnoverAmount,
		A.VATAmount,
		A.WHTAmount,
		A.TaxInvoiceNo,
		A.WitholdingTaxNo,
		A.WHTArticle,
		A.WitholdingTaxDate,
		A.System,
		A.PVNo,
		A.PVCreatedBy,
		A.SAPGRDocNo,
		A.SAPGRPostingDate,
		A.SAPInvoiceDocNo,
		A.SAPInvoicePostingDate,
		A.ReceiveFileDate,
		A.DownloadedDate,
		A.DownloadStatus,
		CASE WHEN ISNULL(A.NominativeFormAttachment,'''')='''' THEN ''N/A'' ELSE A.NominativeFormAttachment END AS NominativeFormAttachment,
		A.NominativeIDNo,
		A.NominativeStatus,
		A.NominativeDate,
		A.CreatedBy,
		A.CreatedOn,
		A.ModifiedBy,
		A.ModifiedOn,
		A.RowStatus,
		B.NominativeType
	FROM dbo.TB_R_Dashboard_Nominative A
	INNER JOIN TB_M_CategoryNominative_ENUM B ON A.CategoryCode=B.CategoryCode 
	WHERE CASE WHEN '''+ISNULL(@NominativeFormAttachment,'')+'''=''Yes'' THEN CASE WHEN (CASE WHEN ISNULL(A.NominativeFormAttachment,'''')='''' THEN ''N/A'' ELSE A.NominativeFormAttachment END) <> ''N/A'' THEN 1 ELSE 0 END
		  WHEN '''+ISNULL(@NominativeFormAttachment,'')+'''=''N/A'' THEN CASE WHEN (CASE WHEN ISNULL(A.NominativeFormAttachment,'''')='''' THEN ''N/A'' ELSE A.NominativeFormAttachment END) = ''N/A'' THEN 1 ELSE 0 END
		  ELSE 1 END = 1 ',
	@QueryCategoryCode varchar(MAX) = '', 
	@QueryStatusInvoice varchar(MAX) = '',
	@QueryPVType varchar(MAX) = '',
	@QueryAccruedYearEndStatus varchar(MAX) = '',
	@QueryAccruedBookingNo varchar(MAX) = '',
	@QuerySettlementStatus varchar(MAX) = '',
	@QueryDescriptionTransaction varchar(MAX) = '',
	@QueryGLAccount varchar(MAX) = '',
	@QueryVendorName varchar(MAX) = '',
	@QueryNpwp varchar(MAX) = '',
	@QueryInvoiceNo varchar(MAX) = '',
	@QueryInvoiceDate varchar(MAX) = '',
	@QueryBookPeriod varchar(MAX) = '', 
	@QueryTurnoverAmount varchar(MAX) = '',
	@QueryVATAmount varchar(MAX) = '',
	@QueryWHTAmount varchar(MAX) = '',
	@QueryTaxInvoiceNo varchar(MAX) = '',
	@QueryWitholdingTaxNo varchar(MAX) = '',
	@QueryWitholdingTaxDate varchar(MAX) = '',
	@QueryWHTArticle varchar(MAX) = '',
	@QuerySystem varchar(MAX) = '',
	@QueryPVNo varchar(MAX) = '',
	@QueryPVCreatedBy varchar(MAX) = '',
	@QuerySAPGRDocNo varchar(MAX) = '',
	@QuerySAPGRPostingDate varchar(MAX) = '',
	@QuerySAPInvoiceDocNo varchar(MAX) = '',
	@QuerySAPInvoicePostingDate varchar(MAX) = '',
	@QueryReceiveFileDate varchar(MAX) = '',
	@QueryDownloadedDate varchar(MAX) = '',
	@QueryDownloadStatus varchar(MAX) = '',
	@QueryNominativeFormAttachment varchar(MAX) = '',
	@QueryNominativeIDNo varchar(MAX) = '',
	@QueryNominativeStatus varchar(MAX) = '',
	@QueryNominativeDate varchar(MAX) = ''

SELECT 
@QueryCategoryCode = dbo.uf_LookupDynamicQueryGenerator(@CategoryCode, 'A.CategoryCode'), 
@QueryStatusInvoice = dbo.uf_LookupDynamicQueryGenerator(@StatusInvoice, 'A.StatusInvoice'),
@QueryPVType = dbo.uf_LookupDynamicQueryGenerator(@PVType, 'A.PVType'),
@QueryAccruedYearEndStatus = dbo.uf_LookupDynamicQueryGenerator(@AccruedYearEndStatus, 'A.AccruedYearEndStatus'),
@QueryAccruedBookingNo = dbo.uf_LookupDynamicQueryGenerator(@AccruedBookingNo, 'A.AccruedBookingNo'),
--@QuerySettlementStatus = dbo.uf_LookupDynamicQueryGenerator(@PVType, 'A.SettlementStatus'),
@QuerySettlementStatus = dbo.uf_LookupDynamicQueryGenerator(@SettlementStatus, 'A.SettlementStatus'),
@QueryDescriptionTransaction = dbo.uf_LookupDynamicQueryGenerator(@DescriptionTransaction, 'A.DescriptionTransaction'), 
@QueryGLAccount = dbo.uf_LookupDynamicQueryGenerator(@GLAccount, 'A.GLAccount'),
@QueryVendorName = dbo.uf_LookupDynamicQueryGenerator(@VendorName, 'A.VendorName'),
@QueryNpwp = dbo.uf_LookupDynamicQueryGenerator(@Npwp, 'A.Npwp'),
@QueryInvoiceNo = dbo.uf_LookupDynamicQueryGenerator(@InvoiceNo, 'A.InvoiceNo'),
@QueryInvoiceDate = dbo.uf_DateRangeDynamicQueryGenerator(@InvoiceDateFrom + ' ' + '0:00 AM', @InvoiceDateTo + ' ' + '11:59 PM', 'A.InvoiceDate'),
@QueryBookPeriod = dbo.uf_DateRangeDynamicQueryGenerator(@BookPeriodFrom + ' ' + '0:00 AM', @BookPeriodTo + ' ' + '11:59 PM', 'A.BookPeriod'), 
@QueryTurnoverAmount = dbo.uf_DecimalRangeDynamicQueryGenerator(@TurnoverAmountOperator, @TurnoverAmount, 'A.TurnoverAmount'),
@QueryVATAmount = dbo.uf_DecimalRangeDynamicQueryGenerator(@VATAmountOperator, @VATAmount, 'A.VATAmount'),
@QueryWHTAmount = dbo.uf_DecimalRangeDynamicQueryGenerator(@WHTAmountOperator, @WHTAmount, 'A.WHTAmount'),
@QueryTaxInvoiceNo = dbo.uf_LookupDynamicQueryGenerator(@TaxInvoiceNo, 'A.TaxInvoiceNo'),
@QueryWitholdingTaxNo = dbo.uf_LookupDynamicQueryGenerator(@WitholdingTaxNo, 'A.WitholdingTaxNo'), 
@QueryWitholdingTaxDate = dbo.uf_DateRangeDynamicQueryGenerator(@WitholdingTaxDateFrom + ' ' + '0:00 AM', @WitholdingTaxDateTo + ' ' + '11:59 PM', 'A.WitholdingTaxDate'),
@QueryWHTArticle = dbo.uf_LookupDynamicQueryGenerator(@WHTArticle, 'A.WHTArticle'),
@QuerySystem = dbo.uf_LookupDynamicQueryGenerator(@System, 'A.System'),
@QueryPVNo = dbo.uf_LookupDynamicQueryGenerator(@PVNo, 'A.PVNo'),
@QueryPVCreatedBy = dbo.uf_LookupDynamicQueryGenerator(@PVCreatedBy, 'A.CreatedBy'),
@QuerySAPGRDocNo = dbo.uf_LookupDynamicQueryGenerator(@SAPGRDocNo, 'A.SAPGRDocNo'),
@QuerySAPGRPostingDate = dbo.uf_DateRangeDynamicQueryGenerator(@SAPGRPostingDateFrom + ' ' + '0:00 AM', @SAPGRPostingDateTo + ' ' + '11:59 PM', 'A.SAPGRPostingDate'),
@QuerySAPInvoiceDocNo = dbo.uf_LookupDynamicQueryGenerator(@SAPInvoiceDocNo, 'A.SAPInvoiceDocNo'),
@QuerySAPInvoicePostingDate = dbo.uf_DateRangeDynamicQueryGenerator(@SAPInvoicePostingDateFrom + ' ' + '0:00 AM', @SAPInvoicePostingDateTo + ' ' + '11:59 PM', 'A.SAPInvoicePostingDate'),
@QueryReceiveFileDate = dbo.uf_DateRangeDynamicQueryGenerator(@ReceiveFileDateFrom + ' ' + '0:00 AM', @ReceiveFileDateTo + ' ' + '11:59 PM', 'A.ReceiveFileDate'),
@QueryDownloadedDate = dbo.uf_DateRangeDynamicQueryGenerator(@DownloadedDateFrom + ' ' + '0:00 AM', @DownloadedDateTo + ' ' + '11:59 PM', 'A.DownloadedDate'), 
@QueryDownloadStatus = dbo.uf_LookupDynamicQueryGenerator(@DownloadStatus, 'A.DownloadStatus'),
--@QueryNominativeFormAttachment = dbo.uf_LookupDynamicQueryGenerator(@NominativeFormAttachment, 'A.NominativeFormAttachment'),
@QueryNominativeIDNo = dbo.uf_LookupDynamicQueryGenerator(@NominativeIDNo, 'A.NominativeIDNo'),
@QueryNominativeStatus = dbo.uf_LookupDynamicQueryGenerator(@NominativeStatus, 'A.NominativeStatus'),
@QueryNominativeDate = dbo.uf_DateRangeDynamicQueryGenerator(@NominativeDateFrom + ' ' + '0:00 AM', @NominativeDateTo + ' ' + '11:59 PM', 'A.NominativeDate')

SET @Query = 'SELECT * FROM ('+ 
		@Query + 
		@QueryCategoryCode + 
		@QueryStatusInvoice +
		@QueryPVType +
		@QueryAccruedYearEndStatus +
		@QueryAccruedBookingNo +
		@QuerySettlementStatus +
		@QueryDescriptionTransaction +
		@QueryGLAccount +
		@QueryVendorName +
		@QueryNpwp +
		@QueryInvoiceNo +
		@QueryInvoiceDate +
		@QueryBookPeriod +
		@QueryTurnoverAmount +
		@QueryVATAmount +
		@QueryWHTAmount +
		@QueryTaxInvoiceNo +
		@QueryWitholdingTaxNo +
		@QueryWitholdingTaxDate +
		@QueryWHTArticle +
		@QuerySystem +
		@QueryPVNo +
		@QueryPVCreatedBy +
		@QuerySAPGRDocNo +
		@QuerySAPGRPostingDate +
		@QuerySAPInvoiceDocNo +
		@QuerySAPInvoicePostingDate +
		@QueryReceiveFileDate +
		@QueryDownloadedDate +
		@QueryDownloadStatus +
		@QueryNominativeFormAttachment +
		@QueryNominativeIDNo +
		@QueryNominativeStatus +
		@QueryNominativeDate +
		') AS TBL WHERE RowNum BETWEEN ' +
		CONVERT(VARCHAR, @FromNumber) + ' AND ' + CONVERT(VARCHAR, @ToNumber)
		
EXEC(@Query)
--Select @Query
END

GO