---$ Alter Procedure [dbo].[usp_Validation_TB_R_Dashboard_Nominative] 
IF OBJECT_ID(N'[dbo].[usp_Validation_TB_R_Dashboard_Nominative]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_Validation_TB_R_Dashboard_Nominative]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_Validation_TB_R_Dashboard_Nominative] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_Validation_TB_R_Dashboard_Nominative]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

ALTER PROCEDURE [dbo].[usp_Validation_TB_R_Dashboard_Nominative]
    @NominativeType VARCHAR(30) ,
    @StatusInvoice VARCHAR(20) ,
    @PVType VARCHAR(30) ,
    @AccruedYearEndStatus VARCHAR(30) ,
    --@AccruedBookingNo VARCHAR(30) ,
    --@SettlementStatus VARCHAR(30) ,
    @DescriptionTransaction VARCHAR(100) ,
    @GLAccount VARCHAR(20) ,
    --@VendorName VARCHAR(200) ,
    --@Npwp VARCHAR(30) ,
    --@InvoiceNo VARCHAR(30) ,
    --@InvoiceDate DATETIME ,
    --@BookPeriod DATETIME ,
    --@TurnoverAmount DECIMAL(18, 0) ,
    --@VATAmount DECIMAL(18, 0) ,
    --@WHTAmount DECIMAL(18, 0) ,
    @TaxInvoiceNo VARCHAR(30) = '' ,
    --@WitholdingTaxNo VARCHAR(30) ,
    --@WitholdingTaxDate DATETIME ,
    @WHTArticle VARCHAR(30) ,
    --@System VARCHAR(30),
    --@PVNo VARCHAR(30) ,
    --@PVCreatedBy VARCHAR(30) ,
    --@SAPGRDocNo VARCHAR(30) ,
    --@SAPGRPostingDate DATETIME ,
    --@SAPInvoiceDocNo VARCHAR(30) ,
    --@SAPInvoicePostingDate DATETIME ,
    --@ReceiveFileDate DATETIME ,
    --@DownloadedDate DATETIME ,
    --@DownloadStatus VARCHAR(30) ,
    --@NominativeFormAttachment VARCHAR(MAX) ,
    --@NominativeIDNo VARCHAR(30) ,
    @NominativeStatus VARCHAR(30) 
    --@NominativeDate DATETIME ,
    --@CreatedOn DATETIME ,
    --@CreatedBy VARCHAR(100) ,
    --@RowStatus BIT
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @ResultCode BIT = 1 ,
            @ResultDesc VARCHAR(MAX) = ''

        IF NOT EXISTS ( SELECT TOP 1
                                *
                        FROM    TB_M_CategoryNominative_ENUM
                        WHERE   NominativeType = @NominativeType )
            BEGIN
                SELECT  @ResultCode = 0 ,
                        @ResultDesc = 'Nominative Type ''' + @NominativeType
                        + ''' not registered'
            END

        IF NOT EXISTS ( SELECT TOP 1
                                *
                        FROM    ELVIS_DB.dbo.TB_M_TRANSACTION_TYPE
                        WHERE   TRANSACTION_NAME = @DescriptionTransaction )
            BEGIN
                SELECT  @ResultCode = 0 ,
                        @ResultDesc += CASE WHEN @ResultDesc <> '' THEN ', '
                                            ELSE ''
                                       END + 'Description Transaction '''
                        + @DescriptionTransaction + ''' not registered'
            END

        IF ( ISNULL(@WHTArticle, '') != '' )
            BEGIN

                IF NOT EXISTS ( SELECT TOP 1
                                        *
                                FROM    dbo.TB_M_JenisPajak
                                WHERE   Article = @WHTArticle )
                    BEGIN
                        SELECT  @ResultCode = 0 ,
                                @ResultDesc += CASE WHEN @ResultDesc <> ''
                                                    THEN ', '
                                                    ELSE ''
                                               END + ' WHT Article '''
                                + @WHTArticle + ''' not registered'
                    END

            END

        IF NOT EXISTS ( SELECT TOP 1
                                *
                        FROM    TB_M_GeneralParam
                        WHERE   ParamValue = @StatusInvoice
                                AND ParamType = 'StatusInvoice' )
            BEGIN
                SELECT  @ResultCode = 0 ,
                        @ResultDesc += CASE WHEN @ResultDesc <> '' THEN ', '
                                            ELSE ''
                                       END + ' Invoice Status '''
                        + @StatusInvoice + ''' not registered'
            END

        IF NOT EXISTS ( SELECT TOP 1
                                *
                        FROM    TB_M_GeneralParam
                        WHERE   ParamValue = @PVType
                                AND ParamType = 'PVType' )
            BEGIN
                SELECT  @ResultCode = 0 ,
                        @ResultDesc += CASE WHEN @ResultDesc <> '' THEN ', '
                                            ELSE ''
                                       END + 'PV Type ''' + @PVType
                        + ''' not registered'
            END

        IF NOT EXISTS ( SELECT TOP 1
                                *
                        FROM    TB_M_GeneralParam
                        WHERE   ParamValue = @AccruedYearEndStatus
                                AND ParamType = 'AccruedYearEndStatus' )
            BEGIN
                SELECT  @ResultCode = 0 ,
                        @ResultDesc += CASE WHEN @ResultDesc <> '' THEN ', '
                                            ELSE ''
                                       END + 'Accrued Year End Status '''
                        + @AccruedYearEndStatus + ''' not registered'
            END

        --IF NOT EXISTS ( SELECT TOP 1
        --                        *
        --                FROM    TB_M_GeneralParam
        --                WHERE   ParamValue = @DownloadStatus
        --                        AND ParamType = 'DownloadStatus' )
        --    BEGIN
        --        SELECT  @ResultCode = 0 ,
        --                @ResultDesc += CASE WHEN @ResultDesc <> '' THEN ', '
        --                                    ELSE ''
        --                               END + 'Download Status '''
        --                + @DownloadStatus + ''' not registered'
        --    END

        --IF NOT EXISTS ( SELECT TOP 1
        --                        *
        --                FROM    TB_M_GeneralParam
        --                WHERE   ParamValue = @NominativeFormAttachment
        --                        AND ParamType = 'NominativeFormAttachment' )
        --    BEGIN
        --        SELECT  @ResultCode = 0 ,
        --                @ResultDesc += CASE WHEN @ResultDesc <> '' THEN ', '
        --                                    ELSE ''
        --                               END + 'Nominative Form Attachment '''
        --                + @NominativeFormAttachment + ''' not registered'
        --    END

        IF NOT EXISTS ( SELECT TOP 1
                                *
                        FROM    TB_M_GeneralParam
                        WHERE   ParamValue = @NominativeStatus
                                AND ParamType = 'NominativeStatus' )
            BEGIN
                SELECT  @ResultCode = 0 ,
                        @ResultDesc += CASE WHEN @ResultDesc <> '' THEN ', '
                                            ELSE ''
                                       END + 'Nominative Status '''
                        + @NominativeStatus + ''' not registered'
            END

        IF NOT EXISTS ( SELECT TOP 1
                                a.*
                        FROM    TB_M_Nominative_ENUM a
                                INNER JOIN TB_M_CategoryNominative_ENUM b ON a.CategoryCode = b.CategoryCode
                        WHERE   GLAccount = @GLAccount
                                AND b.NominativeType = @NominativeType )
            BEGIN
                SELECT  @ResultCode = 0 ,
                        @ResultDesc += CASE WHEN @ResultDesc <> '' THEN ', '
                                            ELSE ''
                                       END + 'GL Account ''' + @GLAccount
                        + ''' for Nominative Type ''' + @NominativeType
                        + ''' not registered'
            END

		--IF NOT EXISTS ( SELECT TOP 1 * FROM ELVIS_DB.dbo.vw_Vendor A WHERE REPLACE(REPLACE(REPLACE(A.VENDOR_NAME,' ',''),'.',''),',','') = REPLACE(REPLACE(REPLACE(@VendorName,' ',''),'.',''),',','') )
  --          BEGIN
  --              SELECT  @ResultCode = 0, @ResultDesc += CASE WHEN @ResultDesc<>'' THEN ', ' ELSE '' END +'Vendor Name ''' +  @VendorName + ''' for Nominative Type '''+ @NominativeType + '''not registered'
  --          END

        IF ( ISNULL(@TaxInvoiceNo, '') != 'N/A' )
            BEGIN
                IF EXISTS ( SELECT  1
                            FROM    TB_R_Dashboard_Nominative
                            WHERE   TaxInvoiceNo = @TaxInvoiceNo )
                    BEGIN
                        SELECT  @ResultCode = 0 ,
                                @ResultDesc += CASE WHEN @ResultDesc <> ''
                                                    THEN ', '
                                                    ELSE ''
                                               END + 'Tax Invoice No'''
                                + @TaxInvoiceNo + ''' Already Exists'
                    END
            END

        IF ( @ResultDesc = '' )
            BEGIN
                SELECT  @ResultDesc = 'File has been uploaded successfully'
            END
        
        
        SELECT  @ResultCode AS ResultCode ,
                @ResultDesc AS ResultDesc
    END

GO
