---$ Alter Procedure [dbo].[usp_GetEntertainmentByID] 
IF OBJECT_ID(N'[dbo].[usp_GetEntertainmentByID]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_GetEntertainmentByID]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_GetEntertainmentByID] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_GetEntertainmentByID]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

ALTER PROCEDURE usp_GetEntertainmentByID

	-- Add the parameters for the stored procedure here
	@ID UNIQUEIDENTIFIER
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT A.*
	FROM dbo.TB_R_Entertainment A
	WHERE A.ENTERTAINMENT_ID=@ID 
END

GO
