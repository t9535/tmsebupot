---$ Alter Procedure [dbo].[usp_DropDownTransactionType] 
IF OBJECT_ID(N'[dbo].[usp_DropDownTransactionType]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_DropDownTransactionType]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_DropDownTransactionType] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_DropDownTransactionType]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

ALTER PROCEDURE [dbo].[usp_DropDownTransactionType]

	-- Add the parameters for the stored procedure here
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
        SELECT  CONVERT(VARCHAR(5), ROW_NUMBER() OVER ( ORDER BY Name ASC )) AS Id ,
                Name
        FROM    ( SELECT DISTINCT
                            DescriptionTransaction Name
                  FROM      dbo.TB_R_Dashboard_Nominative AS trdn
                ) a
	
    END

GO