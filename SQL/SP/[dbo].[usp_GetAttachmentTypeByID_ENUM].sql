---$ Alter Procedure [dbo].[usp_GetAttachmentTypeByID_ENUM] 
IF OBJECT_ID(N'[dbo].[usp_GetAttachmentTypeByID_ENUM]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_GetAttachmentTypeByID_ENUM]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_GetAttachmentTypeByID_ENUM] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_GetAttachmentTypeByID_ENUM]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[usp_GetAttachmentTypeByID_ENUM]

	-- Add the parameters for the stored procedure here
	@AttachmentId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT AttachmentId,
	AttachmentId as KodeID,
	CategoryCode, 
	CategoryDesc,
	Description,
	FileType,
	FileTypeDesc,
	Vendor_Group,
	Vendor_GroupDesc,
	Ebiling,
	Elvis,
	CreatedDate, 
	CreatedBy, 
	ModifiedDate, 
	ModifiedBy
	FROM TB_M_AttachmentType_ENUM
	WHERE [AttachmentId] = @AttachmentId


END

GO