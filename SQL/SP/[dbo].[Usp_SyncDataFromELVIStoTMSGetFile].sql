---$ Alter Procedure [dbo].[Usp_SyncDataFromELVIStoTMSGetFile] 
IF OBJECT_ID(N'[dbo].[Usp_SyncDataFromELVIStoTMSGetFile]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[Usp_SyncDataFromELVIStoTMSGetFile]'
    EXECUTE('CREATE PROCEDURE [dbo].[Usp_SyncDataFromELVIStoTMSGetFile] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[Usp_SyncDataFromELVIStoTMSGetFile]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[Usp_SyncDataFromELVIStoTMSGetFile]
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

    -- Insert statements for procedure here
 
        SELECT DISTINCT
                trdn.PVNo
        FROM    dbo.TB_R_Dashboard_Nominative AS trdn
        WHERE   trdn.[System] = 'ELVIS'
                AND ISNULL(trdn.NominativeFormAttachment, '') = '' AND ISNULL(trdn.PVNo,'')<>'' 
    END

GO
