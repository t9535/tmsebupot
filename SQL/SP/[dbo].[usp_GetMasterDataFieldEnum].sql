---$ Alter Procedure [dbo].[usp_GetMasterDataFieldEnum] 
IF OBJECT_ID(N'[dbo].[usp_GetMasterDataFieldEnum]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_GetMasterDataFieldEnum]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_GetMasterDataFieldEnum] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_GetMasterDataFieldEnum]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go



ALTER PROCEDURE [dbo].[usp_GetMasterDataFieldEnum]
	@CategoryCode varchar(150) = '',
	@FieldNameByTable varchar(MAX) = '',
	@FieldNameByExcel varchar(MAX) = '',
	@VendorGroup varchar(500) = '',
	@SortBy int = 1,
	@SortDirection varchar(4) = 'DESC', 
	@FromNumber int,
	@ToNumber int
	AS
BEGIN
	DECLARE @SortField varchar(max)
	SET @SortField = 
	CASE @SortBy 
	WHEN 1 THEN 'A.Modifieddate'
	WHEN 2THEN 'B.NominativeType'
	WHEN 3 THEN 'A.FieldNameByTable'
	WHEN 4 THEN 'A.FieldNameByExcel'
	WHEN 5 THEN 'STUFF((SELECT '', '' + V.VENDOR_GROUP_NAME 
          FROM ELVIS_DB.dbo.TB_M_VENDOR_GROUP V
		  INNER JOIN dbo.splitstring(A.VendorGroup,'';'') S ON V.VENDOR_GROUP_CD = S.Item 
          ORDER BY V.VENDOR_GROUP_NAME
          FOR XML PATH('''')), 1, 1, '''')'
	ELSE 'B.NominativeType' end; 

	DECLARE 
	@Query VARCHAR(MAX) = 'SELECT 
		ROW_NUMBER() OVER (ORDER BY '+ @SortField + ' ' + @SortDirection + ') AS RowNum,
		(A.DataFieldId), 
		(A.CategoryCode), 
		(A.FieldNameByTable), 
		(A.FieldNameByExcel), 
		STUFF((SELECT '', '' + V.VENDOR_GROUP_NAME 
          FROM ELVIS_DB.dbo.TB_M_VENDOR_GROUP V
		  INNER JOIN dbo.splitstring(A.VendorGroup,'';'') S ON V.VENDOR_GROUP_CD = S.Item 
          ORDER BY V.VENDOR_GROUP_NAME
          FOR XML PATH('''')), 1, 1, '''') AS VendorGroup,
		(B.NominativeType)
		
	FROM dbo.TB_M_DataField_ENUM A
	INNER JOIN TB_M_CategoryNominative_ENUM B ON A.CategoryCode=B.CategoryCode 
	WHERE 1=1',
	@QueryCategoryCode varchar(MAX) = '',
	@QueryFieldNameByTable varchar(MAX) = '',
	@QueryFieldNameByExcel varchar(MAX) = '',
	@QueryVendorGroup varchar(MAX) = ''
		
SELECT @QueryCategoryCode = 
dbo.uf_LookupDynamicQueryGenerator(@CategoryCode, 'A.CategoryCode')

SELECT @QueryFieldNameByTable = 
dbo.uf_LookupDynamicQueryGenerator(@FieldNameByTable, 'A.FieldNameByTable')

SELECT @QueryFieldNameByExcel = 
dbo.uf_LookupDynamicQueryGenerator(@FieldNameByExcel, 'A.FieldNameByExcel')

SELECT @QueryVendorGroup = 
dbo.uf_LookupDynamicQueryGenerator(@VendorGroup, 'A.VendorGroup')


SET @Query = 'SELECT * FROM ('+ 
		@Query + 
		@QueryCategoryCode +
		@QueryFieldNameByTable +
		@QueryFieldNameByExcel +
		@QueryVendorGroup+
		') AS TBL WHERE RowNum BETWEEN ' +
		CONVERT(VARCHAR, @FromNumber) + ' AND ' + CONVERT(VARCHAR, @ToNumber)
		
EXEC(@Query)
--Print @Query
END

GO
