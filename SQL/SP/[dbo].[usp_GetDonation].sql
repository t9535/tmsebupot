---$ Alter Procedure [dbo].[usp_GetDonation] 
IF OBJECT_ID(N'[dbo].[usp_GetDonation]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_GetDonation]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_GetDonation] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_GetDonation]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go



ALTER PROCEDURE [dbo].[usp_GetDonation]
	@DashboardId uniqueidentifier,
	@SortBy int = 1,
	@SortDirection varchar(4) = 'DESC', 
	@FromNumber int,
	@ToNumber int
	AS
BEGIN
--SELECT * FROM TB_R_Donation WHERE DASHBOARD_ID=@DashboardId
DECLARE @SortField varchar(50)
	SET @SortField = 
	CASE @SortBy 
	WHEN 1 THEN 'A.JENIS_SUMBANGAN'
	WHEN 2 THEN 'A.BENTUK_SUMBANGAN'
	WHEN 3 THEN 'A.NILAI_SUMBANGAN' 
	WHEN 4 THEN 'A.TANGGAL_SUMBANGAN'
	WHEN 5 THEN 'A.NAMA_LEMBAGA_PENERIMA'
	WHEN 6 THEN 'A.NPWP_LEMBAGA_PENERIMA'
	WHEN 7 THEN 'A.ALAMAT_LEMBAGA'
	WHEN 8 THEN 'A.NO_TELP_LEMBAGA'
	WHEN 9 THEN 'A.SARANA_PRASARANA_INFRASTRUKTUR'
	WHEN 10 THEN 'A.LOKASI_INFRASTRUKTUR'
	WHEN 11 THEN 'A.BIAYA_PEMBANGUNAN_INFRASTRUKTUR'
	WHEN 12 THEN 'A.IZIN_MENDIRIKAN_BANGUNAN'
	WHEN 13 THEN 'A.NOMOR_REKENING'
	WHEN 14 THEN 'A.NAMA_REKENING_PENERIMA'
	WHEN 15 THEN 'A.NAMA_BANK'
	ELSE 'A.JENIS_SUMBANGAN' end; 

	DECLARE 
	@Query VARCHAR(MAX) = 'SELECT 
		ROW_NUMBER() OVER (ORDER BY '+ @SortField + ' ' + @SortDirection + ') AS RowNum,
		A.*
	FROM dbo.TB_R_Donation A 
	WHERE ISNULL(A.CATEGORY_CODE,'''')<>'''' AND DASHBOARD_ID=' + CASE WHEN @DashboardId IS NULL THEN 'null' ELSE ''''+ CONVERT(VARCHAR(100),@DashboardId)+'''' END

SET @Query = 'SELECT * FROM ('+ 
		@Query + 
		') AS TBL WHERE RowNum BETWEEN ' +
		CONVERT(VARCHAR, @FromNumber) + ' AND ' + CONVERT(VARCHAR, @ToNumber)
		
EXEC(@Query)
END

GO