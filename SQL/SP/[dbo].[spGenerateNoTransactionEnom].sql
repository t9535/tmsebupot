USE [TAM_EFAKTUR]
GO

---$ Alter Function [dbo].[spGenerateNoTransactionEnom] 
IF OBJECT_ID(N'[dbo].[spGenerateNoTransactionEnom]') IS NULL
BEGIN
    PRINT 'Create function : [dbo].[spGenerateNoTransactionEnom]'
    exec ('create function [dbo].[spGenerateNoTransactionEnom]() returns int as begin return 0 end') 
END
GO

PRINT 'Alter function : [dbo].[spGenerateNoTransactionEnom]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

ALTER FUNCTION  [dbo].[spGenerateNoTransactionEnom]   
(  
 @branchID char(3),   
 @businessdate Datetime,  
 @ID as varchar(10)  
) Returns Varchar(100)  
AS  
Begin  
 Declare @SequenceNo as Int  
 Declare @lengthno as int  
 Declare @Prefix as varchar(10)  
 Declare @isBranch as Bit  
 Declare @IsYear As bit  
 Declare @IsMOnth As bit  
 Declare @Suffix as Varchar(10)   
 Declare @strSequence as Varchar(100)  
 Declare @addzero as varchar(1000)  
    
 Declare @i as int   
  
 If exists (   
     select   
      MsSequenceID  
     from   
      dbo.TB_M_TransSequence   
     where   
      mssequenceid=@ID   
      and ISNULL(BranchID,'')=@branchID  
    )  
 Begin  
  select   
   @sequenceno = sequenceno,   
   @lengthno = lengthno,  
   @prefix = isnull(prefix,''),   
   @isBranch = IsBranch,   
   @isyear = IsYear,   
   @ismonth = ismonth,   
   @suffix = isnull(suffix,'')   
      
  from   
    dbo.TB_M_TransSequence
  where   
   mssequenceid=@ID   
  and BranchID=@branchID  
     
  Set @strSequence = @sequenceno  
    
       
  If @isBranch = 1   
   Set @strSequence = Rtrim(@branchID)  + @prefix  
  else  
   Set @strSequence = @prefix + Rtrim(@branchID)  
   
  If @isYear = 1   
  begin  
   set @strSequence = @strSequence + Ltrim(str(Year(@businessdate)))  
  end  
  
  if @ismonth = 1   
  Begin  
   set @strSequence = @strSequence + Right('00'+Ltrim(str(Month(@businessdate))),2)  
  end  
     
  Set @addZero = ''  
  --Set @i = 1  
  
  --While @i <= @lengthno  
  --Begin  
  -- Set @addZero = @addZero + '0'  
  -- Set @i = @i + 1  
  --end  
  select @addZero = right(replicate('0', @lengthno) , @lengthno)
  Set @strSequence = @StrSequence + Right(@addZero+Rtrim(@sequenceno),@lengthno)  
  Set @strSequence = @strSequence + @suffix       
 End  
  
 Return @strSequence  
   
  
  
End

GO
