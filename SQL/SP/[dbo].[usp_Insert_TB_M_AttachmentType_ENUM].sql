---$ Alter Procedure [dbo].[usp_Insert_TB_M_AttachmentType_ENUM] 
IF OBJECT_ID(N'[dbo].[usp_Insert_TB_M_AttachmentType_ENUM]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_Insert_TB_M_AttachmentType_ENUM]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_Insert_TB_M_AttachmentType_ENUM] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_Insert_TB_M_AttachmentType_ENUM]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[usp_Insert_TB_M_AttachmentType_ENUM]

	-- Add the parameters for the stored procedure here
	@AttachmentId int,
	@CategoryCode varchar(100),
	@CategoryDesc varchar(500),
	@Description varchar(100),
	@FileType varchar(500),
	@FileTypeDesc varchar(500),
	@Vendor_Group varchar(500),
	@Vendor_GroupDesc varchar(500),
	@Ebiling bit,
	@Elvis bit,
	@CreatedBy varchar(50)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @getValA As varchar(50)

	set @getValA = (select CategoryCode from TB_M_AttachmentType_ENUM WHERE CategoryCode=@CategoryCode AND Description=@Description)

	IF (@getValA!='') 
	BEGIN
       RAISERROR('Category and Dokumen Name already exist',16,1)
	   RETURN
	END

	else if  (@AttachmentId = 0 )
	BEGIN
	-- Insert statements for procedure here
	INSERT INTO TB_M_AttachmentType_ENUM (
		[CategoryCode],
		[CategoryDesc],
		[Description],
		[FileType],
		[FileTypeDesc],
		[Vendor_Group],
		[Vendor_GroupDesc],
		[Ebiling],
		[Elvis],
		[CreatedDate],
		[CreatedBy]
	)
	VALUES (
		@CategoryCode,
		@CategoryDesc,
		@Description,
		@FileType,
		@FileTypeDesc,
		@Vendor_Group,
		@Vendor_GroupDesc,
		@Ebiling,
		@Elvis,
		GETDATE(),
		@CreatedBy
	)
    END 
		ELSE
	BEGIN
		UPDATE TB_M_AttachmentType_ENUM SET 
			[CategoryCode] = @CategoryCode,
			[CategoryDesc] = @CategoryDesc,
			[Description] = @Description,
			[FileType] = @FileType,
			[FileTypeDesc] = @FileTypeDesc,
			[Vendor_Group] = @Vendor_Group,
			[Vendor_GroupDesc] = @Vendor_GroupDesc,
			[Ebiling] = @Ebiling,
			[Elvis] = @Elvis,
			[ModifiedDate] = GETDATE(),
			[ModifiedBy] = @CreatedBy
		WHERE [AttachmentId] = @AttachmentId
	END

END

GO
