---$ Alter Procedure [dbo].[usp_CountAttachmentListDashboard_ENUM] 
IF OBJECT_ID(N'[dbo].[usp_CountAttachmentListDashboard_ENUM]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_CountAttachmentListDashboard_ENUM]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_CountAttachmentListDashboard_ENUM] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_CountAttachmentListDashboard_ENUM]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

ALTER PROCEDURE [dbo].[usp_CountAttachmentListDashboard_ENUM] 
	@CategoryCode varchar(100) = '',
	@Description varchar(100) = '',
	@FileType varchar(100) = '',
	@Vendor_Group varchar(100) = '',
	--@Ebiling bit,
	--@Elvis bit,
	@Aplication VARCHAR(3)='ALL'
AS
BEGIN
	DECLARE 
	@Query VARCHAR(MAX) = 'SELECT
		COUNT(AttachmentId)
	FROM dbo.TB_M_AttachmentType_ENUM	
	WHERE 1=1',
	@QueryCategoryCode varchar(MAX) = '',
	@QueryDescription varchar(MAX) = '',
	@QueryFileType varchar(MAX) = '',
	@QueryVendor_Group varchar(MAX) = '',
	--@QueryEbiling bit,
	--@QueryElvis bit
	@QueryAplication varchar(MAX) = ''
	
SELECT @QueryCategoryCode = 
dbo.uf_LookupDynamicQueryGenerator(@CategoryCode, 'CategoryCode')
SELECT @QueryDescription = 
dbo.uf_LookupDynamicQueryGenerator(@Description, 'Description')
SELECT @QueryFileType = 
dbo.uf_LookupDynamicQueryGenerator(@FileType, 'FileType')
SELECT @QueryVendor_Group = 
dbo.uf_LookupDynamicQueryGenerator(@Vendor_Group, 'Vendor_Group')
--SELECT @QueryEbiling = 
--dbo.uf_LookupDynamicQueryGenerator(CONVERT(varchar,@Ebiling), 'Ebiling')
--SELECT @QueryElvis = 
--dbo.uf_LookupDynamicQueryGenerator(CONVERT(varchar,@Elvis), 'Elvis')
SELECT @QueryAplication = CASE WHEN @Aplication='TMS' THEN 
dbo.uf_LookupDynamicQueryGeneratorEqual(CONVERT(varchar,'1'), 'Ebiling')
 WHEN @Aplication='ELS' THEN 
dbo.uf_LookupDynamicQueryGeneratorEqual(CONVERT(varchar,'1'), 'Elvis')
ELSE
''
end

SET @Query = 
		ISNULL(@Query,'') + 
		ISNULL(@QueryCategoryCode,'') +
		ISNULL(@QueryDescription,'') +
		ISNULL(@QueryFileType,'') +
		ISNULL(@QueryVendor_Group,'') +
		ISNULL(@QueryAplication,'')
		--CONVERT(varchar,@QueryEbiling) +
		--CONVERT(varchar,@QueryElvis)

EXEC(@Query)
--PRINT(@Query)
END

GO