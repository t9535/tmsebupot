---$ Alter Procedure [dbo].[sp_GetManualFakturPajak] 
IF OBJECT_ID(N'[dbo].[sp_GetManualFakturPajak]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[sp_GetManualFakturPajak]'
    EXECUTE('CREATE PROCEDURE [dbo].[sp_GetManualFakturPajak] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[sp_GetManualFakturPajak]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go


ALTER PROCEDURE [dbo].[sp_GetManualFakturPajak] 
@IdNuminative BIGINT
AS
    BEGIN 
        DECLARE @npwpcom VARCHAR(500)
        SELECT  @npwpcom = tmcd.ConfigValue
        FROM    dbo.TB_M_ConfigurationData AS tmcd
        WHERE   tmcd.ConfigKey = 'CompanyNPWP'

        SELECT  [FakturType] AS FakturType ,
                NomorFakturGabungan AS InvoiceNumberFull ,
                FORMAT (TanggalFaktur, 'dd/MM/yyyy') AS InvoiceDate ,
                @npwpcom AS NPWPLawanTransaksi ,
                NPWPPenjual AS SupplierNPWP ,
                NamaPenjual AS SupplierName ,
                AlamatPenjual AS SupplierAddress ,
                CONVERT(DECIMAL(18,0),JumlahDPP) AS VATBaseAmount ,
                CONVERT(DECIMAL(18,0),JumlahPPN) AS VATAmount ,
                CONVERT(DECIMAL(18,0),JumlahPPNBM) AS JumlahPPnBM
        FROM    dbo.TB_R_VATIn
        WHERE   IdNuminative = @IdNuminative
    END

GO
