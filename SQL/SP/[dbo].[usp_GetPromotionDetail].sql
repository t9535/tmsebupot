---$ Alter Procedure [dbo].[usp_GetPromotionDetail] 
IF OBJECT_ID(N'[dbo].[usp_GetPromotionDetail]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_GetPromotionDetail]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_GetPromotionDetail] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_GetPromotionDetail]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go


ALTER PROCEDURE [dbo].[usp_GetPromotionDetail]
	@PromotionId uniqueidentifier,
	@SortBy int = 1,
	@SortDirection varchar(4) = 'DESC', 
	@FromNumber int,
	@ToNumber int
	AS
BEGIN
--SELECT * FROM TB_R_Promotion_Detail WHERE PROMOTION_ID=@PromotionId
DECLARE @SortField varchar(50)
	SET @SortField = 
	CASE @SortBy 
	WHEN 1 THEN 'A.TANGGAL'
	WHEN 2 THEN 'A.BENTUK_DAN_JENIS_BIAYA'
	WHEN 3 THEN 'A.JUMLAH' 
	WHEN 4 THEN 'A.JUMLAH_GROSS_UP'
	WHEN 5 THEN 'A.KETERANGAN'
	WHEN 6 THEN 'A.JUMLAH_PPH'
	WHEN 7 THEN 'A.JENIS_PPH'
	WHEN 8 THEN 'A.JUMLAH_NET'
	WHEN 9 THEN 'A.NOMOR_REKENING'
	WHEN 10 THEN 'A.NAMA_REKENING_PENERIMA'
	WHEN 11 THEN 'A.NAMA_BANK'
	WHEN 12 THEN 'A.NO_KTP'
	ELSE 'A.TANGGAL' end; 

	DECLARE 
	@Query VARCHAR(MAX) = 'SELECT 
		ROW_NUMBER() OVER (ORDER BY '+ @SortField + ' ' + @SortDirection + ') AS RowNum,
		A.*
	FROM dbo.TB_R_Promotion_Detail A 
	WHERE PROMOTION_ID=' + CASE WHEN @PromotionId IS NULL THEN 'null' ELSE ''''+ CONVERT(VARCHAR(100),@PromotionId)+'''' END
	
SET @Query = 'SELECT * FROM ('+ 
		@Query + 
		') AS TBL WHERE RowNum BETWEEN ' +
		CONVERT(VARCHAR, @FromNumber) + ' AND ' + CONVERT(VARCHAR, @ToNumber)
		
EXEC(@Query)
END

GO
