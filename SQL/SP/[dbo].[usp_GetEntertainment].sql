---$ Alter Procedure [dbo].[usp_GetEntertainment] 
IF OBJECT_ID(N'[dbo].[usp_GetEntertainment]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_GetEntertainment]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_GetEntertainment] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_GetEntertainment]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go



ALTER PROCEDURE [dbo].[usp_GetEntertainment]
	@DashboardId uniqueidentifier,
	@SortBy int = 1,
	@SortDirection varchar(4) = 'DESC', 
	@FromNumber int,
	@ToNumber int
	AS
BEGIN
--SELECT * FROM TB_R_Entertainment WHERE DASHBOARD_ID=@DashboardId
DECLARE @SortField varchar(50)
	SET @SortField = 
	CASE @SortBy 
	WHEN 1 THEN 'A.NO'
	WHEN 2 THEN 'A.TANGGAL'
	WHEN 3 THEN 'A.TEMPAT' 
	WHEN 4 THEN 'A.ALAMAT' 
	WHEN 5 THEN 'A.BENTUK_DAN_JENIS_ENTERTAINMENT'
	WHEN 6 THEN 'A.JUMLAH' 
	ELSE 'A.NO' end; 

	DECLARE 
	@Query VARCHAR(MAX) = 'SELECT 
		ROW_NUMBER() OVER (ORDER BY '+ @SortField + ' ' + @SortDirection + ') AS RowNum,
		A.*
	FROM dbo.TB_R_Entertainment A 
	WHERE ISNULL(A.CATEGORY_CODE,'''')<>'''' AND DASHBOARD_ID=' + CASE WHEN @DashboardId IS NULL THEN 'null' ELSE ''''+ CONVERT(VARCHAR(100),@DashboardId)+'''' END

SET @Query = 'SELECT * FROM ('+ 
		@Query + 
		') AS TBL WHERE RowNum BETWEEN ' +
		CONVERT(VARCHAR, @FromNumber) + ' AND ' + CONVERT(VARCHAR, @ToNumber)
		
EXEC(@Query)
END

GO
