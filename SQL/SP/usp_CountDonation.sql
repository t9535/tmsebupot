---$ Alter Procedure [dbo].[usp_CountDonation] 
IF OBJECT_ID(N'[dbo].[usp_CountDonation]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_CountDonation]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_CountDonation] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_CountDonation]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

ALTER PROCEDURE usp_CountDonation
	@DashboardId uniqueidentifier
	AS
BEGIN
SELECT COUNT(DONATION_ID) FROM TB_R_Donation WHERE DASHBOARD_ID=@DashboardId AND ISNULL(CATEGORY_CODE,'')<>''
END

GO

