---$ Alter Procedure [dbo].[usp_GetAttachmentListDashboard_ENUM] 
IF OBJECT_ID(N'[dbo].[usp_GetAttachmentListDashboard_ENUM]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_GetAttachmentListDashboard_ENUM]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_GetAttachmentListDashboard_ENUM] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_GetAttachmentListDashboard_ENUM]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

ALTER PROCEDURE [dbo].[usp_GetAttachmentListDashboard_ENUM]
--declare
	@SortBy int =0,
	@SortDirection varchar(4) = 'DESC',
	@CategoryCode varchar(100) = '',
	@Description varchar(100) = '',
	@FileType varchar(100) = '',
	@Vendor_Group varchar(100) = '',
	@Aplication VARCHAR(3)='ALL',
	@FromNumber INT=1,
	@ToNumber INT=10
	
AS
BEGIN

	DECLARE @SortField varchar(50)
	SET @SortField = 
	CASE @SortBy 
	WHEN 1 THEN 'AttachmentId'
	WHEN 2 THEN 'CategoryDesc'
	WHEN 3 THEN 'Description'
	WHEN 4 THEN 'FileTypeDesc'
	WHEN 5 THEN 'Vendor_GroupDesc'
	WHEN 6 THEN 'Ebiling'
	WHEN 7 THEN 'Elvis'
	ELSE 'AttachmentId' end; 

	--WHEN 1 THEN 'AttachmentId'
	--WHEN 2 THEN 'CategoryCode'
	--WHEN 3 THEN 'Description'
	--WHEN 4 THEN 'FileTypeDesc'
	--WHEN 5 THEN 'Vendor_GroupDesc'
	--ELSE 'CategoryDesc' end; 

	DECLARE 
	@Query VARCHAR(MAX) = 'SELECT
	ROW_NUMBER() OVER (ORDER BY '+ @SortField + ' ' + @SortDirection + ') AS RowNum,
	AttachmentId,
	CategoryCode,CategoryDesc,Description,FileType,FileTypeDesc,Vendor_Group,Vendor_GroupDesc,Ebiling,Elvis
	FROM dbo.TB_M_AttachmentType_ENUM where 1=1',
	@QueryCategoryCode varchar(MAX) = '',
	@QueryDescription varchar(MAX) = '',
	@QueryFileType varchar(MAX) = '',
	@QueryVendor_Group varchar(MAX) = '',
	@QueryAplication varchar(MAX) = ''


SELECT @QueryCategoryCode = 
dbo.uf_LookupDynamicQueryGenerator(@CategoryCode, 'CategoryCode')
SELECT @QueryDescription = 
dbo.uf_LookupDynamicQueryGenerator(@Description, 'Description')
SELECT @QueryFileType = 
dbo.uf_LookupDynamicQueryGenerator(@FileType, 'FileType')
SELECT @QueryVendor_Group = 
dbo.uf_LookupDynamicQueryGenerator(@Vendor_Group, 'Vendor_Group')
SELECT @QueryAplication = CASE WHEN @Aplication='TMS' THEN 
dbo.uf_LookupDynamicQueryGeneratorEqual(CONVERT(varchar,'1'), 'Ebiling')
 WHEN @Aplication='ELS' THEN 
dbo.uf_LookupDynamicQueryGeneratorEqual(CONVERT(varchar,'1'), 'Elvis')
ELSE
''
end

SET @Query = 'SELECT * FROM (' +
		ISNULL(@Query,'') + 
		ISNULL(@QueryCategoryCode,'') +
		ISNULL(@QueryDescription,'') +
		ISNULL(@QueryFileType,'') +
		ISNULL(@QueryVendor_Group,'') +
		ISNULL(@QueryAplication,'') +
		' ) AS TBL WHERE RowNum BETWEEN ' + 
		CONVERT( VARCHAR,@FromNumber) + ' AND ' + CONVERT( VARCHAR,@ToNumber) 

exec(@Query)
END

GO
