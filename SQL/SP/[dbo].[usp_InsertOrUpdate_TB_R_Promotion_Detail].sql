---$ Alter Procedure [dbo].[usp_InsertOrUpdate_TB_R_Promotion_Detail] 
IF OBJECT_ID(N'[dbo].[usp_InsertOrUpdate_TB_R_Promotion_Detail]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_InsertOrUpdate_TB_R_Promotion_Detail]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_InsertOrUpdate_TB_R_Promotion_Detail] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_InsertOrUpdate_TB_R_Promotion_Detail]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

ALTER PROCEDURE [dbo].[usp_InsertOrUpdate_TB_R_Promotion_Detail]
	--header
	@PROMOTION_ID uniqueidentifier,
	@DASHBOARD_ID uniqueidentifier,	
	@THN_PAJAK int,
	@TMP_PENANDATANGAN varchar(100),
	@TGL_PENANDATANGAN datetime ,
	@NAMA_PENANDATANGAN varchar(100),
	@JABATAN_PENANDATANGAN varchar(100),
	@NO INT,
	@NAMA varchar(100),
	@NPWP varchar(20),
	@ALAMAT varchar(200),
	--detail
	@PROMOTION_DETAIL_ID uniqueidentifier,
	@TANGGAL datetime ,
	@BENTUK_DAN_JENIS_BIAYA varchar(50),
	@JUMLAH decimal(18, 2),
	@JUMLAH_GROSS_UP decimal(18, 2),
	@KETERANGAN varchar(300),
	@JUMLAH_PPH decimal(18, 2),
	@JENIS_PPH varchar(50),
	@JUMLAH_NET decimal(18, 2),
	@NOMOR_REKENING varchar(20),
	@NAMA_REKENING_PENERIMA varchar(100),
	@NAMA_BANK varchar(100),
	@NO_KTP varchar(16), 
	@CREATED_DT datetime,
	@CREATED_BY varchar(20),
	@CHANGED_DT datetime,
	@CHANGED_BY varchar(20)
AS
    BEGIN
        SET NOCOUNT ON; 
		DECLARE @CATEGORY_CODE VARCHAR(20)
		DECLARE @ResultCode BIT = 1 ,
				@ResultDesc VARCHAR(MAX) ='File has been uploaded successfully'

		SELECT @CATEGORY_CODE=CategoryCode FROM TB_R_Dashboard_Nominative WHERE Id=@DASHBOARD_ID
	
	BEGIN TRANSACTION;
	BEGIN TRY 
	--PROCESS HEADER
		IF EXISTS (SELECT * FROM TB_R_Promotion WHERE Promotion_ID=@PROMOTION_ID)
		BEGIN
			UPDATE TB_R_Promotion
			SET DASHBOARD_ID=@DASHBOARD_ID ,
				CATEGORY_CODE=@CATEGORY_CODE,
				NO=@NO,
				NAMA=@NAMA,
				NPWP=@NPWP,
				ALAMAT=@ALAMAT,
				CHANGED_DT=@CHANGED_DT ,
				CHANGED_BY=@CHANGED_BY 
			WHERE Promotion_ID=@PROMOTION_ID
		END
		ELSE IF EXISTS (SELECT * FROM TB_R_Promotion WHERE DASHBOARD_ID=@DASHBOARD_ID AND ISNULL(CATEGORY_CODE,'')='')
		BEGIN
			SELECT @PROMOTION_ID = PROMOTION_ID FROM TB_R_Promotion WHERE DASHBOARD_ID=@DASHBOARD_ID AND ISNULL(CATEGORY_CODE,'')=''
			UPDATE TB_R_Promotion
			SET DASHBOARD_ID=@DASHBOARD_ID ,
				CATEGORY_CODE=@CATEGORY_CODE,
				NO=@NO,
				NAMA=@NAMA,
				NPWP=@NPWP,
				ALAMAT=@ALAMAT,
				CHANGED_DT=@CHANGED_DT ,
				CHANGED_BY=@CHANGED_BY 
			WHERE DASHBOARD_ID=@DASHBOARD_ID AND ISNULL(CATEGORY_CODE,'')=''
		END
		ELSE 
		BEGIN
			SET @PROMOTION_ID = NEWID();
            INSERT  INTO TB_R_Promotion
                    (   Promotion_ID,
						DASHBOARD_ID ,
						CATEGORY_CODE,
						NO,
						NAMA,
						NPWP,
						ALAMAT,
						CREATED_DT ,
						CREATED_BY 
	                )
            VALUES  (	@PROMOTION_ID,
						@DASHBOARD_ID ,
						@CATEGORY_CODE,
						@NO,
						@NAMA,
						@NPWP,
						@ALAMAT,
						@CREATED_DT ,
						@CREATED_BY
	                )
		END 
		
		
		----melakukan proses update path report dikarenakan ada perubahan pada transaksi baik itu donation,promotion dan entertainment
		--UPDATE TB_R_Dashboard_Nominative
		--	 SET NominativeFormAttachment=NULL
		--	 WHERE Id = @DASHBOARD_ID

		--process update header
		UPDATE TB_R_Promotion
			SET THN_PAJAK=@THN_PAJAK,
				TMP_PENANDATANGAN=@TMP_PENANDATANGAN,
				TGL_PENANDATANGAN=@TGL_PENANDATANGAN ,
				NAMA_PENANDATANGAN=@NAMA_PENANDATANGAN,
				JABATAN_PENANDATANGAN=@JABATAN_PENANDATANGAN
			WHERE DASHBOARD_ID=@DASHBOARD_ID
		
		--PROCESS DETAIL
		IF EXISTS (SELECT * FROM TB_R_Promotion_Detail WHERE PROMOTION_DETAIL_ID=@PROMOTION_DETAIL_ID)
		BEGIN
			UPDATE TB_R_Promotion_Detail
			SET TANGGAL=@TANGGAL,
				BENTUK_DAN_JENIS_BIAYA=@BENTUK_DAN_JENIS_BIAYA,
				JUMLAH=@JUMLAH,
				JUMLAH_GROSS_UP=@JUMLAH_GROSS_UP,
				KETERANGAN=@KETERANGAN,
				JUMLAH_PPH=@JUMLAH_PPH,
				JENIS_PPH=@JENIS_PPH,
				JUMLAH_NET=@JUMLAH_NET,
				NOMOR_REKENING=@NOMOR_REKENING,
				NAMA_REKENING_PENERIMA=@NAMA_REKENING_PENERIMA,
				NAMA_BANK=@NAMA_BANK,
				NO_KTP=@NO_KTP, 
				CHANGED_DT=@CHANGED_DT ,
				CHANGED_BY=@CHANGED_BY 
			WHERE PROMOTION_DETAIL_ID=@PROMOTION_DETAIL_ID
		END
		ELSE
		BEGIN 
			 INSERT  INTO TB_R_Promotion_Detail
                    (   PROMOTION_DETAIL_ID,
						PROMOTION_ID,
						TANGGAL,
						BENTUK_DAN_JENIS_BIAYA,
						JUMLAH,
						JUMLAH_GROSS_UP,
						KETERANGAN,
						JUMLAH_PPH,
						JENIS_PPH,
						JUMLAH_NET,
						NOMOR_REKENING,
						NAMA_REKENING_PENERIMA,
						NAMA_BANK,
						NO_KTP, 
						CREATED_DT,
						CREATED_BY
	                )
            VALUES  (	NEWID(),
						@PROMOTION_ID,
						@TANGGAL,
						@BENTUK_DAN_JENIS_BIAYA,
						@JUMLAH,
						@JUMLAH_GROSS_UP,
						@KETERANGAN,
						@JUMLAH_PPH,
						@JENIS_PPH,
						@JUMLAH_NET,
						@NOMOR_REKENING,
						@NAMA_REKENING_PENERIMA,
						@NAMA_BANK,
						@NO_KTP, 
						@CREATED_DT,
						@CREATED_BY
	                )
		END		

        SELECT  @ResultCode AS ResultCode ,
                @ResultDesc AS ResultDesc ,
				CONVERT(VARCHAR(100),@PROMOTION_ID) AS ResultDescs
 
    END TRY
    BEGIN CATCH

		SELECT  0 AS ResultCode ,
                ERROR_MESSAGE() AS ResultDesc ,
				CONVERT(VARCHAR(100),@PROMOTION_ID) AS ResultDescs

        IF @@TRANCOUNT> 0  
            ROLLBACK TRANSACTION;
    END CATCH
 
    IF @@TRANCOUNT > 0  
        COMMIT TRANSACTION;
    END

GO
