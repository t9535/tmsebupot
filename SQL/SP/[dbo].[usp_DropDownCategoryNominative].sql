---$ Alter Procedure [dbo].[usp_DropDownCategoryNominative] 
IF OBJECT_ID(N'[dbo].[usp_DropDownCategoryNominative]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_DropDownCategoryNominative]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_DropDownCategoryNominative] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_DropDownCategoryNominative]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

ALTER PROCEDURE [dbo].[usp_DropDownCategoryNominative]

	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		NominativeType Id, 
		NominativeType As Name 
	FROM TB_M_CategoryNominative_ENUM
	ORDER BY NominativeType
	
END

GO