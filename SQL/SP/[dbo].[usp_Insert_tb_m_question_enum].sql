---$ Alter Procedure [dbo].[usp_Insert_tb_m_question_enum] 
IF OBJECT_ID(N'[dbo].[usp_Insert_tb_m_question_enum]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_Insert_tb_m_question_enum]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_Insert_tb_m_question_enum] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_Insert_tb_m_question_enum]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go


ALTER PROCEDURE [dbo].[usp_Insert_tb_m_question_enum]

	-- Add the parameters for the stored procedure here
	--@QuestionId int,
	@CategoryCode varchar(100),
	@Question varchar(260),
	@IsYes varchar(10),
	@WarningStatusYes varchar(50),
	@IsNo varchar(10),
	@WarningStatusNo varchar(50),
	@CreatedDate datetime,
	@CreatedBy varchar(50)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @getValA As varchar(1000)

	set @getValA = (select CategoryCode from tb_m_question_enum WHERE CategoryCode=@CategoryCode AND Question=@Question)

	IF (@getValA!='') 
	BEGIN
       RAISERROR('Category and Questionaire already exist',16,1)
	   RETURN
	END
    -- Insert statements for procedure here
	INSERT INTO tb_m_question_enum (
		--QuestionId,
		CategoryCode,
		Question,
		IsYes,
		WarningStatusYes,
		IsNo,
		WarningStatusNo,
		CreatedDate,
		CreatedBy		
	)
	VALUES (
	--@QuestionId,
	@CategoryCode,
	@Question ,
	@IsYes,
	@WarningStatusYes,
	@IsNo,
	@WarningStatusNo,
	@CreatedDate,
	@CreatedBy
		
	)

END

GO
