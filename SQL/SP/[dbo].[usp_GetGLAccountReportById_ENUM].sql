---$ Alter Procedure [dbo].[usp_GetGLAccountReportById_ENUM] 
IF OBJECT_ID(N'[dbo].[usp_GetGLAccountReportById_ENUM]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_GetGLAccountReportById_ENUM]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_GetGLAccountReportById_ENUM] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_GetGLAccountReportById_ENUM]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

ALTER PROCEDURE [dbo].[usp_GetGLAccountReportById_ENUM]
--	@NominativeId varchar (MAX)
--AS
--BEGIN
--SELECT ColumnData
--	INTO #tempSelectedParamId
--	FROM uf_SplitString(@NominativeId, ';')

--	SELECT CategoryCode as Field1,
--           GLAccount as Field2,
--           DescriptionTransaction as Field3,
--           Type as Field4

--            FROM dbo.TB_M_Nominative_ENUM
--			WHERE NominativeId IN (SELECT ColumnData From #tempSelectedParamId )

--	drop table #tempSelectedParamId
--end

	@SortBy int = 1,
	@SortDirection varchar(4) = 'DESC',
	@CategoryCode varchar(10) = '',
	--@CategoryName varchar(50) = '',
	@GLAccount varchar(20) = '',
	@DescriptionTransaction varchar(200) = '',
	@Type varchar(30) = ''
	
AS
BEGIN
	DECLARE @SortField varchar(50)
	SET @SortField = 
	CASE @SortBy 
	WHEN 1 THEN 'NominativeId'
	WHEN 2 THEN 'GLAccount'
	WHEN 3 THEN 'DescriptionTransaction'
	WHEN 4 THEN 'Type'
	ELSE 'a.CategoryCode' end; 
	
	DECLARE 
	@Query VARCHAR(MAX) = 'SELECT
	ROW_NUMBER() OVER (ORDER BY '+ @SortField + ' ' + @SortDirection + ') AS RowNum,
	
	NominativeId,
	a.CategoryCode,
	b.NominativeType as CategoryName,GLAccount,DescriptionTransaction,Type
	FROM dbo.TB_M_Nominative_ENUM a
	inner join TB_M_CategoryNominative_ENUM b on a.CategoryCode=b.CategoryCode
	WHERE 1=1',
	@QueryCategoryCode varchar(MAX) = '',
	@QueryGLAccount varchar(MAX) = '',
	@QueryDescriptionTransaction varchar(MAX) = '',
	@QueryType varchar(MAX) = ''

SELECT @QueryCategoryCode = 
dbo.uf_LookupDynamicQueryGenerator(@CategoryCode, 'a.CategoryCode')
SELECT @QueryGLAccount = 
dbo.uf_LookupDynamicQueryGenerator(@GLAccount, 'GLAccount')
SELECT @QueryDescriptionTransaction = 
dbo.uf_LookupDynamicQueryGenerator(@DescriptionTransaction, 'DescriptionTransaction')
SELECT @QueryType = 
dbo.uf_LookupDynamicQueryGenerator(@Type, 'Type')

SET @Query = 'SELECT * FROM (' +
		@Query + 
		@QueryCategoryCode +
		@QueryGLAccount +
		@QueryDescriptionTransaction +
		@QueryType +
		' ) AS TBL '

EXEC(@Query)
--select @Query
END

GO