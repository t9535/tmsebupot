---$ Alter Procedure [dbo].[usp_CountCategoryListDashboard_ENUM] 
IF OBJECT_ID(N'[dbo].[usp_CountCategoryListDashboard_ENUM]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_CountCategoryListDashboard_ENUM]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_CountCategoryListDashboard_ENUM] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_CountCategoryListDashboard_ENUM]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

ALTER PROCEDURE [dbo].[usp_CountCategoryListDashboard_ENUM]
	@CategoryCode varchar(50) = '',
	@NominativeType varchar(50) = '',
	@JointGroup varchar(50) = '',
	@GroupSeq varchar(10)	
AS
BEGIN
	DECLARE 
	@Query VARCHAR(MAX) = 'SELECT
		COUNT(CategoryCode)
	FROM dbo.TB_M_CategoryNominative_ENUM where 1=1',
	@QueryCategoryCode varchar(MAX) = '',
	@QueryNominativeType varchar(MAX) = '',
	@QueryJointGroup varchar(MAX) = '',
	@QueryGroupSeq varchar(MAX) = ''
	
SELECT @QueryCategoryCode = 
dbo.uf_LookupDynamicQueryGenerator(@CategoryCode, 'CategoryCode')
SELECT @QueryNominativeType = 
dbo.uf_LookupDynamicQueryGenerator(@NominativeType, 'NominativeType')
SELECT @QueryJointGroup = 
dbo.uf_LookupDynamicQueryGenerator(@JointGroup, 'JointGroup')
SELECT @QueryGroupSeq = 
dbo.uf_LookupDynamicQueryGenerator(@GroupSeq, 'GroupSeq')

SET @Query = 
		@Query + 
		@QueryCategoryCode +
		@QueryNominativeType +
		@QueryJointGroup +
		@QueryGroupSeq

EXEC(@Query)
END

GO