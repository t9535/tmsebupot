---$ Alter Procedure [dbo].[usp_Update_TB_M_AttachmentType_ENUM_ENUM] 
IF OBJECT_ID(N'[dbo].[usp_Update_TB_M_AttachmentType_ENUM_ENUM]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_Update_TB_M_AttachmentType_ENUM_ENUM]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_Update_TB_M_AttachmentType_ENUM_ENUM] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_Update_TB_M_AttachmentType_ENUM_ENUM]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[usp_Update_TB_M_AttachmentType_ENUM_ENUM]

	-- Add the parameters for the stored procedure here
	@AttachmentId int,
	@CategoryCode varchar(100),
	@CategoryDesc varchar(500),
	@Description varchar(100),
	@FileType varchar(500),
	@FileTypeDesc varchar(500),
	@Vendor_Group varchar(500),
	@Vendor_GroupDesc varchar(500),
	@Ebiling bit,
	@Elvis bit,
	@ModifiedBy varchar(50)

AS
BEGIN

	UPDATE TB_M_AttachmentType_ENUM SET 
			[CategoryCode] = @CategoryCode,
			[CategoryDesc] = @CategoryDesc,
			[Description] = @Description,
			[FileType] = @FileType,
			[FileTypeDesc] = @FileTypeDesc,
			[Vendor_Group] = @Vendor_Group,
			[Vendor_GroupDesc] = @Vendor_GroupDesc,
			[ModifiedDate] = GETDATE(),
			[Ebiling] = @Ebiling,
			[Elvis] = @Elvis,
			[ModifiedBy] = @ModifiedBy
		WHERE [AttachmentId] = @AttachmentId

	END
--END

GO
