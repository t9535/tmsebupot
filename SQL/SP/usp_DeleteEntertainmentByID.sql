---$ Alter Procedure [dbo].[usp_DeleteEntertainmentByID] 
IF OBJECT_ID(N'[dbo].[usp_DeleteEntertainmentByID]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_DeleteEntertainmentByID]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_DeleteEntertainmentByID] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_DeleteEntertainmentByID]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

ALTER PROCEDURE usp_DeleteEntertainmentByID

	-- Add the parameters for the stored procedure here
	@ID UNIQUEIDENTIFIER
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @DashboardId UNIQUEIDENTIFIER, @Count INT
	SELECT @DashboardId= A.DASHBOARD_ID FROM dbo.TB_R_Entertainment A WHERE A.ENTERTAINMENT_ID=@ID 
	
	DELETE A
	FROM dbo.TB_R_Entertainment_Detail A
	WHERE A.ENTERTAINMENT_ID=@ID 

	 SELECT @Count=COUNT(DASHBOARD_ID) FROM  dbo.TB_R_Entertainment WHERE DASHBOARD_ID=@DashboardId
	 IF(@Count=1)
	 BEGIN
		UPDATE TB_R_Entertainment
			SET CATEGORY_CODE=NULL,
				NO=NULL,
				TANGGAL=NULL,
				TEMPAT=NULL,
				ALAMAT=NULL,
				BENTUK_DAN_JENIS_ENTERTAINMENT=NULL,
				JUMLAH=NULL
			WHERE DASHBOARD_ID=@DashboardId
	 END
	 ELSE
	 BEGIN	
		DELETE A
		FROM dbo.TB_R_Entertainment A
		WHERE A.ENTERTAINMENT_ID=@ID 
	END
END

GO
