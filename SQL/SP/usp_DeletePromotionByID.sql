---$ Alter Procedure [dbo].[usp_DeletePromotionByID] 
IF OBJECT_ID(N'[dbo].[usp_DeletePromotionByID]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_DeletePromotionByID]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_DeletePromotionByID] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_DeletePromotionByID]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

ALTER PROCEDURE usp_DeletePromotionByID

	-- Add the parameters for the stored procedure here
	@ID UNIQUEIDENTIFIER
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @DashboardId UNIQUEIDENTIFIER, @Count INT
	SELECT @DashboardId= A.DASHBOARD_ID FROM dbo.TB_R_Promotion A WHERE A.Promotion_ID=@ID 
	
	DELETE A
	FROM dbo.TB_R_Promotion_Detail A
	WHERE A.PROMOTION_ID=@ID 

	 SELECT @Count=COUNT(DASHBOARD_ID) FROM  dbo.TB_R_Promotion WHERE DASHBOARD_ID=@DashboardId
	 IF(@Count=1)
	 BEGIN
		UPDATE TB_R_Promotion
			SET CATEGORY_CODE=NULL,
				NO=NULL,
				NAMA=NULL,
				NPWP=NULL,
				ALAMAT=NULL
			WHERE DASHBOARD_ID=@DashboardId
	 END
	 ELSE
	 BEGIN	
		DELETE A
		FROM dbo.TB_R_Promotion A
		WHERE A.Promotion_ID=@ID 
	END
END

GO