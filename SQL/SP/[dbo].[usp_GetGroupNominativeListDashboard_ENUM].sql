---$ Alter Procedure [dbo].[usp_GetGroupNominativeListDashboard_ENUM] 
IF OBJECT_ID(N'[dbo].[usp_GetGroupNominativeListDashboard_ENUM]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_GetGroupNominativeListDashboard_ENUM]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_GetGroupNominativeListDashboard_ENUM] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_GetGroupNominativeListDashboard_ENUM]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go


ALTER PROCEDURE [dbo].[usp_GetGroupNominativeListDashboard_ENUM]

	@SortBy int = 1,
	@SortDirection varchar(4) = 'DESC',
	@CategoryCode varchar(50) = '',
	@GLAccount varchar(50) = '',
	@DescriptionTransaction varchar(200) = '',
	@Type varchar(30) = '',
	@FromNumber int,
	@ToNumber int
	
AS
BEGIN
	DECLARE @SortField varchar(50)
	SET @SortField = 
	CASE @SortBy 
	WHEN 1 THEN 'a.NominativeId'
	WHEN 2 THEN 'b.NominativeType'
	WHEN 3 THEN 'GLAccount'
	WHEN 4 THEN 'DescriptionTransaction'
	WHEN 5 THEN 'Type'
	ELSE 'A.NominativeId' end; 
	
	DECLARE 
	@Query VARCHAR(MAX) = 'SELECT
	ROW_NUMBER() OVER (ORDER BY '+ @SortField + ' ' + @SortDirection + ') AS RowNum,
	a.NominativeId,
	a.CategoryCode, b.NominativeType as CategoryName,  GLAccount,DescriptionTransaction,Type
	FROM dbo.TB_M_Nominative_ENUM as a
	inner join TB_M_CategoryNominative_ENUM as b on a.CategoryCode=b.CategoryCode where 1=1',
	@QueryCategoryCode varchar(MAX) = '',
	@QueryGLAccount varchar(MAX) = '',
	@QueryDescriptionTransaction varchar(MAX) = '',
	@QueryType varchar(MAX) = ''

SELECT @QueryCategoryCode = 
dbo.uf_LookupDynamicQueryGenerator(@CategoryCode, 'a.CategoryCode')
SELECT @QueryGLAccount = 
dbo.uf_LookupDynamicQueryGenerator(@GLAccount, 'GLAccount')
SELECT @QueryDescriptionTransaction = 
dbo.uf_LookupDynamicQueryGenerator(@DescriptionTransaction, 'DescriptionTransaction')
SELECT @QueryType = 
dbo.uf_LookupDynamicQueryGenerator(@Type, 'Type')

SET @Query = 'SELECT * FROM (' +
		@Query + 
		@QueryCategoryCode +
		@QueryGLAccount +
		@QueryDescriptionTransaction +
		@QueryType +
		' ) AS TBL WHERE RowNum BETWEEN ' +
		CONVERT( VARCHAR,@FromNumber) + ' AND ' + CONVERT( VARCHAR,@ToNumber) 

EXEC(@Query)
END

GO