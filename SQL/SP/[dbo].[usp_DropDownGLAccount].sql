---$ Alter Procedure [dbo].[usp_DropDownGLAccount] 
IF OBJECT_ID(N'[dbo].[usp_DropDownGLAccount]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_DropDownGLAccount]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_DropDownGLAccount] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_DropDownGLAccount]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

ALTER PROCEDURE [dbo].[usp_DropDownGLAccount]
@CategoryCode VARCHAR(50) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT 
		--NominativeId Id, 
		GLAccount Id, 
		GLAccount + ' - ' + DescriptionTransaction Name 
	FROM TB_M_Nominative_ENUM 
	WHERE CategoryCode LIKE '%'+ISNULL(@CategoryCode,'')+'%'
	
END

GO