---$ Alter Procedure [dbo].[usp_CountEntertainment] 
IF OBJECT_ID(N'[dbo].[usp_CountEntertainment]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_CountEntertainment]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_CountEntertainment] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_CountEntertainment]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

ALTER PROCEDURE usp_CountEntertainment
	@DashboardId uniqueidentifier
	AS
BEGIN
SELECT COUNT(ENTERTAINMENT_ID) FROM TB_R_Entertainment WHERE DASHBOARD_ID=@DashboardId AND ISNULL(CATEGORY_CODE,'')<>''
END

GO
