---$ Alter Procedure [dbo].[usp_GetDonationFromPdf] 
IF OBJECT_ID(N'[dbo].[usp_GetDonationFromPdf]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_GetDonationFromPdf]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_GetDonationFromPdf] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_GetDonationFromPdf]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

ALTER PROCEDURE [dbo].[usp_GetDonationFromPdf]
	@DashboardId uniqueidentifier
	AS
BEGIN	
SELECT 
			(SELECT TOP 1 CD.ConfigValue FROM TB_M_ConfigurationData CD WHERE CD.ConfigKey = 'CompanyName') AS CompanyName, 
			(SELECT TOP 1 CD.ConfigValue FROM TB_M_ConfigurationData CD WHERE CD.ConfigKey = 'CompanyNPWP') AS CompanyNPWP,
			(SELECT TOP 1 CD.ConfigValue FROM TB_M_ConfigurationData CD WHERE CD.ConfigKey = 'CompanyAddress') AS CompanyAddress, 
			a.*, ISNULL(c.PVNO,'-') AS PV_NO, c.NominativeIDNo AS REFERENCE_NO
	 FROM TB_R_Donation a 
	 INNER JOIN TB_R_Dashboard_Nominative c ON a.DASHBOARD_ID = c.Id 
	 WHERE a.DASHBOARD_ID = @DashboardId
END

GO
