---$ Alter Procedure [dbo].[usp_GetEntertainmentDetailByID] 
IF OBJECT_ID(N'[dbo].[usp_GetEntertainmentDetailByID]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_GetEntertainmentDetailByID]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_GetEntertainmentDetailByID] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_GetEntertainmentDetailByID]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

ALTER PROCEDURE usp_GetEntertainmentDetailByID

	-- Add the parameters for the stored procedure here
	@ID UNIQUEIDENTIFIER
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT A.*
	FROM dbo.TB_R_Entertainment_Detail A
	WHERE A.ENTERTAINMENT_DETAIL_ID=@ID 
END

GO
