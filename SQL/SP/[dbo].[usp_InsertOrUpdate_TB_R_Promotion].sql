---$ Alter Procedure [dbo].[usp_InsertOrUpdate_TB_R_Promotion] 
IF OBJECT_ID(N'[dbo].[usp_InsertOrUpdate_TB_R_Promotion]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_InsertOrUpdate_TB_R_Promotion]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_InsertOrUpdate_TB_R_Promotion] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_InsertOrUpdate_TB_R_Promotion]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

ALTER PROCEDURE [dbo].[usp_InsertOrUpdate_TB_R_Promotion]
	@PROMOTION_ID uniqueidentifier,
	@DASHBOARD_ID uniqueidentifier,	
	@THN_PAJAK int,
	@TMP_PENANDATANGAN varchar(100),
	@TGL_PENANDATANGAN datetime ,
	@NAMA_PENANDATANGAN varchar(100),
	@JABATAN_PENANDATANGAN varchar(100),
	@NO INT,
	@NAMA varchar(100),
	@NPWP varchar(20),
	@ALAMAT varchar(200),
	@CREATED_DT datetime,
	@CREATED_BY varchar(20),
	@CHANGED_DT datetime,
	@CHANGED_BY varchar(20)
AS
    BEGIN
        SET NOCOUNT ON; 
		DECLARE @CATEGORY_CODE VARCHAR(20)

		SELECT @CATEGORY_CODE=CategoryCode FROM TB_R_Dashboard_Nominative WHERE Id=@DASHBOARD_ID

		IF EXISTS (SELECT * FROM TB_R_Promotion WHERE Promotion_ID=@PROMOTION_ID)
		BEGIN
			UPDATE TB_R_Promotion
			SET DASHBOARD_ID=@DASHBOARD_ID ,
				CATEGORY_CODE=@CATEGORY_CODE,
				NO=@NO,
				NAMA=@NAMA,
				NPWP=@NPWP,
				ALAMAT=@ALAMAT,
				CHANGED_DT=@CHANGED_DT ,
				CHANGED_BY=@CHANGED_BY 
			WHERE Promotion_ID=@PROMOTION_ID
		END
		ELSE IF EXISTS (SELECT * FROM TB_R_Promotion WHERE DASHBOARD_ID=@DASHBOARD_ID AND ISNULL(CATEGORY_CODE,'')='')
		BEGIN
			UPDATE TB_R_Promotion
			SET DASHBOARD_ID=@DASHBOARD_ID ,
				CATEGORY_CODE=@CATEGORY_CODE,
				NO=@NO,
				NAMA=@NAMA,
				NPWP=@NPWP,
				ALAMAT=@ALAMAT,
				CHANGED_DT=@CHANGED_DT ,
				CHANGED_BY=@CHANGED_BY 
			WHERE DASHBOARD_ID=@DASHBOARD_ID AND ISNULL(CATEGORY_CODE,'')=''
		END
		ELSE 
		BEGIN
            INSERT  INTO TB_R_Promotion
                    (   Promotion_ID,
						DASHBOARD_ID ,
						CATEGORY_CODE,
						NO,
						NAMA,
						NPWP,
						ALAMAT,
						CREATED_DT ,
						CREATED_BY 
	                )
            VALUES  ( NEWID(),
						@DASHBOARD_ID ,
						@CATEGORY_CODE,
						@NO,
						@NAMA,
						@NPWP,
						@ALAMAT,
						@CREATED_DT ,
						@CREATED_BY
	                )
		END 
		
		----melakukan proses update path report dikarenakan ada perubahan pada transaksi baik itu donation,promotion dan entertainment
		--UPDATE TB_R_Dashboard_Nominative
		--	 SET NominativeFormAttachment=NULL
		--	 WHERE Id = @DASHBOARD_ID

		--process update header
		UPDATE TB_R_Promotion
			SET THN_PAJAK=@THN_PAJAK,
				TMP_PENANDATANGAN=@TMP_PENANDATANGAN,
				TGL_PENANDATANGAN=@TGL_PENANDATANGAN ,
				NAMA_PENANDATANGAN=@NAMA_PENANDATANGAN,
				JABATAN_PENANDATANGAN=@JABATAN_PENANDATANGAN
			WHERE DASHBOARD_ID=@DASHBOARD_ID
    END

GO
