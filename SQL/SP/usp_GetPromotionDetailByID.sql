---$ Alter Procedure [dbo].[usp_GetPromotionDetailByID] 
IF OBJECT_ID(N'[dbo].[usp_GetPromotionDetailByID]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_GetPromotionDetailByID]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_GetPromotionDetailByID] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_GetPromotionDetailByID]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

ALTER PROCEDURE usp_GetPromotionDetailByID

	-- Add the parameters for the stored procedure here
	@ID UNIQUEIDENTIFIER
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT A.*
	FROM dbo.TB_R_Promotion_Detail A
	WHERE A.Promotion_Detail_ID=@ID 
END

GO
