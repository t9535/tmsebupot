---$ Alter Procedure [dbo].[Get_Usp_Duplicat_Attachment_Enum] 
IF OBJECT_ID(N'[dbo].[Get_Usp_Duplicat_Attachment_Enum]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[Get_Usp_Duplicat_Attachment_Enum]'
    EXECUTE('CREATE PROCEDURE [dbo].[Get_Usp_Duplicat_Attachment_Enum] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[Get_Usp_Duplicat_Attachment_Enum]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE Get_Usp_Duplicat_Attachment_Enum
	-- Add the parameters for the stored procedure here
	@CategoryCode varchar(100),
	@Description varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	--select * from TB_M_Nominative_ENUM WHERE CategoryCode=@CategoryCode AND GLAccount=@GLAccount
	 select * from TB_M_AttachmentType_ENUM WHERE CategoryCode=@CategoryCode AND Description=@Description
END

GO