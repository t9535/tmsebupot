---$ Alter Procedure [dbo].[usp_GetKategoriDropdownList_ENUM] 
IF OBJECT_ID(N'[dbo].[usp_GetKategoriDropdownList_ENUM]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_GetKategoriDropdownList_ENUM]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_GetKategoriDropdownList_ENUM] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_GetKategoriDropdownList_ENUM]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

ALTER PROCEDURE usp_GetKategoriDropdownList_ENUM
AS
BEGIN
	
	SELECT 
		CategoryCode AS	Id,
		NominativeType AS Name
	FROM TB_M_CategoryNominative_ENUM 

END

GO
