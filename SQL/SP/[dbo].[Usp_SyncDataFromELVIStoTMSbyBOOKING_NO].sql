---$ Alter Procedure [dbo].[Usp_SyncDataFromELVIStoTMSbyBOOKING_NO] 
IF OBJECT_ID(N'[dbo].[Usp_SyncDataFromELVIStoTMSbyBOOKING_NO]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[Usp_SyncDataFromELVIStoTMSbyBOOKING_NO]'
    EXECUTE('CREATE PROCEDURE [dbo].[Usp_SyncDataFromELVIStoTMSbyBOOKING_NO] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[Usp_SyncDataFromELVIStoTMSbyBOOKING_NO]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

ALTER PROCEDURE [dbo].[Usp_SyncDataFromELVIStoTMSbyBOOKING_NO]
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

        UPDATE  TAM_EFAKTUR.dbo.TB_R_Dashboard_Nominative
        SET     TurnoverAmount = trab.AVAILABLE_AMT-trab.OUTSTANDING_AMT
        FROM    TAM_EFAKTUR.dbo.TB_R_Dashboard_Nominative a
                INNER JOIN ELVIS_DB.dbo.TB_R_ACCR_BALANCE AS trab ON a.AccruedBookingNo = trab.BOOKING_NO
        WHERE   ISNULL(a.PVNo, '') = ''
    END

GO
