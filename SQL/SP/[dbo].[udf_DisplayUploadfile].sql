---$ Alter Function [dbo].[udf_DisplayUploadfile] 
IF OBJECT_ID(N'[dbo].[udf_DisplayUploadfile]') IS NULL
BEGIN
    PRINT 'Create function : [dbo].[udf_DisplayUploadfile]'
    exec ('create function [dbo].[udf_DisplayUploadfile]() returns int as begin return 0 end') 
END
GO

PRINT 'Alter function : [dbo].[udf_DisplayUploadfile]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

ALTER FUNCTION  [dbo].[udf_DisplayUploadfile]
    (
      @id VARCHAR(max)
    )
RETURNS VARCHAR(MAX)
AS
    BEGIN
        DECLARE @rtn VARCHAR(MAX)= ''  
	
	

        SELECT @rtn = @rtn + LTRIM(RTRIM(NominativeIDNo)) + ', ' FROM dbo.TB_R_Dashboard_Nominative 
		WHERE Id IN (SELECT Item FROM splitstring(@id,','))
            
        RETURN @rtn
	
    END

GO