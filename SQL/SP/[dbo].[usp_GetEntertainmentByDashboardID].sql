---$ Alter Procedure [dbo].[usp_GetEntertainmentByDashboardID] 
IF OBJECT_ID(N'[dbo].[usp_GetEntertainmentByDashboardID]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_GetEntertainmentByDashboardID]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_GetEntertainmentByDashboardID] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_GetEntertainmentByDashboardID]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

ALTER PROCEDURE [dbo].[usp_GetEntertainmentByDashboardID]

	-- Add the parameters for the stored procedure here
	@DashboardId UNIQUEIDENTIFIER
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF EXISTS(SELECT TOP 1 * FROM dbo.TB_R_Entertainment A WHERE A.DASHBOARD_ID=@DashboardId )
	BEGIN
		SELECT TOP 1 A.* ,C.PositionSignature
		FROM dbo.TB_R_Entertainment A
		INNER JOIN dbo.TB_R_Dashboard_Nominative B ON A.DASHBOARD_ID = B.Id
		INNER JOIN dbo.TB_M_CategoryNominative_ENUM C ON B.CategoryCode = C.CategoryCode
		WHERE A.DASHBOARD_ID=@DashboardId 
	END
	ELSE
	BEGIN
		SELECT DASHBOARD_ID=B.Id, THN_PAJAK=YEAR(GETDATE()),C.PositionSignature, CASE WHEN C.PositionSignature=1 THEN '' ELSE 'Direktur' END AS JABATAN_PENANDATANGAN
		FROM dbo.TB_R_Dashboard_Nominative B
		INNER JOIN dbo.TB_M_CategoryNominative_ENUM C ON B.CategoryCode = C.CategoryCode
		WHERE B.ID=@DashboardId  
	END
END

GO
