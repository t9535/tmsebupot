---$ Alter Procedure [dbo].[usp_Update_TB_M_Nominative_ENUM] 
IF OBJECT_ID(N'[dbo].[usp_Update_TB_M_Nominative_ENUM]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_Update_TB_M_Nominative_ENUM]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_Update_TB_M_Nominative_ENUM] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_Update_TB_M_Nominative_ENUM]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[usp_Update_TB_M_Nominative_ENUM]

	-- Add the parameters for the stored procedure here
	@NominativeId int,
	@CategoryCode varchar(10),
	@GLAccount varchar(30),
	@DescriptionTransaction varchar(200),
	@Type varchar(30),
	@ModifiedOn datetime,
	@ModifiedBy varchar(50)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--SET NOCOUNT ON;
	--declare @getValA As varchar(50)
	--declare @getValB As varchar(50)
	--declare @getValC As varchar(50)

	--set @getValA = (select CategoryCode from TB_M_Nominative_ENUM WHERE CategoryCode=@CategoryCode AND GLAccount=@GLAccount)
 --   set @getValB = (select CategoryCode from TB_M_Nominative_ENUM WHERE CategoryCode=@CategoryCode AND DescriptionTransaction=@DescriptionTransaction)
 
	---- Insert statements for procedure here
	--IF (@getValA!='')
	--BEGIN
 --      RAISERROR('Category and GL Account already exist',16,1)
	--   RETURN
	--END
 --  ELSE IF (@getValB!='')
	--BEGIN
 --      RAISERROR('Category and Account Name already exist',16,1)
	--   RETURN
	--END

 -- ELSE
  
 --  BEGIN

    -- Insert statements for procedure here
	UPDATE TB_M_Nominative_ENUM SET 
		[CategoryCode] = @CategoryCode,
		[GLAccount] = @GLAccount,
		[DescriptionTransaction] = @DescriptionTransaction,
		[Type] = @Type,
		[ModifiedDate] = @ModifiedOn,
		[ModifiedBy] = @ModifiedBy
	WHERE [NominativeId] = @NominativeId

	END
--END

GO
