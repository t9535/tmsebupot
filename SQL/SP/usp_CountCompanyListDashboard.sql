---$ Alter Procedure [dbo].[usp_CountCompanyListDashboard] 
IF OBJECT_ID(N'[dbo].[usp_CountCompanyListDashboard]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_CountCompanyListDashboard]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_CountCompanyListDashboard] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_CountCompanyListDashboard]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE usp_CountCompanyListDashboard
	-- Add the parameters for the stored procedure here
	@Lokasi varchar(100) = '',
	@Code varchar(260) = ''
AS
BEGIN
DECLARE 
	@Query VARCHAR(MAX) = 'select COUNT(id)
	from TB_M_Company_Detail
	where flag=0',
	@QueryLokasi varchar(MAX) = '',
	@QueryCode varchar(MAX) = ''

SELECT @QueryLokasi = 
dbo.uf_LookupDynamicQueryGenerator(@Lokasi, 'Lokasi')

SELECT @QueryCode = 
dbo.uf_LookupDynamicQueryGenerator(@Code, 'Code')
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET @Query = 
		@Query + 
		@QueryLokasi +
		@QueryCode 
		
EXEC(@Query)

    -- Insert statements for procedure here
	
END

GO