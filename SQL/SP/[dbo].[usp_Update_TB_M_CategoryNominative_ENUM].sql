---$ Alter Procedure [dbo].[usp_Update_TB_M_CategoryNominative_ENUM] 
IF OBJECT_ID(N'[dbo].[usp_Update_TB_M_CategoryNominative_ENUM]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_Update_TB_M_CategoryNominative_ENUM]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_Update_TB_M_CategoryNominative_ENUM] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_Update_TB_M_CategoryNominative_ENUM]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[usp_Update_TB_M_CategoryNominative_ENUM]

	-- Add the parameters for the stored procedure here
	@CategoryCode varchar(10),
	----@CategoryCodeOld varchar(10),
	--@NominativeType varchar(50),
	@JointGroup varchar(50),
	@GroupSeq int,
	@PositionSignature bit,
	@ModifiedOn datetime,
	@ModifiedBy varchar(50)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE TB_M_CategoryNominative_ENUM SET 
		--[NominativeType] = @NominativeType,
		[CategoryCode] = @CategoryCode,
		[JointGroup] = @JointGroup,
		[GroupSeq] = @GroupSeq,
		[PositionSignature] = @PositionSignature,
		[ModifiedDate] = @ModifiedOn,
		[ModifiedBy] = @ModifiedBy
	WHERE [CategoryCode] = @CategoryCode


END

GO