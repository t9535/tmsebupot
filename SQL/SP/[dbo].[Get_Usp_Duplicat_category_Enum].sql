---$ Alter Procedure [dbo].[Get_Usp_Duplicat_category_Enum] 
IF OBJECT_ID(N'[dbo].[Get_Usp_Duplicat_category_Enum]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[Get_Usp_Duplicat_category_Enum]'
    EXECUTE('CREATE PROCEDURE [dbo].[Get_Usp_Duplicat_category_Enum] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[Get_Usp_Duplicat_category_Enum]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[Get_Usp_Duplicat_category_Enum]
	-- Add the parameters for the stored procedure here
	@category_code_old varchar(10)
	--,
	--@CategoryCode varchar(10)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	--select * from TB_M_Nominative_ENUM WHERE CategoryCode=@CategoryCode AND GLAccount=@GLAccount

	 select a.Category_Code as CategoryCode,count(*) as count
   from (
	SELECT    trph.CATEGORY_CODE AS Category_Code           
          FROM      ELVIS_DB.dbo.TB_R_PV_H AS trph
          WHERE     CATEGORY_CODE =@category_code_old
          UNION ALL
          SELECT    trdn.CategoryCode
          FROM      TAM_EFAKTUR.dbo.TB_R_Dashboard_Nominative AS trdn
          WHERE     trdn.CategoryCode =@category_code_old
		  Union ALL
		   SELECT    trdn.CategoryCode
          FROM      TAM_EFAKTUR.dbo.TB_M_DataField_ENUM AS trdn
          WHERE     trdn.CategoryCode =@category_code_old
		  Union ALL
		   SELECT    trdn.CategoryCode
          FROM      TAM_EFAKTUR.dbo.tb_m_question_enum AS trdn
          WHERE     trdn.CategoryCode =@category_code_old
		  Union ALL
		  SELECT    trdn.CategoryCode
          FROM      TAM_EFAKTUR.dbo.TB_M_AttachmentType_ENUM AS trdn
          WHERE     trdn.CategoryCode =@category_code_old
		  Union All
		  SELECT    trdn.CategoryCode
          FROM      TAM_EFAKTUR.dbo.TB_M_Nominative_ENUM AS trdn
          WHERE     trdn.CategoryCode =@category_code_old
		  ) as a
		  group by Category_Code
END

GO
