---$ Alter Procedure [dbo].[usp_CountQuestionaireListDashboard] 
IF OBJECT_ID(N'[dbo].[usp_CountQuestionaireListDashboard]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_CountQuestionaireListDashboard]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_CountQuestionaireListDashboard] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_CountQuestionaireListDashboard]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[usp_CountQuestionaireListDashboard]
	-- Add the parameters for the stored procedure here
	@NominativeType varchar(260) = '',
	@Question varchar(260) = ''
AS
BEGIN
DECLARE 
	@Query VARCHAR(MAX) = 'SELECT COUNT(b.NominativeType)
	FROM TB_M_Question_ENUM a
	inner join TB_M_CategoryNominative_ENUM as b on a.CategoryCode=b.CategoryCode where a.QuestionId is not null and 1=1',
	@QueryNominativeType varchar(MAX) = '',
	@QueryQuestion varchar(MAX) = ''

SELECT @QueryNominativeType = 
dbo.uf_LookupDynamicQueryGenerator(@NominativeType, 'NominativeType')

SELECT @QueryQuestion = 
dbo.uf_LookupDynamicQueryGenerator(@Question, 'Question')
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET @Query = 
		@Query + 
		@QueryNominativeType +
		@QueryQuestion 
		
EXEC(@Query)
--select @Query
	
END

GO
