---$ Alter Procedure [dbo].[usp_GetReportQuestionById_ENUM] 
IF OBJECT_ID(N'[dbo].[usp_GetReportQuestionById_ENUM]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[usp_GetReportQuestionById_ENUM]'
    EXECUTE('CREATE PROCEDURE [dbo].[usp_GetReportQuestionById_ENUM] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[usp_GetReportQuestionById_ENUM]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

ALTER PROCEDURE [dbo].[usp_GetReportQuestionById_ENUM]
	@SortBy int = 1,
	@SortDirection varchar(4) = 'DESC',
	@CategoryCode varchar(260) = '',
	@Question varchar(50)= '',
	@IsYes varchar(50)='',
	@IsNo varchar(50)= ''
	AS
BEGIN
	DECLARE @SortField varchar(50)
	SET @SortField = 
	CASE @SortBy 
    WHEN 1 THEN 'QuestionId'
	WHEN 2 THEN 'b.CategoryCode'
	WHEN 3 THEN 'Question'
	WHEN 4 THEN 'IsYes'
	WHEN 4 THEN 'IsNo'
	ELSE 'QuestionId' end; 
	DECLARE 
	@Query VARCHAR(MAX) = 'SELECT 
		ROW_NUMBER() OVER (ORDER BY '+ @SortField + ' ' + @SortDirection + ') AS RowNum,
		QuestionId,
		b.NominativeType as CategoryCode, 
		Question, 
		IsYes,
		IsNo,
		warningstatusyes,
		warningstatusno
	FROM dbo.TB_M_Question_ENUM as a 
	inner join TB_M_CategoryNominative_ENUM as b on a.CategoryCode=b.CategoryCode where a.QuestionId is not null ',
	@QueryCategoryCode varchar(MAX) = '',
	@QueryQuestion varchar(MAX) = '',
	@QueryIsYes varchar(MAX) = '',
	@QueryIsNo varchar(MAX) = ''
	
SELECT @QueryCategoryCode = 
dbo.uf_LookupDynamicQueryGenerator(@CategoryCode, 'a.CategoryCode')

SELECT @QueryQuestion = 
dbo.uf_LookupDynamicQueryGenerator(@Question, 'Question')

SELECT @QueryIsYes = 
dbo.uf_LookupDynamicQueryGenerator(@IsYes, 'IsYes')

SELECT @QueryIsNo = 
dbo.uf_LookupDynamicQueryGenerator(@IsNo, 'IsNo')


SET @Query = 'SELECT * FROM ('+ 
		@Query + 
		@QueryCategoryCode +
		@QueryQuestion +
		@QueryIsYes +
		@QueryIsNo+
		' ) AS TBL '
EXEC(@Query)
--Select @Query
END
--	@Condition varchar(max)
--AS
--begin
--declare @query varchar(max)
--set @query = 'SELECT 
--*
--	FROM dbo.TB_M_Question_ENUM where QuestionId in ( ' + @Condition+')'
--exec (@query)
--END

GO
