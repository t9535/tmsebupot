---$ Alter Procedure [dbo].[Usp_SyncDataFromELVIStoTMSFile] 
IF OBJECT_ID(N'[dbo].[Usp_SyncDataFromELVIStoTMSFile]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[Usp_SyncDataFromELVIStoTMSFile]'
    EXECUTE('CREATE PROCEDURE [dbo].[Usp_SyncDataFromELVIStoTMSFile] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[Usp_SyncDataFromELVIStoTMSFile]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[Usp_SyncDataFromELVIStoTMSFile] 
	-- Add the parameters for the stored procedure here
	@pvNumber int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
   --SELECT    trab.NominativeType ,
   --                 tra.[DIRECTORY] ,
   --                 tra.FILE_NAME ,
   --                 tra.ENOM_NO ,
   --                 tra.PV_NO,
			--		tra.FILE_TYPE,
			--		'0' NominativeLis
   --       FROM      ELVIS_DB.dbo.TB_R_ATTACHMENT AS tra
   --                 INNER JOIN dbo.TB_M_CategoryNominative_ENUM AS trab ON tra.CATEGORY_CODE = trab.CategoryCode
   --       WHERE     tra.FILE_TYPE = 'XLS'
   --                 AND tra.ATTACH_CD = 0
   --                 AND tra.IS_ENOMINATIVE = 1
   --                 AND tra.PV_NO = @pvNumber
   --       UNION ALL
          SELECT    trab.NominativeType ,
                    tra.[DIRECTORY] ,
                    tra.FILE_NAME ,
                    tra.ENOM_NO ,
                    tra.PV_NO,
					tra.FILE_TYPE,
					'1' NominativeLis
          FROM      ELVIS_DB.dbo.TB_R_ATTACHMENT AS tra
                    INNER JOIN dbo.TB_M_CategoryNominative_ENUM AS trab ON tra.CATEGORY_CODE = trab.CategoryCode
          WHERE     tra.ATTACH_CD = '3'
                    AND tra.PV_NO =@pvNumber
end

GO
