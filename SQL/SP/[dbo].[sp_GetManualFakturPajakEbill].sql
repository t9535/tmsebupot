
---$ Alter Procedure [dbo].[sp_GetManualFakturPajakEbill] 
IF OBJECT_ID(N'[dbo].[sp_GetManualFakturPajakEbill]') IS NULL
BEGIN
    PRINT 'Create procedure : [dbo].[sp_GetManualFakturPajakEbill]'
    EXECUTE('CREATE PROCEDURE [dbo].[sp_GetManualFakturPajakEbill] AS RETURN 0') 
END
GO

PRINT 'Alter procedure : [dbo].[sp_GetManualFakturPajakEbill]'
GO

SET QUOTED_IDENTIFIER ON 
go

SET ANSI_NULLS ON 
go


ALTER PROCEDURE [dbo].[sp_GetManualFakturPajakEbill]
@IdNuminative BIGINT
AS
    BEGIN 
        DECLARE @npwpcom VARCHAR(500)
        SELECT  @npwpcom = tmcd.ConfigValue
        FROM    dbo.TB_M_ConfigurationData AS tmcd
        WHERE   tmcd.ConfigKey = 'CompanyNPWP'

        SELECT  [FakturType] AS FakturType ,
                NomorFakturGabungan AS InvoiceNumberFull ,
                --FORMAT (TanggalFaktur, 'dd/MM/yyyy') AS InvoiceDate ,
                TanggalFaktur AS InvoiceDate,
                @npwpcom AS NPWPLawanTransaksi ,
                NPWPPenjual AS SupplierNPWP ,
                NamaPenjual AS SupplierName ,
                AlamatPenjual AS SupplierAddress ,
                CONVERT(DECIMAL(18,0),JumlahDPP) AS VATBaseAmount ,
                CONVERT(DECIMAL(18,0),JumlahPPN) AS VATAmount ,
                CONVERT(DECIMAL(18,0),JumlahPPNBM) AS JumlahPPnBM
        FROM    dbo.TB_R_VATIn
        WHERE   IdNuminativeEbill = @IdNuminative
    END

GO
