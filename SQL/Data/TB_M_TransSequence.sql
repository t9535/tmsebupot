/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2016 (13.0.1601)
    Source Database Engine Edition : Microsoft SQL Server Enterprise Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2017
    Target Database Engine Edition : Microsoft SQL Server Standard Edition
    Target Database Engine Type : Standalone SQL Server
*/

USE [TAM_EFAKTUR]
GO
INSERT [dbo].[TB_M_TransSequence] ([MSSequenceID], [BranchID], [SequenceName], [SequenceNo], [LengthNo], [ResetFlag], [Prefix], [IsBranch], [IsYear], [IsMonth], [Suffix], [UsrUpd], [DtmUpd]) VALUES (N'ND', N'   ', N'Enominative', 1, 6, N'N', N'ND', 0, 1, 1, N'', N'', CAST(N'2021-06-01T13:13:31.463' AS DateTime))
GO
INSERT [dbo].[TB_M_TransSequence] ([MSSequenceID], [BranchID], [SequenceName], [SequenceNo], [LengthNo], [ResetFlag], [Prefix], [IsBranch], [IsYear], [IsMonth], [Suffix], [UsrUpd], [DtmUpd]) VALUES (N'REF', N'   ', N'RefNumber', 1, 10, N' ', N'', 0, 0, 0, N'', N'', CAST(N'2021-06-14T16:18:39.117' AS DateTime))
GO
