USE [TAM_EFAKTUR]
GO
INSERT [dbo].[TB_M_CategoryNominative_ENUM] ([CategoryCode], [NominativeType], [JointGroup], [GroupSeq], [PositionSignature], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (N'enom1', N'Entertainment', N'Entertainment', 3, 0, N'efaktur.user', CAST(N'2021-04-01 16:39:38.000' AS DateTime), N'efaktur.user', CAST(N'2021-08-30 05:19:24.000' AS DateTime))
GO
INSERT [dbo].[TB_M_CategoryNominative_ENUM] ([CategoryCode], [NominativeType], [JointGroup], [GroupSeq], [PositionSignature], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (N'enom2', N'Promotion', N'Promotion', 2, 1, N'efaktur.user', CAST(N'2021-04-01 16:53:37.000' AS DateTime), N'efaktur.user', CAST(N'2021-07-21 15:04:25.000' AS DateTime))
GO
INSERT [dbo].[TB_M_CategoryNominative_ENUM] ([CategoryCode], [NominativeType], [JointGroup], [GroupSeq], [PositionSignature], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (N'enom3', N'Donation', N'Promotion', 1, 1, N'efaktur.user', CAST(N'2021-04-02 14:18:25.000' AS DateTime), N'efaktur.user', CAST(N'2021-08-30 04:17:04.000' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[TB_M_Nominative_ENUM] ON 

GO
INSERT [dbo].[TB_M_Nominative_ENUM] ([NominativeId], [CategoryCode], [GLAccount], [DescriptionTransaction], [Type], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (29, N'enom2', N'6110015', N'Sales Promotion Expense-General', N'Accrued & Reguler', N'efaktur.user', CAST(N'2021-04-28 12:20:30.913' AS DateTime), N'efaktur.user', CAST(N'2021-08-25 13:23:40.000' AS DateTime))
GO
INSERT [dbo].[TB_M_Nominative_ENUM] ([NominativeId], [CategoryCode], [GLAccount], [DescriptionTransaction], [Type], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (30, N'enom2', N'6110020', N'Sales Promotion Expense-Dealer Coordination Act', N'Accrued & Reguler', N'efaktur.user', CAST(N'2021-04-28 12:20:30.913' AS DateTime), N'efaktur.user', CAST(N'2021-08-25 13:24:29.000' AS DateTime))
GO
INSERT [dbo].[TB_M_Nominative_ENUM] ([NominativeId], [CategoryCode], [GLAccount], [DescriptionTransaction], [Type], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (33, N'enom2', N'6120020', N'Sales commision-Lxs', N'Accrued & Reguler', N'efaktur.user', CAST(N'2021-04-28 12:20:30.913' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[TB_M_Nominative_ENUM] ([NominativeId], [CategoryCode], [GLAccount], [DescriptionTransaction], [Type], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (34, N'enom2', N'6310030', N'After Sales Service-Campaign', N'Accrued & Reguler', N'efaktur.user', CAST(N'2021-04-28 12:20:30.913' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[TB_M_Nominative_ENUM] ([NominativeId], [CategoryCode], [GLAccount], [DescriptionTransaction], [Type], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (35, N'enom2', N'6310090', N'After Sales Service-Others', N'Accrued & Reguler', N'efaktur.user', CAST(N'2021-04-28 12:20:30.913' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[TB_M_Nominative_ENUM] ([NominativeId], [CategoryCode], [GLAccount], [DescriptionTransaction], [Type], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (36, N'enom3', N'6110010', N'Advertising Expenses-Sales Promotion', N'Accrued & Reguler', N'efaktur.user', CAST(N'2021-04-28 12:25:55.407' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[TB_M_Nominative_ENUM] ([NominativeId], [CategoryCode], [GLAccount], [DescriptionTransaction], [Type], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (37, N'enom3', N'6110015', N'Sales Promotion Expense-General', N'Accrued & Reguler', N'efaktur.user', CAST(N'2021-04-28 12:25:55.407' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[TB_M_Nominative_ENUM] ([NominativeId], [CategoryCode], [GLAccount], [DescriptionTransaction], [Type], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (38, N'enom3', N'6110020', N'Sales Promotion Expense-Dealer Coordination Act', N'Accrued & Reguler', N'efaktur.user', CAST(N'2021-04-28 12:25:55.407' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[TB_M_Nominative_ENUM] ([NominativeId], [CategoryCode], [GLAccount], [DescriptionTransaction], [Type], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (39, N'enom2', N'6110090', N'Advertising Expenses-Others', N'Accrued & Reguler', N'efaktur.user', CAST(N'2021-04-28 12:25:55.407' AS DateTime), N'efaktur.user', CAST(N'2021-07-05 16:09:52.000' AS DateTime))
GO
INSERT [dbo].[TB_M_Nominative_ENUM] ([NominativeId], [CategoryCode], [GLAccount], [DescriptionTransaction], [Type], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (40, N'enom3', N'6120010', N'Sales promotion-Lxs', N'Accrued & Reguler', N'efaktur.user', CAST(N'2021-04-28 12:25:55.407' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[TB_M_Nominative_ENUM] ([NominativeId], [CategoryCode], [GLAccount], [DescriptionTransaction], [Type], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (43, N'enom3', N'6310090', N'After Sales Service-Others', N'Accrued & Reguler', N'efaktur.user', CAST(N'2021-04-28 12:25:55.407' AS DateTime), N'efaktur.user', CAST(N'2021-06-25 08:56:40.000' AS DateTime))
GO
INSERT [dbo].[TB_M_Nominative_ENUM] ([NominativeId], [CategoryCode], [GLAccount], [DescriptionTransaction], [Type], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (49, N'enom3', N'6110090', N'Advertising Expenses-Others', N'Accrued & Reguler', N'efaktur.user', CAST(N'2021-04-30 11:06:26.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[TB_M_Nominative_ENUM] ([NominativeId], [CategoryCode], [GLAccount], [DescriptionTransaction], [Type], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (77, N'enom1', N'7990001', N'Entertainment', N'Accrued & Reguler', N'efaktur.user', CAST(N'2021-05-05 22:39:20.000' AS DateTime), N'efaktur.user', CAST(N'2021-05-06 10:19:56.000' AS DateTime))
GO
INSERT [dbo].[TB_M_Nominative_ENUM] ([NominativeId], [CategoryCode], [GLAccount], [DescriptionTransaction], [Type], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (10082, N'enom2', N'6110010', N'Advertising Expenses-Sales Promotion', N'Accrued & Reguler', N'efaktur.user', CAST(N'2021-05-20 12:45:22.000' AS DateTime), N'efaktur.user', CAST(N'2021-08-25 13:22:23.000' AS DateTime))
GO
INSERT [dbo].[TB_M_Nominative_ENUM] ([NominativeId], [CategoryCode], [GLAccount], [DescriptionTransaction], [Type], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (10083, N'enom2', N'6120010', N'Sales promotion-Lxs', N'Accrued & Reguler', N'efaktur.user', CAST(N'2021-05-20 12:52:10.000' AS DateTime), N'efaktur.user', CAST(N'2021-08-25 13:33:39.000' AS DateTime))
GO
INSERT [dbo].[TB_M_Nominative_ENUM] ([NominativeId], [CategoryCode], [GLAccount], [DescriptionTransaction], [Type], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (30125, N'enom3', N'6310030', N'After Sales Service-Campaign', N'Accrued & Reguler', N'efaktur.user', CAST(N'2021-07-21 15:11:38.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[TB_M_Nominative_ENUM] ([NominativeId], [CategoryCode], [GLAccount], [DescriptionTransaction], [Type], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (40146, N'enom3', N'6120020', N'Sales commision-Lxs', N'Accrued & Reguler', N'efaktur.user', CAST(N'2021-08-25 13:28:17.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[TB_M_Nominative_ENUM] ([NominativeId], [CategoryCode], [GLAccount], [DescriptionTransaction], [Type], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (40147, N'enom2', N'61000001', N'Sales Promo', N'Accrued & Reguler', N'efaktur.user', CAST(N'2021-08-28 16:57:57.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[TB_M_Nominative_ENUM] ([NominativeId], [CategoryCode], [GLAccount], [DescriptionTransaction], [Type], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (40148, N'enom2', N'63130', N'After Sales Service-Campaign 2', N'Accrued & Reguler', N'efaktur.user', CAST(N'2021-08-29 18:48:57.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[TB_M_Nominative_ENUM] ([NominativeId], [CategoryCode], [GLAccount], [DescriptionTransaction], [Type], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (40149, N'enom3', N'63130', N'After Sales Service-Campaign 2', N'Accrued & Reguler', N'efaktur.user', CAST(N'2021-08-29 18:50:52.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[TB_M_Nominative_ENUM] ([NominativeId], [CategoryCode], [GLAccount], [DescriptionTransaction], [Type], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (40150, N'enom1', N'63130', N'After Sales Service-Campaign 2', N'Accrued & Reguler', N'efaktur.user', CAST(N'2021-08-30 14:30:09.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[TB_M_Nominative_ENUM] ([NominativeId], [CategoryCode], [GLAccount], [DescriptionTransaction], [Type], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (40151, N'enom1', N'61190', N'Entertainment 1', N'Accrued & Reguler', N'efaktur.user', CAST(N'2021-08-30 14:33:39.000' AS DateTime), N'efaktur.user', CAST(N'2021-08-30 14:47:21.000' AS DateTime))
GO
INSERT [dbo].[TB_M_Nominative_ENUM] ([NominativeId], [CategoryCode], [GLAccount], [DescriptionTransaction], [Type], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (40152, N'enom2', N'61190', N'Promotion 1', N'Accrued & Reguler', N'efaktur.user', CAST(N'2021-08-30 14:34:00.000' AS DateTime), N'efaktur.user', CAST(N'2021-08-30 14:34:54.000' AS DateTime))
GO
INSERT [dbo].[TB_M_Nominative_ENUM] ([NominativeId], [CategoryCode], [GLAccount], [DescriptionTransaction], [Type], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (40153, N'enom3', N'79901', N'Donation 2', N'Accrued & Reguler', N'efaktur.user', CAST(N'2021-08-30 14:34:25.000' AS DateTime), NULL, NULL)
GO
INSERT [dbo].[TB_M_Nominative_ENUM] ([NominativeId], [CategoryCode], [GLAccount], [DescriptionTransaction], [Type], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (40154, N'enom2', N'79901', N'Promotion 2', N'Accrued & Reguler', N'efaktur.user', CAST(N'2021-08-30 14:34:41.000' AS DateTime), NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[TB_M_Nominative_ENUM] OFF
GO
SET IDENTITY_INSERT [dbo].[TB_M_DataField_ENUM] ON 

GO
INSERT [dbo].[TB_M_DataField_ENUM] ([DataFieldId], [CategoryCode], [FieldNameByTable], [FieldNameByExcel], [VendorGroup], [ModifiedBy], [ModifiedDate]) VALUES (42, N'enom2', N'NO', N'No', N'3;2', N'efaktur.user', CAST(N'2021-07-16 15:48:15.000' AS DateTime))
GO
INSERT [dbo].[TB_M_DataField_ENUM] ([DataFieldId], [CategoryCode], [FieldNameByTable], [FieldNameByExcel], [VendorGroup], [ModifiedBy], [ModifiedDate]) VALUES (43, N'enom2', N'NAMA', N'Nama', N'3;2;1;4', N'efaktur.user', CAST(N'2021-08-25 14:13:47.000' AS DateTime))
GO
INSERT [dbo].[TB_M_DataField_ENUM] ([DataFieldId], [CategoryCode], [FieldNameByTable], [FieldNameByExcel], [VendorGroup], [ModifiedBy], [ModifiedDate]) VALUES (44, N'enom2', N'NPWP', N'NPWP', N'3;2;1;4', N'efaktur.user', CAST(N'2021-08-25 14:13:13.000' AS DateTime))
GO
INSERT [dbo].[TB_M_DataField_ENUM] ([DataFieldId], [CategoryCode], [FieldNameByTable], [FieldNameByExcel], [VendorGroup], [ModifiedBy], [ModifiedDate]) VALUES (45, N'enom2', N'ALAMAT', N'Alamat', N'3;2;1;4', N'efaktur.user', CAST(N'2021-08-25 14:13:29.000' AS DateTime))
GO
INSERT [dbo].[TB_M_DataField_ENUM] ([DataFieldId], [CategoryCode], [FieldNameByTable], [FieldNameByExcel], [VendorGroup], [ModifiedBy], [ModifiedDate]) VALUES (46, N'enom2', N'TANGGAL', N'Tanggal', N'3;2;1;4', N'efaktur.user', CAST(N'2021-08-25 14:13:36.000' AS DateTime))
GO
INSERT [dbo].[TB_M_DataField_ENUM] ([DataFieldId], [CategoryCode], [FieldNameByTable], [FieldNameByExcel], [VendorGroup], [ModifiedBy], [ModifiedDate]) VALUES (47, N'enom2', N'BENTUK_DAN_JENIS_BIAYA', N'Bentuk dan Jenis Biaya', N'3;2;1;4', N'efaktur.user', CAST(N'2021-08-25 14:14:00.000' AS DateTime))
GO
INSERT [dbo].[TB_M_DataField_ENUM] ([DataFieldId], [CategoryCode], [FieldNameByTable], [FieldNameByExcel], [VendorGroup], [ModifiedBy], [ModifiedDate]) VALUES (48, N'enom2', N'JUMLAH', N'Jumlah', N'3;2;1;4', N'efaktur.user', CAST(N'2021-08-25 14:14:40.000' AS DateTime))
GO
INSERT [dbo].[TB_M_DataField_ENUM] ([DataFieldId], [CategoryCode], [FieldNameByTable], [FieldNameByExcel], [VendorGroup], [ModifiedBy], [ModifiedDate]) VALUES (49, N'enom2', N'JUMLAH_GROSS_UP', N'Jumlah Gross Up', N'2;4', N'efaktur.user', CAST(N'2021-08-25 16:42:21.000' AS DateTime))
GO
INSERT [dbo].[TB_M_DataField_ENUM] ([DataFieldId], [CategoryCode], [FieldNameByTable], [FieldNameByExcel], [VendorGroup], [ModifiedBy], [ModifiedDate]) VALUES (50, N'enom2', N'KETERANGAN', N'Keterangan', N'3;2;1;4', N'efaktur.user', CAST(N'2021-08-25 14:13:21.000' AS DateTime))
GO
INSERT [dbo].[TB_M_DataField_ENUM] ([DataFieldId], [CategoryCode], [FieldNameByTable], [FieldNameByExcel], [VendorGroup], [ModifiedBy], [ModifiedDate]) VALUES (51, N'enom2', N'JUMLAH_PPH', N'Jumlah PPh', N'3;2;1;4', N'efaktur.user', CAST(N'2021-08-25 14:14:09.000' AS DateTime))
GO
INSERT [dbo].[TB_M_DataField_ENUM] ([DataFieldId], [CategoryCode], [FieldNameByTable], [FieldNameByExcel], [VendorGroup], [ModifiedBy], [ModifiedDate]) VALUES (52, N'enom2', N'JENIS_PPH', N'Jenis PPh', N'3;2;1;4', N'efaktur.user', CAST(N'2021-08-25 14:14:20.000' AS DateTime))
GO
INSERT [dbo].[TB_M_DataField_ENUM] ([DataFieldId], [CategoryCode], [FieldNameByTable], [FieldNameByExcel], [VendorGroup], [ModifiedBy], [ModifiedDate]) VALUES (53, N'enom2', N'JUMLAH_NET', N'Jumlah Net', N'2;1;4', N'efaktur.user', CAST(N'2021-08-30 16:29:30.000' AS DateTime))
GO
INSERT [dbo].[TB_M_DataField_ENUM] ([DataFieldId], [CategoryCode], [FieldNameByTable], [FieldNameByExcel], [VendorGroup], [ModifiedBy], [ModifiedDate]) VALUES (54, N'enom2', N'NOMOR_REKENING', N'Nomor Rekening', N'2;4', N'efaktur.user', CAST(N'2021-08-25 16:42:40.000' AS DateTime))
GO
INSERT [dbo].[TB_M_DataField_ENUM] ([DataFieldId], [CategoryCode], [FieldNameByTable], [FieldNameByExcel], [VendorGroup], [ModifiedBy], [ModifiedDate]) VALUES (55, N'enom2', N'NAMA_REKENING_PENERIMA', N'Nama Rekening Penerima', N'2;4', N'efaktur.user', CAST(N'2021-08-25 16:43:20.000' AS DateTime))
GO
INSERT [dbo].[TB_M_DataField_ENUM] ([DataFieldId], [CategoryCode], [FieldNameByTable], [FieldNameByExcel], [VendorGroup], [ModifiedBy], [ModifiedDate]) VALUES (56, N'enom2', N'NAMA_BANK', N'Nama Bank', N'2;4', N'efaktur.user', CAST(N'2021-08-25 16:43:32.000' AS DateTime))
GO
INSERT [dbo].[TB_M_DataField_ENUM] ([DataFieldId], [CategoryCode], [FieldNameByTable], [FieldNameByExcel], [VendorGroup], [ModifiedBy], [ModifiedDate]) VALUES (58, N'enom1', N'NO', N'No', N'3;2;1;4', N'efaktur.user', CAST(N'2021-08-25 14:01:50.000' AS DateTime))
GO
INSERT [dbo].[TB_M_DataField_ENUM] ([DataFieldId], [CategoryCode], [FieldNameByTable], [FieldNameByExcel], [VendorGroup], [ModifiedBy], [ModifiedDate]) VALUES (59, N'enom1', N'TANGGAL', N'Tanggal Entertain', N'3;2;1;4', N'efaktur.user', CAST(N'2021-08-25 16:23:26.000' AS DateTime))
GO
INSERT [dbo].[TB_M_DataField_ENUM] ([DataFieldId], [CategoryCode], [FieldNameByTable], [FieldNameByExcel], [VendorGroup], [ModifiedBy], [ModifiedDate]) VALUES (60, N'enom1', N'TEMPAT', N'Tempat', N'3;4', NULL, NULL)
GO
INSERT [dbo].[TB_M_DataField_ENUM] ([DataFieldId], [CategoryCode], [FieldNameByTable], [FieldNameByExcel], [VendorGroup], [ModifiedBy], [ModifiedDate]) VALUES (61, N'enom1', N'ALAMAT', N'Alamat', N'3;2;1;4', N'efaktur.user', CAST(N'2021-08-30 05:41:45.000' AS DateTime))
GO
INSERT [dbo].[TB_M_DataField_ENUM] ([DataFieldId], [CategoryCode], [FieldNameByTable], [FieldNameByExcel], [VendorGroup], [ModifiedBy], [ModifiedDate]) VALUES (62, N'enom1', N'TIPE_ENTERTAINMENT', N'Bentuk dan Jenis Entertain', N'3;2;1;4', N'efaktur.user', CAST(N'2021-08-25 16:23:42.000' AS DateTime))
GO
INSERT [dbo].[TB_M_DataField_ENUM] ([DataFieldId], [CategoryCode], [FieldNameByTable], [FieldNameByExcel], [VendorGroup], [ModifiedBy], [ModifiedDate]) VALUES (63, N'enom1', N'JUMLAH', N'Jumlah (Rp)', N'3;2;1;4', N'efaktur.user', CAST(N'2021-08-25 16:22:02.000' AS DateTime))
GO
INSERT [dbo].[TB_M_DataField_ENUM] ([DataFieldId], [CategoryCode], [FieldNameByTable], [FieldNameByExcel], [VendorGroup], [ModifiedBy], [ModifiedDate]) VALUES (64, N'enom1', N'NAMA_RELASI', N'Nama Relasi', N'3;2;1;4', N'efaktur.user', CAST(N'2021-08-25 16:24:47.000' AS DateTime))
GO
INSERT [dbo].[TB_M_DataField_ENUM] ([DataFieldId], [CategoryCode], [FieldNameByTable], [FieldNameByExcel], [VendorGroup], [ModifiedBy], [ModifiedDate]) VALUES (65, N'enom1', N'JABATAN', N'Jabatan', N'3;2;1;4', N'efaktur.user', CAST(N'2021-08-25 16:21:50.000' AS DateTime))
GO
INSERT [dbo].[TB_M_DataField_ENUM] ([DataFieldId], [CategoryCode], [FieldNameByTable], [FieldNameByExcel], [VendorGroup], [ModifiedBy], [ModifiedDate]) VALUES (66, N'enom1', N'ALAMAT_PERUSAHAAN', N'Perusahaan Relasi', N'3;2;1;4', N'efaktur.user', CAST(N'2021-08-30 05:43:13.000' AS DateTime))
GO
INSERT [dbo].[TB_M_DataField_ENUM] ([DataFieldId], [CategoryCode], [FieldNameByTable], [FieldNameByExcel], [VendorGroup], [ModifiedBy], [ModifiedDate]) VALUES (67, N'enom1', N'TIPE_BISNIS', N'Jenis Usaha Relasi', N'3;2;1;4', N'efaktur.user', CAST(N'2021-08-25 16:24:14.000' AS DateTime))
GO
INSERT [dbo].[TB_M_DataField_ENUM] ([DataFieldId], [CategoryCode], [FieldNameByTable], [FieldNameByExcel], [VendorGroup], [ModifiedBy], [ModifiedDate]) VALUES (68, N'enom3', N'JENIS_SUMBANGAN', N'Jenis Sumbangan', N'2;4', N'efaktur.user', CAST(N'2021-08-29 05:41:42.000' AS DateTime))
GO
INSERT [dbo].[TB_M_DataField_ENUM] ([DataFieldId], [CategoryCode], [FieldNameByTable], [FieldNameByExcel], [VendorGroup], [ModifiedBy], [ModifiedDate]) VALUES (69, N'enom3', N'BENTUK_SUMBANGAN', N'Bentuk Sumbangan', N'3;2;1;4', N'efaktur.user', CAST(N'2021-08-25 14:16:30.000' AS DateTime))
GO
INSERT [dbo].[TB_M_DataField_ENUM] ([DataFieldId], [CategoryCode], [FieldNameByTable], [FieldNameByExcel], [VendorGroup], [ModifiedBy], [ModifiedDate]) VALUES (70, N'enom3', N'NILAI_SUMBANGAN', N'Nilai Sumbangan', N'3;2;1;4', N'efaktur.user', CAST(N'2021-08-25 14:16:38.000' AS DateTime))
GO
INSERT [dbo].[TB_M_DataField_ENUM] ([DataFieldId], [CategoryCode], [FieldNameByTable], [FieldNameByExcel], [VendorGroup], [ModifiedBy], [ModifiedDate]) VALUES (71, N'enom3', N'TANGGAL_SUMBANGAN', N'Tanggal Sumbangan', N'3;2;1;4', N'efaktur.user', CAST(N'2021-08-25 14:16:46.000' AS DateTime))
GO
INSERT [dbo].[TB_M_DataField_ENUM] ([DataFieldId], [CategoryCode], [FieldNameByTable], [FieldNameByExcel], [VendorGroup], [ModifiedBy], [ModifiedDate]) VALUES (72, N'enom3', N'NAMA_LEMBAGA_PENERIMA', N'Nama Lembaga Penerima', N'3;2;1;4', N'efaktur.user', CAST(N'2021-08-25 14:16:55.000' AS DateTime))
GO
INSERT [dbo].[TB_M_DataField_ENUM] ([DataFieldId], [CategoryCode], [FieldNameByTable], [FieldNameByExcel], [VendorGroup], [ModifiedBy], [ModifiedDate]) VALUES (73, N'enom3', N'NPWP_LEMBAGA_PENERIMA', N'NPWP Lembaga Penerima', N'3;2;1;4', N'efaktur.user', CAST(N'2021-08-25 14:17:10.000' AS DateTime))
GO
INSERT [dbo].[TB_M_DataField_ENUM] ([DataFieldId], [CategoryCode], [FieldNameByTable], [FieldNameByExcel], [VendorGroup], [ModifiedBy], [ModifiedDate]) VALUES (74, N'enom3', N'ALAMAT_LEMBAGA', N'Alamat Lembaga', N'3;2;1;4', N'efaktur.user', CAST(N'2021-08-25 14:17:19.000' AS DateTime))
GO
INSERT [dbo].[TB_M_DataField_ENUM] ([DataFieldId], [CategoryCode], [FieldNameByTable], [FieldNameByExcel], [VendorGroup], [ModifiedBy], [ModifiedDate]) VALUES (75, N'enom3', N'NO_TELP_LEMBAGA', N'No Telp Lembaga', N'3;2;1;4', N'efaktur.user', CAST(N'2021-08-25 14:17:26.000' AS DateTime))
GO
INSERT [dbo].[TB_M_DataField_ENUM] ([DataFieldId], [CategoryCode], [FieldNameByTable], [FieldNameByExcel], [VendorGroup], [ModifiedBy], [ModifiedDate]) VALUES (76, N'enom3', N'SARANA_PRASARANA_INFRASTRUKTUR', N'Sarana Prasarana Infrastruktur Yang Diberikan', N'3;2;1;4', N'efaktur.user', CAST(N'2021-08-25 14:17:40.000' AS DateTime))
GO
INSERT [dbo].[TB_M_DataField_ENUM] ([DataFieldId], [CategoryCode], [FieldNameByTable], [FieldNameByExcel], [VendorGroup], [ModifiedBy], [ModifiedDate]) VALUES (77, N'enom3', N'LOKASI_INFRASTRUKTUR', N'Lokasi Infrastruktur', N'3;2;1;4', N'efaktur.user', CAST(N'2021-08-25 14:17:49.000' AS DateTime))
GO
INSERT [dbo].[TB_M_DataField_ENUM] ([DataFieldId], [CategoryCode], [FieldNameByTable], [FieldNameByExcel], [VendorGroup], [ModifiedBy], [ModifiedDate]) VALUES (78, N'enom3', N'IZIN_MENDIRIKAN_BANGUNAN', N'Izin Mendirikan Bangunan Infrastruktur', N'3;2;1;4', N'efaktur.user', CAST(N'2021-08-25 14:18:10.000' AS DateTime))
GO
INSERT [dbo].[TB_M_DataField_ENUM] ([DataFieldId], [CategoryCode], [FieldNameByTable], [FieldNameByExcel], [VendorGroup], [ModifiedBy], [ModifiedDate]) VALUES (79, N'enom3', N'NOMOR_REKENING', N'Nomor Rekening', N'3;2;1;4', N'efaktur.user', CAST(N'2021-08-25 14:18:23.000' AS DateTime))
GO
INSERT [dbo].[TB_M_DataField_ENUM] ([DataFieldId], [CategoryCode], [FieldNameByTable], [FieldNameByExcel], [VendorGroup], [ModifiedBy], [ModifiedDate]) VALUES (80, N'enom3', N'NAMA_REKENING_PENERIMA', N'Nama Rekening Penerima', N'3;2;1;4', N'efaktur.user', CAST(N'2021-08-25 14:18:33.000' AS DateTime))
GO
INSERT [dbo].[TB_M_DataField_ENUM] ([DataFieldId], [CategoryCode], [FieldNameByTable], [FieldNameByExcel], [VendorGroup], [ModifiedBy], [ModifiedDate]) VALUES (81, N'enom3', N'NAMA_BANK', N'Nama Bank', N'3;2;1;4', N'efaktur.user', CAST(N'2021-08-25 14:18:42.000' AS DateTime))
GO
INSERT [dbo].[TB_M_DataField_ENUM] ([DataFieldId], [CategoryCode], [FieldNameByTable], [FieldNameByExcel], [VendorGroup], [ModifiedBy], [ModifiedDate]) VALUES (83, N'enom1', N'KETERANGAN', N'Keterangan', N'3;2;1;4', N'efaktur.user', CAST(N'2021-08-25 14:09:51.000' AS DateTime))
GO
INSERT [dbo].[TB_M_DataField_ENUM] ([DataFieldId], [CategoryCode], [FieldNameByTable], [FieldNameByExcel], [VendorGroup], [ModifiedBy], [ModifiedDate]) VALUES (84, N'enom3', N'BIAYA_PEMBANGUNAN_INFRASTRUKTUR', N'Biaya Pembangunan Infrastruktur', N'3;2;1;4', N'efaktur.user', CAST(N'2021-08-25 14:18:51.000' AS DateTime))
GO
INSERT [dbo].[TB_M_DataField_ENUM] ([DataFieldId], [CategoryCode], [FieldNameByTable], [FieldNameByExcel], [VendorGroup], [ModifiedBy], [ModifiedDate]) VALUES (85, N'enom2', N'NO_KTP', N'No KTP', N'2;4', N'efaktur.user', CAST(N'2021-08-25 16:43:45.000' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[TB_M_DataField_ENUM] OFF
GO
SET IDENTITY_INSERT [dbo].[TB_M_AttachmentType_ENUM] ON 

GO
INSERT [dbo].[TB_M_AttachmentType_ENUM] ([AttachmentId], [CategoryCode], [Description], [FileType], [Vendor_Group], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [CategoryDesc], [FileTypeDesc], [Vendor_GroupDesc], [Ebiling], [Elvis]) VALUES (36, N'enom1', N'Photo/Bukti Serah Terima Elvis', N'1;2;3;4', N'1;2;3;4', NULL, CAST(N'2021-05-03 10:34:31.153' AS DateTime), N'efaktur.user', CAST(N'2021-08-25 13:37:00.673' AS DateTime), N'Entertainment', N'Excel;PDF;Word;Picture (jpg.png)', N'Internal-Individual;Internal-Division;External;One Time Vendor', 0, 1)
GO
INSERT [dbo].[TB_M_AttachmentType_ENUM] ([AttachmentId], [CategoryCode], [Description], [FileType], [Vendor_Group], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [CategoryDesc], [FileTypeDesc], [Vendor_GroupDesc], [Ebiling], [Elvis]) VALUES (64, N'enom2', N'Invoice/Kwitansi Elvis', N'2;4', N'1;2;3;4', NULL, CAST(N'2021-05-06 00:15:42.443' AS DateTime), N'efaktur.user', CAST(N'2021-08-25 13:42:51.483' AS DateTime), N'Promotion', N'PDF;Picture (jpg.png)', N'Internal-Individual;Internal-Division;External;One Time Vendor', 0, 1)
GO
INSERT [dbo].[TB_M_AttachmentType_ENUM] ([AttachmentId], [CategoryCode], [Description], [FileType], [Vendor_Group], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [CategoryDesc], [FileTypeDesc], [Vendor_GroupDesc], [Ebiling], [Elvis]) VALUES (78, N'enom2', N'Nominative Form Approved Elvis', N'1;2;3;4', N'1;2;3;4', NULL, CAST(N'2021-05-06 11:33:06.980' AS DateTime), N'efaktur.user', CAST(N'2021-08-25 13:42:11.900' AS DateTime), N'Promotion', N'Excel;PDF;Word;Picture (jpg.png)', N'Internal-Individual;Internal-Division;External;One Time Vendor', 0, 1)
GO
INSERT [dbo].[TB_M_AttachmentType_ENUM] ([AttachmentId], [CategoryCode], [Description], [FileType], [Vendor_Group], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [CategoryDesc], [FileTypeDesc], [Vendor_GroupDesc], [Ebiling], [Elvis]) VALUES (83, N'enom2', N'Bukti dan/atau Daftar Pemberian Promosi Elvis', N'1;2;3;4', N'1;2;3;4', NULL, CAST(N'2021-05-06 13:26:33.030' AS DateTime), N'efaktur.user', CAST(N'2021-08-25 13:44:13.670' AS DateTime), N'Promotion', N'Excel;PDF;Word;Picture (jpg.png)', N'Internal-Individual;Internal-Division;External;One Time Vendor', 0, 1)
GO
INSERT [dbo].[TB_M_AttachmentType_ENUM] ([AttachmentId], [CategoryCode], [Description], [FileType], [Vendor_Group], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [CategoryDesc], [FileTypeDesc], [Vendor_GroupDesc], [Ebiling], [Elvis]) VALUES (20125, N'enom1', N'Invoice/Billing Elvis', N'1;2;3;4', N'1;2;3;4', NULL, CAST(N'2021-06-25 15:44:18.077' AS DateTime), N'efaktur.user', CAST(N'2021-08-25 13:36:53.447' AS DateTime), N'Entertainment', N'Excel;PDF;Word;Picture (jpg.png)', N'Internal-Individual;Internal-Division;External;One Time Vendor', 0, 1)
GO
INSERT [dbo].[TB_M_AttachmentType_ENUM] ([AttachmentId], [CategoryCode], [Description], [FileType], [Vendor_Group], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [CategoryDesc], [FileTypeDesc], [Vendor_GroupDesc], [Ebiling], [Elvis]) VALUES (30142, N'enom2', N'Proposal Elvis', N'1;2;3;4', N'1;2;3;4', NULL, CAST(N'2021-07-02 22:07:05.770' AS DateTime), N'efaktur.user', CAST(N'2021-08-25 13:44:36.663' AS DateTime), N'Promotion', N'Excel;PDF;Word;Picture (jpg.png)', N'Internal-Individual;Internal-Division;External;One Time Vendor', 0, 1)
GO
INSERT [dbo].[TB_M_AttachmentType_ENUM] ([AttachmentId], [CategoryCode], [Description], [FileType], [Vendor_Group], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [CategoryDesc], [FileTypeDesc], [Vendor_GroupDesc], [Ebiling], [Elvis]) VALUES (30143, N'enom1', N'Nominative Form Approved Elvis', N'1;2;3;4', N'1;2;3;4', NULL, CAST(N'2021-07-21 16:17:39.673' AS DateTime), N'efaktur.user', CAST(N'2021-08-25 13:36:45.393' AS DateTime), N'Entertainment', N'Excel;PDF;Word;Picture (jpg.png)', N'Internal-Individual;Internal-Division;External;One Time Vendor', 0, 1)
GO
INSERT [dbo].[TB_M_AttachmentType_ENUM] ([AttachmentId], [CategoryCode], [Description], [FileType], [Vendor_Group], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [CategoryDesc], [FileTypeDesc], [Vendor_GroupDesc], [Ebiling], [Elvis]) VALUES (30144, N'enom1', N'Entertain Doc Sign By Director Elvis', N'1;2;3;4', N'1;2;3;4', NULL, CAST(N'2021-07-21 16:26:38.793' AS DateTime), N'efaktur.user', CAST(N'2021-08-25 13:36:38.107' AS DateTime), N'Entertainment', N'Excel;PDF;Word;Picture (jpg.png)', N'Internal-Individual;Internal-Division;External;One Time Vendor', 0, 1)
GO
INSERT [dbo].[TB_M_AttachmentType_ENUM] ([AttachmentId], [CategoryCode], [Description], [FileType], [Vendor_Group], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [CategoryDesc], [FileTypeDesc], [Vendor_GroupDesc], [Ebiling], [Elvis]) VALUES (30145, N'enom3', N'Bukti Serah Terima Elvis', N'1;2;3;4', N'1;2;3;4', NULL, CAST(N'2021-07-21 16:36:40.677' AS DateTime), N'efaktur.user', CAST(N'2021-08-25 13:50:55.860' AS DateTime), N'Donation', N'Excel;PDF;Word;Picture (jpg.png)', N'Internal-Individual;Internal-Division;External;One Time Vendor', 0, 1)
GO
INSERT [dbo].[TB_M_AttachmentType_ENUM] ([AttachmentId], [CategoryCode], [Description], [FileType], [Vendor_Group], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [CategoryDesc], [FileTypeDesc], [Vendor_GroupDesc], [Ebiling], [Elvis]) VALUES (30151, N'enom2', N'Agreement/Contract Elvis', N'1;2;3;4', N'1;2;3;4', NULL, CAST(N'2021-07-26 09:49:32.820' AS DateTime), N'efaktur.user', CAST(N'2021-08-25 13:43:25.910' AS DateTime), N'Promotion', N'Excel;PDF;Word;Picture (jpg.png)', N'Internal-Individual;Internal-Division;External;One Time Vendor', 0, 1)
GO
INSERT [dbo].[TB_M_AttachmentType_ENUM] ([AttachmentId], [CategoryCode], [Description], [FileType], [Vendor_Group], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [CategoryDesc], [FileTypeDesc], [Vendor_GroupDesc], [Ebiling], [Elvis]) VALUES (30170, N'enom1', N'Nominative Form Approved Ebilling', N'1;2;3;4', N'1;2;3;4', NULL, CAST(N'2021-08-25 13:37:42.143' AS DateTime), NULL, NULL, N'Entertainment', N'Excel;PDF;Word;Picture (jpg.png)', N'Internal-Individual;Internal-Division;External;One Time Vendor', 1, 0)
GO
INSERT [dbo].[TB_M_AttachmentType_ENUM] ([AttachmentId], [CategoryCode], [Description], [FileType], [Vendor_Group], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [CategoryDesc], [FileTypeDesc], [Vendor_GroupDesc], [Ebiling], [Elvis]) VALUES (30171, N'enom1', N'Invoice/Billing Ebilling', N'1;2;3;4', N'1;2;3;4', NULL, CAST(N'2021-08-25 13:38:41.163' AS DateTime), NULL, NULL, N'Entertainment', N'Excel;PDF;Word;Picture (jpg.png)', N'Internal-Individual;Internal-Division;External;One Time Vendor', 1, 0)
GO
INSERT [dbo].[TB_M_AttachmentType_ENUM] ([AttachmentId], [CategoryCode], [Description], [FileType], [Vendor_Group], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [CategoryDesc], [FileTypeDesc], [Vendor_GroupDesc], [Ebiling], [Elvis]) VALUES (30172, N'enom1', N'Entertain Doc Sign By Director Ebilling', N'1;2;3;4', N'1;2;3;4', NULL, CAST(N'2021-08-25 13:39:14.690' AS DateTime), NULL, NULL, N'Entertainment', N'Excel;PDF;Word;Picture (jpg.png)', N'Internal-Individual;Internal-Division;External;One Time Vendor', 1, 0)
GO
INSERT [dbo].[TB_M_AttachmentType_ENUM] ([AttachmentId], [CategoryCode], [Description], [FileType], [Vendor_Group], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [CategoryDesc], [FileTypeDesc], [Vendor_GroupDesc], [Ebiling], [Elvis]) VALUES (30173, N'enom1', N'Photo/Bukti Serah Terima', N'1;2;3;4', N'1;2;3;4', NULL, CAST(N'2021-08-25 13:39:40.570' AS DateTime), NULL, NULL, N'Entertainment', N'Excel;PDF;Word;Picture (jpg.png)', N'Internal-Individual;Internal-Division;External;One Time Vendor', 1, 0)
GO
INSERT [dbo].[TB_M_AttachmentType_ENUM] ([AttachmentId], [CategoryCode], [Description], [FileType], [Vendor_Group], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [CategoryDesc], [FileTypeDesc], [Vendor_GroupDesc], [Ebiling], [Elvis]) VALUES (30174, N'enom2', N'Photo Nomor Rekening-Buku Tabungan Elvis', N'1;2;3;4', N'1;2;3;4', NULL, CAST(N'2021-08-25 13:45:34.433' AS DateTime), N'efaktur.user', CAST(N'2021-08-25 13:50:11.753' AS DateTime), N'Promotion', N'Excel;PDF;Word;Picture (jpg.png)', N'Internal-Individual;Internal-Division;External;One Time Vendor', 0, 1)
GO
INSERT [dbo].[TB_M_AttachmentType_ENUM] ([AttachmentId], [CategoryCode], [Description], [FileType], [Vendor_Group], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [CategoryDesc], [FileTypeDesc], [Vendor_GroupDesc], [Ebiling], [Elvis]) VALUES (30175, N'enom2', N'Agreement/Contract Ebilling', N'1;2;3;4', N'1;2;3;4', NULL, CAST(N'2021-08-25 13:46:27.890' AS DateTime), NULL, NULL, N'Promotion', N'Excel;PDF;Word;Picture (jpg.png)', N'Internal-Individual;Internal-Division;External;One Time Vendor', 1, 0)
GO
INSERT [dbo].[TB_M_AttachmentType_ENUM] ([AttachmentId], [CategoryCode], [Description], [FileType], [Vendor_Group], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [CategoryDesc], [FileTypeDesc], [Vendor_GroupDesc], [Ebiling], [Elvis]) VALUES (30176, N'enom2', N'Bukti dan/atau Daftar Pemberian Promosi Ebilling', N'1;2;3;4', N'1;2;3;4', NULL, CAST(N'2021-08-25 13:47:04.270' AS DateTime), NULL, NULL, N'Promotion', N'Excel;PDF;Word;Picture (jpg.png)', N'Internal-Individual;Internal-Division;External;One Time Vendor', 1, 0)
GO
INSERT [dbo].[TB_M_AttachmentType_ENUM] ([AttachmentId], [CategoryCode], [Description], [FileType], [Vendor_Group], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [CategoryDesc], [FileTypeDesc], [Vendor_GroupDesc], [Ebiling], [Elvis]) VALUES (30177, N'enom2', N'Invoice/Kwitansi Ebilling', N'1;2;3;4', N'1;2;3;4', NULL, CAST(N'2021-08-25 13:47:32.723' AS DateTime), NULL, NULL, N'Promotion', N'Excel;PDF;Word;Picture (jpg.png)', N'Internal-Individual;Internal-Division;External;One Time Vendor', 1, 0)
GO
INSERT [dbo].[TB_M_AttachmentType_ENUM] ([AttachmentId], [CategoryCode], [Description], [FileType], [Vendor_Group], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [CategoryDesc], [FileTypeDesc], [Vendor_GroupDesc], [Ebiling], [Elvis]) VALUES (30178, N'enom2', N'Nominative Form Approved Ebilling', N'1;2;3;4', N'1;2;3;4', NULL, CAST(N'2021-08-25 13:48:00.010' AS DateTime), NULL, NULL, N'Promotion', N'Excel;PDF;Word;Picture (jpg.png)', N'Internal-Individual;Internal-Division;External;One Time Vendor', 1, 0)
GO
INSERT [dbo].[TB_M_AttachmentType_ENUM] ([AttachmentId], [CategoryCode], [Description], [FileType], [Vendor_Group], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [CategoryDesc], [FileTypeDesc], [Vendor_GroupDesc], [Ebiling], [Elvis]) VALUES (30179, N'enom2', N'Photo Nomor Rekening-Buku Tabungan Ebilling', N'1;2;3;4', N'1;2;3;4', NULL, CAST(N'2021-08-25 13:48:30.750' AS DateTime), N'efaktur.user', CAST(N'2021-08-25 13:50:02.987' AS DateTime), N'Promotion', N'Excel;PDF;Word;Picture (jpg.png)', N'Internal-Individual;Internal-Division;External;One Time Vendor', 1, 0)
GO
INSERT [dbo].[TB_M_AttachmentType_ENUM] ([AttachmentId], [CategoryCode], [Description], [FileType], [Vendor_Group], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [CategoryDesc], [FileTypeDesc], [Vendor_GroupDesc], [Ebiling], [Elvis]) VALUES (30180, N'enom2', N'Proposal Ebilling', N'1;2;3;4', N'1;2;3;4', NULL, CAST(N'2021-08-25 13:48:57.407' AS DateTime), NULL, NULL, N'Promotion', N'Excel;PDF;Word;Picture (jpg.png)', N'Internal-Individual;Internal-Division;External;One Time Vendor', 1, 0)
GO
INSERT [dbo].[TB_M_AttachmentType_ENUM] ([AttachmentId], [CategoryCode], [Description], [FileType], [Vendor_Group], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [CategoryDesc], [FileTypeDesc], [Vendor_GroupDesc], [Ebiling], [Elvis]) VALUES (30181, N'enom3', N'Bukti Serah Terima Ebilling', N'1;2;3;4', N'1;2;3;4', NULL, CAST(N'2021-08-25 13:51:19.247' AS DateTime), NULL, NULL, N'Donation', N'Excel;PDF;Word;Picture (jpg.png)', N'Internal-Individual;Internal-Division;External;One Time Vendor', 1, 0)
GO
INSERT [dbo].[TB_M_AttachmentType_ENUM] ([AttachmentId], [CategoryCode], [Description], [FileType], [Vendor_Group], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [CategoryDesc], [FileTypeDesc], [Vendor_GroupDesc], [Ebiling], [Elvis]) VALUES (30182, N'enom3', N'Nominative Form Approved Ebilling', N'1;2;3;4', N'1;2;3;4', NULL, CAST(N'2021-08-25 13:51:50.237' AS DateTime), NULL, NULL, N'Donation', N'Excel;PDF;Word;Picture (jpg.png)', N'Internal-Individual;Internal-Division;External;One Time Vendor', 1, 0)
GO
INSERT [dbo].[TB_M_AttachmentType_ENUM] ([AttachmentId], [CategoryCode], [Description], [FileType], [Vendor_Group], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [CategoryDesc], [FileTypeDesc], [Vendor_GroupDesc], [Ebiling], [Elvis]) VALUES (30183, N'enom3', N'Nominative Form Approved Elvis', N'1;2;3;4', N'1;2;3;4', NULL, CAST(N'2021-08-25 13:52:09.370' AS DateTime), NULL, NULL, N'Donation', N'Excel;PDF;Word;Picture (jpg.png)', N'Internal-Individual;Internal-Division;External;One Time Vendor', 0, 1)
GO
INSERT [dbo].[TB_M_AttachmentType_ENUM] ([AttachmentId], [CategoryCode], [Description], [FileType], [Vendor_Group], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [CategoryDesc], [FileTypeDesc], [Vendor_GroupDesc], [Ebiling], [Elvis]) VALUES (30184, N'enom3', N'Invoice/Kwitansi Ebilling', N'1;2;3;4', N'1;2;3;4', NULL, CAST(N'2021-08-25 13:52:34.363' AS DateTime), NULL, NULL, N'Donation', N'Excel;PDF;Word;Picture (jpg.png)', N'Internal-Individual;Internal-Division;External;One Time Vendor', 1, 0)
GO
INSERT [dbo].[TB_M_AttachmentType_ENUM] ([AttachmentId], [CategoryCode], [Description], [FileType], [Vendor_Group], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [CategoryDesc], [FileTypeDesc], [Vendor_GroupDesc], [Ebiling], [Elvis]) VALUES (30185, N'enom3', N'Invoice/Kwitansi Elvis', N'1;2;3;4', N'1;2;3;4', NULL, CAST(N'2021-08-25 13:52:59.037' AS DateTime), N'efaktur.user', CAST(N'2021-08-25 13:53:35.630' AS DateTime), N'Donation', N'Excel;PDF;Word;Picture (jpg.png)', N'Internal-Individual;Internal-Division;External;One Time Vendor', 0, 1)
GO
INSERT [dbo].[TB_M_AttachmentType_ENUM] ([AttachmentId], [CategoryCode], [Description], [FileType], [Vendor_Group], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [CategoryDesc], [FileTypeDesc], [Vendor_GroupDesc], [Ebiling], [Elvis]) VALUES (30186, N'enom3', N'Agreement/Contract Ebilling', N'1;2;3;4', N'1;2;3;4', NULL, CAST(N'2021-08-25 13:53:30.013' AS DateTime), NULL, NULL, N'Donation', N'Excel;PDF;Word;Picture (jpg.png)', N'Internal-Individual;Internal-Division;External;One Time Vendor', 1, 0)
GO
INSERT [dbo].[TB_M_AttachmentType_ENUM] ([AttachmentId], [CategoryCode], [Description], [FileType], [Vendor_Group], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [CategoryDesc], [FileTypeDesc], [Vendor_GroupDesc], [Ebiling], [Elvis]) VALUES (30187, N'enom3', N'Agreement/Contract Elvis', N'1;2;3;4', N'1;2;3;4', NULL, CAST(N'2021-08-25 13:54:20.323' AS DateTime), NULL, NULL, N'Donation', N'Excel;PDF;Word;Picture (jpg.png)', N'Internal-Individual;Internal-Division;External;One Time Vendor', 0, 1)
GO
INSERT [dbo].[TB_M_AttachmentType_ENUM] ([AttachmentId], [CategoryCode], [Description], [FileType], [Vendor_Group], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [CategoryDesc], [FileTypeDesc], [Vendor_GroupDesc], [Ebiling], [Elvis]) VALUES (30188, N'enom3', N'Surat Izin Resmi Badan Yang ditunjuk Pemerintah Penerima Sumbangan Ebilling', N'1;2;3;4', N'1;2;3;4', NULL, CAST(N'2021-08-25 13:55:23.800' AS DateTime), NULL, NULL, N'Donation', N'Excel;PDF;Word;Picture (jpg.png)', N'Internal-Individual;Internal-Division;External;One Time Vendor', 1, 0)
GO
INSERT [dbo].[TB_M_AttachmentType_ENUM] ([AttachmentId], [CategoryCode], [Description], [FileType], [Vendor_Group], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [CategoryDesc], [FileTypeDesc], [Vendor_GroupDesc], [Ebiling], [Elvis]) VALUES (30189, N'enom3', N'Surat Izin Resmi Badan Yang ditunjuk Pemerintah Penerima Sumbangan Elvis', N'1;2;3;4', N'1;2;3;4', NULL, CAST(N'2021-08-25 13:55:46.190' AS DateTime), NULL, NULL, N'Donation', N'Excel;PDF;Word;Picture (jpg.png)', N'Internal-Individual;Internal-Division;External;One Time Vendor', 0, 1)
GO
INSERT [dbo].[TB_M_AttachmentType_ENUM] ([AttachmentId], [CategoryCode], [Description], [FileType], [Vendor_Group], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [CategoryDesc], [FileTypeDesc], [Vendor_GroupDesc], [Ebiling], [Elvis]) VALUES (30190, N'enom3', N'Proposal Ebilling', N'1;2;3;4', N'1;2;3;4', NULL, CAST(N'2021-08-25 13:56:08.130' AS DateTime), NULL, NULL, N'Donation', N'Excel;PDF;Word;Picture (jpg.png)', N'Internal-Individual;Internal-Division;External;One Time Vendor', 1, 0)
GO
INSERT [dbo].[TB_M_AttachmentType_ENUM] ([AttachmentId], [CategoryCode], [Description], [FileType], [Vendor_Group], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [CategoryDesc], [FileTypeDesc], [Vendor_GroupDesc], [Ebiling], [Elvis]) VALUES (30191, N'enom3', N'Proposal Elvis', N'1;2;3;4', N'1;2;3;4', NULL, CAST(N'2021-08-25 13:56:27.917' AS DateTime), NULL, NULL, N'Donation', N'Excel;PDF;Word;Picture (jpg.png)', N'Internal-Individual;Internal-Division;External;One Time Vendor', 0, 1)
GO
INSERT [dbo].[TB_M_AttachmentType_ENUM] ([AttachmentId], [CategoryCode], [Description], [FileType], [Vendor_Group], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [CategoryDesc], [FileTypeDesc], [Vendor_GroupDesc], [Ebiling], [Elvis]) VALUES (30192, N'enom3', N'Photo Nomor Rekening-Buku Tabungan Ebilling', N'1;2;3;4', N'1;2;3;4', NULL, CAST(N'2021-08-25 13:57:10.877' AS DateTime), NULL, NULL, N'Donation', N'Excel;PDF;Word;Picture (jpg.png)', N'Internal-Individual;Internal-Division;External;One Time Vendor', 1, 0)
GO
INSERT [dbo].[TB_M_AttachmentType_ENUM] ([AttachmentId], [CategoryCode], [Description], [FileType], [Vendor_Group], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [CategoryDesc], [FileTypeDesc], [Vendor_GroupDesc], [Ebiling], [Elvis]) VALUES (30193, N'enom3', N'Photo Nomor Rekening-Buku Tabungan Elvis', N'1;2;3;4', N'1;2;3;4', NULL, CAST(N'2021-08-25 13:57:34.087' AS DateTime), NULL, NULL, N'Donation', N'Excel;PDF;Word;Picture (jpg.png)', N'Internal-Individual;Internal-Division;External;One Time Vendor', 0, 1)
GO
SET IDENTITY_INSERT [dbo].[TB_M_AttachmentType_ENUM] OFF
GO
SET IDENTITY_INSERT [dbo].[TB_M_Question_ENUM] ON 

GO
INSERT [dbo].[TB_M_Question_ENUM] ([QuestionId], [CategoryCode], [Question], [IsYes], [WarningStatusYes], [IsNo], [WarningStatusNo], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (7, N'enom1', N'Entertain to TAM Employee', 0, N'NA', 1, N'BQ', NULL, NULL, N'TAM efaktur', CAST(N'2021-04-28 00:00:00.000' AS DateTime))
GO
INSERT [dbo].[TB_M_Question_ENUM] ([QuestionId], [CategoryCode], [Question], [IsYes], [WarningStatusYes], [IsNo], [WarningStatusNo], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (8, N'enom1', N'Entertain to Customer/Dealer or Vendor ?', 1, N'BQ', 1, N'OQ', NULL, NULL, N'efaktur.user', CAST(N'2021-07-15 14:41:56.000' AS DateTime))
GO
INSERT [dbo].[TB_M_Question_ENUM] ([QuestionId], [CategoryCode], [Question], [IsYes], [WarningStatusYes], [IsNo], [WarningStatusNo], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (9, N'enom1', N'Entertain to Government ? ', 0, N'NA', 1, N'OQ', NULL, NULL, N'efaktur.user', CAST(N'2021-07-15 14:40:26.000' AS DateTime))
GO
INSERT [dbo].[TB_M_Question_ENUM] ([QuestionId], [CategoryCode], [Question], [IsYes], [WarningStatusYes], [IsNo], [WarningStatusNo], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (10, N'enom1', N'Entertain to other Astra Group / TMC Group ?', 1, N'OQ', 0, N'NA', NULL, NULL, N'efaktur.user', CAST(N'2021-07-15 14:41:24.000' AS DateTime))
GO
INSERT [dbo].[TB_M_Question_ENUM] ([QuestionId], [CategoryCode], [Question], [IsYes], [WarningStatusYes], [IsNo], [WarningStatusNo], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (11, N'enom2', N'Promotion relate with the Business ?', 1, N'BQ', 0, N'NA', NULL, NULL, N'TAM efaktur', CAST(N'2021-04-28 00:00:00.000' AS DateTime))
GO
INSERT [dbo].[TB_M_Question_ENUM] ([QuestionId], [CategoryCode], [Question], [IsYes], [WarningStatusYes], [IsNo], [WarningStatusNo], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (12, N'enom2', N'Get Benefit Promotion ?', 1, N'BQ', 0, N'NA', NULL, NULL, N'TAM efaktur', CAST(N'2021-04-28 00:00:00.000' AS DateTime))
GO
INSERT [dbo].[TB_M_Question_ENUM] ([QuestionId], [CategoryCode], [Question], [IsYes], [WarningStatusYes], [IsNo], [WarningStatusNo], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (13, N'enom2', N'No Tax object and/or subject to final tax.', 1, N'BQ', 0, N'NA', NULL, NULL, N'TAM efaktur', CAST(N'2021-04-28 00:00:00.000' AS DateTime))
GO
INSERT [dbo].[TB_M_Question_ENUM] ([QuestionId], [CategoryCode], [Question], [IsYes], [WarningStatusYes], [IsNo], [WarningStatusNo], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (14, N'enom2', N'Advertising? ', 1, N'BQ', 1, N'OQ', NULL, NULL, N'efaktur.user', CAST(N'2021-07-15 14:38:45.000' AS DateTime))
GO
INSERT [dbo].[TB_M_Question_ENUM] ([QuestionId], [CategoryCode], [Question], [IsYes], [WarningStatusYes], [IsNo], [WarningStatusNo], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (15, N'enom2', N'Product exhibition ? Testing', 1, N'BQ', 1, N'OQ', NULL, NULL, N'efaktur.user', CAST(N'2021-07-15 14:37:23.000' AS DateTime))
GO
INSERT [dbo].[TB_M_Question_ENUM] ([QuestionId], [CategoryCode], [Question], [IsYes], [WarningStatusYes], [IsNo], [WarningStatusNo], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (16, N'enom2', N'Insentive?', 1, N'BQ', 1, N'OQ', NULL, NULL, N'efaktur.user', CAST(N'2021-07-06 09:15:45.000' AS DateTime))
GO
INSERT [dbo].[TB_M_Question_ENUM] ([QuestionId], [CategoryCode], [Question], [IsYes], [WarningStatusYes], [IsNo], [WarningStatusNo], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (17, N'enom2', N'Sponshorship?', 1, N'BQ', 1, N'OQ', NULL, NULL, N'efaktur.user', CAST(N'2021-07-06 09:14:24.000' AS DateTime))
GO
INSERT [dbo].[TB_M_Question_ENUM] ([QuestionId], [CategoryCode], [Question], [IsYes], [WarningStatusYes], [IsNo], [WarningStatusNo], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (18, N'enom2', N'Subsidy ?', 1, N'BQ', 1, N'OQ', NULL, NULL, N'efaktur.user', CAST(N'2021-07-05 00:00:00.000' AS DateTime))
GO
INSERT [dbo].[TB_M_Question_ENUM] ([QuestionId], [CategoryCode], [Question], [IsYes], [WarningStatusYes], [IsNo], [WarningStatusNo], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (19, N'enom3', N'Donation Through Indonesian Governement Official Institute', 1, N'BQ', 0, N'NA', NULL, NULL, N'efaktur.user', CAST(N'2021-07-15 14:39:41.000' AS DateTime))
GO
INSERT [dbo].[TB_M_Question_ENUM] ([QuestionId], [CategoryCode], [Question], [IsYes], [WarningStatusYes], [IsNo], [WarningStatusNo], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (20, N'enom3', N'National Disaster ?', 1, N'BQ', 1, N'OQ', NULL, NULL, N'TAM efaktur', CAST(N'2021-07-05 00:00:00.000' AS DateTime))
GO
INSERT [dbo].[TB_M_Question_ENUM] ([QuestionId], [CategoryCode], [Question], [IsYes], [WarningStatusYes], [IsNo], [WarningStatusNo], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (21, N'enom3', N'National Research and development ?', 1, N'BQ', 1, N'OQ', NULL, NULL, N'TAM efaktur', CAST(N'2021-07-05 00:00:00.000' AS DateTime))
GO
INSERT [dbo].[TB_M_Question_ENUM] ([QuestionId], [CategoryCode], [Question], [IsYes], [WarningStatusYes], [IsNo], [WarningStatusNo], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (22, N'enom3', N'National Educational facilities ?', 1, N'BQ', 1, N'OQ', NULL, NULL, N'TAM efaktur', CAST(N'2021-07-05 00:00:00.000' AS DateTime))
GO
INSERT [dbo].[TB_M_Question_ENUM] ([QuestionId], [CategoryCode], [Question], [IsYes], [WarningStatusYes], [IsNo], [WarningStatusNo], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (23, N'enom3', N'National Social infrastructure development ?', 1, N'BQ', 0, N'NA', NULL, NULL, N'efaktur.user', CAST(N'2021-07-06 11:38:18.000' AS DateTime))
GO
INSERT [dbo].[TB_M_Question_ENUM] ([QuestionId], [CategoryCode], [Question], [IsYes], [WarningStatusYes], [IsNo], [WarningStatusNo], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (10087, N'enom1', N'Entertainment relate to Business ?', 1, N'BQ', 0, N'NA', N'TAM Efaktur', CAST(N'2021-07-15 14:42:50.000' AS DateTime), N'efaktur.user', CAST(N'2021-08-12 19:51:16.000' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[TB_M_Question_ENUM] OFF
GO
SET IDENTITY_INSERT [dbo].[TB_M_Param_Document_Type_Detail_ENUM] ON 

GO
INSERT [dbo].[TB_M_Param_Document_Type_Detail_ENUM] ([DokDetailId], [DocumentId], [DetailFileType], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, 1, N'xls', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[TB_M_Param_Document_Type_Detail_ENUM] ([DokDetailId], [DocumentId], [DetailFileType], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, 1, N'xlsx', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[TB_M_Param_Document_Type_Detail_ENUM] ([DokDetailId], [DocumentId], [DetailFileType], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, 2, N'pdf', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[TB_M_Param_Document_Type_Detail_ENUM] ([DokDetailId], [DocumentId], [DetailFileType], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (4, 3, N'doc', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[TB_M_Param_Document_Type_Detail_ENUM] ([DokDetailId], [DocumentId], [DetailFileType], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (5, 3, N'docx', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[TB_M_Param_Document_Type_Detail_ENUM] ([DokDetailId], [DocumentId], [DetailFileType], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (6, 4, N'jpg', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[TB_M_Param_Document_Type_Detail_ENUM] ([DokDetailId], [DocumentId], [DetailFileType], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (7, 4, N'png', NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[TB_M_Param_Document_Type_Detail_ENUM] OFF
GO
SET IDENTITY_INSERT [dbo].[TB_M_Param_Document_Type_ENUM] ON 

GO
INSERT [dbo].[TB_M_Param_Document_Type_ENUM] ([DocumentId], [TypeFile], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, N'Excel', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[TB_M_Param_Document_Type_ENUM] ([DocumentId], [TypeFile], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, N'PDF', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[TB_M_Param_Document_Type_ENUM] ([DocumentId], [TypeFile], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, N'Word', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[TB_M_Param_Document_Type_ENUM] ([DocumentId], [TypeFile], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (4, N'Picture (jpg.png)', NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[TB_M_Param_Document_Type_ENUM] OFF
GO
