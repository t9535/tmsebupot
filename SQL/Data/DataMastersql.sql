INSERT  INTO TB_M_ConfigurationData
VALUES  ( NEWID(), 'TempDownloadFolderEnomElvis',
          'F:\Interface\Temp\Download\eNominative\', GETDATE(), 'system', NULL,
          NULL, 0 )
INSERT  INTO TB_M_ConfigurationData
VALUES  ( NEWID(), 'ShareFolder10.85.40.199Pass', 'Toyota2020', GETDATE(),
          'system', NULL, NULL, 0 )
INSERT  INTO TB_M_ConfigurationData
VALUES  ( NEWID(), 'ShareFolder10.85.40.199User', 'admin.tax', GETDATE(),
          'system', NULL, NULL, 0 )
INSERT  INTO TB_M_ConfigurationData
VALUES  ( NEWID(), 'ShareFolder10.85.40.199',
          '\\10.85.40.199\Interface\Temp\Download\eNominative\', GETDATE(),
          'system', NULL, NULL, 0 )
INSERT  INTO TB_M_ConfigurationData
VALUES  ( NEWID(), 'UrlTemplateDonation',
          '/FileStorage/TemplateUpload/Upload Donation.xls', GETDATE(),
          'system', NULL, NULL, 0 )
INSERT  INTO TB_M_ConfigurationData
VALUES  ( NEWID(), 'UrlTemplatePromotion',
          '/FileStorage/TemplateUpload/Upload Promotion.xls', GETDATE(),
          'system', NULL, NULL, 0 )
INSERT  INTO TB_M_ConfigurationData
VALUES  ( NEWID(), 'UrlTemplateEntertainment',
          '/FileStorage/TemplateUpload/Upload Entertainment.xls', GETDATE(),
          'system', NULL, NULL, 0 )
INSERT  INTO TB_M_ConfigurationData
VALUES  ( NEWID(), 'UrlTemplateNominativeData',
          '/FileStorage/TemplateUpload/Upload Nominative Data.xls', GETDATE(),
          'system', NULL, NULL, 0 )


INSERT  INTO TB_M_GeneralParam
VALUES  ( NEWID(), 'AC', 'Accrued', 'GLAccount', 'Accrued', GETDATE(),
          'system', NULL, NULL, 0 )
INSERT  INTO TB_M_GeneralParam
VALUES  ( NEWID(), 'AYES007', 'ASS', 'AccruedYearEndStatus',
          'Accrued-Settlement Suspense', GETDATE(), 'system', NULL, NULL, 0 )
INSERT  INTO TB_M_GeneralParam
VALUES  ( NEWID(), 'AYES002', 'ALS', 'AccruedYearEndStatus',
          'Accrued List-Suspense', GETDATE(), 'system', NULL, NULL, 0 )
INSERT  INTO TB_M_GeneralParam
VALUES  ( NEWID(), 'AYES001', 'ALD', 'AccruedYearEndStatus',
          'Accrued List-Direct', GETDATE(), 'system', NULL, NULL, 0 )
INSERT  INTO TB_M_GeneralParam
VALUES  ( NEWID(), 'PVT003', 'AYE', 'PVType', 'Accrued Year End', GETDATE(),
          'system', NULL, NULL, 0 )
INSERT  INTO TB_M_GeneralParam
VALUES  ( NEWID(), 'NFA002', 'Yes', 'NominativeFormAttachment', 'Yes',
          GETDATE(), 'system', NULL, NULL, 0 )
INSERT  INTO TB_M_GeneralParam
VALUES  ( NEWID(), 'NS002', 'System', 'NominativeStatus', 'System', GETDATE(),
          'system', NULL, NULL, 0 )
INSERT  INTO TB_M_GeneralParam
VALUES  ( NEWID(), 'NS001', 'Manual', 'NominativeStatus', 'Manual', GETDATE(),
          'system', NULL, NULL, 0 )
INSERT  INTO TB_M_GeneralParam
VALUES  ( NEWID(), 'AYES008', 'NA', 'AccruedYearEndStatus', 'N/A', GETDATE(),
          'system', NULL, NULL, 0 )
INSERT  INTO TB_M_GeneralParam
VALUES  ( NEWID(), 'PVT002', 'Settlement', 'PVType', 'Settlement', GETDATE(),
          'system', NULL, NULL, 0 )
INSERT  INTO TB_M_GeneralParam
VALUES  ( NEWID(), 'AYES005', 'PFS', 'AccruedYearEndStatus',
          'PV Form Settlement Suspense', GETDATE(), 'system', NULL, NULL, 0 )
INSERT  INTO TB_M_GeneralParam
VALUES  ( NEWID(), 'NFA001', 'N/A', 'NominativeFormAttachment', 'N/A',
          GETDATE(), 'system', NULL, NULL, 0 )
INSERT  INTO TB_M_GeneralParam
VALUES  ( NEWID(), 'AYES006', 'APFD', 'AccruedYearEndStatus',
          'Accrued-PV Form Direct', GETDATE(), 'system', NULL, NULL, 0 )
INSERT  INTO TB_M_GeneralParam
VALUES  ( NEWID(), 'AYES003', 'ALPP', 'AccruedYearEndStatus',
          'Accrued List-PR/PO', GETDATE(), 'system', NULL, NULL, 0 )
INSERT  INTO TB_M_GeneralParam
VALUES  ( NEWID(), 'RG', 'Reguler', 'GLAccount', 'Reguler', GETDATE(),
          'system', NULL, NULL, 0 )
INSERT  INTO TB_M_GeneralParam
VALUES  ( NEWID(), 'PVT001', 'Direct', 'PVType', 'Direct', GETDATE(), 'system',
          NULL, NULL, 0 )
INSERT  INTO TB_M_GeneralParam
VALUES  ( NEWID(), 'PVT004', 'Suspense', 'PVType', 'Suspense', GETDATE(), 'system',
          NULL, NULL, 0 )
INSERT  INTO TB_M_GeneralParam
VALUES  ( NEWID(), 'AYES004', 'PFD', 'AccruedYearEndStatus', 'PV Form Direct',
          GETDATE(), 'system', NULL, NULL, 0 )







