/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2016 (13.0.1601)
    Source Database Engine Edition : Microsoft SQL Server Enterprise Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2016
    Target Database Engine Edition : Microsoft SQL Server Enterprise Edition
    Target Database Engine Type : Standalone SQL Server
*/

USE [TAM_EFAKTUR]
GO

/****** Object:  Table [dbo].[TB_R_Entertainment_Detail_Temp]    Script Date: 21/08/2021 22:14:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TB_R_Entertainment_Detail_Temp](
	[ENTERTAINMENT_DETAIL_ID] [uniqueidentifier] NOT NULL,
	[ENTERTAINMENT_ID] [uniqueidentifier] NOT NULL,
	[NAMA_RELASI] [varchar](100) NULL,
	[POSISI_RELASI] [varchar](100) NULL,
	[PERUSAHAAN_RELASI] [varchar](200) NULL,
	[JENIS_USAHA_RELASI] [varchar](30) NULL,
	[KETERANGAN] [varchar](300) NULL,
	[CREATED_DT] [datetime] NULL,
	[CREATED_BY] [varchar](20) NULL,
	[CHANGED_DT] [datetime] NULL,
	[CHANGED_BY] [varchar](20) NULL,
	[RowStatus] [bit] NULL,
 CONSTRAINT [PK_TB_R_Entertainment_Detail_Temp] PRIMARY KEY CLUSTERED 
(
	[ENTERTAINMENT_DETAIL_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[TB_R_Entertainment_Detail_Temp]  WITH CHECK ADD  CONSTRAINT [FK_TB_R_Entertainment_Detail_Temp_TB_R_Entertainment_Temp] FOREIGN KEY([ENTERTAINMENT_ID])
REFERENCES [dbo].[TB_R_Entertainment_Temp] ([ENTERTAINMENT_ID])
GO

ALTER TABLE [dbo].[TB_R_Entertainment_Detail_Temp] CHECK CONSTRAINT [FK_TB_R_Entertainment_Detail_Temp_TB_R_Entertainment_Temp]
GO


