/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2016 (13.0.1601)
    Source Database Engine Edition : Microsoft SQL Server Enterprise Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2016
    Target Database Engine Edition : Microsoft SQL Server Enterprise Edition
    Target Database Engine Type : Standalone SQL Server
*/

USE [TAM_EFAKTUR]
GO

/****** Object:  Table [dbo].[TB_R_Donation]    Script Date: 21/08/2021 22:07:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TB_R_Donation](
	[DONATION_ID] [uniqueidentifier] NOT NULL,
	[DASHBOARD_ID] [uniqueidentifier] NOT NULL,
	[CATEGORY_CODE] [varchar](10) NULL,
	[THN_PAJAK] [int] NULL,
	[TMP_PENANDATANGAN] [varchar](100) NULL,
	[TGL_PENANDATANGAN] [datetime] NULL,
	[NAMA_PENANDATANGAN] [varchar](100) NULL,
	[JABATAN_PENANDATANGAN] [varchar](100) NULL,
	[JENIS_SUMBANGAN] [varchar](100) NULL,
	[BENTUK_SUMBANGAN] [varchar](50) NULL,
	[NILAI_SUMBANGAN] [decimal](18, 2) NULL,
	[TANGGAL_SUMBANGAN] [datetime] NULL,
	[NAMA_LEMBAGA_PENERIMA] [varchar](100) NULL,
	[NPWP_LEMBAGA_PENERIMA] [varchar](20) NULL,
	[ALAMAT_LEMBAGA] [varchar](200) NULL,
	[NO_TELP_LEMBAGA] [varchar](20) NULL,
	[SARANA_PRASARANA_INFRASTRUKTUR] [varchar](100) NULL,
	[LOKASI_INFRASTRUKTUR] [varchar](200) NULL,
	[BIAYA_PEMBANGUNAN_INFRASTRUKTUR] [decimal](18, 2) NULL,
	[IZIN_MENDIRIKAN_BANGUNAN] [varchar](200) NULL,
	[NOMOR_REKENING] [varchar](20) NULL,
	[NAMA_REKENING_PENERIMA] [varchar](100) NULL,
	[NAMA_BANK] [varchar](100) NULL,
	[CREATED_DT] [datetime] NULL,
	[CREATED_BY] [varchar](20) NULL,
	[CHANGED_DT] [datetime] NULL,
	[CHANGED_BY] [varchar](20) NULL,
 CONSTRAINT [PK_TB_R_DONATION] PRIMARY KEY CLUSTERED 
(
	[DONATION_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[TB_R_Donation]  WITH CHECK ADD  CONSTRAINT [FK_TB_R_Donation_TB_M_CategoryNominative_ENUM] FOREIGN KEY([CATEGORY_CODE])
REFERENCES [dbo].[TB_M_CategoryNominative_ENUM] ([CategoryCode])
GO

ALTER TABLE [dbo].[TB_R_Donation] CHECK CONSTRAINT [FK_TB_R_Donation_TB_M_CategoryNominative_ENUM]
GO

ALTER TABLE [dbo].[TB_R_Donation]  WITH CHECK ADD  CONSTRAINT [FK_TB_R_Donation_TB_R_Dashboard_Nominative] FOREIGN KEY([DASHBOARD_ID])
REFERENCES [dbo].[TB_R_Dashboard_Nominative] ([Id])
GO

ALTER TABLE [dbo].[TB_R_Donation] CHECK CONSTRAINT [FK_TB_R_Donation_TB_R_Dashboard_Nominative]
GO


