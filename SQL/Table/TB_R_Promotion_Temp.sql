/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2016 (13.0.1601)
    Source Database Engine Edition : Microsoft SQL Server Enterprise Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2016
    Target Database Engine Edition : Microsoft SQL Server Enterprise Edition
    Target Database Engine Type : Standalone SQL Server
*/

USE [TAM_EFAKTUR]
GO

/****** Object:  Table [dbo].[TB_R_Promotion_Temp]    Script Date: 21/08/2021 22:15:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TB_R_Promotion_Temp](
	[PROMOTION_ID] [uniqueidentifier] NOT NULL,
	[DASHBOARD_ID] [uniqueidentifier] NULL,
	[CATEGORY_CODE] [varchar](10) NULL,
	[THN_PAJAK] [int] NULL,
	[TMP_PENANDATANGAN] [varchar](100) NULL,
	[TGL_PENANDATANGAN] [datetime] NULL,
	[NAMA_PENANDATANGAN] [varchar](100) NULL,
	[JABATAN_PENANDATANGAN] [varchar](100) NULL,
	[NO] [int] NULL,
	[NAMA] [varchar](100) NULL,
	[NPWP] [varchar](20) NULL,
	[ALAMAT] [varchar](200) NULL,
	[CREATED_DT] [datetime] NULL,
	[CREATED_BY] [varchar](20) NULL,
	[CHANGED_DT] [datetime] NULL,
	[CHANGED_BY] [varchar](20) NULL,
	[RowStatus] [bit] NULL,
 CONSTRAINT [PK_TB_R_Promotion_Temp] PRIMARY KEY CLUSTERED 
(
	[PROMOTION_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[TB_R_Promotion_Temp]  WITH CHECK ADD  CONSTRAINT [FK_TB_R_Promotion_Temp_TB_M_CategoryNominative_ENUM] FOREIGN KEY([CATEGORY_CODE])
REFERENCES [dbo].[TB_M_CategoryNominative_ENUM] ([CategoryCode])
GO

ALTER TABLE [dbo].[TB_R_Promotion_Temp] CHECK CONSTRAINT [FK_TB_R_Promotion_Temp_TB_M_CategoryNominative_ENUM]
GO

ALTER TABLE [dbo].[TB_R_Promotion_Temp]  WITH CHECK ADD  CONSTRAINT [FK_TB_R_Promotion_Temp_TB_R_Dashboard_Nominative_Temp] FOREIGN KEY([DASHBOARD_ID])
REFERENCES [dbo].[TB_R_Dashboard_Nominative_Temp] ([Id])
GO

ALTER TABLE [dbo].[TB_R_Promotion_Temp] CHECK CONSTRAINT [FK_TB_R_Promotion_Temp_TB_R_Dashboard_Nominative_Temp]
GO


