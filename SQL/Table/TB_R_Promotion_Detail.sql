/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2016 (13.0.1601)
    Source Database Engine Edition : Microsoft SQL Server Enterprise Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2016
    Target Database Engine Edition : Microsoft SQL Server Enterprise Edition
    Target Database Engine Type : Standalone SQL Server
*/

USE [TAM_EFAKTUR]
GO

/****** Object:  Table [dbo].[TB_R_Promotion_Detail]    Script Date: 21/08/2021 22:01:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TB_R_Promotion_Detail](
	[PROMOTION_DETAIL_ID] [uniqueidentifier] NOT NULL,
	[PROMOTION_ID] [uniqueidentifier] NOT NULL,
	[TANGGAL] [datetime] NULL,
	[BENTUK_DAN_JENIS_BIAYA] [varchar](50) NULL,
	[JUMLAH] [decimal](18, 0) NULL,
	[JUMLAH_GROSS_UP] [decimal](18, 0) NULL,
	[KETERANGAN] [varchar](300) NULL,
	[JUMLAH_PPH] [decimal](18, 0) NULL,
	[JENIS_PPH] [varchar](50) NULL,
	[JUMLAH_NET] [decimal](18, 0) NULL,
	[NOMOR_REKENING] [varchar](20) NULL,
	[NAMA_REKENING_PENERIMA] [varchar](100) NULL,
	[NAMA_BANK] [varchar](100) NULL,
	[NO_KTP] [varchar](300) NULL,
	[CREATED_DT] [datetime] NULL,
	[CREATED_BY] [varchar](20) NULL,
	[CHANGED_DT] [datetime] NULL,
	[CHANGED_BY] [varchar](20) NULL,
 CONSTRAINT [PK_TB_R_Promotion_Detail] PRIMARY KEY CLUSTERED 
(
	[PROMOTION_DETAIL_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[TB_R_Promotion_Detail]  WITH CHECK ADD  CONSTRAINT [FK_TB_R_Promotion_Detail_TB_R_Promotion] FOREIGN KEY([PROMOTION_ID])
REFERENCES [dbo].[TB_R_Promotion] ([PROMOTION_ID])
GO

ALTER TABLE [dbo].[TB_R_Promotion_Detail] CHECK CONSTRAINT [FK_TB_R_Promotion_Detail_TB_R_Promotion]
GO


