/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2016 (13.0.1601)
    Source Database Engine Edition : Microsoft SQL Server Enterprise Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2016
    Target Database Engine Edition : Microsoft SQL Server Enterprise Edition
    Target Database Engine Type : Standalone SQL Server
*/

USE [TAM_EFAKTUR]
GO

/****** Object:  Table [dbo].[TB_R_Dashboard_Nominative_Temp]    Script Date: 21/08/2021 22:10:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TB_R_Dashboard_Nominative_Temp](
	[Id] [uniqueidentifier] NOT NULL,
	[CategoryCode] [varchar](10) NULL,
	[StatusInvoice] [varchar](20) NULL,
	[PVType] [varchar](30) NULL,
	[SettlementStatus] [varchar](30) NULL,
	[AccruedYearEndStatus] [varchar](30) NULL,
	[AccruedBookingNo] [varchar](30) NULL,
	[DescriptionTransaction] [varchar](100) NULL,
	[GLAccount] [varchar](20) NULL,
	[VendorName] [varchar](200) NULL,
	[NPWP] [varchar](30) NULL,
	[InvoiceNo] [varchar](30) NULL,
	[InvoiceDate] [datetime] NULL,
	[BookPeriod] [datetime] NULL,
	[TurnoverAmount] [decimal](18, 2) NULL,
	[VATAmount] [decimal](18, 2) NULL,
	[WHTAmount] [decimal](18, 2) NULL,
	[TaxInvoiceNo] [varchar](30) NULL,
	[WitholdingTaxNo] [varchar](30) NULL,
	[WHTArticle] [varchar](30) NULL,
	[WitholdingTaxDate] [datetime] NULL,
	[System] [varchar](30) NULL,
	[PVNo] [varchar](30) NULL,
	[PVCreatedBy] [varchar](30) NULL,
	[SAPGRDocNo] [varchar](30) NULL,
	[SAPGRPostingDate] [datetime] NULL,
	[SAPInvoiceDocNo] [varchar](30) NULL,
	[SAPInvoicePostingDate] [datetime] NULL,
	[ReceiveFileDate] [datetime] NULL,
	[DownloadedDate] [datetime] NULL,
	[DownloadStatus] [varchar](30) NULL,
	[NominativeFormAttachment] [varchar](max) NULL,
	[NominativeIDNo] [varchar](30) NULL,
	[NominativeStatus] [varchar](30) NULL,
	[NominativeDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedOn] [datetime] NULL,
	[RowStatus] [bit] NULL,
 CONSTRAINT [PK_TB_R_Dashboard_Nominative_Temp] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[TB_R_Dashboard_Nominative_Temp]  WITH CHECK ADD  CONSTRAINT [FK_TB_R_Dashboard_Nominative_Temp_TB_M_CategoryNominative_ENUM] FOREIGN KEY([CategoryCode])
REFERENCES [dbo].[TB_M_CategoryNominative_ENUM] ([CategoryCode])
GO

ALTER TABLE [dbo].[TB_R_Dashboard_Nominative_Temp] CHECK CONSTRAINT [FK_TB_R_Dashboard_Nominative_Temp_TB_M_CategoryNominative_ENUM]
GO


