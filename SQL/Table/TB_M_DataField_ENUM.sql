/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2016 (13.0.1601)
    Source Database Engine Edition : Microsoft SQL Server Enterprise Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2016
    Target Database Engine Edition : Microsoft SQL Server Enterprise Edition
    Target Database Engine Type : Standalone SQL Server
*/

USE [TAM_EFAKTUR]
GO

/****** Object:  Table [dbo].[TB_M_DataField_ENUM]    Script Date: 21/08/2021 21:59:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TB_M_DataField_ENUM](
	[DataFieldId] [bigint] IDENTITY(1,1) NOT NULL,
	[CategoryCode] [varchar](10) NOT NULL,
	[FieldNameByTable] [varchar](100) NULL,
	[FieldNameByExcel] [varchar](200) NULL,
	[VendorGroup] [varchar](500) NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_TB_M_DataField] PRIMARY KEY CLUSTERED 
(
	[DataFieldId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[TB_M_DataField_ENUM]  WITH CHECK ADD  CONSTRAINT [FK_TB_M_DataField_ENUM_TB_M_CategoryNominative_ENUM] FOREIGN KEY([CategoryCode])
REFERENCES [dbo].[TB_M_CategoryNominative_ENUM] ([CategoryCode])
GO

ALTER TABLE [dbo].[TB_M_DataField_ENUM] CHECK CONSTRAINT [FK_TB_M_DataField_ENUM_TB_M_CategoryNominative_ENUM]
GO


