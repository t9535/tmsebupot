/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2016 (13.0.1601)
    Source Database Engine Edition : Microsoft SQL Server Enterprise Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2016
    Target Database Engine Edition : Microsoft SQL Server Enterprise Edition
    Target Database Engine Type : Standalone SQL Server
*/

USE [TAM_EFAKTUR]
GO

/****** Object:  Table [dbo].[TB_M_Nominative_ENUM]    Script Date: 21/08/2021 21:56:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TB_M_Nominative_ENUM](
	[NominativeId] [bigint] IDENTITY(1,1) NOT NULL,
	[CategoryCode] [varchar](10) NOT NULL,
	[GLAccount] [varchar](30) NULL,
	[DescriptionTransaction] [varchar](200) NOT NULL,
	[Type] [varchar](30) NOT NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_TB_M_Nominative] PRIMARY KEY CLUSTERED 
(
	[NominativeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[TB_M_Nominative_ENUM]  WITH CHECK ADD  CONSTRAINT [FK_TB_M_Nominative_ENUM_TB_M_CategoryNominative_ENUM] FOREIGN KEY([CategoryCode])
REFERENCES [dbo].[TB_M_CategoryNominative_ENUM] ([CategoryCode])
GO

ALTER TABLE [dbo].[TB_M_Nominative_ENUM] CHECK CONSTRAINT [FK_TB_M_Nominative_ENUM_TB_M_CategoryNominative_ENUM]
GO


