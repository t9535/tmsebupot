USE [TAM_EFAKTUR]
GO

IF NOT EXISTS(SELECT * FROM SYS.COLUMNS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[TB_R_VATIn]') AND NAME = 'IdNuminative')
BEGIN
    PRINT 'Add column : [dbo].[TB_R_VATIn].IdNuminative'
    ALTER TABLE [dbo].[TB_R_VATIn]
        ADD [IdNuminative] BIGINT NULL
END
GO

IF NOT EXISTS(SELECT * FROM SYS.COLUMNS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[TB_R_VATIn]') AND NAME = 'IdNuminativeEbill')
BEGIN
    PRINT 'Add column : [dbo].[TB_R_VATIn].IdNuminativeEbill'
    ALTER TABLE [dbo].[TB_R_VATIn]
        ADD [IdNuminativeEbill] BIGINT NULL
END
GO


