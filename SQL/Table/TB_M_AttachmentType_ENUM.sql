/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2016 (13.0.1601)
    Source Database Engine Edition : Microsoft SQL Server Enterprise Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2016
    Target Database Engine Edition : Microsoft SQL Server Enterprise Edition
    Target Database Engine Type : Standalone SQL Server
*/

USE [TAM_EFAKTUR]
GO

/****** Object:  Table [dbo].[TB_M_AttachmentType_ENUM]    Script Date: 21/08/2021 21:57:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TB_M_AttachmentType_ENUM](
	[AttachmentId] [bigint] IDENTITY(1,1) NOT NULL,
	[CategoryCode] [varchar](10) NOT NULL,
	[Description] [varchar](100) NOT NULL,
	[FileType] [varchar](500) NULL,
	[Vendor_Group] [varchar](500) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[CategoryDesc] [varchar](100) NULL,
	[FileTypeDesc] [varchar](500) NULL,
	[Vendor_GroupDesc] [varchar](500) NULL,
	[Ebiling] [bit] NULL,
	[Elvis] [bit] NULL,
 CONSTRAINT [PK_TB_M_AttachmentType] PRIMARY KEY CLUSTERED 
(
	[AttachmentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[TB_M_AttachmentType_ENUM]  WITH CHECK ADD  CONSTRAINT [FK_TB_M_AttachmentType_ENUM_TB_M_CategoryNominative_ENUM] FOREIGN KEY([CategoryCode])
REFERENCES [dbo].[TB_M_CategoryNominative_ENUM] ([CategoryCode])
GO

ALTER TABLE [dbo].[TB_M_AttachmentType_ENUM] CHECK CONSTRAINT [FK_TB_M_AttachmentType_ENUM_TB_M_CategoryNominative_ENUM]
GO


