/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2016 (13.0.1601)
    Source Database Engine Edition : Microsoft SQL Server Enterprise Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2016
    Target Database Engine Edition : Microsoft SQL Server Enterprise Edition
    Target Database Engine Type : Standalone SQL Server
*/

USE [TAM_EFAKTUR]
GO

/****** Object:  Table [dbo].[TB_M_TransSequence]    Script Date: 21/08/2021 22:08:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TB_M_TransSequence](
	[MSSequenceID] [varchar](50) NOT NULL,
	[BranchID] [char](3) NOT NULL,
	[SequenceName] [varchar](50) NOT NULL,
	[SequenceNo] [int] NOT NULL,
	[LengthNo] [int] NOT NULL,
	[ResetFlag] [char](1) NOT NULL,
	[Prefix] [varchar](10) NULL,
	[IsBranch] [bit] NOT NULL,
	[IsYear] [bit] NOT NULL,
	[IsMonth] [bit] NOT NULL,
	[Suffix] [varchar](10) NULL,
	[UsrUpd] [varchar](20) NOT NULL,
	[DtmUpd] [datetime] NOT NULL,
 CONSTRAINT [PK_TB_M_TransSequence] PRIMARY KEY CLUSTERED 
(
	[MSSequenceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


